//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

# include "Operators/LocalVariationalOperatorInstances/___VARIABLE_operatorNature___Form/___FILEBASENAMEASIDENTIFIER___.hpp"


namespace MoReFEM
{
    
    
    namespace GlobalVariationalOperatorNS
    {
        
        
        class ___FILEBASENAMEASIDENTIFIER___ final : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            ___FILEBASENAMEASIDENTIFIER___,
            Advanced::LocalVariationalOperatorNS::___FILEBASENAMEASIDENTIFIER___
        >
        {
            
        public:
            
            //! \copydoc doxygen_hide_alias_self
            // \TODO This might seem a bit dumb but is actually very convenient for template classes.
            using self = ___FILEBASENAMEASIDENTIFIER___;
            
            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const self>;
            
            //! Returns the name of the operator.
            static const std::string& ClassName();
            
            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::___FILEBASENAMEASIDENTIFIER___;
            
            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                local_operator_type
            >;
            
            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            
            
        public:
            
            /// \name Special members.
            ///@{
            
            /*!
             * \brief Constructor.
             *
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit ___FILEBASENAMEASIDENTIFIER___(const FEltSpace& felt_space,
                                                    const Unknown& unknown,
                                                    // POSSIBLY ANOTHER UNKNOWN IF 2 UNKNOWN OPERATOR
                                                    unsigned int mesh_dimension,
                                                    DoComputeProcessorWiseLocal2Global do_consider_processor_wise_local_2_global, // If not locked by the operator (for instance probably yes for non linear operators)
                                                    // POSSIBLY ADDITIONAL ARGUMENTS SPECIFIC TO OPERATOR
                                                    const QuadratureRulePerTopology* const quadrature_rule_per_topology
                                                    );
            
            //! Destructor.
            ~___FILEBASENAMEASIDENTIFIER___() = default;
            
            //! \copydoc doxygen_hide_copy_constructor
            ___FILEBASENAMEASIDENTIFIER___(const ___FILEBASENAMEASIDENTIFIER___& rhs) = delete;
            
            //! \copydoc doxygen_hide_move_constructor
            ___FILEBASENAMEASIDENTIFIER___(___FILEBASENAMEASIDENTIFIER___&& rhs) = delete;
            
            //! \copydoc doxygen_hide_copy_affectation
            ___FILEBASENAMEASIDENTIFIER___& operator=(const ___FILEBASENAMEASIDENTIFIER___& rhs) = delete;
            
            //! \copydoc doxygen_hide_move_affectation
            ___FILEBASENAMEASIDENTIFIER___& operator=(___FILEBASENAMEASIDENTIFIER___&& rhs) = delete;
            
            ///@}
         
            
        public:
            
            // TODO Remove matrix references in the text if linear operator.
            // TODO Remove vector references in the text if bilinear operator.
            
            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
             * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             *
             */
            
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;
            
            
        private:
            
            // \TODO If your operator doesn't have any supplementary argument, just delete the method below.
            // If not, it must set data attributes in LocalVariationalOperator for the parameters required at the local level.
            // You should have a look to SecondPiolaKirchhoffStressTensor or TransientSource operator to
            // catch the drift of it.
            
            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple</* Add here the list of types of GlobalVariational operator to be 'converted' for the LocalVariationalOperator.*/>& additional_arguments) const;
            
            
        };
        
        
    } // namespace GlobalVariationalOperatorNS
    
    
} // namespace MoReFEM


# include "OperatorInstances/VariationalOperator/___VARIABLE_operatorNature___Form/___FILEBASENAME___.hxx"


#endif 
