//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#include "OperatorInstances/VariationalOperator/___VARIABLE_operatorNature___Form/___FILEBASENAME___.hpp"


namespace MoReFEM
{
    
    
    namespace GlobalVariationalOperatorNS
    {
        
        
        ___FILEBASENAMEASIDENTIFIER___::___FILEBASENAMEASIDENTIFIER___(FEltSpace& felt_space,
                                                                       const Unknown& unknown,
                                                                       // POSSIBLY ANOTHER UNKNOWN IF 2 UNKNOWN OPERATOR
                                                                       unsigned int mesh_dimension,
                                                                       const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                                                       DoComputeProcessorWiseLocal2Global do_consider_processor_wise_local_2_global, // If not locked by the operator (for instance probably yes for non linear operators)
        // POSSIBLY ADDITIONAL ARGUMENTS SPECIFIC TO OPERATOR
        )

        : parent(felt_space,
        unknown,
                 // \TODO Possibly another unknown ,
        mesh_dimension,
        AllocateGradientFEltPhi::no, // \TODO CHOOSE yes if you need to call at some point GetGradientFeltPhi() at each quadrature point.
        // \TODO Add possibly other arguments.
        {
        }
        
        
        const std::string& ___FILEBASENAMEASIDENTIFIER___::ClassName()
        {
            static std::string name("___FILEBASENAMEASIDENTIFIER___");
            return name;
        }
        
        
        template<class LocalOperatorTypeT>
         void ___FILEBASENAMEASIDENTIFIER___
         ::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                       LocalOperatorTypeT& local_operator,
                                       const std::tuple</* Add here the list of types of GlobalVariational operator to be 'converted' for the LocalVariationalOperator.*/>& additional_arguments) const
         {
             auto& tuple = local_operator.GetNonCstComputeEltArrayTuple();
             
             // \TODO There should be something like:
             // std::get<0>(tuple) = f(std::get<0>(additional_arguments));
             
        }
        

     
        
    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM
