//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER_______FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER_______FILEBASENAMEASIDENTIFIER_____HPP

# include "Core/InputData/Instances/Crtp/Section.hpp"

# include "Core/InputData/Instances/Parameter/Advanced/UsualDescription.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        //! \copydoc doxygen_hide_core_input_data_section
        struct ___FILEBASENAMEASIDENTIFIER___ : public Crtp::Section<___FILEBASENAMEASIDENTIFIER___, NoEnclosingSection>
        {
            
            
            
            //! Convenient alias.
            using self = ___FILEBASENAMEASIDENTIFIER___;
            
            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;
            friend parent;
            
            
            /*!
             * \brief Return the name of the section in the input parameter.
             *
             */
            static const std::string& GetName();
            
            
            
            /*!
             * \brief Choose how is described the viscosity (through a scalar, a function, etc...)
             */
            struct Nature : public Crtp::InputData<Nature, self, typename Advanced::InputDataNS::ParamNS::Nature<>::storage_type>,
            public Advanced::InputDataNS::ParamNS::Nature<>
            { };
            
            
            
            /*!
             * \brief Scalar value. Irrelevant if nature is not scalar.
             */
            struct Scalar : public Crtp::InputData<Scalar, self, Internal::InputData::Scalar::storage_type>,
            public Internal::InputData::Scalar
            { };
            
            
            /*!
             * \brief Function that determines viscosity value. Irrelevant if nature is not lua_function.
             */
            struct LuaFunction : public Crtp::InputData<LuaFunction, self, Internal::InputData::LuaFunction::storage_type>,
            public Internal::InputData::LuaFunction
            { };
            
            
            
            /*!
             * \brief Piecewise Constant domain index.
             */
            struct PiecewiseConstantByDomainId : public Crtp::InputData<PiecewiseConstantByDomainId, self, Internal::InputData::PiecewiseConstantByDomainId::storage_type>,
            public Internal::InputData::PiecewiseConstantByDomainId
            { };
            
            
            /*!
             * \brief Piecewise Constant value by domain.
             */
            struct PiecewiseConstantByDomainValue : public Crtp::InputData<PiecewiseConstantByDomainValue, self, Internal::InputData::PiecewiseConstantByDomainValue::storage_type>,
            public Internal::InputData::PiecewiseConstantByDomainValue
            { };
            
            
            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                Nature,
                    Value
            >;
            
            
        private:
            
            //! Content of the section.
            section_content_type section_content_;
            
            
        }; // struct ___FILEBASENAMEASIDENTIFIER___
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


# include "Core/InputData/Instances/Parameter/___FILEBASENAME___.hxx"


#endif /* defined(_____PROJECTNAMEASIDENTIFIER_______FILEBASENAMEASIDENTIFIER_____HPP) */
