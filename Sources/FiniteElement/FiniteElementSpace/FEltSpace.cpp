/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:45:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
        
        
        unsigned int ExtractDimensionFromDomain(const Domain& domain)
        {
            if (!domain.IsConstraintOn<DomainNS::Criterion::dimension>())
            {
                std::ostringstream oconv;
                oconv << "By design, a finite element space covers one and only one dimension; current underlying "
                "domain cover doesn't get any dimension restriction.";
                throw Exception(oconv.str(), __FILE__,  __LINE__);
            }
            
            
            if (domain.GetDimensionList().size() != 1u)
            {
                std::ostringstream oconv;
                oconv << "By design, a finite element space covers one and only one dimension; current underlying "
                "domain cover " << domain.GetDimensionList().size() << " different dimensions.";
                throw Exception(oconv.str(), __FILE__,  __LINE__);
            }
            
            return domain.GetDimensionList().back();
        }
        
        
        NumberingSubset::vector_const_shared_ptr
        ComputeNumberingSubsetList(const ExtendedUnknown::vector_const_shared_ptr& unknown_list)
        {
            NumberingSubset::vector_const_shared_ptr ret;
            
            for (const auto& extended_unknown_ptr : unknown_list)
            {
                assert(!(!extended_unknown_ptr));
                ret.push_back(extended_unknown_ptr->GetNumberingSubsetPtr());
            }
            
            Utilities::EliminateDuplicate(ret);
            
            return ret;
        }

        
        
    } // namespace anonymous
    
    
    const std::string& FEltSpace::ClassName()
    {
        static std::string ret("FEltSpace");
        return ret;
    }
    
    
    FEltSpace::FEltSpace(const std::shared_ptr<const GodOfDof>& god_of_dof_ptr,
                         const Domain& domain,
                         const unsigned int unique_id,
                         ExtendedUnknown::vector_const_shared_ptr&& extended_unknown_list)
    : Crtp::CrtpMpi<FEltSpace>(god_of_dof_ptr->GetMpi()),
    unique_id_parent(unique_id),
    felt_storage_(nullptr),
    dimension_(ExtractDimensionFromDomain(domain)),
    god_of_dof_(god_of_dof_ptr),
    domain_(domain),
    extended_unknown_list_(std::move(extended_unknown_list)),
    numbering_subset_list_(ComputeNumberingSubsetList(GetExtendedUnknownList()))
    {
        assert(!(!god_of_dof_ptr));
    
        SetFEltList(god_of_dof_ptr->GetMesh(), domain);
        
        if (IsEmpty() && god_of_dof_ptr->GetMpi().IsRootProcessor())
        {
            std::ostringstream oconv;
            oconv << "[WARNING] Finite element space " << GetUniqueId() << " does not seem to store any finite element; "
            "the most likely explanation is that the domain upon which it is built is ill-formed.";
            std::cout << oconv.str();
        }

     
        // Beware: some operations are not possible at this stage as the FEltSpace is actually initialized completely
        // along with full initialization of its GodOfDof, which is not complete when current constructor is called.
        // That's the reason for instance default_quadrature_rule_per_topology_ is defined in subsequent InitLocal2Global()
        // rather than here.
        
    }
    
    
    void FEltSpace::InitLocal2Global(DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global)
    {
        const auto& felt_storage = GetLocalFEltSpacePerRefLocalFEltSpace();
        
        const auto& numbering_subset_list = GetNumberingSubsetList();
        
        for (const auto& pair : felt_storage)
        {
            const auto& local_felt_space_list = pair.second;
            
            for (auto& local_felt_space_pair : local_felt_space_list)
            {
                auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));
                
                local_felt_space_ptr->InitLocal2Global(numbering_subset_list,
                                                       do_consider_processor_wise_local_2_global);
            }
        }
        
        const auto max_order = ComputeMaxOrderFElt();
        
        default_quadrature_rule_per_topology_ = std::make_unique<QuadratureRulePerTopology>(DEFAULT_DEGREE_OF_EXACTNESS,
                                                                             max_order);

    }
    
    
    void FEltSpace::ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                        DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global) const
    {
        assert(!extended_unknown_list.empty());
        
        #ifndef NDEBUG
        if (do_compute_processor_wise_local_2_global == DoComputeProcessorWiseLocal2Global::yes)
        {
            assert(GetGodOfDofFromWeakPtr()->GetDoConsiderProcessorWiseLocal2Global() == DoConsiderProcessorWiseLocal2Global::yes
                   && "Unable to compute local2global if pre-initialization has not been performed.");
        }
        
        assert(std::none_of(extended_unknown_list.cbegin(),
                            extended_unknown_list.cend(),
                            Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
        
        #endif // NDEBUG
        
        const auto& felt_storage = GetLocalFEltSpacePerRefLocalFEltSpace();

        for (const auto& pair : felt_storage)
        {
            const auto& local_felt_space_list = pair.second;
            
            for (auto& local_felt_space_pair : local_felt_space_list)
            {
                auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));
                
                local_felt_space_ptr->ComputeLocal2Global(extended_unknown_list,
                                                          do_compute_processor_wise_local_2_global);
            }
        }
    }

    
    void FEltSpace::SetFEltList(const Mesh& mesh, const Domain& domain)
    {
        const auto felt_space_dimension = GetDimension();
        const auto&& bag = mesh.BagOfEltType(felt_space_dimension);
        
        const auto& unknown_storage = GetExtendedUnknownList();
        
        LocalFEltSpacePerRefLocalFEltSpace felt_list_per_type;
        
        for (const auto& geometric_type_ptr : bag)
        {
            assert(!(!geometric_type_ptr));

            if (!domain.DoRefGeomEltMatchCriteria(*geometric_type_ptr))
                continue;

            auto&& ref_felt_space_ptr =
                std::make_unique<const Internal::RefFEltNS::RefLocalFEltSpace>(geometric_type_ptr,
                                                                         unknown_storage,
                                                                         mesh.GetDimension(),
                                                                         felt_space_dimension);
            
            auto geometric_elt_range = mesh.GetSubsetGeometricEltList(*geometric_type_ptr);
            
            LocalFEltSpace::per_geom_elt_index local_felt_space_list;
            local_felt_space_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
            
            assert(geometric_elt_range.second > geometric_elt_range.first);
            
            const std::size_t Ngeometric_elt_in_range =
            static_cast<std::size_t>(geometric_elt_range.second - geometric_elt_range.first);
            
            local_felt_space_list.reserve(Ngeometric_elt_in_range);

            for (auto it = geometric_elt_range.first; it != geometric_elt_range.second; ++it)
            {
                const auto& geometric_elt_ptr = *it;
                assert(!(!geometric_elt_ptr));
                const auto& geometric_elt = *geometric_elt_ptr;
                assert(geometric_elt.GetIdentifier() == geometric_type_ptr->GetIdentifier());
                
                if (domain.IsGeometricEltInside(geometric_elt))
                {
                    auto&& ptr = new LocalFEltSpace(*ref_felt_space_ptr, geometric_elt_ptr);
                    local_felt_space_list.insert({ geometric_elt.GetIndex(),  LocalFEltSpace::shared_ptr(ptr)});
                }
            }

            assert(Ngeometric_elt_in_range >= local_felt_space_list.size()); // equal if no domain restriction applied.
            
            if (local_felt_space_list.empty()) // might happen if for instance RefGeomElt not considered in the domain.
                continue;

            felt_list_per_type.push_back(std::make_pair(std::move(ref_felt_space_ptr), local_felt_space_list));
        }
        
        felt_storage_ = std::make_unique<Internal::FEltSpaceNS::Storage>(GetMpi(),
                                                                        std::move(felt_list_per_type));
    }
    
    

    # ifndef NDEBUG
    void FEltSpace::AssertGodOfDofInitialized() const
    {
        assert(GetGodOfDofFromWeakPtr()->HasInitBeenCalled()
               && "Finite element and node list not yet existing; GodOfDof::Init() must be called first!");
    }
    
    # endif // NDEBUG
    
    
        
    ExtendedUnknown::const_shared_ptr FEltSpace::GetExtendedUnknownPtr(const Unknown& unknown) const
    {
        const auto& unknown_storage = GetExtendedUnknownList();
        auto it = std::find_if(unknown_storage.cbegin(), unknown_storage.cend(),
                               [&unknown](const auto& extended_unknown_ptr)
                               {
                                   assert(!(!extended_unknown_ptr));
                                   return extended_unknown_ptr->GetUnknown() == unknown;
                               }
                               );
        
        assert(it != unknown_storage.cend() && "Your unknown is probably not present in the \a FEltSpace upon which "
               "the operator is defined.");
        
        return *it;
    }
    
    
    void FEltSpace::ClearTemporaryData() const noexcept
    {
        const auto& storage = GetLocalFEltSpacePerRefLocalFEltSpace();
        
        for (const auto& pair : storage)
        {
            const auto& local_felt_space_list = pair.second;
            
            for (const auto& local_felt_space_pair : local_felt_space_list)
            {
                auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));
                local_felt_space_ptr->ClearTemporaryData();
            }
        }
    }
    
    
    namespace // anonymous
    {
        
        
        auto IteratorLocalFEltSpacePair(const FEltSpace& felt_space,
                                        const RefGeomElt& ref_geom_elt)
        {
            const auto& full_storage = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace();
            
            const auto ref_geom_elt_id = ref_geom_elt.GetIdentifier();
            
            auto it = std::find_if(full_storage.cbegin(),
                                   full_storage.cend(),
                                   [ref_geom_elt_id](const auto& pair)
                                   {
                                       const auto& current_ref_felt_space = pair.first;
                                       assert(!(!current_ref_felt_space));
                                       // Two ref felt spaces are equal if they are related to the same ref geometric element.
                                       return current_ref_felt_space->GetRefGeomElt().GetIdentifier() == ref_geom_elt_id;
                                   });
            
            assert(it != full_storage.cend());
            return it;
        }
        
        
        
        
    } // namespace anonymous
    
    
    const LocalFEltSpace::per_geom_elt_index& FEltSpace
    ::GetLocalFEltSpaceList(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space) const
    {
        auto it = IteratorLocalFEltSpacePair(*this, ref_felt_space.GetRefGeomElt());
        return it->second;
    }
    
    
    const Internal::RefFEltNS::RefLocalFEltSpace& FEltSpace::GetRefLocalFEltSpace(const RefGeomElt& ref_geom_elt) const
    {
        auto it = IteratorLocalFEltSpacePair(*this, ref_geom_elt);
        assert(!(!it->first));
        return *(it->first);
    }
    

    
    unsigned int FEltSpace::GetMeshDimension() const
    {
        const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();
        return god_of_dof_ptr->GetMesh().GetDimension();
        
    }
    
    
    namespace // anonymous
    {
        
        
        /*!
         * \brief Helper to ComputeDofList.
         *
         * \param[in] felt_space_dof_unique_id_list Unique ids of all the dofs involved in the finite element space,
         * regardless of partitioning.
         * \param[in] processor_or_ghost_wise_dof_list Processor- OR ghost- wise dof list in the whole GodOfDof.
         * \return Processor- OR ghost- dofs that belongs to the finite element space.
         */
        Dof::vector_shared_ptr ComputeDofListHelper(const std::vector<unsigned int>& felt_space_dof_unique_id_list,
                                                    const Dof::vector_shared_ptr& processor_or_ghost_wise_dof_list)
        {
            Dof::vector_shared_ptr ret;
            assert(std::is_sorted(processor_or_ghost_wise_dof_list.cbegin(),
                                  processor_or_ghost_wise_dof_list.cend(),
                                  Utilities::PointerComparison::Less<Dof::shared_ptr>()));
            
            const auto begin = felt_space_dof_unique_id_list.cbegin();
            const auto end = felt_space_dof_unique_id_list.cend();
            
            for (const auto& dof_ptr : processor_or_ghost_wise_dof_list)
            {
                assert(!(!dof_ptr));
                const auto unique_id = dof_ptr->GetUniqueId();
                
                if (std::binary_search(begin, end, unique_id))
                    ret.push_back(dof_ptr);
            }
            
//            std::sort(ret.begin(),
//                      ret.end(),
//                      Utilities::PointerComparison::Less<Dof::shared_ptr>());
            
            return ret;
            
        }
        
        
    } // namespace anonymous
    
    
    void FEltSpace::ComputeDofList(const std::vector<unsigned int>& dof_unique_id_list)
    {
        const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();

        dof_list_ = ComputeDofListHelper(dof_unique_id_list,
                                         god_of_dof_ptr->GetProcessorWiseDofList());

        ghost_dof_list_ = ComputeDofListHelper(dof_unique_id_list,
                                               god_of_dof_ptr->GetGhostedDofList());
    }
    
    
    const LocalFEltSpace&
    FEltSpace::GetLocalFEltSpace(const GeometricElt& geometric_elt) const
    {
        const auto& ref_geom_elt = geometric_elt.GetRefGeomElt();
        
        const auto& local_felt_list = GetLocalFEltSpaceList(ref_geom_elt);
        
        const auto it = local_felt_list.find(geometric_elt.GetIndex());
        assert(it != local_felt_list.cend() && "If this happens, there's likely a discrepancy somewhere in an operator "
               "between \a FEltSpace and for instance a ParameterAtDof defined on a wholly different mesh.");
        
        const auto& local_felt_space_ptr = it->second;
        
        assert(!(!local_felt_space_ptr));
        
        return *local_felt_space_ptr;
    }
    
    
    Dof::vector_shared_ptr FEltSpace::GetProcessorWiseDofList(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) complete_list = GetProcessorWiseDofList();
        
        Dof::vector_shared_ptr ret;
        ret.reserve(complete_list.size());
        const auto end = complete_list.cend();
        
        std::copy_if(complete_list.cbegin(),
                     end,
                     std::back_inserter(ret),
                     [&numbering_subset](const auto& dof_ptr)
                     {
                         assert(!(!dof_ptr));
                         return dof_ptr->IsInNumberingSubset(numbering_subset);
                     });
        
        ret.shrink_to_fit();
        
        return ret;
    }
    
    
    void FEltSpace::SetMovemeshData()
    {
        assert(GetDimension() == GetMeshDimension() && "It makes little sense to move only contours of the mesh!");
        
        const auto& numbering_subset_list = GetNumberingSubsetList();
        
        decltype(auto) extended_unknown_list = GetExtendedUnknownList();
        const auto begin_extended_unknown_list = extended_unknown_list.cbegin();
        const auto end_extended_unknown_list = extended_unknown_list.cend();
        
        const auto god_of_dof_ptr = GetGodOfDofFromWeakPtr();
        const auto& god_of_dof = *god_of_dof_ptr;

        #ifndef NDEBUG
        decltype(auto) felt_space_processor_wise_dof_list = GetProcessorWiseDofList();
        decltype(auto) felt_space_ghosted_dof_list = GetGhostedDofList();
        #endif // NDEBUG
        
        auto& movemesh_helper_data = GetNonCstMovemeshHelperStorage();
        
        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;
            
            if (!numbering_subset.DoMoveMesh())
                continue;
            
            // Determine which unknown provides the actual displacement to move the mesh.
            auto find_unknown = [&numbering_subset](const auto& ptr)
            {
                assert(!(!ptr));
                return ptr->GetNumberingSubset() == numbering_subset;
            };
            
            auto it = std::find_if(begin_extended_unknown_list,
                                   end_extended_unknown_list,
                                   find_unknown);
            
            assert(it != end_extended_unknown_list);
            assert(std::count_if(begin_extended_unknown_list, end_extended_unknown_list, find_unknown) == 1);
            const auto& extended_unknown = (*(*it));
            assert(extended_unknown.GetNature() == UnknownNS::Nature::vectorial && "Unknown should be a displacement!");
            
            // Create an helper object which retains all relevant informations to perform the move during the course
            // of the time iterations.
            auto&& helper_per_numbering_subset =
                std::make_pair(numbering_subset.GetUniqueId(), Internal::FEltSpaceNS::MovemeshHelper(god_of_dof,
                                                                                                    extended_unknown
                                                                                                    #ifndef NDEBUG
                                                                                                    , felt_space_processor_wise_dof_list
                                                                                                    , felt_space_ghosted_dof_list
                                                                                                    #endif // NDEBUG
                                                                                                    ));
            
            auto check = movemesh_helper_data.emplace(std::move(helper_per_numbering_subset));
            static_cast<void>(check);
            assert(check.second && "There should be only one entry per numbering subset.");
            
        } // for (const auto& numbering_subset_ptr : numbering_subset_list)
    }

    
    const Internal::FEltSpaceNS::MovemeshHelper& FEltSpace::GetMovemeshHelper(const GlobalVector& vector) const noexcept
    {
        const auto& movemesh_helper_data = GetMovemeshHelperStorage();
        
        const auto& numbering_subset = vector.GetNumberingSubset();
        const auto it = movemesh_helper_data.find(numbering_subset.GetUniqueId());
        assert(it != movemesh_helper_data.cend());
        
       return it->second;
    }
    
    
    
    bool FEltSpace::DoCoverNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        decltype(auto) list = GetNumberingSubsetList();
        
        for (const auto& item : list)
        {
            assert(!(!item));
            if (*item == numbering_subset)
                return true;
        }
        
        return false;
    }

    
    
    unsigned int FEltSpace::ComputeMaxOrderFElt() const
    {
        decltype(auto) storage = GetLocalFEltSpacePerRefLocalFEltSpace();
        
        unsigned int ret = 0u;
        
        for (const auto& item : storage)
        {
            const auto& ref_local_felt_space_ptr = item.first;
            assert(!(!ref_local_felt_space_ptr));
            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;
            
            decltype(auto) ref_felt_list = ref_local_felt_space.GetRefFEltList();
            
            for (const auto& ref_felt : ref_felt_list)
            {
                assert(!(!ref_felt));
                
                decltype(auto) basic_ref_felt = ref_felt->GetBasicRefFElt();
                
                const auto order = basic_ref_felt.GetOrder();
                
                if (order > ret)
                    ret = order;
            }
            
        }
        
        return ret;
    }
    
    
    bool FEltSpace::IsEmpty() const noexcept
    {
        // I do not use the accessor here as it also checkts the GodOfDof is fully built... and I want to use this
        // method during GodOfDof initialization.
        assert(!(!felt_storage_));
        return felt_storage_->IsEmpty();
    }
    

    

} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
