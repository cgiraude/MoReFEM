/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/TimeKeep/TimeKeep.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CreateNodeListHelper.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/DofComputations.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Connectivity.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/ReduceToProcessorWise.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
        
        
        //! Print informations about the partition that has been performed.
        void PrintInformations(const FEltSpace::vector_unique_ptr& felt_space_list,
                               const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                               const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                               const unsigned int Nprogram_wise_dof,
                               const unsigned int Nprocessor_wise_dof,
                               const Wrappers::Mpi& mpi);
        
        // For each finite element space, determine the full list of dofs. This list will be used to reconstitute
        // later the list of processor- and ghost-wise dofs in each finite element space.
        // It is intentional here that indexes are used rather than shared pointers: reduction to processor-wise
        // rely on the automatic deletion of unused shared pointers and storing more here would harm the process.
        std::map<unsigned int, std::vector<unsigned int>>
            ComputeDofIndexListPerFEltSpace(const FEltSpace::vector_unique_ptr& felt_space_list);
        
        
        // Very high value for penalization.
        constexpr const double very_high_value = 1.e30;
        
        
    } // namespace anonymous
    
    
    GodOfDof::GodOfDof(const Wrappers::Mpi& mpi,
                       Mesh& mesh)
    : Crtp::CrtpMpi<GodOfDof>(mpi),
    unique_id_parent(mesh.GetUniqueId()),
    mesh_(mesh)
    { }

    
    const std::string& GodOfDof::ClassName()
    {
        static std::string ret("GodOfDof");
        return ret;
    }
    
    
    void GodOfDof::Init1(const FilesystemNS::Directory& output_directory,
                         FEltSpace::vector_unique_ptr&& a_felt_space_list,
                         std::map<unsigned int, std::vector<unsigned int>>& dof_list_per_felt_space,
                         const Internal::Parallelism* parallelism)
    {
        static_cast<void>(parallelism); // temporary!
        assert(!HasInitBeenCalled() && "Must be called only once per GodOfDof!");
        
        #ifndef NDEBUG
        has_init_been_called_ = true;
        
        assert(std::none_of(a_felt_space_list.cbegin(),
                            a_felt_space_list.cend(),
                            Utilities::IsNullptr<FEltSpace::unique_ptr>));
        #endif // NDEBUG
        
        felt_space_list_ = std::move(a_felt_space_list);
        const auto& felt_space_list = GetFEltSpaceList();
        
        decltype(auto) numbering_subset_list = ComputeNumberingSubsetList();

        output_directory_storage_ =
            std::make_unique<Internal::GodOfDofNS::OutputDirectoryStorage>(output_directory,
                                                                           numbering_subset_list);

        output_directory_wildcard_storage_ =
            std::make_unique<Internal::GodOfDofNS::OutputDirectoryStorage>(output_directory,
                                                                           numbering_subset_list,
                                                                           Internal::GodOfDofNS::wildcard_for_rank::yes);

        const auto& mpi = GetMpi();
        const auto rank = mpi.GetRank<unsigned int>();

        // Create the nodes for all finite element spaces.
        CreateNodeList();
                
        // For each finite element space, determine the full list of dofs. This list will be used to reconstitute
        // later the list of processor- and ghost-wise dofs in each finite element space.
        // It is intentional here that indexes are used rather than shared pointers: reduction to processor-wise
        // rely on the automatic deletion of unused shared pointers and storing more here would harm the process.
        dof_list_per_felt_space = ComputeDofIndexListPerFEltSpace(felt_space_list);
        
        auto& node_bearer_list = GetNonCstProcessorWiseNodeBearerList();
        
        // Compute the required elements to perform the partition. Parmetis call for instance is done there.
        // Node bearer list is still the same length in output (reduction not yet done) but its elements are sort
        // differently: those to be on first processor comes first, then those on second one, and so on...
        // Program-wise numbering is also applied: each of them gets as program-wise index its position in the new
        // vector.
        if (mpi.Nprocessor<int>() > 1)
            Internal::FEltSpaceNS::PreparePartition(mpi,
                                                    GetFEltSpaceList(),
                                                    node_bearer_list);
        else
        {
            for (auto& node_bearer_ptr : node_bearer_list)
            {
                assert(!(!node_bearer_ptr));
                node_bearer_ptr->SetProcessor(0u);
            }
        }
        
        // Give each dof its program-wise index for each numbering subset.
        {
            for (const auto& numbering_subset_ptr : numbering_subset_list)
            {
                unsigned int current_index = 0u;
                
                Internal::FEltSpaceNS
                ::ComputeDofIndexesForNumberingSubset<MpiScale::program_wise>(node_bearer_list,
                                                                              CurrentDofNumberingScheme(),
                                                                              numbering_subset_ptr,
                                                                              current_index);
            }
        }
        
        // Compute the number of program- and processor- wise dofs.
        Ndof_holder_ =
            std::make_unique<Internal::FEltSpaceNS::NdofHolder>(node_bearer_list, numbering_subset_list, rank);
    }
    
    
    void GodOfDof::Init3(DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                         const std::map<unsigned int, std::vector<unsigned int>>& dof_list_per_felt_space,
                         const Internal::Parallelism* parallelism)
    {
        #ifndef NDEBUG
        do_consider_proc_wise_local_2_global_ = do_consider_proc_wise_local_2_global;
        #endif // NDEBUG
        
        const auto& mpi = GetMpi();
        const auto rank = mpi.GetRank<unsigned int>();

        // Reduce the list of node bearers to the ones present on processor.
        // Pattern computation will correctly address those not in the list as it will query finite elements to find
        // the adequate columns to fill with non-zero.
        Internal::FEltSpaceNS::ReduceNodeBearerList(rank, GetNonCstProcessorWiseNodeBearerList());

        // Compute the pattern of the global matrix.
        const auto& felt_space_list = GetFEltSpaceList();
        const auto& node_bearer_list = GetProcessorWiseNodeBearerList();
        const auto& numbering_subset_list = GetNumberingSubsetList();
        
        matrix_pattern_per_numbering_subset_ =
            Internal::FEltSpaceNS::ComputeMatrixPattern::Perform(rank,
                                                                 felt_space_list,
                                                                 node_bearer_list,
                                                                 numbering_subset_list,
                                                                 GetNdofHolder());
             
        // - Reduce the list of finite elements in each finite element space to the processor-wise ones only.
        // - Compute the ghost node bearers for each finite element space.
        // - Compute the reduced mesh, limited to processor-wise geometric elements.
        // The only quantity not yet reduced is the dofs involved in the boundary conditions.
        if (mpi.Nprocessor<int>() > 1)
            ReduceToProcessorWise();

        
        
        // Assign correctly the processor-wise dof indexes.
        ComputeProcessorWiseAndGhostDofIndex();
        
        for (const auto& felt_space_ptr : felt_space_list)
        {
            auto& felt_space = *felt_space_ptr;
            
            const auto unique_id = felt_space.GetUniqueId();
            
            auto it = dof_list_per_felt_space.find(unique_id);
            assert(it != dof_list_per_felt_space.cend());
            
            const auto& dof_unique_id_list = it->second;
            
            felt_space.ComputeDofList(dof_unique_id_list);            
        }
        
        // Finish the reduction to processor-wise: remove dof in boundary conditions that are not processor-wise.
        if (mpi.Nprocessor<int>() > 1)
        {
            // Reduce boundary conditions to processor-wise and ghost ones.
            auto& bc_list = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();
            
            for (auto& bc_ptr : bc_list)
            {
                assert(!(!bc_ptr));
                auto& bc = *bc_ptr;
                
                // God of dof should only reduce its own boundary conditions.
                if (bc.GetDomain().GetMeshIdentifier() == GetUniqueId())
                    bc.ShrinkToProcessorWise(*this);
            }
        }
        
        PrintInformations(felt_space_list,
                          node_bearer_list,
                          GetGhostNodeBearerList(),
                          NprogramWiseDof(),
                          NprocessorWiseDof(),
                          mpi);
        
        // Init the local2global for each local finite element space in each finite element space.
        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            felt_space_ptr->InitLocal2Global(do_consider_proc_wise_local_2_global);
        }
        
        // Prepare if relevant the data to compute movemeshes in finite element spaces.
        const auto mesh_dimension = GetMesh().GetDimension();
        
        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            auto& felt_space = *felt_space_ptr;
            
            // It makes little sense to move only contours of the mesh!
            if (mesh_dimension != felt_space.GetDimension())
                continue;
            
            felt_space.SetMovemeshData();
        }
        
        SetBoundaryCondition();
        
        // Prepare the output directories.
        PrepareOutput(parallelism);

        #ifndef NDEBUG
        if (mpi.Nprocessor<int>() == 1)
            assert(NprocessorWiseDof() == NprogramWiseDof());
        else
        {
            assert(mpi.AllReduce(NprocessorWiseDof(), Wrappers::MpiNS::Op::Sum) == NprogramWiseDof());
            //            assert(mpi.AllReduce(NnodeBearer(), Wrappers::MpiNS::Op::Sum) == Nprogram_wise_node_bearer);
        }
        
        #endif // NDEBUG
    }

    
    void GodOfDof::CreateNodeList()
    {
        const auto& boundary_condition_list = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();
        
        auto& processor_wise_node_bearer_list = GetNonCstProcessorWiseNodeBearerList();
        
        Internal::FEltSpaceNS::CreateNodeListHelper node_helper(processor_wise_node_bearer_list);
        
        #ifdef MOREFEM_DEBUG_FILES
        const int rank = GetMpi().Rank<int>();
        
        std::ofstream output_file;
        
        if (rank == 0)
            File::Create(output_file, "/Volumes/Data/sebastien/MoReFEM/Results/Hyperelasticity/FElt/node_per_fe.hhdata");
        #endif // MOREFEM_DEBUG_FILES
        
        const auto& felt_space_list = GetFEltSpaceList();
        
        for (const auto& felt_space_ptr : felt_space_list)
        {
            assert(!(!felt_space_ptr));
            
            const auto& felt_storage = felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace();
            
            for (const auto& pair : felt_storage)
            {
                const auto& ref_felt_space_ptr = pair.first;
                assert(!(!ref_felt_space_ptr));
                const auto& ref_felt_space = *ref_felt_space_ptr;
                
                const auto& local_felt_space_list = pair.second;
                
                for (const auto& local_felt_space_pair : local_felt_space_list)
                {
                    const auto& local_felt_space_ptr = local_felt_space_pair.second;
                    assert(!(!local_felt_space_ptr));
                    auto& local_felt_space = *local_felt_space_ptr;
                    
                    node_helper.AddNodes(ref_felt_space, local_felt_space);
                }
            }
        }
        
        // Constitute here the list of indexes concerned by BC.
        // \todo #619 This is rather awkward: it would be better to set completely the boundary condition
        // after the reduction to processor-wise, at the beginning of the method SetBoundaryCondition().
        // However it means severing the dependancy here upon node_helper; actually only two data attributes are
        // used in the method ComputeNodeBearerListOnBoundary() and it should be relatively easy to avoid this.
        const auto& mesh = GetMesh();
        
        
        for (auto& boundary_condition_ptr : boundary_condition_list)
        {
            assert(!(!boundary_condition_ptr));
            auto& boundary_condition = *boundary_condition_ptr;
            auto&& node_bearer_on_bc = node_helper.ComputeNodeBearerListOnBoundary(mesh, boundary_condition);
            
            boundary_condition.ComputeDofList(std::move(node_bearer_on_bc));
        }
    }
    
    
    void GodOfDof::ComputeProcessorWiseAndGhostDofIndex()
    {
        auto& node_bearer_list = GetNonCstProcessorWiseNodeBearerList();
        auto& ghost_node_bearer_list = GetNonCstGhostNodeBearerList();
        
        // Sort both node_bearer list and ghost node_bearer list per increasing index.
        std::sort(node_bearer_list.begin(),
                  node_bearer_list.end(),
                  Utilities::PointerComparison::Less<NodeBearer::shared_ptr>());
        
        std::sort(ghost_node_bearer_list.begin(),
                  ghost_node_bearer_list.end(),
                  Utilities::PointerComparison::Less<NodeBearer::shared_ptr>());
        
        // Attribute incrementally processor-wise index to the dofs that belongs to node_bearer on the processor.
        {
            unsigned int current_processor_wise_or_ghost_dof_index = 0u;
            
            auto& processor_wise_dof_list = GetNonCstProcessorWiseDofList();
            
            {
               
                
                Internal::FEltSpaceNS
                ::ComputeProcessorWiseDofIndexes(node_bearer_list,
                                                 CurrentDofNumberingScheme(),
                                                 current_processor_wise_or_ghost_dof_index,
                                                 processor_wise_dof_list);
                
                assert(NprocessorWiseDof() == current_processor_wise_or_ghost_dof_index);
            }
            
            // Ghost dofs are created similarly; the same index keeps being used (so we get first all processor-wise dofs
            // and then all ghost in a Petsc parallel vector).
            
            auto& ghosted_dof_list = GetNonCstGhostedDofList();
            
            Internal::FEltSpaceNS
            ::ComputeProcessorWiseDofIndexes(ghost_node_bearer_list,
                                             CurrentDofNumberingScheme(),
                                             current_processor_wise_or_ghost_dof_index,
                                             ghosted_dof_list);
            
            // Less<Dof> sort the Dofs per program-wise index, but per construct program-wise and processor-wise indexes
            // get the same relative ordering.
            assert(std::is_sorted(processor_wise_dof_list.cbegin(), processor_wise_dof_list.cend(),
                                  Utilities::PointerComparison::Less<Dof::shared_ptr>()));
            
            assert(std::is_sorted(ghosted_dof_list.cbegin(), ghosted_dof_list.cend(),
                                  Utilities::PointerComparison::Less<Dof::shared_ptr>()));
        }
        
        
        // Now assign the processor-wise indexes within each numbering subset.
        const auto& numbering_subset_list = GetNumberingSubsetList();
        
        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            
            unsigned int current_processor_wise_or_ghost_dof_index = 0u;

            Internal::FEltSpaceNS
            ::ComputeDofIndexesForNumberingSubset<MpiScale::processor_wise>(node_bearer_list,
                                                                            CurrentDofNumberingScheme(),
                                                                            numbering_subset_ptr,
                                                                            current_processor_wise_or_ghost_dof_index);
            
            // Ghost dofs are created similarly; the same index keeps being used (so we get first all processor-wise dofs
            // and then all ghost in a Petsc parallel vector).
            Internal::FEltSpaceNS
            ::ComputeDofIndexesForNumberingSubset<MpiScale::processor_wise>(ghost_node_bearer_list,
                                                                            CurrentDofNumberingScheme(),
                                                                            numbering_subset_ptr,
                                                                            current_processor_wise_or_ghost_dof_index);
        }
    }

    
    void GodOfDof::ReduceToProcessorWise()
    {
        const auto& mpi = GetMpi();
        
        assert(GetMpi().Nprocessor<int>() > 1 && "Should not be called otherwise!");
                
        decltype(auto) processor_wise_node_bearer_list = GetProcessorWiseNodeBearerList();
        
        Internal::FEltSpaceNS::ReduceToProcessorWise::Perform(mpi,
                                                              GetFEltSpaceList(),
                                                              processor_wise_node_bearer_list,
                                                              GetNonCstGhostNodeBearerList(),
                                                              GetNonCstMesh());
    }
    
    
    
    void GodOfDof::OndomaticLikePrint(const NumberingSubset& numbering_subset,
                                      const std::string& output_directory) const
    {
        static_cast<void>(numbering_subset);
        static_cast<void>(output_directory);
//        std::string filename;
//        
//        const auto& mpi = GetMpi();
//
//        {
//            std::ostringstream oconv;
//            oconv << output_directory << "/Nodes_and_dofs_"
//            << mpi.GetRank<unsigned int>() << ".hhdata";
//
//            filename = oconv.str();
//        }
//
//        std::cout << "The nodes and dofs are written in file " << filename << '.' << std::endl;
//
//        std::ofstream out(filename);
//
////        unsigned int elt_index = 0u;
////        
////        const auto& felt_space_list = GetFEltSpaceList();
////        
////        for (const auto& felt_space_ptr : felt_space_list)
////        {
////            assert(!(!felt_space_ptr));
////            
////            const auto& felt_storage = felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace();
////
////            for (const auto& pair : felt_storage)
////            {
////                const LocalFEltSpace::vector_shared_ptr& local_felt_space_list = pair.second;
////                
////                for (const auto& local_felt_space_ptr : local_felt_space_list)
////                {
////                    std::vector<unsigned int> index_list;
////                    
////                    assert(!(!local_felt_space_ptr));
////                    
////                    const auto& felt_list = local_felt_space_ptr->GetFEltList();
////                    
////                    for (const auto& felt_ptr : felt_list)
////                    {
////                        assert(!(!felt_ptr));
////                        const auto& local_2_global = felt_ptr->GetLocal2Global<MpiScale::program_wise>();
////                        out << "Element " << elt_index++ << ": ";
////                        
////                        Utilities::PrintContainer<>::Do(local_2_global, out, ' ', ' ', '\n');
////                    }
////                }
////            }
////        }
//        
//        const auto& node_bearer_list = GetNodeBearerList();
//        
//        
//        for (const auto& node_bearer_ptr : node_bearer_list)
//        {
//            assert(!(!node_bearer_ptr));
//            const auto& node_bearer = *node_bearer_ptr;
//
//            std::ostringstream oconv;
//            oconv << node_bearer.GetNature();
//
//            const std::string nature(oconv.str());
//
//            const auto& node_storage = node_bearer.GetNodeStorage();
//            const auto& coords = node_bearer.GetInterface().GetVertexCoordsList();
//
//            out << std::endl;
//            out << "=============================" << std::endl;
//            out << "Node bearer " << node_bearer.GetIndex();
//            out << " (" << nature << " - " << node_bearer.GetInterface().GetIndex() << ')' << std::endl;
//            out << "=============================" << std::endl;
//
//            for (const auto& unknown_pair : node_storage)
//            {
//                const auto& node_list_per_shape_fct = unknown_pair.second;
//
//                for (const auto& shape_fct_pair : node_list_per_shape_fct)
//                {
//                    const auto& node_list = shape_fct_pair.second;
//
//                    unsigned int dof_in_node_counter = 0;
//
//                    for (const auto& node_ptr : node_list)
//                    {
//                        assert(!(!node_ptr));
//                        const std::string& shape_function = node_ptr->GetShapeFunctionLabel();
//
//                        const auto& unknown = node_ptr->GetUnknown();
//
//                        const std::string unknown_name(unknown.GetName());
//
//                        const auto& dof_list = node_ptr->GetDofList();
//
//                        const std::size_t Ndof_in_node = dof_list.size();
//
//                        for (std::size_t i = 0ul; i < Ndof_in_node; ++i)
//                        {
//                            const auto& dof_ptr = dof_list[i];
//                            assert(!(!dof_ptr));
//
//                            out << "Dof " << dof_ptr->GetProgramWiseIndex(numbering_subset) << ": "
//                            << "variable/comp = " << unknown_name << " " << i
//                            << "; shape_function = " << shape_function
//                            <<  "; position_on_interface_index = " << dof_in_node_counter++ << "; ";
//                            
//                            out << "coords on vertex = ";
//                            
//                            std::vector<unsigned int> coords_index;
//                            
//                            for (auto coord: coords)
//                            {
//                                coords_index.push_back(coord->GetIndex());
//                            }
//                            
//                            Utilities::PrintContainer<>::Do(coords_index, out);
//                        }
//                    }
//                }
//            }
//        }
    }

    
    
    void GodOfDof::PrintDofInformation(const NumberingSubset& numbering_subset,
                                       std::ostream& out) const
    {
        const auto& node_bearer_list = GetProcessorWiseNodeBearerList();
        
        out << "Ndof (processor_wise) = " << NprocessorWiseDof(numbering_subset) << std::endl;
        out << "# First column: program-wise index." << std::endl;
        out << "# Second column: processor-wise index." << std::endl;
        out << "# Third column: the interface upon which the dof is located. Look at the interface file in same "
        "directory to relate it to the initial mesh." << std::endl;
        out << "# Fourth column: unknown and component involved." << std::endl;
        out << "# Fifth column: shape function label." << std::endl;
        
        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            const auto& node_bearer = *node_bearer_ptr;
            
            std::ostringstream oconv;
            oconv << node_bearer.GetNature() << ' ' << node_bearer.GetInterface().GetIndex();
            
            const std::string nature(oconv.str());
            
            const auto& node_list = node_bearer.GetNodeList();
            
            for (auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;
                
                if (!node.DoBelongToNumberingSubset(numbering_subset))
                    continue;
                
                const auto& unknown = node.GetUnknown();
                
                const std::string unknown_name(unknown.GetName());
                
                const auto& dof_list = node.GetDofList();
                
                const std::size_t Ndof_in_node = dof_list.size();
                
                const auto& shape_function_label = node.GetShapeFunctionLabel();
                
                for (std::size_t i = 0ul; i < Ndof_in_node; ++i)
                {
                    const auto& dof_ptr = dof_list[i];
                    assert(!(!dof_ptr));
                    
                    out << dof_ptr->GetProgramWiseIndex(numbering_subset) << ';';
                    out << dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset) << ';';
                    out << nature << ';';
                    out << unknown_name << ' ' << i << ';';
                    out << shape_function_label;
                    out << std::endl;
                }
            }
        }
    }
    
    
    void PrintNodeBearerList(const NodeBearer::vector_shared_ptr& node_bearer_list,
                             const unsigned int rank,
                             std::ostream& out)
    {
        std::vector<unsigned int> indexes;
        indexes.reserve(node_bearer_list.size());
        
        std::vector<unsigned int> on_proc;
        on_proc.reserve(node_bearer_list.size());
        
        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            indexes.push_back(node_bearer_ptr->GetIndex());
            on_proc.push_back(node_bearer_ptr->GetProcessor());
        }
        
        std::ostringstream oconv;
        oconv << "Node bearers on processor " << rank << " -> [";
        
        Utilities::PrintContainer<>::Do(indexes, out, ", ", oconv.str());
    }
    

    
    
    void GodOfDof::SetBoundaryCondition()
    {
        const auto& boundary_condition_list = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();
       
        const auto& numbering_subset_list = GetNumberingSubsetList();
        
        const auto& mpi = GetMpi();
        
        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;

            Dof::vector_shared_ptr relevant_dof_for_any_bc_list;
            
            for (auto& boundary_condition_ptr : boundary_condition_list)
            {
                assert(!(!boundary_condition_ptr));
                auto& boundary_condition = *boundary_condition_ptr;
                
                const auto& dof_list = boundary_condition.GetDofList();
                
                Dof::vector_shared_ptr relevant_dof_for_current_bc_list;
                relevant_dof_for_current_bc_list.reserve(dof_list.size());
                
                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    const auto& dof = *dof_ptr;
                    
                    // Keep only the dofs that are encompassed by current numbering subset.
                    if (!dof.IsInNumberingSubset(numbering_subset))
                        continue;

                    // Consider only processor-wise values.
                    if (IsGhosted(dof))
                        continue;

                    relevant_dof_for_current_bc_list.push_back(dof_ptr);
                    relevant_dof_for_any_bc_list.push_back(dof_ptr);
                }
                
                // Fill appropriately the storage of dofs related to the pair boundary condition/numbering subset.
                const bool is_not_empty = !relevant_dof_for_current_bc_list.empty();
                
                const bool is_relevant_for_any_processor = mpi.AllReduce(is_not_empty, Wrappers::MpiNS::Op::LogicalOr);
                
               if (is_not_empty)
                {
                    assert(is_relevant_for_any_processor && "If current processor is relevant reduction should "
                           "automatically be as well...");
                    boundary_condition.SetDofListForNumberingSubset(numbering_subset,
                                                                    std::move(relevant_dof_for_current_bc_list));
                }
                else if (is_relevant_for_any_processor)
                    boundary_condition.ConsiderNumberingSubset(numbering_subset);
            }
            
            const auto Nbc_dof_in_numbering_subset = relevant_dof_for_any_bc_list.size();
            
            Utilities::EliminateDuplicate(relevant_dof_for_any_bc_list);
            
            if (relevant_dof_for_any_bc_list.size() != Nbc_dof_in_numbering_subset)
            {
                std::cout << "[WARNING] At least two different boundary conditions deal with the same dof in the same "
                "numbering subset (namely " << numbering_subset.GetUniqueId() << ")! Both boundary conditions should "
                "therefore be set with 'may_overlap = yes' in the input data file." << std::endl;
                
                for (auto& boundary_condition_ptr : boundary_condition_list)
                    boundary_condition_ptr->SetBruteForceShrinking();
            }
        }
    }
    
    
    auto GodOfDof::GetIteratorFEltSpace(unsigned int felt_space_index) const
    {
        const auto& felt_space_list = GetFEltSpaceList();
        
        return std::find_if(felt_space_list.cbegin(),
                            felt_space_list.cend(),
                            [felt_space_index](const auto& felt_space_ptr)
                            {
                                assert(!(!felt_space_ptr));
                                return felt_space_ptr->GetUniqueId() == felt_space_index;
                            });
    }
    
    
    bool GodOfDof::IsFEltSpace(unsigned int felt_space_index) const
    {
        return GetIteratorFEltSpace(felt_space_index) != GetFEltSpaceList().cend();
    }
    
    
    const FEltSpace& GodOfDof::GetFEltSpace(unsigned int felt_space_index) const
    {
        auto it = GetIteratorFEltSpace(felt_space_index);
        
        assert(it != GetFEltSpaceList().cend() && "If not something went awry in GodOfDof initialization!");
        
        return *(*it);
    }
    
    
    const NumberingSubset::vector_const_shared_ptr& GodOfDof::ComputeNumberingSubsetList()
    {
        const auto& felt_space_list = GetFEltSpaceList();
        
        assert(numbering_subset_list_.empty() && "Should be initialized only once!");
        
        for (const auto& felt_space_ptr : felt_space_list)
        {
            decltype(auto) numbering_subset_list_in_felt_space = felt_space_ptr->GetNumberingSubsetList();
            
            std::move(numbering_subset_list_in_felt_space.begin(),
                      numbering_subset_list_in_felt_space.end(),
                      std::back_inserter(numbering_subset_list_));
        }
        
        Utilities::EliminateDuplicate(numbering_subset_list_,
                                      Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                      Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());
                                      
        assert(!numbering_subset_list_.empty() && "There must be at least one...");

        return numbering_subset_list_;
    }

    
    const Wrappers::Petsc::MatrixPattern& GodOfDof::GetMatrixPattern(const NumberingSubset& row_numbering_subset,
                                                                     const NumberingSubset& column_numbering_subset) const
    {
        auto it = std::find_if(matrix_pattern_per_numbering_subset_.cbegin(),
                               matrix_pattern_per_numbering_subset_.cend(),
                               [&row_numbering_subset, &column_numbering_subset](const auto& item)
                               {
                                   assert(!(!item));
                                   return item->GetRowNumberingSubset() == row_numbering_subset
                                   && item->GetColumnNumberingSubset() == column_numbering_subset;
                               });
        
        assert(it != matrix_pattern_per_numbering_subset_.cend());
        assert(!(!*it));
        return (*it)->GetPattern();
    }

    

   
    const NumberingSubset::const_shared_ptr& GodOfDof::GetNumberingSubsetPtr(unsigned int unique_id) const
    {
        const auto& numbering_subset_list = GetNumberingSubsetList();
        
        auto it = std::find_if(numbering_subset_list.cbegin(),
                               numbering_subset_list.cend(),
                               [unique_id](const auto& numbering_subset_ptr)
                               {
                                   assert(!(!numbering_subset_ptr));
                                   return numbering_subset_ptr->GetUniqueId() == unique_id;
                               });
        
        // \todo #608 Should be an exception (currently assert is fine but for some model in which subsets
        // may be monolithic or not it's not that sure...)
        assert(Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance(__FILE__, __LINE__).DoExist(unique_id)
               && "The requested numbering subset is not even defined in the InputData tuple!");
        
        assert(it != numbering_subset_list.cend()
               && "Numbering subset was not found in the GodOfDof but do exists in the InputData tuple; "
               "the most likely explanation is that no FEltSpace in the GodOfDof is actually using it.");
               
        assert(!(!*it));
        
        return *it;
    }
    
    
    
    void GodOfDof::PrepareOutput(const Internal::Parallelism* parallelism)
    {
        decltype(auto) numbering_subset_list = GetNumberingSubsetList();
        
        decltype(auto) mpi = GetMpi();
        
        // First create all the relevant folders; make sure this is done before any processor attempts to create a file.
        {
            for (const auto& numbering_subset_ptr : numbering_subset_list)
            {
                assert(!(!numbering_subset_ptr));
                const auto& numbering_subset = *numbering_subset_ptr;
                
                // Create the subfolder to store numbering subset data.
                // Root processor is in charge of it; other processors must wait until it's done.
                auto&& subfolder = GetOutputDirectoryForNumberingSubset(numbering_subset);
                
                if (!Advanced::FilesystemNS::DirectoryNS::DoExist(subfolder)) // \todo #497
                    Advanced::FilesystemNS::DirectoryNS::Create(subfolder, __FILE__, __LINE__);
            }
        }
        
        mpi.Barrier();
        
        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;
            
            // Create the subfolder to store numbering subset data.
            // Root processor is in charge of it; other processors must wait until it's done.
            auto&& subfolder = GetOutputDirectoryForNumberingSubset(numbering_subset);
            
            // Report the dof informations.
            std::ostringstream oconv;
            oconv << subfolder << "/dof_information.hhdata";
            std::ofstream out;
            FilesystemNS::File::Create(out, oconv.str(), __FILE__, __LINE__);
            PrintDofInformation(numbering_subset, out);
        }

        // Write if required the parallelism strategy.
        if (parallelism != nullptr)
        {
            const auto parallelism_strategy = parallelism->GetParallelismStrategy();

            decltype(auto) mesh = GetMesh();

            switch(parallelism_strategy)
            {
                case Advanced::parallelism_strategy::parallel:
                case Advanced::parallelism_strategy::precompute:
                {
                    FilesystemNS::Directory mesh_subdir(parallelism->GetDirectory(),
                                                        "Mesh_" + std::to_string(mesh.GetUniqueId()),
                                                        __FILE__, __LINE__);

                    Advanced::MeshNS::WritePrepartitionedData partition_data_facility(mesh,
                                                                                      mesh_subdir,
                                                                                      mesh.GetInitialFormat());

                    break;
                }
                case Advanced::parallelism_strategy::none:
                case Advanced::parallelism_strategy::run_from_preprocessed:
                case Advanced::parallelism_strategy::parallel_no_write:
                    break;
            }
        }


    }
    
    
    bool GodOfDof::IsGhosted(const Dof& dof) const
    {
        const auto& ghosted_dof_list = GetGhostedDofList();
        const auto end = ghosted_dof_list.cend();
        const auto dof_id = dof.GetInternalProcessorWiseOrGhostIndex();
        
        auto it = std::find_if(ghosted_dof_list.cbegin(),
                               end,
                               [&dof_id](const auto& dof_ptr)
                               {
                                   assert(!(!dof_ptr));
                                   return dof_ptr->GetInternalProcessorWiseOrGhostIndex() == dof_id;
                               });
        return it != end;
    }
    
    


    
    namespace // anonymous
    {
        
        void PrintInformations(const FEltSpace::vector_unique_ptr& felt_space_list,
                               const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                               const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                               const unsigned int Nprogram_wise_dof,
                               const unsigned int Nprocessor_wise_dof,
                               const Wrappers::Mpi& mpi)
        {
            unsigned int Nfelt = 0;
            
            {
                for (const auto& felt_space_ptr : felt_space_list)
                {
                    const auto& felt_list_per_type = felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace();
                
                    for (const auto& pair : felt_list_per_type)
                    {
                        const auto& local_felt_space_list = pair.second;
                        
                        Nfelt += static_cast<unsigned int>(local_felt_space_list.size());
                    }
                }
            }
            
            const unsigned int rank = mpi.GetRank<unsigned int>();
            
            if (rank == 0)
                std::cout << "Total number of dofs = " << Nprogram_wise_dof << std::endl;
            
            std::cout << "On processor " << rank << " -> "
            << Nfelt << " finite elements, "
            << processor_wise_node_bearer_list.size() << " node_bearers, "
            << Nprocessor_wise_dof << " dofs, "
            << ghost_node_bearer_list.size() << " ghosted node_bearers." << std::endl;
        }
        

        std::map<unsigned int, std::vector<unsigned int>>
            ComputeDofIndexListPerFEltSpace(const FEltSpace::vector_unique_ptr& felt_space_list)
        {
            std::map<unsigned int, std::vector<unsigned int>> ret;
            
            for (const auto& felt_space_ptr : felt_space_list)
            {
                const auto& felt_space = *felt_space_ptr;
                const auto key = felt_space.GetUniqueId();
                
                std::vector<unsigned int> dof_id_list;
                
                const auto& local_felt_space_storage = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace();
                
                decltype(auto) extended_unknown_list = felt_space.GetExtendedUnknownList();
                
                for (const auto& pair : local_felt_space_storage)
                {
                    const auto& local_felt_space_list = pair.second;
                    
                    for (const auto& local_felt_space_pair : local_felt_space_list)
                    {
                        const auto& local_felt_space_ptr = local_felt_space_pair.second;
                        assert((!(!local_felt_space_ptr)));
                        decltype(auto) local_felt_space = *local_felt_space_ptr;
                      
                        for (const auto& extended_unknown_ptr : extended_unknown_list)
                        {
                            assert(!(!extended_unknown_ptr));
                            decltype(auto) extended_unknown = *extended_unknown_ptr;
                            decltype(auto) unknown = extended_unknown.GetUnknown();
                            decltype(auto) shape_function_label = extended_unknown.GetShapeFunctionLabel();
                            
                            const auto& node_bearer_list = local_felt_space.GetNodeBearerList();
                            
                            for (const auto& node_bearer_ptr : node_bearer_list)
                            {
                                assert(!(!node_bearer_ptr));
                                decltype(auto) node_bearer = *node_bearer_ptr;
                                const auto& node_list = node_bearer.GetNodeList(unknown,
                                                                                shape_function_label);
                                
                                for (const auto& node_ptr : node_list)
                                {
                                    assert(!(!node_ptr));
                                    const auto& dof_list = node_ptr->GetDofList();

                                    for (const auto& dof_ptr : dof_list)
                                    {
                                        assert(!(!dof_ptr));
                                        dof_id_list.emplace_back(dof_ptr->GetUniqueId());
                                    }
                                }
                            }
                        }
                    }
                }
                
                Utilities::EliminateDuplicate(dof_id_list);

                auto check = ret.insert({key, std::move(dof_id_list)});
                static_cast<void>(check);
                assert(check.second && "A given finite element space should only be added once!");
            
            } // for over felt space list.
            
            return ret;
        }

        
    } // namespace anonymous
    
    

    void GodOfDof::ClearTemporaryData() const noexcept
    {
        const auto& felt_list = GetFEltSpaceList();
        
        for (const auto& felt_ptr : felt_list)
        {
            assert(!(!felt_ptr));
            felt_ptr->ClearTemporaryData();
        }
    }
    
    
    Dof::vector_shared_ptr GodOfDof::GetBoundaryConditionDofList(const NumberingSubset& numbering_subset) const
    {
        const auto& boundary_condition_list = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__).GetList();
        
        Dof::vector_shared_ptr ret;
        
        for (const auto& boundary_condition_ptr : boundary_condition_list)
        {
            assert(!(!boundary_condition_ptr));
            const auto& local_dof_storage = boundary_condition_ptr->GetDofStorage(numbering_subset);
            
            const auto& dof_list = local_dof_storage.GetDofList();
            
            std::copy(dof_list.cbegin(),
                      dof_list.cend(),
                      std::back_inserter(ret));
        }
        
        return ret;
    }
    
    
    void GodOfDof::ApplyPseudoElimination(const DirichletBoundaryCondition& dirichlet_bc,
                                          GlobalMatrix& matrix) const
    {
        const auto& row_numbering_subset = matrix.GetRowNumberingSubset();
        
        assert(dirichlet_bc.IsNumberingSubset(row_numbering_subset));
        
        // Note: ZeroRows is collective so each processor must call it, even if there are no rows to zero
        // on that processor.
        const auto& dof_storage = dirichlet_bc.GetDofStorage(row_numbering_subset);
        
        const auto& dof_index_list = dof_storage.GetProgramWiseDofIndexList();
        
        matrix.ZeroRows(dof_index_list, 1., __FILE__, __LINE__);
    }
    
    
    void GodOfDof::ApplyPenalization(const DirichletBoundaryCondition& dirichlet_bc,
                                     GlobalMatrix& matrix) const
    {
        const auto& row_numbering_subset = matrix.GetRowNumberingSubset();
        
        assert(dirichlet_bc.IsNumberingSubset(row_numbering_subset));
        
        const auto& dof_storage = dirichlet_bc.GetDofStorage(row_numbering_subset);
        const auto& dof_index_list = dof_storage.GetProgramWiseDofIndexList();
        
        // \todo SetValues should be used here!
        for (auto program_wise_index : dof_index_list)
            matrix.SetValue(program_wise_index,
                            program_wise_index,
                            very_high_value,
                            INSERT_VALUES,
                            __FILE__, __LINE__);
    }
    
    
    void GodOfDof::ApplyPenalization(const DirichletBoundaryCondition& dirichlet_bc,
                                     GlobalVector& vector) const
    {
        const auto& numbering_subset = vector.GetNumberingSubset();
        
        assert(dirichlet_bc.IsNumberingSubset(numbering_subset));
        
        const auto& dof_storage = dirichlet_bc.GetDofStorage(numbering_subset);
        const auto& dof_index_list = dof_storage.GetProcessorWiseDofIndexList();
        decltype(auto) dof_value_list = dof_storage.GetDofValueList();
        
        assert(dof_index_list.size() == dof_value_list.size());
        const auto Nbc_value = dof_value_list.size();
        
        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector, __FILE__, __LINE__);
        
            #ifndef NDEBUG
            const auto size = content.GetSize(__FILE__, __LINE__);
            #endif // NDEBUG
            
            for (auto i = 0ul; i < Nbc_value; ++i)
            {
                const auto processor_wise_index = dof_index_list[i];
                assert(processor_wise_index < size);
                content[processor_wise_index] = dof_value_list[i] * very_high_value;
            }
        }
        
        vector.UpdateGhosts(__FILE__, __LINE__);
    }

    
    void GodOfDof::ApplyPseudoElimination(const DirichletBoundaryCondition& dirichlet_bc,
                                          GlobalVector& vector) const
    {
        const auto& numbering_subset = vector.GetNumberingSubset();
        
        assert(dirichlet_bc.IsNumberingSubset(numbering_subset));
        const auto& dof_storage = dirichlet_bc.GetDofStorage(numbering_subset);

        const auto& dof_index_list = dof_storage.GetProgramWiseDofIndexList();
        
        // VecSetValues is not collective: no need to call it on a processor without any dof related to the
        // boundary condition.
        if (dof_index_list.empty())
            return;        
        
        const auto& value_list = dof_storage.GetDofValueList();
     
        vector.SetValues(dof_index_list,
                         value_list.data(),
                         INSERT_VALUES,
                         __FILE__, __LINE__);
    }
    
    
    # ifndef NDEBUG
    void AssertMatrixRespectPattern(const GodOfDof& god_of_dof,
                                    const GlobalMatrix& matrix,
                                    const char* invoking_file, int invoking_line)
    {
        decltype(auto) mpi = god_of_dof.GetMpi();
        
        try
        {
            decltype(auto) pattern = god_of_dof.GetMatrixPattern(matrix.GetRowNumberingSubset(),
                                                                 matrix.GetColNumberingSubset());
            
            const auto Nrow = static_cast<unsigned int>(matrix.GetProcessorWiseSize(invoking_file, invoking_line).first);
            
            if (Nrow != static_cast<unsigned int>(pattern.Nrow()))
                throw Exception("Number of rows in pattern and matrix is not the same!",
                                invoking_file, invoking_line);
            
            decltype(auto) iCsr = pattern.GetICsr();
            decltype(auto) jCsr = pattern.GetJCsr();
            
            assert(iCsr.size() == static_cast<std::size_t>(Nrow + 1)
                   && "This would highlight a bug within MatrixPattern class");
            
            auto current_jcsr_index = 0ul;
            
            decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();
            
            std::vector<PetscInt> position_list_in_matrix;
            std::vector<PetscScalar> value_list_in_matrix;
            
            decltype(auto) row_numbering_subset = matrix.GetRowNumberingSubset();
            
            
            Dof::vector_shared_ptr processor_wise_dof_list_in_row_numbering_subset;
            
            
            std::copy_if(processor_wise_dof_list.cbegin(),
                         processor_wise_dof_list.cend(),
                         std::back_inserter(processor_wise_dof_list_in_row_numbering_subset),
                         [&row_numbering_subset](const auto& dof_ptr)
                          {
                              assert(!(!dof_ptr));
                              return dof_ptr->IsInNumberingSubset(row_numbering_subset);
                          });
            
            for (auto row_index = 0u; row_index < Nrow; ++row_index)
            {
                position_list_in_matrix.clear();
                value_list_in_matrix.clear();
                
                const auto it = std::find_if(processor_wise_dof_list_in_row_numbering_subset.cbegin(),
                                             processor_wise_dof_list_in_row_numbering_subset.cend(),
                                             [&row_numbering_subset, row_index](const auto& dof_ptr)
                                             {
                                                 assert(!(!dof_ptr));
                                                 return dof_ptr->GetProcessorWiseOrGhostIndex(row_numbering_subset) == row_index;
                                             });
                assert(it != processor_wise_dof_list.cend());
                assert(!(!(*it)));
                
                const auto program_wise_index = static_cast<PetscInt>((*it)->GetProgramWiseIndex(row_numbering_subset));
                
                matrix.GetRow(program_wise_index, position_list_in_matrix, value_list_in_matrix, invoking_file, invoking_line);
                
                const auto Nvalue_in_pattern_row =
                static_cast<std::size_t>(iCsr[static_cast<std::size_t>(row_index + 1)] - iCsr[static_cast<std::size_t>(row_index)]);
                
                std::vector<PetscInt> position_list_in_pattern;
                
                for (auto j = 0ul; j < Nvalue_in_pattern_row; ++j)
                {
                    assert(current_jcsr_index + j < jCsr.size());
                    position_list_in_pattern.push_back(static_cast<PetscInt>(jCsr[current_jcsr_index + j]));
                }
                
                
                current_jcsr_index += Nvalue_in_pattern_row;
                
                if (!std::includes(position_list_in_pattern.cbegin(),
                                   position_list_in_pattern.cend(),
                                   position_list_in_matrix.cbegin(),
                                   position_list_in_matrix.cend()))
                {
                    PrintNumberingSubset("matrix", matrix);
                    
                    std::ostringstream oconv;
                    oconv << "Position in the actual matrix must match those defined in the pattern! Not the case "
                    "for row " << program_wise_index << " (program-wise numbering) on processor " << mpi.GetRank<int>();
                    
                    Utilities::PrintContainer<>::Do(position_list_in_matrix, oconv, ", ", "\n\t- Position in matrix -> [");
                    Utilities::PrintContainer<>::Do(position_list_in_pattern, oconv, ", ", "\t- Position in pattern -> [");
                    
                    throw Exception(oconv.str(),
                                    invoking_file, invoking_line);
                    
                }
            }
        }
        catch(const Exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
    # endif // NDEBUG
    
    
    std::vector<unsigned int> ComputeProcessorWiseIndexList(const GodOfDof& gof_of_dof,
                                                            const NumberingSubset& numbering_subset,
                                                            const Unknown& unknown)
    {
        std::vector<unsigned int> ret;
        
        decltype(auto) node_bearer_list = gof_of_dof.GetProcessorWiseNodeBearerList();
        
        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));
            decltype(auto) node_list = node_bearer_ptr->GetNodeList();
            
            for (const auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;
                
                if (node.GetUnknown() != unknown)
                    continue;
                
                decltype(auto) dof_list = node.GetDofList();
                
                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    ret.push_back(dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset));
                }
            }
        }
        
        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
