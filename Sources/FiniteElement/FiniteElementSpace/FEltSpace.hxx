/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:45:59 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HXX_




namespace MoReFEM
{


    inline const LocalFEltSpacePerRefLocalFEltSpace& FEltSpace::GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept
    {
        return GetFEltStorage().GetLocalFEltSpacePerRefLocalFEltSpace();
    }


    inline const LocalFEltSpacePerRefLocalFEltSpace&
    FEltSpace::GetLocalFEltSpacePerRefLocalFEltSpace(const Domain& domain) const
    {
        return GetFEltStorage().GetLocalFEltSpacePerRefLocalFEltSpace(domain);
    }


    inline const Dof::vector_shared_ptr& FEltSpace::GetProcessorWiseDofList() const noexcept
    {
        return dof_list_;
    }


    inline const Dof::vector_shared_ptr& FEltSpace::GetGhostedDofList() const noexcept
    {
        return ghost_dof_list_;
    }


    inline unsigned int FEltSpace::GetDimension() const noexcept
    {
        return dimension_;
    }


    inline const Internal::FEltSpaceNS::Storage& FEltSpace::GetFEltStorage() const noexcept
    {
        assert(!(!felt_storage_));
        # ifndef NDEBUG
        AssertGodOfDofInitialized();
        # endif // NDEBUG

        return *felt_storage_;
    }


    inline Internal::FEltSpaceNS::Storage& FEltSpace::GetNonCstFEltStorage() noexcept
    {
        return const_cast<Internal::FEltSpaceNS::Storage&>(GetFEltStorage());
    }


    inline std::shared_ptr<const GodOfDof> FEltSpace::GetGodOfDofFromWeakPtr() const
    {
        assert(!god_of_dof_.expired());
        return god_of_dof_.lock();
    }


    inline const ExtendedUnknown::vector_const_shared_ptr& FEltSpace::GetExtendedUnknownList() const noexcept
    {
        return extended_unknown_list_;
    }


    inline const NumberingSubset& FEltSpace::GetNumberingSubset(const Unknown& unknown) const
    {
        return GetExtendedUnknown(unknown).GetNumberingSubset();
    }


    inline const NumberingSubset::const_shared_ptr& FEltSpace::GetNumberingSubsetPtr(const Unknown& unknown) const
    {
        return GetExtendedUnknown(unknown).GetNumberingSubsetPtr();
    }


    inline const ExtendedUnknown& FEltSpace::GetExtendedUnknown(const Unknown& unknown) const
    {
        return *GetExtendedUnknownPtr(unknown);
    }


    inline const NumberingSubset::vector_const_shared_ptr& FEltSpace::GetNumberingSubsetList() const noexcept
    {
        assert(std::none_of(numbering_subset_list_.cbegin(),
                            numbering_subset_list_.cend(),
                            Utilities::IsNullptr<NumberingSubset::const_shared_ptr>));

        return numbering_subset_list_;
    }


    inline const LocalFEltSpace::per_geom_elt_index&
    FEltSpace::GetLocalFEltSpaceList(const RefGeomElt& ref_geom_elt) const
    {
        const auto& ref_local_felt_space = GetRefLocalFEltSpace(ref_geom_elt);
        return GetLocalFEltSpaceList(ref_local_felt_space);
    }


    inline void FEltSpace
    ::ComputeLocal2Global(ExtendedUnknown::const_shared_ptr&& unknown_ptr,
                          DoComputeProcessorWiseLocal2Global do_compute_processor_wise_local_2_global) const
    {
        ExtendedUnknown::vector_const_shared_ptr buf { std::move(unknown_ptr) };
        return ComputeLocal2Global(buf, do_compute_processor_wise_local_2_global);
    }


    inline const std::map<unsigned int, Internal::FEltSpaceNS::MovemeshHelper>& FEltSpace
    ::GetMovemeshHelperStorage() const noexcept
    {
        return movemesh_helper_data_;
    }


    inline std::map<unsigned int, Internal::FEltSpaceNS::MovemeshHelper>& FEltSpace
    ::GetNonCstMovemeshHelperStorage() noexcept
    {
        return const_cast<std::map<unsigned int, Internal::FEltSpaceNS::MovemeshHelper>&>(GetMovemeshHelperStorage());
    }


    inline void FEltSpace::MoveMesh(const GlobalVector& vector) const
    {
        GetMovemeshHelper(vector).Movemesh(vector, Internal::FEltSpaceNS::MovemeshHelper::From::current_mesh);
    }


    inline void FEltSpace::MoveMeshFromInitialPosition(const GlobalVector& vector) const
    {
        GetMovemeshHelper(vector).Movemesh(vector, Internal::FEltSpaceNS::MovemeshHelper::From::initial_mesh);
    }


    inline const QuadratureRulePerTopology& FEltSpace::GetQuadratureRulePerTopology() const noexcept
    {
        assert(!(!default_quadrature_rule_per_topology_));
        return *default_quadrature_rule_per_topology_;
    }


    inline const QuadratureRulePerTopology* FEltSpace::GetQuadratureRulePerTopologyRawPtr() const noexcept
    {
        assert(!(!default_quadrature_rule_per_topology_));
        return default_quadrature_rule_per_topology_.get();
    }


    inline const Domain& FEltSpace::GetDomain() const noexcept
    {
        return domain_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_F_ELT_SPACE_HXX_
