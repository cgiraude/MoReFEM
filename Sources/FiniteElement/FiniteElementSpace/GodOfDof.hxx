/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Nov 2014 16:08:20 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_


namespace MoReFEM
{



    template<class MoReFEMDataT>
    void GodOfDof::Init(const MoReFEMDataT& morefem_data,
                        FEltSpace::vector_unique_ptr&& a_felt_space_list,
                        DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                        const FilesystemNS::Directory& output_directory)
    {
        std::map<unsigned int, std::vector<unsigned int>> dof_list_per_felt_space;

        const auto parallelism_ptr = morefem_data.GetParallelismPtr();

        Init1(output_directory,
              std::move(a_felt_space_list),
              dof_list_per_felt_space,
              parallelism_ptr);

        Init2(morefem_data.GetInputData());

        Init3(do_consider_proc_wise_local_2_global, dof_list_per_felt_space, parallelism_ptr);
    }


    template<class InputDataT>
    void GodOfDof::Init2(const InputDataT& input_data)
    {
        auto& manager = Internal::FEltSpaceNS::DofProgramWiseIndexListPerVertexCoordIndexListManager
        ::CreateOrGetInstance(__FILE__, __LINE__);
        Advanced::SetFromInputData<>(input_data, manager, *this);

    }


    inline const Mesh& GodOfDof::GetMesh() const noexcept
    {
        return mesh_;
    }


    inline Mesh& GodOfDof::GetNonCstMesh() noexcept
    {
        return mesh_;
    }


    inline const FEltSpace::vector_unique_ptr& GodOfDof::GetFEltSpaceList() const noexcept
    {
        return felt_space_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetProcessorWiseNodeBearerList() const noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseNodeBearerList() noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstGhostNodeBearerList() noexcept
    {
        return ghost_node_bearer_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetGhostNodeBearerList() const noexcept
    {
        return ghost_node_bearer_list_;
    }



    inline unsigned int GodOfDof::NprogramWiseDof() const noexcept
    {
        return GetNdofHolder().NprogramWiseDof();
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetProcessorWiseDofList() const noexcept
    {
        return processor_wise_dof_list_;
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetGhostedDofList() const noexcept
    {
        return ghosted_dof_list_;
    }


    inline unsigned int GodOfDof::NprocessorWiseDof() const noexcept
    {
        assert(GetNdofHolder().NprocessorWiseDof() == static_cast<unsigned int>(processor_wise_dof_list_.size()));
        return GetNdofHolder().NprocessorWiseDof();
    }


    inline unsigned int GodOfDof::NprocessorWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprocessorWiseDof(numbering_subset);
    }


    inline unsigned int GodOfDof::NprogramWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprogramWiseDof(numbering_subset);
    }


    inline unsigned int GodOfDof::NprocessorWiseNodeBearer() const noexcept
    {
        return static_cast<unsigned int>(processor_wise_node_bearer_list_.size());
    }


    inline GodOfDof::shared_ptr GodOfDof::GetSharedPtr()
    {
        return shared_from_this();
    }


    # ifndef NDEBUG
    inline bool GodOfDof::HasInitBeenCalled() const
    {
        return has_init_been_called_;
    }
    # endif // NDEBUG


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseDofList() noexcept
    {
        return processor_wise_dof_list_;
    }


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstGhostedDofList() noexcept
    {
        return ghosted_dof_list_;
    }


    inline const NumberingSubset::vector_const_shared_ptr& GodOfDof::GetNumberingSubsetList() const noexcept
    {
        assert(!numbering_subset_list_.empty());
        assert(std::is_sorted(numbering_subset_list_.cbegin(),
                              numbering_subset_list_.cend(),
                              Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>()));

        return numbering_subset_list_;
    }


    template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard>
    inline const std::string& GodOfDof::GetOutputDirectory() const noexcept
    {
        if constexpr (is_wildcard == Internal::GodOfDofNS::wildcard_for_rank::no)
        {
            assert(!(!output_directory_storage_));
            return output_directory_storage_->GetOutputDirectory();
        }
        else
        {
            assert(!(!output_directory_wildcard_storage_));
            return output_directory_wildcard_storage_->GetOutputDirectory();
        }
    }


    inline const Internal::FEltSpaceNS::NdofHolder& GodOfDof::GetNdofHolder() const noexcept
    {
        assert(!(!Ndof_holder_));
        return *Ndof_holder_;
    }


    inline const Wrappers::Petsc::MatrixPattern& GodOfDof
    ::GetMatrixPattern(const NumberingSubset& numbering_subset) const
    {
        return GetMatrixPattern(numbering_subset, numbering_subset);
    }


    inline const NumberingSubset& GodOfDof::GetNumberingSubset(unsigned int unique_id) const
    {
        return *(GetNumberingSubsetPtr(unique_id));
    }



    # ifndef NDEBUG
    inline DoConsiderProcessorWiseLocal2Global GodOfDof::GetDoConsiderProcessorWiseLocal2Global() const
    {
        return do_consider_proc_wise_local_2_global_;
    }

    # endif // NDEBUG


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof
    ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalMatrix& matrix) const
    {
        switch(BoundaryConditionMethodT)
        {
            case BoundaryConditionMethod::pseudo_elimination:
                ApplyPseudoElimination(boundary_condition, matrix);
                break;
            case BoundaryConditionMethod::penalization:
                ApplyPenalization(boundary_condition, matrix);
                break;
        }
    }


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof
    ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition, GlobalVector& vector) const
    {
        switch(BoundaryConditionMethodT)
        {
            case BoundaryConditionMethod::pseudo_elimination:
                ApplyPseudoElimination(boundary_condition, vector);
                break;
            case BoundaryConditionMethod::penalization:
                ApplyPenalization(boundary_condition, vector);
                break;
        }
    }


    template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard>
    inline const std::string& GodOfDof
    ::GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        if constexpr(is_wildcard == Internal::GodOfDofNS::wildcard_for_rank::yes)
        {
            assert(!(!output_directory_wildcard_storage_));
            return output_directory_wildcard_storage_->GetOutputDirectoryForNumberingSubset(numbering_subset);
        }
        else
        {
            assert(!(!output_directory_storage_));
            return output_directory_storage_->GetOutputDirectoryForNumberingSubset(numbering_subset);
        }
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_HXX_
