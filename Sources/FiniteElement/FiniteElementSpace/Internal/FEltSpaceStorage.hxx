/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_STORAGE_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_STORAGE_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            inline const LocalFEltSpacePerRefLocalFEltSpace& Storage::GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept
            {
                return GetStorage().GetLocalFEltSpacePerRefLocalFEltSpace();
            }


            inline LocalFEltSpacePerRefLocalFEltSpace& Storage::GetNonCstLocalFEltSpacePerRefLocalFEltSpace() noexcept
            {
                return GetNonCstStorage().GetNonCstFEltListPerRefLocalFEltSpace();
            }


            inline const Impl::InternalStorage & Storage::GetStorage() const noexcept
            {
                return internal_storage_;
            }


            inline Impl::InternalStorage& Storage::GetNonCstStorage() noexcept
            {
                return const_cast<Impl::InternalStorage&>(GetStorage());
            }


            inline std::unordered_map<unsigned int, Impl::InternalStorage>& Storage
            ::GetNonCstFEltStoragePerDomain() const noexcept
            {
                return felt_storage_per_domain_;
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_STORAGE_HXX_
