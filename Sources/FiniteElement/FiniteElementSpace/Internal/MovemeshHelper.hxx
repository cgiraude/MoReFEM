/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 10 Feb 2016 11:52:23 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MOVEMESH_HELPER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MOVEMESH_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            inline std::size_t MovemeshHelper::Ncoords() const noexcept
            {
                return dof_index_list_per_coord_.size();
            }


            inline const std::vector<unsigned int>& MovemeshHelper::GetDofIndexList(std::size_t index) const noexcept
            {
                assert(index < dof_index_list_per_coord_.size());
                return dof_index_list_per_coord_[index];
            }


            inline const GodOfDof& MovemeshHelper::GetGodOfDof() const noexcept
            {
                return god_of_dof_;
            }


            inline const std::array<double, 3>& MovemeshHelper
            ::GetInitialPosition(std::size_t coord_index) const noexcept
            {
                assert(coord_index < initial_position_per_coord_.size());
                return initial_position_per_coord_[coord_index];
            }


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_MOVEMESH_HELPER_HXX_
