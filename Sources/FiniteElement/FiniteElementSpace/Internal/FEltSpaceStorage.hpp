/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_STORAGE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_STORAGE_HPP_

# include <unordered_map>
# include <memory>

# include "Utilities/Mutex/Mutex.hpp"

# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
# include "FiniteElement/FiniteElementSpace/Internal/Impl/FEltSpaceInternalStorage.hpp"


namespace MoReFEM
{

    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Domain;


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            struct ReduceToProcessorWise;


        } // namespace FEltSpaceNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief The class in charge of storing finite elements and dofs structure of a given finite element space.
             *
             * There are in fact two different types of storage in this class:
             * - The first is really the list of all the finite elements covered by the FEltSpace.
             * - The second is the subset of this list for every relevant Domain. The reason for this is that
             * the test to know whether a FElt (or in fact its related GeometricElt) belongs to a given
             * Domain is not cheap, and therefore doing it only once is appealing.
             */
            class Storage final : public ::MoReFEM::Crtp::Mutex<Storage>,
                                  public ::MoReFEM::Crtp::CrtpMpi<Storage>
            {

            public:

                //! Alias to unique pointer.
                using const_unique_ptr = std::unique_ptr<const Storage>;

                //! Friendship to Internal class ReduceToProcessorWise, which needs access to non constant accessor.
                friend struct ReduceToProcessorWise;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] local_felt_space_list_per_type List of \a LocalFEltSpace per \a RefLocalFEltSpace. This container is
                 * computed by FEltSpace::SetFEltList(), which is the method in charge of building current object
                 * to store and give quick access to relevant finite element data.
                 */
                explicit Storage(const ::MoReFEM::Wrappers::Mpi& mpi,
                                 LocalFEltSpacePerRefLocalFEltSpace&& local_felt_space_list_per_type);

                //! Destructor.
                ~Storage() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Storage(const Storage& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Storage(Storage&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Storage& operator=(const Storage& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Storage& operator=(Storage&& rhs) = delete;


                ///@}

                //! Access to the complete finite element list.
                const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept;

                //! Access to the finite element list related to a given Domain.
                //! \param[in] domain \a Domain used as filter.
                const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace(const Domain& domain) const;

                //! Whether there are finite elements in the storage or not.
                bool IsEmpty() const noexcept;

            private:


                //! Access to the underlying object that handles truly the list of finite element and possibly dofs.
                const Impl::InternalStorage& GetStorage() const noexcept;

                //! Non constant access to the underlying object that handles truly the list of finite element and possibly dofs.
                Impl::InternalStorage& GetNonCstStorage() noexcept;

                //! Non constant access to the list of finite element per reference finite element space.
                LocalFEltSpacePerRefLocalFEltSpace& GetNonCstLocalFEltSpacePerRefLocalFEltSpace() noexcept;

                /*!
                 * \brief Non constant access to the finite element storage per domain.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is nonetheless marked const as the quantity
                 * accessed is mutable.
                 * \endinternal
                 *
                 * \return Reference to the finite element storage per domain.
                 */
                std::unordered_map<unsigned int, Impl::InternalStorage>& GetNonCstFEltStoragePerDomain() const noexcept;


            private:

                //! The complete list of finite elements inside a FEltSpace, regardless of Domain.
                Impl::InternalStorage internal_storage_;


                /*!
                 * \brief The list of finite elements and dof structure sort per domain.
                 *
                 * The key is the UniqueId of a Domain, which is given by the method Domain::GetUniqueId().
                 *
                 * \internal <b><tt>[internal]</tt></b> The mutable keyword is there because
                 * GetLocalFEltSpacePerRefLocalFEltSpace(const Domain&) may have
                 * to compute its content on the fly. No other method should be allowed to modify it!
                 * \endinternal
                 */
                mutable std::unordered_map<unsigned int, Impl::InternalStorage> felt_storage_per_domain_;
            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/FEltSpaceStorage.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_STORAGE_HPP_
