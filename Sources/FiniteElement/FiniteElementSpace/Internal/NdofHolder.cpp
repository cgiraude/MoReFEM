/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 14:51:03 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
        
            NdofHolder::NdofHolder(const NodeBearer::vector_shared_ptr& program_wise_node_bearer_list,
                                   const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                                   const unsigned int mpi_rank)
            {
                // Perform the computation disregarding the numbering subsets.
                {
                    assert(!numbering_subset_list.empty());
                    
                    
                    for (const auto& node_bearer_ptr : program_wise_node_bearer_list)
                    {
                        assert(!(!node_bearer_ptr));
                        const auto& node_bearer = *node_bearer_ptr;
                        
                        auto Ndof = node_bearer.Ndof();
                        Nprogram_wise_dof_ += Ndof;
                        
                        if (node_bearer.GetProcessor() == mpi_rank)
                            Nprocessor_wise_dof_ += Ndof;
                    }
                }
                
                {
                    
                    // Now do it for each numbering subset.
                    for (const auto& numbering_subset_ptr : numbering_subset_list)
                    {
                        assert(!(!numbering_subset_ptr));
                        const auto& numbering_subset = *numbering_subset_ptr;
                        unsigned int local_Nprocessor_wise_dof = 0u;
                        unsigned int local_Nprogram_wise_dof = 0u;
                        
                        for (const auto& node_bearer_ptr : program_wise_node_bearer_list)
                        {
                            assert(!(!node_bearer_ptr));
                            const auto& node_bearer = *node_bearer_ptr;
                            
                            const auto Ndof = node_bearer.Ndof(numbering_subset);
                            local_Nprogram_wise_dof += Ndof;
                            
                            if (node_bearer.GetProcessor() == mpi_rank)
                                local_Nprocessor_wise_dof += Ndof;
                        }
                        
                        {
                            auto&& pair = std::make_pair(numbering_subset.GetUniqueId(), local_Nprocessor_wise_dof);
                            auto check = Nprocessor_wise_dof_per_numbering_subset_.insert(std::move(pair));
                        
                            static_cast<void>(check);
                            assert(check.second && "A numbering subset should be inserted only once!");
                        }
                        
                        {
                            auto&& pair = std::make_pair(numbering_subset.GetUniqueId(), local_Nprogram_wise_dof);
                            auto check = Nprogram_wise_dof_per_numbering_subset_.insert(std::move(pair));
                            
                            static_cast<void>(check);
                            assert(check.second && "A numbering subset should be inserted only once!");
                        }
                    }
                }
            }

            
         
            
        } // namespace FEltSpaceNS
            
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
