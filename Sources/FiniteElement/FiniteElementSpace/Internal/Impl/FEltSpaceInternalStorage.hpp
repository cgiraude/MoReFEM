/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_

# include <memory>
# include <vector>
# include <unordered_map>

# include "Utilities/Mutex/Mutex.hpp"
# include "Utilities/Containers/EnumClass.hpp"
# include "Utilities/Mpi/Mpi.hpp"

# include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"
# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"


namespace MoReFEM
{


    /*!
     * \brief Provides for all the \a RefLocalFEltSpace the list of associated \a LocalFEltSpace.
     *
     * \internal <b><tt>[internal]</tt></b> This container behaves almost like a map, except that there
     * is no ordering relation on the keys (that's why a std::map was not used in the first place).
     * \endinternal
     */
    using LocalFEltSpacePerRefLocalFEltSpace =
        std::vector<std::pair<Internal::RefFEltNS::RefLocalFEltSpace::const_unique_ptr, LocalFEltSpace::per_geom_elt_index>>;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;
    class GlobalVector;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief Find in a LocalFEltSpacePerRefLocalFEltSpace the \a RefLocalFEltSpace matching the \a RefGeomElt
             * which underlying index is I.
             *
             * \tparam I Underlying index beneath a \a GeometricEltEnum
             *
             * \param[in] container The container into which the search is performed.
             *
             * \return The \a RefLocalFEltSpace adequate for the given \a GeometricEltEnum if one is defined, nullptr
             * otherwise. The latter is not an error: it might just be for instance the search was performed for a
             * \a RefGeometricElt not present in the model currently run.
             *
             */
            template<unsigned int I>
            const Internal::RefFEltNS::RefLocalFEltSpace*
                FindRefLocalFEltSpace(const LocalFEltSpacePerRefLocalFEltSpace& container);



            namespace Impl
            {


                /*!
                 * \brief Helper class that holds finite element and dof informations about either a FEltSpace
                 * or a couple FEltSpace/Domain.
                 *
                 * Should be used only within Internal::FEltSpaceNS::Storage.
                 */
                class InternalStorage final : public ::MoReFEM::Crtp::Mutex<InternalStorage>,
                                              public ::MoReFEM::Crtp::CrtpMpi<InternalStorage>
                {

                public:

                    /// \name Special members.
                    ///@{

                    /*!
                     * \brief Constructor.
                     *
                     * \copydetails doxygen_hide_mpi_param
                     * \param[in] felt_list_per_ref_felt_space Finite element list per reference finite element space.
                     * It's just stored in the current class; it is actually computed elsewhere.
                     */
                    explicit InternalStorage(const ::MoReFEM::Wrappers::Mpi& mpi,
                                             LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_ref_felt_space);

                    //! Destructor.
                    ~InternalStorage() = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    InternalStorage(const InternalStorage& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    InternalStorage(InternalStorage&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    InternalStorage& operator=(const InternalStorage& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    InternalStorage& operator=(InternalStorage&& rhs) = delete;

                    ///@}

                public:

                    //! Get the list of all finite element sort per reference felt space and local felt space.
                    const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept;

                    //! Non constant access to the list of all finite element sort per reference felt space and local felt space.
                    LocalFEltSpacePerRefLocalFEltSpace& GetNonCstFEltListPerRefLocalFEltSpace() noexcept;

                    //! Whether there are finite elements in the storage.
                    bool IsEmpty() const noexcept;


                private:

                    //! List of all finite element sort per reference felt space and local felt space.
                    LocalFEltSpacePerRefLocalFEltSpace felt_list_per_ref_felt_space_;

                };


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/Impl/FEltSpaceInternalStorage.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_
