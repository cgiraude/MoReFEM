/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 19 Aug 2014 14:27:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_ATTRIBUTE_PROCESSOR_HELPER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_ATTRIBUTE_PROCESSOR_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            namespace Impl
            {




            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_ATTRIBUTE_PROCESSOR_HELPER_HXX_
