/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 24 Oct 2014 12:28:00 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/Impl/InterfaceSpecialization.hpp"



namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
            namespace Impl
            {
                
                
                
                
                InterfaceSpecialization<Vertex>::InterfaceListType InterfaceSpecialization<Vertex>
                ::GetInterfaceList(const GeometricElt& geom_elt)
                {
                    return geom_elt.GetVertexList();
                }
                
                
                InterfaceSpecialization<Vertex>::NodeListType InterfaceSpecialization<Vertex>
                ::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                              const Vertex& vertex,
                              unsigned int local_index)
                {
                    static_cast<void>(vertex); // not used; interface is used merely to fetch orientation, which is
                    // irrelevant for vertices.
                    return { basic_ref_felt.GetLocalNodeOnVertexPtr(local_index) };
                }
                
                
                NodeBearer::shared_ptr InterfaceSpecialization<Vertex>
                ::CreateNodeBearer(const Vertex::shared_ptr& vertex_ptr)
                {
                    assert(!(!vertex_ptr));
                    return std::make_shared<NodeBearer>(vertex_ptr);
                }
                
                
                unsigned int InterfaceSpecialization<Vertex>::Ninterface(const GeometricElt& geom_elt)
                {
                    return geom_elt.Nvertex();
                }
                
                
                InterfaceSpecialization<OrientedEdge>::NodeListType InterfaceSpecialization<OrientedEdge>
                ::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                              const OrientedEdge& edge,
                              unsigned int local_index)
                {
                    return basic_ref_felt.GetLocalNodeOnEdgeList(local_index, edge.GetOrientation());
                }
                
                
                NodeBearer::shared_ptr InterfaceSpecialization<OrientedEdge>
                ::CreateNodeBearer(const OrientedEdge::shared_ptr& edge_ptr)
                {
                    assert(!(!edge_ptr));
                    return std::make_shared<NodeBearer>(edge_ptr->GetUnorientedInterfacePtr());
                }
                
                
                InterfaceSpecialization<OrientedEdge>::InterfaceListType InterfaceSpecialization<OrientedEdge>
                ::GetInterfaceList(const GeometricElt& geom_elt)
                {
                    return geom_elt.GetOrientedEdgeList();
                }

                
                unsigned int InterfaceSpecialization<OrientedEdge>::Ninterface(const GeometricElt& geom_elt)
                {
                    return geom_elt.Nedge();
                }
                
                
                InterfaceSpecialization<OrientedFace>::NodeListType InterfaceSpecialization<OrientedFace>
                ::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                              const OrientedFace& face,
                              unsigned int local_index)
                {
                    return basic_ref_felt.GetLocalNodeOnFaceList(local_index, face.GetOrientation());
                }
                
                
                NodeBearer::shared_ptr InterfaceSpecialization<OrientedFace>
                ::CreateNodeBearer(const OrientedFace::shared_ptr& face_ptr)
                {
                    assert(!(!face_ptr));
                    return std::make_shared<NodeBearer>(face_ptr->GetUnorientedInterfacePtr());
                }
                
                
                InterfaceSpecialization<OrientedFace>::InterfaceListType InterfaceSpecialization<OrientedFace>
                ::GetInterfaceList(const GeometricElt& geom_elt)
                {
                    return geom_elt.GetOrientedFaceList();
                }
                
                
                unsigned int InterfaceSpecialization<OrientedFace>::Ninterface(const GeometricElt& geom_elt)
                {
                    return geom_elt.Nface();
                }
                
                
                InterfaceSpecialization<Volume>::NodeListType InterfaceSpecialization<Volume>
                ::GetNodeList(const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
                              const Volume& volume,
                              unsigned int local_index)
                {
                    static_cast<void>(volume);
                    static_cast<void>(local_index);
                    return basic_ref_felt.GetLocalNodeOnVolumeList();
                }
                
                
                InterfaceSpecialization<Volume>::InterfaceListType InterfaceSpecialization<Volume>
                ::GetInterfaceList(const GeometricElt& geom_elt)
                {
                    return { geom_elt.GetVolumePtr() };
                }
                
                
                NodeBearer::shared_ptr InterfaceSpecialization<Volume>
                ::CreateNodeBearer(const Volume::shared_ptr& volume_ptr)
                {
                    assert(!(!volume_ptr));
                    return std::make_shared<NodeBearer>(volume_ptr);
                }
                
                
                unsigned int InterfaceSpecialization<Volume>::Ninterface(const GeometricElt& geom_elt)
                {
                    return (geom_elt.GetDimension() >= 3u ? 1u : 0u);
                }
                
                
            } // namespace Impl
            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
