/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 May 2014 15:59:32 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_ATTRIBUTE_PROCESSOR_HELPER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_ATTRIBUTE_PROCESSOR_HELPER_HPP_

# include <memory>
# include <vector>
# include <functional>

# include "Utilities/Containers/UnorderedMap.hpp"
# include "Utilities/Containers/Tuple.hpp"

# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

# include "Geometry/GeometricElt/GeometricElt.hpp"

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class LocalFEltSpace;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            namespace Impl
            {


                //! Helper class to decide which processor should handle each finite element.
                class AttributeProcessorHelper final
                {
                public:

                    //! Constructor.
                    //! \param[in] Nprocessor Number of processor in the mpi scheme.
                    explicit AttributeProcessorHelper(unsigned int Nprocessor);

                    //! Destructor.
                    ~AttributeProcessorHelper() = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    AttributeProcessorHelper(const AttributeProcessorHelper& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    AttributeProcessorHelper(AttributeProcessorHelper&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    AttributeProcessorHelper& operator=(const AttributeProcessorHelper& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    AttributeProcessorHelper& operator=(AttributeProcessorHelper&& rhs) = delete;


                    /*!
                     * \brief Determine which processor takes the current finite element group.
                     *
                     * \param[in] local_felt_space Finite element group considered.
                     *
                     * \return Processor to which the finite element group is attributed.
                     *
                     * The assignment rule is the following:
                     * - If there is a Volumic node bearer in the LocalFEltSpace, take its processor.
                     * - If not, assign the finite element group to the processor in charge of most of its nodes.
                     * - In case there is a tie, choose the processor with the most ghost nodes at this stage.
                     */
                    unsigned int ProcessorForCurrentLocalFEltSpace(const LocalFEltSpace& local_felt_space);

                    //! Number of finite elements on \a processor.
                    //! \param[in] processor Processor for which the tally is done.
                    unsigned int NfiniteElt(unsigned int processor) const;

                private:

                    /*!
                     * \brief Return the number of node bearers on each processor.
                     *
                     * \param[in] node_bearer_list List of node bearers to consider.
                     * \return How \a node_list is partitionned among processors. Index of this vector
                     * is the processor involved, the value the number of nodes from \a node_list that
                     * are handled by said processor.
                     */
                    std::vector<unsigned int> NnodeBearerPerProcessor(const NodeBearer::vector_shared_ptr& node_bearer_list) const;

                    //! Returns the number of processors.
                    unsigned int Nprocessor() const;

                private:

                    //! How the finite element groups managed so far are shared among processors.
                    std::vector<unsigned int> Nlocal_felt_space_per_processor_;

                    /*!
                     * \brief Processor chosen for each geometric element (and its related local felt space).
                     *
                     * This container is useful to ensure a given geometriuc element is truly handled by the same processor
                     * (without it each finite element space could yield a different decision).
                     */
                    std::unordered_map<unsigned int, unsigned int> processor_for_each_geom_elt_;
                };


            } // namespace Impl


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/Impl/AttributeProcessorHelper.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_IMPL_x_ATTRIBUTE_PROCESSOR_HELPER_HPP_
