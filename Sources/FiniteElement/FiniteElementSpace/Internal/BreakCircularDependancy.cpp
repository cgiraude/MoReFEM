/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 14:02:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "FiniteElement/FiniteElementSpace/Internal/BreakCircularDependancy.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
            unsigned int GetUniqueId(const GodOfDof& god_of_dof)
            {
                return god_of_dof.GetUniqueId();
            }
            
            
            const Mesh& GetMesh(const GodOfDof& god_of_dof)
            {
                return god_of_dof.GetMesh();
            }
            
            
            const ::MoReFEM::Wrappers::Mpi& GetMpi(const GodOfDof& god_of_dof)
            {
                return god_of_dof.GetMpi();
            }

         
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
