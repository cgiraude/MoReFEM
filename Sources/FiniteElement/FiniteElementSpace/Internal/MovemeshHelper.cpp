/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 10 Feb 2016 11:52:23 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"



namespace MoReFEM
{
    
    
    namespace Internal
    {
        
    
        namespace FEltSpaceNS
        {
            
            
            void MovemeshHelper::ConstructHelper(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                                 const ExtendedUnknown& extended_unknown
                                                 # ifndef NDEBUG
                                                 , const Mesh& mesh
                                                 , const Dof::vector_shared_ptr& felt_space_dof_list
                                                 # endif // NDEBUG
                                                 )
            {
                #ifndef NDEBUG
                const auto Ncoords = this->Ncoords();
                assert(Ncoords == static_cast<std::size_t>(mesh.NprocessorWiseCoord() + mesh.NghostCoord()));
                #endif // NDEBUG
                
                const auto& unknown = extended_unknown.GetUnknown();
                const auto& numbering_subset = extended_unknown.GetNumberingSubset();
                decltype(auto) shape_function_label = extended_unknown.GetShapeFunctionLabel();
                
                for (const auto& node_bearer_ptr : node_bearer_list)
                {
                    assert(!(!node_bearer_ptr));
                    
                    const auto& node_bearer = *node_bearer_ptr;
                    
                    const auto node_list = node_bearer.GetNodeList(unknown, shape_function_label);
                    
                    const auto& interface = node_bearer.GetInterface();
                    
                    // \todo #248 Works only for P1 geometry!
                    if (interface.GetNature() != ::MoReFEM::InterfaceNS::Nature::vertex)
                        continue;
                    
                    const auto& coords_list = interface.GetVertexCoordsList();
                    assert(coords_list.size() == 1 && "Due to current limitation to P1 geometry...");
                    assert(!(!coords_list.back()));
                    const auto& coords = *(coords_list.back());
                    const auto coords_index = coords.GetPositionInCoordsListInMesh<MpiScale::processor_wise>();
                    assert(coords_index < Ncoords);
                    
                    for (const auto& node_ptr : node_list)
                    {
                        assert(!(!node_ptr));
                        const auto& node = *node_ptr;
                        
                        if (!node.DoBelongToNumberingSubset(numbering_subset))
                            continue;
                        
                        std::vector<unsigned int> new_entry;
                        
                        const auto& dof_list = node.GetDofList();
                        
                        for (const auto& dof_ptr : dof_list)
                        {
                            assert(!(!dof_ptr));
                            const auto& dof = *dof_ptr;
                            
                            assert(std::find_if(felt_space_dof_list.cbegin(),
                                                felt_space_dof_list.cend(),
                                                [&dof](const auto& felt_space_dof_ptr)
                                                {
                                                    return *felt_space_dof_ptr == dof;
                                                })
                                   != felt_space_dof_list.cend()
                                   );
                            
                            new_entry.push_back(dof.GetProcessorWiseOrGhostIndex(numbering_subset));
                        }
                        
                        if (new_entry.empty())
                            continue;
                        
                        assert(new_entry.size() == static_cast<std::size_t>(mesh.GetDimension()));
                        dof_index_list_per_coord_[coords_index] = std::move(new_entry);
                        initial_position_per_coord_[coords_index] = coords.GetCoordinateList();
                    }
                }
            }

          
            MovemeshHelper::MovemeshHelper(const GodOfDof& god_of_dof,
                                           const ExtendedUnknown& extended_unknown
                                           # ifndef NDEBUG
                                           , const Dof::vector_shared_ptr& felt_space_processor_wise_dof_list
                                           , const Dof::vector_shared_ptr& felt_space_ghosted_dof_list
                                           #endif // NDEBUG
                                           )
            : god_of_dof_(god_of_dof)
            # ifndef NDEBUG
            , numbering_subset_(extended_unknown.GetNumberingSubset())
            #endif // NDEBUG
            {
                const auto& mesh = god_of_dof.GetMesh();
                const auto Ncoords = static_cast<std::size_t>(mesh.NprocessorWiseCoord() + mesh.NghostCoord());
                
                dof_index_list_per_coord_.resize(static_cast<std::size_t>(Ncoords));
                initial_position_per_coord_.resize(static_cast<std::size_t>(Ncoords));
                
                ConstructHelper(god_of_dof.GetProcessorWiseNodeBearerList(),
                                extended_unknown
                                # ifndef NDEBUG
                                , mesh
                                , felt_space_processor_wise_dof_list
                                 #endif // NDEBUG
                                );
                
                ConstructHelper(god_of_dof.GetGhostNodeBearerList(),
                                extended_unknown
                                # ifndef NDEBUG
                                , mesh
                                , felt_space_ghosted_dof_list
                                #endif // NDEBUG
                                );
            }
            
            
            void MovemeshHelper::MoveCoords(const Coords::vector_unique_ptr& coords_list,
                                            const ::MoReFEM::Wrappers::Petsc
                                            ::AccessVectorContent<Utilities::Access::read_only>& vector_content,
                                            From from) const
            {
                const auto Ncoord = coords_list.size();
                
                for (auto coord_index = 0ul; coord_index < Ncoord; ++coord_index)
                {
                    auto& coord_ptr = coords_list[coord_index];
                    assert(!(!coord_ptr));
                    auto& coord = *coord_ptr;
                    
                    const auto& dof_index_list = GetDofIndexList(coord_index);
                    
                    unsigned int index = 0u;
                    
                    const auto& coord_position = GetInitialPosition(coord_index);
                    
                    for (const auto& dof_index : dof_index_list)
                    {
                        switch(from)
                        {
                            case From::current_mesh:
                                coord.GetNonCstValue(index) += vector_content.GetValue(dof_index);
                                break;
                            case From::initial_mesh:
                                assert(index < coord_position.size());
                                coord.GetNonCstValue(index) = coord_position[index] + vector_content.GetValue(dof_index);
                                break;
                        }
                        
                        ++index;
                    }
                }
            }

            
            
            void MovemeshHelper::Movemesh(const GlobalVector& displacement,
                                          From from) const
            {
                assert(numbering_subset_ == displacement.GetNumberingSubset());
                
                const auto& god_of_dof = GetGodOfDof();
                const auto& mesh = god_of_dof.GetMesh();
                
                ::MoReFEM::Wrappers::Petsc
                ::AccessGhostContent helper(displacement,
                                            __FILE__, __LINE__);
                
                decltype(auto) vector_with_ghost = helper.GetVectorWithGhost();
                
                ::MoReFEM::Wrappers::Petsc
                ::AccessVectorContent<Utilities::Access::read_only> vector_content(vector_with_ghost,
                                                                                    __FILE__, __LINE__);
                
                MoveCoords(mesh.GetProcessorWiseCoordsList(), vector_content, from);
                MoveCoords(mesh.GetGhostedCoordsList(), vector_content, from);
            }

            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
        
   
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
