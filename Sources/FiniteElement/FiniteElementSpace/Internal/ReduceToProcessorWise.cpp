/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Nov 2014 17:24:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Impl/AttributeProcessorHelper.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/ReduceToProcessorWise.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
            
            
            namespace // anonymous
            {
                
                
                
                /*!
                 * \brief Reduce the finite element list to the processor-wise values.
                 *
                 * This is this function that attributes a processor to each local_felt_space, unless it has already been
                 * attributed for a previous finite element space (this function is called once per finite element space).
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in,out] felt_list_per_type The finite element list to be reduced to processor-wise values.
                 * \param[in,out] attribute_processor_helper Object which make the decision about the processor on which
                 * each geometric element should be related.
                 */
                void ReduceFEltList(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    LocalFEltSpacePerRefLocalFEltSpace& felt_list_per_type,
                                    Impl::AttributeProcessorHelper& attribute_processor_helper);
                
                
                /*!
                 * \brief Compute the list of ghost node bearers.
                 *
                 * \param[in] mpi_rank Rank of the current processor.
                 * \param[in] processor_wise_felt_list_per_type The list of processor-wise finite elements
                 * as computed by ReduceFEltList().
                 *
                 * \param[in,out] ghost_node_bearer_list The list of ghost node bearers, i.e. the ones that were in
                 * processor-wise finite elements but not in processor-wise node bearer list. It can also be an input
                 * parameter: if several finite element spaces are involved the same list us kept for all of them.
                 */
                void ComputeGhostNodeBearerList(const unsigned int mpi_rank,
                                                const LocalFEltSpacePerRefLocalFEltSpace& processor_wise_felt_list_per_type,
                                                NodeBearer::vector_shared_ptr& ghost_node_bearer_list);
                
            } // namespace anonymous
            
            
            /*!
             * \brief Reduce the mesh object to its components on the local processor.
             *
             * \internal This is a struct solely to help friendship declaration in Mesh.
             * This frienship is also the reason it is not in the anonymous namespace!
             * \endinternal
             */
            struct ReduceMesh
            {
                
                
                /*!
                 * \brief Static method that actually does all the work.
                 *
                 * \param[in] felt_space_list List of all finite element space. For each of them, the list
                 * of geometric elements used will be extracted and put in the list of elements to keep.
                 * \param[in] processor_wise_node_bearer_list List of processor-wise node bearers.
                 * \param[in,out] mesh The Mesh to be reduced to processor-wise data.
                 */
                static void Perform(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    const FEltSpace::vector_unique_ptr& felt_space_list,
                                    const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                    const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                                    Mesh& mesh);
            };
            
            
            void ReduceToProcessorWise::Perform(const ::MoReFEM::Wrappers::Mpi& mpi,
                                                const FEltSpace::vector_unique_ptr& felt_space_list,
                                                const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                                                NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                                                Mesh& mesh)
            
            {
                const unsigned int mpi_rank = mpi.GetRank<unsigned int>();
                
                assert(ghost_node_bearer_list.empty());
                
                std::unordered_map<unsigned int, unsigned int> processor_for_each_geom_elt;
                processor_for_each_geom_elt.max_load_factor(Utilities::DefaultMaxLoadFactor());
                
                Impl::AttributeProcessorHelper attribute_processor_helper(mpi.Nprocessor<unsigned int>());
                
                for (const auto& felt_space_ptr : felt_space_list)
                {
                    assert(!(!felt_space_ptr));
                    auto& felt_space = *felt_space_ptr;
                    auto& felt_storage = felt_space.GetNonCstFEltStorage();
                    auto& felt_list_per_type = felt_storage.GetNonCstLocalFEltSpacePerRefLocalFEltSpace();
                    ReduceFEltList(mpi, felt_list_per_type, attribute_processor_helper);
                    
                    ComputeGhostNodeBearerList(mpi_rank,
                                               felt_list_per_type,
                                               ghost_node_bearer_list);
                }
                
                Utilities::EliminateDuplicate(ghost_node_bearer_list,
                                              Utilities::PointerComparison::Less<NodeBearer::shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NodeBearer::shared_ptr>());
                
                ReduceMesh::Perform(mpi,
                                    felt_space_list,
                                    processor_wise_node_bearer_list,
                                    ghost_node_bearer_list,
                                    mesh);
            }
            
            
            namespace // anonymous
            {
                
                
                void CoordsListHelper(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                      Coords::vector_raw_ptr& reduced_coords_list)
                {
                    for (const auto& node_bearer_ptr : node_bearer_list)
                    {
                        assert(!(!node_bearer_ptr));
                        
                        decltype(auto) interface = node_bearer_ptr->GetInterface();
                        
                        decltype(auto) vertex_coords_on_interface = interface.GetVertexCoordsList();
                        
                        // #248 I assume here Coords are only vertices.
                        std::copy(vertex_coords_on_interface.cbegin(),
                                  vertex_coords_on_interface.cend(),
                                  std::back_inserter(reduced_coords_list));
                    }
                }
                
                
            } // namespace anonymous
            
            
            
            void ReduceMesh
            ::Perform(const ::MoReFEM::Wrappers::Mpi& mpi,
                      const FEltSpace::vector_unique_ptr& felt_space_list,
                      const NodeBearer::vector_shared_ptr& processor_wise_node_bearer_list,
                      const NodeBearer::vector_shared_ptr& ghost_node_bearer_list,
                      Mesh& mesh)
            {
                GeometricElt::vector_shared_ptr processor_wise_geo_element;
                
                for (const auto& felt_space_ptr : felt_space_list)
                {
                    assert(!(!felt_space_ptr));
                    const auto& processor_wise_felt_list_per_type =
                    felt_space_ptr->GetLocalFEltSpacePerRefLocalFEltSpace();
                    
                    for (const auto& pair : processor_wise_felt_list_per_type)
                    {
                        const auto& local_felt_space_list = pair.second;
                        
                        for (const auto& local_felt_space_pair : local_felt_space_list)
                        {
                            const auto& local_felt_space_ptr = local_felt_space_pair.second;
                            assert(!(!local_felt_space_ptr));
                            processor_wise_geo_element.push_back(local_felt_space_ptr->GetGeometricEltPtr());
                        }
                    }
                }
                
                Utilities::EliminateDuplicate(processor_wise_geo_element,
                                              Utilities::PointerComparison::Less<GeometricElt::shared_ptr>(),
                                              Utilities::PointerComparison::Equal<GeometricElt::shared_ptr>());
                
                // Extract the list of \a Coords from the node bearer lists.
                Coords::vector_raw_ptr processor_wise_coords_list;
                Coords::vector_raw_ptr ghosted_coords_list;
                
                CoordsListHelper(processor_wise_node_bearer_list, processor_wise_coords_list);
                CoordsListHelper(ghost_node_bearer_list, ghosted_coords_list);
                
                Utilities::EliminateDuplicate(processor_wise_coords_list,
                                              Utilities::PointerComparison::Less<Coords*>(),
                                              Utilities::PointerComparison::Equal<Coords*>());
                
                Utilities::EliminateDuplicate(ghosted_coords_list,
                                              Utilities::PointerComparison::Less<Coords*>(),
                                              Utilities::PointerComparison::Equal<Coords*>());
                
                const auto proc_wise_coords_list_begin = processor_wise_coords_list.cbegin();
                const auto proc_wise_coords_list_end = processor_wise_coords_list.cend();
                
                const auto it_partition =
                    std::partition(ghosted_coords_list.begin(),
                                   ghosted_coords_list.end(),
                                   [&proc_wise_coords_list_begin, &proc_wise_coords_list_end](const Coords* coords)
                                   {
                                       return !std::binary_search(proc_wise_coords_list_begin,
                                                                  proc_wise_coords_list_end,
                                                                  coords,
                                                                  Utilities::PointerComparison::Less<const Coords*>());
                                   });
                
                ghosted_coords_list.erase(it_partition, ghosted_coords_list.end());
                
                std::vector<unsigned int> vertex_index_list;
                std::vector<unsigned int> edge_index_list;
                std::vector<unsigned int> face_index_list;

                // Memory allocated here is overkill, but it's probably better than recopying multiple times the
                // growing vector. We could have tried to be clever and use a factor such as 1. / (Nrank - 1) but
                // it's probably overthinking.
                vertex_index_list.reserve(mesh.Nvertex());
                edge_index_list.reserve(mesh.Nedge());
                face_index_list.reserve(mesh.Nface());

                unsigned int Nprocessor_wise_volume {};

                for (const auto& geom_elt_ptr : processor_wise_geo_element)
                {
                    assert(!(!geom_elt_ptr));
                    const auto& geom_elt = *geom_elt_ptr;

                    decltype(auto) vertex_list = geom_elt.GetVertexList();

                    for (const auto& vertex_ptr : vertex_list)
                    {
                        assert(!(!vertex_ptr));
                        vertex_index_list.push_back(vertex_ptr->GetIndex());
                    }

                    if (mesh.AreEdgesBuilt())
                    {
                        decltype(auto) edge_list = geom_elt.GetOrientedEdgeList();

                        for (const auto& edge_ptr : edge_list)
                        {
                            assert(!(!edge_ptr));
                            edge_index_list.push_back(edge_ptr->GetIndex());
                        }
                    }

                    if (mesh.AreFacesBuilt())
                    {
                        decltype(auto) face_list = geom_elt.GetOrientedFaceList();

                        for (const auto& face_ptr : face_list)
                        {
                            assert(!(!face_ptr));
                            face_index_list.push_back(face_ptr->GetIndex());
                        }
                    }

                    if (mesh.AreVolumesBuilt())
                    {
                        if (geom_elt.GetVolumePtr() != nullptr)
                            ++Nprocessor_wise_volume;
                    }
                }

                Utilities::EliminateDuplicate(vertex_index_list);
                Utilities::EliminateDuplicate(edge_index_list);
                Utilities::EliminateDuplicate(face_index_list);

                const auto Nprocessor_wise_vertex = static_cast<unsigned int>(vertex_index_list.size());
                const auto Nprocessor_wise_edge = static_cast<unsigned int>(edge_index_list.size());
                const auto Nprocessor_wise_face = static_cast<unsigned int>(face_index_list.size());

                mesh.ShrinkToProcessorWise(mpi,
                                           processor_wise_geo_element,
                                           std::move(processor_wise_coords_list),
                                           std::move(ghosted_coords_list),
                                           static_cast<unsigned int>(Nprocessor_wise_vertex),
                                           static_cast<unsigned int>(Nprocessor_wise_edge),
                                           static_cast<unsigned int>(Nprocessor_wise_face),
                                           static_cast<unsigned int>(Nprocessor_wise_volume));
            }
            

            namespace // anonymous
            {
                
                
                void ReduceFEltList(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    LocalFEltSpacePerRefLocalFEltSpace& felt_list_per_type,
                                    Impl::AttributeProcessorHelper& attribute_processor_helper)
                {
                    LocalFEltSpacePerRefLocalFEltSpace shrunk_list;
                    const unsigned int mpi_rank = mpi.GetRank<unsigned int>();
                    
                    for (const auto& pair : felt_list_per_type)
                    {
                        const auto& ref_felt_space_ptr = pair.first;
                        const auto& local_felt_space_list = pair.second;
                        
                        LocalFEltSpace::per_geom_elt_index buf;
                        buf.max_load_factor(Utilities::DefaultMaxLoadFactor());
                        
                        for (const auto& local_felt_space_pair : local_felt_space_list)
                        {
                            const auto& local_felt_space_ptr = local_felt_space_pair.second;
                            assert(!(!local_felt_space_ptr));
                            const auto& local_felt_space = *local_felt_space_ptr;
                            
                            // Determine to which processor the local finite element space should be attributed.
                            const auto chosen_processor_for_local_felt_space =
                            attribute_processor_helper.ProcessorForCurrentLocalFEltSpace(local_felt_space);
                            
                            if (chosen_processor_for_local_felt_space == mpi_rank)
                                buf.insert({ local_felt_space.GetGeometricElt().GetIndex(),  local_felt_space_ptr });
                            
                            //                        IF NOT BUT IF THERE ARE SOME NODE BEARERS DOF SHOULD BE ADDED TO THE DOF LIST OF THE
                            //                        FINITE ELEMENT SPACE (WHERE IT COULD IN FACT BE GHOST OR NOT!)
                            //                        IF THIS DOES NOT WORK, another SOLUTION (perhaps even better!) IS TO GIVE YET ANOTHER INDEX TO DOFS, THAT
                            //                        ACTS AS A unique_id. We could then store the indexes in the reduction, not getting in the way
                            //                        of the reduction, and then retrieve any relevant dof.
                            
                        }
                        
                        #ifndef NDEBUG
                        for (const auto& pair_in_shrunk : shrunk_list)
                            assert(pair_in_shrunk.first != ref_felt_space_ptr);
                        #endif // NDEBUG
                        
                        auto&& copy_ref_felt_space_ptr = std::make_unique<Internal::RefFEltNS::RefLocalFEltSpace>(*ref_felt_space_ptr);
                        
                        shrunk_list.push_back(std::make_pair(std::move(copy_ref_felt_space_ptr), std::move(buf)));
                    }
                    
                    felt_list_per_type.swap(shrunk_list);
                }
                
                
                
                void
                ComputeGhostNodeBearerList(const unsigned int mpi_rank,
                                           const LocalFEltSpacePerRefLocalFEltSpace& processor_wise_felt_list_per_type,
                                           NodeBearer::vector_shared_ptr& ghost_node_bearer_list)
                {
                    for (const auto& pair : processor_wise_felt_list_per_type)
                    {
                        const auto& local_felt_space_list = pair.second;
                        
                        for (const auto& local_felt_space_pair : local_felt_space_list)
                        {
                            const auto& local_felt_space_ptr = local_felt_space_pair.second;
                            
                            assert(!(!local_felt_space_ptr));
                            const auto& node_bearer_list_in_geometric_element = local_felt_space_ptr->GetNodeBearerList();
                            
                            // All node_bearers that are not on the current processor must be ghosted here.
                            for (const auto& node_bearer_ptr : node_bearer_list_in_geometric_element)
                            {
                                assert(!(!node_bearer_ptr));
                                
                                if (node_bearer_ptr->GetProcessor() != mpi_rank)
                                    ghost_node_bearer_list.push_back(node_bearer_ptr);
                           }
                        }
                    }
                }
                
                
            } // namespace anonymous
            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
