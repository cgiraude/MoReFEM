/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexList.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace FEltSpaceNS
        {
        
        
            DofProgramWiseIndexListPerVertexCoordIndexList
            ::DofProgramWiseIndexListPerVertexCoordIndexList(const unsigned int unique_id,
                                                         const FEltSpace& felt_space,
                                                         const NumberingSubset& numbering_subset)
            :  unique_id_parent(unique_id),
            felt_space_(felt_space),
            numbering_subset_(numbering_subset)
            {
                const auto& local_felt_space_storage = felt_space.GetLocalFEltSpacePerRefLocalFEltSpace();
                
                unsigned int Ndof = 0u;
                
                auto& dof_list_per_coord_list = GetNonCstDofProgramWiseIndexPerCoordIndexList();
                dof_list_per_coord_list.max_load_factor(Utilities::DefaultMaxLoadFactor());
                
                for (const auto& pair : local_felt_space_storage)
                {
                    const auto& local_felt_space_list = pair.second;
                    
                    for (const auto& local_felt_space_per_geom_elt : local_felt_space_list)
                    {
                        const auto& local_felt_space_ptr = local_felt_space_per_geom_elt.second;
                        
                        assert(!(!local_felt_space_ptr));
                        
                        const auto& local_felt_space = *local_felt_space_ptr;
                        
                        const auto& node_bearer_list = local_felt_space.GetNodeBearerList();
                        
                        for (const auto& node_bearer_ptr : node_bearer_list)
                        {
                            assert(!(!node_bearer_ptr));
                            const auto& node_bearer = *node_bearer_ptr;
                            
                            // Determine the index of the coords. Check whether it is in the matching list and whether it
                            // has already been handled.
                            const auto& vertex_coords_list = node_bearer.GetInterface().GetVertexCoordsList();
                            
                            std::vector<unsigned int> vertex_coords_index_list(vertex_coords_list.size());
                            
                            std::transform(vertex_coords_list.cbegin(),
                                           vertex_coords_list.cend(),
                                           vertex_coords_index_list.begin(),
                                           [](const auto& coords_ptr)
                                           {
                                               assert(!(!coords_ptr));
                                               return coords_ptr->GetIndex();
                                           });
                            
                            {
                                const auto it_coord = dof_list_per_coord_list.find(vertex_coords_index_list);
                                
                                if (it_coord != dof_list_per_coord_list.cend())
                                    continue;
                            }
                            
                            // Fill the list of related dof.
                            std::vector<unsigned int> index_list;
                            
                            const auto& node_list = node_bearer.GetNodeList();
                            
                            for (const auto& node_ptr : node_list)
                            {
                                assert(!(!node_ptr));
                                const auto& node = *node_ptr;
                                if (!node.DoBelongToNumberingSubset(numbering_subset))
                                    continue;
                                
                                const auto& dof_list = node.GetDofList();
                                
                                for (const auto& dof_ptr : dof_list)
                                {
                                    assert(!(!dof_ptr));
                                    const auto& dof = *dof_ptr;
                                    index_list.push_back(dof.GetProgramWiseIndex(numbering_subset));
                                    
                                    ++Ndof;
                                }
                            }
                            
                            if (!index_list.empty()) // might happen if \a NodeBearer bear no \a Node in the current
                                                     // \a NumberingSubset.
                            {
                                dof_list_per_coord_list.insert({ std::move(vertex_coords_index_list), std::move(index_list) });
                            }
                        }
                    }
                }

                Nprogram_wise_dof_ = Ndof;
            }
            
            
            const std::string& DofProgramWiseIndexListPerVertexCoordIndexList::ClassName()
            {
                static std::string ret("DofProgramWiseIndexListPerVertexCoordIndexList");
                return ret;
            }
            
            
            unsigned int DofProgramWiseIndexListPerVertexCoordIndexList::ComputeNprocessorWisedof() const
            {
                const auto& felt_space = GetFEltSpace();
                decltype(auto) complete_dof_list = felt_space.GetProcessorWiseDofList();
                decltype(auto) numbering_subset = GetNumberingSubset();
                
                unsigned int ret = 0u;
                
                for (const auto& dof_ptr : complete_dof_list)
                {
                    assert(!(!dof_ptr));
                    const auto& dof = *dof_ptr;
                    
                    if (dof.IsInNumberingSubset(numbering_subset))
                        ++ret;
                }
                
                return ret;
            }

            
            
        } // namespace FEltSpaceNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
