/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_MANAGER_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_MANAGER_HPP_

# include "Utilities/Singleton/Singleton.hpp"

# include "Core/InputData/Instances/Interpolator/InitVertexMatching.hpp"

# include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexList.hpp"


namespace MoReFEM
{



    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GodOfDof;
    class FEltSpace;
    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            /*!
             * \brief This class is used to create and retrieve \a DofProgramWiseIndexListPerVertexCoordIndexList objects.
             *
             * DofProgramWiseIndexListPerVertexCoordIndexList objects get private constructor and can only be created
             * through this class.
             */
            class DofProgramWiseIndexListPerVertexCoordIndexListManager
            : public Utilities::Singleton<DofProgramWiseIndexListPerVertexCoordIndexListManager>
            {

            public:

                /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
                static const std::string& ClassName();


                /*!
                 * \brief Base type of DofProgramWiseIndexListPerVertexCoordIndexList as input parameter.
                 */
                using input_data_type = ::MoReFEM::InputDataNS::BaseNS::InitVertexMatchingInterpolator;

            public:


                /*!
                 * \brief Create a new DofProgramWiseIndexListPerVertexCoordIndexList object from the data of the input
                 * parameter file.
                 *
                 * This method will be called from all GodOfDofs, but only the once that encompass relevant \a FEltSpace
                 * is allowed to proceed.
                 *
                 * \param[in] section Relevant section of the input data file (interpreted a as \a
                 * InputDataNS::BaseNS: InitVertexMatchingInterpolator).
                 * \param[in] god_of_dof God of dof for which the object might be created. It will be only if it matches
                 * the \a FEltSpace given in \a section.
                 *
                 * \tparam SectionT Type pf \a section.
                 */
                template<class SectionT>
                void Create(const SectionT& section,
                            const GodOfDof& god_of_dof);

                //! Fetch the object associated with \a unique_id unique identifier.
                //! \unique_id_param_in_accessor{DofProgramWiseIndexListPerVertexCoordIndexList}
                const DofProgramWiseIndexListPerVertexCoordIndexList&
                    GetDofProgramWiseIndexListPerVertexCoordIndexList(unsigned int unique_id) const;

            private:

                //! Access to the storage.
                const auto& GetStorage() const noexcept;

                //! Access to the storage.
                auto& GetNonCstStorage() noexcept;

                /*!
                 * \brief Create from a finite element space and a numbering subset.
                 *
                 * \param[in] unique_id Unique identifier to give to the \a DofProgramWiseIndexListPerVertexCoordIndexList
                 * object being created.
                 * \param[in] god_of_dof \a GodOfDof for which the object might be created (only if \a felt_space_unique_id
                 * belongs to this \a GodOfDof.
                 * \param[in] felt_space_unique_id Unique id of the \a FEltSpace for which
                 * \a DofProgramWiseIndexListPerVertexCoordIndexList is to be created.
                 * \param[in] numbering_subset_unique_id Unique id of the relevant \a NumberingSubset.
                 */
                void Create(unsigned int unique_id,
                            const GodOfDof& god_of_dof,
                            unsigned int felt_space_unique_id,
                            unsigned int numbering_subset_unique_id);


            private:


                //! \name Singleton requirements.
                ///@{
                
                //! Constructor.
                DofProgramWiseIndexListPerVertexCoordIndexListManager() = default;

                //! Destructor.
                virtual ~DofProgramWiseIndexListPerVertexCoordIndexListManager() override;

                //! Friendship declaration to Singleton template class (to enable call to constructor).
                friend class Utilities::Singleton<DofProgramWiseIndexListPerVertexCoordIndexListManager>;
                ///@}


            private:

                //! Store the god of dof objects by their unique identifier.
                std::unordered_map<unsigned int, DofProgramWiseIndexListPerVertexCoordIndexList::const_unique_ptr> list_;

            };


        } // namespace FEltSpaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElementSpace/Internal/DofProgramWiseIndexListPerVertexCoordIndexListManager.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_DOF_PROGRAM_WISE_INDEX_LIST_PER_VERTEX_COORD_INDEX_LIST_MANAGER_HPP_
