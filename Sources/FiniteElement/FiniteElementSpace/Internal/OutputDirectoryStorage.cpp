//! \file 
//
//
//  OutputDirectoryStorage.cpp
//  MoReFEM
//
//  Created by sebastien on 19/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include <sstream>
#include <regex>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Filesystem/Advanced/Directory.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/OutputDirectoryStorage.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{


    OutputDirectoryStorage::OutputDirectoryStorage(const std::string& output_directory,
                                                   const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                                                   wildcard_for_rank is_wildcard)
    : output_directory_(output_directory)
    {
        if (is_wildcard == wildcard_for_rank::yes)
        {
            std::regex re("Rank_[0-9]+");
            output_directory_ = std::regex_replace(output_directory_, re, "Rank_*");
        }
        else
        {
            if (!Advanced::FilesystemNS::DirectoryNS::DoExist(output_directory))
                throw Exception("Folder " + output_directory + " is expected to exist prior to this constructor call!",
                                __FILE__, __LINE__);
        }

        std::ostringstream oconv;
        output_directory_per_numbering_subset_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;
            const auto numbering_subset_id = numbering_subset.GetUniqueId();

            oconv.str("");
            oconv << output_directory_ << "/NumberingSubset_" << numbering_subset_id;

            auto check = output_directory_per_numbering_subset_.insert(std::make_pair(numbering_subset_id, oconv.str()));
            assert(check.second);
            static_cast<void>(check);
        }
    }


    const std::string& OutputDirectoryStorage
    ::GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto id = numbering_subset.GetUniqueId();

        decltype(auto) output_directory_per_numbering_subset = GetOutputDirectoryPerNumberingSubset();

        const auto it = output_directory_per_numbering_subset.find(id);

        assert(it != output_directory_per_numbering_subset.cend() &&
               "All relevant numbering subsets should have been loaded into the map.");

        return it->second;

    }


} // namespace MoReFEM::Internal::GodOfDofNS
