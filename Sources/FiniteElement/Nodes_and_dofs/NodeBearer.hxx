/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:06:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HXX_


namespace MoReFEM
{


    inline unsigned int NodeBearer::GetIndex() const noexcept
    {
        assert(index_ != NumericNS::UninitializedIndex<decltype(index_)>());
        return index_;
    }


    inline unsigned int NodeBearer::GetProcessor() const noexcept
    {
        return processor_;
    }


    inline bool operator<(const NodeBearer& lhs, const NodeBearer& rhs)
    {
        return lhs.GetIndex() < rhs.GetIndex();
    }


    inline bool operator==(const NodeBearer& lhs, const NodeBearer& rhs)
    {
        return lhs.GetIndex() == rhs.GetIndex();
    }


    inline InterfaceNS::Nature NodeBearer::GetNature() const noexcept
    {
        assert(!(!interface_));
        return interface_->GetNature();
    }


    inline const Interface& NodeBearer::GetInterface() const noexcept
    {
        assert(!(!interface_));
        return *interface_;
    }


    inline const Node::vector_shared_ptr& NodeBearer::GetNodeList() const noexcept
    {
        return node_list_;
    }


    inline DofNumberingScheme CurrentDofNumberingScheme()
    {
        return DofNumberingScheme::contiguous_per_node;
    }


    inline bool NodeBearer::IsEmpty() const noexcept
    {
        return node_list_.empty();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_NODE_BEARER_HXX_
