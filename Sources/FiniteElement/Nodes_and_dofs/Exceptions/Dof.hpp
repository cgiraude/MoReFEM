/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Sep 2013 15:14:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_EXCEPTIONS_x_DOF_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_EXCEPTIONS_x_DOF_HPP_


# include <map>
# include "Utilities/Exceptions/Exception.hpp"



namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Dof
        {


            //! Generic class
            struct Exception : public MoReFEM::Exception
            {
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] msg Message
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~Exception() override;

                //! \copydoc doxygen_hide_copy_constructor
                Exception(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                Exception(Exception&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                Exception& operator=(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                Exception& operator=(Exception&& rhs) = default;
            };



            //! When the number of unknowns read in the input file doesn't match the expected one.
            struct InvalidNumberOfUnknown : public Exception
            {
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] Nin_file Number of unknowns read in the input data file.
                 * \param[in] Nexpected Expected number of unknowns (cdetermined by the size of the tuple.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InvalidNumberOfUnknown(unsigned int Nin_file, unsigned int Nexpected,
                                                const char* invoking_file, int invoking_line);

                //! \copydoc doxygen_hide_copy_constructor
                InvalidNumberOfUnknown(const InvalidNumberOfUnknown& rhs) = default;

                //! Destructor
                virtual ~InvalidNumberOfUnknown();

            };



            //! When the same unknown is present at lest twice in the list in the input data file.
            struct DuplicatedUnknownInInputFile final : public Exception
            {
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] duplicated_unknown Unknown that is present twice.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit DuplicatedUnknownInInputFile(const std::string& duplicated_unknown,
                                                      const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~DuplicatedUnknownInInputFile();

                //! \copydoc doxygen_hide_copy_constructor
                DuplicatedUnknownInInputFile(const DuplicatedUnknownInInputFile& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                DuplicatedUnknownInInputFile(DuplicatedUnknownInInputFile&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                DuplicatedUnknownInInputFile& operator=(const DuplicatedUnknownInInputFile& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                DuplicatedUnknownInInputFile& operator=(DuplicatedUnknownInInputFile&& rhs) = default;

            };



            //! When the unknowns in input file don't match the ones expected in the tuple.
            struct InconsistentUnknownList final : public Exception
            {
                /*!
                 * \brief Constructor with simple message
                 *
                 * \param[in] input_file_unknown_list Unknowns read in the input file. Dismiss the value part:
                 * map is given there only because it is the container including the data when the exception was found.
                 * \param[in] unknown_list_in_tuple Unknowns expected in the tuple for the problem.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit InconsistentUnknownList(const std::map<std::string, unsigned int>& input_file_unknown_list,
                                                 const std::vector<std::string>& unknown_list_in_tuple,
                                                 const char* invoking_file, int invoking_line);

                //! Destructor
                virtual ~InconsistentUnknownList();

                //! \copydoc doxygen_hide_copy_constructor
                InconsistentUnknownList(const InconsistentUnknownList& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                InconsistentUnknownList(InconsistentUnknownList&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                InconsistentUnknownList& operator=(const InconsistentUnknownList& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                InconsistentUnknownList& operator=(InconsistentUnknownList&& rhs) = default;

            };


        } // namespace Dof


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup



#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_EXCEPTIONS_x_DOF_HPP_
