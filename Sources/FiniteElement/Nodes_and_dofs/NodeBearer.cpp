/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 14:06:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>

#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"


namespace MoReFEM
{

    
    NodeBearer::NodeBearer(const Interface::shared_ptr& interface)
    : index_(NumericNS::UninitializedIndex<decltype(index_)>()),
    processor_(NumericNS::UninitializedIndex<decltype(processor_)>()),
    interface_(interface)
    {
        assert(!(!interface));
    }


    Node::vector_shared_ptr NodeBearer
    ::GetNodeList(const Unknown& unknown,
                  const std::string& shape_function_label) const
    {
        const auto& node_list = GetNodeList();

        static_cast<void>(shape_function_label); // #1146 temporary!
        
        Node::vector_shared_ptr ret;
        // #1146    std::cout << "GetNodeList()" << std::endl;
        std::copy_if(node_list.cbegin(),
                     node_list.cend(),
                     std::back_inserter(ret),
                     [&unknown/*, &shape_function_label*/](const auto& node_ptr)
                     {
                         assert(!(!node_ptr));
                         return node_ptr->GetUnknown() == unknown;
                         // #1146 Temporary deactivated.
                                // && node_ptr->GetShapeFunctionLabel() == shape_function_label;
                     });
        
        return ret;
    }
    
      
    Node::shared_ptr NodeBearer::AddNode(const ExtendedUnknown& extended_unknown,
                                         const unsigned int Ndof)
    {
        assert(Ndof > 0);
        
        auto node_ptr = std::make_shared<Node>(shared_from_this(),
                                               extended_unknown,
                                               Ndof);
        
        node_list_.push_back(node_ptr);
        
        return node_ptr;
    }
    
    
    void NodeBearer::SetProcessor(unsigned int processor)
    {
        processor_ = processor;
    }
    
    
    bool NodeBearer::IsUnknown(const Unknown& unknown) const
    {
        const auto& node_list = GetNodeList();
        
        const auto end = node_list.cend();
        const auto it = std::find_if(node_list.cbegin(),
                                     end,
                                     [&unknown](const auto& node_ptr)
                                     {
                                         assert(!(!node_ptr));
                                         return node_ptr->GetUnknown() == unknown;
                                     });
        
        return it != end;
    }

    
    
    unsigned int NodeBearer::Ndof() const
    {
        const auto& node_list = GetNodeList();
        
        unsigned int ret = 0u;
        
        for (const auto& node_ptr : node_list)
        {
            assert(!(!node_ptr));
            ret += node_ptr->Ndof();
        }
        
        return ret;
    }
    
    
    unsigned int NodeBearer::Ndof(const NumberingSubset& numbering_subset) const
    {
        const auto& node_list = GetNodeList();
        
        unsigned int ret = 0u;
        
        for (const auto& node_ptr : node_list)
        {
            assert(!(!node_ptr));
            
            if (node_ptr->DoBelongToNumberingSubset(numbering_subset))
                ret += node_ptr->Ndof();
        }
        
        return ret;
    }
    
    
    void NodeBearer::SetIndex(std::size_t index)
    {
        index_ = static_cast<unsigned int>(index);
    }
    
    
    unsigned int NodeBearer::Nnode(const Unknown& unknown,
                                   const std::string& shape_function_label) const
    {
        const auto& node_list = GetNodeList();
        
        return static_cast<unsigned int>(std::count_if(node_list.cbegin(),
                                                       node_list.cend(),
                                                       [&unknown, &shape_function_label](const auto& node_ptr)
                                                       {
                                                           assert(!(!node_ptr));
                                                           const auto& node = *node_ptr;
                                                           return node.GetUnknown() == unknown
                                                           && node.GetShapeFunctionLabel() == shape_function_label;
                                                       }));
    }
    

    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
