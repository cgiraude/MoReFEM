/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 Apr 2016 23:42:27 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <sstream>
#include <vector>

#include "Utilities/Containers/Print.hpp"

#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace BoundaryConditionNS
        {
            
            
            ComponentManager::ComponentManager(const std::string& name,
                                               const std::bitset<3>& is_activated)
            : name_(name),
            is_activated_(is_activated)
            { }
                        
            
            std::string ComponentManager::AsString() const
            {
                if (!is_activated_.any())
                    return "NA";
                
                std::vector<char> buf;
                
                if (IsActive(0))
                    buf.push_back('x');
                
                if (IsActive(1))
                    buf.push_back('y');
                
                if (IsActive(2))
                    buf.push_back('z');
                
                std::ostringstream oconv;
                
                Utilities::PrintContainer<>::Do(buf, oconv, ", ", "", "");
                return oconv.str();
            }
            
            
            unsigned int ComponentManager::ActiveComponent(unsigned int i) const
            {
                unsigned int ret = 0;
                assert(i < is_activated_.count());
                
                for (unsigned int Nactive_read = 0 ; Nactive_read < i + 1; ++ret)
                {
                    if (IsActive(ret))
                        ++Nactive_read;
                }
                
                return ret - 1;
            }
            
            
        } // namespace BoundaryConditionNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
