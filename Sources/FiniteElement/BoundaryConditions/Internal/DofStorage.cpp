/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Apr 2016 13:48:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "Utilities/Containers/Print.hpp"

#include "FiniteElement/BoundaryConditions/Internal/DofStorage.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace BoundaryConditionNS
        {
            
            
            DofStorage
            ::DofStorage(const NumberingSubset& numbering_subset,
                         Utilities::PointerComparison::Map<Dof::shared_ptr, PetscScalar>&& value_per_index)
            {
                const auto size = value_per_index.size();
                program_wise_dof_index_list_.reserve(size);
                processor_wise_dof_index_list_.reserve(size);
                dof_value_list_.reserve(size);
                dof_list_.reserve(size);
                
                for (const auto& pair : value_per_index)
                {
                    const auto& dof_ptr = pair.first;
                    assert(!(!dof_ptr));
                    dof_list_.push_back(dof_ptr);
                    program_wise_dof_index_list_.push_back(static_cast<PetscInt>(dof_ptr->GetProgramWiseIndex(numbering_subset)));
                    processor_wise_dof_index_list_.push_back(dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset));
                    dof_value_list_.push_back(pair.second);
                }
            }
            
            
            void DofStorage::UpdateValueList(std::vector<PetscScalar>&& new_values)
            {
                assert(new_values.size() == dof_value_list_.size());
                std::swap(dof_value_list_, new_values);
            }
            
            
        } // namespace BoundaryConditionNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
