### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryConditionManager.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryCondition.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryConditionManager.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/DirichletBoundaryConditionManager.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
