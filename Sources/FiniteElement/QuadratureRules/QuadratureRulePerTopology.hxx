/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 May 2013 11:55:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HXX_


namespace MoReFEM
{


    inline const QuadratureRulePerTopology::storage_type& QuadratureRulePerTopology::GetRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }



} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_QUADRATURE_RULE_PER_TOPOLOGY_HXX_
