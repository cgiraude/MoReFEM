### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_FELT}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/QuadraturePoint.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadratureRule.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadratureRulePerTopology.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/EnumGaussQuadratureFormula.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GaussQuadratureFormula.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/GaussQuadratureFormula.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/QuadraturePoint.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadraturePoint.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/QuadratureRule.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadratureRule.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/QuadratureRulePerTopology.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/QuadratureRulePerTopology.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/Advanced/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instantiation/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
