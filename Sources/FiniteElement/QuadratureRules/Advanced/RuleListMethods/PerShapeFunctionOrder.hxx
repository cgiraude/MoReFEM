/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 25 Apr 2016 16:11:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_SHAPE_FUNCTION_ORDER_HXX_
# define MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_SHAPE_FUNCTION_ORDER_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace QuadratureRuleNS
        {


            template<class DerivedT>
            QuadratureRule::const_shared_ptr PerShapeFunctionOrder<DerivedT>
            ::GetRuleFromShapeFunctionOrder(unsigned int order)
            {
                // \todo #910 Fix more properly that!
                if (order == 0u)
                    ++order;

                auto& quadrature_rule_per_order_list = GetNonCstQuadratureRulePerOrderList();

                const auto it = quadrature_rule_per_order_list.find(order);

                if (it != quadrature_rule_per_order_list.cend())
                    return it->second;

                auto rule = DerivedT::GetShapeFunctionOrder(order);

                quadrature_rule_per_order_list.insert({order, rule});
                return rule;
            }


            template<class DerivedT>
            std::unordered_map<unsigned int, QuadratureRule::const_shared_ptr>&
            PerShapeFunctionOrder<DerivedT>::GetNonCstQuadratureRulePerOrderList()
            {
                static std::unordered_map<unsigned int, QuadratureRule::const_shared_ptr> ret;

                static bool first = true;

                if (first)
                {
                    first = false;
                    ret.max_load_factor(Utilities::DefaultMaxLoadFactor());
                }

                return ret;
            }


        } // namespace QuadratureRuleNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_QUADRATURE_RULES_x_ADVANCED_x_RULE_LIST_METHODS_x_PER_SHAPE_FUNCTION_ORDER_HXX_
