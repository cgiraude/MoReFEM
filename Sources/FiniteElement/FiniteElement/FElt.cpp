/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Apr 2014 15:48:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "Utilities/Containers/PointerComparison.hpp"

#include "FiniteElement/FiniteElement/FElt.hpp"


namespace MoReFEM
{


    FElt::FElt(const Internal::RefFEltNS::RefFEltInFEltSpace& ref_felt)
    : ref_felt_(ref_felt)
    { }

    
    void FElt::AddNode(const Node::shared_ptr& node_ptr)
    {
        assert(!(!node_ptr));
        node_list_.push_back(node_ptr);
    }
    

} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
