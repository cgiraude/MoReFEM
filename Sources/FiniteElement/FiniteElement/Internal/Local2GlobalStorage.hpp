/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 13:36:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_LOCAL2_GLOBAL_STORAGE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_LOCAL2_GLOBAL_STORAGE_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

# include "FiniteElement/Unknown/ExtendedUnknown.hpp"
# include "FiniteElement/FiniteElement/FElt.hpp"


namespace MoReFEM
{



    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class LocalFEltSpace;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FEltNS
        {


            /*!
             * \brief Storage of all relevant local2global related to a \a LocalFEltSpace and a \a NumberingSubset.
             *
             * Within a given LocalFEltSpace, different operators might have different needs: one might need for instance
             * the local2global related to only one unknown, and another the one related to several ones. The goal of
             * current class is to store all relevant possibilities and give quick access to them.
             *
             * As Local2Global array makes sense only within a given numbering subset, there is one such object for each
             * numbering subset.
             *
             * Similarly, Local2GlobalStorage might be used to store either processor-wise or program-wise local2global;
             * in practice in LocalFEltSpace there are two different \a Local2GlobalStorage objects for either case.
             */
            class Local2GlobalStorage
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = Local2GlobalStorage;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Friendship to the class able to call ComputeLocal2Global().
                friend class MoReFEM::LocalFEltSpace;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * Create here the list for the full numbering subset; partial lists are created on demand by a call
                 * to CreatePartialLocal2Global().
                 *
                 * \param[in] felt_list One or several finite element(s) considered by an operator, for which local2global
                 * must be reachable. They must share the same numbering subset (local2global would be meaningless for
                 * more than 1).
                 * \param[in] mpi_scale Whether we consider program-wise or processor-wise indexes.
                 */
                explicit Local2GlobalStorage(const FElt::vector_shared_ptr& felt_list,
                                             MpiScale mpi_scale);

                //! Destructor.
                ~Local2GlobalStorage() = default;

                //! \copydoc doxygen_hide_copy_constructor
                Local2GlobalStorage(const Local2GlobalStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Local2GlobalStorage(Local2GlobalStorage&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Local2GlobalStorage& operator=(const Local2GlobalStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Local2GlobalStorage& operator=(Local2GlobalStorage&& rhs) = delete;

                ///@}

                /*!
                 * \class doxygen_hide_local_2_global_storage_array
                 *
                 * It is assumed here it has already been computed with ComputeLocal2Global().
                 *
                 * \return Local -> global array.
                 */


                /*!
                 * \brief Get the local2global related to a list of unknowns, where local index is the position within the
                 * vector and global the value stored.
                 *
                 * \copydoc doxygen_hide_local_2_global_storage_array
                 *
                 * \param[in] unknown_list List of unknwowns for which local2global is required.
                 *
                 */
                const std::vector<PetscInt>& GetLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list) const;

                /*!
                 * \brief Get the local2global related to a single unknown, where local index is the position within the
                 * vector and global the value stored.
                 *
                 * \copydoc doxygen_hide_local_2_global_storage_array
                 *
                 * \param[in] unknown Unknown for which local2global is required.
                 */
                const std::vector<PetscInt>& GetLocal2Global(const ExtendedUnknown& unknown) const;


                /*!
                 * \brief Clear \a local_2_global_ and \a unknown_id_list_.
                 *
                 * This one is only required to build the others, which are required by operators. One each operator
                 * has built what it needs, there is no need to keep the first one, which might vbe quite heavy on memory.
                 *
                 */
                void Clear() noexcept;


            private:

                /*!
                 * \brief Compute Local2GLobal for a subset of the unknowns.
                 *
                 * This method is supposed to be called by each operator, that asks upon its creation that the
                 * local2global it will need are correctly built.
                 *
                 * \param[in] unknown_list List of unknwowns for which local2global is required.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method is const because actual local2global storage is mutable;
                 * the reason for such a choice is that it would have require to remove constness to GodOfDof and FEltSpace
                 * within a Model or a VariationalFormulation solely for the purpose of populating the local2global during
                 * the initialisation of the program.
                 * \endinternal
                 *
                 */
                void ComputeLocal2Global(const ExtendedUnknown::vector_const_shared_ptr& unknown_list) const;


            private:

                //! Constant access to local_2_global_.
                const std::vector<PetscInt>& GetLocal2Global() const noexcept;

                //! Non constant access to local_2_global_.
                std::vector<PetscInt>& GetNonCstLocal2Global() noexcept;

                //! Non constant access to unknown_id_list_.
                std::vector<unsigned int>& GetNonCstUnknownIdList() noexcept;

                //! Constant access to unknown_id_list_.
                const std::vector<unsigned int>& GetUnknownIdList() const noexcept;


                //! Access to the finite element list.
                const FElt::vector_shared_ptr& GetFEltList() const noexcept;

                //! Constant access to the local2global per unknown list.
                const std::vector<std::pair<std::vector<unsigned int>, std::vector<PetscInt>>>&
                GetLocal2GlobalPerUnknownIdList() const noexcept;

                //! Non constant access to the local2global per unknown list.
                std::vector<std::pair<std::vector<unsigned int>, std::vector<PetscInt>>>&
                GetNonCstLocal2GlobalPerUnknownIdList() const noexcept;


            private:

                /*!
                 * \brief Local2Global that covers a full numbering subset.
                 *
                 * This one is used to build \a local_2global_per_unknown_id_list_ but is not accessed directly.
                 */
                std::vector<PetscInt> local_2_global_;

                /*!
                 * \brief List of all unknowns related to the numbering subset.
                 *
                 * This list is given in the same order as the one used to build the \a local_2_global_.
                 *
                 * \internal <b><tt>[internal]</tt></b> As we are in a low-level class, indexes are used rather than
                 * shared_ptr.
                 * \endinternal
                 */
                std::vector<unsigned int> unknown_id_list_;

                /*!
                 * \brief Smaller local2global that encompass only a subset of the unknowns of the numbering subset.
                 *
                 * \internal <b><tt>[internal]</tt></b> This data attribute is populated during the construction of GlobalVariationalOperator:
                 * in the constructor they notify the finite element space of the Local2Global arrays they will need,
                 * and the finite element spaces delegate the call to present class to store it if not already known.
                 * The reason present attribute is mutable is that this call from the operators is the only one that
                 * requires to modify the FEltSpace. So the lesser evil is to make a low level attribute mutable rather
                 * than removing at several places const that are actually relevant for all operations save the computation
                 * of local2global.
                 * \endinternal
                 *
                 */
                mutable std::vector<std::pair<std::vector<unsigned int>, std::vector<PetscInt>>> local_2global_per_unknown_id_list_;


                /*!
                 * \brief Finite element list.
                 *
                 * There is one finite element per relevant unknown: a same unknown can be associated to only one
                 * numbering subset in a given (global) finite element space.
                 *
                 */
                FElt::vector_shared_ptr felt_list_;


                # ifndef NDEBUG
                /*!
                 * \brief Telles whether Clear() has already been called.
                 *
                 * Once all relevants local2global arrays have been built, some of the attributes (local_2_global_,
                 * unknown_id_list_) are cleared to free memory through Clear() method.
                 *
                 * Present data attribute helps to check it is done at most once and that creation operations don't occur
                 * once clearance is done.
                 *
                 */
                bool cleared_ = false;

                # endif // NDEBUG


            };


        } // namespace FEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_x_INTERNAL_x_LOCAL2_GLOBAL_STORAGE_HPP_
