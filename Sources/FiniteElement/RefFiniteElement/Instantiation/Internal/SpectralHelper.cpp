/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 29 Sep 2014 12:13:46 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <cassert>

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"


namespace MoReFEM
{

    
    namespace Internal
    {
        
        
        namespace RefFEltNS
        {
            
            
            double Interpolation(const std::vector<double>& pos, double p, unsigned int phi)
            {
                const std::size_t n = pos.size();
                double s = 1.;
                
                assert(static_cast<std::size_t>(phi) < pos.size());
                const double pos_phi = pos[static_cast<std::size_t>(phi)];
                
                for (std::size_t r = 0ul; r < n; ++r)
                {
                    if (r != phi)
                        s *= (p - pos[r]) / (pos_phi - pos[r]);
                }
                
                return s;
            }
            
            
            double DerivativeInterpolation(const std::vector<double>& pos, double p, size_t phi)
            {
                double c;
                double s = 0.;
                double d = 1.;
                std::size_t n = pos.size();
                
                for (std::size_t r = 0; r < n; ++r)
                {
                    if (r != phi)
                        d *= (pos[phi] - pos[r]);
                }
                
                for (std::size_t r1 = 0; r1 < n; ++r1)
                {
                    if (r1 != phi)
                    {
                        c = 1.;
                        for (std::size_t r2 = 0; r2 < n; ++r2)
                        {
                            if (r2 != r1 && r2 != phi)
                                c *= (p - pos[r2]);
                        }
                        
                        s += c;
                    }
                }
                return s / d;
            }
            
            
        } // namespace RefFEltNS
        
        
    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
