/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 14:28:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "Utilities/Numeric/Numeric.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TriangleP1Bubble.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace ShapeFunctionNS
        {
            
            
            namespace // anonymous
            {
                
                
                std::array<TriangleP1Bubble::shape_function_type, 4> ComputeShapeFunctionList();
                
                
                std::array<TriangleP1Bubble::shape_function_type, 8> ComputeGradShapeFunctionList();
                
                
            } // namespace anonymous
            
            
            
            const std::array<TriangleP1Bubble::shape_function_type, 4>& TriangleP1Bubble::ShapeFunctionList()
            {
                static auto ret = ComputeShapeFunctionList();
                
                return ret;
            };
            
            
            
            const std::array<TriangleP1Bubble::shape_function_type, 8>& TriangleP1Bubble
            ::FirstDerivateShapeFunctionList()
            {
                static auto ret = ComputeGradShapeFunctionList();
                
                return ret;
            }
            
            
            namespace // anonymous
            {
                
                
                std::array<TriangleP1Bubble::shape_function_type, 4> ComputeShapeFunctionList()
                {
                    return
                    {
                        {
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return 1. - r - s + 9. * r * s * (r + s - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return r + 9. * r * s * (r + s - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return s + 9. * r * s * (r + s - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return 27. * r * s * (1. - r - s);
                            }
                        }
                    };
                };
                
                
                std::array<TriangleP1Bubble::shape_function_type, 8> ComputeGradShapeFunctionList()
                {
                    return
                    {
                        {
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return - 1. + 9. * s * (2. * r + s - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return - 1. + 9. * r * (2. * s + r - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return 1. + 9. * s * (2. * r + s - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return 9. * r * (2. * s + r - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return 9. * s * (2. * r + s - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                const auto s = local_coords.s();
                                return 1. + 9. * r * (2. * s + r - 1.);
                            },
                            [](const auto& local_coords)
                            {
                                const auto s = local_coords.s();
                                return 27. * s * (1. - 2. * local_coords.r() - s);
                            },
                            [](const auto& local_coords)
                            {
                                const auto r = local_coords.r();
                                return 27. * r * (1. - 2. * local_coords.s() - r);
                            }
                        }
                    };
                    
                }
                
                
            } // namespace anonymous
            
            
        } // namespace ShapeFunctionNS
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
