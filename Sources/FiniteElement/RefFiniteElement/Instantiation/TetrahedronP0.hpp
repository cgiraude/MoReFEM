/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P0_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P0_HPP_

# include <memory>
# include <vector>

# include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"
# include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron4.hpp"

# include "FiniteElement/Nodes_and_dofs/LocalNode.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
# include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp"


namespace MoReFEM
{


    namespace RefFEltNS
    {


        /*!
         * \brief Reference finite element for TetrahedronP0.
         *
         * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
         * RefGeomEltNS::TopologyNS::Tetrahedron class.
         *
         * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
         */
        class TetrahedronP0 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Tetrahedron,
            Internal::ShapeFunctionNS::Order0,
            InterfaceNS::Nature::none,
            1u
        >
        {

        public:

            //! Name of the shape function used.
            static const std::string& ShapeFunctionLabel();


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            explicit TetrahedronP0() = default;

            //! Destructor.
            ~TetrahedronP0() override;

            //! \copydoc doxygen_hide_copy_constructor
            TetrahedronP0(const TetrahedronP0& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            TetrahedronP0(TetrahedronP0&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            TetrahedronP0& operator=(const TetrahedronP0& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            TetrahedronP0& operator=(TetrahedronP0&& rhs) = default;

            ///@}

        };



    } // namespace RefFEltNS


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_TETRAHEDRON_P0_HPP_
