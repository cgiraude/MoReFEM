/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 16:12:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_LOCAL_F_ELT_SPACE_HPP_
# define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_LOCAL_F_ELT_SPACE_HPP_

# include <memory>
# include <vector>

# include "Utilities/Containers/PointerComparison.hpp"
# include "Utilities/MatrixOrVector.hpp"

# include "Core/NumberingSubset/NumberingSubset.hpp"
# include "Core/Solver/Solver.hpp"

# include "Parameters/ParameterType.hpp"
# include "Parameters/Internal/Alias.hpp"

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"

# include "FiniteElement/Unknown/ExtendedUnknown.hpp"
# include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"
# include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;


    namespace Internal
    {


        namespace FEltSpaceNS
        {


            class CreateNodeListHelper;


        } // namespace FEltSpaceNS


        namespace LocalVariationalOperatorNS
        {


             class ElementaryDataImpl;


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal



    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            class SourceOrTargetData;


        } // namespace ConformInterpolatorNS


    } // namespace Advanced


    namespace ParameterNS
    {


        namespace Policy
        {


            template
            <
                ParameterNS::Type TypeT,
                unsigned int Ndim
            >
            class AtDof;


        } // namespace Policy


    } // namespace ParameterNS

    template
    <
        ParameterNS::Type TypeT,
        unsigned int Ndim
    >
    class ParamAtDof;


    template
    <
        class DerivedT,
        unsigned int SolverIndexT,
        enable_non_linear_solver NonLinearSolverT
    >
    class VariationalFormulation;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Internal
    {


        namespace RefFEltNS
        {



            /*!
             * \brief This class stores the list of all RefFElts that are related to a given RefGeomElt.
             *
             * Its major use is inside the FEltSpace: there will be one RefLocalFEltSpace per RefGeomElt. Within
             * current object, there is indeed a RefFEltInFEltSpace object for each Unknown considered in the FEltSpace.
             */
            class RefLocalFEltSpace final
            {

            public:

                //! Alias to unique pointer over a const object.
                using const_unique_ptr = std::unique_ptr<const RefLocalFEltSpace>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN


                //! Friendship to CreateNodeListHelper.
                friend FEltSpaceNS::CreateNodeListHelper;

                //! Friendship to ElementaryDataImpl.
                friend Internal::LocalVariationalOperatorNS::ElementaryDataImpl;

                //! Friendship.
                friend Advanced::ConformInterpolatorNS::SourceOrTargetData;

                //! Friendship to AtDof parameter policy.
                template
                <
                    ParameterNS::Type TypeT,
                    unsigned int Ndim
                >
                friend class ::MoReFEM::ParameterNS::Policy::AtDof;

                //! Friendship to AtDof parameter policy.
                template
                <
                    ParameterNS::Type TypeT,
                    unsigned int Ndim
                >
                friend class ::MoReFEM::ParamAtDof;


                //! Friendship.
                template
                <
                    class DerivedT,
                    unsigned int SolverIndexT,
                    enable_non_linear_solver NonLinearSolverT
                >
                friend class ::MoReFEM::VariationalFormulation;

                //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] ref_geom_elt Reference geometric element (e.g. 'Triangle3').
                 * \param[in] extended_unknown_list List of all unknowns consider in the FEltSpace. One
                 * \a RefFEltInFEltSpace object will be created per unknown.
                 * \param[in] mesh_dimension Highest dimension of the mesh onto which the finite element space is built.
                 * May be higher than \a felt_space_dimension.
                 * \param[in] felt_space_dimension Dimension considered in the finite element space into which new
                 * object is built.
                 */
                explicit RefLocalFEltSpace(const RefGeomElt::shared_ptr& ref_geom_elt,
                                      const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                      unsigned int mesh_dimension,
                                      unsigned int felt_space_dimension);


                //! Destructor.
                ~RefLocalFEltSpace() = default;

                //! \copydoc doxygen_hide_copy_constructor
                RefLocalFEltSpace(const RefLocalFEltSpace& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                RefLocalFEltSpace(RefLocalFEltSpace&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                RefLocalFEltSpace& operator=(const RefLocalFEltSpace& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                RefLocalFEltSpace& operator=(RefLocalFEltSpace&& rhs) = delete;

                ///@}

                //! Access to the related RefGeomElt.
                const RefGeomElt& GetRefGeomElt() const noexcept;

                //! Returns the number of nodes.
                unsigned int Nnode() const noexcept;

                //! Returns the number of dofs.
                unsigned int Ndof() const noexcept;

                //! Return the RefFEltInFEltSpace list regardless of the unknown.
                const RefFEltInFEltSpace::vector_const_shared_ptr& GetRefFEltList() const noexcept;

                //! Return the RefFEltInFEltSpace related to \a extended_unknown.
                //! \param[in] extended_unknown \a ExtendedUnknown for which reference finite element is sought.
                const RefFEltInFEltSpace& GetRefFElt(const ExtendedUnknown& extended_unknown) const;

            private:

                //! Return the RefFEltInFEltSpace related to \a unknown in \a felt_space.
                //! \param[in] felt_space \a FEltSpace in which the reference finite element is sought.
                //! \param[in] unknown \a Unknown for which reference finite element is sought.
                const RefFEltInFEltSpace& GetRefFElt(const FEltSpace& felt_space,
                                                     const Unknown& unknown) const;



            private:

                //! Related geometric element type. It is truly the type of the geometric elements in the mesh.
                const RefGeomElt::shared_ptr ref_geom_elt_;

                /*!
                 * \brief List of RefFEltInFEltSpace objects related to current ref_geom_elt_.
                 */
                RefFEltInFEltSpace::vector_const_shared_ptr ref_felt_list_;


            private:

                /// \name Cached data.
                ///@{

                //! Number of nodes in the finite element.
                unsigned int Nnode_;

                //! Number of dofs in the finite element.
                unsigned int Ndof_;

                ///@}

            };


        } // namespace RefFEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


# include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_LOCAL_F_ELT_SPACE_HPP_
