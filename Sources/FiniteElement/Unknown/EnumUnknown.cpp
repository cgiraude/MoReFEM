/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Mar 2015 16:50:27 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


# include <ostream>

# include "FiniteElement/Unknown/EnumUnknown.hpp"


namespace MoReFEM::UnknownNS
{

    
    std::ostream& operator<<(std::ostream& stream, const Nature nature)
    {
        switch(nature)
        {
            case Nature::scalar:
                stream << "scalar";
                break;
            case Nature::vectorial:
                stream << "vectorial";
                break;
        }
        
        return stream;
    }

    
} // namespace MoReFEM::UnknownNS


/// @} // addtogroup FiniteElementGroup

