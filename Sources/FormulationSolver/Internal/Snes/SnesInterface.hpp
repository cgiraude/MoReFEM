/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Oct 2016 14:26:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SNES_INTERFACE_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SNES_INTERFACE_HPP_

# include "ThirdParty/Wrappers/Petsc/Print.hpp"
# include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"
# include "ThirdParty/Wrappers/Petsc/SnesMacro.hpp"


namespace MoReFEM::Internal::SolverNS
{


    /*!
     * \brief Class that includes the most possible generic functions required by SNES.
     *
     * \tparam VariationalFormulationT Your \a VariationalFormulation in which a SolveNonLinear() call is issued.
     *
     * \attention A friendship should be written in \a VariationalFormulationT toward current class:
     * \code
     * friend struct Internal::SolverNS::SnesInterface<self>;
     * \endcode
     * where self is the internal alias toward the \a VariationalFormulationT.
     *
     * The prerequisites of a valid \a VariationalFormulationT are explained in the header of \a VariationalFormulation base class.
     */
    template<class VariationalFormulationT>
    struct SnesInterface
    {

    public:

        //! Constructor.
        SnesInterface() = default;

        //! Destructor.
        ~SnesInterface() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SnesInterface(const SnesInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor - deactivated.
        SnesInterface(SnesInterface&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SnesInterface& operator=(const SnesInterface& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SnesInterface& operator=(SnesInterface&& rhs) = delete;


    public:


        /*!
         * \brief This function is given to SNES to compute the function.
         *
         * \copydoc doxygen_hide_snes_interface_common_arg
         * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
         * \param[in,out] residual Petsc vector that include residual. It is not used per se in MoReFEM; there is
         * however a check in debug that VariationalFormulationT::GetSystemRhs() contains the same data as this
         * parameter.
         *
         *
         */
        static PetscErrorCode Function(SNES snes, Vec evaluation_state, Vec residual, void* context_as_void);


        /*!
         * \brief This function is supposed to be given to SNES to compute the jacobian.
         *
         * \copydoc doxygen_hide_snes_interface_common_arg
         * \param[in,out] evaluation_state Most recent value of the quantity the solver tries to compute.
         * \param[in,out] jacobian Jacobian matrix. Actually unused in our wrapper.
         * \param[in,out] preconditioner Preconditioner matrix. Actually unused in our wrapper.
         */
        static PetscErrorCode Jacobian(SNES snes, Vec evaluation_state, Mat jacobian, Mat preconditioner,
                                        void* context_as_void);


        /*!
         * \brief This function is used to monitor evolution of the convergence.
         *
         * Is is intended to be passed to SNESMonitorSet()
         *
         * \copydoc doxygen_hide_snes_interface_common_arg
         * \param[in] its Index of iteration.
         * \param[in] norm Current L^2 norm.
         */
        static PetscErrorCode Viewer(SNES snes, PetscInt its, PetscReal norm, void* context_as_void);


    };


} // namespace MoReFEM::Internal::SolverNS


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/Snes/SnesInterface.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SNES_INTERFACE_HPP_
