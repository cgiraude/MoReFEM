/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 16 Feb 2016 10:28:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HXX_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace FormulationSolverNS::Policy
        {


            template<ParameterNS::Type TypeT>
            LuaFunction<TypeT>::LuaFunction(storage_type lua_function)
            : lua_function_(lua_function)
            { }


            template<ParameterNS::Type TypeT>
            typename LuaFunction<TypeT>::return_type
            LuaFunction<TypeT>::GetValueFromPolicy(const SpatialPoint& coords) const
            {
                return lua_function_(coords[0],
                                     coords[1],
                                     coords[2]);
            }


            template<ParameterNS::Type TypeT>
            typename LuaFunction<TypeT>::return_type
            LuaFunction<TypeT>::GetAnyValueFromPolicy() const
            {
                return lua_function_(0., 0., 0.);
            }


            template<ParameterNS::Type TypeT>
            [[noreturn]] typename LuaFunction<TypeT>::return_type
            LuaFunction<TypeT>::GetConstantValueFromPolicy() const
            {
                assert(false && "A Lua function should yield IsConstant() = false!");
                exit(EXIT_FAILURE);
            }


            template<ParameterNS::Type TypeT>
            bool LuaFunction<TypeT>::IsConstant() const
            {
                return false;
            }

            template<ParameterNS::Type TypeT>
            void LuaFunction<TypeT>::WriteFromPolicy(std::ostream& out) const
            {
                out << "# Given by the Lua function given in the input data file." << std::endl;
            }


            template<ParameterNS::Type TypeT>
            inline SpatialPoint& LuaFunction<TypeT>::GetNonCstWorkCoords() const noexcept
            {
                return work_coords_;
            }


            template<ParameterNS::Type TypeT>
            inline void LuaFunction<TypeT>::SetConstantValue(double value)
            {
                static_cast<void>(value);
                assert(false);
                exit(EXIT_FAILURE);
            }


        } // namespace FormulationSolverNS::Policy


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_INITIAL_CONDITION_x_POLICY_x_LUA_FUNCTION_HXX_
