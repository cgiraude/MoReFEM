/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 10 Apr 2016 21:51:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_VECTOR_STORAGE_HPP_
# define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_VECTOR_STORAGE_HPP_

# include <memory>
# include <vector>

# include "Core/LinearAlgebra/GlobalVector.hpp"


namespace MoReFEM
{


    class GodOfDof;


    namespace Internal
    {


        namespace VarfNS
        {


            /*!
             * \brief Class in charge of storing global vectors.
             *
             * The storage is intended to store vectors which hold the same functionality for different numbering subsets.
             * For instance solutions vectors for different numbering subsets are expected to be stored in the same
             * GlobalVectorStorage, but if you also need to store rhs, another GlobalVectorStorage object should be created.
             *
             * \internal <b><tt>[internal]</tt></b> Actual storage use a 'flat-map': it is a STL vector upon which find
             * algorithm is used.
             * The reason for this is that we do not expect many global vectors stored, so flat map is likely to
             * be more efficient that std::map or even std::unordered_map.
             * The key for the search is the numbering subset that identify which dofs are used.
             * \endinternal
             */

            class GlobalVectorStorage final
            {

            public:

                //! Alias to unique pointer.
                using const_unique_ptr = std::unique_ptr<const GlobalVectorStorage>;

                //! Alias to vector of unique pointers.
                using vector_const_unique_ptr = std::vector<const_unique_ptr> ;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit GlobalVectorStorage() = default;

                //! Destructor.
                ~GlobalVectorStorage() = default;

                //! \copydoc doxygen_hide_copy_constructor
                GlobalVectorStorage(const GlobalVectorStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                GlobalVectorStorage(GlobalVectorStorage&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                GlobalVectorStorage& operator=(const GlobalVectorStorage& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                GlobalVectorStorage& operator=(GlobalVectorStorage&& rhs) = delete;

                ///@}


                /*!
                 * \brief Add the vector described by the given numbering subset.
                 *
                 * There can be only one for a given numbering subset.
                 *
                 * \copydetails doxygen_hide_vector_numbering_subset_arg
                 * \param[in] god_of_dof \a GodOfDof onto which the new vector will be allocated.
                 *
                 * \return A reference to the newly created vector.
                */
                GlobalVector& NewVector(const GodOfDof& god_of_dof,
                                        const NumberingSubset& numbering_subset);


                //! Access to the global vector matching the given numbering subset.
                //! \copydetails doxygen_hide_vector_numbering_subset_arg
                const GlobalVector& GetVector(const NumberingSubset& numbering_subset) const;

                //! Non constant access to the global vector matching the given numbering subset.
                //! \copydetails doxygen_hide_vector_numbering_subset_arg
                GlobalVector& GetNonCstVector(const NumberingSubset& numbering_subset);


            private:

                //! Access to the internal storage.
                const GlobalVector::vector_unique_ptr& GetStorage() const;

                //! Non constant access to the internal storage.
                GlobalVector::vector_unique_ptr& GetNonCstStorage();

                # ifndef NDEBUG
                //! Check \a NewMatrix() is called at most once for each pair of numbering subsets.
                void AssertNoDuplicate() const;
                # endif // NDEBUG

            private:

                //! List of all global matrices with the numbering subsets used to find them.
                GlobalVector::vector_unique_ptr storage_;


            };


        } // namespace VarfNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup FormulationSolverGroup


# include "FormulationSolver/Internal/Storage/GlobalVectorStorage.hxx"


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_STORAGE_x_GLOBAL_VECTOR_STORAGE_HPP_
