/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 09:45:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <set>

#include "Utilities/String/String.hpp"
#include "Utilities/OutputFormat/ReadBinaryFile.hpp"
#include "Utilities/Filesystem/Internal/GetRankDirectory.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "PostProcessing/PostProcessing.hpp"
#include "PostProcessing/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {
        
        
        PostProcessing::PostProcessing(const FilesystemNS::Directory& data_directory,
                                       const std::vector<unsigned int>& numbering_subset_id_list,
                                       const Mesh& mesh)
        : data_directory_(data_directory),
        mesh_(mesh),
        Nprocessor_(NumericNS::UninitializedIndex<std::size_t>()),
        interface_file_(nullptr),
        unknown_file_(nullptr),
        time_iteration_file_(nullptr),
        numbering_subset_id_list_(numbering_subset_id_list)
        {
            data_directory_.SetBehaviour(FilesystemNS::behaviour::read);
            
            // Read the different files.
            ReadInterfaceFile();
            
            //! Read the unknown file and store its content into the class.
            ReadUnknownFile();
            
            //! Read the time iteration file and store its content into the class.
            ReadTimeIterationFile();
            
            //! Read how many processors were involved in the simulation.
            ReadNprocessor();
            
            //! Read the dof files (one per processor) and store its content into the class.
            ReadDofFiles();
        }
        
        
        std::vector<double> LoadVector(const std::string& file)
        {
            std::vector<double> ret;

            const auto binary_or_ascii_choice =
                Utilities::OutputFormat::GetInstance(__FILE__, __LINE__).IsBinaryOutput();

            switch(binary_or_ascii_choice)
            {
                case binary_or_ascii::binary:
                {
                    return Advanced::ReadSimpleBinaryFile(file, __FILE__, __LINE__);
                }
                case binary_or_ascii::ascii:
                {
                    std::ifstream stream;
                    FilesystemNS::File::Read(stream, file, __FILE__, __LINE__);

                    std::string line;

                    while(getline(stream, line))
                        ret.push_back(std::stod(line));
                    break;
                }
                case binary_or_ascii::from_input_data:
                {
                    assert(false && "Should not happen!");
                    exit(EXIT_FAILURE);
                }
            }

            return ret;
        }
        
                
        void PostProcessing::ReadInterfaceFile()
        {
            assert(GetDataDirectory().GetBehaviour() == FilesystemNS::behaviour::read);
            FilesystemNS::Directory mesh_dir (GetDataDirectory(),
                                              "Mesh_" + std::to_string(GetMesh().GetUniqueId()),
                                              __FILE__, __LINE__,
                                              std::make_unique<FilesystemNS::behaviour>(FilesystemNS::behaviour::read));

            std::string input_file = mesh_dir.AddFile("interfaces.hhdata");
            
            assert(!interface_file_);
            interface_file_ = std::make_unique<InterfaceFile>(input_file);
        }
        
        
        void PostProcessing::ReadUnknownFile()
        {
            std::string input_file(GetDataDirectory());
            input_file += "/unknowns.hhdata";
     
            assert(!unknown_file_);
            unknown_file_ = std::make_unique<UnknownInformationFile>(input_file);
        }
        
        
        void PostProcessing::ReadTimeIterationFile()
        {
            decltype(auto) data_dir = GetDataDirectory();

            // First check whether a 'filtered_time_iteration.hhdata' is present (it would have been generated through
            // the PostProcessing/FilterTimeIteration script). If found, use it.
            std::string input_file = data_dir.AddFile("filtered_time_iteration.hhdata");

            assert(!time_iteration_file_);

            if (FilesystemNS::File::DoExist(input_file))
                std::cout << "[Time iterations] Using the filtered_time_iteration.hhdata file." << std::endl;
            else
                input_file = data_dir.AddFile("time_iteration.hhdata");

            time_iteration_file_ = std::make_unique<TimeIterationFile>(input_file);
        }
        
        
        void PostProcessing::ReadNprocessor()
        {
            assert(Nprocessor_ == NumericNS::UninitializedIndex<std::size_t>());
            std::ifstream in;
            
            FilesystemNS::File::Read(in, GetDataDirectory().AddFile("mpi.hhdata"), __FILE__, __LINE__);
            
            std::string line;
            getline(in, line);
            
            const std::string sequence("Nprocessor:");
            
            if (!Utilities::String::StartsWith(line, sequence))
                throw ExceptionNS::InvalidFormatInLine(line, __FILE__, __LINE__);
            
            line.erase(0, sequence.size());
            
            Nprocessor_ = std::stoul(line);
        }
        
        
        void PostProcessing::ReadDofFiles()
        {
            DofInformationFile::vector_unique_ptr dof_information_file_list_for_numbering_subset;
                
            const std::size_t Nproc = Nprocessor();
            dof_information_file_list_for_numbering_subset.reserve(Nproc);
            
            assert(Ndof_ == 0u);
            
            const auto& interface_file = GetInterfaceData();
            
            decltype(auto) numbering_subset_id_list = GetNumberingSubsetIdList();
            
            auto set_numbering_subset_id_list = std::set<unsigned int>(numbering_subset_id_list.cbegin(),
                                                                       numbering_subset_id_list.cend());
            
            for (const auto& numbering_subset_id : set_numbering_subset_id_list)
            {
                std::ostringstream oconv;

                for (std::size_t rank = 0; rank < Nproc; ++rank)
                {
                    decltype(auto) numbering_subset_directory = GetNumberingSubsetDirectory(static_cast<unsigned int>(rank),
                                                                                            numbering_subset_id);

                    const auto dof_infos_data_file = numbering_subset_directory.AddFile("dof_information.hhdata");

                    auto&& dof_list_ptr= std::make_unique<DofInformationFile>(rank,
                                                                              dof_infos_data_file,
                                                                              interface_file);
                    Ndof_ += dof_list_ptr->Ndof();
                    
                    dof_information_file_list_for_numbering_subset.emplace_back(std::move(dof_list_ptr));
                }

                assert(dof_information_file_list_for_numbering_subset.size() == Nproc);
                
                using pair_type = std::pair<unsigned int, DofInformationFile::vector_unique_ptr>;
                
                pair_type&& pair = { numbering_subset_id, std::move(dof_information_file_list_for_numbering_subset) };
                
                dof_file_per_processor_list_per_numbering_subset_.insert(std::move(pair));
            }
            
        }


        const DofInformationFile& PostProcessing::GetDofFile(const unsigned int numbering_subset_id,
                                                             const std::size_t processor) const
        {
            assert(processor < Nprocessor());
            
            auto it = dof_file_per_processor_list_per_numbering_subset_.find(numbering_subset_id);
            assert(it != dof_file_per_processor_list_per_numbering_subset_.cend());
            
            const auto& dof_file_per_processor_list = it->second;
            
            assert(Nprocessor() == dof_file_per_processor_list.size());
            return *dof_file_per_processor_list[processor];
        }
        
        
        FilesystemNS::Directory PostProcessing::GetNumberingSubsetDirectory(const unsigned int rank,
                                                                            const unsigned int numbering_subset_id) const
        {
            decltype(auto) data_directory = GetDataDirectory();

            FilesystemNS::Directory rank_data_directory =
                Internal::FilesystemNS::GetRankDirectory(data_directory, rank, __FILE__, __LINE__);

            FilesystemNS::Directory mesh_dir(rank_data_directory,
                                             "Mesh_" + std::to_string(GetMesh().GetUniqueId()),
                                             __FILE__, __LINE__);

            FilesystemNS::Directory ret(mesh_dir,
                                        "NumberingSubset_" + std::to_string(numbering_subset_id),
                                        __FILE__, __LINE__);

            return ret;
        }
       
        
    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
