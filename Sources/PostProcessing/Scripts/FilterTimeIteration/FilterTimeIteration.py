import os
import decimal


class FilterTimeIteration:
    '''
    By default, all the time iterations run at some point during a model are post-processed.
    
    However, that is not always the desired outcome: for instance, in some models such as CardiacMechanics smaller time steps may be used when computation can't deal with the prescribed time step, but these steps aren't relevant for Ensigtht output for instance.
    
    The purpose of current script is to filter out the time steps that are not requested. 
    
    Considering the current need, we're doing it rather crudely here: the filtering assumes what is wanted is time iterations separated by a current time step.
    
    The script is not subtle at all: i
    
    At some point, this functionality may be introduced in the C++ code directly; however current script is a faster way to solve the issue currently at hand.
    '''

    __slots__ = ('_time_sequence', '_epsilon')

    def __init__(self,
                 directory,
                 initial_time,
                 time_step,
                 final_time,
                 epsilon=1.e-6):
        '''
        @param directory Output directory in which the result data of a MoReFEM run are stored. This directory is expected to include directories "Mesh_*". In each of this directory, the file time_iteration.hhdata will be analysed and a new one named filtered_time_iteration.hhdata will be generated. Post processing code first looks for such a filtered file, and if none is found then takes the original time_iteration.hhdata. In other words, once the script has been run just run post processing and the only the relevant time steps will be kept.
        @param initial_time In seconds.
        @param time_step In seconds.    
        @param final_time Final time considered (included), in seconds.
        @param epsilon Absolute difference acceptable to match a prescribed time and what was read in the file.
        '''
        initial_time = decimal.Decimal(initial_time)
        final_time = decimal.Decimal(final_time + epsilon)
        time_step = decimal.Decimal(time_step)

        time = initial_time

        self._time_sequence = []

        while time <= final_time:
            self._time_sequence.append(float(time))
            time += time_step

        if not self._time_sequence:
            raise ValueError(
                "Times given were wrong: no iteration wou;d be selected with current criteria!"
            )

        self._epsilon = epsilon

        if not os.path.isdir(directory):
            raise NotADirectoryError(directory)

        mesh_directory_list = list(
            os.path.join(directory, item) for item in os.listdir(directory)
            if os.path.isdir(os.path.join(directory, item))
            and item.startswith("Mesh_"))

        if not mesh_directory_list:
            raise ValueError(
                "No Mesh_* directory was found inside the given directory (namely {})".
                format(directory))

        for mesh_directory in mesh_directory_list:
            self.__Process(mesh_directory)

    def __Process(self, mesh_directory):
        """Actually process the time iteration file to keep only relevant time iterations. A file named 'filtered_time_iteration.hhdata' will be generated within the \a mesh_directory.
        
        @param mesh_directory Mesh directory into which the file time_iteration.hhdata to process is expected to be located.
        """
        assert (os.path.isdir(mesh_directory))

        time_iteration_file = os.path.join(mesh_directory,
                                           "time_iteration.hhdata")

        if not os.path.isfile(time_iteration_file):
            raise FileNotFoundError(time_iteration_file)

        filtered_time_iteration_file = os.path.join(
            mesh_directory, "filtered_time_iteration.hhdata")

        FILE_in = open(time_iteration_file)
        FILE_out = open(filtered_time_iteration_file, 'w')

        # Take the first line with comments
        FILE_out.write(FILE_in.readline())

        time_sequence = self._time_sequence
        index_in_time_seq = 0
        Nelt_in_seq_minus_one = len(time_sequence) - 1
        
        Nline_in_input_file = 0
        Nline_in_output_file = 0
        
        # We assume here the file is correctly ordered in time.
        for line in FILE_in:
            
            Nline_in_input_file += 1
            
            splitted = line.split(";")
            assert (len(splitted) >= 4)

            time_read = (float)(splitted[1])

            while index_in_time_seq < Nelt_in_seq_minus_one and time_read > time_sequence[index_in_time_seq]:
                index_in_time_seq += 1

            if abs(time_read - time_sequence[index_in_time_seq]
                   ) < self._epsilon:
                Nline_in_output_file += 1
                FILE_out.write(line)
                
        print("File {} was generated: {} lines out of the {} in the original file were written.".format(filtered_time_iteration_file,
                                                                                Nline_in_output_file,
                                                                                Nline_in_input_file))


if __name__ == "__main__":
    FilterTimeIteration(directory="/Volumes/Data/sebastien/MoReFEM/Results/Elasticity/3D",
                        initial_time=0.,
                        time_step=0.2,
                        final_time=0.5,
                        epsilon=1.e-6)
