/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Dec 2014 16:05:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <sstream>

#include "PostProcessing/Exceptions/Exception.hpp"


namespace // anonymous
{
    
    
    std::string InvalidFormatInFileMsg(const std::string& file, const std::string& description);
    
    
    std::string InvalidFormatInLineMsg(const std::string& line);
    

    std::string InvalidFormatInLineMsg(const std::string& line, const std::string& description);
    
    
} // namespace anonymous



namespace MoReFEM
{


    namespace PostProcessingNS
    {
        
        
        namespace ExceptionNS
        {
            
            
            InvalidFormatInFile::~InvalidFormatInFile() = default;
            
            
            InvalidFormatInFile::InvalidFormatInFile(const std::string& file, const std::string& description,
                                                     const char* invoking_file, int invoking_line)
            : ::MoReFEM::Exception(InvalidFormatInFileMsg(file, description)
                                      , invoking_file, invoking_line)
            { }
            
            
            
            InvalidFormatInLine::~InvalidFormatInLine() = default;
            
            
            InvalidFormatInLine::InvalidFormatInLine(const std::string& line, const char* invoking_file, int invoking_line)
            : ::MoReFEM::Exception(InvalidFormatInLineMsg(line), invoking_file, invoking_line)
            { }
            
            
            InvalidFormatInLine::InvalidFormatInLine(const std::string& line, const std::string& description,
                                                     const char* invoking_file, int invoking_line)
            : ::MoReFEM::Exception(InvalidFormatInLineMsg(line, description),
                                      invoking_file, invoking_line)
            { }
            

          
            
        } // namespace ExceptionNS
        
        
    } // namespace PostProcessingNS
    

} // namespace MoReFEM



namespace // anonymous
{
    
    
    std::string InvalidFormatInFileMsg(const std::string& file, const std::string& description)
    {
        std::ostringstream oconv;
        oconv << "Invalid file (" << file << "): " << description << '.';
        
        return oconv.str();
    }
    
    
    
    std::string InvalidFormatInLineMsg(const std::string& line)
    {
        std::ostringstream oconv;
        oconv << "Invalid format in " << line;
        
        return oconv.str();
    }
    
    
    std::string InvalidFormatInLineMsg(const std::string& line, const std::string& description)
    {
        std::ostringstream oconv;
        oconv << InvalidFormatInLineMsg(line) << ": " << description;
        
        return oconv.str();
    }

    
    
} // namespace anonymous


/// @} // addtogroup PostProcessingGroup

