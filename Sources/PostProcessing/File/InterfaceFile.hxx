/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 14:46:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HXX_
# define MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HXX_


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        template<InterfaceNS::Nature NatureT>
        unsigned int InterfaceFile::Ninterface() const
        {
            auto it = content_.find(NatureT);

            if (it == content_.cend())
                return 0u;

            return static_cast<unsigned int>(it->second.size());
        }


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HXX_
