/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 14:46:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HPP_
# define MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HPP_

# include <map>
# include <vector>
# include <string>

# include "Geometry/Interfaces/EnumInterface.hpp"
# include "Geometry/Coords/Coords.hpp"

# include "PostProcessing/Data/Interface.hpp"


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        /*!
         * \brief Class which holds the informations obtained from interfaces.hhdata output file.
         *
         * There is one such file per mesh.
         *
         * This file gives for each interface (vertex, edge, face or volume)  the list of \a Coords that delimit it.
         *
         * \attention Some of the methods of this class were implemented for debug purposes, and are not necessarily
         * very efficient (noticeably accessors to interface). If they are to be used extensively in some post
         * processing informations, there are many ways to improve them tremendously.
         */
        class InterfaceFile final
        {

        public:

            //! Alias to unique_ptr.
            using const_unique_ptr = std::unique_ptr<const InterfaceFile>;


        public:

            /// \name Special members.
            ///@{

            //! Constructor.
            //! \param[in] input_file FIle which includes the interfaces considered; should be named "interfaces.hhdata"
            //! and be positioned inside a mesh-related folder.
            explicit InterfaceFile(const std::string& input_file);

            //! Destructor.
            ~InterfaceFile() = default;

            //! \copydoc doxygen_hide_copy_constructor
            InterfaceFile(const InterfaceFile& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            InterfaceFile(InterfaceFile&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            InterfaceFile& operator=(const InterfaceFile& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            InterfaceFile& operator=(InterfaceFile&& rhs) = delete;

            ///@}


            //! Return the number of a given type of interface.
            template<InterfaceNS::Nature NatureT>
            unsigned int Ninterface() const;

            //! Return the adequate interface.
            //! \param[in] nature Nature of the interface used as filter.
            //! \param[in] index Position in the local vector of the interface sought (value in [0, Ninterface[)
            const Data::Interface& GetInterface(InterfaceNS::Nature nature,
                                                unsigned int index) const;

            //! Return the vertex interface that matches a given coord_index.
            //! \param[in] coord_index Index of the \a Coord related to the sought interface.
            const Data::Interface& GetVertexInterfaceFromCoordIndex(unsigned int coord_index) const;

            //! Return the face interface that matches a list of coords.
            //! \param[in] coords_list List of \a Coords that delimit the sought face interface.
            const Data::Interface& GetFaceInterfaceFromCoordsList(const Coords::vector_raw_ptr& coords_list) const;


        private:

            //! Return the list of interfaces of \a nature.
            //! \param[in] nature Nature of the interface used as filter.
            const Data::Interface::vector_unique_ptr& GetInterfaceList(InterfaceNS::Nature nature) const;


        private:

            /*!
             * \brief Content of the interface file.
             *
             * Key is the nature of the interface.
             * Value is for each node the list of Coords that delimit it.
             */
            std::map<InterfaceNS::Nature, Data::Interface::vector_unique_ptr> content_;

        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/File/InterfaceFile.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_INTERFACE_FILE_HPP_
