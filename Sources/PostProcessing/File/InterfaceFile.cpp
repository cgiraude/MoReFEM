/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 14:46:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <cassert>
#include <sstream>
#include <memory>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Containers/Print.hpp"

#include "PostProcessing/File/InterfaceFile.hpp"


namespace MoReFEM
{
    
    
    namespace PostProcessingNS
    {

    
        InterfaceFile::InterfaceFile(const std::string& input_file)
        {
            std::ifstream interface_stream;
            
            FilesystemNS::File::Read(interface_stream, input_file, __FILE__, __LINE__);
            
            std::string line;
            
            // The file is written so that vertices come first, then edges, faces and volume.
            InterfaceNS::Nature current_nature(InterfaceNS::Nature::vertex);
            
            Data::Interface::vector_unique_ptr current_interface_content;
            
            unsigned int counter_in_interface = 0u; // a quantity there only to check file format is the expected one.
            
            using difference_type = std::string::iterator::difference_type;
            
            while (getline(interface_stream, line))
            {
                // Ignore comment lines.
                if (Utilities::String::StartsWith(line, "#"))
                    continue;
                
                auto begin = line.cbegin();
                
                // The format of each line should be: *Interface* *n*;[*list of vertices defining the interface*]
                
                // Identify the first space. The word before must match an interface.
                {
                    const auto space_pos = line.find(' ');
                    
                    if (space_pos == std::string::npos)
                        throw Exception("Invalid format in line " + line + ": no space found!", __FILE__, __LINE__);
                    
                    std::string interface_name(line, 0, space_pos);
                    InterfaceNS::Nature interface_nature = InterfaceNS::GetNature(interface_name);
                    
                    if (interface_nature != current_nature)
                    {
                        assert(interface_nature > current_nature);
                        content_.emplace(std::make_pair(current_nature, std::move(current_interface_content)));
                        current_nature = interface_nature;
                        current_interface_content.clear();
                        counter_in_interface = 0u;
                    }
                    
                    const auto semicolon_pos = line.find(';', space_pos + 1);
                    
                    if (semicolon_pos == std::string::npos)
                        throw Exception("Invalid format in line " + line + ": no semicolon found!", __FILE__, __LINE__);
                    
                    std::string str_interface_index(begin + static_cast<difference_type>(space_pos + 1ul),
                                                    begin + static_cast<difference_type>(semicolon_pos));
                    
                    const unsigned int interface_index = static_cast<unsigned int>(std::stoi(str_interface_index));
                    
                    if (counter_in_interface++ != interface_index)
                        throw Exception("Invalid format in line " + line + ": interface aren't ordered per increasing "
                                        "index and type.", __FILE__, __LINE__);
                    
                    const auto left_bracket_pos = line.find('[', semicolon_pos + 1);
                    
                    if (left_bracket_pos == std::string::npos)
                        throw Exception("Invalid format in line " + line + ": no [ found!", __FILE__, __LINE__);
                    
                    const auto right_bracket_pos = line.find(']', left_bracket_pos + 1);
                    
                    if (right_bracket_pos == std::string::npos)
                        throw Exception("Invalid format in line " + line + ": no ] found!", __FILE__, __LINE__);
                    
                    std::string str_coord_index(begin + static_cast<difference_type>(left_bracket_pos + 1ul),
                                                begin + static_cast<difference_type>(right_bracket_pos));
                    
                    std::vector<unsigned int> coords_on_vertex_in_interface;
                    {
                        std::istringstream iconv(str_coord_index);
                        unsigned int buf;
                        
                        while (!iconv.eof())
                        {
                            iconv >> buf;
                            
                            coords_on_vertex_in_interface.push_back(buf);
                            
                            if (iconv.fail())
                                throw Exception("Invalid format in line " + line + ": Coords were not correctly read.",
                                                __FILE__, __LINE__);
                            
                            iconv.ignore(); // ignore the separating ',' or the final '/'.
                            
                        }
                    }
                    
                    auto interface = std::make_unique<Data::Interface>(current_nature,
                                                                       interface_index,
                                                                       std::move(coords_on_vertex_in_interface));
                    current_interface_content.emplace_back(std::move(interface));
                }
            }
            
            
            if (!current_interface_content.empty())
                content_.emplace(std::make_pair(current_nature, std::move(current_interface_content)));
        }
        
        
        
        const Data::Interface::vector_unique_ptr& InterfaceFile
        ::GetInterfaceList(InterfaceNS::Nature interface_nature) const
        {
            auto it = content_.find(interface_nature);
            assert(it != content_.cend());
            return it->second;
        }
        
        
        const Data::Interface& InterfaceFile::GetInterface(InterfaceNS::Nature interface_nature,
                                                           unsigned int interface_index) const
        {

            decltype(auto) interface_list = GetInterfaceList(interface_nature);

            auto lambda = [interface_index](const auto& interface_ptr)
            {
                assert(!(!interface_ptr));
                return interface_index == interface_ptr->GetIndex();
            };

            auto guess_min = interface_list.cbegin() + interface_index - 3;

            if (guess_min <  interface_list.cbegin())
                guess_min = interface_list.cbegin();

            auto guess_max = guess_min + 6;

            if (guess_max > interface_list.cend())
                guess_max = interface_list.cend();

            auto it_interface = std::find_if(guess_min,
                                             guess_max,
                                             lambda);

            if (it_interface == guess_max)
            {
                it_interface = std::find_if(interface_list.cbegin(),
                                            interface_list.cend(),
                                            lambda);
            }
            
            assert(it_interface != interface_list.cend());
            return *(*it_interface);
        }
        
        
        const Data::Interface& InterfaceFile::GetVertexInterfaceFromCoordIndex(unsigned int coords_index) const
        {
            decltype(auto) interface_list = GetInterfaceList(InterfaceNS::Nature::vertex);
            const auto end_interface_list = interface_list.cend();
            
            const auto it_interface = std::find_if(interface_list.cbegin(),
                                                   end_interface_list,
                                                   [coords_index](const auto& interface_ptr)
                                                   {
                                                       assert(!(!interface_ptr));
                                                       decltype(auto) coords = interface_ptr->GetVertexCoordsIndexList();
                                                       assert(coords.size() == 1);
                                                       decltype(auto) current_coords_index = coords.back();
                                                       return current_coords_index == coords_index;
                                                   });
            
            assert(it_interface != end_interface_list);
            return *(*it_interface);
        }
        
        
        const Data::Interface& InterfaceFile
        ::GetFaceInterfaceFromCoordsList(const Coords::vector_raw_ptr& coords_list) const
        {
            decltype(auto) interface_list = GetInterfaceList(InterfaceNS::Nature::face);
            const auto end_interface_list = interface_list.cend();
            
            std::vector<unsigned int> coords_index_list(coords_list.size());
            
            std::transform(coords_list.cbegin(),
                           coords_list.cend(),
                           coords_index_list.begin(),
                           [](const auto& coords_ptr)
                           {
                               assert(!(!coords_ptr));
                               return coords_ptr->template GetPositionInCoordsListInMesh<MpiScale::processor_wise>();
                           });
            
            std::sort(coords_index_list.begin(), coords_index_list.end());
            
            const auto it = std::find_if(interface_list.cbegin(),
                                         end_interface_list,
                                         [coords_index_list](const auto& interface_ptr)
                                         {
                                             assert(!(!interface_ptr));
                                             
                                             auto coords = interface_ptr->GetVertexCoordsIndexList();
                                             // < Copy incurred on purpose (see sort below).
                                             
                                             assert(coords.size() == coords_index_list.size());
                                             
                                             std::sort(coords.begin(), coords.end());
                                             return coords == coords_index_list;
                                         });
            
            assert(it != end_interface_list);

            return *(*it);
        }
        
        
    } // namespace PostProcessingNS
    
    
} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
