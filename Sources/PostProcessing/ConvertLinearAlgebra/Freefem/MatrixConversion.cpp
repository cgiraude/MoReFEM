/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 May 2016 12:26:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <map>
#include <unordered_set>

#include "Utilities/Containers/UnorderedMap.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "PostProcessing/ConvertLinearAlgebra/Freefem/MatrixConversion.hpp"
#include "PostProcessing/PostProcessing.hpp"


namespace MoReFEM
{
    
    
    namespace PostProcessingNS
    {
        
        
        namespace FreefemNS
        {
            
            
            namespace // anonymous
            {
                
                
                /*!
                 * \brief Read and interpret the Freefem file that provides the keys to Freefem dof numbering.
                 *
                 * See MatrixConversion constructor for more detsils about it.
                 *
                 * \return Index of outer vector is index of the GeometricElt.
                 * Inner vector includes all dofs related to the geometric element.
                 */
                std::vector<std::vector<unsigned int>> ReadFreefemDofNumbering(const std::string& file);
                
                
                              
                
                // Sort MoReFEM dofs within the GeometricElt in the following order:
                // - (unknown 1 / component 1 / vertex 1)
                // - (unknown 1 / component 1 / vertex 2)
                // - ...
                // - (unknown 1 / component 1 / face 1)
                // - ...
                // - (unknown 1 / component 2 / vertex 1) // if relevant!
                // - ...
                // - (unknown 2 / component 1 / vertex 1)
                //
                std::vector<unsigned> ComputeMoReFEMOrdering(const PostProcessing& data,
                                                                const NumberingSubset& numbering_subset,
                                                                const GeometricElt& geom_elt,
                                                                const std::vector<std::string>& unknown_in_matrix_list);
                 
                
                
            } // namespace anonymous

            
            MatrixConversion::MatrixConversion(const Wrappers::Mpi& mpi,
                                               const std::string& freefem_numbering_informations_file,
                                               const std::string& matrix_binary_file,
                                               const FilesystemNS::Directory& data_directory,
                                               const std::vector<std::string>& unknown_in_matrix_list,
                                               const FilesystemNS::Directory& output_directory,
                                               const NumberingSubset& numbering_subset,
                                               const Mesh& mesh,
                                               const char* invoking_file, int invoking_line)
            : mpi_parent(mpi),
            original_matrix_(Wrappers::Petsc::Matrix(mpi,
                                                     matrix_binary_file,
                                                     invoking_file, invoking_line)),
            output_directory_(output_directory)
            {
                assert(mpi.Nprocessor<int>() == 1 && "This class should be invoked only in sequential (but may deal "
                       "with matrix obtained in parallel computation).");
                
                auto&& numbering_subset_id_list = { numbering_subset.GetUniqueId() };
                
                post_processing_data_ = std::make_unique<PostProcessing>(data_directory,
                                                                         std::move(numbering_subset_id_list),
                                                                         mesh);
                
                freefem_dof_numbering_per_geom_elt_ = ReadFreefemDofNumbering(freefem_numbering_informations_file);
                
                ComputeDofMapping(numbering_subset, unknown_in_matrix_list);
                ComputeTransfertMatrix();
            }
            
            
            
            void MatrixConversion::ComputeTransfertMatrix() const
            {
                decltype(auto) data = GetPostProcessingData();
            
                const auto Ndof = data.Ndof();
                std::vector<std::vector<PetscInt>> non_zero_rows;
                non_zero_rows.resize(Ndof);
                
                decltype(auto) mapping = GetDofMapping();
                
                assert(mapping.size() == Ndof);
                
                for (auto i = 0ul; i < Ndof; ++i)
                    non_zero_rows[i] = { static_cast<PetscInt>(mapping[i])};
                

                
                assert(non_zero_rows.size() == Ndof);
                
                Wrappers::Petsc::MatrixPattern pattern(non_zero_rows);
                
                Wrappers::Petsc::Matrix transfert_matrix;
                
                transfert_matrix.InitSequentialMatrix(Ndof, Ndof,
                                                      pattern,
                                                      mpi_parent::GetMpi(),
                                                      __FILE__, __LINE__);

                std::vector<PetscScalar> one { 1. };
                
                for (PetscInt i = 0; i < static_cast<PetscInt>(Ndof); ++i)
                    transfert_matrix.SetValuesRow(i, one.data(), __FILE__, __LINE__);
                
                transfert_matrix.Assembly(__FILE__, __LINE__);
                
                decltype(auto) output_directory = GetOutputDirectory();
                
                transfert_matrix.View(GetMpi(),
                                      output_directory.AddFile("transfert_matrix.hhdata"),
                                      __FILE__, __LINE__);
                
                Wrappers::Petsc::Matrix matrix_in_freefem_numbering;
                
                Wrappers::Petsc::PtAP(GetOriginalMatrix(),
                                      transfert_matrix,
                                      matrix_in_freefem_numbering,
                                      __FILE__, __LINE__);
                
                matrix_in_freefem_numbering.View(GetMpi(),
                                                 output_directory.AddFile("matrix_in_freefem_numbering.m"),
                                                 __FILE__, __LINE__,
                                                 PETSC_VIEWER_ASCII_MATLAB);
                
                
            }
            
            
            
            void MatrixConversion::ComputeDofMapping(const NumberingSubset& numbering_subset,
                                                     const std::vector<std::string>& unknown_in_matrix_list)
            {
                decltype(auto) data = GetPostProcessingData();

                decltype(auto) mesh = data.GetMesh();
                
                const auto mesh_dimension = mesh.GetDimension();
                
                decltype(auto) geom_elt_list = mesh.GetSubsetGeometricEltList(mesh_dimension);
                
                const auto begin = geom_elt_list.first;
                const auto end = geom_elt_list.second;
                
                const auto Ndof = data.Ndof();
                
                constexpr auto unused = NumericNS::UninitializedIndex<unsigned int>();
                std::vector<unsigned int> mapping(Ndof, unused);
                
                unsigned int current_geom_elt_index = 0u;
                
                // Loop over geometric elements.
                for (auto it = begin; it != end; ++it)
                {
                    decltype(auto) geom_elt_ptr = *it;
                    assert(!(!geom_elt_ptr));
                    decltype(auto) geom_elt = *geom_elt_ptr;
                    
                    auto&& morefem_ordering = ComputeMoReFEMOrdering(data,
                                                                            numbering_subset,
                                                                            geom_elt,
                                                                            unknown_in_matrix_list);
                    
                    decltype(auto) freefem_ordering = GetFreefemDofList(current_geom_elt_index);
                    
                    assert(morefem_ordering.size() == freefem_ordering.size()
                           && "Check the Freefem ordering file you gave in input is valid!");
                    
                    const auto size = morefem_ordering.size();
                    
                    for (auto i = 0ul; i < size; ++i)
                    {
                        const auto morefem_dof_index = morefem_ordering[i];
                        assert(morefem_dof_index < Ndof);
                        
                        const auto freefem_dof_index = freefem_ordering[i];
                        
                        auto& current_dof_in_mapping = mapping[morefem_dof_index];
                        
                        if (current_dof_in_mapping == unused)
                            current_dof_in_mapping = freefem_dof_index;
                        else
                        {
                            assert(current_dof_in_mapping == freefem_dof_index && "A dof must be mapped the same "
                                   "way for all geometric elements!");
                        }
                    }
                    
                    ++current_geom_elt_index;
                    
                } // loop over geometric elements
                
                assert(std::none_of(mapping.cbegin(),
                                    mapping.cend(),
                                    [](unsigned int value)
                                    {
                                        return value == unused;
                                    }));
                
                SetDofMapping(std::move(mapping));
                
            }
            
            
            
            namespace // anonymous
            {
                
                
                std::vector<unsigned> ComputeMoReFEMOrdering(const PostProcessing& data,
                                                                const NumberingSubset& numbering_subset,
                                                                const GeometricElt& geom_elt,
                                                                const std::vector<std::string>& unknown_in_matrix_list)
                {
                    std::vector<unsigned> ret;
                    
                    assert(geom_elt.GetIdentifier() == ::MoReFEM::Advanced::GeometricEltEnum::Triangle3
                           && "At the moment limited to that case!");
                    
                    decltype(auto) coords_list = geom_elt.GetCoordsList();
                    
                    // Dofs in the following order
                    // - (unknown 1 / component 1 / vertex 1)
                    // - (unknown 1 / component 1 / vertex 2)
                    // - ...
                    // - (unknown 1 / component 1 / face 1)
                    // - ...
                    // - (unknown 1 / component 2 / vertex 1) // if relevant!
                    // - ...
                    // - (unknown 2 / component 1 / vertex 1)
                    
                    decltype(auto) unknown_list = data.GetExtendedUnknownList();
                    
                    
                    
                    decltype(auto) mesh_dimension = data.GetMesh().GetDimension();
                    
                    decltype(auto) interface_data = data.GetInterfaceData();
                    
                    assert(data.Nprocessor() == 1u && "For the time being, works only on code obtained during a sequential run.");
                    // Extend it to parallel isn't though (especially considering all dofs on a same NodeBearer are on
                    // same processor by design), but I do not have the time to do it now.
                    constexpr const auto processor = 0u;
                    decltype(auto) dof_information_data = data.GetDofFile(numbering_subset.GetUniqueId(), processor);

                    for (const auto& unknown_name : unknown_in_matrix_list)
                    {
                        const auto it = std::find_if(unknown_list.cbegin(),
                                                     unknown_list.cend(),
                                                     [&unknown_name](const auto& unknown_ptr)
                                                     {
                                                         assert(!(!unknown_ptr));
                                                         return unknown_ptr->GetName() == unknown_name;
                                                     });
                        assert(it != unknown_list.cend() && "Unknown given in input not found in output file; check it "
                               "is spelled the same way as in the Lua file!");

                        decltype(auto) unknown_ptr = *it;
                        assert(!(!unknown_ptr));
                        const auto& unknown = *unknown_ptr;
                        
                        const auto Ncomponent =
                            unknown.GetNature() == UnknownNS::Nature::vectorial
                            ? mesh_dimension
                            : 1u;
                        
                        std::string shape_function_label;
                        
                        std::vector<std::vector<unsigned int>> dof_order_per_component(Ncomponent);
                        
                        for (const auto& coords : coords_list)
                        {
                            assert(!(!coords));
                            const auto coords_index = coords->GetIndex();
                            decltype(auto) vertex = interface_data.GetVertexInterfaceFromCoordIndex(coords_index);
                            
                            for (auto component = 0u; component < Ncomponent; ++component)
                            {
                                decltype(auto) dof = dof_information_data.GetDof(InterfaceNS::Nature::vertex,
                                                                                 vertex.GetIndex(),
                                                                                 unknown_name,
                                                                                 component);
                                
                                shape_function_label = dof.GetShapeFunctionLabel();
                                
                                dof_order_per_component[component].push_back(dof.GetProgramWiseIndex());
                            }
                        }
                        
                        if (shape_function_label != "P1" && shape_function_label != "P1b")
                            throw Exception("Currently only P1 and P1b are supported; '" + shape_function_label
                                            + "' was read.", __FILE__, __LINE__);
                        
                        if (shape_function_label == "P1b")
                        {
                            decltype(auto) face = interface_data.GetFaceInterfaceFromCoordsList(coords_list);
                            
                            for (auto component = 0u; component < Ncomponent; ++component)
                            {
                                decltype(auto) dof = dof_information_data.GetDof(InterfaceNS::Nature::face,
                                                                                 face.GetIndex(),
                                                                                 unknown_name,
                                                                                 component);
                                
                                dof_order_per_component[component].push_back(dof.GetProgramWiseIndex());
                            }
                        }
                        
                        for (auto component = 0u; component < Ncomponent; ++component)
                        {
                            std::move(dof_order_per_component[component].begin(),
                                      dof_order_per_component[component].end(),
                                      std::back_inserter(ret));
                        }
                        
                    } // for (const auto& unknown : unknown_list)
                    
                    return ret;
                }

                
                
                std::vector<std::vector<unsigned int>> ReadFreefemDofNumbering(const std::string& file)
                {
                    std::ifstream stream;
                    FilesystemNS::File::Read(stream, file, __FILE__, __LINE__);
                    
                    std::string line;
                    
                    std::vector<std::vector<unsigned int>> ret;
                    
                    unsigned int buf;
                    std::istringstream iconv;
                    
                    while (getline(stream, line))
                    {
                        std::vector<unsigned int> dof_list;
                        iconv.clear();
                        iconv.str(line);
                        
                        while (iconv >> buf)
                            dof_list.push_back(buf);

                        ret.emplace_back(std::move(dof_list));
                    }
                    
                    return ret;
                }

                
                
            } // namespace anonymous
          
            
        } // namespace FreefemNS
        
        
    } // namespace PostProcessingNS
  

} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup
