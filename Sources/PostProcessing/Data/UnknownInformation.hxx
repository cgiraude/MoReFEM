/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 18 Dec 2014 18:20:41 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_UNKNOWN_INFORMATION_HXX_
# define MOREFEM_x_POST_PROCESSING_x_DATA_x_UNKNOWN_INFORMATION_HXX_


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            inline const std::string& UnknownInformation::GetName() const
            {
                return name_;
            }


            inline UnknownNature UnknownInformation::GetNature() const
            {
                return nature_;
            }


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_UNKNOWN_INFORMATION_HXX_
