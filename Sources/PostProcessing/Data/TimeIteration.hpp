/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:50:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HPP_
# define MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HPP_

# include <string>
# include <memory>
# include <vector>


namespace MoReFEM
{


    namespace PostProcessingNS
    {


        namespace Data
        {


            /*!
             * \brief Class which holds the informations obtained from one line of time_iteration.hhdata output file.
             *
             * There is one such file per mesh.
             *
             * Example:
             * \verbatim
             # Time iteration; time; numbering subset id; filename
             1;0.001;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_* /Mesh_1/NumberingSubset_10/fluid_velocity_time_00001.hhdata
             2;0.002;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_* /Mesh_1/NumberingSubset_10/fluid_velocity_time_00002.hhdata
             ...
             \endverbatim
             *
             * (no space after Rank_* - but I had to add it to avoid compiler mistaking it for an end of a C comment).
             */
            class TimeIteration final
            {

            public:

                //! Alias for unique ptr.
                using const_unique_ptr = std::unique_ptr<const TimeIteration>;

                //! Alias for a vector of unique_ptr.
                using vector_const_unique_ptr = std::vector<const_unique_ptr>;


            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] line Line as read in time_iteration.hhdata output file. Format is:
                 * *Time iteration*;*time*;*filename*
                 *
                 * For instance: '2;0.2;/Volumes/Data/sebastien/MoReFEM/Results/Hyperelasticity/solution_00002_proc0.hhdata'.
                 */

                explicit TimeIteration(const std::string& line);

                //! Destructor.
                ~TimeIteration() = default;

                //! \copydoc doxygen_hide_copy_constructor
                TimeIteration(const TimeIteration& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                TimeIteration(TimeIteration&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                TimeIteration& operator=(const TimeIteration& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                TimeIteration& operator=(TimeIteration&& rhs) = delete;

                ///@}

            public:

                //! Time iteration.
                unsigned int GetIteration() const noexcept;

                //! Time (in seconds).
                double GetTime() const noexcept;

                //! Get the filename of the solution at the given time iteration.
                const std::string& GetSolutionFilename() const noexcept;

                //! Get the numbering subset id.
                unsigned int GetNumberingSubsetId() const noexcept;

            private:

                //! Time iteration.
                unsigned int time_iteration_;

                //! Time (in seconds).
                double time_;

                //! Filename of the solution at this iteration.
                std::string solution_filename_;

                //! Numbering subset id.
                unsigned int numbering_subset_id_;

            };


        } // namespace Data


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/Data/TimeIteration.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HPP_
