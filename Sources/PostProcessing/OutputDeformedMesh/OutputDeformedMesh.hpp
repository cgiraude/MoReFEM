/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 11 Apr 2017 11:30:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_OUTPUT_DEFORMED_MESH_x_OUTPUT_DEFORMED_MESH_HPP_
# define MOREFEM_x_POST_PROCESSING_x_OUTPUT_DEFORMED_MESH_x_OUTPUT_DEFORMED_MESH_HPP_

# include <memory>
# include <vector>

# include "Geometry/Mesh/Mesh.hpp"
# include "Geometry/Mesh/Internal/MeshManager.hpp"
# include "Geometry/Domain/Domain.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace PostProcessingNS
    {

        /*!
         * \brief Generates a deformed mesh from a domain of the mesh.
         *
         */
        class OutputDeformedMesh
        {

        public:

            //! Alias to self.
            using self = OutputDeformedMesh;

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to vector of unique pointers.
            using vector_unique_ptr = std::vector<unique_ptr> ;

        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] data_directory The result directory of a MoReFEM run.
             * \param[in] unknown_name Name of the unknown that deforms the mesh, as given in input data file.
             * \param[in] numbering_subset_id Unique identifier of the \a NumberingSubset considered.
             * \param[in] mesh_manager The \a MeshManager singleton (an odd choice to put it as constructor argument,
             * but I'm not the author of this class...)
             * \param[in] mesh \a Mesh to be deformed.
             * \param[in] domain Keep only \a GeometricElt that are inside this \a Domain.
             * \param[in] output_mesh_index Used to tag the directory in which deformed mesh will be stored.
             * \todo But there is as well a generated unique id; it's likely this class is not very consistent...
             * \param[in] output_format Name of the format to use in output; of course the elements must be compatible
             * with it (if geometric Quadrangle9 are involved and you're choosing a format that does not handle them
             * you're screwed).
             * \param[in] output_name Used to generate the name of the output mesh files.
             * \param[in] output_space_unit Space unit to use for output mesh.
             * \param[in] output_offset Only time iterations with index greater or equal to this value are kept.
             * \param[in] output_frequence Only one time iterations over this number is kept (choose 1 to keep them all,
             * 2, to get one half of the iterations and so forth...).
             */
            explicit OutputDeformedMesh(const FilesystemNS::Directory& data_directory,
                                        const std::string& unknown_name,
                                        const unsigned int numbering_subset_id,
                                        Internal::MeshNS::MeshManager& mesh_manager,
                                        const Mesh& mesh,
                                        const Domain& domain,
                                        const unsigned int output_mesh_index,
                                        const std::string& output_name,
                                        const std::string& output_format,
                                        const double output_space_unit,
                                        const unsigned int output_offset,
                                        const unsigned int output_frequence);

            //! Destructor.
            ~OutputDeformedMesh() = default;

            //! \copydoc doxygen_hide_copy_constructor
            OutputDeformedMesh(const OutputDeformedMesh& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            OutputDeformedMesh(OutputDeformedMesh&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            OutputDeformedMesh& operator=(const OutputDeformedMesh& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            OutputDeformedMesh& operator=(OutputDeformedMesh&& rhs) = delete;

            ///@}

        private:



        };


    } // namespace PostProcessingNS


} // namespace MoReFEM


/// @} // addtogroup PostProcessingGroup


# include "PostProcessing/OutputDeformedMesh/OutputDeformedMesh.hxx"


#endif // MOREFEM_x_POST_PROCESSING_x_OUTPUT_DEFORMED_MESH_x_OUTPUT_DEFORMED_MESH_HPP_
