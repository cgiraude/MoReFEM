/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 27 Jan 2016 14:47:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HXX_
# define MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HXX_


namespace MoReFEM
{





} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_DIAGONAL_MATRIX_HXX_
