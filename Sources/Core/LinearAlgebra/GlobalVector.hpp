/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 27 Apr 2015 09:33:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_VECTOR_HPP_
# define MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_VECTOR_HPP_

# include <memory>
# include <vector>
# include <array>

# include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class NumberingSubset;


    namespace Internal::SolverNS
    {


        template<class VariationalFormulationT>
        struct SnesInterface;


    } // namespace Internal::SolverNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Class which encapsulates both the Petsc vector and the numbering subset used to described it.
     */
    class GlobalVector final : public Wrappers::Petsc::Vector
    {

    public:

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<GlobalVector>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias to array of unique pointers.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;

        //! Alias to parent.
        using parent = Wrappers::Petsc::Vector;

        //! Friendship (to enable ChangeInternal usage).
        template<class VariationalFormulationT>
        friend struct Internal::SolverNS::SnesInterface;

    public:

        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] numbering_subset \a NumberingSubset to use to define the numbering of the vector.
        explicit GlobalVector(const NumberingSubset& numbering_subset);

        //! Destructor.
        virtual ~GlobalVector() override;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalVector(const GlobalVector& rhs);

        //! \copydoc doxygen_hide_move_constructor
        GlobalVector(GlobalVector&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalVector& operator=(const GlobalVector& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalVector& operator=(GlobalVector&& rhs) = delete;

        ///@}

        //! Numbering subset used to describe vector.
        const NumberingSubset& GetNumberingSubset() const;

    private:

        // ===========================================================================
        // \attention Do not forget to update Swap() if a new data member is added!
        // =============================================================================

        //! Numbering subset used to describe vector.
        const NumberingSubset& numbering_subset_;

    };


    # ifndef NDEBUG

    /*!
     * \brief Debug tool to print the unique id of \a NumberingSubset.
     *
     * \param[in] vector_name Tag to identify the vector which \a NumberingSubset information will be written.
     * \param[in] vector Vector under investigation.
     */
    void PrintNumberingSubset(std::string&& vector_name,
                              const GlobalVector& vector);


    # endif // NDEBUG


    /*!
     * \brief Swap two vectors.
     *
     * The Petsc content of the vectors is swapped; however the numbering subsets must be the same on both ends
     * (we expect here to swap only vectors with same structure).
     *
     * \attention Do not use it until #530 is resolved; Petsc's defined swap might have to be used.
     *
     * \param[in] A One of the vector to swap.
     * \param[in] B The other matrix.
     */
    void Swap(GlobalVector& A, GlobalVector& B);

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalVector&, GlobalVector::parent& );

    //! \copydoc doxygen_hide_global_linear_algebra_forbidden_swap_function
    void Swap(GlobalVector::parent&, GlobalVector&);


    /*!
     * \brief Useful alias to avoid cluttering the main programs with too low-level C++.
     *
     * \code
     * GlobalVectorWithCoefficient(vm.GetNonCstForce(), 1.)
     * \endcode
     *
     * is probably easier to grasp than either:
     * \code
     * std::pair<GlobalVector&, double>(vm.GetNonCstForce(), 1.)
     * \endcode
     *
     * or
     *
     * \code
     * std::make_pair(std::ref(vm.GetNonCstForce()), 1.)
     * \endcode
     */
    using GlobalVectorWithCoefficient = std::pair<GlobalVector&, double>;


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/LinearAlgebra/GlobalVector.hxx"


#endif // MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_VECTOR_HPP_
