/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 27 Apr 2015 09:33:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_VECTOR_HXX_
# define MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_VECTOR_HXX_


namespace MoReFEM
{


    inline const NumberingSubset& GlobalVector::GetNumberingSubset() const
    {
        return numbering_subset_;
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_LINEAR_ALGEBRA_x_GLOBAL_VECTOR_HXX_
