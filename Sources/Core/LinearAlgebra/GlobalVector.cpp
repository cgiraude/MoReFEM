/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 27 Apr 2015 09:33:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef NDEBUG
# include <iostream>
#endif // NDEBUG

#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM
{
    
    
    GlobalVector::~GlobalVector() = default;
    
    
    GlobalVector::GlobalVector(const NumberingSubset& numbering_subset)
    : numbering_subset_(numbering_subset)
    { }
    
    
    
    GlobalVector::GlobalVector(const GlobalVector& rhs)
    : parent(rhs),
    numbering_subset_(rhs.numbering_subset_)
    { }
    
    
    void Swap(GlobalVector& A, GlobalVector& B)
    {
        assert(A.GetNumberingSubset() == B.GetNumberingSubset());
        
        using parent = GlobalVector::parent;

        Swap(static_cast<parent&>(A), static_cast<parent&>(B));        
    }
    
    
    #ifndef NDEBUG
    void PrintNumberingSubset(std::string&& vector_name,
                              const GlobalVector& vector)
    {
        std::cout << "Numbering subsets for vector '" << vector_name << "': "
        << vector.GetNumberingSubset().GetUniqueId() << std::endl;
    }
    #endif // NDEBUG
  

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
