/*!
* \defgroup CoreGroup Core
*
* \brief This module encompass some generic stuff that may be used at every level of MoReFEM.
*
* Contrary to Utilities, the content here is really related to the purpose of MoReFEM, whereas Utilities is meant
* to be a generic library that could be used in any kind of C++ project.
*
*/


/// \addtogroup CoreGroup
///@{


/// \namespace MoReFEM::TimeManagerNS
/// \brief Namespace that enclose stuff related to TimeManager.


/// \namespace MoReFEM::Internal::NumberingSubsetNS
/// \brief Namespace that enclose internals related to NumberingSubset.

///@} // addtogroup



/// \namespace MoReFEM::TimeManagerNS::Policy
/// \brief Namespace that enclose time step policies related to TimeManager.



/*!
 *
 * \class doxygen_hide_input_data_arg
 *
 * \param[in] input_data Object which hold the values of all the parameters defined in
 * the input file.
 */


/*!
 *
 * \class doxygen_hide_morefem_data_param
 *
 * \param[in] morefem_data Object which hold few objects (mpi, input data file, Petsc helper and directory into
 * which results are stored) that are almost global variables.
 */


/*!
 * \class doxygen_hide_mesh_enum
 *
 * \brief Enum used to index the available meshes.
 */


/*!
 * \class doxygen_hide_domain_enum
 *
 * \brief Enum used to index the available domains.
 */


/*!
 * \class doxygen_hide_felt_space_enum
 *
 * \brief Enum used to index the available finite element spaces.
 */


/*!
 * \class doxygen_hide_unknown_enum
 *
 * \brief Enum used to index the available unknowns.
 */


/*!
 * \class doxygen_hide_solver_enum
 *
 * \brief Enum used to index the available solvers.
 */


/*!
 * \class doxygen_hide_numbering_subset_enum
 *
 * \brief Enum used to index the available numbering subsets.
 */

/*!
 * \class doxygen_hide_source_enum
 *
 * \brief Enum used to index the available sources.
 */

/*!
 * \class doxygen_hide_boundary_condition_enum
 *
 * \brief Enum used to index the available boundary conditions.
 */


/*!
 * \class doxygen_hide_input_data_tuple
 *
 * \brief Tuple which enumerates all the quantities that are addressed in the input file.
 *
 */


/*!
 * \class doxygen_hide_initial_condition_enum
 *
 * \brief Enum used to index the available initial conditions.
 *
 */


/*!
 * \class doxygen_hide_model_specific_input_data
 *
 * \brief The input parameter list object required for the current test or model.
 *
 */



/*!
 * \class doxygen_hide_fiber_enum
 *
 * \brief Enum used to index the available fibers.
 *
 */

/*!
 * \class doxygen_hide_morefem_data_type
 *
 * \brief The MoReFEMDataType object required for the current test or model.
 *
 */


/*!
 * \class doxygen_hide_input_data_type
 *
 * \brief The input data type that was used in \a MoReFEMData.
 *
 */


/*!
 *
 * \class doxygen_hide_init_morefem_param
 *
 * \param[in] morefem_data The object which encapsulates some stuff that acts as global data, such as:
 * - The content of the input parameter data.
 * - Mpi related informations.
 * - The directory into which output is to be written.
 */



