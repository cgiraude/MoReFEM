### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetManager.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FetchFunction.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FetchFunction.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/FindFunctor.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FindFunctor.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetManager.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetManager.hxx"
)

