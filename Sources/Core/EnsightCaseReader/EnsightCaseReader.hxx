/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 20 Jul 2016 11:01:34 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_ENSIGHT_CASE_READER_x_ENSIGHT_CASE_READER_HXX_
# define MOREFEM_x_CORE_x_ENSIGHT_CASE_READER_x_ENSIGHT_CASE_READER_HXX_


namespace MoReFEM
{


    inline const std::vector<EnsightGeometry>& EnsightCaseReader::GetEnsightGeometry() const noexcept
    {
        return geometry_;
    }


    inline const std::vector<EnsightTime>& EnsightCaseReader::GetEnsightTime() const noexcept
    {
        return time_;
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_ENSIGHT_CASE_READER_x_ENSIGHT_CASE_READER_HXX_
