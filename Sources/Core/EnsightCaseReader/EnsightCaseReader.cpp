/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 20 Jul 2016 11:01:34 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include <fstream>
#include <sstream>
#include <numeric>
#include <algorithm>
#include <iostream>
#include <cassert>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Core/EnsightCaseReader/EnsightCaseReader.hpp"


namespace MoReFEM
{

    
    EnsightGeometry::EnsightGeometry(unsigned int time_set, const std::string& file)
    : time_set_(time_set),
    file_(file)
    { }
    

    EnsightTime::EnsightTime(unsigned int time_set,
                             unsigned int number_of_steps,
                             unsigned int filename_start_number,
                             unsigned int filename_increment,
                             const std::vector<std::pair<double, unsigned int>>& time_values)
    : time_set_(time_set),
    number_of_steps_(number_of_steps),
    filename_start_number_(filename_start_number),
    filename_increment_(filename_increment),
    time_values_(time_values)
    { }
    
    
    EnsightCaseReader::EnsightCaseReader(std::string filename)
    : filename_(filename)
    {
        std::ifstream file_in;
        FilesystemNS::File::Read(file_in, filename, __FILE__, __LINE__);
        
        std::string buf;
        std::string token;
        size_t last = 0;
        size_t next = 0;
        
        unsigned int time_set_time = 0;
        unsigned int number_of_steps = 0;
        unsigned int filename_start_number = 0;
        unsigned int filename_increment = 0;
        std::vector<std::pair<double, unsigned int>> time_values;
        unsigned int time_set_geometry;
        std::string file;
        bool fill_time_values = false;
        unsigned int counter = 0u;
        
        // Read the format
        getline(file_in, buf);
        
        if (buf == "FORMAT")
        {
            getline(file_in, buf);
            
            while ((next = buf.find(' ', last)) != std::string::npos)
            {
                token = buf.substr(last, next-last);
                last = next + 1;
            }
            
            token = buf.substr(last);
            
            if (token != "ensight")
                throw Exception("The format of the Ensight Case is not the right one.", __FILE__, __LINE__);
            
        }
        else
            throw Exception("The format of the Ensight Case is not the right one.", __FILE__, __LINE__);
        
        // \todo #1097 Implementation to check (at the very least fix cppcheck warnings...)
        while (file_in)
        {
            getline(file_in, buf);
            
            token = buf.substr(0, buf.find(':'));
            
            if (token == "model")
            {
                last = 0;
                
                next = buf.find(' ', last);
                token = buf.substr(last, next-last);
                last = next + 1;
                
                next = buf.find(' ', last);
                token = buf.substr(last, next-last);
                time_set_geometry = static_cast<unsigned int>(std::stoul(token));
                
                last = next + 1;
                next = buf.find(' ', last);
                token = buf.substr(last, next-last);
                file = token;
                
                EnsightGeometry geometry(time_set_geometry, file);
                
                geometry_.push_back(geometry);
            }
            else if (token == "time set")
            {
                last = 0;

                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                last = next + 1;
                
                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                time_set_time = static_cast<unsigned int>(std::stoul(token));
            }
            else if (token == "number of steps")
            {
                last = 0;

                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                last = next + 1;
                
                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                number_of_steps = static_cast<unsigned int>(std::stoul(token));
            }
            else if (token == "filename start number")
            {
                last = 0;

                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                last = next + 1;
                
                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                filename_start_number = static_cast<unsigned int>(std::stoul(token));
                
                counter = filename_start_number;
            }
            else if (token == "filename increment")
            {
                last = 0;

                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                last = next + 1;
                
                next = buf.find(':', last);
                token = buf.substr(last, next-last);
                filename_increment = static_cast<unsigned int>(std::stoul(token));
            }
            else if (token == "time values")
            {
                fill_time_values = true;
            }
            else if (fill_time_values)
            {
                last = 0;
                
                while ((next = buf.find(' ', last)) != std::string::npos)
                {
                    token = buf.substr(last, next-last);
                    last = next + 1;
                    
                    if (!token.empty())
                    {
                        time_values.push_back(std::make_pair(std::stod(token), counter));
                        assert(filename_increment != 0);
                        counter += filename_increment;
                    }
                }
                
                token = buf.substr(last);
                
                if (!token.empty())
                {
                    time_values.push_back(std::make_pair(std::stod(token), counter));
                    counter += filename_increment;
                }
            }
        }
        
        assert(number_of_steps == time_values.size());
        
        EnsightTime ensight_time(time_set_time,
                                 number_of_steps,
                                 filename_start_number,
                                 filename_increment,
                                 time_values);
        
        time_.push_back(ensight_time);
        
        assert(file_in.eof());
    }
    
    
    void EnsightCaseReader::Print()
    {
        unsigned int size = static_cast<unsigned int>(geometry_.size());
        
        std::cout << "Geometry" << std::endl;
        
        for (unsigned int i = 0 ; i < size ; ++i)
        {
            std::cout << "Time set" << std::endl;
            std::cout << geometry_[i].time_set_ << std::endl;
            std::cout << "File" << std::endl;
            std::cout << geometry_[i].file_ << std::endl;
        }
        
        size = static_cast<unsigned int>(time_.size());
        
        std::cout << "Time" << std::endl;
        
        for (unsigned int i = 0 ; i < size ; ++i)
        {
            std::cout << "Time set" << std::endl;
            std::cout << time_[i].time_set_ << std::endl;
            std::cout << "Number of Steps" << std::endl;
            std::cout << time_[i].number_of_steps_ << std::endl;
            std::cout << "Filename start number" << std::endl;
            std::cout << time_[i].filename_start_number_ << std::endl;
            std::cout << "Filename increment" << std::endl;
            std::cout << time_[i].filename_increment_ << std::endl;
            
            std::cout << "Time values" << std::endl;
            unsigned int size_time_values = static_cast<unsigned int>(time_[i].time_values_.size());
            
            for (unsigned int j = 0 ; j < size_time_values ; ++j)
            {
                std::cout << time_[i].time_values_[j].first << ' ';
            }
            
            std::cout << std::endl;
        }
    }
  

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
