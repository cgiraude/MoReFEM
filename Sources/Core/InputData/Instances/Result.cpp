/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Result.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        const std::string& Result::GetName()
        {
            static std::string ret("Result");
            return ret;
        }
        
            
        const std::string& Result::OutputDirectory::NameInFile()
        {
            static std::string ret("output_directory");
            return ret;
        }
        
        
        const std::string& Result::OutputDirectory::Description()
        {
            static std::string ret("Directory in which all the results will be written. This path may use the "
                                   "environment variable MOREFEM_RESULT_DIR, which is either provided in user's "
                                   "environment or automatically set to '/Volumes/Data/${USER}/MoReFEM/Results' "
                                   "in MoReFEM initialization step. You may also use ${MOREFEM_START_TIME} in the value "
                                   "which will be replaced by a time under format YYYY_MM_DD_HH_MM_SS."
                                   "Please do not read the value directly from this Lua file: whenever you need the "
                                   "path to the result directory, use instead MoReFEMData::GetResultDirectory()."
                                   );
            return ret;
        }
        
        
        const std::string& Result::OutputDirectory::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        const std::string& Result::OutputDirectory::DefaultValue()
        {
            return Utilities::EmptyString();
        }

        
        const std::string& Result::DisplayValue::NameInFile()
        {
            static std::string ret("display_value");
            return ret;
        }
        
        
        const std::string& Result::DisplayValue::Description()
        {
            static std::string ret("Enables to skip some printing in the console. Can be used to WriteSolution every n time.");
            return ret;
        }


        const std::string& Result::DisplayValue::Constraint()
        {
            static std::string ret("v > 0");
            return ret;
        }
        
        
        
        const std::string& Result::DisplayValue::DefaultValue()
        {
            static std::string ret("1");
            return ret;
        }
        
        
        const std::string& Result::BinaryOutput::NameInFile()
        {
            static std::string ret("binary_output");
            return ret;
        }
        
        
        const std::string& Result::BinaryOutput::Description()
        {
            static std::string ret("Defines the solutions output format. Set to false for ascii or true for binary.");
            return ret;
        }
        
        
        const std::string& Result::BinaryOutput::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        
        const std::string& Result::BinaryOutput::DefaultValue()
        {
            static std::string ret("false");
            return ret;
        }
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup


