/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 16:10:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/FElt/Impl/NumberingSubset.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        namespace Impl
        {
            
            
            namespace NumberingSubsetNS
            {
                
                
                const std::string& Name::NameInFile()
                {
                    static std::string ret("name");
                    return ret;
                }
                
                
                const std::string& Name::Description()
                {
                    static std::string ret("Name of the numbering subset (not really used; at the moment I just "
                                           "need one input parameter to ground the possible values to choose "
                                           "elsewhere).");
                    return ret;
                }
                
                
                const std::string& Name::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                
                const std::string& Name::DefaultValue()
                {
                    return Utilities::EmptyString();
                }
                
                
                const std::string& DoMoveMesh::NameInFile()
                {
                    static std::string ret("do_move_mesh");
                    return ret;
                }
                
                
                const std::string& DoMoveMesh::Description()
                {
                    static std::string ret("Whether a vector defined on this numbering subset might be used to compute "
                                           "a movemesh. If true, a FEltSpace featuring this numbering subset will "
                                           "compute additional quantities to enable fast computation. This should be "
                                           "false for most numbering subsets, and when it's true the sole "
                                           "unknown involved should be a displacement.");
                    return ret;
                }
                
                
                const std::string& DoMoveMesh::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                
                const std::string& DoMoveMesh::DefaultValue()
                {
                    static std::string ret("false");
                    return ret;
                }

                
                
            } // namespace NumberingSubsetNS
            
            
        } // namespace Impl
        
        
    } // namespace InputDataNS
  

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
