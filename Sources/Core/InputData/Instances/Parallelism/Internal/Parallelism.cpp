//! \file 
//
//
//  Parallelism.cpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Parallelism/Internal/Parallelism.hpp"


namespace MoReFEM::Internal::InputDataNS::ParallelismNS
{


    const std::string& Policy::NameInFile()
    {
        static std::string ret("policy");
        return ret;
    }


    const std::string& Policy::Description()
    {
        static std::string ret("What should be done for a parallel run. There are 4 possibilities:\n"
                               " 'Precompute': Precompute the data for a later parallel run and stop once it's done.\n"
                               " 'ParallelNoWrite': Run the code in parallel without using any pre-processed data "
                               "and do not write down the processed data.\n"
                               " 'Parallel': Run the code in parallel without using any pre-processed data and write "
                               "down the processed data.\n"
                               " 'RunFromPreprocessed': Run the code in parallel using pre-processed data.");
        return ret;
    }


    const std::string& Policy::Constraint()
    {
        static std::string ret("value_in(v, {'Precompute', 'ParallelNoWrite', 'Parallel', 'RunFromPreprocessed'})");
        return ret;
    }


    const std::string& Policy::DefaultValue()
    {
        static std::string ret("'ParallelNoWrite'");
        return ret;
    }


    const std::string& Directory::NameInFile()
    {
        static std::string ret("directory");
        return ret;
    }


    const std::string& Directory::Description()
    {
        static std::string ret("Directory in which parallelism data will be written or read (depending on the policy).");
        return ret;
    }


    const std::string& Directory::Constraint()
    {
        return Utilities::EmptyString();
    }


    const std::string& Directory::DefaultValue()
    {
        static std::string ret("''"); // not empty string on purpose: I want the default value to be this!
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::ParallelismNS
