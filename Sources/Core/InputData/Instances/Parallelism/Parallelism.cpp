//! \file 
//
//
//  Parallelism.cpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Parallelism::GetName()
    {
        static std::string ret("Parallelism");
        return ret;
    }


} // namespace MoReFEM::InputDataNS
