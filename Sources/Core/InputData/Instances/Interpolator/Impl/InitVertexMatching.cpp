/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 16:19:57 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include <string>

#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Interpolator/Impl/InitVertexMatching.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        namespace Impl
        {
            
            
            namespace InitVertexMatchingInterpolatorNS
            {
                
                
                const std::string& FEltSpaceIndexImpl::NameInFile()
                {
                    static std::string ret("finite_element_space");
                    return ret;
                }
                
                
                const std::string& FEltSpaceIndexImpl::Description()
                {
                    static std::string ret("Finite element space for which the dofs index will be associated to each vertex.");
                    return ret;
                }
                
                
                const std::string& FEltSpaceIndexImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                const std::string& FEltSpaceIndexImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }

                
                
                const std::string& NumberingSubsetIndexImpl::NameInFile()
                {
                    static std::string ret("numbering_subset");
                    return ret;
                }
                
                
                const std::string& NumberingSubsetIndexImpl::Description()
                {
                    static std::string ret("Numbering subsetfor which the dofs index will be associated to each vertex.");
                    return ret;
                }
                
                
                const std::string& NumberingSubsetIndexImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                const std::string& NumberingSubsetIndexImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }

                
                
                
            } // namespace InitVertexMatchingInterpolatorNS
            
            
        } // namespace Impl
        
        
    } // namespace InputDataNS
  

} // namespace MoReFEM


/// @} // addtogroup CoreGroup
