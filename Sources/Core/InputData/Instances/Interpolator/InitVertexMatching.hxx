/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 16:19:57 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_INIT_VERTEX_MATCHING_HXX_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_INIT_VERTEX_MATCHING_HXX_


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<unsigned int IndexT>
        const std::string& InitVertexMatchingInterpolator<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("InitVertexMatchingInterpolator", IndexT);
            return ret;
        };


        template<unsigned int IndexT>
        constexpr unsigned int InitVertexMatchingInterpolator<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }




    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_INIT_VERTEX_MATCHING_HXX_
