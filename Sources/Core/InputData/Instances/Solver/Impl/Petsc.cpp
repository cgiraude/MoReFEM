/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 15:17:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


# include "Core/InputData/Instances/Solver/Impl/Petsc.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        namespace Impl
        {
            
            
            namespace PetscNS
            {
                
                
                const std::string& Solver::NameInFile()
                {
                    static std::string ret("solver");
                    return ret;
                }
                
                
                const std::string& Solver::Description()
                {
                    static std::string ret("Solver to use."); //chebychev cg gmres preonly bicg python };\n\t-- To use Mumps choose preonly.");
                    return ret;
                }
                
                const std::string& Solver::Constraint()
                {
                    static std::string ret("value_in(v, { 'Mumps', 'Umfpack', 'Gmres' })"); // 'chebychev', 'umfpack', 'cg', 'gmres', 'preonly', 'bicg', 'python'})");
                    return ret;
                }
                
                
                const std::string& Solver::DefaultValue()
                {
                    static std::string ret("'Mumps'");
                    return ret;
                }
                
                
                const std::string& GmresRestart::NameInFile()
                {
                    static std::string ret("gmresRestart");
                    return ret;
                }
                
                
                const std::string& GmresRestart::Description()
                {
                    static std::string ret("gmresStart");
                    return ret;
                }
                
                const std::string& GmresRestart::Constraint()
                {
                    static std::string ret("v >= 0");
                    return ret;
                }
                
                
                
                const std::string& GmresRestart::DefaultValue()
                {
                    static std::string ret("200");
                    return ret;
                }
                
                
                const std::string& Preconditioner::NameInFile()
                {
                    static std::string ret("preconditioner");
                    return ret;
                }
                
                
                const std::string& Preconditioner::Description()
                {
                    static std::string ret("Preconditioner to use. Must be lu for any direct solver.");
                    return ret;
                }
                
                const std::string& Preconditioner::Constraint()
                {
                    static std::string ret("value_in(v, {'lu', 'none'})"); // none', 'jacobi', 'sor', 'lu', 'bjacobi', 'ilu', 'asm', 'cholesky'})");
                    return ret;
                }
                
                
                const std::string& Preconditioner::DefaultValue()
                {
                    static std::string ret("'lu'");
                    return ret;
                }
                
                
                const std::string& RelativeTolerance::NameInFile()
                {
                    static std::string ret("relativeTolerance");
                    return ret;
                }
                
                
                const std::string& RelativeTolerance::Description()
                {
                    static std::string ret("Relative tolerance");
                    return ret;
                }
                
                const std::string& RelativeTolerance::Constraint()
                {
                    static std::string ret("v > 0.");
                    return ret;
                }
                
                
                
                const std::string& RelativeTolerance::DefaultValue()
                {
                    static std::string ret("1e-9");
                    return ret;
                }
                
                
                const std::string& AbsoluteTolerance::NameInFile()
                {
                    static std::string ret("absoluteTolerance");
                    return ret;
                }
                
                
                const std::string& AbsoluteTolerance::Description()
                {
                    static std::string ret("Absolute tolerance");
                    return ret;
                }
                
                const std::string& AbsoluteTolerance::Constraint()
                {
                    static std::string ret("v > 0.");
                    return ret;
                }
                
                
                
                const std::string& AbsoluteTolerance::DefaultValue()
                {
                    static std::string ret("1e-50");
                    return ret;
                }
                
                
                const std::string& StepSizeTolerance::NameInFile()
                {
                    static std::string ret("stepSizeTolerance");
                    return ret;
                }
                
                
                const std::string& StepSizeTolerance::Description()
                {
                    static std::string ret("Step size tolerance");
                    return ret;
                }
                
                const std::string& StepSizeTolerance::Constraint()
                {
                    static std::string ret("v > 0.");
                    return ret;
                }
                
                
                const std::string& StepSizeTolerance::DefaultValue()
                {
                    static std::string ret("1e-8");
                    return ret;
                }
                
                
                const std::string& MaxIteration::NameInFile()
                {
                    static std::string ret("maxIteration");
                    return ret;
                }
                
                
                const std::string& MaxIteration::Description()
                {
                    static std::string ret("Maximum iteration");
                    return ret;
                }
                
                const std::string& MaxIteration::Constraint()
                {
                    static std::string ret("v > 0");
                    return ret;
                }
                
                
                const std::string& MaxIteration::DefaultValue()
                {
                    static std::string ret("1000");
                    return ret;
                }
                
                
            } // namespace PetscNS
            
            
        } // namespace Impl
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup

