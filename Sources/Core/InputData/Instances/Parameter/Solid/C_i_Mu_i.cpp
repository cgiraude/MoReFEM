/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 15:31:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        const std::string& Solid::C0::GetName()
        {
            static std::string ret("C0");
            return ret;
        }
        
        
        const std::string& Solid::C1::GetName()
        {
            static std::string ret("C1");
            return ret;
        }
        
        
        const std::string& Solid::C2::GetName()
        {
            static std::string ret("C2");
            return ret;
        }
        
        
        const std::string& Solid::C3::GetName()
        {
            static std::string ret("C3");
            return ret;
        }
        
        
        const std::string& Solid::C4::GetName()
        {
            static std::string ret("C4");
            return ret;
        }
        
        
        const std::string& Solid::C5::GetName()
        {
            static std::string ret("C5");
            return ret;
        }
        
        
        const std::string& Solid::Mu1::GetName()
        {
            static std::string ret("Mu1");
            return ret;
        }
        
        
        const std::string& Solid::Mu2::GetName()
        {
            static std::string ret("Mu2");
            return ret;
        }
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
