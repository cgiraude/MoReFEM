/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_USUAL_DESCRIPTION_HXX_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_USUAL_DESCRIPTION_HXX_


namespace MoReFEM
{


    namespace Advanced::InputDataNS::ParamNS
    {


        template<IsVectorial IsVectorialT>
        const std::string& Nature<IsVectorialT>::NameInFile()
        {
            static std::string ret("nature");
            return ret;
        }


        template<IsVectorial IsVectorialT>
        const std::string& Nature<IsVectorialT>::Description()
        {
            if constexpr (IsVectorialT == IsVectorial::yes)
            {
                static std::string ret("How is given each component of the parameter (as a constant, as a Lua function"
                                       ", per quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                       "parameter (in this case it will stay at nullptr).");
                return ret;
            }
            else
            {
                static std::string ret("How is given the parameter (as a constant, as a Lua function, per "
                                       "quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                       "parameter (in this case it will stay at nullptr).");
                return ret;
            }

        }


        template<IsVectorial IsVectorialT>
        const std::string& Nature<IsVectorialT>::Constraint()
        {
            static std::string ret("value_in(v, {"
                                   "'ignore', "
                                   "'constant', "
                                   "'lua_function',"
                                   "'piecewise_constant_by_domain'})");
            return ret;
        }


        template<IsVectorial IsVectorialT>
        const std::string& Nature<IsVectorialT>::DefaultValue()
        {
            if constexpr (IsVectorialT == IsVectorial::yes)
            {
                static std::string ret("{}");
                return ret;
            }
            else
                return Utilities::EmptyString();
        }


        template<class VariantT>
        VariantT SelectorForComponent(const std::string& nature)
        {
            static_assert(Utilities::IsSpecializationOf<std::variant, VariantT>());

            if (nature == "constant")
                return double(); //std::variant_alternative_t<0, storage_type>();
            else if (nature == "lua_function")
                return spatial_lua_function();// std::variant_alternative_t<1, storage_type>();
            else if (nature == "piecewise_constant_by_domain")
                return std::map<unsigned int, double>(); // smtd::variant_alternative_t<2, storage_type>();
            else if (nature == "ignore")
                return nullptr;
            else
            {
                std::cerr << "Choice '" << nature << "' is invalid!" << std::endl;
                assert(false && "Possible choices should all be dealt with here; the possibilities should have been "
                       "checked by the Constraint in NatureT.");
                exit(EXIT_FAILURE);
            }
        }


        template <class NatureT, IsVectorial IsVectorialT>
        template<class InputDataT>
        typename Value<NatureT, IsVectorialT>::storage_type Value<NatureT, IsVectorialT>
        ::Selector(const InputDataT* input_data)
        {
            namespace IPL = Utilities::InputDataNS;

            auto selector = IPL::Extract<NatureT>::Value(*input_data);

            if constexpr(IsVectorialT == IsVectorial::no)
            {
                using type = typename Value<NatureT, IsVectorialT>::storage_type;
                return SelectorForComponent<type>(selector);
            }
            else
            {
                static_assert(std::is_same<decltype(selector), std::vector<std::string>>());
                assert(selector.size() == 3ul);
                using type = typename Value<NatureT, IsVectorialT>::storage_type::value_type;

                return {
                    SelectorForComponent<type>(selector[0]),
                    SelectorForComponent<type>(selector[1]),
                    SelectorForComponent<type>(selector[2])
                };

            }
        }


        template <class NatureT, IsVectorial IsVectorialT>
        const std::string& Value<NatureT, IsVectorialT>::NameInFile()
        {
            static std::string ret("value");
            return ret;
        }


        template <class NatureT, IsVectorial IsVectorialT>
        const std::string& Value<NatureT, IsVectorialT>::Description()
        {
            static std::string ret = Impl::SelectorDescription<IsVectorialT>();
            return ret;
        }


        template <class NatureT, IsVectorial IsVectorialT>
        const std::string& Value<NatureT, IsVectorialT>::Constraint()
        {
            return Utilities::EmptyString();
        }


        template <class NatureT, IsVectorial IsVectorialT>
        const std::string& Value<NatureT, IsVectorialT>::DefaultValue()
        {
            if constexpr(IsVectorialT == IsVectorial::yes)
            {
                static std::string ret("{}");
                return ret;
            }
            else
                return Utilities::EmptyString();
        }


        namespace Impl
        {


            template<IsVectorial IsVectorialT>
            std::string SelectorDescription()
            {
                std::ostringstream oconv;

                decltype(auto) empty_str = Utilities::EmptyString();

                if constexpr(IsVectorialT == IsVectorial::yes)
                    oconv << "The values of the vectorial parameter; expected format is a table (opening = '{', "
                    "closing = '} and separator = ',') and each item depends on the nature specified at the "
                    "namesake field:\n";
                else
                    oconv << "The value for the parameter, which type depends directly on the nature chosen in the "
                    "namesake field:\n";

                oconv << "\n If nature is 'constant', expected format is "
                << Internal::InputDataNS::Traits::Format<double>::Print(empty_str);
                oconv << "\n If nature is 'piecewise_constant_by_domain', expected format is "
                << Internal::InputDataNS::Traits::Format<std::map<unsigned int, double>>::Print(empty_str);
                oconv << "\n If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block:\n"
                "[[\n"
                "function(x, y, z) \n"
                "return x + y - z\n"
                "end\n"
                "]]\n"
                "where x, y and z are global coordinates. "
                "sin, cos, tan, exp and so forth require a 'math.' preffix.\n";

                std::string ret(oconv.str());
                return ret;
            }


        } // namespace Impl


    } // namespace Advanced::InputDataNS::ParamNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_USUAL_DESCRIPTION_HXX_
