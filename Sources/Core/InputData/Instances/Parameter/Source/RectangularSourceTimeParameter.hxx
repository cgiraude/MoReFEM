/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Sep 2015 15:34:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HXX_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HXX_


namespace MoReFEM
{


    namespace InputDataNS
    {

        template<unsigned int IndexT>
        constexpr unsigned int RectangularSourceTimeParameter<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }


        template<unsigned int IndexT>
        const std::string& RectangularSourceTimeParameter<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("RectangularSourceTimeParameter", IndexT);
            return ret;
        }


        template<unsigned int IndexT>
        typename RectangularSourceTimeParameter<IndexT>::section_content_type&
        RectangularSourceTimeParameter<IndexT>::GetNonCstSectionContent() noexcept
        {
            return section_content_;
        }


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HXX_
