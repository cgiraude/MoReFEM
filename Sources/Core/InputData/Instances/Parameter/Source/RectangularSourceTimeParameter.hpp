/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HPP_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HPP_




# include "Core/InputData/Instances/Crtp/Section.hpp"

# include "Core/InputData/Instances/Parameter/Source/Impl/RectangularSourceTimeParameter.hpp"


namespace MoReFEM
{


    namespace InputDataNS
    {


        namespace BaseNS
        {


            /*!
             * \brief Common base class from which all InputData::RectangularSourceTimeParameter should inherit.
             *
             * This brief class is used to tag domains within the input parameter data (through std::is_base_of<>).
             */
            class RectangularSourceTimeParameter
            { };


        } // namespace BaseNS


        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<unsigned int IndexT>
        class RectangularSourceTimeParameter
        : public Crtp::Section<RectangularSourceTimeParameter<IndexT>, NoEnclosingSection>,
        public BaseNS::RectangularSourceTimeParameter
        {
        public:

            //! Return the unique id (i.e. 'IndexT').
            static constexpr unsigned int GetUniqueId() noexcept;

            //! Return the name of the section in the input parameter.
            static const std::string& GetName();

            //! Convenient alias.
            using self = RectangularSourceTimeParameter<IndexT>;


            //! Friendship to section parent.
            using parent = Crtp::Section<self, NoEnclosingSection>;

                //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN



            //! Initial time of the horizontal source in ReactionDiffusion.
            struct InitialTimeOfActivationList : public Crtp::InputData<InitialTimeOfActivationList, self, double>,
                                                 public Impl::RectangularSourceTimeParameterNS::InitialTimeOfActivationList
            { };


            //! Initial time of the horizontal source in ReactionDiffusion.
            struct FinalTimeOfActivationList : public Crtp::InputData<FinalTimeOfActivationList, self, double>,
                                               public Impl::RectangularSourceTimeParameterNS::FinalTimeOfActivationList
            { };


            //! Alias to the tuple of structs.
            using section_content_type = std::tuple
            <
                InitialTimeOfActivationList,
                FinalTimeOfActivationList
            >;

            //! Non constant accessor to the section content.
            section_content_type& GetNonCstSectionContent() noexcept;

        private:

            //! Content of the section.
            section_content_type section_content_;




        }; // struct RectangularSourceTimeParameter


    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Source/RectangularSourceTimeParameter.hxx"


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HPP_
