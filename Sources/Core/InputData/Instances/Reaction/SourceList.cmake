### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/ReactionCoefficient.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/FitzHughNagumo.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/MitchellSchaeffer.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/ReactionCoefficient.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ReactionCoefficient.hxx"
)

