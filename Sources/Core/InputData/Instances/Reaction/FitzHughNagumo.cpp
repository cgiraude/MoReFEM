/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 1 Jul 2015 11:51:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Reaction/FitzHughNagumo.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        namespace ReactionNS
        {
            
 
            
            const std::string& FitzHughNagumo::GetName()
            {
                static std::string ret("ReactionFitzHughNagumo");
                return ret;
            };

            
            
            const std::string& FitzHughNagumo::ACoefficient::NameInFile()
            {
                static std::string ret("a");
                return ret;
            }
            
            
            const std::string& FitzHughNagumo::ACoefficient::Description()
            {
                static std::string ret("Value of a in FitzHughNagumo.");
                return ret;
            }
            
            const std::string& FitzHughNagumo::ACoefficient::Constraint()
            {
                return Utilities::EmptyString();
            }
            
            
            const std::string& FitzHughNagumo::ACoefficient::DefaultValue()
            {
                static std::string ret("0.08");
                return ret;
            }
            
            
            const std::string& FitzHughNagumo::BCoefficient::NameInFile()
            {
                static std::string ret("b");
                return ret;
            }
            
            
            const std::string& FitzHughNagumo::BCoefficient::Description()
            {
                static std::string ret("Value of b in FitzHughNagumo.");
                return ret;
            }
            
            const std::string& FitzHughNagumo::BCoefficient::Constraint()
            {
                return Utilities::EmptyString();
            }
            
            
            const std::string& FitzHughNagumo::BCoefficient::DefaultValue()
            {
                static std::string ret("0.7");
                return ret;
            }
            
                        
            const std::string& FitzHughNagumo::CCoefficient::NameInFile()
            {
                static std::string ret("c");
                return ret;
            }
            
            
            const std::string& FitzHughNagumo::CCoefficient::Description()
            {
                static std::string ret("Value of c in FitzHughNagumo.");
                return ret;
            }
            
            const std::string& FitzHughNagumo::CCoefficient::Constraint()
            {
                return Utilities::EmptyString();
            }
            
            
            const std::string& FitzHughNagumo::CCoefficient::DefaultValue()
            {
                static std::string ret("0.8");
                return ret;
            }
            
            
        } // namespace ReactionNS
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
