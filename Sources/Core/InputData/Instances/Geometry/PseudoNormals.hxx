/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_PSEUDO_NORMALS_HXX_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_PSEUDO_NORMALS_HXX_


namespace MoReFEM
{


    namespace InputDataNS
    {


        template<unsigned int IndexT>
        const std::string& PseudoNormals<IndexT>::GetName()
        {
            static std::string ret = Impl::GenerateSectionName("PseudoNormals", IndexT);
            return ret;
        };


        template<unsigned int IndexT>
        constexpr unsigned int PseudoNormals<IndexT>::GetUniqueId() noexcept
        {
            return IndexT;
        }



    } // namespace InputDataNS


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_PSEUDO_NORMALS_HXX_
