### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Domain.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormals.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Domain.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mesh.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/PseudoNormals.hpp"
)

