/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 15:14:35 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/Containers/Print.hpp"

#include "Core/InputData/Instances/Geometry/Impl/Domain.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        namespace Impl
        {
            
            
            namespace DomainNS
            {
                
                  
                const std::string& MeshIndexList::NameInFile()
                {
                    static std::string ret("mesh_index");
                    return ret;
                }
                
                
                const std::string& MeshIndexList::Description()
                {
                    static std::string ret("Index of the geometric mesh upon which the domain is defined (as defined "
                                           "in the present file). Might be left empty if domain not limited to one mesh; "
                                           "at most one value is expected here.");
                                           
                    return ret;
                }
                
                
                const std::string& MeshIndexList::Constraint()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                
                const std::string& MeshIndexList::DefaultValue()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                const std::string& DimensionList::NameInFile()
                {
                    static std::string ret("dimension_list");
                    return ret;
                }
                
                
                const std::string& DimensionList::Description()
                {
                    static std::string ret("List of dimensions encompassed by the domain. Might be left empty if no "
                                           "restriction at all upon dimensions.");
                    return ret;
                }
                
                
                const std::string& DimensionList::Constraint()
                {
                    static std::string ret("value_in(v, {0, 1, 2, 3})");
                    return ret;
                }
                
                
                
                const std::string& DimensionList::DefaultValue()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                const std::string& MeshLabelList::NameInFile()
                {
                    static std::string ret("mesh_label_list");
                    return ret;
                }
                
                
                const std::string& MeshLabelList::Description()
                {
                    static std::string ret("List of mesh labels encompassed by the domain. Might be left empty if no "
                                           "restriction at all upon mesh labels. This parameter does not make sense "
                                           "if no mesh is defined for the domain.");
        
                    return ret;
                }
                
                
                const std::string& MeshLabelList::Constraint()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                
                const std::string& MeshLabelList::DefaultValue()
                {
                    static std::string ret;
                    return ret;
                }
                
                
                const std::string& GeomEltTypeList::NameInFile()
                {
                    static std::string ret("geometric_element_type_list");
                    return ret;
                }
                
                
                
                const std::string& GeomEltTypeList::Description()
                {
                    static std::string ret("List of geometric element types considered in the domain. Might be left "
                                           "empty if no restriction upon these. No constraint is applied at \a LuaOptionFile level, "
                                           "as some geometric element types could be added after generation of current "
                                           "input data file. Current list is below; if an incorrect value is "
                                           "put there it will be detected a bit later when the domain object is built.\n "
                                           "The known types when this file was generated are: \n "
                                           ". Point1\n "
                                           ". Segment2, Segment3\n "
                                           ". Triangle3, Triangle6\n "
                                           ". Quadrangle4, Quadrangle8, Quadrangle9\n "
                                           ". Tetrahedron4, Tetrahedron10\n "
                                           ". Hexahedron8, Hexahedron20, Hexahedron27.");
                    return ret;
                }
                
                
                const std::string& GeomEltTypeList::Constraint()
                {

                    static std::string ret;
                    return ret;
                }
                
                
                
                const std::string& GeomEltTypeList::DefaultValue()
                {
                    static std::string ret;
                    return ret;
                }
                
                
            } // namespace DomainNS
            
            
        } // namespace Impl
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
