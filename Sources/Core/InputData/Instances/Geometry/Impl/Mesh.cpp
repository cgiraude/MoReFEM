/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 14:48:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/Geometry/Impl/Mesh.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        namespace Impl
        {
            
            
            namespace MeshNS
            {
                
                
                const std::string& PathImpl::NameInFile()
                {
                    static std::string ret("mesh");
                    return ret;
                }
                
                
                const std::string& PathImpl::Description()
                {
                    static std::string ret("Path of the mesh file to use.");
                    return ret;
                }
                
                
                const std::string& PathImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                const std::string& PathImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }
                
                
                const std::string& FormatImpl::NameInFile()
                {
                    static std::string ret("format");
                    return ret;
                }
                
                const std::string& FormatImpl::Description()
                {
                    static std::string ret("Format of the input mesh.");
                    return ret;
                }
                
                
                const std::string& FormatImpl::Constraint()
                {
                    static std::string ret("value_in(v, {'Ensight', 'Medit'})");
                    return ret;
                }
                
                
                const std::string& FormatImpl::DefaultValue()
                {
                    static std::string ret("\"Medit\"");
                    return ret;
                }
                
                
                const std::string& DimensionImpl::NameInFile()
                {
                    static std::string ret("dimension");
                    return ret;
                }
                
                
                const std::string& DimensionImpl::Description()
                {
                    static std::string ret("Highest dimension of the input mesh. This dimension might be lower than the one "
                                           "effectively read in the mesh file; in which case Coords will be reduced provided all the dropped "
                                           "values are 0. If not, an exception is thrown.");
                    return ret;
                }
                
                
                const std::string& DimensionImpl::Constraint()
                {
                    static std::string ret("v <= 3 and v > 0");
                    return ret;
                }
                
                
                const std::string& DimensionImpl::DefaultValue()
                {
                    return Utilities::EmptyString();
                }
                
                const std::string& SpaceUnitImpl::NameInFile()
                {
                    static std::string ret("space_unit");
                    return ret;
                }
                
                
                const std::string& SpaceUnitImpl::Description()
                {
                    static std::string ret("Space unit of the mesh.");
                    return ret;
                }
                
                
                const std::string& SpaceUnitImpl::Constraint()
                {
                    return Utilities::EmptyString();
                }
                
                
                const std::string& SpaceUnitImpl::DefaultValue()
                {
                    static std::string ret("1.");
                    return ret;
                }
                
                
            } // namespace MeshNS
            
            
        } // namespace Impl
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
