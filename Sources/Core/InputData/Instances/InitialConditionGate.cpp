/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Feb 2015 14:38:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include "Utilities/String/EmptyString.hpp"

#include "Core/InputData/Instances/InitialConditionGate.hpp"


namespace MoReFEM
{
    
    
    namespace InputDataNS
    {
        
        
        const std::string& InitialConditionGate::GetName()
        {
            static std::string ret("InitialConditionGate");
            return ret;
        }
        
        
        const std::string& InitialConditionGate::Value::NameInFile()
        {
            static std::string ret("initial_condition_gate");
            return ret;
        }
        
        
        const std::string& InitialConditionGate::Value::Description()
        {
            static std::string ret("Value of Initial Condition Gate in ReactionDiffusion.");
            return ret;
        }


        const std::string& InitialConditionGate::Value::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        const std::string& InitialConditionGate::Value::DefaultValue()
        {
            static std::string ret("0.");
            return ret;
        }
      
        
        const std::string& InitialConditionGate::WriteGate::NameInFile()
        {
            static std::string ret("write_gate");
            return ret;
        }
        
        
        const std::string& InitialConditionGate::WriteGate::Description()
        {
            static std::string ret("Either Write the Gate at each time step or not.");
            return ret;
        }

        
        const std::string& InitialConditionGate::WriteGate::Constraint()
        {
            return Utilities::EmptyString();
        }
        
        
        const std::string& InitialConditionGate::WriteGate::DefaultValue()
        {
            static std::string ret("true");
            return ret;
        }
        
        
    } // namespace InputDataNS
    
    
} // namespace MoReFEM


/// @} // addtogroup CoreGroup
