/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 1 Aug 2013 10:44:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INPUT_DATA_HXX_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_INPUT_DATA_HXX_


namespace MoReFEM
{



    template<class TupleT>
    InputData<TupleT>::InputData(const std::string& filename,
                                 const Wrappers::Mpi& mpi,
                                 Utilities::InputDataNS::DoTrackUnusedFields do_track_unused_fields)
    : Utilities::InputDataNS::Base<InputData, TupleT>(filename,
                                                      mpi,
                                                      do_track_unused_fields)
    { }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INPUT_DATA_HXX_
