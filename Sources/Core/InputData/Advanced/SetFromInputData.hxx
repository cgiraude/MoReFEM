/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Dec 2015 11:34:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HXX_
# define MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        template
        <
            class ManagerT,
            class InputDataT,
            typename... Args
        >
        void SetFromInputData(const InputDataT& input_data,
                              ManagerT& manager,
                              Args&&... args)
        {
            auto create = [&manager, &args...](const auto& section) ->void
            {
                manager.Create(section, std::forward<Args>(args)...);
            };

            namespace ipl =  Internal::InputDataNS;

            using input_data_tuple_iteration =
                ipl::TupleIteration
                    <
                        typename InputDataT::Tuple,
                        0,
                        std::tuple_size<typename InputDataT::Tuple>::value
                    >;

            input_data_tuple_iteration
            ::template ActIfSection<typename ManagerT::input_data_type>(input_data.GetTuple(),
                                                                        create);
        }


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HXX_
