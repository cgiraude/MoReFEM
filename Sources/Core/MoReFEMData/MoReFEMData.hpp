/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Jan 2015 15:50:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_
# define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_

# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
# include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
# include "ThirdParty/Wrappers/Petsc/Print.hpp"
# include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"

# include "Utilities/Filesystem/File.hpp"
# include "Utilities/Filesystem/Directory.hpp"
# include "Utilities/TimeKeep/TimeKeep.hpp"
# include "Utilities/OutputFormat/OutputFormat.hpp"
# include "Utilities/Datetime/Now.hpp"
# include "Utilities/Exceptions/GracefulExit.hpp"

# include "Core/InputData/Instances/Result.hpp"
# include "Core/InitTimeKeepLog.hpp"
# include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
# include "Core/MoReFEMData/Enum.hpp"
# include "Core/MoReFEMData/Internal/Parallelism.hpp"
# include "Core/MoReFEMData/Internal/Helper.hpp"

# include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
# include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"

namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{




    /*!
     * \brief Init MoReFEM: initialize mpi and read the input data file.
     *
     * \warning As mpi is not assumed to exist until the constructor has done is job, the exceptions there that might
     * happen only on some of the ranks don't lead to a call to MPI_Abort(), which can lead to a dangling program.
     * Make sure each exception is properly communicated to all ranks so that each rank can gracefully throw
     * an exception and hence allow the program to stop properly.
     *
     * \tparam ProgramTypeT Type of the program run. For instance for post-processing there is no removal of the
     * existing result directory, contrary to what happens in model run.
     * \tparam AdditionalCommandLineArgumentsPolicyT Policy if you need additional arguments on the command line.
     * To see a concrete example of this possibility, have a look at Test/Core/MoReFEMData/test_command_line_options.cpp
     * which demonstrate the possibility. If none, use std::false_type.
     *
     * http://tclap.sourceforge.net gives a nice manual of how to add additional argument on the command lines.
     * By default, there is one mandatory argument (--input_data *lua file*) and one optional that might be
     * repeated to define pseudo environment variables (-e KEY=VALUE).
     */
    template
    <
        class InputDataT,
        program_type ProgramTypeT,
        Utilities::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT = Utilities::InputDataNS::DoTrackUnusedFields::yes,
        class AdditionalCommandLineArgumentsPolicyT = std::false_type
    >
    class MoReFEMData : public AdditionalCommandLineArgumentsPolicyT
    {

    public:

        //! Alias to self.
        using self = MoReFEMData<InputDataT, ProgramTypeT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>;

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;

        //! Alias to \a InputDataT.
        using input_data_type = InputDataT;

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] argc Number of argument in the command line (including the program name).
         * \param[in] argv List of arguments read.
         */
        explicit MoReFEMData(int argc, char** argv);

        //! Destructor.
        ~MoReFEMData();

        //! \copydoc doxygen_hide_copy_constructor
        MoReFEMData(const MoReFEMData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MoReFEMData(MoReFEMData&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MoReFEMData& operator=(const MoReFEMData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MoReFEMData& operator=(MoReFEMData&& rhs) = delete;

        ///@}

        //! Accessor to underlying mpi object.
        const Wrappers::Mpi& GetMpi() const noexcept;

        //! Accessor to underlying InputData object.
        const InputDataT& GetInputData() const noexcept;

        /*!
         * \brief Accessor to the result directory, in which all the outputs of MoReFEM should be written.
         *
         * \return Result directory.
         */
        const FilesystemNS::Directory& GetResultDirectory() const noexcept;

        //! Accessor to the object which keeps the data related to parallelism strategy.  Might be nullptr if none specified in the input lua file.
        const Internal::Parallelism* GetParallelismPtr() const noexcept;

    private:

        //! Accessor to the object which keeps the data related to parallelism strategy.
        //! No pointer here, but it assumes parallelism_ is not nullptr (should be called only under the
        //! if constexpr (InputDataT::template Find<InputDataNS::Parallelism>()) condition).
        const Internal::Parallelism& GetParallelism() const noexcept;

    private:

        //! Holds InputData.
        typename InputDataT::const_unique_ptr input_data_ = nullptr;

        //! Directory into which model results will be written,
        FilesystemNS::Directory::const_unique_ptr result_directory_ = nullptr;

        //! Object which holds the parallelism strategy to use.
        Internal::Parallelism::unique_ptr parallelism_ = nullptr;
    };


    /*!
     * \brief Provide the precompute exit.
     *
     * When the parallelism strategy is "precompute", once all the data are written we want to exit the program as
     * soon as possible.
     * The proper way to do so is to throw a GracefulExit exception, which should be properly trapped in the main.
     *
     * This function does so IF the parallelism strategy is "precompute"; if not it does nothing (including the case there are no parallelism policy specified in the
     * Lua file).
     *
     * \copydoc doxygen_hide_morefem_data_param
     */
    template<class MoReFEMDataT>
    void PrecomputeExit(const MoReFEMDataT& morefem_data);


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/MoReFEMData/MoReFEMData.hxx"


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HPP_
