//! \file
//
//
//  Enum.hpp
//  MoReFEM
//
//  Created by sebastien on 31/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ENUM_HPP_
# define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ENUM_HPP_

namespace MoReFEM
{



    //! Whether a model, a test or a post-processing program is run.
    enum class program_type { model, test, post_processing };


} // namespace MoReFEM


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ENUM_HPP_
