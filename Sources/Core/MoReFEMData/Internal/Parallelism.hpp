//! \file
//
//
//  Parallelism.hpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_PARALLELISM_HPP_
# define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_PARALLELISM_HPP_

# include <memory>
# include <string>
# include <cassert>

# include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
# include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"


// ============================
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::Wrappers
{


    class Mpi;


} // namespace MoReFEM::Wrappers


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// ============================


namespace MoReFEM::Internal
{


    /*!
     * \brief Holds the interpreted content of the section 'Parallelism' of the input data file.
     */
    class Parallelism
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = Parallelism;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_input_data_arg
         * \copydetails doxygen_hide_mpi_param
         * \param[in] behaviour Behaviour to use when the subdirectory to create already exist. Irrelevant for policies
         * that only read existing directories.
         */
        template<class InputDataT>
        explicit Parallelism(const ::MoReFEM::Wrappers::Mpi& mpi,
                             const InputDataT& input_data,
                             ::MoReFEM::FilesystemNS::behaviour behaviour);

        //! Destructor.
        ~Parallelism() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Parallelism(const Parallelism& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Parallelism(Parallelism&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Parallelism& operator=(const Parallelism& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Parallelism& operator=(Parallelism&& rhs) = delete;

        ///@}

        //! Get the path of the parallelism directory.
        const ::MoReFEM::FilesystemNS::Directory& GetDirectory() const noexcept;

        //! Get the parallelism strategy.
        ::MoReFEM::Advanced::parallelism_strategy GetParallelismStrategy() const noexcept;

    private:

        //! The chosen parallelism strategy.
        ::MoReFEM::Advanced::parallelism_strategy parallelism_strategy_ =
            ::MoReFEM::Advanced::parallelism_strategy::none;

        //! Path to the parallelism directory.
        ::MoReFEM::FilesystemNS::Directory::const_unique_ptr directory_ = nullptr;

    };


} // namespace MoReFEM::Internal


# include "Core/MoReFEMData/Internal/Parallelism.hxx"


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_PARALLELISM_HPP_
