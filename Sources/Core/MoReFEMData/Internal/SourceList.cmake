### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CheckInvertedElements.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Helper.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/CheckInvertedElements.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/CheckInvertedElements.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/EnumInvertedElements.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Helper.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Helper.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Parallelism.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Parallelism.hxx"
)

