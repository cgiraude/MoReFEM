//
//  CheckInvertedElements.h
//  MoReFEM
//
//  Created by Jerôme Diaz on 10/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_CHECK_INVERTED_ELEMENTS_HXX_
# define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_CHECK_INVERTED_ELEMENTS_HXX_


namespace MoReFEM::Internal::MoReFEMDataNS
{


    inline check_inverted_elements_policy CheckInvertedElements::DoCheckInvertedElements() const
	{
		if (check_inverted_elements_policy_ == check_inverted_elements_policy::unspecified)
			throw Exception("The singleton CheckInvertedElements was not initialised with a proper value "
							"- check there is a dedicated field Solid::CheckInvertedElement in your "
							"input data!", __FILE__, __LINE__);


		return check_inverted_elements_policy_;
	}


} // namepsace MoReFEM::Internal::FEltSpaceNS


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_CHECK_INVERTED_ELEMENTS_HXX_
