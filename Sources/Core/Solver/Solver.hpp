/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Oct 2015 16:00:12 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_
# define MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

# include "Core/InputData/Instances/Solver/Petsc.hpp"
# include "Core/InputData/InputData.hpp"


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    //! Whether a non linear solver might be used in the variational formulation
    enum class enable_non_linear_solver { no, yes };


    /*!
     * \brief Init Petsc solver.
     *
     *
     * \copydetails doxygen_hide_mpi_param
     * \copydoc doxygen_hide_input_data_arg
     * \copydoc doxygen_hide_snes_functions_args
     *
     * \return \a Wrappers::Petsc::Snes correctly initialized.
     */
    template<unsigned int SolverIndexT, class InputDataT>
    Wrappers::Petsc::Snes::unique_ptr InitSolver(const Wrappers::Mpi& mpi,
                                                 const InputDataT& input_data,
                                                 Wrappers::Petsc::Snes::SNESFunction snes_function = nullptr,
                                                 Wrappers::Petsc::Snes::SNESJacobian snes_jacobian = nullptr,
                                                 Wrappers::Petsc::Snes::SNESViewer snes_viewer = nullptr,
                                                 Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function = nullptr);



    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


# include "Core/Solver/Solver.hxx"


#endif // MOREFEM_x_CORE_x_SOLVER_x_SOLVER_HPP_
