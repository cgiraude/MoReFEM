/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 12 Oct 2013 00:02:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PARMETIS_x_PARMETIS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PARMETIS_x_PARMETIS_HPP_

# include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-align")

# include "Utilities/Warnings/Internal/IgnoreWarning/weak-vtables.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/deprecated.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/undef.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/cast-function-type.hpp"

PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

#include "parmetis.h"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PARMETIS_x_PARMETIS_HPP_
