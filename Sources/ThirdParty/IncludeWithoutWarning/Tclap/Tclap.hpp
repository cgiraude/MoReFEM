/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 15 Feb 2017 17:13:49 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_TCLAP_x_TCLAP_HPP_
# define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_TCLAP_x_TCLAP_HPP_

# include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

# include "Utilities/Warnings/Internal/IgnoreWarning/weak-vtables.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/deprecated.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/shadow-field.hpp"
# include "Utilities/Warnings/Internal/IgnoreWarning/extra-semi-stmt.hpp"

PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")

#include "tclap/CmdLine.h"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_TCLAP_x_TCLAP_HPP_
