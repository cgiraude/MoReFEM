/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 13:53:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Tclap
        {


            inline const std::pair<std::string, std::string>& StringPair::GetValue() const noexcept
            {
                return value_;
            }


        } // namespace Tclap


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_TCLAP_x_STRING_PAIR_HXX_
