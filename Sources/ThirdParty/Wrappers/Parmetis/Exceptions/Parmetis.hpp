/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Oct 2013 17:33:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PARMETIS_x_EXCEPTIONS_x_PARMETIS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PARMETIS_x_EXCEPTIONS_x_PARMETIS_HPP_


# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Parmetis
        {


            namespace ExceptionNS
            {


                //! Generic class
                struct Exception : public MoReFEM::Exception
                {
                    /*!
                     * \brief Constructor with simple message.
                     *
                     * \param[in] msg Message
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);


                    /*!
                     * \brief Constructor with simple message.
                     *
                     * \param[in] error_code Error code returned by Parmetis.
                     * \param[in] parmetis_function Name of the Parmetis function that returned the error code.
                      * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                     * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                     */
                    explicit Exception(int error_code, const std::string& parmetis_function,
                                       const char* invoking_file, int invoking_line);

                    //! Destructor
                    virtual ~Exception() override;

                    //! \copydoc doxygen_hide_copy_constructor
                    Exception(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_constructor
                    Exception(Exception&& rhs) = default;

                    //! \copydoc doxygen_hide_copy_affectation
                    Exception& operator=(const Exception& rhs) = default;

                    //! \copydoc doxygen_hide_move_affectation
                    Exception& operator=(Exception&& rhs) = default;
                };


            } // namespace ExceptionNS


        } // namespace Parmetis


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup




#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PARMETIS_x_EXCEPTIONS_x_PARMETIS_HPP_
