//! \file
//
//
//  RAII.hxx
//  MoReFEM
//
//  Created by sebastien on 22/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_INTERNAL_x_R_A_I_I_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_INTERNAL_x_R_A_I_I_HXX_


namespace MoReFEM::Internal::PetscNS
{


    inline const ::MoReFEM::Wrappers::Mpi& RAII::GetMpi() const noexcept
    {
        assert(!(!mpi_));
        return *mpi_;
    }


} // namespace MoReFEM::Internal::PetscNS


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_INTERNAL_x_R_A_I_I_HXX_
