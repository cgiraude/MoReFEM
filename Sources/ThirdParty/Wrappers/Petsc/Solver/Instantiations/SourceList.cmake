### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Gmres.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mumps.cpp"
		"${CMAKE_CURRENT_LIST_DIR}/Umfpack.cpp"

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Gmres.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Gmres.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Mumps.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Mumps.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/Umfpack.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Umfpack.hxx"
)

