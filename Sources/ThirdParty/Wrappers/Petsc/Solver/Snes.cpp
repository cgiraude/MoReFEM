/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:24:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


#include "Utilities/TimeKeep/TimeKeep.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            Snes::Snes(const Mpi& mpi,
                       const std::string& solver_name,
                       const std::string& preconditioner,
                       const unsigned int gmres_restart,
                       const double absolute_tolerance,
                       const double relative_tolerance,
                       const double step_size_tolerance,
                       const unsigned int max_iteration,
                       SNESFunction snes_function,
                       SNESJacobian snes_jacobian,
                       SNESViewer snes_viewer,
                       SNESConvergenceTestFunction snes_convergence_test_function,
                       const char* invoking_file, int invoking_line)
            : mpi_(mpi),
            snes_function_(snes_function),
            snes_jacobian_(snes_jacobian),
            snes_viewer_(snes_viewer),
            snes_convergence_test_function_(snes_convergence_test_function)
            {
                // \todo #730 Improve this ugly switch (by a factory).
                // It is there as it is easier at the moment to deal this way with the different possible constructor
                // arguments; factory could be introduced when InitSolver() that calls current constructor is more
                // refined.
                if (solver_name == "Mumps")
                    solver_ = std::make_unique<Instantiations::Mumps>();
                else if (solver_name == "Gmres")
                    solver_ = std::make_unique<Instantiations::Gmres>(gmres_restart);
                else if (solver_name == "Umfpack")
                {
                    if (mpi.Nprocessor<int>() != 1)
                        throw Exception("Umfpack solver only tackles sequential solve!", invoking_file, invoking_line);
                    
                    solver_ = std::make_unique<Instantiations::Umfpack>();
                }
                else
                {
                    assert(false && "All legit choices should be addressed here!");
                }
                    
                
                const auto& solver = GetSolver();
                
                auto communicator = mpi.GetCommunicator();
                
                // By extracting the  KSP, KSP, and PC contexts from the SNES context, we can then
                // directly call any KSP, KSP, and PC routines to set various options.
                int error_code = SNESCreate(communicator, &snes_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESCreate", invoking_file, invoking_line);
                
                auto ksp = GetKsp(invoking_file, invoking_line);

                error_code = SNESSetFromOptions(snes_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESSetFromOptions", invoking_file, invoking_line);

                // If the solver is a direct one, type must be 'preonly' and preconditioner 'lu'; actual solver
                // used is determined by PCFactorSetMatSolverPackage().
                if (solver.GetSolverType() == solver_type::direct)
                {
                    error_code = KSPSetType(ksp, KSPPREONLY);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "KSPSetType", invoking_file, invoking_line);
                    
                    if (preconditioner != PCLU)
                    {
                        std::ostringstream oconv;
                        oconv << "Error: for a direct solver preconditioner must be " << PCLU << '.';
                        
                        throw Exception(oconv.str(), invoking_file, invoking_line);
                    }
                    
                    auto pc = GetPreconditioner(invoking_file, invoking_line);
                    
                    error_code = PCSetType(pc, preconditioner.c_str());
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "PCSetType", invoking_file, invoking_line);

                    #if PETSC_VERSION_GE(3, 9, 0)
                    error_code = PCFactorSetMatSolverType(pc, solver.GetPetscName().c_str());
                    #else
                    error_code = PCFactorSetMatSolverPackage(pc, solver.GetPetscName().c_str());
                    #endif

                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "PCFactorSetMatSolverType",
                                                     invoking_file, invoking_line);
                }
                else
                {
                    error_code = KSPSetType(ksp, solver.GetPetscName().c_str());
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "KSPSetType", invoking_file, invoking_line);
                    
                                     
                    auto pc = GetPreconditioner(invoking_file, invoking_line);
                    
                    error_code = PCSetType(pc, preconditioner.c_str());
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "PCSetType", invoking_file, invoking_line);
                    
                }
                
                
                GetNonCstSolver().SupplInitOptions(*this, invoking_file, invoking_line);
                
                error_code = KSPSetTolerances(ksp,
                                              static_cast<PetscReal>(relative_tolerance),
                                              static_cast<PetscReal>(absolute_tolerance),
                                              PETSC_DEFAULT,
                                              static_cast<PetscInt>(max_iteration));
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPSetTolerances", invoking_file, invoking_line);
                
                error_code = SNESSetTolerances(snes_,
                                               static_cast<PetscReal>(absolute_tolerance),
                                               static_cast<PetscReal>(relative_tolerance),
                                               static_cast<PetscReal>(step_size_tolerance),
                                               static_cast<PetscInt>(max_iteration),
                                               PETSC_DEFAULT);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESSetTolerances", invoking_file, invoking_line);
                
                auto pc = GetPreconditioner(invoking_file, invoking_line);
                error_code = PCFactorSetReuseFill(pc, PETSC_FALSE);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PCFactorSetReuseFill", invoking_file, invoking_line);
            }
            
            
            Snes::~Snes()
            {
                int error_code = SNESDestroy(&snes_);
                assert(error_code == 0); // Can't throw exception in a destructor.
                static_cast<void>(error_code); // no compilation warning in release mode.
            }

            
            void Snes::SolveNonLinear(void* context,
                                      const Vector& rhs,
                                      const Matrix& jacobian_matrix,
                                      const Matrix& preconditioner_matrix,
                                      Vector& solution,
                                      const char* invoking_file,
                                      int invoking_line,
                                      check_convergence do_check_convergence)
            {
                int error_code = SNESSetFunction(snes_, rhs.Internal(), GetSnesFunction(), context);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESSetFunction", invoking_file, invoking_line);

                error_code = SNESSetJacobian(snes_,
                                             jacobian_matrix.Internal(),
                                             preconditioner_matrix.Internal(),
                                             GetSnesJacobian(),
                                             context);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESSetJacobian", invoking_file, invoking_line);
                
                if (GetSnesConvergenceTestFunction() != nullptr)
                {
                    error_code = SNESSetConvergenceTest(snes_,
                                                        GetSnesConvergenceTestFunction(),
                                                        context,
                                                        nullptr);
                
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "SNESSetConvergenceTest", invoking_file, invoking_line);
                }
                
                error_code = SNESMonitorSet(snes_, GetSnesViewer(), context, PETSC_NULL);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESMonitorSet", invoking_file, invoking_line);


                error_code = SNESSolve(snes_, nullptr, solution.Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESSolve", invoking_file, invoking_line);
                
                if (do_check_convergence == check_convergence::yes)
                {
                    const auto convergence = GetNonLinearConvergenceReason(invoking_file, invoking_line);
                
                    switch(convergence)
                    {
                        case convergence_status::yes:
                        case convergence_status::unspecified:
                            break;
                        case convergence_status::no:
                        {
                            throw Exception(std::string("Newton solve didn't converge due to: ") + *convergence_reason_,
                                            invoking_file, invoking_line);
                        }
                        case convergence_status::pending:
                        {
                            assert(false && "Should never happen: this call is explicitly placed after SNESSolve is done!");
                            exit(EXIT_FAILURE);
                        }
                    }
                }
            }
            
            
            KSP Snes::GetKsp(const char* invoking_file, int invoking_line) const
            {
                KSP ksp;
                int error_code = SNESGetKSP(snes_, &ksp);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESGetKSP", invoking_file, invoking_line);
                return ksp;
            }
            
            
            PC Snes::GetPreconditioner(const char* invoking_file, int invoking_line) const
            {
                PC preconditioner;
                
                auto ksp = GetKsp(invoking_file, invoking_line);
                
                // NOTE: There is a Petsc a SNESGetPC but weirdly enough it returns a SNES object rather than a PC one.
                // KSPGetPC on the other hand returns correctly the expected type.
                int error_code = KSPGetPC(ksp, &preconditioner);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPGetPC", invoking_file, invoking_line);
                
                return preconditioner;
            }
            
            
            void Snes::SolveLinear(const Vector& rhs,
                                   Vector& solution,
                                   const char* invoking_file, int invoking_line,
                                   print_solver_infos do_print_solver_infos)
            {
                KSP ksp = GetKsp(invoking_file, invoking_line);
                
                /*if (do_print_solver_infos == print_solver_infos::yes)
                {
                    KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);
                }*/
                
                int error_code = ::KSPSolve(ksp, rhs.Internal(), solution.Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPSolve", invoking_file, invoking_line);
                
                const auto convergence = GetLinearConvergenceReason(invoking_file, invoking_line);
                
                switch(convergence)
                {
                    case convergence_status::yes:
                    case convergence_status::unspecified:
                        break;
                    case convergence_status::no:
                    {
                        throw Exception(std::string("Linear solve didn't converge due to: ") + *convergence_reason_,
                                        invoking_file, invoking_line);
                    }
                    case convergence_status::pending:
                    {
                        assert(false && "Should never happen: this call is explicitly placed after KSPSolve is done!");
                        exit(EXIT_FAILURE);
                    }
                }
                
                if (do_print_solver_infos == print_solver_infos::yes)
                {
                    PetscInt Niteration;
                    error_code = KSPGetIterationNumber(ksp, &Niteration);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "KSPGetIterationNumber", invoking_file, invoking_line);
                    
                    PetscReal residual;
                    error_code = KSPGetResidualNorm(ksp, &residual);
                    if (error_code)
                        throw ExceptionNS::Exception(error_code, "KSPGetResidualNorm", invoking_file, invoking_line);
                    
                    PrintMessageOnFirstProcessor("%s-%s converged in %d iterations (residual = %e)\n",
                                                 GetMpi(),
                                                 invoking_file, invoking_line,
                                                 GetKspType(invoking_file, invoking_line).c_str(),
                                                 GetPreconditionerType(invoking_file, invoking_line).c_str(),
                                                 static_cast<int>(Niteration),
                                                 static_cast<double>(residual));
                    
                    GetSolver().SupplPrintSolverInfos(*this, invoking_file, invoking_line);
                }
            }

            
            unsigned int Snes::GetSnesIteration(const char* invoking_file, int invoking_line) const
            {
                PetscInt value;
                int error_code = SNESGetLinearSolveIterations(snes_, &value);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESGetLinearSolveIterations",
                                                 invoking_file, invoking_line);
                
                return static_cast<unsigned int>(value);
            }
            
            
            const Mpi& Snes::GetMpi() const
            {
                return mpi_;
            }
            
            
            std::string Snes::GetKspType(const char* invoking_file, int invoking_line) const
            {
                auto ksp = GetKsp(invoking_file, invoking_line);
                
                KSPType type;
                int error_code = KSPGetType(ksp, &type);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPGetType", invoking_file, invoking_line);
                
                return std::string(type);
            }
            
            
            std::string Snes::GetPreconditionerType(const char* invoking_file, int invoking_line) const
            {
                auto ksp = GetPreconditioner(invoking_file, invoking_line);
                
                PCType type;
                int error_code = PCGetType(ksp, &type);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "PCGetType", invoking_file, invoking_line);
                
                return std::string(type);
            }
            
            
            SNESConvergedReason Snes::GetSnesConvergenceReason(const char* invoking_file, int invoking_line) const
            {
                SNESConvergedReason reason;
                
                int error_code = SNESGetConvergedReason(snes_, &reason);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESGetConvergedReason", invoking_file, invoking_line);
                
                return reason;
            }
            
            
            std::string Snes::GetSnesType(const char* invoking_file, int invoking_line) const
            {
                SNESType type;
                int error_code = SNESGetType(snes_ , &type);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "GetSnesType", invoking_file, invoking_line);
                
                return std::string(type);
            }
            
            
            convergence_status Snes::GetNonLinearConvergenceReason(const char* invoking_file, int invoking_line) const
            {
                SNESConvergedReason reason;
                
                int error_code = SNESGetConvergedReason(snes_, &reason);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESGetConvergedReason", invoking_file, invoking_line);
                
                switch(reason)
                {
                    case SNES_CONVERGED_FNORM_ABS:
                        return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_FNORM_ABS>();
                    case SNES_CONVERGED_FNORM_RELATIVE:
                        return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_FNORM_RELATIVE>();
                    case SNES_CONVERGED_SNORM_RELATIVE:
                        return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_SNORM_RELATIVE>();
                    case SNES_CONVERGED_ITS:
                        return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_ITS>();
					#if PETSC_VERSION_LT(3, 12, 0)
                    case SNES_CONVERGED_TR_DELTA:
                        return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_TR_DELTA>();
					#endif
                    case SNES_DIVERGED_FUNCTION_DOMAIN:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_FUNCTION_DOMAIN>();
                    case SNES_DIVERGED_FUNCTION_COUNT:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_FUNCTION_COUNT>();
                    case SNES_DIVERGED_LINEAR_SOLVE:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_LINEAR_SOLVE>();
                    case SNES_DIVERGED_FNORM_NAN:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_FNORM_NAN>();
                    case SNES_DIVERGED_MAX_IT:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_MAX_IT>();
                    case SNES_DIVERGED_LINE_SEARCH:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_LINE_SEARCH>();
                    case SNES_DIVERGED_INNER:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_INNER>();
                    case SNES_DIVERGED_LOCAL_MIN:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_LOCAL_MIN>();
                    case SNES_CONVERGED_ITERATING:
                        return GetNonLinearConvergenceReasonHelper<SNES_CONVERGED_ITERATING>();
                    #if PETSC_VERSION_GE(3, 8, 0)
                    case SNES_DIVERGED_DTOL:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_DTOL>();
                    #endif // PETSC_VERSION_GE(3, 8, 0)
                    #if PETSC_VERSION_GE(3, 11, 0)
                    case SNES_DIVERGED_JACOBIAN_DOMAIN:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_JACOBIAN_DOMAIN>();
                    #endif // PETSC_VERSION_GE(3, 11, 0)
                    #if PETSC_VERSION_GE(3, 12, 0)
                    case SNES_DIVERGED_TR_DELTA:
                        return GetNonLinearConvergenceReasonHelper<SNES_DIVERGED_TR_DELTA>();
                    #endif // PETSC_VERSION_GE(3, 12, 0)


                    // You might have to add or remove an entry after updating Petsc version; look after the warnings
                    // about missing switch entry!
                }
                
                assert(false && "Make sure all entries are properly covered!");
                exit(EXIT_FAILURE);
            }
            
                
            convergence_status Snes::GetNonLinearConvergenceReason(std::string& explanation,
                                                                   const char* invoking_file, int invoking_line) const
            {
                const auto ret = GetNonLinearConvergenceReason(invoking_file, invoking_line);
                explanation = *convergence_reason_;
                return ret;
            }
            
            
            convergence_status Snes::GetLinearConvergenceReason(const char* invoking_file, int invoking_line) const
            {
                KSPConvergedReason reason;
                
                int error_code = KSPGetConvergedReason(GetKsp(invoking_file, invoking_line),
                                                       &reason);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "KSPGetConvergedReason", invoking_file, invoking_line);
                
                switch(reason)
                {
                    case KSP_CONVERGED_RTOL_NORMAL:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_RTOL_NORMAL>();
                    case KSP_CONVERGED_ATOL_NORMAL:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ATOL_NORMAL>();
                    case KSP_CONVERGED_RTOL:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_RTOL>();
                    case KSP_CONVERGED_ATOL:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ATOL>();
                    case KSP_CONVERGED_ITS:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ITS>();
                    case KSP_CONVERGED_CG_NEG_CURVE:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_CG_NEG_CURVE>();
                    case KSP_CONVERGED_CG_CONSTRAINED:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_CG_CONSTRAINED>();
                    case KSP_CONVERGED_STEP_LENGTH:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_STEP_LENGTH>();
                    case KSP_CONVERGED_HAPPY_BREAKDOWN:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_HAPPY_BREAKDOWN>();
                    case KSP_DIVERGED_NULL:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_NULL>();
                    case KSP_DIVERGED_ITS:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_ITS>();
                    case KSP_DIVERGED_DTOL:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_DTOL>();
                    case KSP_DIVERGED_BREAKDOWN:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_BREAKDOWN>();
                    case KSP_DIVERGED_BREAKDOWN_BICG:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_BREAKDOWN_BICG>();
                    case KSP_DIVERGED_NONSYMMETRIC:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_NONSYMMETRIC>();
                    case KSP_DIVERGED_INDEFINITE_PC:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_INDEFINITE_PC>();
                    case KSP_DIVERGED_NANORINF:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_NANORINF>();
                    case KSP_DIVERGED_INDEFINITE_MAT:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_INDEFINITE_MAT>();
                    #if PETSC_VERSION_LT(3, 11, 0)
                    case KSP_DIVERGED_PCSETUP_FAILED:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_PCSETUP_FAILED>();
                    #endif
                    #if PETSC_VERSION_GE(3, 11, 0)
                    case KSP_DIVERGED_PC_FAILED:
                        return GetLinearConvergenceReasonHelper<KSP_DIVERGED_PC_FAILED>();
                    #endif // PETSC_VERSION_GE(3, 11, 0)
                    case KSP_CONVERGED_ITERATING:
                        return GetLinearConvergenceReasonHelper<KSP_CONVERGED_ITERATING>();
                        // You might have to add or remove an entry after updating Petsc version; look after the warnings
                        // about missing switch entry!
                }
                
                assert(false && "Make sure all entries are properly covered!");
                exit(EXIT_FAILURE);
            }
            
            
            convergence_status Snes::GetLinearConvergenceReason(std::string& explanation,
                                                                const char* invoking_file, int invoking_line) const
            {
                const auto ret = GetLinearConvergenceReason(invoking_file, invoking_line);
                explanation = *convergence_reason_;
                return ret;
            }
            
            
            double Snes::GetAbsoluteTolerance(const char* invoking_file, int invoking_line) const
            {
                PetscReal value;
                int error_code = SNESGetTolerances(snes_, &value, nullptr, nullptr, nullptr, nullptr);
              
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESGetTolerances", invoking_file, invoking_line);
                

                return static_cast<double>(value);
            }
            
            
            double Snes::GetRelativeTolerance(const char* invoking_file, int invoking_line) const
            {
                PetscReal value;
                int error_code = SNESGetTolerances(snes_, nullptr, &value, nullptr, nullptr, nullptr);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESGetTolerances", invoking_file, invoking_line);
                
                
                return static_cast<double>(value);

            }
            
            
            unsigned int Snes::NmaxIteration(const char* invoking_file, int invoking_line) const
            {
                PetscInt value;
                int error_code = SNESGetTolerances(snes_, nullptr, nullptr, nullptr, &value, nullptr);
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "SNESGetTolerances", invoking_file, invoking_line);
                
                
                return static_cast<unsigned int>(value);

            }


            void Snes::SetLineSearchType(SNESLineSearchType type)
            {
                SNESLineSearch linesearch;
                SNESGetLineSearch(Internal(), &linesearch);
                SNESLineSearchSetType(linesearch, type);
            }

                        
        } // namespace Petsc
        
        
    } // namespace Wrappers
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
