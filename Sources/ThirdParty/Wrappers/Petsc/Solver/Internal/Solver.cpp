/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 15:45:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace Wrappers
        {
            
            
            namespace Petsc
            {
                
                
                
                Solver::~Solver() = default;
                
                
                Solver::Solver(solver_type type)
                : type_(type)
                { }
                
                
                solver_type Solver::GetSolverType() const noexcept
                {
                    return type_;
                }
                
                
            } // namespace Petsc
            
            
        } // namespace Wrappers
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
