/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Sep 2016 12:01:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/SnesConvergenceReason.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace Wrappers
        {
            
            
            namespace Petsc
            {
                                
                
                const std::string& SnesConvergenceReason<SNES_CONVERGED_FNORM_ABS>::Explanation()
                {
                    static std::string ret("||residual|| < absolute tolerance");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_CONVERGED_FNORM_RELATIVE>::Explanation()
                {
                    static std::string ret("||residual|| < relative tolerance * ||initial residual||");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_CONVERGED_SNORM_RELATIVE>::Explanation()
                {
                    static std::string ret("Newton step norm is less than tolerance times solution norm, ||dy|| < step_tol||y||.");
                    return ret;
                }

                                
                const std::string& SnesConvergenceReason<SNES_CONVERGED_ITS>::Explanation()
                {
                    static std::string ret("Maximum number of iterations was reached and convergence test was skipped.");
                    return ret;
                }
                

				#if PETSC_VERSION_LT(3, 12, 0)
                const std::string& SnesConvergenceReason<SNES_CONVERGED_TR_DELTA>::Explanation()
                {
                    static std::string ret("SNES_CONVERGED_TR_DELTA in Petsc; no further documentation available.");
                    return ret;
                }
				#endif 
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_FUNCTION_DOMAIN>::Explanation()
                {
                    static std::string ret("Petsc help: 'the new x location passed the function is not in the domain of F'.");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_FUNCTION_COUNT>::Explanation()
                {
                    static std::string ret("SNES_DIVERGED_FUNCTION_COUNT in Petsc; no further documentation available.");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_LINEAR_SOLVE>::Explanation()
                {
                    static std::string ret("Petsc help: 'the linear solve failed'.");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_FNORM_NAN>::Explanation()
                {
                    static std::string ret("A not-a-number appeared in the residual.");
                    return ret;
                }
                
                
                #if PETSC_VERSION_GE(3, 8, 0)
                const std::string& SnesConvergenceReason<SNES_DIVERGED_DTOL>::Explanation()
                {
                    static std::string ret("||residual|| > divtol * ||initial residual||");
                    return ret;
                }
                #endif // PETSC_VERSION_GE(3, 8, 0)
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_MAX_IT>::Explanation()
                {
                    static std::string ret("Maximum number of iterations was reached without attaining convergence.");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_LINE_SEARCH>::Explanation()
                {
                    static std::string ret("SNES_DIVERGED_LINE_SEARCH in Petsc; no further documentation available.");

                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_INNER>::Explanation()
                {
                    static std::string ret("Petsc help: 'inner solve failed'.");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_DIVERGED_LOCAL_MIN>::Explanation()
                {
                    static std::string ret("Petsc help: '|| J^T b || is small, implies converged to local minimum of F()'.");
                    return ret;
                }
                
                
                const std::string& SnesConvergenceReason<SNES_CONVERGED_ITERATING>::Explanation()
                {
                    static std::string ret("Newton still in progress.");
                    return ret;
                }


                #if PETSC_VERSION_GE(3, 11, 0)
                const std::string& SnesConvergenceReason<SNES_DIVERGED_JACOBIAN_DOMAIN>::Explanation()
                {
                    static std::string ret("SNES_DIVERGED_JACOBIAN_DOMAIN");
                    return ret;
                }
                #endif


                #if PETSC_VERSION_GE(3, 12, 0)
                const std::string& SnesConvergenceReason<SNES_DIVERGED_TR_DELTA>::Explanation()
                {
                    static std::string ret("SNES_DIVERGED_TR_DELTA");
                    return ret;
                }
                #endif
                            
                
            } // namespace Petsc
            
            
        } // namespace Wrappers
        
        
    } // namespace Internal
    
    
} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
