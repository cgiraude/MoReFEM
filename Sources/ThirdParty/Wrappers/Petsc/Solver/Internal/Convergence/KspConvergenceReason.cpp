/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 28 Sep 2016 00:10:19 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
        
        
        namespace Wrappers
        {
            
            
            namespace Petsc
            {
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_RTOL_NORMAL>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_RTOL_NORMAL");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_ATOL_NORMAL>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_ATOL_NORMAL");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_RTOL>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_RTOL (residual 2-norm decreased by a factor of rtol, from 2-norm of right hand side)");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_ATOL>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_ATOL (residual 2-norm less than abstol)");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_ITS>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_ITS (used by the preonly preconditioner that always uses ONE iteration, or when the KSPConvergedSkip() convergence test routine is set.");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_CG_NEG_CURVE>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_CG_NEG_CURVE");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_CG_CONSTRAINED>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_CG_CONSTRAINED");
                    return ret;
                }
                

                const std::string& KspConvergenceReason<KSP_CONVERGED_STEP_LENGTH>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_STEP_LENGTH");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_HAPPY_BREAKDOWN>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_HAPPY_BREAKDOWN");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_NULL>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_NULL");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_ITS>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_ITS  (required more than its to reach convergence)");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_DTOL>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_DTOL (residual norm increased by a factor of divtol)");
                    return ret;
                }
                
            
                const std::string& KspConvergenceReason<KSP_DIVERGED_BREAKDOWN>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_BREAKDOWN (generic breakdown in method)");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_BREAKDOWN_BICG>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_BREAKDOWN_BICG (Initial residual is orthogonal to preconditioned initial residual. Try a different preconditioner, or a different initial Level.)");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_NONSYMMETRIC>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_NONSYMMETRIC");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_INDEFINITE_PC>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_INDEFINITE_PC");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_NANORINF>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_NANORINF (residual norm became Not-a-number or Inf likely due to 0/0)");
                    return ret;
                }
                
                
                const std::string& KspConvergenceReason<KSP_DIVERGED_INDEFINITE_MAT>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_INDEFINITE_MAT");
                    return ret;
                }
                

                #if PETSC_VERSION_LT(3, 11, 0)
                const std::string& KspConvergenceReason<KSP_DIVERGED_PCSETUP_FAILED>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_PCSETUP_FAILED");
                    return ret;
                }
                #endif


                #if PETSC_VERSION_GE(3, 11, 0)
                const std::string& KspConvergenceReason<KSP_DIVERGED_PC_FAILED>::Explanation()
                {
                    static std::string ret("KSP_DIVERGED_PC_FAILED");
                    return ret;
                }
                #endif
                
                
                const std::string& KspConvergenceReason<KSP_CONVERGED_ITERATING>::Explanation()
                {
                    static std::string ret("KSP_CONVERGED_ITERATING");
                    return ret;
                }
                
                
            } // namespace Petsc
            
            
        } // namespace Wrappers
        
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
