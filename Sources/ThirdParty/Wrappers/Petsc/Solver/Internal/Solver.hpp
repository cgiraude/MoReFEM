/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 17:41:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SOLVER_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SOLVER_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM
{



    // ============================
    // Forward declarations.
    // ============================


    namespace Wrappers
    {


        namespace Petsc
        {


            class Snes;



        } // namespace Petsc


    } // namespace Wrappers


    // ============================
    // End of forward declarations.
    // ============================



    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                //! \copydoc doxygen_hide_namespace_cluttering
                using solver_type = ::MoReFEM::Wrappers::Petsc::solver_type;


                /*!
                 * \brief Polymorphic base class over a solver used in Petsc.
                 *
                 * Each solver is not set the same way with the same function calls, hence the need for such a class.
                 */
                class Solver
                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = Solver;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                protected:

                    //! Convenient aloas to avoid repeating namespaces.
                    using Snes = ::MoReFEM::Wrappers::Petsc::Snes;


                public:

                    /// \name Special members.
                    ///@{

                    //! Constructor.
                    //! \param[in] type An enum in MoReFEM which tells which kind of solver is considered (direct or
                    //! iterative).
                    explicit Solver(solver_type type);

                    //! Destructor.
                    virtual ~Solver();

                    //! \copydoc doxygen_hide_copy_constructor
                    Solver(const Solver& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    Solver(Solver&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    Solver& operator=(const Solver& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    Solver& operator=(Solver&& rhs) = delete;

                    ///@}

                    //! Set the options that are specific to the solver in SolveLinear().
                    //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
                    //! * \copydoc doxygen_hide_invoking_file_and_line
                    virtual void SetSolveLinearOptions(Snes& snes,
                                                       const char* invoking_file, int invoking_line) = 0;

                    //! Set the options that are specific to the solver.
                    //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
                    //! * \copydoc doxygen_hide_invoking_file_and_line
                    virtual void SupplInitOptions(Snes& snes,
                                                  const char* invoking_file, int invoking_line) = 0;

                    //! Print informtations after solve that are specific to the solver.
                    //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
                    //! * \copydoc doxygen_hide_invoking_file_and_line
                    virtual void SupplPrintSolverInfos(Snes& snes,
                                                       const char* invoking_file, int invoking_line) const = 0;

                    //! \copydoc doxygen_hide_petsc_solver_name
                    virtual const std::string& GetPetscName() const noexcept = 0;

                    //! Get the type of solver.
                    solver_type GetSolverType() const noexcept;

                private:

                    //! Type of solver.
                    solver_type type_;

                };


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SOLVER_HPP_
