/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 12:41:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_OPERATIONS_HXX_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_OPERATIONS_HXX_


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMultTranspose(const MatrixT& matrix,
                             const Vector& v1,
                             Vector& v2,
                             const char* invoking_file, int invoking_line,
                             update_ghost do_update_ghost)
            {
                int error_code = ::MatMultTranspose(matrix.Internal(), v1.Internal(), v2.Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMultTranspose", invoking_file, invoking_line);

                v2.UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMultTransposeAdd(const MatrixT& matrix,
                                const Vector& v1,
                                const Vector& v2,
                                Vector& v3,
                                const char* invoking_file, int invoking_line,
                                update_ghost do_update_ghost)
            {
                int error_code = ::MatMultTransposeAdd(matrix.Internal(), v1.Internal(), v2.Internal(), v3.Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMultTransposeAdd", invoking_file, invoking_line);

                v3.UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }


            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatMatMult(const MatrixT& matrix1,
                       const MatrixU& matrix2,
                       MatrixV& out,
                       const char* invoking_file, int invoking_line,
                       DoReuseMatrix do_reuse_matrix)
            {
                Mat result;
                int error_code { };

                switch (do_reuse_matrix)
                {
                    case DoReuseMatrix::yes:
                    {
                        result = out.Internal();
                        error_code = ::MatMatMult(matrix1.Internal(),
                                                  matrix2.Internal(),
                                                  MAT_REUSE_MATRIX,
                                                  PETSC_DEFAULT,
                                                  &result);
                        break;
                    }
                    case DoReuseMatrix::no:
                    {
                        error_code = ::MatMatMult(matrix1.Internal(),
                                                  matrix2.Internal(),
                                                  MAT_INITIAL_MATRIX,
                                                  PETSC_DEFAULT,
                                                  &result);
                        out.SetFromPetscMat(result);
                        break;
                    }
                    case DoReuseMatrix::in_place:
                    {
                        static_cast<void>(error_code);
                        assert(false && "In place matrix option not supported for this function.");
                        exit(EXIT_FAILURE);
                        break;
                    }
                } // switch

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMatMult", invoking_file, invoking_line);
            }


            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV,
                class MatrixW
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixW>::value,
                void
            >
            MatMatMatMult(const MatrixT& matrix1,
                          const MatrixU& matrix2,
                          const MatrixV& matrix3,
                          MatrixW& out,
                          const char* invoking_file, int invoking_line,
                          DoReuseMatrix do_reuse_matrix)
            {
                Mat result;
                int error_code {};

                switch (do_reuse_matrix)
                {
                    case DoReuseMatrix::yes:
                    {
                        result = out.Internal();
                        error_code = ::MatMatMatMult(matrix1.Internal(),
                                                     matrix2.Internal(),
                                                     matrix3.Internal(),
                                                     MAT_REUSE_MATRIX,
                                                     PETSC_DEFAULT,
                                                     &result);


                        break;
                    }
                    case DoReuseMatrix::no:
                    {
                        error_code = ::MatMatMatMult(matrix1.Internal(),
                                                     matrix2.Internal(),
                                                     matrix3.Internal(),
                                                     MAT_INITIAL_MATRIX,
                                                     PETSC_DEFAULT,
                                                     &result);

                        out.SetFromPetscMat(result);
                        break;
                    }
                    case DoReuseMatrix::in_place:
                    {
                        static_cast<void>(error_code);
                        assert(false && "In place matrix option not supported for this function.");
                        exit(EXIT_FAILURE);
                        break;
                    }
                } // switch

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMatMatMult", invoking_file, invoking_line);
            }


            template
            <
                NonZeroPattern NonZeroPatternT,
                class MatrixT,
                class MatrixU
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value,
                void
            >
            AXPY(PetscScalar a,
                 const MatrixT& X,
                 MatrixU& Y,
                 const char* invoking_file, int invoking_line)
            {
                int error_code = ::MatAXPY(Y.Internal(),
                                           a,
                                           X.Internal(),
                                           Internal::Wrappers::Petsc::NonZeroPatternPetsc<NonZeroPatternT>());

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatAXPY", invoking_file, invoking_line);
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatShift(const PetscScalar a,
                     MatrixT& matrix,
                     const char* invoking_file, int invoking_line)
            {
                int error_code = ::MatShift(matrix.Internal(), a);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatShift", invoking_file, invoking_line);
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMult(const MatrixT& matrix,
                    const Vector& v1,
                    Vector& v2,
                    const char* invoking_file, int invoking_line,
                    update_ghost do_update_ghost)
            {
                int error_code = ::MatMult(matrix.Internal(), v1.Internal(), v2.Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMult", invoking_file, invoking_line);

                v2.UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }


            template
            <
                class MatrixT,
                class MatrixU
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value,
                void
            >
            MatTranspose(MatrixT& matrix1,
                         MatrixU& matrix2,
                         const char* invoking_file, int invoking_line,
                         DoReuseMatrix do_reuse_matrix)
            {
                Mat result;
                int error_code {};
                switch (do_reuse_matrix)
                {
                    case DoReuseMatrix::no:
                    {
                        error_code = ::MatTranspose(matrix1.Internal(), MAT_INITIAL_MATRIX, &result);
                        matrix2.SetFromPetscMat(result);
                        break;
                    }
                    case DoReuseMatrix::yes:
                    {
                        result = matrix2.Internal();
                        error_code = ::MatTranspose(matrix1.Internal(), MAT_REUSE_MATRIX, &result);
                        break;
                    }
                    case DoReuseMatrix::in_place:
                    {
                        result = matrix1.Internal();
                        assert(matrix1.Internal() == matrix2.Internal() && "For in place transpose both arguments"
                                                                           "are expected to be pointers to the"
                                                                           "same PETSc matrix object.");
                        error_code = ::MatTranspose(matrix1.Internal(), MAT_INPLACE_MATRIX, &result);
                        break;
                    }
                } // switch
                
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatTranspose", invoking_file, invoking_line);
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMultAdd(const MatrixT& matrix,
                       const Vector& v1,
                       const Vector& v2,
                       Vector& v3,
                       const char* invoking_file, int invoking_line,
                       update_ghost do_update_ghost)
            {
                int error_code = ::MatMultAdd(matrix.Internal(), v1.Internal(), v2.Internal(), v3.Internal());
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMultAdd", invoking_file, invoking_line);

                v3.UpdateGhosts(invoking_file, invoking_line, do_update_ghost);
            }



            template
            <
                class MatrixT,
                class MatrixU
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value,
                void
            >
            MatCreateTranspose(const MatrixT& A,
                               MatrixU& transpose,
                               const char* invoking_file, int invoking_line)
            {
                Mat result;

                int error_code = ::MatCreateTranspose(A.Internal(), &result);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCreateTranspose", invoking_file, invoking_line);

                transpose.SetFromPetscMat(result);
            }


            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatTransposeMatMult(const MatrixT& matrix1,
                                const MatrixU& matrix2,
                                MatrixV& matrix3,
                                const char* invoking_file, int invoking_line,
                                DoReuseMatrix do_reuse_matrix)
            {
                Mat result;

                int error_code {};

                switch (do_reuse_matrix)
                {
                    case DoReuseMatrix::yes:
                    {
                        result = matrix3.Internal();

                        error_code = ::MatTransposeMatMult(matrix1.Internal(),
                                                           matrix2.Internal(),
                                                           MAT_REUSE_MATRIX,
                                                           PETSC_DEFAULT,
                                                           &result);

                        break;
                    }
                    case DoReuseMatrix::no:
                    {
                        error_code = ::MatTransposeMatMult(matrix1.Internal(),
                                                           matrix2.Internal(),
                                                           MAT_INITIAL_MATRIX,
                                                           PETSC_DEFAULT,
                                                           &result);

                        matrix3.SetFromPetscMat(result);

                        break;
                    }
                    case DoReuseMatrix::in_place:
                    {
                        static_cast<void>(error_code);
                        assert(false && "In place matrix option not supported for this function.");
                        exit(EXIT_FAILURE);
                        break;
                    }
                } // switch

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatTransposeMatMult", invoking_file, invoking_line);
            }


            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatMatTransposeMult(const MatrixT& matrix1,
                                const MatrixU& matrix2,
                                MatrixV& matrix3,
                                const char* invoking_file, int invoking_line,
                                DoReuseMatrix do_reuse_matrix)
            {
                Mat result;
                int error_code {};

                switch (do_reuse_matrix)
                {
                    case DoReuseMatrix::yes:
                    {
                        result = matrix3.Internal();
                        error_code = ::MatMatTransposeMult(matrix1.Internal(),
                                                           matrix2.Internal(),
                                                           MAT_REUSE_MATRIX,
                                                           PETSC_DEFAULT,
                                                           &result);
                        break;
                    }
                    case DoReuseMatrix::no:
                    {
                        error_code = ::MatMatTransposeMult(matrix1.Internal(),
                                                           matrix2.Internal(),
                                                           MAT_INITIAL_MATRIX,
                                                           PETSC_DEFAULT,
                                                           &result);

                        matrix3.SetFromPetscMat(result);
                        break;
                    }
                    case DoReuseMatrix::in_place:
                    {
                        static_cast<void>(error_code);
                        assert(false && "In place matrix option not supported for this function.");
                        exit(EXIT_FAILURE);
                        break;
                    }
                } // switch

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMatTransposeMult", invoking_file, invoking_line);
            }



            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            PtAP(const MatrixT& A,
                 const MatrixU& P,
                 MatrixV& out,
                 const char* invoking_file, int invoking_line,
                 DoReuseMatrix do_reuse_matrix)
            {
                Mat result;
                int error_code {};

                switch (do_reuse_matrix)
                {
                    case DoReuseMatrix::yes:
                    {
                        result = out.Internal();
                        error_code = ::MatPtAP(A.Internal(),
                                               P.Internal(),
                                               MAT_REUSE_MATRIX,
                                               PETSC_DEFAULT,
                                               &result);
                        break;
                    }
                    case DoReuseMatrix::no:
                    {
                        error_code = ::MatPtAP(A.Internal(),
                                               P.Internal(),
                                               MAT_INITIAL_MATRIX,
                                               PETSC_DEFAULT,
                                               &result);
                        out.SetFromPetscMat(result);
                        break;
                    }
                    case DoReuseMatrix::in_place:
                    {
                        static_cast<void>(error_code);
                        assert(false && "In place matrix option not supported for this function.");
                        exit(EXIT_FAILURE);
                        break;
                    }
                } // switch

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatPtAP", invoking_file, invoking_line);
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            GetOrdering(MatrixT& A,
                        MatOrderingType type,
                        IS *rperm,
                        IS *cperm,
                        const char* invoking_file, int invoking_line)
            {
                int error_code = ::MatGetOrdering(A.Internal(),
                                                  type,
                                                  rperm,
                                                  cperm);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatGetOrdering", invoking_file, invoking_line);
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            LUFactor(MatrixT& A,
                     IS row, IS col,
                     const MatFactorInfo *info,
                     const char* invoking_file, int invoking_line)
            {
                int error_code = ::MatLUFactor(A.Internal(),
                                               row,
                                               col,
                                               info);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatLUFactor", invoking_file, invoking_line);
            }


            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatMatSolve(const MatrixT& A,
                        const MatrixU& B,
                        MatrixV& X,
                        const char* invoking_file, int invoking_line)
            {
                int error_code = ::MatMatSolve(A.Internal(),
                                               B.Internal(),
                                               X.Internal());

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatMatSolve", invoking_file, invoking_line);
            }


            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            CholeskyFactor(MatrixT& mat,
                           IS perm,
                           const MatFactorInfo *info,
                           const char* invoking_file, int invoking_line)
            {
                int error_code = ::MatCholeskyFactor(mat.Internal(),
                                                     perm,
                                                     info);

                if (error_code)
                    throw ExceptionNS::Exception(error_code, "MatCholeskyFactor", invoking_file, invoking_line);
            }


        } //namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_OPERATIONS_HXX_
