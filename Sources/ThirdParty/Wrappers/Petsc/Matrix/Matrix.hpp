/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Oct 2013 11:24:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_HPP_


# include <iostream>
# include <vector>

# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"

# include "ThirdParty/Wrappers/Petsc/Matrix/Internal/BaseMatrix.hpp"

# include "Utilities/LinearAlgebra/SparseMatrix/CSRPattern.hpp"



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Wrappers
    {


        class Mpi;


        namespace Petsc
        {


            class Vector;
            class MatrixPattern;


        } // namespace Petsc


    } // namespace Wrappers



    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \brief Enum which enable to ignore zero entries in SetValues() calls.
             *
             * It is advanced use; default value of no is used.
             */
            enum class ignore_zero_entries { yes, no };



            /*!
             * \brief A wrapper class over Petsc Mat objects.
             *
             * Most of the Petsc functions used over Petsc matrices have been included as methods in this class, which
             * also acts as RAII over Petsc Mat object.
             *
             * \internal <b><tt>[internal]</tt></b> This class is way more trickier to implement that it might seems
             * because of Petsc's internal structure: Mat objects are in fact pointers over an internal Petsc type.
             * So copy and destruction operations must be made with great care! That's why several choices have been
             * made:
             *
             * - No implicit conversion to the internal Mat. It seems alluring at first sight to allow it but can
             * lead very fastly to runtime problems (covered by asserts in debug mode).
             * - No copy semantic for this class.
             * - The most usual way to proceed is to construct with the default constructor and then init it either'
             * by a 'Init***()' method or by using 'DuplicateLayout', 'Copy' or 'CompleteCopy'methods.
             * \endinternal
             *
             */
            class Matrix : private Internal::Wrappers::Petsc::BaseMatrix
            {
            private:

                //! Alias to parent.
                using parent = Internal::Wrappers::Petsc::BaseMatrix;

            public:

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<Matrix>;

            public:

                //! Constructor.
                explicit Matrix();

                /*!
                 * \brief Constructor from a file: load a matrix dumped with View() method.
                 *
                 * \param[in] binary_file File from which the matrix must be loaded. This file must be in binary format.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \copydoc doxygen_hide_mpi_param
                 */
                explicit Matrix(const Mpi& mpi,
                                const std::string& binary_file,
                                const char* invoking_file, int invoking_line);

                //! Destructor.
                virtual ~Matrix();

                //! \copydoc doxygen_hide_move_constructor
                Matrix(Matrix&& rhs) = default;

                //! \copydoc doxygen_hide_copy_constructor
                //! Both layout and data are copied.
                Matrix(const Matrix& rhs);

                //! \copydoc doxygen_hide_copy_affectation
                Matrix& operator=(const Matrix& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Matrix& operator=(Matrix&& rhs) = delete;


                /*!
                 * \brief Create a sequential sparse matrix.
                 *
                 *
                 * \param[in] Nrow Number of rows.
                 * \param[in] Ncolumn Number of columns.
                 * \param[in] matrix_pattern Pattern of the matrix (number of elements expected on each row_).
                 * \copydetails doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * The matrix hence created will keep the non-zero structure: even if a value becomes 0 it will
                 * go on being held as a non-zero emplacement.
                 */
                void InitSequentialMatrix(unsigned int Nrow, unsigned int Ncolumn,
                                          const MatrixPattern& matrix_pattern,
                                          const Mpi& mpi, const char* invoking_file, int invoking_line);


                /*!
                 * \brief Create a parallel sparse matrix.
                 *
                 * \copydoc doxygen_hide_parallel_matrix_args
                 * \param[in] matrix_pattern Pattern of the matrix (number of elements expected on each row_).
                 * \copydetails doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 */
                void InitParallelMatrix(unsigned int Nlocal_row, unsigned int Nlocal_column,
                                        unsigned int Nglobal_row, unsigned int Nglobal_column,
                                        const MatrixPattern& matrix_pattern,
                                        const Mpi& mpi, const char* invoking_file, int invoking_line);


                /*!
                 * \brief Create a sequential dense matrix.
                 *
                 * \param[in] Nrow Number of rows.
                 * \param[in] Ncolumn Number of columns.
                 * \copydetails doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void InitSequentialDenseMatrix(unsigned int Nrow, unsigned int Ncolumn,
                                               const Mpi& mpi, const char* invoking_file, int invoking_line);


                /*!
                 * \brief Create a parallel dense matrix.
                 *
                 * \copydoc doxygen_hide_parallel_matrix_args
                 * \copydetails doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 */
                void InitParallelDenseMatrix(unsigned int Nlocal_row, unsigned int Nlocal_column,
                                             unsigned int Nglobal_row, unsigned int Nglobal_column,
                                             const Mpi& mpi,
                                             const char* invoking_file, int invoking_line);


                /*!
                 * \brief Handle over the internal Mat object.
                 *
                 * \return Internal Mat object, which is indeed a pointer in Petsc.
                 *
                 * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
                 * method should be implemented over the function that might need access to the Vec internal object.
                 */
                Mat Internal() const noexcept;


                /*!
                 * \brief Init a Matrix from a Petsc Mat object.
                 *
                 * Ideally it shouldn't be used at all except in the implementation of few of the associated
                 * function such as MatMatMult.
                 *
                 * \param[in] petsc_matrix Native Petsc matrix which should be put inside current object.
                 */
                void SetFromPetscMat(Mat petsc_matrix);


                /*!
                 * \brief Duplicate the layout of the matrix.
                 *
                 * \param[in] original Original matrix, which layout is to be duplicated.
                 * \param[in] option Fine tune the behaviour of the duplication. Three values are possible:
                 * - MAT_DO_NOT_COPY_VALUES
                 * - MAT_COPY_VALUES
                 * - MAT_SHARE_NONZERO_PATTERN (share the nonzero patterns with the previous matrix but do not copy them.)
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * To make the third option work, the \a original matrix must have been assembled (i.e. where are the
                 * non-zero values must already be determined accurately, not just how many of them there are on
                 * each line - which is the information we get when a matrix is created). So technically
                 * it means you can't just duplicate the layout of a matrix you've just initialized but in which
                 * you haven't put any value yet.
                 *
                 */
                void DuplicateLayout(const Matrix& original, MatDuplicateOption option, const char* invoking_file, int invoking_line);



                /*!
                 * \brief A wrapper over MatCopy, which assumed target already gets the right layout.
                 *
                 * \param[in] source Original matrix which content is copied. It is assumed the object
                 * that called the method already gets the same layout as \a source.
                 * \param[in] structure SAME_NONZERO_PATTERN or DIFFERENT_NONZERO_PATTERN. SAME_NONZERO_PATTERN
                 * should apply for almost all cases in MoReFEM; however if you're using raw Petsc library
                 * you'll see there are more prerequisite to this choice (the pattern must be fully built).
                 * Within the frame of this wrapper there shouldn't be any problem provided one of the InitXXX()
                 * method has been called.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void Copy(const Matrix& source, const char* invoking_file, int invoking_line,
                          const MatStructure& structure = SAME_NONZERO_PATTERN);


                /*!
                 * \brief A complete copy: layout is copied first and then the values.
                 *
                 * \param[in] source Original matrix which layout AND content is copied.
                 * Beware: this method requires that \a source has already been deeply initialized (SetValues()
                 * and Assembly() should already have been called). See DuplicateLayout() for a lenghtier explanation.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 */
                void CompleteCopy(const Matrix& source, const char* invoking_file, int invoking_line);

                /*!
                 * \brief Get the number of elements in the local matrix.
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return First index is number of rows, second one the number of columns.
                 */
                std::pair<PetscInt, PetscInt> GetProcessorWiseSize(const char* invoking_file, int invoking_line) const;

                /*!
                 * \brief Get the number of elements in the global matrix (first is number of rows, second number of columns).
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return First index is number of rows, second one the number of columns.
                 */
                std::pair<PetscInt, PetscInt> GetProgramWiseSize(const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Set all the entries to zero.
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void ZeroEntries(const char* invoking_file, int invoking_line);


                /*!
                 * \brief Zeros all entries (except possibly the main diagonal) of a set of rows of a matrix.
                 *
                 * \param[in] row_indexes Indexes of the row considered (C - numbering).
                 * \param[in] diagonal_value Value for the diagonal term. Put 0. if you want it zeroed too.
                 * \copydoc doxygen_hide_invoking_file_and_line

                 */
                void ZeroRows(const std::vector<PetscInt>& row_indexes,
                              PetscScalar diagonal_value,
                              const char* invoking_file, int invoking_line);


                /*!
                 * \brief Zeros all entries (except possibly the main diagonal) of a set of rows and columns of a matrix.
                 *
                 *
                 * \param[in] row_indexes Indexes of the row considered (C - numbering). All values on this row
                 * (except the diagonal value) AND all values on the colums will be zeroed.
                 * \param[in] diagonal_value Value for the diagonal term. Put 0. if you want it zeroed too.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void ZeroRowsColumns(const std::vector<PetscInt>& row_indexes,
                                     PetscScalar diagonal_value,
                                     const char* invoking_file, int invoking_line);


                /*!
                 * \brief Petsc Assembling of the matrix.
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void Assembly(const char* invoking_file, int invoking_line);


                /*!
                 * \brief Indicates if a matrix has been assembled and is ready for use; for example, in matrix-vector product.
                 *
                 * \note This is a direct wrapper upon Petsc MatAssembled() function.
                 *
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return True if the matrix has been assembled.
                 */
                bool IsAssembled(const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Wrapper over MatScale, that performs Y = a * Y.
                 *
                 * \param[in] a Factor by which the vector is scaled.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void Scale(PetscScalar a, const char* invoking_file, int invoking_line);


                /*!
                 * \brief Add or modify values inside a Petsc matrix.
                 *
                 * \note Petsc documentation says you should use this over MatSetValue, but usage is actually not that
                 * wide: you actually define here a block of values. So for instance you choose \a row_indexing = { 1, 2 }
                 * and \a col_indexing = { 3, 5 }, you modify the four values (1, 3), (1, 5), (2, 3), (2, 5)... (which
                 * might fail if some of it are actually not non zero values! See however \a ignore_zero_entries
                 * for a possible workaround here).
                 * To my knowledge there are no way to modify just (1, 3) and (2, 5) in one call! I skimmed a bit through
                 * petsc-user help list, and they seem to suggest MatSetValues should be called for one row at a time.
                 *
                 * \warning Assembly() must be called afterwards!
                 *
                 * \param[in] row_indexing Program-wise index of the rows which values will be set.
                 * \param[in] column_indexing Program-wise index of the columns which values will be set.
                 * \param[in] values Values to put in the matrix. This array should be the same size as \a row_indexing
                 * x \a column_indexing: it is a block of values that is actually introduced!
                 * \param [in] insertOrAppend Petsc ADD_VALUES or INSERT_VALUES (see Petsc documentation).
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \param[in] do_ignore_zero_entries If yes, you might specify a block which encompass locations that
                 * are not within the pattern. For instance in our case above, you may specify \a row_indexing = { 1, 2 }
                 * and \a col_indexing = { 3, 5 } even if pattern says there are no value at (1, 5). In this case,
                 * the associated value in \a values MUST be 0.; any other value will rightfully trigger a Petsc error.
                 *
                 */
                void SetValues(const std::vector<PetscInt>& row_indexing,
                               const std::vector<PetscInt>& column_indexing,
                               const PetscScalar* values, InsertMode insertOrAppend,
                               const char* invoking_file, int invoking_line,
                               ignore_zero_entries do_ignore_zero_entries = ignore_zero_entries::no);


                /*!
                 * \brief Set the values of a row.
                 *
                 * \warning Pattern must have been set beforehand!
                 * \warning Assembly() must be called afterwards!
                 *
                 * \param[in] row_index Program-wise index of the row which values will be set.
                 * \param[in] values Non-zero values to report in the matrix.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void SetValuesRow(PetscInt row_index,
                                  const PetscScalar* values,
                                  const char* invoking_file, int invoking_line);

                /*!
                 * \brief Set a single entry into a Petsc matrix.
                 *
                 * \warning Assembly() must be called afterwards!
                 *
                 * As noted in Petsc manual pages, if several values the SetValues() method should be preferred;
                 * however see \a SetValues() documentation that specify the cases in which it can be applied.
                 *
                 * \param[in] row_index Index of the row which value will be set.
                 * \param[in] column_index Index of the column which value will be set.
                 * \param[in] value Value to put in the matrix.
                 * \param [in] insertOrAppend Petsc ADD_VALUES or INSERT_VALUES (see Petsc documentation).
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void SetValue(PetscInt row_index,
                              PetscInt column_index,
                              PetscScalar value, InsertMode insertOrAppend,
                              const char* invoking_file, int invoking_line);


                /*!
                 * \brief Get the value of an element of the matrix.
                 *
                 * The underlying Petsc function requires that the row belong to the current processor; however for
                 * the column it is much more liberal: if there is an attempt to get a value that doesn't belong
                 * to the CSR pattern, the function simply returns 0.
                 *
                 * \param[in] row_index Program-wise index of the row. The row MUST be
                 * one of the row handled by the current processor.
                 * \param[in] column_index Program-wise index of the column.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return Value of the chosen element in the matrix.
                 */
                PetscScalar GetValue(PetscInt row_index, PetscInt column_index,
                                     const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Get a row for the matrix.
                 *
                 * This is a wrapper over MatGetRow; it should be noticed that Petsc people hope this function is
                 * actually not used...
                 *
                 * \param[in] row_index Program-wise index of the row. The row MUST be
                 * one of the row handled by the current processor.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \param[out] row_content_position_list Position of the non-zero values in the row.
                 * \param[out] row_content_value_list Values of non-zero values in the row. This container is the same
                 * size as \a row_content_position
                 * \todo At the moment row_content is allocated each time; it might be useful not to do this... But this
                 * means structure has to be kept for each line of the matrix considered.
                 */
                void GetRow(PetscInt row_index,
                            std::vector<PetscInt>& row_content_position_list,
                            std::vector<PetscScalar>& row_content_value_list,
                            const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Get a row for the matrix.
                 *
                 * This is a wrapper over MatGetRow; it should be noticed that Petsc people hope this function is
                 * actually not used...
                 *
                 * \param[in] row_index Program-wise index of the row. The row MUST be
                 * one of the row handled by the current processor.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \param[out] row_content Key is position of the non-zero values, value the actual value.
                 * \todo At the moment row_content is allocated each time; it might be useful not to do this... But this
                 * means structure has to be kept for each line of the matrix considered.
                 */
                void GetRow(PetscInt row_index,
                            std::vector<std::pair<PetscInt, PetscScalar>>& row_content,
                            const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Get the column of the matrix.
                 *
                 * This is a wrapper over MatGetColumnVector; it should be noticed that Petsc people hope this function is
                 * actually not used...
                 *
                 * \param[in] column_index Program-wise index of the column.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 * \param[out] column Vector containing the column.
                 */
                void GetColumnVector(PetscInt column_index,
                                     Vector& column,
                                     const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Wrapper over MatView.
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void View(const Mpi& mpi, const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Wrapper over MatView in the case the viewer is a file.
                 *
                 * \copydoc doxygen_hide_mpi_param
                 * \param[in] format Format in which the matrix is written. See Petsc manual pages to get all the
                 * formats available; relevant ones so far are PETSC_VIEWER_DEFAULT	and PETSC_VIEWER_ASCII_MATLAB.                 
                 * \param[in] output_file File into which the vector content will be written.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 */
                void View(const Mpi& mpi, const std::string& output_file, const char* invoking_file, int invoking_line,
                          PetscViewerFormat format = PETSC_VIEWER_DEFAULT) const;


                /*!
                 * \brief Wrapper over MatView in the case the viewer is a binary file.
                 *
                 * \param[in] output_file File into which the vector content will be written.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \copydoc doxygen_hide_mpi_param
                 */
                void ViewBinary(const Mpi& mpi,
                                const std::string& output_file,
                                const char* invoking_file, int invoking_line) const;


                /*!
                 * \brief Wrapper over MatNorm.
                 *
                 * \param[in] type Type of norm. Available norms are NORM_1, NORM_FROBENIUS and NORM_INFINITY.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 *
                 * \return Norm of the vector of the chose \a type.
                 */
                double Norm(NormType type, const char* invoking_file, int invoking_line) const;


                //! Wrapper over MatGetInfo()
                //! \copydoc doxygen_hide_invoking_file_and_line
                //! \param[in] infos Matrix information context (see [Petsc documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatGetInfo.html) for more details).
                void GetInfo(MatInfo* infos, const char* invoking_file, int invoking_line);

                //! Wrapper over MatGetOption()
                //! \copydoc doxygen_hide_invoking_file_and_line
                //! \param[in] op The [Petsc MatOption](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatOption.html#MatOption)
                //! which status is sought.
                //! \param[out] flg True if the option is set, false otherwise.
                void GetOption(MatOption op, PetscBool *flg, const char* invoking_file, int invoking_line);

                //! Wrapper over MatSetOption()
                //! \copydoc doxygen_hide_invoking_file_and_line
                //! \param[in] op The [Petsc MatOption](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatOption.html#MatOption)
                //! which status is to be modified.
                //! \param[in] flg Whether the option is activated or not.
                void SetOption(MatOption op, PetscBool flg, const char* invoking_file, int invoking_line);

            protected:

                /*!
                 * \brief Change the underlying \a Mat object pointed by the class.
                 *
                 * \attention This should not be encouraged and is provided only t comply with PETSc non linear solve interface.
                 *
                 * \param[in] mpi \a Mpi object. It is provided so that \a Barrier() may be called: we do not want the ranks oto be inconsistent...
                 * \param[in] new_petsc_matrix The \a Mat object that should now be used internally (for thecurrent mpi rank).
                 */
                void ChangeInternal(const ::MoReFEM::Wrappers::Mpi& mpi, Mat new_petsc_matrix);

            private:

                // ===========================================================================
                // \attention Do not forget to update Swap() if a new data member is added!
                // =============================================================================

                //! Underlying Petsc matrix.
                Mat petsc_matrix_;

                /*!
                 * \brief Whether the underlying Petsc matrix will be destroyed upon destruction of the object.
                 *
                 * Default behaviour is to do so, but in some cases it is wiser not to.
                 */
                bool do_petsc_destroy_;


                // ============================
                //! \cond IGNORE_BLOCK_IN_DOXYGEN
                //! Friendship.
                // ============================


                friend void Swap(Matrix& A, Matrix& B);


                // ============================
                //! \endcond IGNORE_BLOCK_IN_DOXYGEN
                // ============================


            };


            //! Swap two matrices.
            void Swap(Matrix& , Matrix& );


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_HPP_
