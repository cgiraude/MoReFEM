/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 12:41:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_OPERATIONS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_OPERATIONS_HPP_

# include <memory>
# include <vector>

# include "Utilities/Containers/EnumClass.hpp"

# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"

# include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"
# include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
# include "ThirdParty/Wrappers/Petsc/Matrix/Internal/BaseMatrix.hpp"
# include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================



    // Matrix actually used in MoReFEM, that wraps a Wrappers::Petsc::Matrix.
    class GlobalMatrix;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Wrappers
    {


        namespace Petsc
        {


            /*!
             * \class doxygen_hide_petsc_do_reuse_matrix
             *
             * Whether matrix must be reused or initialized from scratch in functions such
             * as MatTransposeMatMult.
             * If yes, it must have been called once with no beforehand, and one has to be sure the pattern
             * of the matrices involved is still the same.
             * If is set to in_place, the input matrix will be replaced by the result of the function.
             * This is not applicable to all functions but can be useful in some, for instance with
             * MatTranspose which replaces the input matrix by its transposed matrix.
             */

            /*!
             * \class doxygen_hide_petsc_do_reuse_matrix_arg
             *
             * \param[in] do_reuse_matrix \copydetails doxygen_hide_petsc_do_reuse_matrix
             */


            /*!
             * \brief \copydoc doxygen_hide_petsc_do_reuse_matrix
             */
            enum class DoReuseMatrix { yes, no, in_place };

            /*!
             * \brief Wrapper over MatAXPY, that performs Y = a * X + Y.
             *
             *
             * \tparam NonZeroPatternT This value indicates the level of similarity between X and Y non zero patterns.
             * \warning Beware: if X and Y aren't following the same pattern, 'Same' won't yield what
             * you expect! In the case you're considering adding a part computed by a transfert matrix, you should
             * use 'Subset'. In MoReFEM, I advise you to call in debug mode AssertSameNumberingSubset()
             * (defined in Core) just before or after the function call; if the assert is raised it means you can't
             * use 'Same'. \todo This could be enforced by a proper overload (but it mix Utilities and Core
             * libraries...), but for the time being the separate assert will do.
             * \copydoc doxygen_hide_invoking_file_and_line
             *
             * \param[in] a See formula above.
             * \param[in] X See formula above.
             * \param[in] Y See formula above.
             */
            template
            <
                NonZeroPattern NonZeroPatternT,
                class MatrixT,
                class MatrixU
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
             && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value,
                void
            >
            AXPY(PetscScalar a,
                 const MatrixT& X,
                 MatrixU& Y,
                 const char* invoking_file, int invoking_line);


            /*!
             * \brief Wrapper over MatShift, that performs M = M + a I, where a is a PetscScalar and I is the identity matrix.
             *
             * \param[in] matrix See formula above.
             * \param[in] a See formula above.
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatShift(const PetscScalar a, MatrixT& matrix, const char* invoking_file, int invoking_line);


            /*!
             * \brief Wrapper over MatMult, that performs v2 = matrix * v1.
             *
             * \copydoc doxygen_hide_invoking_file_and_line
             * \param[in] matrix See formula above.
             * \param[in] v1 See formula above.
             * \param[in] v2 See formula above.
             * \copydoc doxygen_hide_do_update_ghost_arg
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMult(const MatrixT& matrix,
                    const Vector& v1,
                    Vector& v2,
                    const char* invoking_file, int invoking_line,
                    update_ghost do_update_ghost = update_ghost::yes);

            /*!
             * \brief Wrapper over MatTranspose, that performs in-place or out-of-place transpose of a matrix.
             *
             * \param[in] matrix1 Matrix to transpose.
             * \param[in] matrix2 Matrix to contain the result of the transpose.
             * \copydoc doxygen_hide_invoking_file_and_line
             * \copydetails doxygen_hide_petsc_do_reuse_matrix_arg
             */
            template
            <
            class MatrixT,
            class MatrixU
            >
            std::enable_if_t
            <
            std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
            && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value,
            void
            >
            MatTranspose(MatrixT& matrix1,
                         MatrixU& matrix2,
                         const char* invoking_file, int invoking_line,
                         DoReuseMatrix do_reuse_matrix = DoReuseMatrix::no);


            /*!
             * \brief Wrapper over MatMultAdd, that performs v3 = v2 + matrix * v1.
             *
             * \copydoc doxygen_hide_invoking_file_and_line
             * \param[in] matrix See formula above.
             * \param[in] v1 See formula above.
             * \param[in] v2 See formula above.
             * \param[in] v3 See formula above.
             * \copydoc doxygen_hide_do_update_ghost_arg
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMultAdd(const MatrixT& matrix,
                       const Vector& v1,
                       const Vector& v2,
                       Vector& v3,
                       const char* invoking_file, int invoking_line,
                       update_ghost do_update_ghost = update_ghost::yes);



            /*!
             * \brief Wrapper over MatMultTranspose, that performs v2 = transpose(matrix) * v1.
             *
             * \copydoc doxygen_hide_invoking_file_and_line
             * \copydoc doxygen_hide_do_update_ghost_arg
             * \param[in] matrix See formula above.
             * \param[in] v1 See formula above.
             * \param[in] v2 See formula above.
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMultTranspose(const MatrixT& matrix,
                             const Vector& v1,
                             Vector& v2,
                             const char* invoking_file, int invoking_line,
                             update_ghost do_update_ghost = update_ghost::yes);


            /*!
             * \brief Wrapper over MatMultTransposeAdd, that performs v3 = v2 + transpose(matrix) * v1.
             *
             * \copydoc doxygen_hide_invoking_file_and_line
             * \copydoc doxygen_hide_do_update_ghost_arg
             * \param[in] matrix See formula above.
             * \param[in] v1 See formula above.
             * \param[in] v2 See formula above.
             * \param[in] v3 See formula above.
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            MatMultTransposeAdd(const MatrixT& matrix,
                                const Vector& v1,
                                const Vector& v2,
                                Vector& v3,
                                const char* invoking_file, int invoking_line,
                                update_ghost do_update_ghost = update_ghost::yes);

            /*!
             * \class doxygen_hide_matmatmult_warning
             *
             * \attention In Petsc, matrix-matrix multiplication functions compute on the fly the
             * pattern for the resulting matrix. However, it is possible this pattern isn't the one expected;
             * in the richer MoReFEM interface you should call in debug mode \a AssertMatrixRespectPattern()
             * to make sure the resulting matrix respects the pattern defined for the \a GlobalMatrix pair
             * of \a NumberingSubset.
             *
             * \internal <b><tt>[internal]</tt></b> If you know Petsc, you might see I didn't give access to
             * argument MatReuse, setting it each time to MAT_INITIAL_MATRIX and skipping entirely MAT_REUSE_MATRIX.
             * This is because at the time being MatMatMult operations are seldom in the code (only Poromechanics so
             * far) and using Symbolic/Numeric seems more elegant. Of course, reintroducing the argument is really easy;
             * feel free to do so if you need it (for instance for MatMatMatMult support: Symbolic/Numeric doesn't
             * work for them and Petsc guys seemed unlikely to fix that in our exchanges).
             *
             * <b><tt>[internal]</tt></b> \todo #684 Investigate to use the argument fill, which provides an
             * estimation of the non zero of the resulting matrix. Currently PETSC_DEFAULT is used.

             * \endinternal
             */



            /*!
             * \brief Wrapper over MatMatMult, that performs m3 = m1 * m2.
             *
             * \copydetails doxygen_hide_matmatmult_warning
             *
             * If the operation is performed many times with each time the same non zero pattern for the matrices,
             * rather use MatMatMultSymbolic/MatMatMultNumeric to improve efficiency.

             * One can also reuse a pattern for several matrices, but so far I do not need that in MoReFEM so
             * I equally use the default setting.
             * \param[in] m1 See formula above.
             * \param[in] m2 See formula above.
             * \param[in] m3 See formula above.
             *
             * \copydetails doxygen_hide_petsc_do_reuse_matrix_arg
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            template
            <
            class MatrixT,
            class MatrixU,
            class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatMatMult(const MatrixT& m1,
                       const MatrixU& m2,
                       MatrixV& m3,
                       const char* invoking_file, int invoking_line,
                       DoReuseMatrix do_reuse_matrix = DoReuseMatrix::no);


            /*!
             * \brief Wrapper over MatMatMatMult, that performs m4 = m1 * m2 * m3.
             *
             * \param[in] m1 See formula above.
             * \param[in] m2 See formula above.
             * \param[in] m3 See formula above.
             * \param[in] m4 See formula above.
             * \copydetails doxygen_hide_petsc_do_reuse_matrix_arg
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            template
            <
            class MatrixT,
            class MatrixU,
            class MatrixV,
            class MatrixW
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixW>::value,
                void
            >
            MatMatMatMult(const MatrixT& m1,
                          const MatrixU& m2,
                          const MatrixV& m3,
                          MatrixW& m4,
                          const char* invoking_file, int invoking_line,
                          DoReuseMatrix do_reuse_matrix = DoReuseMatrix::no);

            /*!
             * \brief Creates a new matrix object that behaves like A'.
             *
             * The transpose A' is NOT actually formed! Rather the new matrix object performs the matrix-vector product
             * by using the MatMultTranspose() on the original matrix.
             *
             * \param[in] A matrix to transpose.
             * \param[out] transpose The matrix that figuratively represents A'. This matrix must not have been
             * allocated!
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            template
            <
            class MatrixT,
            class MatrixU
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value,
                void
            >
            MatCreateTranspose(const MatrixT& A,
                               MatrixU& transpose,
                               const char* invoking_file, int invoking_line);


            /*!
             * \class doxygen_hide_mat_transpose_mat_mult
             *
             * Formula is:
             * \verbatim
             m3 = m1^T * m2.
             * \endverbatim
             *
             * \copydetails doxygen_hide_matmatmult_warning
             * \param[in] m1 See formula above.
             * \param[in] m2 See formula above.
             * \param[out] m3 See formula above. The matrix must be not allocated when this function is called.
             * \copydoc doxygen_hide_invoking_file_and_line
             */


            /*!
             * \brief Performs Matrix-Matrix Multiplication m3 = m1^T * m2.
             *
             * \copydetails doxygen_hide_mat_transpose_mat_mult
             * \copydetails doxygen_hide_petsc_do_reuse_matrix_arg
             */
            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatTransposeMatMult(const MatrixT& m1,
                                const MatrixU& m2,
                                MatrixV& m3,
                                const char* invoking_file, int invoking_line,
                                DoReuseMatrix do_reuse_matrix = DoReuseMatrix::no);



            /*!
             * \brief Performs Matrix-Matrix Multiplication C = A * B^T.
             *
             * \copydetails doxygen_hide_matmatmult_warning
             * \copydetails doxygen_hide_petsc_do_reuse_matrix_arg
             *
             * \param[in] matrix1 A in C = A * B^T.
             * \param[in] matrix2 B in C = A * B^T.
             * \param[out] matrix3 C in B in C = A * B^T. The matrix must be not allocated when this function is called.
             * \copydoc doxygen_hide_invoking_file_and_line
             */
            template
            <
            class MatrixT,
            class MatrixU,
            class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatMatTransposeMult(const MatrixT& matrix1,
                                const MatrixU& matrix2,
                                MatrixV& matrix3,
                                const char* invoking_file, int invoking_line,
                                DoReuseMatrix do_reuse_matrix = DoReuseMatrix::no);


            /*!
             * \class doxygen_hide_mat_pt_a_p
             *
             * \warning Unfortunately a simple test with P = I leads to error
             * \verbatim
             [0;39m[0;49m[0]PETSC ERROR: Nonconforming object sizes
             [0]PETSC ERROR: Expected fill=-2 must be >= 1.0
             \endverbatim
             * The reason is that PETSC_DEFAULT may not be supported (I've asked Petsc developers); but even with
             * hand-tailored fill it doesn't seem to work...
             * So unfortunately I have to advise instead MatMatMult followed by MatTransposeMatMult instead...
             *
             * \copydetails doxygen_hide_matmatmult_warning
             *
             * \param[in] A A in C = P^T * A * P.
             * \param[in] P P in C = P^T * A * P
             * \param[out] out C in C = P^T * A * P. The matrix must be not allocated when this function is called.
             * \copydetails doxygen_hide_invoking_file_and_line
             * \copydoc doxygen_hide_petsc_do_reuse_matrix_arg
             */

            /*!
             * \brief Performs the matrix product C = P^T * A * P
             *
             * \copydoc doxygen_hide_mat_pt_a_p
             *
             *
             */
            template
            <
            class MatrixT,
            class MatrixU,
            class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            PtAP(const MatrixT& A,
                 const MatrixU& P,
                 MatrixV& out,
                 const char* invoking_file, int invoking_line,
                 DoReuseMatrix do_reuse_matrix = DoReuseMatrix::no);



            /*!
             * \brief Wrapper over MatGetOrdering, gets a reordering for a matrix to reduce fill or to improve numerical stability of LU factorization.
             *
             * \see http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/MatOrderings/MatGetOrdering.html#MatGetOrdering
             * for more details.
             *
             * \param[in] A Matrix to get the ordering.
             * \param[in] type Type of the ordering.
             * \param[in] rperm Row permutation for the ordering.
             * \param[in] cperm Column permutation for the ordering.
             * \copydetails doxygen_hide_invoking_file_and_line
             *
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            GetOrdering(MatrixT& A,
                        MatOrderingType type,
                        IS *rperm,
                        IS *cperm,
                        const char* invoking_file, int invoking_line);


            /*!
             * \brief Wrapper over MatLUFactor, that performs in-place LU factorization of matrix.
             *
             * \see http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatLUFactor.html#MatLUFactor
             * for more details.
             *
             * \param[in] A Matrix to factor.
             * \param[in] row Result of the GetOrdering.
             * \param[in] col Result of the GetOrdering.
             * \param[in] info Info of LUFactor.
             * \copydetails doxygen_hide_invoking_file_and_line
             *
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            LUFactor(MatrixT& A,
                     IS row, IS col,
                     const MatFactorInfo *info,
                     const char* invoking_file, int invoking_line);


            /*!
             * \brief Wrapper over MatMatSolve, solves A X = B, given a factored matrix.
             *
             * \see http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatMatSolve.html
             * for more details.
             *
             * \param[in] A See formula above.
             * \param[in] B See formula above.
             * \param[in] X See formula above.
             * \copydetails doxygen_hide_invoking_file_and_line
             *
             */
            template
            <
                class MatrixT,
                class MatrixU,
                class MatrixV
            >
            std::enable_if_t
            <
                std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixU>::value
                && std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixV>::value,
                void
            >
            MatMatSolve(const MatrixT& A,
                        const MatrixU& B,
                        MatrixV& X,
                        const char* invoking_file, int invoking_line);


            /*!
             * \brief Wrapper over MatCholeskyFactor, that performs in-place Cholesky factorization of a symmetric matrix.
             *
             * \see http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatCholeskyFactor.html
             * for more details.
             *
             * \attention Not used yet, but might be useful in midterm developments. If finally too unwieldy for our
             * purposes it will be removed.
             *
             * \param[in] mat Matrix to factor.
             * \param[in] perm Result of the GetOrdering, row and column permutations.
             * \param[in] info Info of CholeskyFactor.
             * \copydetails doxygen_hide_invoking_file_and_line
             *
             */
            template<class MatrixT>
            std::enable_if_t<std::is_base_of<Internal::Wrappers::Petsc::BaseMatrix, MatrixT>::value, void>
            CholeskyFactor(MatrixT& mat,
                           IS perm,
                           const MatFactorInfo *info,
                           const char* invoking_file, int invoking_line);




        } //namespace Petsc


    } // namespace Wrappers



    /*!
     * \brief Convenient alias to avoid repeating the namespaces in each call.
     *
     * It should be used anyway only as template arguments of functions within this namespaces, such as
     * Wrappers::Petsc::AXPY.
     */
    using NonZeroPattern = Wrappers::Petsc::NonZeroPattern;


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


# include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hxx"


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_MATRIX_x_MATRIX_OPERATIONS_HPP_
