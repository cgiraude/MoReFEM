/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 11:36:08 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_VECTOR_HELPER_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_VECTOR_HELPER_HPP_

# include <string>

# include "ThirdParty/Wrappers/Petsc/Vector/BinaryOrAscii.hpp"


namespace MoReFEM
{


    // ============================
    // Forward declarations.
    // ============================


    namespace Wrappers
    {


        namespace Petsc
        {


            class Vector;


        } // namespace Petsc


    } // namespace Wrappers


    // ============================
    // End of forward declarations.
    // ============================



    namespace Internal
    {


        namespace Wrappers
        {


            namespace Petsc
            {


                //! Convenient alias to avoid repeating namespaces.
                using Vector = ::MoReFEM::Wrappers::Petsc::Vector;


                /*!
                 * \brief The function called when Print() is called in processor-wise mode.
                 *
                 * \param[in] vector Vector which processor-wise content is to be written.
                 * \param[in] output_file Output file into which the values will be written. Beware: this file should
                 * be named differently for each rank!
                 * \param[in] binary_or_ascii_choice Whether the vector should be printed as binary or ascii. Default
                 * value takes its cue from the choice written in the input data file.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 *
                 */
                void PrintPerProcessor(const Vector& vector,
                                       const std::string& output_file,
                                       const char* invoking_file, int invoking_line,
                                       binary_or_ascii binary_or_ascii_choice);


            } // namespace Petsc


        } // namespace Wrappers


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_INTERNAL_x_VECTOR_HELPER_HPP_
