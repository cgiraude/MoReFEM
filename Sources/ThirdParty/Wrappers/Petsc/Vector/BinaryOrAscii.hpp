//! \file
//
//
//  BinaryOrAscii.hpp
//  MoReFEM
//
//  Created by sebastien on 16/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_BINARY_OR_ASCII_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_BINARY_OR_ASCII_HPP_

# include <memory>
# include <vector>


namespace MoReFEM
{


    //! Enum class to specify whether the choice of output (ascii or binary)
    enum class binary_or_ascii
    {
        from_input_data, // takes the value specified in the input data file.
        ascii,
        binary
    };


} // namespace MoReFEM


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_BINARY_OR_ASCII_HPP_
