/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 12 Jun 2015 09:53:03 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM
{
    
    
    namespace Wrappers
    {
        
        
        namespace Petsc
        {
            
            
            AccessGhostContent::AccessGhostContent(const Vector& vector,
                                                   const char* invoking_file, int invoking_line)
            : vector_without_ghost_(vector)
            //            local_content_(nullptr)
            {
                int error_code = VecGhostGetLocalForm(vector_without_ghost_.Internal(),
                                                      &petsc_vector_with_ghost_);
                if (error_code)
                    throw ExceptionNS::Exception(error_code, "VecGhostGetLocalForm", invoking_file, invoking_line);
            }
            
            
            AccessGhostContent::~AccessGhostContent()
            {
                assert(petsc_vector_with_ghost_ != PETSC_NULL);
                assert(vector_without_ghost_.Internal() != PETSC_NULL);
                
                int error_code = VecGhostRestoreLocalForm(vector_without_ghost_.Internal(), &petsc_vector_with_ghost_);
                static_cast<void>(error_code); // avoid warning in release mode
                assert(error_code == 0); // error code should be 0; exception can't be thrown in a destructor!
            }
            
            
        } // namespace Petsc
        
        
    } // namespace Wrappers
  

} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup
