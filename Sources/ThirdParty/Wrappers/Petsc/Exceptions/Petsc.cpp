/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Sep 2013 14:51:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <sstream>
#include <cassert>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"



namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string ErrorCodeMsg(int error_code, std::string&& petsc_function);
   
    std::string WrongMatlabExtensionMsg(const std::string& filename);


} // namespace anonymous



namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Petsc
        {


            namespace ExceptionNS
            {


                Exception::~Exception() = default;


                Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(msg, invoking_file, invoking_line)
                { }


                Exception::Exception(int error_code, std::string&& petsc_function,
                                     const char* invoking_file, int invoking_line)
                : MoReFEM::Exception(ErrorCodeMsg(error_code, std::move(petsc_function)), invoking_file, invoking_line)
                { }

                
                WrongMatlabExtension::~WrongMatlabExtension() = default;
                
                WrongMatlabExtension::WrongMatlabExtension(const std::string& filename,
                                                           const char* invoking_file, int invoking_line)
                : Exception(WrongMatlabExtensionMsg(filename), invoking_file, invoking_line)
                { }
                

            } // namespace ExceptionNS


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM



namespace // anonymous
{
    
    
    // Definitions of functions defined at the beginning of the file
    std::string ErrorCodeMsg(int error_code, std::string&& petsc_function)
    {
        std::ostringstream oconv;
        oconv << "Petsc function " << petsc_function << " returned the error code " << error_code << '.';

        return oconv.str();
    }

    
    
    std::string WrongMatlabExtensionMsg(const std::string& filename)
    {
        std::ostringstream oconv;
        oconv << "Invalid name for Matlab output (\"" << filename << "\"): a '.m'extension was expected.";
        return oconv.str();
        
    }

    
} // namespace anonymous


/// @} // addtogroup ThirdPartyGroup
