/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 6 Feb 2014 11:56:07 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VIEWER_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VIEWER_HPP_


# include <string>

# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // Forward declarations.
        // ============================


        class Mpi;


        // ============================
        // End of forward declarations.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


        namespace Petsc
        {


            /*!
             * \brief RAII over PetscViewer object.
             */
            class Viewer
            {
            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor for an ascii file.
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] ascii_file File to which the Petsc object will be associated.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \param[in] format Format in which the matrix is written. See Petsc manual pages to get all the
                 * formats available; relevant ones so far are PETSC_VIEWER_DEFAULT	and PETSC_VIEWER_ASCII_MATLAB.
                 *
                 */
                Viewer(const Mpi& mpi,
                       const std::string& ascii_file,
                       PetscViewerFormat format,
                       const char* invoking_file, int invoking_line);

                /*!
                 * \class doxygen_hide_petsc_file_mode
                 *
                 * \param[in] file_mode Same modes as in Petsc documentation, i.e.:
                 * - FILE_MODE_WRITE - create new file for binary output.
                 * - FILE_MODE_READ - open existing file for binary input.
                 * - FILE_MODE_APPEND - open existing file for binary output.
                 */


                /*!
                 * \brief Constructor for a binary file.
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] binary_file File to which the Petsc object will be associated.
                 * \copydoc doxygen_hide_invoking_file_and_line
                 * \copydoc doxygen_hide_petsc_file_mode
                 */
                Viewer(const Mpi& mpi,
                       const std::string& binary_file,
                       PetscFileMode file_mode,
                       const char* invoking_file, int invoking_line);


                //! Destructor.
                ~Viewer();

                //! \copydoc doxygen_hide_copy_constructor
                Viewer(const Viewer& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Viewer(Viewer&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Viewer& operator=(const Viewer& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Viewer& operator=(Viewer&& rhs) = delete;


                ///@}


                //! Access to the underlying viewer.
                PetscViewer& GetUnderlyingPetscObject();

            private:

                //! Underlying PetscViewer object (it is truly a pointer over an alias);
                PetscViewer viewer_;

            };


        } // namespace Petsc


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VIEWER_HPP_
