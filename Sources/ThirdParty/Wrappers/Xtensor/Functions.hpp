/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 9 Sep 2013 09:55:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_XTENSOR_x_FUNCTIONS_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_XTENSOR_x_FUNCTIONS_HPP_

# include <cassert>

# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/MatrixOrVector.hpp"
# include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace Xtensor
        {


            /*!
             * \brief Computes the cross product of two 3d vectors.
             *
             * \internal <b><tt>[internal]</tt></b> The types of the vectors and matrix below are very constrained
             * on purpose; I need this calculation only for a very specific case (calculation of small matrices for
             * local finite elements).
             * \endinternal
             *
             * \tparam T1 Type of the content of the vector and matrix. Typically 'double'.
             * \tparam Allocator1 Allocator of the first vector.
             * \tparam Allocator2 Allocator of the second vector.
			 * *
			 * \todo #1491 Probably already existing in Xtensor-blas
             *
             * \param[in] X First vector. Must be 3d.
             * \param[in] Y Second vector. Must be 3d.
             * \param[out] out Resulting vector.
             */
            void CrossProduct(const LocalVector& X,
                              const LocalVector& Y,
                              LocalVector& out);



            /*!
             * \brief Check whether a matrix is filled with zero or not.
             *
             * \param[in] matrix Matrix under scrutiny.
			 *
			 * \todo #1491 Could probably be done more efficiently (by going through the unidimensional container directly)
             */
            bool IsZeroMatrix(const LocalMatrix& matrix);


            /*!
             * \brief Check whether a vector is filled with zero or not.
             *
             * \param[in] vector Vector under scrutiny.
			 *
			 * \todo #1491 Could probably be done more efficiently (by going through the unidimensional container directly)
             */
            bool IsZeroVector(const LocalVector& vector);

			
			/*!
			 * \brief Check whether there is a nan or inf value in a \a LocalMatrix.
			 *
			 * \param[in] matrix Matrix being scrutinized.
			 *
			 * \return True if at least one value if NaN or inf.
			 */
			bool AreNanOrInfValues(const LocalMatrix& matrix);


			/*!
			 * \brief Check whether there is a nan or inf value in a \a LocalVector.
			 *
			 * \param[in] vector Vector being scrutinized.
			 *
			 * \return True if at least one value if NaN or inf.
			 */
			bool AreNanOrInfValues(const LocalVector& vector);


			/*!
			 * \brief Compute the inverse matrix of \a local_matrix (if existing; if not an exception is throw).
			 *
			 * Works only for dimensions 1 to 3.
			 * \param[out] inverse Inverse of the matrix.
			 * \param[in] local_matrix Matrix for which we want to compute the determinant.
			 * \param[out] determinant Determinant of \a local_matrix.
			 */
			void ComputeInverseSquareMatrix(const LocalMatrix& local_matrix, LocalMatrix& inverse, double& determinant);
			


        } // namespace Xtensor


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_XTENSOR_x_FUNCTIONS_HPP_
