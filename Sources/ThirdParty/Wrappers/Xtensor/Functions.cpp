/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 9 Sep 2013 09:55:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


# include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


namespace MoReFEM::Wrappers::Xtensor
{


	namespace // anonymous
	{


		void ComputeTransposeComatrix(const LocalMatrix& local_matrix, LocalMatrix& out);


	} // namespace // anonymous



	bool IsZeroMatrix(const LocalMatrix& matrix)
	{
		const auto Nrow = matrix.shape(0);
		const auto Ncol = matrix.shape(1);

		for (auto i = 0ul; i < Nrow; ++i)
		{
			for (auto j = 0ul; j < Ncol; ++j)
			{
				if (!NumericNS::IsZero(matrix(i, j)))
					return false;
			}
		}

		return true;
	}


	bool IsZeroVector(const LocalVector& vector)
	{
		const auto Nitem = vector.shape(0);

		for (auto i = 0ul; i < Nitem; ++i)
		{
			if (!NumericNS::IsZero(vector(i)))
				return false;
		}

		return true;
	}


	void CrossProduct(const LocalVector& X,
					  const LocalVector& Y,
					  LocalVector& out)
	{
		assert(X.size() == 3);
		assert(Y.size() == 3);
		assert(out.size() == 3);

		out(0) = X(1) * Y(2) - X(2) * Y(1);
		out(1) = X(2) * Y(0) - X(0) * Y(2);
		out(2) = X(0) * Y(1) - X(1) * Y(0);
	}


	void ComputeInverseSquareMatrix(const LocalMatrix& local_matrix, LocalMatrix& inverse, double& determinant)
	{
		const auto Nrow = local_matrix.shape(0);
		assert(Nrow == local_matrix.shape(1));
		assert(Nrow == inverse.shape(0));
		assert(Nrow == inverse.shape(1));

		determinant = xt::linalg::det(local_matrix);

		if (NumericNS::IsZero(determinant))
			throw Exception("Determinant is zero!", __FILE__, __LINE__);

		if (Nrow == 1)
			inverse(0, 0) = 1. / determinant;
		else
		{
			ComputeTransposeComatrix(local_matrix, inverse);
			inverse *= 1. / determinant;
		}
	}


	bool AreNanOrInfValues(const LocalMatrix& matrix)
	{
		const auto Nrow = matrix.shape(0);
		const auto Ncol = matrix.shape(1);

		assert(Nrow > 0);
		assert(Ncol > 0);

		for (auto i = 0ul; i < Nrow; ++i)
		{
			for (auto j = 0ul; j < Ncol; ++j)
			{
				if (!NumericNS::IsNumber(matrix(i, j)))
					return true;
			}
		}

		return false;
	}


	bool AreNanOrInfValues(const LocalVector& vector)
	{
		const auto size = vector.size();

		assert(size > 0);

		for (auto i = 0ul; i < size; ++i)
		{
			if (!NumericNS::IsNumber(vector(i)))
				return true;
		}

		return false;
	}


	namespace // anonymous
	{


		void ComputeTransposeComatrix(const LocalMatrix& local_matrix, LocalMatrix& out)
		{
			const auto Nrow = local_matrix.shape(0);
			assert(Nrow == local_matrix.shape(1) && "Matrix must be square!");
			assert(Nrow < 4 && "At the moment, only determinants for 2x2 or 3x3 matrices are "
				   "computed.");
			assert(Nrow > 1);
			assert(out.shape(0) == Nrow);
			assert(out.shape(1) == Nrow);

			switch (Nrow)
			{
				case 2:
				{
					out(0, 0) = local_matrix(1, 1);
					out(1, 0) = -local_matrix(1, 0);
					out(0, 1) = -local_matrix(0, 1);
					out(1, 1) = local_matrix(0, 0);
					break;
				}
				case 3:
				{
					out(0, 0) = local_matrix(1, 1) * local_matrix(2, 2) - local_matrix(2, 1) * local_matrix(1, 2);
					out(1, 0) = -local_matrix(1, 0) * local_matrix(2, 2) + local_matrix(2, 0) * local_matrix(1, 2);
					out(2, 0) = local_matrix(1, 0) * local_matrix(2, 1) - local_matrix(2, 0) * local_matrix(1, 1);

					out(0, 1) = -local_matrix(0, 1) * local_matrix(2, 2) + local_matrix(2, 1) * local_matrix(0, 2);
					out(1, 1) = local_matrix(0, 0) * local_matrix(2, 2) - local_matrix(2, 0) * local_matrix(0, 2);
					out(2, 1) = -local_matrix(0, 0) * local_matrix(2, 1) + local_matrix(2, 0) * local_matrix(0, 1);

					out(0, 2) = local_matrix(0, 1) * local_matrix(1, 2) - local_matrix(1, 1) * local_matrix(0, 2);
					out(1, 2) = -local_matrix(0, 0) * local_matrix(1, 2) + local_matrix(1, 0) * local_matrix(0, 2);
					out(2, 2) = local_matrix(0, 0) * local_matrix(1, 1) - local_matrix(1, 0) * local_matrix(0, 1);
					break;
				}
				default:
					assert(false && "Should not happen!");
					break;
			}
		}



	} // namespace // anonymous


} // namespace MoReFEM::Wrappers::Xtensor


/// @} // addtogroup ThirdPartyGroup


