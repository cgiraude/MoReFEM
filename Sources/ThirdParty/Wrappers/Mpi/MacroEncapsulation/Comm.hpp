/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Sep 2014 13:53:24 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MACRO_ENCAPSULATION_x_COMM_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MACRO_ENCAPSULATION_x_COMM_HPP_


# include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Wrappers
    {


        namespace MpiNS
        {



            /*!
             * \brief Enum that encapsulates MPI_Comm.
             *
             * The reason for this is that MPI_Comm are often macros that trigger the Wold-style-cast warning;
             * the level of indirection allows to neutralize it.
             *
             * \internal <b><tt>[internal]</tt></b> This enum is populated only for the operation I needed at some
             * point; some should be added as soon as they are required.
             * \endinternal
             */
            enum class Comm
            {
                World,
                Self
            };



            /*!
             * \brief Function used to choose the Mpi communicator from the MoReFEM defined enum.
             *
             * \param[in] communicator Communicator enum value defined in MoReFEM.
             *
             * \return MPI_Comm The Openmpi communicator object.
             */
            MPI_Comm Communicator(Comm communicator);


        } // namespace MpiNS


    } // namespace Wrappers


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MACRO_ENCAPSULATION_x_COMM_HPP_
