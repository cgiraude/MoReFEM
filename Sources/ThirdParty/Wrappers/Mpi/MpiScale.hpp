/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Nov 2014 10:09:36 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_SCALE_HPP_
# define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_SCALE_HPP_


namespace MoReFEM
{


    /*!
     * \brief Convenient alias to distinguish processor-wise and program-wise quantities.
     *
     * \note The usual words used in HPC are simply 'local' and 'global'. However, given that in a finite element
     * context those words also get a very different meaning, we chose to rename the ones related to parallelism.
     */
    enum class MpiScale
    {
        processor_wise = 0,
        program_wise
    };


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_MPI_x_MPI_SCALE_HPP_
