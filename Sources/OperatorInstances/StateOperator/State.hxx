/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Feb 2015 10:39:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HXX_


namespace MoReFEM
{


    namespace InterpolationOperatorNS
    {



        inline const State::DirichletDofListType& State::GetProcessorWiseDirichletDofList() const
        {
            return processor_wise_dirichlet_dof_list_;
        }


        inline bool State::AreProcessorWiseDirichletDof() const
        {
            return !processor_wise_dirichlet_dof_list_.empty();
        }


        # ifndef NDEBUG



        inline unsigned int State::NexpectedProgramWiseDofIncludingDirichlet() const noexcept
        {
            assert(Nexpected_program_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<unsigned int>());
            return Nexpected_program_wise_dof_including_dirichlet_;
        }


        inline unsigned int State::NexpectedProgramWiseDofExcludingDirichlet() const noexcept
        {
            assert(Nexpected_program_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<unsigned int>());
            return Nexpected_program_wise_dof_excluding_dirichlet_;
        }


        inline unsigned int State::NexpectedProcessorWiseDofIncludingDirichlet() const noexcept
        {
            assert(Nexpected_processor_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<unsigned int>());
            return Nexpected_processor_wise_dof_including_dirichlet_;

        }


        inline unsigned int State::NexpectedProcessorWiseDofExcludingDirichlet() const noexcept
        {
            assert(Nexpected_processor_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<unsigned int>());
            return Nexpected_processor_wise_dof_excluding_dirichlet_;
        }

        # endif // NDEBUG


    } // namespace InterpolationOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_STATE_OPERATOR_x_STATE_HXX_
