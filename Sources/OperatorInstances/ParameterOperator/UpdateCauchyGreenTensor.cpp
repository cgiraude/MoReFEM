/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Oct 2016 14:15:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


# include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"


namespace MoReFEM
{
    
    
    namespace GlobalParameterOperatorNS
    {
        
        
        UpdateCauchyGreenTensor
        ::UpdateCauchyGreenTensor(const FEltSpace& felt_space,
                                  const Unknown& unknown,
                                  ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>& cauchy_green_tensor,
                                  const QuadratureRulePerTopology* const quadrature_rule_per_topology)
        : parent(felt_space,
                 unknown,
                 quadrature_rule_per_topology,
                 AllocateGradientFEltPhi::yes,
                 cauchy_green_tensor)
        {
            decltype(auto) extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

            felt_space.ComputeLocal2Global(std::move(extended_unknown_ptr),
                                           DoComputeProcessorWiseLocal2Global::yes);
        }
        
        
        const std::string& UpdateCauchyGreenTensor::ClassName()
        {
            static std::string name("UpdateCauchyGreenTensor");
            return name;
        }
        
        
    } // namespace GlobalParameterOperatorNS
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
