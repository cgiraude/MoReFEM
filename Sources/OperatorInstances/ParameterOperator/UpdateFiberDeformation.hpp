/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 17:07:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_FIBER_DEFORMATION_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_FIBER_DEFORMATION_HPP_

# include "Parameters/Parameter.hpp"
# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

# include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"

# include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp"
# include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hpp"


namespace MoReFEM
{


    namespace GlobalParameterOperatorNS
    {


        /*!
         * \brief Implementation of global UpdateFiberDeformation operator.
         *
         * G_BS = (tau_c_n+12# + mu*ec_p_n+12)(1+2ec_n+12) - Es*(e1D_n+12# - ec_n+12)(1+2e1D_n+12#)
         * K22 = dG_BS_dec_n+1
         * K21 = dG_dyn_dy_n+1, where G_dyn is the residual of the dynamics.
         * ec_n+1(k+1) = ec_n+1_(k) + dec
         * dec = K22^-1(-G_BS - K21*dY)
         * ec = fiber deformation
         * dY = displacement increment
         *
         */
        class UpdateFiberDeformation final
        : public GlobalParameterOperator
        <
            UpdateFiberDeformation,
            LocalParameterOperatorNS::UpdateFiberDeformation,
            ParameterNS::Type::scalar
        >
        {

        public:

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<UpdateFiberDeformation>;

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! Convenient alias to pinpoint the GlobalParameterOperator parent.
            using parent = GlobalParameterOperator
            <
                UpdateFiberDeformation,
                LocalParameterOperatorNS::UpdateFiberDeformation,
                ParameterNS::Type::scalar
            >;

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] felt_space Finite element space upon which the operator is defined.
             * \param[in] unknown Unknown considered for this operator (might be scalar or vectorial).
             * \param[in,out] fiber_deformation Fiber deformation parameter, which is updated by present class.
             * \param[in] contraction_rheology_residual Contraction rheology residual.
             * \param[in] schur_complement Schur complement.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             */
            explicit UpdateFiberDeformation(const FEltSpace& felt_space,
                                            const Unknown& unknown,
                                            ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>& fiber_deformation,
                                            const ParameterAtQuadraturePoint<ParameterNS::Type::scalar, ParameterNS::TimeDependencyNS::None>& contraction_rheology_residual,
                                            const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>& schur_complement,
                                            const QuadratureRulePerTopology* const quadrature_rule_per_topology);

            //! Destructor.
            ~UpdateFiberDeformation() = default;

            //! \copydoc doxygen_hide_copy_constructor
            UpdateFiberDeformation(const UpdateFiberDeformation& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            UpdateFiberDeformation(UpdateFiberDeformation&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            UpdateFiberDeformation& operator=(const UpdateFiberDeformation& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            UpdateFiberDeformation& operator=(UpdateFiberDeformation&& rhs) = delete;

            ///@}


            /*!
             * \brief Assemble into one or several vectors.
             *
             * \param[in] displacement_increment Vector that includes data from the previous iteration.
             */
            void Update(const GlobalVector& displacement_increment) const;


        public:

            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally.
             */
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalParameterOperator& local_operator,
                                        const std::tuple<const GlobalVector&>& additional_arguments) const;


        };


    } // namespace GlobalParameterOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/ParameterOperator/UpdateFiberDeformation.hxx"

#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_UPDATE_FIBER_DEFORMATION_HPP_
