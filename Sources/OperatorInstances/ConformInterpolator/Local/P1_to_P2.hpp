/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 17:20:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P2_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P2_HPP_

# include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"



namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Local
        {


            /*!
             * \brief Local operator to interpolate from P1 to P2.
             */
            class P1_to_P2 : public LagrangianNS::LocalLagrangianInterpolator
            {
            private:

                //! Alias to parent.
                using parent = LagrangianNS::LocalLagrangianInterpolator;

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = P1_to_P2;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

                //! Alias to vector of unique pointers.
                using vector_unique_ptr = std::vector<unique_ptr>;

                //! Label of the target shape function label.
                static const std::string& GetTargetShapeFunctionLabel();

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] source_felt_space \a FEltSpace for the P1 dofs,
                 * \param[in] target_ref_local_felt_space \a RefLocalFEltSpace for the target (with the P2 dofs).
                 * \copydoc doxygen_hide_conform_interpolator_interpolation_data_arg
                 *
                 */
                explicit P1_to_P2(const FEltSpace& source_felt_space,
                                  const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                                  const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data);

                //! Destructor.
                virtual ~P1_to_P2();

                //! \copydoc doxygen_hide_copy_constructor
                P1_to_P2(const P1_to_P2& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                P1_to_P2(P1_to_P2&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                P1_to_P2& operator=(const P1_to_P2& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                P1_to_P2& operator=(P1_to_P2&& rhs) = delete;

                ///@}


            };


        } // namespace Local


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/ConformInterpolator/Local/P1_to_P2.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_LOCAL_x_P1_xTO_x_P2_HPP_
