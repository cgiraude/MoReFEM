/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 12:06:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "OperatorInstances/ConformInterpolator/Local/P1b_to_P1.hpp"


namespace MoReFEM
{
    
    
    namespace ConformInterpolatorNS
    {
        
        
        namespace Local
        {
                        
            
            const std::string& P1b_to_P1::ClassName()
            {
                static std::string ret = GetSourceShapeFunctionLabel() + "_to_P1";
                return ret;
            }
            
            
            const std::string& P1b_to_P1::GetSourceShapeFunctionLabel()
            {
                static std::string ret("P1b");
                return ret;
            }

            
            P1b_to_P1::P1b_to_P1(const FEltSpace& source_felt_space,
                                 const Internal::RefFEltNS::RefLocalFEltSpace& target_ref_local_felt_space,
                                 const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
            : parent(source_felt_space,
                     target_ref_local_felt_space,
                     interpolation_data)
            { }
            

            P1b_to_P1::~P1b_to_P1() = default;
          
            
        } // namespace Local
        
        
    } // namespace ConformInterpolatorNS
  

} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
