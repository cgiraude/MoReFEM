/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 16:27:28 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1_xTO_x_P1B_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1_xTO_x_P1B_HPP_

# include "OperatorInstances/ConformInterpolator/Local/P1_to_P1b.hpp"
# include "OperatorInstances/ConformInterpolator/Internal/P1_to_Phigher.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        //! Alias that defines P1 -> P1b interpolator.
        using P1_to_P1b = Internal::ConformInterpolatorNS::P1_to_Phigher<Local::P1_to_P1b>;


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_CONFORM_INTERPOLATOR_x_P1_xTO_x_P1B_HPP_
