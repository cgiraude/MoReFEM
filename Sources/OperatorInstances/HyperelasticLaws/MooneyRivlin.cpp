/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include <string>

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Containers/EnumClass.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"
#include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"



namespace MoReFEM
{
    
   
    namespace HyperelasticLawNS
    {

                    
        const std::string& MooneyRivlin::ClassName()
        {
            static std::string ret("Mooney-Rivlin");
            return ret;
        }
        
        
        MooneyRivlin::MooneyRivlin(const Solid& solid)
        : kappa1_(solid.GetKappa1()),
        kappa2_(solid.GetKappa2()),
        bulk_(solid.GetHyperelasticBulk())
        { }
        
        double MooneyRivlin::W(const invariant_holder_type& invariant_holder,
                               const QuadraturePoint& quad_pt,
                               const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            const double I3_pow_minus_one_third = std::pow(I3, -1. / 3.);
            const double sqrt_I3 = std::sqrt(I3);
            
            return GetKappa1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
            + GetKappa2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.)
            + GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1.) * (sqrt_I3 - 1.);
        }
        
        
        double MooneyRivlin::FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                            const QuadraturePoint& quad_pt,
                                                            const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            
            constexpr const double minus_one_third = -1. / 3.;
            
            return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * minus_one_third * std::pow(I3, 4. * minus_one_third)
            + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 2. * minus_one_third * std::pow(I3, 5. * minus_one_third)
            + GetBulk().GetValue(quad_pt, geom_elt) * (1 - std::pow(I3, -0.5));
        }
        
        
        double MooneyRivlin::SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                                             const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt) const
        {
            const double I1 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I1);
            const double I2 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I2);
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;
            constexpr const double one_ninth = 1. / 9.;

            return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * 4. * one_ninth * std::pow(I3, 7. * minus_one_third)
            + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 10. * one_ninth * std::pow(I3, 8. * minus_one_third)
            + GetBulk().GetValue(quad_pt, geom_elt) * 0.5 * std::pow(I3, -1.5);
        }
        
        
        double MooneyRivlin::SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                     const QuadraturePoint& quad_pt,
                                                                     const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;
            
            return GetKappa1().GetValue(quad_pt, geom_elt) * minus_one_third * std::pow(I3, 4. * minus_one_third);
        }
        
        
        double MooneyRivlin::SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                      const QuadraturePoint& quad_pt,
                                                                      const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            constexpr const double minus_one_third = -1. / 3.;
            
            return GetKappa2().GetValue(quad_pt, geom_elt) * 2. * minus_one_third * std::pow(I3, 5. * minus_one_third);
        }
        
        
        
        double MooneyRivlin::FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                            const QuadraturePoint& quad_pt,
                                                            const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            return GetKappa1().GetValue(quad_pt, geom_elt) * std::pow(I3, -1. / 3.);
        }
        
        
        double MooneyRivlin::FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                             const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt) const
        {
            const double I3 = invariant_holder.GetInvariant(invariant_holder_type::invariants_index::I3);
            return GetKappa2().GetValue(quad_pt, geom_elt) * std::pow(I3, -2. / 3.);
        }
                
        
    
    } // namespace HyperelasticLawNS
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup

