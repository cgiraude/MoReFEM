/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 28 Jan 2019 19:12:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_J6_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_J6_HXX_


namespace MoReFEM
{


    namespace HyperelasticLawNS
    {


        inline constexpr double ExponentialJ1J4J6
        ::SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                           const QuadraturePoint& quad_pt,
                                           const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double ExponentialJ1J4J6
        ::SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quad_pt,
                                                   const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double ExponentialJ1J4J6
        ::SecondDerivativeWFirstAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quad_pt,
                                                   const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double ExponentialJ1J4J6
        ::SecondDerivativeWSecondAndFourthInvariant(const invariant_holder_type& invariant_holder,
                                                    const QuadraturePoint& quad_pt,
                                                    const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double ExponentialJ1J4J6
        ::SecondDerivativeWFirstAndSixthInvariant(const invariant_holder_type& invariant_holder,
                                                  const QuadraturePoint& quad_pt,
                                                  const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline constexpr double ExponentialJ1J4J6
        ::SecondDerivativeWSecondAndSixthInvariant(const invariant_holder_type& invariant_holder,
                                                   const QuadraturePoint& quad_pt,
                                                   const GeometricElt& geom_elt) noexcept
        {
            static_cast<void>(geom_elt);
            static_cast<void>(quad_pt);
            static_cast<void>(invariant_holder);
            return 0.;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetMu1() const noexcept
        {
            return mu_1_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetMu2() const noexcept
        {
            return mu_2_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetC0() const noexcept
        {
            return c_0_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetC1() const noexcept
        {
            return c_1_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetC2() const noexcept
        {
            return c_2_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetC3() const noexcept
        {
            return c_3_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetC4() const noexcept
        {
            return c_4_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetC5() const noexcept
        {
            return c_5_;
        }


        inline const ExponentialJ1J4J6::scalar_parameter& ExponentialJ1J4J6::GetBulk() const noexcept
        {
            return bulk_;
        }


        inline const FiberList<ParameterNS::Type::vector>* ExponentialJ1J4J6::GetFibersI4() const noexcept
        {
            return fibers_I4_;
        }


        inline const FiberList<ParameterNS::Type::vector>* ExponentialJ1J4J6::GetFibersI6() const noexcept
        {
            return fibers_I6_;
        }

        
    } // namespace HyperelasticLawNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_J6_HXX_
