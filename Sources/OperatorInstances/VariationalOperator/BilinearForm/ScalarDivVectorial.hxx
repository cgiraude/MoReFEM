/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 8 Sep 2014 14:19:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SCALAR_DIV_VECTORIAL_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SCALAR_DIV_VECTORIAL_HXX_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template<class LinearAlgebraTupleT>
        inline void ScalarDivVectorial
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple, const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
        }


    } //  namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_SCALAR_DIV_VECTORIAL_HXX_
