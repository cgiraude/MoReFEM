/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 16:03:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_ALE_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_ALE_HPP_

# include "Parameters/Parameter.hpp"

# include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"
# include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"

# include "OperatorInstances/VariationalOperator/BilinearForm/Local/Ale.hpp"


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        /*!
         * \brief Operator description.
         *
         * \todo #9 Describe operator!
         */
        class Ale final : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            Ale,
            Advanced::OperatorNS::Nature::bilinear,
            Advanced::LocalVariationalOperatorNS::Ale
        >
        {

        public:

            //! Returns the name of the operator.
            static const std::string& ClassName();

            //! \copydoc doxygen_hide_alias_self
            using self = Ale;

            //! Alias to unique pointer.
            using const_unique_ptr = std::unique_ptr<const self>;

            //! Alias to local operator.
            using local_operator_type = Advanced::LocalVariationalOperatorNS::Ale;

            //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
            using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
            <
                self,
                Advanced::OperatorNS::Nature::bilinear,
                local_operator_type
            >;

            //! Friendship to the parent class so that the CRTP can reach private methods defined below.
            friend parent;

            //! \copydoc doxygen_hide_operator_alias_scalar_parameter
            using scalar_parameter = typename local_operator_type::scalar_parameter;


        public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] unknown_ptr Velocity unknown.
             * \copydoc doxygen_hide_test_unknown_ptr_param
             * \param[in] density Density parameter.
             * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
             * \copydoc doxygen_hide_gvo_felt_space_arg
             */
            explicit Ale(const FEltSpace& felt_space,
                         const Unknown::const_shared_ptr unknown_ptr,
                         const Unknown::const_shared_ptr test_unknown_ptr,
                         const scalar_parameter& density,
                         const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);


            //! Destructor.
            ~Ale() = default;

            //! \copydoc doxygen_hide_copy_constructor
            Ale(const Ale& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Ale(Ale&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Ale& operator=(const Ale& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Ale& operator=(Ale&& rhs) = delete;

            ///@}


        public:

            /*!
             * \brief Assemble into one or several matrices and/or vectors.
             *
             * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
             * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
             *
             * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
             * assembled. These objects are assumed to be already properly allocated.
             * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
             * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
             * element space; if current \a domain is not a subset of finite element space one, assembling will occur
             * upon the intersection of both.
             * \param[in] modified_velocity_from_previous_iteration As it said. \todo #9 Improve comment!
             *
             */
            template<class LinearAlgebraTupleT>
            void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                          const GlobalVector& modified_velocity_from_previous_iteration,
                          const Domain& domain = Domain()) const;


        public:


            /*!
             * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
             * a tuple.
             *
             * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
             * variable is actually set in this call.
             * \param[in] local_felt_space List of finite elements being considered; all those related to the
             * same GeometricElt.
             * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
             * These arguments might need treatment before being given to ComputeEltArray: for instance if there
             * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
             * relevant locally.
             */
            template<class LocalOperatorTypeT>
            void
            SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                        LocalOperatorTypeT& local_operator,
                                        const std::tuple<const GlobalVector&>& additional_arguments) const;




        };


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/BilinearForm/Ale.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_ALE_HPP_
