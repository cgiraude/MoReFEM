/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/




#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


#include "Utilities/Numeric/Numeric.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        SurfacicBidomain::SurfacicBidomain(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                           const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                           elementary_data_type&& a_elementary_data,
                                           const scalar_parameter& intracellular_trans_diffusion_tensor,
                                           const scalar_parameter& extracellular_trans_diffusion_tensor,
                                           const scalar_parameter& intracellular_fiber_diffusion_tensor,
                                           const scalar_parameter& extracellular_fiber_diffusion_tensor,
                                           const scalar_parameter& heterogeneous_conductivity_coefficient,
                                           const FiberList<ParameterNS::Type::vector>& fibers,
                                           const FiberList<ParameterNS::Type::scalar>& angles)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        matrix_parent(),
        vector_parent(),
        intracellular_trans_diffusion_tensor_(intracellular_trans_diffusion_tensor),
        extracellular_trans_diffusion_tensor_(extracellular_trans_diffusion_tensor),
        intracellular_fiber_diffusion_tensor_(intracellular_fiber_diffusion_tensor),
        extracellular_fiber_diffusion_tensor_(extracellular_fiber_diffusion_tensor),
        heterogeneous_conductivity_coefficient_(heterogeneous_conductivity_coefficient),
        fibers_(fibers),
        angles_(angles)
        {
            const auto& elementary_data = GetElementaryData();

            const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode();

            const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
            const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode();

            const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const int Nnode_for_test_unknown1 = static_cast<int>(test_unknown1_ref_felt.Nnode());

            const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));
            const int Nnode_for_test_unknown2 = static_cast<int>(test_unknown2_ref_felt.Nnode());

            const unsigned int ref_felt_space_dimension = unknown1_ref_felt.GetFEltSpaceDimension();

            const unsigned int mesh_dimension = unknown1_ref_felt.GetMeshDimension();

            assert(ref_felt_space_dimension < mesh_dimension && "This operator is a surfacic operator.");
            assert((ref_felt_space_dimension + 1) == mesh_dimension && "This operator is a surfacic operator.");

            InitLocalMatrixStorage({
                {
                    { Nnode_for_test_unknown1, Nnode_for_unknown1}, // block matrix 1 (Vm,Vm)
                    { Nnode_for_test_unknown2, Nnode_for_unknown2}, // block matrix 2 (Ue,Ue)
                    { Nnode_for_test_unknown1, Nnode_for_unknown2}, // block matrix 3 (Vm, Ue)
                    { Nnode_for_test_unknown2, Nnode_for_unknown1}, // block matrix 4 (Ue, Vm)
                    { ref_felt_space_dimension, Nnode_for_unknown1}, // transposed dPhi
                    { ref_felt_space_dimension, Nnode_for_unknown2}, // transposed dPsi
                    { Nnode_for_test_unknown1, ref_felt_space_dimension}, // dPhi_test*sigma
                    { Nnode_for_test_unknown2, ref_felt_space_dimension}, // dPsi_test*sigma
                    { Nnode_for_test_unknown1, ref_felt_space_dimension}, // dPhi_test*contravariant_metric_tensor
                    { Nnode_for_test_unknown2, ref_felt_space_dimension}, // dPsi_test*contravariant_metric_tensor
                    { ref_felt_space_dimension, ref_felt_space_dimension}, // tau_0_X_tau_0
                    { ref_felt_space_dimension, ref_felt_space_dimension}, // tau_0_ortho_X_tau_0_ortho
                    { mesh_dimension, mesh_dimension}, // covariant basis
                    { mesh_dimension, mesh_dimension}, // transpose covariant basis
                    { mesh_dimension, mesh_dimension}, // contravariant basis
                    { mesh_dimension, mesh_dimension}, // covariant metric tensor
                    { mesh_dimension, mesh_dimension}, // full contravariant metric tensor
                    { ref_felt_space_dimension, ref_felt_space_dimension} // reduced contravariant metric tensor
                }});

            this->InitLocalVectorStorage
            ({{
                mesh_dimension, // tau_interpolate_ortho
                ref_felt_space_dimension, // tau_covariant_basis
                ref_felt_space_dimension // tau_ortho_covariant_basis
            }});
        }


        SurfacicBidomain::~SurfacicBidomain() = default;


        const std::string& SurfacicBidomain::ClassName()
        {
            static std::string name("SurfacicBidomain");
            return name;
        }


        void SurfacicBidomain::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();

            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.fill(0.);

            const auto& unknown1_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& unknown2_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));

            const auto& test_unknown1_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const auto& test_unknown2_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(1));

            const int mesh_dimension = static_cast<int>(unknown1_ref_felt.GetMeshDimension());

            auto& block_matrix1 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix1)>();
            auto& block_matrix2 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix2)>();
            auto& block_matrix3 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix3)>();
            auto& block_matrix4 = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix4)>();
            auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
            auto& transposed_dpsi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dpsi)>();
            auto& dphi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
            auto& dpsi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_test_sigma)>();
            auto& dphi_test_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_contravariant_metric_tensor)>();
            auto& dpsi_test_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dpsi_test_contravariant_metric_tensor)>();
            auto& tau_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_sigma)>();
            auto& tau_ortho_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_ortho_sigma)>();
            auto& contravariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
            auto& reduced_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::reduced_contravariant_metric_tensor)>();

            auto& tau_interpolate_ortho = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_interpolate_ortho)>();
            auto& tau_covariant_basis = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_covariant_basis)>();
            auto& tau_ortho_covariant_basis = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_ortho_covariant_basis)>();

            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            const auto Nnode_for_unknown1 = unknown1_ref_felt.Nnode();
            const auto Nnode_for_unknown2 = unknown2_ref_felt.Nnode();

            assert(unknown1_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");
            assert(unknown2_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");

            const auto Nnode_for_test_unknown1 = test_unknown1_ref_felt.Nnode();
            const auto Nnode_for_test_unknown2 = test_unknown2_ref_felt.Nnode();

            assert(test_unknown1_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");
            assert(test_unknown2_ref_felt.Ncomponent() == 1u && "SurfacicBidomain operator limited to scalar unknowns.");

            const auto& intracellular_trans_diffusion_tensor = GetIntracelluarTransDiffusionTensor();
            const auto& extracellular_trans_diffusion_tensor = GetExtracelluarTransDiffusionTensor();
            const auto& intracellular_fiber_diffusion_tensor = GetIntracelluarFiberDiffusionTensor();
            const auto& extracellular_fiber_diffusion_tensor = GetExtracelluarFiberDiffusionTensor();
            const auto& heterogeneous_conductivity_coefficient = GetHeterogeneousConductivityCoefficient();

            auto& fibers = GetFibers();
            auto& angles = GetAngles();

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            const int felt_space_dimension = static_cast<int>(unknown1_ref_felt.GetFEltSpaceDimension());


            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
                decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                ComputeContravariantBasis(quad_pt_unknown_list_data);

                const double factor = quad_pt.GetWeight()
                                    * quad_pt_unknown_list_data.GetAbsoluteValueJacobianDeterminant();

                const double factor1 = factor * intracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);

                const double factor2 = factor * extracellular_trans_diffusion_tensor.GetValue(quad_pt, geom_elt);

                const double factor3 = factor * intracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

                const double factor4 = factor * extracellular_fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

                const double heterogeneous_conductivity_coefficient_at_quad = heterogeneous_conductivity_coefficient.GetValue(quad_pt, geom_elt);

                const auto& grad_felt_phi_for_unknown = quad_pt_unknown_list_data.GetGradientRefFEltPhi();

                const auto& dphi = ExtractSubMatrix(grad_felt_phi_for_unknown,
                                                    unknown1_ref_felt);

                const auto& dpsi = ExtractSubMatrix(grad_felt_phi_for_unknown,
                                                    unknown2_ref_felt);

                const auto& grad_felt_phi_for_test_unknown = test_unknown_data.GetGradientRefFEltPhi();

                const auto& dphi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown,
                                                         test_unknown1_ref_felt);

                const auto& dpsi_test = ExtractSubMatrix(grad_felt_phi_for_test_unknown,
                                                         test_unknown2_ref_felt);

                assert(dphi.shape(0) == Nnode_for_unknown1);
                assert(dpsi.shape(0) == Nnode_for_unknown2);
                assert(dphi_test.shape(0) == Nnode_for_test_unknown1);
                assert(dpsi_test.shape(0) == Nnode_for_test_unknown2);

                xt::noalias(transposed_dphi) = xt::transpose(dphi);
                xt::noalias(transposed_dpsi) = xt::transpose(dpsi);

                block_matrix1.fill(0.);
                block_matrix2.fill(0.);
                block_matrix3.fill(0.);
                block_matrix4.fill(0.);

                xt::noalias(dphi_test_contravariant_metric_tensor) =
                    factor1 * xt::linalg::dot(dphi_test, reduced_contravariant_metric_tensor);

                xt::noalias(block_matrix1) =
                    xt::linalg::dot(dphi_test_contravariant_metric_tensor, transposed_dphi);


                xt::noalias(dpsi_test_contravariant_metric_tensor) = (factor1 + factor2) *
                    xt::linalg::dot(dpsi_test, reduced_contravariant_metric_tensor);

                xt::noalias(block_matrix2) = xt::linalg::dot(dpsi_test_contravariant_metric_tensor,
                                                             transposed_dpsi);

                xt::noalias(dphi_test_contravariant_metric_tensor) = factor1
                    * xt::linalg::dot(dphi_test, reduced_contravariant_metric_tensor);


                xt::noalias(block_matrix3) = xt::linalg::dot(dphi_test_contravariant_metric_tensor,
                                                             transposed_dpsi);

                xt::noalias(dpsi_test_contravariant_metric_tensor) =
                    factor1 * xt::linalg::dot(dpsi_test, reduced_contravariant_metric_tensor);

                xt::noalias(block_matrix4) = xt::linalg::dot(dpsi_test_contravariant_metric_tensor, transposed_dphi);

                // Fiber part of the operator.
                const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);

                double norm = 0.;
                for (int component = 0 ; component < mesh_dimension ; ++component)
                    norm += tau_interpolate(component) * tau_interpolate(component);

                tau_sigma.fill(0.);
                tau_ortho_sigma.fill(0.);

                if (!(NumericNS::IsZero(norm)))
                {
                    // tau_interpolate_ortho
                    tau_interpolate_ortho.fill(0.);
                    tau_interpolate_ortho(0) = tau_interpolate(1) * contravariant_basis(2,2) - tau_interpolate(2) * contravariant_basis(1,2);
                    tau_interpolate_ortho(1) = tau_interpolate(2) * contravariant_basis(0,2) - tau_interpolate(0) * contravariant_basis(2,2);
                    tau_interpolate_ortho(2) = tau_interpolate(0) * contravariant_basis(1,2) - tau_interpolate(1) * contravariant_basis(0,2);

                    tau_covariant_basis.fill(0.);
                    tau_ortho_covariant_basis.fill(0.);

                    // Projection in the covariant basis.
                    for (int component = 0 ; component < mesh_dimension ; ++component)
                    {
                        for (int cov_component = 0 ; cov_component < felt_space_dimension ; ++cov_component)
                        {
                            tau_covariant_basis(cov_component) += tau_interpolate(component) * contravariant_basis(component,cov_component);
                            tau_ortho_covariant_basis(cov_component) += tau_interpolate_ortho(component) * contravariant_basis(component,cov_component);
                        }
                    }

					xt::noalias(tau_sigma) = xt::linalg::outer(tau_covariant_basis, tau_covariant_basis);
					xt::noalias(tau_ortho_sigma) = xt::linalg::outer(tau_ortho_covariant_basis, tau_ortho_covariant_basis);

                    const auto& theta = angles.GetValue(quad_pt, geom_elt);

                    double i_theta = 0.;

                    if (std::fabs(theta) < NumericNS::DefaultEpsilon<double>())
                        i_theta = 1.;
                    else
                        i_theta = 0.5 + std::sin(2. * theta) / (4. * theta);

                    xt::noalias(tau_ortho_sigma) *= (1. - i_theta);

                    xt::noalias(tau_sigma) *= i_theta / norm;

                    xt::noalias(tau_sigma) += tau_ortho_sigma / norm;

                    xt::noalias(dphi_test_sigma) = (factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad
													   * xt::linalg::dot(dphi_test, tau_sigma);

                    xt::noalias(block_matrix1) += xt::linalg::dot(dphi_test_sigma, transposed_dphi);

                    xt::noalias(dphi_test_sigma) = (factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad
														* xt::linalg::dot(dphi_test, tau_sigma);

                    xt::noalias(block_matrix3) += xt::linalg::dot(dphi_test_sigma, transposed_dpsi);

					xt::noalias(dpsi_test_sigma) = (factor3 - factor1) * heterogeneous_conductivity_coefficient_at_quad
						* xt::linalg::dot(dpsi_test, tau_sigma);

                    xt::noalias(block_matrix4) += xt::linalg::dot(dpsi_test_sigma, transposed_dphi);

                    xt::noalias(dpsi_test_sigma) = (factor3 - factor1 + factor4 - factor2) * heterogeneous_conductivity_coefficient_at_quad
						* xt::linalg::dot(dpsi_test, tau_sigma);

                    xt::noalias(block_matrix2) += xt::linalg::dot(dpsi_test_sigma, transposed_dpsi);
                }

                // Then report it into the elementary matrix.
                for (auto node_test_unknown1_index = 0ul; node_test_unknown1_index < Nnode_for_test_unknown1; ++node_test_unknown1_index)
                {
                    for (auto node_unknown1_index = 0ul; node_unknown1_index < Nnode_for_unknown1; ++node_unknown1_index)
                    {
                        const double value1 = block_matrix1(node_test_unknown1_index, node_unknown1_index);

                        matrix_result(node_test_unknown1_index, node_unknown1_index) += value1;
                    }
                }

                for (auto node_test_unknown2_index = 0ul; node_test_unknown2_index < Nnode_for_test_unknown2; ++node_test_unknown2_index)
                {
                    for (auto node_unknown2_index = 0ul; node_unknown2_index < Nnode_for_unknown2; ++node_unknown2_index)
                    {
                        const double value2 = block_matrix2(node_test_unknown2_index, node_unknown2_index);

                        matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1, node_unknown2_index + Nnode_for_unknown1) += value2;
                    }
                }

                for (auto node_test_unknown2_index = 0ul; node_test_unknown2_index < Nnode_for_test_unknown2; ++node_test_unknown2_index)
                {
                    for (auto node_unknown1_index = 0ul; node_unknown1_index < Nnode_for_unknown1; ++node_unknown1_index)
                    {
                        const double value4 = block_matrix4(node_test_unknown2_index, node_unknown1_index);

                        matrix_result(node_test_unknown2_index + Nnode_for_test_unknown1, node_unknown1_index) += value4;
                    }
                }

                for (auto node_test_unknown1_index = 0ul; node_test_unknown1_index < Nnode_for_test_unknown1; ++node_test_unknown1_index)
                {
                    for (auto node_unknown2_index = 0ul; node_unknown2_index < Nnode_for_unknown2; ++node_unknown2_index)
                    {
                        const double value3 = block_matrix3(node_test_unknown1_index, node_unknown2_index);

                        matrix_result(node_test_unknown1_index, node_unknown2_index + Nnode_for_unknown1) += value3;
                    }
                }
            }
        }


        void SurfacicBidomain
        ::ComputeContravariantBasis(const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data)
        {
            auto& elementary_data = GetNonCstElementaryData();

            const auto mesh_dimension = quad_pt_unknown_list_data.GetMeshDimension();

            auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
            auto& transposed_covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_covariant_basis)>();
            auto& contravariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
            auto& covariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
            auto& contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();
            auto& reduced_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::reduced_contravariant_metric_tensor)>();

            covariant_basis.fill(0.);
            transposed_covariant_basis.fill(0.);
            contravariant_basis.fill(0.);
            covariant_metric_tensor.fill(0.);
            contravariant_metric_tensor.fill(0.);
            reduced_contravariant_metric_tensor.fill(0.);

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            const auto& dphi_geo = quad_pt_unknown_list_data.GetGradientRefGeometricPhi();

            const unsigned int Ncomponent = geom_elt.GetDimension();

            covariant_basis.fill(0.);

            const unsigned int Nshape_function = static_cast<unsigned int>(dphi_geo.shape(0));

            for (unsigned int shape_fct_index = 0u; shape_fct_index < Nshape_function; ++shape_fct_index)
            {
                const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

                for (unsigned int component_shape_function = 0u; component_shape_function < Ncomponent;
					 ++component_shape_function)
                {
                    for (unsigned int coord_index = 0u; coord_index < mesh_dimension; ++coord_index)
                    {
                        covariant_basis(coord_index, component_shape_function) += coords_in_geom_elt[coord_index]
						* dphi_geo(shape_fct_index, component_shape_function);
                    }
                }
            }

            double norm = 0.;

            // Normal computation.
            switch (mesh_dimension)
            {
                case 2:
                    covariant_basis(0,mesh_dimension - 1) = covariant_basis(1,0);
                    covariant_basis(1,mesh_dimension - 1) = - covariant_basis(0,0);
                    norm = std::sqrt(covariant_basis(0,mesh_dimension - 1) * covariant_basis(0,mesh_dimension - 1) + covariant_basis(1,mesh_dimension - 1) * covariant_basis(1,mesh_dimension - 1));
                    covariant_basis(0,mesh_dimension - 1) /= norm;
                    covariant_basis(1,mesh_dimension - 1) /= norm;
                    break;
                case 3:
                    covariant_basis(0,mesh_dimension - 1) = covariant_basis(1,0) * covariant_basis(2,1) - covariant_basis(2,0) * covariant_basis(1,1);
                    covariant_basis(1,mesh_dimension - 1) = covariant_basis(2,0) * covariant_basis(0,1) - covariant_basis(0,0) * covariant_basis(2,1);
                    covariant_basis(2,mesh_dimension - 1) = covariant_basis(0,0) * covariant_basis(1,1) - covariant_basis(1,0) * covariant_basis(0,1);
                    norm = std::sqrt(covariant_basis(0,mesh_dimension - 1) * covariant_basis(0,mesh_dimension - 1) +
                                     covariant_basis(1,mesh_dimension - 1) * covariant_basis(1,mesh_dimension - 1) +
                                     covariant_basis(2,mesh_dimension - 1) * covariant_basis(2,mesh_dimension - 1));
                    covariant_basis(0,mesh_dimension - 1) /= norm;
                    covariant_basis(1,mesh_dimension - 1) /= norm;
                    covariant_basis(2,mesh_dimension - 1) /= norm;
                    break;
            }

            covariant_metric_tensor.fill(0.);

			xt::noalias(transposed_covariant_basis) = xt::transpose(covariant_basis);

            xt::noalias(covariant_metric_tensor) = xt::linalg::dot(transposed_covariant_basis, covariant_basis);

            double determinant = 0;

            Wrappers::Xtensor::ComputeInverseSquareMatrix(covariant_metric_tensor, contravariant_metric_tensor, determinant);

            for (auto i = 0ul; i <Ncomponent ; ++i)
            {
                for (auto j = 0ul; j < Ncomponent; ++j)
                    reduced_contravariant_metric_tensor(i,j) = contravariant_metric_tensor(i,j);
            }

            contravariant_basis.fill(0.);

            xt::noalias(contravariant_basis) = xt::linalg::dot(covariant_basis, contravariant_metric_tensor);
        }


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
