/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 May 2014 16:04:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Mass.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        Mass::Mass(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                   elementary_data_type&& a_elementary_data)
        : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data))
        {
            assert(unknown_list.size() == 1);
            assert(test_unknown_list.size() == 1);
        }
        
        
        Mass::~Mass() = default;
       
        
        const std::string& Mass::ClassName()
        {
            static std::string name("Mass");
            return name;
        }
        
        
        void Mass::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();
            
            const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            
            const auto Ncomponent = ref_felt.Ncomponent();
            
            assert(Ncomponent == test_ref_felt.Ncomponent());
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();            
            
            const auto Nnode_for_unknown = ref_felt.Nnode();
            const auto Nnode_for_test_unknown = test_ref_felt.Nnode();
            
            
            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
                decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();
                
                const auto& phi_unknown = quad_pt_unknown_data.GetRefFEltPhi();
                const auto& phi_test_unknown = test_unknown_data.GetRefFEltPhi();
                
                const double factor = infos_at_quad_pt.GetQuadraturePoint().GetWeight()
                                    * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();
                
                for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
                {
                    const double m_value = factor * phi_test_unknown(m);
                    
                    for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                    {
                        const double value = m_value * phi_unknown(n);
                        
                        for (auto component = 0ul; component < Ncomponent; ++component)
                        {
                            const auto shift = Nnode_for_test_unknown * component;
                            matrix_result(m + shift, n + shift) += value;
                        }
                    }
                }
            }
        }

        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
