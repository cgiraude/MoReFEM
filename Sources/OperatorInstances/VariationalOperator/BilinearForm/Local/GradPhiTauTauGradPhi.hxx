/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 9 Aug 2017 17:28:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_TAU_GRAD_PHI_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_TAU_GRAD_PHI_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        inline const GradPhiTauTauGradPhi::scalar_parameter& GradPhiTauTauGradPhi::GetTransverseDiffusionTensor() const noexcept
        {
            return transverse_diffusion_tensor_;
        }


        inline const GradPhiTauTauGradPhi::scalar_parameter& GradPhiTauTauGradPhi::GetFiberDiffusionTensor() const noexcept
        {
            return fiber_diffusion_tensor_;
        }


        inline const FiberList<ParameterNS::Type::vector>& GradPhiTauTauGradPhi::GetFibers() const noexcept
        {
            return fibers_;
        }


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_LOCAL_x_GRAD_PHI_TAU_TAU_GRAD_PHI_HXX_
