/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 8 Sep 2014 12:16:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/ScalarDivVectorial.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {
        
        
        
        ScalarDivVectorial::ScalarDivVectorial(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                               const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                               elementary_data_type&& a_elementary_data,
                                               double alpha,
                                               double beta)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        alpha_(alpha),
        beta_(beta)
        {
            #ifndef NDEBUG
            {
                const auto& unknown_storage = this->GetExtendedUnknownList();
                assert(unknown_storage.size() == 2ul && "Namesake GlobalVariationalOperator should provide the correct one.");
                assert(!(!unknown_storage.front()));
                assert("Namesake GlobalVariationalOperator should provide two unknowns, the first being vectorial"
                       && unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
                assert(!(!unknown_storage.back()));
                assert("Namesake GlobalVariationalOperator should provide two unknowns, the second being scalar"
                       && unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);

                const auto& test_unknown_storage = this->GetExtendedTestUnknownList();
                assert(test_unknown_storage.size() == 2ul && "Namesake GlobalVariationalOperator should provide the correct one.");
                assert(!(!test_unknown_storage.front()));
                assert("Namesake GlobalVariationalOperator should provide two test unknowns, the first being vectorial"
                       && test_unknown_storage.front()->GetUnknown().GetNature() == UnknownNS::Nature::vectorial);
                assert(!(!test_unknown_storage.back()));
                assert("Namesake GlobalVariationalOperator should provide two test unknowns, the second being scalar"
                       && test_unknown_storage.back()->GetUnknown().GetNature() == UnknownNS::Nature::scalar);
            }
            #endif // NDEBUG            
        }
        
        
        ScalarDivVectorial::~ScalarDivVectorial() = default;
        
        
        const std::string& ScalarDivVectorial::ClassName()
        {
            static std::string name("ScalarDivVectorial");
            return name;
        }
        
        
        void ScalarDivVectorial::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();

            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
                        
            const auto& scalar_ref_felt =
                elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::scalar)));
            const auto& vectorial_ref_felt =
                elementary_data.GetRefFElt(GetNthUnknown(EnumUnderlyingType(UnknownIndex::vectorial)));
            
            const auto& scalar_test_ref_felt =
                elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::scalar)));
            const auto& vectorial_test_ref_felt =
                elementary_data.GetTestRefFElt(GetNthTestUnknown(EnumUnderlyingType(TestUnknownIndex::vectorial)));
            
            const auto Nnode_velocity = static_cast<std::size_t>(vectorial_ref_felt.Nnode());
            const auto Ndof_velocity = static_cast<std::size_t>(vectorial_ref_felt.Ndof());
            const auto Nnode_pressure = static_cast<std::size_t>(scalar_ref_felt.Nnode());
            
            const auto Nnode_test_velocity = static_cast<std::size_t>(vectorial_test_ref_felt.Nnode());
            const auto Ndof_test_velocity = static_cast<std::size_t>(vectorial_test_ref_felt.Ndof());
            const auto Nnode_test_pressure = static_cast<std::size_t>(scalar_test_ref_felt.Nnode());
            
            const auto Ncomponent = static_cast<std::size_t>(vectorial_ref_felt.Ncomponent());
            
            assert(Ncomponent == static_cast<std::size_t>(vectorial_test_ref_felt.Ncomponent()));
            
            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            const double alpha = GetAlpha();
            const double beta = GetBeta();

            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                decltype(auto) quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
                decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();
                
                const auto& felt_phi = quad_pt_unknown_list_data.GetFEltPhi(); // on (v p)
                const auto& gradient_felt_phi = quad_pt_unknown_list_data.GetGradientFEltPhi(); // on (v p)
                
                const auto& test_felt_phi = test_unknown_data.GetFEltPhi(); // on (v* p*)
                const auto& test_gradient_felt_phi = test_unknown_data.GetGradientFEltPhi(); // on (v* p*)
                
                const auto& pressure_felt_phi = ExtractSubVector(felt_phi,
                                                                 scalar_ref_felt); // only on p
                
                const auto& test_pressure_felt_phi = ExtractSubVector(test_felt_phi,
                                                                      scalar_test_ref_felt); // only on p*

                const double factor = quad_pt.GetWeight()
                                    * quad_pt_unknown_list_data.GetAbsoluteValueJacobianDeterminant();
                
                // Part in (v* p)
                for (auto node_test_velocity_index = 0ul; node_test_velocity_index < Nnode_test_velocity; ++node_test_velocity_index)
                {
                    for (auto node_pressure_index = 0ul; node_pressure_index < Nnode_pressure; ++node_pressure_index)
                    {
                        const auto dof_pressure = node_pressure_index;
         
                        // The two terms below give the same result but in a different manner. It is just here to show how to use them.
                        //const auto test_pressure_term = test_felt_phi(Nnode_test_velocity + node_test_pressure_index);
                        const auto pressure_term = pressure_felt_phi(node_pressure_index);
                        
                        for (auto component = 0ul; component < Ncomponent; ++component)
                        {
                            const auto dof_test_velocity_index = node_test_velocity_index + Nnode_test_velocity * component;

                            const double product =
                                factor * pressure_term * test_gradient_felt_phi(node_test_velocity_index, component);

                            assert(dof_test_velocity_index < matrix_result.shape(0));
                            assert(Ndof_velocity + dof_pressure < matrix_result.shape(1));
                            matrix_result(dof_test_velocity_index, Ndof_velocity + dof_pressure) += beta * product;
                        }
                    }
                }
                
                // Part in (p* v)
                for (auto node_test_pressure_index = 0ul; node_test_pressure_index < Nnode_test_pressure; ++node_test_pressure_index)
                {
                    const auto dof_test_pressure = node_test_pressure_index;
                    
                    // The two terms below give the same result but in a different manner. It is just here to show how to use them.
                    // const auto pressure_term = felt_phi(Nnode_velocity + node_pressure_index);
                    const auto test_pressure_term = test_pressure_felt_phi(node_test_pressure_index);
                    
                    for (auto node_velocity_index = 0ul; node_velocity_index < Nnode_velocity; ++node_velocity_index)
                    {
                        auto dof_velocity_index = node_velocity_index;
                        
                        for (auto component = 0ul; component < Ncomponent; ++component, dof_velocity_index += Nnode_velocity)
                        {
                            const double product =
                                factor * test_pressure_term * gradient_felt_phi(node_velocity_index, component);

                            assert(Ndof_test_velocity + dof_test_pressure < matrix_result.shape(0));
                            assert(dof_velocity_index < matrix_result.shape(1));

                            matrix_result(Ndof_test_velocity + dof_test_pressure, dof_velocity_index) += alpha * product;
                        }
                    }
                }
            }            
        }
        

    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
