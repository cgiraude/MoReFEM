/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 9 Aug 2017 17:28:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauTauGradPhi.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace LocalVariationalOperatorNS
    {
        
        
        GradPhiTauTauGradPhi::GradPhiTauTauGradPhi(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                                   const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
                                                   elementary_data_type&& a_elementary_data,
                                                   const scalar_parameter& transverse_diffusion_tensor,
                                                   const scalar_parameter& fiber_diffusion_tensor,
                                                   const FiberList<ParameterNS::Type::vector>& fibers)
        : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)),
        matrix_parent(),
        transverse_diffusion_tensor_(transverse_diffusion_tensor),
        fiber_diffusion_tensor_(fiber_diffusion_tensor),
        fibers_(fibers)
        {
            const auto& elementary_data = GetElementaryData();
            
            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto Nnode_for_unknown = unknown_ref_felt.Nnode();
            
            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();
            
            const auto felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();
            
            InitLocalMatrixStorage({
                {
                    { Nnode_for_test_unknown, Nnode_for_unknown}, // block matrix
                    { felt_space_dimension, Nnode_for_unknown }, // transposed dPhi
                    { Nnode_for_test_unknown, felt_space_dimension }, // dPhi_test*sigma
                    { felt_space_dimension, felt_space_dimension } // tau_X_tau
                }});
        }
                                                                       
                                                                       
        GradPhiTauTauGradPhi::~GradPhiTauTauGradPhi() = default;
        
                                                                       
        const std::string& GradPhiTauTauGradPhi::ClassName()
        {
            static std::string name("GradPhiTauTauGradPhi");
            return name;
        }
        
        
        void GradPhiTauTauGradPhi::ComputeEltArray()
        {
            auto& elementary_data = GetNonCstElementaryData();
            
            auto& matrix_result = elementary_data.GetNonCstMatrixResult();
            matrix_result.fill(0.);
            
            const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            
            // Current operator yields in fact a diagonal per block matrix where each block is the same.
            auto& block_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix)>();
            auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
            auto& dphi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
            auto& tau_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_sigma)>();
            
            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            
            const auto Nnode_for_unknown = ref_felt.Nnode();
            const auto Nnode_for_test_unknown = test_ref_felt.Nnode();
            
            const auto Ncomponent = ref_felt.Ncomponent();
            
            assert(Ncomponent == test_ref_felt.Ncomponent());
            
            assert(Ncomponent == 1u && "GradPhiTauTauGradPhi operator limited to scalar unknowns.");
            
            static_cast<void>(Ncomponent);
            
            const auto& transverse_diffusion_tensor = GetTransverseDiffusionTensor();
            const auto& fiber_diffusion_tensor = GetFiberDiffusionTensor();
            
            const auto& geom_elt = elementary_data.GetCurrentGeomElt();
            
            const auto felt_space_dimension = ref_felt.GetFEltSpaceDimension();
            
            auto& fibers = GetFibers();
            
            
            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
                decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                // First compute the content of the block matrix.
                const double factor = quad_pt.GetWeight()
                                    * quad_pt_unknown_list_data.GetAbsoluteValueJacobianDeterminant();

                const double factor1 = factor * transverse_diffusion_tensor.GetValue(quad_pt, geom_elt);
                const double factor2 = factor * fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);
                
                const auto& dphi = ExtractSubMatrix(quad_pt_unknown_list_data.GetGradientFEltPhi(),
                                                    ref_felt);
                
                const auto& dphi_test = ExtractSubMatrix(test_unknown_data.GetGradientFEltPhi(),
                                                    test_ref_felt);
                
                assert(dphi.shape(0) == Nnode_for_unknown);
                assert(dphi_test.shape(0) == Nnode_for_test_unknown);
                
                xt::noalias(transposed_dphi) = xt::transpose(dphi);
                
                block_matrix.fill(0.);
                
                xt::noalias(block_matrix) = factor1 * xt::linalg::dot(dphi_test, transposed_dphi);
                
                const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);
                
                double norm = 0.;
                for (auto component = 0ul; component < felt_space_dimension; ++component)
                    norm += tau_interpolate(component) * tau_interpolate(component);
                
                tau_sigma.fill(0.);

                if (!(NumericNS::IsZero(norm)))
                {                   
					xt::noalias(tau_sigma) = xt::linalg::outer(tau_interpolate, tau_interpolate) / norm;
                    
                    xt::noalias(dphi_test_sigma) = (factor2 - factor1) * xt::linalg::dot(dphi_test, tau_sigma);

                    xt::noalias(block_matrix) += xt::linalg::dot(dphi_test_sigma, transposed_dphi);
                }

                // Then report it into the elementary matrix.
                for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
                {
                    for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                    {
                        const double value = block_matrix(m, n);
                        
                        matrix_result(m, n) += value;
                    }
                }
            }
        }
        
        
    } // namespace LocalVariationalOperatorNS
        
        
    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
