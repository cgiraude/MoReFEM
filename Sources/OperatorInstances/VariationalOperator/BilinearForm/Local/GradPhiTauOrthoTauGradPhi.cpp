/*!
 //
 // \file
 //
 //
 // Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 19 Oct 2015 17:44:11 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup OperatorInstancesGroup
 // \addtogroup OperatorInstancesGroup
 // \{
 */

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauOrthoTauGradPhi.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            GradPhiTauOrthoTauGradPhi::GradPhiTauOrthoTauGradPhi(const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
                                                                 const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_list,
                                                                 elementary_data_type&& a_elementary_data,
                                                                 const scalar_parameter& transverse_diffusion_tensor,
                                                                 const scalar_parameter& fiber_diffusion_tensor,
                                                                 const FiberList<ParameterNS::Type::vector>& fibers,
                                                                 const FiberList<ParameterNS::Type::scalar>& angles)
            : BilinearLocalVariationalOperator(a_unknown_storage, a_test_unknown_list, std::move(a_elementary_data)),
            matrix_parent(),
            vector_parent(),
            transverse_diffusion_tensor_(transverse_diffusion_tensor),
            fiber_diffusion_tensor_(fiber_diffusion_tensor),
            fibers_(fibers),
            angles_(angles)
            {
                const auto& elementary_data = GetElementaryData();
                const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto Nnode_for_unknown = ref_felt.Nnode();

                const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
                const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

                const unsigned int ref_felt_space_dimension = ref_felt.GetFEltSpaceDimension();

                const unsigned int mesh_dimension = ref_felt.GetMeshDimension();

                assert(ref_felt_space_dimension < mesh_dimension && "This operator is a surfacic operator.");
                assert((ref_felt_space_dimension + 1) == mesh_dimension && "This operator is a surfacic operator.");

                InitLocalMatrixStorage({
                    {
                        { Nnode_for_test_unknown, Nnode_for_unknown}, // block matrix (Vm,Vm)
                        { ref_felt_space_dimension, Nnode_for_unknown}, // transposed dPhi
                        { Nnode_for_test_unknown, ref_felt_space_dimension}, // dPhi_test*sigma
                        { Nnode_for_test_unknown, ref_felt_space_dimension}, // dPhi_test*contravariant_metric_tensor
                        { ref_felt_space_dimension, ref_felt_space_dimension}, // tau_0_X_tau_0
                        { ref_felt_space_dimension, ref_felt_space_dimension}, // tau_0_ortho_X_tau_0_ortho
                        { mesh_dimension, mesh_dimension}, // covariant basis
                        { mesh_dimension, mesh_dimension}, // transpose covariant basis
                        { mesh_dimension, mesh_dimension}, // contravariant basis
                        { mesh_dimension, mesh_dimension}, // covariant metric tensor
                        { mesh_dimension, mesh_dimension}, // full contravariant metric tensor
                        { ref_felt_space_dimension, ref_felt_space_dimension} // reduced contravariant metric tensor
                    }});

                this->InitLocalVectorStorage
                ({{
                    mesh_dimension, // tau_interpolate_ortho
                    ref_felt_space_dimension, // tau_covariant_basis
                    ref_felt_space_dimension // tau_ortho_covariant_basis
                }});
            }


            GradPhiTauOrthoTauGradPhi::~GradPhiTauOrthoTauGradPhi() = default;


            const std::string& GradPhiTauOrthoTauGradPhi::ClassName()
            {
                static std::string name("GradPhiTauOrthoTauGradPhi");
                return name;
            }


            void GradPhiTauOrthoTauGradPhi::ComputeEltArray()
            {
                auto& elementary_data = GetNonCstElementaryData();

                auto& matrix_result = elementary_data.GetNonCstMatrixResult();
                matrix_result.fill(0.);

                const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
                const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

                const auto mesh_dimension = ref_felt.GetMeshDimension();

                auto& block_matrix = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_matrix)>();
                auto& transposed_dphi = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
                auto& dphi_test_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_sigma)>();
                auto& dphi_test_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_contravariant_metric_tensor)>();
                auto& tau_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_sigma)>();
                auto& tau_ortho_sigma = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tau_ortho_sigma)>();
                auto& contravariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
                auto& reduced_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::reduced_contravariant_metric_tensor)>();

                auto& tau_interpolate_ortho = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_interpolate_ortho)>();
                auto& tau_covariant_basis = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_covariant_basis)>();
                auto& tau_ortho_covariant_basis = GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_ortho_covariant_basis)>();

                const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

                const auto Nnode_for_unknown = ref_felt.Nnode();
                const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

                assert(ref_felt.Ncomponent() == 1u && "GradPhiTauOrthoTauGradPhi operator limited to scalar unknowns.");

                const auto& transverse_diffusion_tensor = GetTransverseDiffusionTensor();
                const auto& fiber_diffusion_tensor = GetFiberDiffusionTensor();

                auto& fibers = GetFibers();
                auto& angles = GetAngles();

                const auto& geom_elt = elementary_data.GetCurrentGeomElt();

                const auto felt_space_dimension = ref_felt.GetFEltSpaceDimension();

                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
                    decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                    ComputeContravariantBasis(quad_pt_unknown_list_data);

                    const double factor = infos_at_quad_pt.GetQuadraturePoint().GetWeight()
                    * quad_pt_unknown_list_data.GetAbsoluteValueJacobianDeterminant();

                    const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                    const double factor1 = factor * transverse_diffusion_tensor.GetValue(quad_pt, geom_elt);

                    const double factor2 = factor * fiber_diffusion_tensor.GetValue(quad_pt, geom_elt);

                    const auto& dphi = ExtractSubMatrix(quad_pt_unknown_list_data.GetGradientRefFEltPhi(),
                                                        ref_felt);

                    const auto& dphi_test = ExtractSubMatrix(test_unknown_data.GetGradientRefFEltPhi(),
                                                             test_ref_felt);

                    assert(dphi.shape(0) == Nnode_for_unknown);
                    assert(dphi_test.shape(0) == Nnode_for_test_unknown);

                    xt::noalias(transposed_dphi) = xt::transpose(dphi);

                    block_matrix.fill(0.);

                    xt::noalias(dphi_test_contravariant_metric_tensor) = factor1 *
                    xt::linalg::dot(dphi_test, reduced_contravariant_metric_tensor);

                    xt::noalias(block_matrix) = xt::linalg::dot(dphi_test_contravariant_metric_tensor, transposed_dphi);

					// Fiber part of the operator.
					const auto& tau_interpolate = fibers.GetValue(quad_pt, geom_elt);

					double norm = 0.;
					for (auto component = 0ul; component < mesh_dimension; ++component)
						norm += tau_interpolate(component) * tau_interpolate(component);

					tau_sigma.fill(0.);
					tau_ortho_sigma.fill(0.);

					if (!(NumericNS::IsZero(norm)))
					{
                        // tau_interpolate_ortho
                        tau_interpolate_ortho.fill(0.);
                        tau_interpolate_ortho(0) = tau_interpolate(1) * contravariant_basis(2,2) - tau_interpolate(2) * contravariant_basis(1,2);
                        tau_interpolate_ortho(1) = tau_interpolate(2) * contravariant_basis(0,2) - tau_interpolate(0) * contravariant_basis(2,2);
                        tau_interpolate_ortho(2) = tau_interpolate(0) * contravariant_basis(1,2) - tau_interpolate(1) * contravariant_basis(0,2);

                        tau_covariant_basis.fill(0.);
                        tau_ortho_covariant_basis.fill(0.);

                        // Projection in the covariant basis.
                        for (auto cov_component = 0ul; cov_component < felt_space_dimension; ++cov_component)
                        {
                            for (auto component = 0ul; component < mesh_dimension; ++component)
                            {
                                tau_covariant_basis(cov_component) +=
									tau_interpolate(component) * contravariant_basis(component, cov_component);
                                tau_ortho_covariant_basis(cov_component) +=
									tau_interpolate_ortho(component) * contravariant_basis(component, cov_component);
                            }
                        }

						xt::noalias(tau_sigma) = xt::linalg::outer(tau_covariant_basis, tau_covariant_basis);
						xt::noalias(tau_ortho_sigma) = xt::linalg::outer(tau_ortho_covariant_basis, tau_ortho_covariant_basis);

                        const auto& theta = angles.GetValue(quad_pt, geom_elt);

                        double i_theta = 0.;

                        if (std::fabs(theta) < NumericNS::DefaultEpsilon<double>())
                            i_theta = 1.;
                        else
                            i_theta = 0.5 + std::sin(2. * theta) / (4. * theta);

                        tau_ortho_sigma *= (1. - i_theta);
                        tau_sigma *= i_theta / norm;

                        tau_sigma += tau_ortho_sigma / norm;

                        xt::noalias(dphi_test_sigma) =
						(factor2 - factor1) * xt::linalg::dot(dphi_test, tau_sigma);

                        xt::noalias(block_matrix) += xt::linalg::dot(dphi_test_sigma, transposed_dphi);
                    }

						// Then report it into the elementary matrix.
					for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
					{
                        for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                        {
                            const double value1 = block_matrix(m, n);

                            matrix_result(m, n) += value1;
                        }
                    }
				}
			}


			void GradPhiTauOrthoTauGradPhi
			::ComputeContravariantBasis(const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data)
			{
				auto& elementary_data = GetNonCstElementaryData();

				auto& covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
				auto& transposed_covariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_covariant_basis)>();
				auto& contravariant_basis = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
				auto& covariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
				auto& contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();
				auto& reduced_contravariant_metric_tensor = GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::reduced_contravariant_metric_tensor)>();

				covariant_basis.fill(0.);
				transposed_covariant_basis.fill(0.);
				contravariant_basis.fill(0.);
				covariant_metric_tensor.fill(0.);
				contravariant_metric_tensor.fill(0.);
				reduced_contravariant_metric_tensor.fill(0.);

				const auto& geom_elt = elementary_data.GetCurrentGeomElt();

				const auto& dphi_geo = quad_pt_unknown_list_data.GetGradientRefGeometricPhi();

				const auto mesh_dimension = quad_pt_unknown_list_data.GetMeshDimension();
				const unsigned int Ncomponent = geom_elt.GetDimension();

				covariant_basis.fill(0.);

				const unsigned int Nshape_function = static_cast<unsigned int>(dphi_geo.shape(0));

				for (unsigned int shape_fct_index = 0u; shape_fct_index < Nshape_function; ++shape_fct_index)
				{
					const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

					for (unsigned int component_shape_function = 0u; component_shape_function < Ncomponent;
						 ++component_shape_function)
					{
						for (unsigned int coord_index = 0u; coord_index < mesh_dimension; ++coord_index)
						{
							covariant_basis(coord_index, component_shape_function) +=
								coords_in_geom_elt[coord_index] * dphi_geo(shape_fct_index, component_shape_function);
						}
					}
				}

				double norm = 0.;

					// Normal computation.
				switch (mesh_dimension)
				{
					case 2:
						covariant_basis(0,mesh_dimension - 1) = covariant_basis(1,0);
						covariant_basis(1,mesh_dimension - 1) = - covariant_basis(0,0);
						norm = std::sqrt(covariant_basis(0,mesh_dimension - 1) * covariant_basis(0,mesh_dimension - 1) + covariant_basis(1,mesh_dimension - 1) * covariant_basis(1,mesh_dimension - 1));
						covariant_basis(0,mesh_dimension - 1) /= norm;
						covariant_basis(1,mesh_dimension - 1) /= norm;
						break;
					case 3:
						covariant_basis(0,mesh_dimension - 1) = covariant_basis(1,0) * covariant_basis(2,1) - covariant_basis(2,0) * covariant_basis(1,1);
						covariant_basis(1,mesh_dimension - 1) = covariant_basis(2,0) * covariant_basis(0,1) - covariant_basis(0,0) * covariant_basis(2,1);
						covariant_basis(2,mesh_dimension - 1) = covariant_basis(0,0) * covariant_basis(1,1) - covariant_basis(1,0) * covariant_basis(0,1);
						norm = std::sqrt(covariant_basis(0,mesh_dimension - 1) * covariant_basis(0,mesh_dimension - 1) +
										 covariant_basis(1,mesh_dimension - 1) * covariant_basis(1,mesh_dimension - 1) +
										 covariant_basis(2,mesh_dimension - 1) * covariant_basis(2,mesh_dimension - 1));
						covariant_basis(0,mesh_dimension - 1) /= norm;
						covariant_basis(1,mesh_dimension - 1) /= norm;
						covariant_basis(2,mesh_dimension - 1) /= norm;
						break;
				}

				covariant_metric_tensor.fill(0.);

				xt::noalias(transposed_covariant_basis) = xt::transpose(covariant_basis);

				xt::noalias(covariant_metric_tensor) = xt::linalg::dot(transposed_covariant_basis, covariant_basis);

				double determinant = 0;

				Wrappers::Xtensor::ComputeInverseSquareMatrix(covariant_metric_tensor, contravariant_metric_tensor, determinant);

				for (auto i = 0u; i <Ncomponent ; ++i)
				{
					for (auto j = 0u; j <Ncomponent ; ++j)
					{
						reduced_contravariant_metric_tensor(i, j) = contravariant_metric_tensor(i, j);
					}
				}

				contravariant_basis.fill(0.);

				xt::noalias(contravariant_basis) = xt::linalg::dot(covariant_basis, contravariant_metric_tensor);
			}


		} // namespace LocalVariationalOperatorNS


	} // namespace Advanced


} // namespace MoReFEM


	/// @} // addtogroup OperatorInstancesGroup
