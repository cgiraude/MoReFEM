/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 15:37:44 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "ParameterInstances/GradientBasedElasticityTensor/GradientBasedElasticityTensor.hpp"
#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Configuration.hpp"
#include "Parameters/Internal/Alias.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Internal/GradOnGradientBasedElasticityTensor.hpp"


namespace MoReFEM
{
    
    
    namespace Internal
    {
    
    
        namespace LocalVariationalOperatorNS
        {
            
            
            Parameter<ParameterNS::Type::matrix, LocalCoords, ::MoReFEM::ParameterNS::TimeDependencyNS::None>::unique_ptr
                InitGradientBasedElasticityTensor(const ScalarParameter<>& young_modulus,
                                                  const ScalarParameter<>& poisson_ratio,
                                                  const ParameterNS::GradientBasedElasticityTensorConfiguration configuration)
                {
                    using Configuration = ParameterNS::GradientBasedElasticityTensorConfiguration;
                    
                    switch (configuration)
                    {
                        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim1:
                        {
                            using parameter_type = ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<Configuration::dim1>;
                            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
                        }
                        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain:
                        {
                            using parameter_type = ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<Configuration::dim2_plane_strain>;
                            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
                        }
                        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress:
                        {
                            using parameter_type = ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<Configuration::dim2_plane_stress>;
                            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
                        }
                        case ParameterNS::GradientBasedElasticityTensorConfiguration::dim3:
                        {
                            using parameter_type = ::MoReFEM::ParameterNS::GradientBasedElasticityTensor<Configuration::dim3>;
                            return std::make_unique<parameter_type>(young_modulus, poisson_ratio);
                        }
                    }
                    
                    assert(false && "All configuration cases should have been dealt with above.");
                    exit(EXIT_FAILURE);
                    
                }

            
        } // namespace LocalVariationalOperatorNS
            
        
    } // namespace Internal
  

} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
