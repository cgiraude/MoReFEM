/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 26 Nov 2015 15:21:54 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_ALE_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_ALE_HXX_


namespace MoReFEM
{


    namespace GlobalVariationalOperatorNS
    {


        template<class LinearAlgebraTupleT>
        inline void Ale
        ::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                   const GlobalVector& input_vector,
                   const Domain& domain) const
        {
            return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
        }


        template<class LocalOperatorTypeT>
        inline void
        Ale::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         LocalOperatorTypeT& local_operator,
                                         const std::tuple<const GlobalVector&>& additional_arguments) const
        {
            ExtractLocalDofValues(local_felt_space,
                                  parent::GetNthUnknown(),
                                  std::get<0>(additional_arguments),
                                  local_operator.GetNonCstFormerLocalVelocity());
        }


    } // namespace GlobalVariationalOperatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_BILINEAR_FORM_x_ALE_HXX_
