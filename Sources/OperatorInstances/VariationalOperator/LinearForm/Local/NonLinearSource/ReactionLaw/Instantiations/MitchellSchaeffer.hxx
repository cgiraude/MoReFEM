/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 30 Jun 2015 17:06:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HXX_


# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"


namespace MoReFEM
{


    namespace Advanced
    {


    namespace ReactionLawNS
    {


        template <class InputDataT>
        ReactionLaw<ReactionLawName::MitchellSchaeffer>::ReactionLaw(const InputDataT& input_data,
                                                                     const Domain& domain,
                                                                     const TimeManager& time_manager,
                                                                     const QuadratureRulePerTopology& default_quadrature_rule_set)
        : tau_in_(Utilities::InputDataNS::Extract<input_param_ms::TauIn>::Value(input_data)),
        tau_out_(Utilities::InputDataNS::Extract<input_param_ms::TauOut>::Value(input_data)),
        tau_open_(Utilities::InputDataNS::Extract<input_param_ms::TauOpen>::Value(input_data)),
        u_gate_(Utilities::InputDataNS::Extract<input_param_ms::PotentialGate>::Value(input_data)),
        u_min_(Utilities::InputDataNS::Extract<input_param_ms::PotentialMin>::Value(input_data)),
        u_max_(Utilities::InputDataNS::Extract<input_param_ms::PotentialMax>::Value(input_data)),
        time_manager_(time_manager)
        {
            using InitialConditionGate = ::MoReFEM::InputDataNS::InitialConditionGate;

            namespace ipl = Utilities::InputDataNS;

            const double initial_condition_gate = ipl::Extract<InitialConditionGate::Value>::Value(input_data);

            gate_ = std::make_unique<ScalarParameterAtQuadPt>("Gate",
                                                              domain,
                                                              default_quadrature_rule_set,
                                                              initial_condition_gate,
                                                              this->GetTimeManager());

            tau_close_ = InitScalarParameterFromInputData<input_param_ms::TauClose>("Tau Close",
                                                                       domain,
                                                                       input_data);
        }


        inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetLocalPotential() const noexcept
        {
            return local_potential_;
        }


        inline double& ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetNonCstLocalPotential() noexcept
        {
            return local_potential_;
        }


        inline const TimeManager& ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTimeManager() const noexcept
        {
            return time_manager_;
        }


        inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTauIn() const noexcept
        {
            return tau_in_;
        }

        inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTauOut() const noexcept
        {
            return tau_out_;
        }


        inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTauOpen() const noexcept
        {
            return tau_open_;
        }


        inline const ReactionLaw<ReactionLawName::MitchellSchaeffer>::scalar_parameter&
        ReactionLaw<ReactionLawName::MitchellSchaeffer>
        ::GetTauClose() const
        {
            assert(!(!tau_close_));
            return *tau_close_;
        }


        inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetUGate() const noexcept
        {
            return u_gate_;
        }


        inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetUMin() const noexcept
        {
            return u_min_;
        }


        inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetUMax() const noexcept
        {
            return u_max_;
        }

        inline ReactionLaw<ReactionLawName::MitchellSchaeffer>::ScalarParameterAtQuadPt&
        ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetNonCstGate() noexcept
        {
            return const_cast<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>&>(this->GetGate());
        }


        inline const ReactionLaw<ReactionLawName::MitchellSchaeffer>::ScalarParameterAtQuadPt&
        ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetGate() const noexcept
        {
            assert(!(!gate_));
            return *gate_;
        }


    } // namespace ReactionLawNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HXX_
