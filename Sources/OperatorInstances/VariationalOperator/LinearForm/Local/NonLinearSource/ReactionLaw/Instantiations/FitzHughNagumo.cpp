/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 30 Jun 2015 17:06:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


# include "Utilities/Numeric/Numeric.hpp"


#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/FitzHughNagumo.hpp"



namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
    namespace ReactionLawNS
    {
        
        
        const std::string& ReactionLaw<ReactionLawName::FitzHughNagumo>::ClassName()
        {
            static std::string name("FitzHughNagumo");
            return name;
        }
    
        
        void ReactionLaw<ReactionLawName::FitzHughNagumo>::GateLawFunction(const QuadraturePoint& quad_pt,
                                                                           const GeometricElt& geom_elt,
                                                                           double& gate) const
        {
            static_cast<void>(quad_pt); // avoid warning in release mode.
            static_cast<void>(geom_elt); // avoid warning in release mode.
            
            const auto& local_potential = GetLocalPotential();
            gate += GetTimeManager().GetTimeStep() * GetA() * (local_potential + GetB() - GetC() * gate);
        }
        
        
        double ReactionLaw<ReactionLawName::FitzHughNagumo>::ReactionLawFunction(const double local_potential,
                                                                                 const QuadraturePoint& quad_pt,
                                                                                 const GeometricElt& geom_elt)
        {
            auto functor = [&quad_pt, &geom_elt, this](double& gate)
            {
                return GateLawFunction(quad_pt, geom_elt, gate);
            };
            
            const double new_gate = GetNonCstGate().UpdateAndGetValue(quad_pt,
                                                                      geom_elt,
                                                                      functor);
            
            return local_potential - NumericNS::Cube(local_potential)/3. - new_gate;
        }
        
        void ReactionLaw<ReactionLawName::FitzHughNagumo>::WriteGate(const std::string& filename) const
        {
            GetGate().Write(filename);
        }
        
        
    } // namespace ReactionLawNS
        
        
    } // namespace Advanced
  

} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup
