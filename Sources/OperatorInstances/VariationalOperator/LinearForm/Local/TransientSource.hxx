/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 3 Feb 2015 14:25:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT
            >
            TransientSource<TypeT, TimeDependencyT>
            ::TransientSource(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                              const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                              typename parent::elementary_data_type&& a_elementary_data,
                              const parameter_type& source)
            : parent(unknown_list, test_unknown_list, std::move(a_elementary_data)),
            source_(source)
            {
                assert(unknown_list.size() == 1);
                assert(test_unknown_list.size() == 1);
            }


            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT
            >
            const std::string& TransientSource<TypeT, TimeDependencyT>::ClassName()
            {
                static std::string name("TransientSource");
                return name;
            }



            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT
            >
            void TransientSource<TypeT, TimeDependencyT>::ComputeEltArray()
            {
                auto& elementary_data = GetNonCstElementaryData();

                const auto& infos_at_quad_pt_list =
                    elementary_data.GetInformationsAtQuadraturePointList();

                auto& vector_result = elementary_data.GetNonCstVectorResult();

                const auto& current_geom_elt = elementary_data.GetCurrentGeomElt();

                const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown());

                const auto Ncomponent = test_ref_felt.Ncomponent();

                # ifndef NDEBUG
                {
                    if (TypeT == ParameterNS::Type::scalar)
                        assert(Ncomponent == 1);
                    else if (TypeT == ParameterNS::Type::vector)
                        assert(static_cast<unsigned int>(Ncomponent) == elementary_data.GetMeshDimension());
                }
                # endif // NDEBUG

                const auto Nnode_test = static_cast<int>(test_ref_felt.Nnode());

                for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
                {
                    decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();

                    const auto& phi_test = test_unknown_data.GetRefFEltPhi();

                    const auto& quad_pt = test_unknown_data.GetQuadraturePoint();

                    const double quad_pt_factor = quad_pt.GetWeight()
                    * test_unknown_data.GetAbsoluteValueJacobianDeterminant();

                    const auto& force_value = GetSource().GetValue(quad_pt, current_geom_elt);

                    for (auto node_index = 0; node_index < Nnode_test; ++node_index)
                    {
                        auto dof_index = node_index;

                        const auto factor = quad_pt_factor * phi_test(node_index);

                        for (auto component = 0ul; component < Ncomponent; ++component, dof_index += Nnode_test)
                        {
                            if constexpr(TypeT == ParameterNS::Type::scalar)
                                vector_result(dof_index) += factor * force_value;
                            else if constexpr(TypeT == ParameterNS::Type::vector)
                                vector_result(dof_index) += factor * force_value(component);
                        }
                    }
                }
            }


            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT
            >
            inline const typename TransientSource<TypeT, TimeDependencyT>::parameter_type& TransientSource<TypeT, TimeDependencyT>
            ::GetSource() const
            {
                return source_;
            }


            template
            <
                ParameterNS::Type TypeT,
                template<ParameterNS::Type> class TimeDependencyT
            >
            inline void TransientSource<TypeT, TimeDependencyT>::SetTime(double time) noexcept
            {
                time_ = time;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_TRANSIENT_SOURCE_HXX_
