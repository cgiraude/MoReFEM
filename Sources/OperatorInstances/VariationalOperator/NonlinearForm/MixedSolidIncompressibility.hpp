/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 May 2014 16:03:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_MIXED_SOLID_INCOMPRESSIBILITY_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_MIXED_SOLID_INCOMPRESSIBILITY_HPP_

# include "Utilities/Type/StrongType.hpp"

# include "Parameters/ParameterType.hpp"

# include "Parameters/Parameter.hpp"
# include "Parameters/ParameterAtQuadraturePoint.hpp"

# include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"
# include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"

# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/MixedSolidIncompressibility.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief MixedSolidIncompressibility description.
     *
     * \todo #9 Describe operator!
     */
    template <class HydrostaticLawPolicyT>
    class MixedSolidIncompressibility
    : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
    <
        MixedSolidIncompressibility<HydrostaticLawPolicyT>,
        Advanced::OperatorNS::Nature::nonlinear,
        typename Advanced::LocalVariationalOperatorNS::MixedSolidIncompressibility<HydrostaticLawPolicyT>
    >
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = MixedSolidIncompressibility<HydrostaticLawPolicyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Const Unique ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_variational_operator_type =
        Advanced::LocalVariationalOperatorNS::MixedSolidIncompressibility<HydrostaticLawPolicyT>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            self,
            Advanced::OperatorNS::Nature::nonlinear,
            local_variational_operator_type
        >;

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Alias to CauchyGreenTensor.
        using cauchy_green_tensor_type =
        ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>;
        
        //! Strong type for displacement global vectors.
        using DisplacementGlobalVector =
        StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_gvo_felt_space_arg
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         * \param[in] unknown_list Container with vectorial then scalar unknown.
         * \copydoc doxygen_hide_test_unknown_list_param
         * \param[in] cauchy_green_tensor Cauchy-Green tensor used in the SecondPiolaKirchoffStressTensor operator.
         * \param[in] hydrostatic_law Policy concerning hydrostatic law.
         */
        explicit MixedSolidIncompressibility(const FEltSpace& felt_space,
                                             const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                             const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                             const cauchy_green_tensor_type& cauchy_green_tensor,
                                             const HydrostaticLawPolicyT* hydrostatic_law,
                                             const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Destructor.
        ~MixedSolidIncompressibility() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MixedSolidIncompressibility(const MixedSolidIncompressibility& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MixedSolidIncompressibility(MixedSolidIncompressibility&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MixedSolidIncompressibility& operator=(const MixedSolidIncompressibility& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MixedSolidIncompressibility& operator=(MixedSolidIncompressibility&& rhs) = delete;

        ///@}


    public:

        /*!
         * \brief Assemble into one or several matrices and/or vectors.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient and/or
         * \a GlobalVectorWithCoefficient objects. Ordering doesn't matter.
         *
         * \param[in] linear_algebra_tuple List of global matrices and/or vectors into which the operator is
         * assembled. These objects are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         * \param[in] previous_iteration_data Value from previous time iteration.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                      ConstRefDisplacementGlobalVector previous_iteration_data,
                      const Domain& domain = Domain()) const;


    private:


        /*!
         * \brief Computes the additional arguments required by the AssembleWithVariadicArguments() method as
         * a tuple.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         * \param[in] additional_arguments Additional arguments given to PerformElementaryCalculation().
         * These arguments might need treatment before being given to ComputeEltArray: for instance if there
         * is a GlobalVector that reflects a previous state, ComputeEltArray needs only the dofs that are
         * relevant locally.
         */
        template<class LocalOperatorTypeT>
        void
        SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                    LocalOperatorTypeT& local_operator,
                                    const std::tuple<ConstRefDisplacementGlobalVector>& additional_arguments) const;


    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/MixedSolidIncompressibility.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_MIXED_SOLID_INCOMPRESSIBILITY_HPP_
