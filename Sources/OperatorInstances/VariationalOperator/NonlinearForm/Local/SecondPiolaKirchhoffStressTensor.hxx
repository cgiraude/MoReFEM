/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 May 2014 11:40:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        const std::string& SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>::ClassName()
        {
            static std::string name("SecondPiolaKirchhoffStressTensor");
            return name;
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::~SecondPiolaKirchhoffStressTensor() = default;


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::SecondPiolaKirchhoffStressTensor(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                           const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                           elementary_data_type&& a_elementary_data,
                                           const Solid& solid,
                                           const TimeManager& time_manager,
                                           const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
                                           input_internal_variable_policy_type* input_internal_variable_policy)
        : HyperelasticityPolicyT(a_elementary_data.GetMeshDimension(), hyperelastic_law),
        ViscoelasticityPolicyT(a_elementary_data.GetMeshDimension(),
                               a_elementary_data.NdofRow(),
                               a_elementary_data.GetGeomEltDimension(),
                               solid,
                               time_manager),
        InternalVariablePolicyT(a_elementary_data.GetMeshDimension(),
                            a_elementary_data.NnodeRow(),
                            a_elementary_data.NquadraturePoint(),
                            time_manager,
                            input_internal_variable_policy),
        parent(unknown_list, test_unknown_list, std::move(a_elementary_data)),
        matrix_parent(),
        vector_parent()
        {
            const auto& elementary_data = parent::GetElementaryData();
            const unsigned int Ncomponent = elementary_data.GetGeomEltDimension();
            const unsigned int square_Ncomponent = NumericNS::Square(Ncomponent);

            const auto mesh_dimension = elementary_data.GetMeshDimension();

            const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
            const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
            const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

            const auto felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

            unsigned int dIndC_size(0u);

            switch(mesh_dimension)
            {
                case 1:
                    dIndC_size = 1u;
                    break;
                case 2:
                    dIndC_size = 3u;
                    break;
                case 3:
                    dIndC_size = 6u;
                    break;
                default:
                    assert(false);
                    break;
            }

            deriv_green_lagrange_ =
                std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(mesh_dimension);

            const auto square_mesh_dimension = NumericNS::Square(mesh_dimension);

            this->matrix_parent::InitLocalMatrixStorage
            ({{
                { square_Ncomponent, square_Ncomponent },  // tangent_matrix
                { square_Ncomponent, square_Ncomponent },  // linear_part
                { Nnode_for_test_unknown, Ncomponent }, // dPhi_test_mult_gradient_based_block
                { Nnode_for_test_unknown, Nnode_for_unknown }, // block_contribution
                { felt_space_dimension, Nnode_for_unknown }, // transposed dPhi
                { dIndC_size, dIndC_size }, // d2W,
                { square_mesh_dimension, dIndC_size }, // linear_part_intermediate_matrix
                { mesh_dimension, mesh_dimension }, // gradient-based block
                { mesh_dimension, mesh_dimension }, // gradient displacement
                { mesh_dimension, mesh_dimension } // deformation gradient
            }});


            this->vector_parent::InitLocalVectorStorage
            ({{
                dIndC_size, // dW
                square_mesh_dimension // rhs_part
            }});

            former_local_displacement_.resize(elementary_data.NdofCol());
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::ComputeEltArray()
        {
            auto& elementary_data = parent::GetNonCstElementaryData();

            const auto& local_displacement = GetFormerLocalDisplacement();

            auto& tangent_matrix = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
            const auto& rhs_part = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();

            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            const auto mesh_dimension = elementary_data.GetMeshDimension();

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                tangent_matrix.fill(0.);

                PrepareInternalDataForQuadraturePoint(infos_at_quad_pt.GetUnknownData(),
                                                      geom_elt,
                                                      elementary_data.GetRefFElt(this->GetNthUnknown(0)),
                                                      local_displacement,
                                                      mesh_dimension);

                ComputeAtQuadraturePoint(infos_at_quad_pt,
                                         tangent_matrix,
                                         rhs_part,
                                         elementary_data);
            }

            const auto& internal_variable_ptr = static_cast<InternalVariablePolicyT*>(this);
            auto& internal_variable = *internal_variable_ptr;

            using dispatcher
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::CorrectRHSWithActiveSchurComplement<InternalVariablePolicyT>;

            dispatcher::Perform(internal_variable,
                                elementary_data.GetNonCstVectorResult());
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::UpdateInternalVariableInternalVariables()
        {
            auto& elementary_data = parent::GetNonCstElementaryData();
            const auto& infos_at_quad_pt_list = elementary_data.GetInformationsAtQuadraturePointList();

            const auto& internal_variable_ptr = static_cast<InternalVariablePolicyT*>(this);
            auto& internal_variable = *internal_variable_ptr;

            const auto& geom_elt = elementary_data.GetCurrentGeomElt();

            for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
            {
                auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

                internal_variable.UpdateInternalVariables(quad_pt,
                                                      geom_elt,
                                                      infos_at_quad_pt.GetUnknownData().GetRefFEltPhi(),
                                                      internal_variable.GetFiberDeformationAtCurrentTimeIteration().GetValue(quad_pt, geom_elt),
                                                      internal_variable.GetFiberDeformationAtPreviousTimeIteration().GetValue(quad_pt, geom_elt));
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::ComputeAtQuadraturePoint(const InformationsAtQuadraturePoint& infos_at_quad_pt,
                                   const LocalMatrix& tangent_matrix,
                                   const LocalVector& rhs_part,
                                   elementary_data_type& elementary_data) const
        {
            const auto Ncomponent = elementary_data.GetGeomEltDimension();

            decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& dphi = quad_pt_unknown_list_data.GetGradientFEltPhi();
            const auto& dphi_test = test_unknown_data.GetGradientFEltPhi();

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const auto weight_meas = quad_pt.GetWeight() * quad_pt_unknown_list_data.GetAbsoluteValueJacobianDeterminant();

            const auto& unknown_ref_felt = elementary_data.GetRefFElt(this->GetNthUnknown(0));
            const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

            const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(this->GetNthTestUnknown(0));
            const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

            if (parent::DoAssembleIntoMatrix())
            {
                auto& gradient_based_block = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();
                auto& transposed_dphi = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
                xt::noalias(transposed_dphi) = xt::transpose(dphi);

                // Matrix related calculation.
                auto& matrix_result = elementary_data.GetNonCstMatrixResult();

                // LocalMatrix dPhi_mult_gradient_based_block(dPhi.shape(0), static_cast<int>(Ncomponent));
                auto& dphi_test_mult_gradient_based_block =
                    this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::dphi_test_mult_gradient_based_block)>();

                auto& block_contribution =
                    this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();

                for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
                {
                    const auto row_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);

                    for (unsigned int col_component = 0u; col_component < Ncomponent; ++col_component)
                    {
                        const auto col_first_index = unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                        Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(tangent_matrix,
                                                                                        row_component,
                                                                                        col_component,
                                                                                        gradient_based_block);

                        xt::noalias(dphi_test_mult_gradient_based_block) =
                            xt::linalg::dot(dphi_test, gradient_based_block);

                        xt::noalias(block_contribution) =
                            weight_meas * xt::linalg::dot(dphi_test_mult_gradient_based_block, transposed_dphi);

                        for (auto row_node = 0ul; row_node < Nnode_for_test_unknown; ++row_node)
                        {
                            for (auto col_node = 0ul; col_node < Nnode_for_unknown; ++col_node)
                                matrix_result(row_first_index + row_node, col_first_index + col_node)
                                += block_contribution(row_node, col_node);
                        }
                    }
                }
            }

            if (parent::DoAssembleIntoVector())
            {
                // Vector related calculation.
                auto& vector_result = elementary_data.GetNonCstVectorResult();

                for (unsigned int row_component = 0u; row_component < Ncomponent; ++row_component)
                {
                    const auto dof_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);
                    const auto component_first_index = row_component * Ncomponent;

                    // Compute the new contribution to vector_result here.
                    // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
                    for (auto row_node = 0ul; row_node < Nnode_for_test_unknown; ++row_node)
                    {
                        double value = 0.;

                        for (auto col = 0ul; col < Ncomponent; ++col)
                           value += dphi_test(row_node, col) * rhs_part(col + component_first_index);

                        vector_result(dof_first_index + row_node) += value * weight_meas;
                    }
                }
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        template<unsigned int DimensionT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::ComputeWDerivates(const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                            const GeometricElt& geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const LocalMatrix& gradient_displacement,
                            const LocalMatrix& De,
                            const LocalMatrix& transposed_De,
                            LocalVector& dW,
                            LocalMatrix& d2W)
        {
            static_cast<void>(gradient_displacement);

            const auto& hyperelasticity_ptr = static_cast<HyperelasticityPolicyT*>(this);
            auto& hyperelasticity = *hyperelasticity_ptr;

            decltype(auto) cauchy_green_tensor = GetCauchyGreenTensor();
            decltype(auto) quad_pt = quad_pt_unknown_list_data.GetQuadraturePoint();
            decltype(auto) cauchy_green_tensor_value = cauchy_green_tensor.GetValue(quad_pt, geom_elt);

            using dispatcher
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::ComputeWDerivatesHyperelasticity<DimensionT, HyperelasticityPolicyT>;

            dispatcher::Perform(quad_pt,
                                geom_elt,
                                ref_felt,
                                cauchy_green_tensor_value,
                                hyperelasticity,
                                dW,
                                d2W);

            const auto& internal_variable_ptr = static_cast<InternalVariablePolicyT*>(this);
            auto& internal_variable = *internal_variable_ptr;

            using dispatcher2
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::ComputeWDerivatesInternalVariable<DimensionT, InternalVariablePolicyT>;

            dispatcher2::Perform(quad_pt_unknown_list_data,
                                 geom_elt,
                                 ref_felt,
                                 cauchy_green_tensor_value,
                                 transposed_De,
                                 internal_variable,
                                 dW,
                                 d2W);

            
            const auto& viscoelasticity_ptr = static_cast<ViscoelasticityPolicyT*>(this);
            auto& viscoelasticity = *viscoelasticity_ptr;

            using dispatcher3
                = typename Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
            ::ComputeWDerivatesViscoelasticity<DimensionT, ViscoelasticityPolicyT>;

            dispatcher3::Perform(quad_pt_unknown_list_data,
                                 geom_elt,
                                 ref_felt,
                                 De,
                                 transposed_De,
                                 viscoelasticity,
                                 dW,
                                 d2W);
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::PrepareInternalDataForQuadraturePoint(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                                                const GeometricElt& geom_elt,
                                                const Advanced::RefFEltInLocalOperator& ref_felt,
                                                const std::vector<double>& local_displacement,
                                                const unsigned int mesh_dimension)
        {
            switch(mesh_dimension)
            {
                case 1:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<1>(quad_pt_unknown_list_data,
                                                                               geom_elt,
                                                                               ref_felt,
                                                                               local_displacement);
                    break;
                }
                case 2:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<2>(quad_pt_unknown_list_data,
                                                                               geom_elt,
                                                                               ref_felt,
                                                                               local_displacement);
                    break;
                }
                case 3:
                {
                    this->PrepareInternalDataForQuadraturePointForDimension<3>(quad_pt_unknown_list_data,
                                                                               geom_elt,
                                                                               ref_felt,
                                                                               local_displacement);
                    break;
                }
                default:
                    assert(false);
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        inline const std::vector<double>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::GetFormerLocalDisplacement() const noexcept
        {
            return former_local_displacement_;
        }



        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::GetNonCstDerivativeGreenLagrange() noexcept
        {
            assert(!(!deriv_green_lagrange_));
            return *deriv_green_lagrange_;
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        inline std::vector<double>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::GetNonCstFormerLocalDisplacement() noexcept
        {
            return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        template<unsigned int DimensionT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::PrepareInternalDataForQuadraturePointForDimension(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                                                            const GeometricElt& geom_elt,
                                                            const Advanced::RefFEltInLocalOperator& ref_felt,
                                                            const std::vector<double>& local_displacement)
        {
            auto& gradient_displacement =
                this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_displacement)>();

            Advanced::OperatorNS::ComputeGradientDisplacementMatrix(quad_pt_unknown_list_data,
                                                                    ref_felt,
                                                                    local_displacement,
                                                                    gradient_displacement);

            // ========================================================================================================
            // Compute first the invariants for current quadrature point.
            // For this purpose, Cauchy-Green tensor and De matrix (see P22 on Philippe's note) must be computed first.
            // ========================================================================================================

            auto& derivative_green_lagrange = GetNonCstDerivativeGreenLagrange();
            const auto& De = derivative_green_lagrange.Update(gradient_displacement);
            const auto& transposed_De = derivative_green_lagrange.GetTransposed();

			decltype(auto) check_inverted =
				MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements::GetInstance(__FILE__, __LINE__);

            switch (check_inverted.DoCheckInvertedElements())
            {
                case check_inverted_elements_policy::do_check:
                {
                    auto& deformation_gradient =
						this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deformation_gradient)>();
                    deformation_gradient = gradient_displacement;
                    
                    for (auto i = 0ul; i < DimensionT; ++i)
                         deformation_gradient(i, i) += 1.0; // F = Id + grad_y
                    
                    const double determinant_deformation_gradient = xt::linalg::det(deformation_gradient);
                    
                    if (determinant_deformation_gradient <= 0.)
                        throw Exception("Some mesh elements have been inverted. "
                                        "Consider using an adaptative loading method "
                                        "(i.e continuation method for static cases or smaller "
                                        "time steps for dynamic ones).",
                                        __FILE__, __LINE__);
                    break;
                }
                case check_inverted_elements_policy::no_check:
                    break; // Maybe free the memory allocated for the local matrix deformation gradient?
            
                case check_inverted_elements_policy::from_input_data:
				case check_inverted_elements_policy::unspecified:
                    assert(false && "Should not happen");
                    exit(EXIT_FAILURE);
            } // switch

			// ===================================================================================
            // Then compute the derivates of W, required to build both bilinear and linear terms.
            // ===================================================================================

            auto& dW = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dW)>();
            auto& d2W = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W)>();

            // Put to zero as in each ComputeWDerivates of the policies once should add the contributions and use Add.
            dW.fill(0.);
            d2W.fill(0.);

            ComputeWDerivates<DimensionT>(quad_pt_unknown_list_data,
                                          geom_elt,
                                          ref_felt,
                                          gradient_displacement,
                                          De,
                                          transposed_De,
                                          dW,
                                          d2W);

            // ===================================================================================
            // Finally build the terms that are actually required.
            // ===================================================================================

            // Linear term.
            if (parent::DoAssembleIntoVector())
            {
                auto& rhs_part = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_part)>();
                xt::noalias(rhs_part) = xt::linalg::dot(transposed_De, dW);
            }

            // Bilinear terms. There are in fact both: one for linear part and another for non-linear one.
            if (parent::DoAssembleIntoMatrix())
            {
                auto& tangent_matrix = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix)>();
                auto& linear_part_intermediate_matrix = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part_intermediate_matrix)>();
                auto& linear_part = this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part)>();

                Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
                ::ComputeLinearPart(De,
                                   transposed_De,
                                   d2W,
                                   linear_part_intermediate_matrix,
                                   // < internal quantity; better design
                                   // would be to encapsulate it in
                                   // an Internal class but I have no time
                                   // to do this minor fix now.
                                   linear_part);

                Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ComputeNonLinearPart<DimensionT>(dW, tangent_matrix); // tangent_matrix not complete here; linear_part
                // must be added for that (done immediately after this function call...)

                const auto& viscoelasticity_ptr = static_cast<ViscoelasticityPolicyT*>(this);
                auto& viscoelasticity = *viscoelasticity_ptr;

                Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
                ::AddTangentMatrixViscoelasticity<ViscoelasticityPolicyT>
                ::Perform(tangent_matrix, viscoelasticity);

                // Complete the tangent matrix with the linear part!
                xt::noalias(tangent_matrix) += linear_part;
            }
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        void SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::SetCauchyGreenTensor(const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>* param)
        {
            assert(cauchy_green_tensor_ == nullptr && "Should be called only once.");
            cauchy_green_tensor_ = param;
        }


        template <class HyperelasticityPolicyT, class ViscoelasticityPolicyT, class InternalVariablePolicyT>
        inline const ParameterAtQuadraturePoint<ParameterNS::Type::vector, ParameterNS::TimeDependencyNS::None>&
        SecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT, ViscoelasticityPolicyT, InternalVariablePolicyT>
        ::GetCauchyGreenTensor() const noexcept
        {
            assert(!(!cauchy_green_tensor_));
            return *cauchy_green_tensor_;
        }


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
