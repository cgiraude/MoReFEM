/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Jan 2016 15:10:18 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            namespace SecondPiolaKirchhoffStressTensorNS
            {


                template <unsigned int DimensionT, class HyperelasticityPolicyT>
                void ComputeWDerivatesHyperelasticity<DimensionT, HyperelasticityPolicyT>
                ::Perform(const QuadraturePoint& quad_pt,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ref_felt,
                          const LocalVector& cauchy_green_tensor_value,
                          HyperelasticityPolicyT& hyperelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    hyperelasticity.ComputeWDerivates(quad_pt,
                                                      geom_elt,
                                                      ref_felt,
                                                      cauchy_green_tensor_value,
                                                      dW,
                                                      d2W);
                }


                template <unsigned int DimensionT>
                void ComputeWDerivatesHyperelasticity<DimensionT, SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS::None>
                ::Perform(const QuadraturePoint& quad_pt,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ,
                          const LocalVector& cauchy_green_tensor_value,
                          SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS::None& hyperelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    static_cast<void>(quad_pt);
                    static_cast<void>(geom_elt);
                    static_cast<void>(cauchy_green_tensor_value);
                    static_cast<void>(hyperelasticity);
                    static_cast<void>(dW);
                    static_cast<void>(d2W);
                }


                template <unsigned int DimensionT, class InternalVariablePolicyT>
                void ComputeWDerivatesInternalVariable<DimensionT, InternalVariablePolicyT>
                ::Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ref_felt,
                          const LocalVector& cauchy_green_tensor_value,
                          const LocalMatrix& transposed_De,
                          InternalVariablePolicyT& internal_variable,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    internal_variable.template ComputeWDerivates<DimensionT>(quad_pt_unknown_list_data,
                                                                         geom_elt,
                                                                         ref_felt,
                                                                         cauchy_green_tensor_value,
                                                                         transposed_De,
                                                                         dW,
                                                                         d2W);
                }


                template <unsigned int DimensionT>
                void ComputeWDerivatesInternalVariable<DimensionT, SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None>
                ::Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ref_felt,
                          const LocalVector& cauchy_green_tensor_value,
                          const LocalMatrix& transposed_De,
                          SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None& internal_variable,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    static_cast<void>(quad_pt_unknown_list_data);
                    static_cast<void>(geom_elt);
                    static_cast<void>(ref_felt);
                    static_cast<void>(cauchy_green_tensor_value);
                    static_cast<void>(transposed_De);
                    static_cast<void>(internal_variable);
                    static_cast<void>(dW);
                    static_cast<void>(d2W);
                }


                template <class InternalVariablePolicyT>
                void CorrectRHSWithActiveSchurComplement<InternalVariablePolicyT>
                ::Perform(InternalVariablePolicyT& internal_variable,
                          LocalVector& rhs)
                {
                    internal_variable.CorrectRHSWithActiveSchurComplement(rhs);
                }


                template <unsigned int FiberIndexT>
                inline void CorrectRHSWithActiveSchurComplement<SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT>>
                ::Perform(SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT>& internal_variable,
                          LocalVector& rhs)
                {
                    static_cast<void>(rhs);
                    static_cast<void>(internal_variable);
                }

                
                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                inline void CorrectRHSWithActiveSchurComplement<SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::Microsphere<FiberIndexI4T, FiberIndexI6T>>
                ::Perform(SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::Microsphere<FiberIndexI4T, FiberIndexI6T>& internal_variable,
                          LocalVector& rhs)
                {
                    static_cast<void>(rhs);
                    static_cast<void>(internal_variable);
                }

                
                inline void CorrectRHSWithActiveSchurComplement<SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None>
                ::Perform(SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None& internal_variable,
                          LocalVector& rhs)
                {
                    static_cast<void>(rhs);
                    static_cast<void>(internal_variable);
                }


                template <unsigned int DimensionT, class ViscoelasticityPolicyT>
                void ComputeWDerivatesViscoelasticity<DimensionT, ViscoelasticityPolicyT>
                ::Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ref_felt,
                          const LocalMatrix& De,
                          const LocalMatrix& transposed_De,
                          ViscoelasticityPolicyT& viscoelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    viscoelasticity.template ComputeWDerivates<DimensionT>(quad_pt_unknown_list_data,
                                                                           geom_elt,
                                                                           ref_felt,
                                                                           De,
                                                                           transposed_De,
                                                                           dW,
                                                                           d2W);
                }


                template <unsigned int DimensionT>
                void ComputeWDerivatesViscoelasticity<DimensionT, SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None>
                ::Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                          const GeometricElt& geom_elt,
                          const Advanced::RefFEltInLocalOperator& ,
                          const LocalMatrix& De,
                          const LocalMatrix& transposed_De,
                          SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None& viscoelasticity,
                          LocalVector& dW,
                          LocalMatrix& d2W)
                {
                    static_cast<void>(quad_pt_unknown_list_data);
                    static_cast<void>(geom_elt);
                    static_cast<void>(viscoelasticity);
                    static_cast<void>(dW);
                    static_cast<void>(d2W);
                    static_cast<void>(De);
                    static_cast<void>(transposed_De);
                }


                template <class ViscoelasticityPolicyT>
                void AddTangentMatrixViscoelasticity<ViscoelasticityPolicyT>
                ::Perform(LocalMatrix& tangent_matrix,
                          ViscoelasticityPolicyT& viscoelasticity)
                {
                    const auto& tangent_matrix_visco = viscoelasticity.GetMatrixTangentVisco();

                    tangent_matrix += tangent_matrix_visco;
                }


                inline void AddTangentMatrixViscoelasticity<SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None>
                ::Perform(LocalMatrix& tangent_matrix,
                          ViscoelasticityPolicyNS::None& viscoelasticity)
                {
                    static_cast<void>(tangent_matrix);
                    static_cast<void>(viscoelasticity);
                }


            } // namespace SecondPiolaKirchhoffStressTensorNS


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_x_PARTIAL_SPECIALIZATION_HXX_
