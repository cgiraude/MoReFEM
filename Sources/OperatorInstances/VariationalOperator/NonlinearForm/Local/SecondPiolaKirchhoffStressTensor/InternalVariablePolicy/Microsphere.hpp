/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 16:35:18 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HPP_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HPP_

# include <memory>
# include <vector>
# include <array>

# include "Utilities/Containers/EnumClass.hpp"
# include "Utilities/Containers/Array.hpp"
# include "Utilities/Numeric/Numeric.hpp"
# include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

# include "Core/TimeManager/TimeManager.hpp"

# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/FiberListManager.hpp"
# include "Parameters/Parameter.hpp"
# include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
# include "Parameters/ParameterAtQuadraturePoint.hpp"

# include "Operators/LocalVariationalOperator/ElementaryData.hpp"

# include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"

namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace GlobalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                template<unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                class Microsphere;
                class InputMicrosphere;


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace GlobalVariationalOperatorNS


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                /*!
                 * \brief Policy to use when \a Microsphere is involved in \a SecondPiolaKirchhoffStressTensor.
                 *
                 * \tparam FiberIndexI4T Index of the fiber related to the 4th invariant in the \a InputData file.
                 *
                 * \tparam FiberIndexI6T Index of the fiber related to the 6th invariant in the \a InputData file.
                 *
                 */
                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                class Microsphere
                : public Crtp::LocalMatrixStorage<Microsphere<FiberIndexI4T, FiberIndexI6T>, 1ul>,
                public Crtp::LocalVectorStorage<Microsphere<FiberIndexI4T, FiberIndexI6T>, 3ul>

                {

                public:

                    //! \copydoc doxygen_hide_alias_self
                    using self = Microsphere<FiberIndexI4T, FiberIndexI6T>;

                    //! Alias to unique pointer.
                    using unique_ptr = std::unique_ptr<self>;

                    //! Alias to vector of unique pointers.
                    using vector_unique_ptr = std::vector<unique_ptr>;

                    //! Type of the elementary vector.
                    using vector_type = LocalVector;

                    //! Alias to the parent that provides LocalVectorStorage.
                    using vector_parent = Crtp::LocalVectorStorage<Microsphere<FiberIndexI4T, FiberIndexI6T>, 3ul>;

                    //! Type of the elementary matrix.
                    using matrix_type = LocalMatrix;

                    //! Alias to the parent that provides LocalMatrixStorage.
                    using matrix_parent = Crtp::LocalMatrixStorage<Microsphere<FiberIndexI4T, FiberIndexI6T>, 1ul>;

                    //! Friendship to the GlobalVariationalOperator.
                    friend ::MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
                                                                 ::Microsphere<FiberIndexI4T, FiberIndexI6T>;

                    //! Alias to a scalar parameter at quadrature point.
                    using ScalarParameterAtQuadPt = ParameterAtQuadraturePoint<ParameterNS::Type::scalar>;

                    //! Alias to the type of the input of the policy.
                    using input_internal_variable_policy_type =
                    ::MoReFEM::InputMicrosphere;

            public:

                    /// \name Special members.
                    ///@{

                    /*!
                     * \brief Constructor.
                     *
                     * \param[in] mesh_dimension Dimension of the mesh.
                     * \param[in] Nnode_unknown Number of nodes.
                     * \param[in] Nquad_point Nunmber of quadrature point.
                     * \param[in] time_manager Object that keeps track of the time within the Model.
                     * \param[in] input_microsphere \a InputMicrosphere object.
                     */
                    explicit Microsphere(const unsigned int mesh_dimension,
                                                 const unsigned int Nnode_unknown,
                                                 const unsigned int Nquad_point,
                                                 const TimeManager& time_manager,
                                                 InputMicrosphere* input_microsphere);

                    //! Destructor.
                    ~Microsphere() = default;

                    //! \copydoc doxygen_hide_copy_constructor
                    Microsphere(const Microsphere& rhs) = delete;

                    //! \copydoc doxygen_hide_move_constructor
                    Microsphere(Microsphere&& rhs) = delete;

                    //! \copydoc doxygen_hide_copy_affectation
                    Microsphere& operator=(const Microsphere& rhs) = delete;

                    //! \copydoc doxygen_hide_move_affectation
                    Microsphere& operator=(Microsphere&& rhs) = delete;

                    ///@}

                public:

                    /*!
                     * \class doxygen_hide_second_piola_compute_W_derivates_internal_variable
                     *
                     * \brief Compute the active stress and its derivatives.
                     *
                     * \copydoc doxygen_hide_dW_d2W_derivates_arg
                     * \copydoc doxygen_hide_quad_pt_unknown_list_data_arg
                     * \param[in] ref_felt Reference finite element considered.
                     * \param[in] geom_elt \a GeometricElt for which the computation takes place.
                     * \param[in] cauchy_green_tensor_value Value oif the CauchyGreen tensor at the current
                     * \a QuadraturePoint.
                     * \param[in] transposed_De Transposed De matrix.
                     */

                    //! \copydoc doxygen_hide_second_piola_compute_W_derivates_internal_variable
                    template<unsigned int DimensionT>
                    void ComputeWDerivates(const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                                           const GeometricElt& geom_elt,
                                           const Advanced::RefFEltInLocalOperator& ref_felt,
                                           const LocalVector& cauchy_green_tensor_value,
                                           const LocalMatrix& transposed_De,
                                           LocalVector& dW,
                                           LocalMatrix& d2W);

                private:

                    //! Constant accessor to \a TimeManager.
                    const TimeManager& GetTimeManager() const noexcept;

                    //! Constant accessor to the fibers related to I4.
                    const ::MoReFEM::FiberList<ParameterNS::Type::vector>& GetFibersI4() const noexcept;

                    //! Constant accessor to the fibers related to I6.
                    const ::MoReFEM::FiberList<ParameterNS::Type::vector>& GetFibersI6() const noexcept;

					//! Hardcoded quadrature points \a phi_i along the plane.
					const std::array<double, 20ul>& GetQuadraturePointsAlongPlane() const noexcept;

					//! Hardcoded quadrature points \a theta_i outside the plane.
					const std::array<double, 5ul>& GetQuadraturePointsOutsidePlane() const noexcept;


                private:

                    //! Contant accessor to the input of the policy.
                    const input_internal_variable_policy_type& GetInputMicrosphere() const noexcept;

                private:

                    //! Time manager.
                    const TimeManager& time_manager_;

                private:

                    //! Fibers parameter for I4.
                    const FiberList<ParameterNS::Type::vector>& fibers_I4_;

                    //! Fibers parameter for I6.
                    const FiberList<ParameterNS::Type::vector>& fibers_I6_;

                    //! Input of the policy.
                    input_internal_variable_policy_type& input_internal_variable_policy_;

                    //! Hardcoded quadrature points \a phi_i along the plane.
                    std::array<double, 20ul> lbdvp_;

                    //! Hardcoded quadrature points \a theta_i outside the plane.
                    std::array<double, 5ul> lbdvt_;


                private:

                    /// \name Useful indexes to fetch the work matrices and vectors.
                    ///@{

                    //! Convenient enum to alias vectors.
                    enum class LocalVectorIndex : std::size_t
                    {
                        tauxtau = 0,
                        tau_n = 1,
                        tau_global = 2
                    };

                    enum class LocalMatrixIndex : std::size_t
                    {
                        tauxtauxtauxtau = 0
                    };

                    ///@}


                };


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


# include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HPP_
