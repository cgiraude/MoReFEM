/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 16:35:18 +0100
// Copyright (c) Inria. All rights reserved.
//
//  Modified by Patrick Le Tallec to handle microsphere models
// Add microsphere contribution to free energy derivatives dW and d2W if  the local input fiber length are non zero.
// Only works in 3D.
// Data
// two family of fibers I4 and I6. For each family, need at node level
// Must also provide the integration rule on the sphere (number of points and angles) in radian respectively in angle phi and angle theta.
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HXX_
# define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HXX_


namespace MoReFEM
{


    namespace Advanced
    {


    namespace LocalVariationalOperatorNS
    {


        namespace SecondPiolaKirchhoffStressTensorNS
        {


            namespace InternalVariablePolicyNS
            {


                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                Microsphere<FiberIndexI4T, FiberIndexI6T>::Microsphere(const unsigned int mesh_dimension,
                                                                       const unsigned int Nnode_unknown,
                                                                       const unsigned int Nquad_point,
                                                                       const TimeManager& time_manager,
                                                                       input_internal_variable_policy_type* input_internal_variable_policy)
                : matrix_parent(),
                vector_parent(),
                time_manager_(time_manager),
                fibers_I4_(::MoReFEM::Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__).GetFiberList(FiberIndexI4T)),
                fibers_I6_(::MoReFEM::Internal::FiberNS::FiberListManager<ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__).GetFiberList(FiberIndexI6T)),
                input_internal_variable_policy_(*input_internal_variable_policy)
                {
                    static_cast<void>(Nquad_point);
                    static_cast<void>(Nnode_unknown);
                    
                    unsigned int tauxtau_init(0u);
                    unsigned int tau_n_init(0u);
                    unsigned int tau_global_init(0u);

                    switch (mesh_dimension)
                    // Only the 3D case is relevant for this operator.
                    {
                        case 3u:
                            tauxtau_init = 6u;
                            tau_n_init = 3u;
                            tau_global_init = 3u;
                            break;
                        default:
                            assert(false);
							exit(EXIT_FAILURE);
                            break;
                    } // switch

                    this->matrix_parent::InitLocalMatrixStorage
                    ({{
                        {tauxtau_init, tauxtau_init}
                    }});

                    this->vector_parent::InitLocalVectorStorage
                    ({{
                        tauxtau_init,
                        tau_n_init,
                        tau_global_init
                    }});

                    // Hardcoded quadrature rule on the unit sphere.
                    const double pi = static_cast<double>(M_PI);
                    const auto factor_conv = pi / 180.;

					constexpr auto npquad = Utilities::ArraySize<decltype(lbdvp_)>::GetValue();

                    for (auto i = 0u; i < npquad; ++i)
                        lbdvp_[i] = pi * static_cast<double>(i + 1.) / static_cast<double>(npquad);

					constexpr auto ntquad = Utilities::ArraySize<decltype(lbdvt_)>::GetValue();

                    for (auto i = 0u; i < ntquad; ++i)
                        lbdvt_[i] = factor_conv * (70. + 10. * static_cast<double>(i));
                }


                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                template<unsigned int DimensionT>
                void Microsphere<FiberIndexI4T, FiberIndexI6T>
                ::ComputeWDerivates(const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_list_data,
                                    const GeometricElt& geom_elt,
                                    const Advanced::RefFEltInLocalOperator& ref_felt,
                                    const LocalVector& cauchy_green_tensor_value,
                                    const LocalMatrix& transposed_De,
                                    LocalVector& dW,
                                    LocalMatrix& d2W)
                {
                    static_cast<void>(ref_felt);
                    static_cast<void>(transposed_De);

                    const auto& quad_pt = quad_pt_unknown_list_data.GetQuadraturePoint();

                    // References to initiliazed vectors and matrices.
                    auto& tauxtau =
						this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tauxtau)>();
                    auto& tau_n =
						this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_n)>();
                    auto& tau_global =
						this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::tau_global)>();

                    // Non linear stiffness contribution (second order derivative of the energetic potential).
                    auto& tauxtauxtauxtau =
						this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tauxtauxtauxtau)>();

                    // Get local values of fiber vector, fiber density, in plane fiber dispersion and out of plane fiber
                    // dispersion for families I4 and I6 from the InputPolicy. The fiber densities need to be prenormalized.
					decltype(auto) input_microsphere = GetInputMicrosphere();

                    const auto& fiber_I4 = fibers_I4_.GetValue(quad_pt, geom_elt);
                    const auto& density_I4 =
						input_microsphere.GetFiberStiffnessDensityI4().GetValue(quad_pt, geom_elt);
                    const auto& inplane_dispersion_I4 =
						input_microsphere.GetInPlaneFiberDispersionI4().GetValue(quad_pt, geom_elt);
                    const auto& transverse_dispersion_I4 =
						input_microsphere.GetOutOfPlaneFiberDispersionI4().GetValue(quad_pt, geom_elt);

                    const auto& fiber_I6 = fibers_I6_.GetValue(quad_pt, geom_elt);
                    const auto& density_I6 =
						input_microsphere.GetFiberStiffnessDensityI6().GetValue(quad_pt, geom_elt);
                    const auto& inplane_dispersion_I6 =
						input_microsphere.GetInPlaneFiberDispersionI6().GetValue(quad_pt, geom_elt);
                    const auto& transverse_dispersion_I6 =
						input_microsphere.GetOutOfPlaneFiberDispersionI6().GetValue(quad_pt, geom_elt);

                    // normalisation of fiber directions and calculation of the normal to the fiber plane
                    // fiber vectors are copied
					constexpr int norm2 = 2;
                    const auto norm_I4 = xt::linalg::norm(fiber_I4, norm2);
                    const auto norm_I6 = xt::linalg::norm(fiber_I6, norm2);
					auto tau_I4 = fiber_I4;
					auto tau_I6 = fiber_I6;

                    if (!(NumericNS::IsZero(norm_I4)) and !(NumericNS::IsZero(norm_I6)))
					{
						// local basis vectors tau_I4 & tau_I6
						tau_I4 /= norm_I4;
						tau_I6 /= norm_I6;
						
						// Vectorial product to construct third local basis vector tau_n (normal out of the plane).
						tau_n(0) = tau_I4(1) * tau_I6(2) - tau_I4(2) * tau_I6(1);
						tau_n(1) = tau_I4(2) * tau_I6(0) - tau_I4(0) * tau_I6(2);
						tau_n(2) = tau_I4(0) * tau_I6(1) - tau_I4(1) * tau_I6(0);
						
						const auto size = dW.shape(0);

						decltype(auto) quad_pt_list_along_plane = GetQuadraturePointsAlongPlane();
						decltype(auto) quad_pt_list_outside_plane = GetQuadraturePointsOutsidePlane();

						
						// Loop on spherical quadrature points.
						for (const auto value_quad_pt_p : quad_pt_list_along_plane)
						{
							for (const auto value_quad_pt_t : quad_pt_list_outside_plane)
							{
								double norm = 0.;
								
								// Local coordinates of the integration direction in the fiber frame of reference.
								const double xloc1 = std::cos(value_quad_pt_p) * std::sin(value_quad_pt_t);
								const double xloc2 = std::sin(value_quad_pt_p) * std::sin(value_quad_pt_t);
								const double xloc3 = std::cos(value_quad_pt_t);
								
								// Calculation of the local integration directions and diagonal values sigma_xx, sigma_yy and sigma_zz.
								for (auto i = 0u; i < DimensionT ; ++i)
								{
									tau_global(i) = tau_I4(i) * xloc1 + tau_I6(i) * xloc2 + tau_n(i) * xloc3;
									tauxtau(i) = NumericNS::Square(tau_global(i));
									norm += NumericNS::Square(tau_global(i));
								}
								
								// Extra diagonal values sigma_xy, sigma_yz and sigma_xz.
								for (auto i = DimensionT ; i < size ; ++i)
									tauxtau(i) = tau_global(i % DimensionT) * tau_global((i + 1) % DimensionT);

								// normalisation of initial fiberlength
								tauxtau /= norm;

								// calculation of the squ are elongation, of the fiber elongation, of the integration weights, of weigthed stiffness
                                double elongation_squared = xt::linalg::vdot(cauchy_green_tensor_value, tauxtau);

                                for (auto i = DimensionT; i < size ; ++i)
                                    elongation_squared += cauchy_green_tensor_value(i) * tauxtau(i);
								
								const double elongation = std::sqrt(elongation_squared);
								const double weight_I4 = std::exp(inplane_dispersion_I4 * std::cos(2.0 * value_quad_pt_p)
															- transverse_dispersion_I4 * std::cos(2.0 * value_quad_pt_t));
								const double weight_I6 = std::exp(- inplane_dispersion_I6 * std::cos(2.0 * value_quad_pt_p)
															- transverse_dispersion_I6 * std::cos(2.0 * value_quad_pt_t));
								const  double local_stiffness = weight_I4 * density_I4 + weight_I6 * density_I6;
								
								
								// Calculation of the fiber traction and of its derivative with respect to the squared elongation.
								// Piola = 2 * dW
								const double dW_contribution = local_stiffness * (elongation - 1.) / elongation;
								// Tangent = 4 * d2W
								const double d2W_contribution = local_stiffness / NumericNS::Cube(elongation);
								
								// update of gradient and of second derivative with respect to Cauchy Greene
								dW += dW_contribution * tauxtau;
								xt::noalias(tauxtauxtauxtau) = xt::linalg::outer(tauxtau, tauxtau);
								d2W += d2W_contribution * tauxtauxtauxtau;
							}
						}
					}
				}
				

                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                inline const ::MoReFEM::FiberList<ParameterNS::Type::vector>&
				Microsphere<FiberIndexI4T, FiberIndexI6T>::GetFibersI4() const noexcept
                {
                    return fibers_I4_;
                }

                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                inline const ::MoReFEM::FiberList<ParameterNS::Type::vector>&
				Microsphere<FiberIndexI4T, FiberIndexI6T>::GetFibersI6() const noexcept
                {
                    return fibers_I6_;
                }

                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                inline const TimeManager& Microsphere<FiberIndexI4T, FiberIndexI6T>::GetTimeManager() const noexcept
                {
                    return time_manager_;
                }


                template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                inline const typename Microsphere<FiberIndexI4T, FiberIndexI6T>::input_internal_variable_policy_type&
                Microsphere<FiberIndexI4T, FiberIndexI6T>::GetInputMicrosphere() const noexcept
                {
                    return input_internal_variable_policy_;
                }


				template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
                inline const std::array<double, 20ul>& Microsphere<FiberIndexI4T, FiberIndexI6T>
				::GetQuadraturePointsAlongPlane() const noexcept
				{
					return lbdvp_;
				}


				template <unsigned int FiberIndexI4T, unsigned int FiberIndexI6T>
				inline const std::array<double, 5ul>& Microsphere<FiberIndexI4T, FiberIndexI6T>
				::GetQuadraturePointsOutsidePlane() const noexcept
				{
					return lbdvt_;
				}


            } // namespace InternalVariablePolicyNS


        } // namespace SecondPiolaKirchhoffStressTensorNS


    } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_INTERNAL_VARIABLE_POLICY_x_MICROSPHERE_HXX_
