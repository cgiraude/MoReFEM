/*!
 * \defgroup OperatorInstancesGroup OperatorInstances
 *
 * \brief Module to encompass instances of \a Operator.
 *
 * This is disjoined from \a Operator module to break potential circular dependency: definition order is:
 * - Parameters library
 * - Operators library
 * - OperatorInstances library, which may use stuff defined in Operators this way.
 *
 */


/// \addtogroup OperatorInstancesGroup
///@{



///@}



/*!
 * \class doxygen_hide_local_variational_operator_compute_elt_array_ancillary_methods
 *
* Up until 2017, there was no such method: Assemble() method of a \a GlobalVariationalOperator just called
* ComputeEltArray(). However, in some cases we might want to just perform this pre-computation without
* assembling the matrix, hence the decomposition of the call in the internals of Assemble():
*
* \code
* local_operator.InitLocalComputation();
* local_operator.ComputeEltArray();
* local_operator.FinalizeLocalComputation();
* \endcode
*
* For most operators, we might as well keep the first and third method call empty, but there is now room to
* fine tune it if needed.
 */


/*!
 * \class doxygen_hide_local_variational_operator_empty_init_local_computation
 *
 * \brief Initialization phase of the local computation; kept empty for this operator.
 *
 * \copydoc doxygen_hide_local_variational_operator_compute_elt_array_ancillary_methods
 */


/*!
* \class doxygen_hide_local_variational_operator_empty_finalize_local_computation
*
* \brief Initialization phase of the local computation; kept empty for this operator.
*
* \copydoc doxygen_hide_local_variational_operator_compute_elt_array_ancillary_methods
*/

 /*!
  * \class doxygen_hide_test_unknown_list_param
  *
  * \param[in] test_unknown_list Same as \a unknown_list for test unknowns; there are no implicit
  * relationship expected between this one and \a unknown_list. Usually for most operators each
  * test unknown is related to the solution unknown ranked at the same position, but it is really
  * at the discretion of the operator developer, depending on its choice of ordering for both
  * unknowns and test unknowns in constructor. However, the library itself doesn't expect anything.
  */


/*!
* \class doxygen_hide_test_unknown_ptr_param
*
* \param[in] test_unknown_ptr Same as \a unknown_ptr for test functions.
*/

