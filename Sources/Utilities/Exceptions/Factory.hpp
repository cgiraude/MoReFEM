/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 8 Oct 2013 13:37:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_FACTORY_HPP_
# define MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_FACTORY_HPP_


# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace ExceptionNS
    {


        namespace Factory
        {


            //! Generic exception for factory.
            class Exception : public MoReFEM::Exception
            {
            public:

                /*!
                 * \brief Constructor with simple message.
                 *
                 * \param[in] msg Message.
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
                 */
                explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

                //! \copydoc doxygen_hide_copy_constructor
                Exception(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                Exception(Exception&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                Exception& operator=(const Exception& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                Exception& operator=(Exception&& rhs) = default;

                //! Destructor
                virtual ~Exception() override;
            };


            //! Thrown when a new element try to register with an already existing name.
            class UnableToRegister final : public Exception
            {
            public:

                /*!
                 * \brief Called when the code was unable to register a new object.
                 *
                 * Insertion failed, so the most likely explanation is that another object with the same name already existed.
                 *
                 * \param[in] object_name Name of the object that failed to be inserted in the factory_content.
                 * \param[in] factory_content Content of the factory for which the problem arose. For instance, "GeometricElt".
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit UnableToRegister(const std::string& object_name,
                                          const std::string& factory_content,
                                          const char* invoking_file, int invoking_line);

                //! \copydoc doxygen_hide_copy_constructor
                UnableToRegister(const UnableToRegister& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                UnableToRegister(UnableToRegister&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                UnableToRegister& operator=(const UnableToRegister& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                UnableToRegister& operator=(UnableToRegister&& rhs) = default;

                //! Destructor
                virtual ~UnableToRegister();

            };



            //! Thrown when trying to create an object which name is not registered.
            class UnregisteredName final : public Exception
            {
            public:

                /*!
                 * \brief Thrown when trying to create an object which name is not registered.
                 *
                 * \param[in] object_name Name provided to create a new object.
                 * \param[in] factory_content Content of the factory for which the problem arose. For instance, "GeometricElt".
                 * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
                 * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

                 */
                explicit UnregisteredName(const std::string& object_name,
                                          const std::string& factory_content,
                                          const char* invoking_file, int invoking_line);


                //! Destructor
                virtual ~UnregisteredName();

                //! \copydoc doxygen_hide_copy_constructor
                UnregisteredName(const UnregisteredName& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                UnregisteredName(UnregisteredName&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                UnregisteredName& operator=(const UnregisteredName& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                UnregisteredName& operator=(UnregisteredName&& rhs) = default;
            };


        } // namespace FactoryNS


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_FACTORY_HPP_
