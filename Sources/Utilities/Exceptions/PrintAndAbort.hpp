/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 4 Mar 2015 10:51:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_PRINT_AND_ABORT_HPP_
# define MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_PRINT_AND_ABORT_HPP_

# include <string>


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Wrappers
    {


        class Mpi;


    } // namespace Wrappers



    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================



    namespace ExceptionNS
    {


        /*!
         * \brief Print the exception in the main and abort the mpi processes.
         *
         * \copydetails doxygen_hide_mpi_param Used here to give away the rank for which the
         * exception was thrown.
         * \param[in] exception_message Explanation message encapsulated within the exception. Usually the output
         * of \a what() method for exceptions that derive from std::exception, but some exceptions from third-party
         * libraries do not and define their own method for that (currently no such library is in use within MoReFEM).
         */
        void PrintAndAbort(const Wrappers::Mpi& mpi,
                           const std::string& exception_message);


    } // namespace ExceptionNS


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Exceptions/PrintAndAbort.hxx"


#endif // MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_PRINT_AND_ABORT_HPP_
