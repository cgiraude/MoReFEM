/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 8 Oct 2013 13:37:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_GRACEFUL_EXIT_HPP_
# define MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_GRACEFUL_EXIT_HPP_


# include <exception>


namespace MoReFEM::ExceptionNS
{


    //! Exception when we want to exit gracefull with a EXIT_SUCCESS return code.
    class GracefulExit : private std::exception
    {
    public:

        //! Alias to parent.
        using parent = std::exception;

    public:

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit GracefulExit(const char* invoking_file, int invoking_line);

        //! \copydoc doxygen_hide_copy_constructor
        GracefulExit(const GracefulExit& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        GracefulExit(GracefulExit&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        GracefulExit& operator=(const GracefulExit& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        GracefulExit& operator=(GracefulExit&& rhs) = default;

        //! Destructor
        virtual ~GracefulExit() override;

        /*!
         * \brief Display the what message from std::exception.
         *
         * \return The what() message as a char* (which reads the internal std::string so no risk of deallocation
         * issue).
         */
        virtual const char* what() const noexcept override final;

    private:

        //! The complete what() message (with the location part)
        std::string what_message_;
    };



} // namespace MoReFEM::ExceptionNS


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_GRACEFUL_EXIT_HPP_
