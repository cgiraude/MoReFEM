/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_EXCEPTION_HPP_
# define MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_EXCEPTION_HPP_



#include <string>
#include <exception>



namespace MoReFEM
{


    /*!
     * \brief Generic class for MoReFEM exceptions.
     */
    class Exception: public std::exception
    {
    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */

        //@}
        explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~Exception() noexcept override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;

        ///@}

        /*!
         * \brief Display the what message from std::exception.
         *
         * \return The what() message as a char* (which reads the internal std::string so no risk of deallocation
         * issue).
         */
        virtual const char* what() const noexcept override final;

        /*!
         * \brief Display the raw message (Without file and line).
         *
         * Might be useful if exception is caught to rewrite a more refined message.
         *
         * Before introducing this, we could end up in some cases with something like:
         * \verbatim
         * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined
         * finite element space 1: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 101:
         * Domain 1 is not defined!
         * \endverbatim
         *
         * Clearly it is nicer to provide:
         * \verbatim
         * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined
         * finite element space 1: Domain 1 is not defined!
         * \endverbatim
         *
         * \return Exception error message withou information about file and line in which the exception was invoked.
         */
        const std::string& GetRawMessage() const noexcept;


    private:

        //! The complete what() message (with the location part)
        std::string what_message_;

        //! Incomplete message (might be useful if we catch an exception to tailor a more specific message).
        std::string raw_message_;

    };



    /*!
     * \brief Print the what() of an exception that occurred before the main() call.
     *
     * In the Factory idiom, some registering functions are intended to be called BEFORE the main().
     *
     * For this reason, an additional step is added (namely print to std::cerr the explanation of the exception)
     * so that developer might find quickly what happened.
     *
     * \param[in] exception Exception to throw.
     */
    [[noreturn]] void ThrowBeforeMain(Exception&& exception);





} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_EXCEPTIONS_x_EXCEPTION_HPP_
