/*!
//
// \file
//
//
//
//  Created by Jérôme Diaz on 26/02/2020.
//  Copyright © 2020 Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_HXX_
# define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_HXX_


namespace MoReFEM
{


        template <class T, class Parameter>
        StrongType<T, Parameter>::StrongType(T const& value)
        : value_(value)
        { }


        template <class T, class Parameter>
        template <class T_>
        StrongType<T, Parameter>::StrongType(T&& value,
                                             std::enable_if_t<!std::is_reference<T_>{},
                                             std::nullptr_t>)
        : value_(std::move(value))
        { }


        template <class T, class Parameter>
        inline T const& StrongType<T, Parameter>::get() const noexcept
        {
            return value_;
        }


        template <class T, class Parameter>
        inline T& StrongType<T, Parameter>::get() noexcept
        {
            return value_;
        }


}


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_HXX_
