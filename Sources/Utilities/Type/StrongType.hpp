/*!
//
// \file
//
//
//
//  Created by Jérôme Diaz on 26/02/2020.
//  Copyright © 2020 Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_HPP_
# define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_HPP_

#include <type_traits>
#include <utility>

namespace MoReFEM
{


    /*!
     * \brief Abstract class used to define a StrongType, which allows more expressive code and ensures that the order of arguments of the same
     * underlying type is respected at call sites.
     *
     * Adapted from https://github.com/joboccara/NamedType/
     *
     * \tparam T Type of the parameter.
     * \tparam Parameter This is a phantom type used to specialize the type deduced for SrongType, it is not actually used in the implementation.
     *
     * As an example we could create a class Rectangle with strong types to differentiate its width from its length as:
     *
     * \code
     * using Width = StrongType<double, struct WidthTag>;
     * using Height = StrongType<double, struct HeightTag>;
     *
     * class StrongRectangle
     * {
     * public:
     *     StrongRectangle (Width width, Height height) : width_(width.get()), height_(height.get()) {}
     *     double getWidth() const {return width_;}
     *     double getHeight() const {return height_;}
     *
     * private:
     *     double width_;
     *     double height_;
     * };
     * \endcode
     *
     * At the calling site we would have:
     *
     * \code
     * StrongRectangle rectangle((Width(5.0)), (Height((2.0))));
     * \endcode
     *
     * Note that the extra parenthesis are only required for constructors due to the most vexing parse.
     */
    template <class T, class Parameter>
    class StrongType
    {
    public:

        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor.
         *
         * \param[in] value Underlying value held by the strong type.
         *
         */
        explicit StrongType(T const& value);

        /*!
         * \brief Move constructor.
         *
         * \param[in] value Underlying value held by the strong type.
         *
         * \tparam T_ Artifical template parameter to ensure that SFINAE applies in case \a T  is  a reference.
         *
         */
        template<class T_ = T>
        explicit StrongType(T&& value,
                            std::enable_if_t<!std::is_reference<T_>{},
                                                    std::nullptr_t> = nullptr);

        //! Destructor.
        ~StrongType() = default;

        //! \copydoc doxygen_hide_copy_constructor
        StrongType(const StrongType& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        StrongType(StrongType&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        StrongType& operator=(const StrongType& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        StrongType& operator=(StrongType&& rhs) = default;


        ///@}

    public:

        //! Constant accessor to the underlying value held by the strong type.
        T const& get() const noexcept;

        //! Non constant accessor to the underlying value held by the strong type.
        T& get() noexcept;

    private:

        //! Underlying value held by the strong type.
        T value_;
    };


}


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Type/StrongType.hxx"


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_HPP_
