/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Mar 2018 09:12:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_PRINT_TYPE_NAME_HPP_
# define MOREFEM_x_UTILITIES_x_TYPE_x_PRINT_TYPE_NAME_HPP_

# include <string_view>


namespace MoReFEM
{


    /*!
     * \brief Fetch the name of the type given as template parameter.
     *
     * Adapted from https://stackoverflow.com/questions/81870/is-it-possible-to-print-a-variables-type-in-standard-c
     *
     * \tparam T Type which information is requested.
     *
     * \return Name of the type, e.g. 'double' or 'const std::string&'.
     */
    template <class T>
    constexpr std::string_view GetTypeName();


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/Type/PrintTypeName.hxx"


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_PRINT_TYPE_NAME_HPP_
