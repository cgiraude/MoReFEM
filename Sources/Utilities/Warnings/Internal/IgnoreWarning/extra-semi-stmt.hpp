//
//  extra-semi-stmt.hpp
//  Utilities
//
//  Created by sebastien on 20/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

# include "Utilities/Warnings/Pragma.hpp"

# ifdef __clang__
	#  if !defined(__apple_build_version__) || __clang_major__ >= 11
	PRAGMA_DIAGNOSTIC(ignored "-Wextra-semi-stmt")
	#  endif
# endif // __clang__
