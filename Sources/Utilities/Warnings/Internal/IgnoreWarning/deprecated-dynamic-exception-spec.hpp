//
//  extra-semi-stmt.hpp
//  Utilities
//
//  Created by sebastien on 20/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

# include "Utilities/Warnings/Pragma.hpp"

# ifdef __clang__
	PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated-dynamic-exception-spec")
# endif // __clang__
