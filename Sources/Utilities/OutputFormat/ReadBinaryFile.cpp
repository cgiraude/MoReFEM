//! \file 
//
//
//  ReadBinaryFile.cpp
//  MoReFEM
//
//  Created by sebastien on 17/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include <fstream>
#include <cmath>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/OutputFormat/ReadBinaryFile.hpp"
#include "Utilities/Filesystem/File.hpp"


namespace MoReFEM::Advanced
{


    std::vector<double> ReadSimpleBinaryFile(const std::string& binary_file,
                                             const char* invoking_file, int invoking_line,
                                             double epsilon)
    {
        std::vector<double> ret;

        std::ifstream in;
        FilesystemNS::File::Read(in, binary_file, invoking_file, invoking_line);

        if (in)
        {
            // Get length of file.
            std::ifstream::pos_type block_size;
            block_size = in.seekg(0, std::ifstream::end).tellg();
            in.seekg(0,  std::ifstream::beg);

            std::vector<char> buffer(static_cast<std::size_t>(block_size));

            // Read data as a block.
            in.read(buffer.data(), block_size);

            if (!in)
                throw Exception("Unable to read file " + binary_file, invoking_file, invoking_line);

            in.close();

            // Buffer contains the entire file. Convert bytes back into doubles.
            const double * values = reinterpret_cast<double *> (buffer.data());

            const unsigned long Nvalue_binary = static_cast<std::size_t>(block_size) / sizeof(double);

            for (std::size_t i = 0u; i < Nvalue_binary; ++i)
            {
                if (std::fabs(values[i]) <= epsilon)
                    ret.push_back(0.);
                else
                    ret.push_back(values[i]);
            }
        }

        return ret;
    }


} // namespace MoReFEM::Advanced
