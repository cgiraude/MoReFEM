/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Mar 2015 14:57:33 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_MATRIX_STORAGE_HXX_
# define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_MATRIX_STORAGE_HXX_


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
        constexpr std::size_t LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::N()
        {
            return NlocalMatricesT;
        }


        template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
        void LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>
        ::InitLocalMatrixStorage(const std::array<std::pair<unsigned int, unsigned int>, NlocalMatricesT>& matrices_dimension)
        {
            for (std::size_t i = 0ul; i < NlocalMatricesT; ++i)
            {
                auto& matrix = matrix_list_[i];

                if constexpr(std::is_same<LocalMatrixT, LocalMatrix>())
                {
                    matrix.resize({ static_cast<std::size_t>(matrices_dimension[i].first),
                        static_cast<std::size_t>(matrices_dimension[i].second) });
                    matrix.fill(0.);
                }
            }
        }


        template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
        template<std::size_t IndexT>
        inline LocalMatrixT& LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::GetLocalMatrix() const
        {
            static_assert(IndexT < NlocalMatricesT, "Check index is within bounds!");
            return matrix_list_[IndexT];
        }



    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_LOCAL_x_LOCAL_MATRIX_STORAGE_HXX_
