/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 5 Sep 2016 10:32:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_ACCESS_HXX_
# define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_ACCESS_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalLinearAlgebraNS
        {


            template<class WorkVariableT>
            template<class DataT>
            GlobalVectorTemporaryAccess<WorkVariableT>::GlobalVectorTemporaryAccess(const DataT& data)
            : GlobalVectorTemporaryAccess<WorkVariableT>(data.GetNonCstTemporaryGlobalVectorManager(typename WorkVariableT::identifier_type()))
            { }


            template<class WorkVariableT>
            GlobalVectorTemporaryAccess<WorkVariableT>::GlobalVectorTemporaryAccess(WorkVariableT& work_variable)
            : work_variable_(work_variable),
            vector_index_(work_variable.DeterminedUnusedIndex())
            {
                work_variable_.usage_.set(vector_index_);
            }


            template<class WorkVariableT>
            GlobalVectorTemporaryAccess<WorkVariableT>::~GlobalVectorTemporaryAccess()
            {
                work_variable_.usage_.reset(vector_index_);
            }


            template<class WorkVariableT>
            typename WorkVariableT::vector_type& GlobalVectorTemporaryAccess<WorkVariableT>::GetNonCstVector() noexcept
            {
                assert(!(!work_variable_.storage_[vector_index_]));

                return *work_variable_.storage_[vector_index_];
            }


        } // namespace GlobalLinearAlgebraNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_ACCESS_HXX_
