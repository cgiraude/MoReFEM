/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 5 Sep 2016 10:32:49 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_STORAGE_HXX_
# define MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_STORAGE_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalLinearAlgebraNS
        {


            template
            <
                class GlobalVectorT,
                std::size_t N,
                std::size_t IdentifierT
            >
            GlobalVectorTemporaryStorage<GlobalVectorT, N, IdentifierT>
            ::GlobalVectorTemporaryStorage(const GlobalVectorT& model)
            {
                for (auto i = 0ul; i < N; ++i)
                    storage_[i] = std::make_unique<GlobalVectorT>(model);

                usage_.reset();
            }


            template
            <
                class GlobalVectorT,
                std::size_t N,
                std::size_t IdentifierT
            >
            std::size_t GlobalVectorTemporaryStorage<GlobalVectorT, N, IdentifierT>
            ::DeterminedUnusedIndex() noexcept
            {
                // Look for an unused vector.
                std::size_t unused_vector_index = 0;

                assert(!usage_.all() && "If this assert shows up, it means you are using at once all availables "
                       "work vectors. Consider incrementing the template parameter N, or check you release "
                       "correctly the vectors after usage! (release is automatic as soon as RAII AccessGlobalVectorTemporaryStorage "
                       "goes out of scope).");

                while (usage_[unused_vector_index])
                    ++unused_vector_index;

                return unused_vector_index;
            }


        } // namespace GlobalLinearAlgebraNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_LINEAR_ALGEBRA_x_STORAGE_x_GLOBAL_x_INTERNAL_x_GLOBAL_VECTOR_TEMPORARY_STORAGE_HXX_
