/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 27 Dec 2016 15:47:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EXTRACT_PARAMETER_x_EXTRACT_PARAMETER_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EXTRACT_PARAMETER_x_EXTRACT_PARAMETER_HPP_

# include "Utilities/InputData/Crtp/Section.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            //! \copydoc doxygen_hide_namespace_cluttering
            using CountAsUsed = ::MoReFEM::Utilities::InputDataNS::CountAsUsed;


            /*!
             * \brief Extract the value of a parameter which is directly (i.e. not within a subsection) present in
             * the section.
             *
             * This might be useful for instance if we have tracked a specific section such as 'Domain' and we
             * want to access parameters that we know are directly there, such as 'DimensionList'.
             *
             * \param[in] section The section currently considered (in the example above, it is a specific Domain ad
             * 'Domain5').
             *
             * \tparam ParameterT Usual parameter type to be expected within the current section. e.g. 'DimensionList'
             * for 'Domain'.
             * \tparam DerivedT One of the template parameter to identify the type of section considered.
             * Expected to be automatically derived from section.
             * \tparam EnclosingSectionT Same as \a DerivedT.
             * \tparam CountAsUsedT Whether the extracted is to be marked as used following this extraction. In most
             * of the case the default value of 'yes'is what is expected.
             *
             * \return Value of the input parameter (possibly through a reference).
             */
            template<class ParameterT, class DerivedT, class EnclosingSectionT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
            decltype(auto) ExtractParameter(const Utilities::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section);


            /*!
             * \brief Same functionality as ExtractParameter, except that environment variables are replaced by their
             * values.
             *
             * \param[in] section Section that enclose the input parameter.
             *
             * \return Path given by the input parameter.
             */
            template
            <
                class ParameterT,
                class DerivedT,
                class EnclosingSectionT,
                CountAsUsed CountAsUsedT = CountAsUsed::yes
            >
            std::string ExtractPathParameter(const Utilities::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section);



        } //  namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputData/Internal/ExtractParameter/ExtractParameter.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EXTRACT_PARAMETER_x_EXTRACT_PARAMETER_HPP_
