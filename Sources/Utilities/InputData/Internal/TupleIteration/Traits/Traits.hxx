/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            namespace Traits
            {


                template<class return_type>
                std::string Format<return_type>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "VALUE";
                };


                template<class T, class U>
                std::string Format<std::map<T, U>>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "{[KEY1] = VALUE1, [KEY2] = VALUE2, ...}";
                };


                template<class T>
                std::string Format<std::vector<T>>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "{ VALUE1, VALUE2, ...}";
                };


                template<class T>
                std::string Format<Utilities::InputDataNS::LuaFunction<T>>
                ::Print(const std::string& indent_comment, bool none_desc)
                {
                    std::ostringstream oconv;

                    oconv << "function in Lua language, for instance:\n"
                    << indent_comment << "function(arg1, arg2, arg3)\n"
                    << indent_comment << "return arg1 + arg2 - arg3\n"
                    << indent_comment << "end\n"
                    << indent_comment << "sin, cos and tan require a 'math.' preffix.\n";

                    if (none_desc)
                        oconv << indent_comment << "If you do not wish to provide one, put anything you want (e.g. 'none'): "
                        "the content is not interpreted by LuaOptionFile until an actual use of the underlying function.";

                    return oconv.str();
                };


                template<class... Args>
                std::string Format<std::variant<Args...>>::Print(const std::string& indent_comment)
                {
                    static_cast<void>(indent_comment);
                    return "see the variant description...";
                };


            } // namespace Traits


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_TRAITS_x_TRAITS_HXX_
