/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 23 Dec 2016 17:21:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace InputDataNS
        {


            namespace Impl
            {



                template<class TupleEltTypeT>
                template<class InputDataT>
                void ElementType<TupleEltTypeT>::Set(TupleEltTypeT& element,
                                                     const InputDataT* input_data,
                                                     const std::string& full_name,
                                                     LuaOptionFile& lua_option_file)
                {
                    static_cast<void>(input_data); // as it might ot be used in one of the static if/else case below.

                    using return_type = typename TupleEltTypeT::return_type;

                    {
                        return_type ret;

                        if constexpr(HAS_MEMBER_Selector<TupleEltTypeT>::value)
                        {
                            lua_option_file.Read(full_name,
                                                 TupleEltTypeT::Constraint(),
                                                 ret,
                                                 __FILE__, __LINE__,
                                                 TupleEltTypeT::Selector(input_data));
                        }
                        else
                        {
                            lua_option_file.Read(full_name,
                                                 TupleEltTypeT::Constraint(),
                                                 ret,
                                                 __FILE__, __LINE__,
                                                 nullptr);
                        }

                        element.SetValue(ret);
                    }

                }


            } // namespace Impl


        } // namespace InputDataNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_TUPLE_ITERATION_x_IMPL_x_TUPLE_ITEM_HXX_
