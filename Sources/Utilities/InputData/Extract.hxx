//! \file
//
//
//  Extract.hxx
//  MoReFEM
//
//  Created by sebastien on 23/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HXX_


namespace MoReFEM::Utilities::InputDataNS
{


    template<class ObjectT>
    template<class InputDataT, CountAsUsed CountAsUsedT>
    typename ConstRefOrValue<typename Extract<ObjectT>::return_type>::type
    Extract<ObjectT>::Value(const InputDataT& input_data)
    {
        return input_data.template ReadHelper<ObjectT, CountAsUsedT>();
    }


    template<class ObjectT>
    template<class InputDataT>
    std::string Extract<ObjectT>::Path(const InputDataT& input_data)
    {
        return input_data.template ReadHelperPath<ObjectT>();
    }


    template<class ObjectT>
    template<class InputDataT>
    unsigned int Extract<ObjectT>::Number(const InputDataT& input_data)
    {
        return input_data.template ReadHelperNumber<ObjectT>();
    }


    template<class ObjectT>
    template<class InputDataT, CountAsUsed CountAsUsedT>
    decltype(auto)
    Extract<ObjectT>::Subscript(const InputDataT& input_data,
                                unsigned int index)
    {
        return input_data.template ReadHelper<ObjectT, CountAsUsedT>(index);
    }


} // namespace MoReFEM::Utilities::InputDataNS


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HXX_
