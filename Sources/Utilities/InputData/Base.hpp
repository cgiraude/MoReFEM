/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Aug 2013 15:09:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_BASE_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_BASE_HPP_


# include <vector>
# include <string>
# include <cassert>
# include <tuple>
# include <type_traits>

# include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
# include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

# include "Utilities/Containers/Tuple.hpp"
# include "Utilities/Filesystem/File.hpp"
# include "Utilities/Filesystem/Advanced/Directory.hpp"
# include "Utilities/String/String.hpp"
# include "Utilities/Environment/Environment.hpp"
# include "Utilities/LuaOptionFile/LuaOptionFile.hpp"
# include "Utilities/InputData/Extract.hpp"
# include "Utilities/InputData/Exceptions/InputData.hpp"
# include "Utilities/InputData/Internal/Subtuple/Subtuple.hpp"
# include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
# include "Utilities/Mpi/Mpi.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        /// \namespace MoReFEM::Utilities::InputDataNS
        /// \brief Namespace dedicated to InputData.
        namespace InputDataNS
        {


            /*!
             * \brief Whether a field found in the file but not referenced in the tuple yields an exception or not.
             *
             * Should be 'yes' most of the time; I have introduced the 'no' option for cases in which we need only
             * a handful of parameters shared by all models for post-processing purposes (namely mesh-related ones).
             */
            enum class DoTrackUnusedFields { yes, no };


            // ============================
            // Forward declarations.
            // ============================

            template<class ObjectT>
            struct Extract;


            // ============================
            // End of forward declarations.
            // ============================



            /*!
             * \brief Provides all the mechanisms to read the input parameters and hold their values.
             *
             * As it names hints, it is intended to be a base class for a user-defined class which will deals
             * with the input parameters specific to the user problem.
             *
             * \tparam DerivedT Derived CRTP class.
             * \tparam TupleT A tuple which should include a class for each input parameter to consider in the file.
             * The class for each input parameter should derive from InputData::Crtp::InputData and defines several
             * static methods such as Description(), NameInFile() or Section(). Compiler won't let you not define one
             * of those anyway; to see an example of such a class see in Core/InputData or in MoReFEM documentation.
             *
             * \internal <b><tt>[internal]</tt></b> It should be a specialization of InputData; however I didn't manage to make it
             * a template template parameter that works both on clang and gcc.
             * \endinternal
             */
            template<class DerivedT, class TupleT>
            class Base : public ::MoReFEM::Crtp::CrtpMpi<Base<DerivedT, TupleT>>
            {

            public:


                //! Friendship.
                template<class ObjectT>
                friend struct Extract;


            public:

                //! Mpi parent.
                using mpi_parent = ::MoReFEM::Crtp::CrtpMpi<Base<DerivedT, TupleT>>;

                //! The underlying tuple type.
                using Tuple = TupleT;

                //! Size of the tuple.
                static constexpr std::size_t Size();

                //! Special members.
                //@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydetails doxygen_hide_mpi_param
                 * \param[in] filename Name of the Lua input parameter list to be read.
                 * \param[in] do_track_unused_fields Whether a field found in the file but not referenced in the tuple
                 * yields an exception or not. Should be 'yes' most of the time; I have introduced the 'no' option for
                 * cases in which we need only a handful of parameters shared by all models for post-processing
                 * purposes (namely mesh-related ones).
                 */
                explicit Base(const std::string& filename,
                              const Wrappers::Mpi& mpi,
                              DoTrackUnusedFields do_track_unused_fields = DoTrackUnusedFields::yes);

                //! \copydoc doxygen_hide_copy_constructor
                Base(const Base& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Base(Base&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Base& operator=(const Base& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                Base& operator=(Base&& rhs) = delete;


                //! Destructor.
                ~Base();
                //@}


                /*!
                 * \brief Print the list of input parameter that weren't used in the program.
                 *
                 * This method is dedicated to be called at destruction, but when using OpenMPI it is rather uneasy
                 * to make it called in the destructor due to racing conditions (error if MPI_Finalize() has already
                 * been called.
                 *
                 * So this method should be called at the very end of your program if you want the information
                 * it gives.
                 *
                 * \copydoc doxygen_hide_stream_inout
                 * If nothing added then all input parameters were used.
                 *
                 * \internal <b><tt>[internal]</tt></b> This method collects the data from all mpi processors; the list is therefore ensured to
                 * be exhaustive.
                 * \endinternal
                 *
                 */
                void PrintUnused(std::ostream& stream) const;

                //! Get the path to the input data file.
                //! \return Path to the input data file.
                const std::string& GetInputFile() const;


                /*!
                 * \brief Whether \a ItemT is present in the tuple or not.
                 *
                 * \tparam ItemT Field sought in the tuple.
                 *
                 * \return True if present.
                 */
                template<class ItemT>
                constexpr static bool Find();


            private:


                /*!
                 * \brief Helper function used to extract quantity from the tuple.
                 *
                 * This should not be used directly by a user; ExtractValue class deals with it with a more
                 * user-friendly interface (no pesky template keyword to add!).
                 *
                 * \tparam InputDataT InputData class used to store the wanted input parameter.
                 * \tparam CountAsUsedT Whether the call to the methods counts as an effective use of the input
                 * parameter in the program. See CountAsUsed for more details; default value is fine
                 * in almost all cases.
                 *
                 * \return The requested element in the tuple. It is either copied by value or a const reference.
                 */
                template<class InputDataT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                typename Utilities::ConstRefOrValue<typename InputDataT::return_type>::type
                ReadHelper() const;


                /*!
                 * \brief Helper function used to extract a value from a vector element from the tuple.
                 *
                 * This should not be used directly by a user; Extract class deals with it with a more
                 * user-friendly interface (no pesky template keyword to add!).
                 *
                 * When an input parameter is a vector, this method may be used to fetch an element of this vector.
                 *
                 * \param[in] index Index in the vector of the quantity fetched.
                 * \tparam CountAsUsedT See ReadHelper().
                 *
                 * \return The requested element in the tuple. It is either copied by value or a const reference.
                 */
                template<class InputDataT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                typename Utilities::ConstRefOrValue<typename InputDataT::return_type::value_type>::type
                ReadHelper(unsigned int index) const;


                /*!
                 * \brief Helper function used to extract the size of a vector input parameter.
                 *
                 * \return Size of the vector read as input parameter.
                 *
                 */
                template<class InputDataT, CountAsUsed CountAsUsedT = CountAsUsed::yes>
                unsigned int ReadHelperNumber() const;


                /*!
                 * \brief Helper function used to handle a path: it is basically handled as a mere string, except
                 * that environment variables are replaced on the fly if they respect the format ${...}.
                 *
                 * \warning Contrary to what the const subject (there to give the same interface whatever the string input
                 * parameter is), this method might modify the value stored in the tuple. On first call all environment
                 * variables are replaced, and what is stored afterwards is the result after the substitution.
                 *
                 * For instance "${HOME}/Codes/MoReFEM" will be resolved on a Mac in /Users/ *username* /Codes/MoReFEM.
                 *
                 * \return The requested element in the tuple.
                 */
                template<class InputDataT>
                std::string ReadHelperPath() const;


            private:

                /*!
                 * \brief Ensure all input parameters in \a SubTupleT address vectors with the same size.
                 */
                template<class SubTupleT>
                void EnsureSameLength() const;



                /*!
                 * \brief Throw an exception if there is a duplicate in the keys.
                 *
                 * The key for an InputData A class is given by free function InputDataKey<A>().
                 *
                 * As these keys are what is used by \a LuaOptionFile to read the value, it is important not to use
                 * the same for two parameters.
                 *
                 * An additional bonus is that a same InputData class put twice in the tuple will also trigger this
                 * exception.
                 */
                void CheckNoDuplicateKeysInTuple() const;


                /*!
                 * \brief Check no input parameter present in the Lua file is actually not known by the input parameter
                 * file tuple.
                 *
                 * \param[in] filename Path to the input file.
                 * \param[in] lua_option_file The object that helps interpreting the content of the Lua file.
                 * \param[in] do_track_unused_fields See constructor.
                 *
                 */
                void CheckUnboundInputData(const std::string& filename,
                                          LuaOptionFile& lua_option_file,
                                           DoTrackUnusedFields do_track_unused_fields) const;

            public:

                //! Accessor to the underlying tuple.
                //! \return Underlying tuple.
                const auto& GetTuple() const;

            private:

                //! Number of elements in the tuple.
                enum { tuple_size_ = std::tuple_size<Tuple>::value };

                //! The tuple that actually stores all the relevant quantities for the problem.
                Tuple tuple_;

                //! Path of the input data file.
                std::string input_data_file_;

                //! Helper object to iterate upon the tuple (including the in-depth sections).
                using tuple_iteration = Internal::InputDataNS::TupleIteration<TupleT, 0, tuple_size_>;

            };


            /*!
             * \brief Create a default input file, with all relevant input parameters provided in \a TupleT.
             *
             * If a default parameter is provided in InputData class, it will be written in the file, otherwise
             * a filler text will be written to remind it should be filled.
             *
             * \tparam TupleT including all the input parameters we want to consider.
             *
             * \param[in] path Path to the file that will contain the result. An exception is thrown if the path is invalid.
             */
            template<class TupleT>
            void CreateDefaultInputFile(const std::string& path);


            /*!
             * \brief Write in a file the content of \a input_data.
             *
             * \tparam InputDataT The object which holds the information about input data file content.
             *
             * \param[in] target Path to the file into which the data will be written in the proper format (so it might
             * be reused as input for a MoReFEM model). The file must not exist yet but the directory in which it is
             * located must already exist.
             * \param[in] input_data The data to be written in the file.
             */
            template<class InputData>
            void Write(const InputData& input_data,
                       const std::string& target);


        } //  namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputData/Internal/ExtractParameter/ExtractParameter.hpp"
# include "Utilities/InputData/Base.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_BASE_HPP_
