//! \file 
//
//
//  Enum.cpp
//  MoReFEM
//
//  Created by sebastien on 23/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//


#include "Utilities/InputData/Enum.hpp"
#include "Utilities/String/EmptyString.hpp"




namespace MoReFEM::Utilities::InputDataNS
{


    const std::string& NoEnclosingSection::GetName()
    {
        return EmptyString();
    }


    const std::string& NoEnclosingSection::GetFullName()
    {
        return EmptyString();
    }
    

} // namespace MoReFEM::Utilities::InputDataNS
