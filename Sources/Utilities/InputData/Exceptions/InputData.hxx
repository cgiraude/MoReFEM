/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 17:46:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


//  geometric_elements_exceptions.h
//
//
//  Created by Sebastien Gilles on 12/10/12.
//  Copyright 2012 INRIA. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HXX_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HXX_



namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            namespace ExceptionNS
            {



                /*!
                 * \brief Prepare the error message for MismatchedVector.
                 *
                 * \tparam SubTupleT The list of input parameters objects which vector should have had the same size.
                 */
                template<class SubTupleT>
                std::string MismatchedVectorSizeMsg();


                template<class SubTupleT>
                MismatchedVectorSize<SubTupleT>::MismatchedVectorSize(const char* invoking_file, int invoking_line)
                : Exception(MismatchedVectorSizeMsg<SubTupleT>(), invoking_file, invoking_line)
                { }


                template<class SubTupleT>
                MismatchedVectorSize<SubTupleT>::~MismatchedVectorSize() = default;


                template<class SubTupleT>
                std::string MismatchedVectorSizeMsg()
                {
                    std::ostringstream oconv;
                    oconv << "Some input parameters were expected to be vectors of the same size, "
                    "but that is not the case:\n";

                    return oconv.str();
                }


            } // namespace ExceptionNS


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup




#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HXX_
