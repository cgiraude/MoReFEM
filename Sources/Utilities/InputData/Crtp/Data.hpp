/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Aug 2013 12:19:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_DATA_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_DATA_HPP_


# include <unordered_map>
# include <tuple>
# include "Utilities/Miscellaneous.hpp"
# include "Utilities/Containers/Vector.hpp"
# include "Utilities/String/String.hpp"
# include "Utilities/InputData/LuaFunction.hpp"
# include "Utilities/InputData/Enum.hpp"
# include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
# include "Utilities/InputData/Internal/TupleIteration/Impl/TupleItem.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            namespace Crtp
            {


                /*!
                 * \brief Generic class from which each InputData should derive.
                 *
                 * \tparam DerivedT To enact CRTP behaviour.
                 * \tparam EnclosingSectionT Type of the enclosing section. Choose NoEnclosingSection if the parameter is
                 * at root level.
                 * \tparam ReturnTypeT Type that will be used to store the value read in the input data file.
                 */
                template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
                class InputData
                {
                public:

                    //! Type of the input parameter.
                    using return_type = ReturnTypeT;

                    //! Type of the enclosing section.
                    using enclosing_section = EnclosingSectionT;

                    //! Specifies the nature is 'parameter' and not 'section'.
                    static constexpr Nature GetNature() noexcept;


                    /*!
                     * \return The input parameter.
                     *
                     * Shouldn't be called anywhere but within InputData class: this class gets safeguards to ensure the
                     * variable was properly initialized.
                     *
                     * \internal <b><tt>[internal]</tt></b> There is a reason why this method is NOT called Value(): as explained in the class
                     * description, the purpose of this class is to spawn a new class for each input parameter
                     * to consider. A user might see fit to name such an input parameter class Value, which would
                     * trigger compilation error as the compiler would think the constructor is called instead of
                     * the present method. On the contrary, I would raise a brow if a user would want to name
                     * one of its input parameter GetTheValue...
                     * \endinternal
                     */
                    typename Utilities::ConstRefOrValue<return_type>::type GetTheValue() const;

                    //! Set the value.
                    //! \param[in] value Value to assign.
                    void SetValue(return_type value);

                    //! Tells the value has been called and has therefore probably be used.
                    void SetAsUsed() const noexcept;

                    //! Tells whether the value has been used at least once.
                    bool IsUsed() const noexcept;

                    //! Induce the identifier from section and name.
                    static const std::string& GetIdentifier();

                    /*!
                     * \brief Get the enclosing name (i.e. the sections in which the current input data is nested).
                     *
                     * \return The enclosing sections with the final dot, e.g. 'solid.bulk.'. If none, empty string
                     * is returned.
                     */
                    static const std::string& GetEnclosingSection();

                private:

                    //! Value of the input parameter stored.
                    return_type value_;

                    //! Whether the value has been called at least once.
                    mutable bool is_used_ = false;
                };


            } // namespace Crtp


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputData/Crtp/Data.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CRTP_x_DATA_HPP_
