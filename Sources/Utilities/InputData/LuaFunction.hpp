/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Aug 2013 11:06:17 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup UtilitiesGroup
 // \addtogroup UtilitiesGroup
 // \{
 */


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HPP_
# define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HPP_

# include <cassert>
# include <string>
# include <functional>
# include <sstream>
# include <memory>

# include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"
# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/LuaOptionFile/Internal/LuaUtilityFunctions.hpp"
# include "Utilities/InputData/Internal/Wrapper/LuaState.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        namespace InputDataNS
        {


            //! \cond IGNORE_BLOCK_IN_DOXYGEN

            template<class T>
            struct LuaFunction;

            //! \endcond IGNORE_BLOCK_IN_DOXYGEN



            /*!
             * \brief High-level functor that handles the call to lua to get the result of a function
             * defined as an input parameter.
             *
             * \internal <b><tt>[internal]</tt></b> This is defined as a child of std::function because it is really one
             * specific instance of it; nevertheless the only feature inherited is result_type alias. The derivation
             * could have been skipped and \code alias ResultTypeT result_type \endcode put instead in this class.
             * \endinternal
             *
             * \tparam ReturnTypeT Return type of the function.
             * \tparam Args Types of the arguments of the function. Currently all the arguments
             * should share the same type, due to a limitation in current Ops implementation. Hopefully Ops
             * will be extended to allow use of different arguments; anyway this interface is ready to face it
             * when possible.
             */
            template<typename ReturnTypeT, typename... Args>
            struct LuaFunction<ReturnTypeT(Args...)>
            {

                /// \name Special members.
                ///@{

                /*!
                 * \brief Default constructor.
                 *
                 * Required only to be able to store an LuaFunction object in a tuple; should not be used otherwise.
                 */
                explicit LuaFunction();

                /*!
                 * \brief Effective constructor.
                 *
                 * \param[in] content The Lua function that is written as a string in the Lua file. For instance:
                 * \verbatim
                 function (x, y, z)
                 return 3. * x + y - z
                 end
                 * \endverbatim
                 */
                explicit LuaFunction(const std::string& content);

                //! Destructor.
                ~LuaFunction() = default;

                //! \copydoc doxygen_hide_copy_constructor
                LuaFunction(const LuaFunction& rhs) = default;

                //! \copydoc doxygen_hide_move_constructor
                LuaFunction(LuaFunction&& rhs) = default;

                //! \copydoc doxygen_hide_copy_affectation
                LuaFunction& operator=(const LuaFunction& rhs) = default;

                //! \copydoc doxygen_hide_move_affectation
                LuaFunction& operator=(LuaFunction&& rhs) = default;


                ///@}

                /*!
                 * \brief Ensure functor behaviour.
                 *
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 *
                 * \return Value computed by the functor.
                 */
                ReturnTypeT operator()(Args... args) const;

                //! Returns the string that defined the Lua function in the first place.
                const std::string& GetString() const noexcept;

            private:

                //! Get internal lua state.
                lua_State* GetInternalState() const noexcept;

            private:

                //! Lua stack that will be used for function computations.
                Internal::LuaNS::LuaState state_;
            };


        } // namespace InputDataNS


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/InputData/LuaFunction.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_LUA_FUNCTION_HPP_
