/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 May 2014 13:27:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_MATRIX_OR_VECTOR_HPP_
# define MOREFEM_x_UTILITIES_x_MATRIX_OR_VECTOR_HPP_

# include <memory>
# include <vector>

# include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
# include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
# include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"


namespace MoReFEM
{


    //! Convenient alias for the type used in MoReFEM for local vectors.
    using LocalVector = xt::xtensor<double, 1, xt::layout_type::row_major>;

    //! Convenient alias for the type used most of the time in MoReFEM for local matrices.
    using LocalMatrix = xt::xtensor<double, 2, xt::layout_type::row_major>;


    //! Convenient enum class.
    enum class IsMatrixOrVector
    {
        vector,
        matrix
    };


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_MATRIX_OR_VECTOR_HPP_
