/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 11:39:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_STRING_x_STRING_HXX_
# define MOREFEM_x_UTILITIES_x_STRING_x_STRING_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        namespace String
        {


            inline void Strip(std::string& string, const std::string& char_to_strip)
            {
                StripLeft(string, char_to_strip);
                StripRight(string, char_to_strip);
            }


            inline bool StartsWith(const std::string& string, const std::string& sequence)
            {
                return 0 == string.compare(0, sequence.size(), sequence);
            }


            template<class StringT>
            std::string Repeat(const StringT& string, unsigned int times)
            {
                std::ostringstream oconv;

                for (unsigned int i = 0; i < times; ++i)
                    oconv << string;

                return oconv.str();
            }



        } // namespace String


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_STRING_x_STRING_HXX_
