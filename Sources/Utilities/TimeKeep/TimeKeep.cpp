/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 May 2014 11:56:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "Utilities/TimeKeep/TimeKeep.hpp"


namespace MoReFEM
{


    TimeKeep::~TimeKeep() = default;
    
    
    const std::string& TimeKeep::ClassName()
    {
        static std::string ret("TimeKeep");
        return ret;
    }
    
    
    TimeKeep::TimeKeep(std::ofstream&& stream)
    : stream_(std::move(stream)),
    init_time_(std::chrono::steady_clock::now()),
    previous_call_time_(init_time_)
    { }


    #ifdef MOREFEM_EXTENDED_TIME_KEEP
    void TimeKeep::PrintTimeElapsed(const std::string& label)
    {
        auto now = std::chrono::steady_clock::now();
        auto time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - init_time_);
        auto time_elapsed_since_previous_call = std::chrono::duration_cast<std::chrono::milliseconds>(now - previous_call_time_);
        
        stream_ << label << "\n\t - Time elapsed = " << std::setw(8) << time_elapsed.count()
        << " milliseconds since beginning and "
        << std::setw(8) << time_elapsed_since_previous_call.count()
        << " milliseconds since previous call." << std::endl;
        
        previous_call_time_ = now;
    }
    #endif // MOREFEM_EXTENDED_TIME_KEEP
    

    std::string TimeKeep::TimeElapsedSinceBeginning() const 
    {
        auto now = std::chrono::steady_clock::now();
        auto time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - init_time_);
        
        unsigned int time = static_cast<unsigned int>(time_elapsed.count());
        
        unsigned int seconds, minutes, hours;
        
        seconds = (time / 1000) % 60;
        minutes = (time / 60000) % 60;
        hours = (time / 3600000) % 24;
        
        std::ostringstream out;
        out << hours << ":" << minutes << ":" << seconds;
        
        return out.str();
    }
    
    
    void TimeKeep::PrintEndProgram()
    {
        stream_ << "End of program after " << TimeElapsedSinceBeginning() << std::endl;
    }
    

} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup
