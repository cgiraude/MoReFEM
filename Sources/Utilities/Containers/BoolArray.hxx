/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 10 Mar 2014 16:05:29 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_BOOL_ARRAY_HXX_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_BOOL_ARRAY_HXX_


namespace MoReFEM
{


    namespace Utilities
    {


        inline BoolArray::operator bool*() const
        {
            return array_;
        }


        inline bool* BoolArray::data()
        {
            return array_;
        }


        inline const bool* BoolArray::data() const
        {
            return array_;
        }


        inline void BoolArray::resize(unsigned int Nelt)
        {
            AllocateSize(Nelt);
        }


        inline bool BoolArray::operator[](unsigned int i) const
        {
            assert(i < Nelt_);
            return array_[i];
        }


        inline bool& BoolArray::operator[](unsigned int i)
        {
            assert(i < Nelt_);
            return array_[i];
        }


        inline unsigned int BoolArray::Size() const
        {
            return Nelt_;
        }


        inline unsigned int BoolArray::size() const
        {
            assert(Nelt_ == Size());
            return Nelt_;
        }


        inline bool BoolArray::empty() const
        {
            return Nelt_ == 0u;
        }


    } // namespace Utilities


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup



#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_BOOL_ARRAY_HXX_
