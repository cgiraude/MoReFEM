//! \file
//
//
//  Print.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_PRINT_HXX_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_PRINT_HXX_


namespace MoReFEM::Internal::PrintNS
{


    template<class PrintPolicyT, class StringT>
    SeparatorFacility<PrintPolicyT, StringT>::SeparatorFacility(std::ostream& stream, const StringT& separator)
    : stream_(stream),
    separator_(separator)
    { }


    template<class PrintPolicyT, class StringT, class T>
    SeparatorFacility<PrintPolicyT, StringT>&
    operator<<(SeparatorFacility<PrintPolicyT, StringT>& facility, const T& value)
    {
        if (facility.is_first_)
            facility.is_first_ = false;
        else
            facility.stream_ << facility.separator_;

        PrintPolicyT::Do(facility.stream_, value);

        return facility;
    }


    template<class StreamT, unsigned int Index, unsigned int Max, class TupleT>
    template<typename StringT>
    void PrintTupleHelper<StreamT, Index, Max, TupleT>
    ::Print(StreamT& stream, const TupleT& t, const StringT& separator)
    {
        using EltTupleType = typename std::tuple_element<Index,TupleT>::type;

        const auto quote =
        Utilities::String::IsString<EltTupleType>::value ? "\"" : "";

        stream << quote << std::get<Index>(t) << quote << (Index + 1 == Max ? "" : separator);
        PrintTupleHelper<StreamT, Index + 1, Max, TupleT>::Print(stream, t, separator);
    };


    template<class StreamT, unsigned int Max, class TupleT>
    template<typename StringT>
    void PrintTupleHelper<StreamT, Max, Max, TupleT>::Print(StreamT&, const TupleT& , const StringT&)
    {
        // Do nothing!
    }


} // namespace MoReFEM::Internal::PrintNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_PRINT_HXX_
