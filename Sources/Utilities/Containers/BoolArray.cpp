/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 10 Mar 2014 16:05:29 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include "Utilities/Containers/BoolArray.hpp"


namespace MoReFEM
{


    namespace Utilities
    {


        BoolArray::BoolArray()
        : array_(nullptr),
        Nelt_(0u)
        { }


        BoolArray::BoolArray(unsigned int Nelt)
        : array_(nullptr),
        Nelt_(Nelt)
        {
            array_ = new bool[Nelt];
        }


        BoolArray::BoolArray(std::initializer_list<bool> list)
        : array_(nullptr),
        Nelt_(static_cast<unsigned int>(list.size()))
        {
            array_ = new bool[Nelt_];

            unsigned int index(0);

            for (auto it = list.begin(), end = list.end(); it != end; ++it)
                array_[index++] = *it;
        }




        BoolArray::BoolArray(const std::vector<bool>& vector)
        : array_(nullptr),
        Nelt_(static_cast<unsigned int>(vector.size()))
        {
            assert(!vector.empty());
            array_ = new bool[Nelt_];

            for (unsigned int i = 0; i < Nelt_; ++i)
                array_[i] = vector[i];
        }
        


        void BoolArray::AllocateSize(unsigned int Nelt)
        {
            assert(Nelt_ == 0u);
            Nelt_ = Nelt;
            array_ = new bool[Nelt];
        }


        BoolArray::~BoolArray()
        {
            delete[] array_;
        }


        BoolArray::BoolArray(const BoolArray& rhs)
        : array_(nullptr),
        Nelt_(rhs.Nelt_)
        {
            array_ = new bool[Nelt_];
            for (unsigned int i = 0; i < Nelt_; ++i)
                array_[i] = rhs.array_[i];
        }
        
        

        BoolArray& BoolArray::operator=(const BoolArray& rhs)
        {
            if (this != &rhs)
            {
                Nelt_ = rhs.Nelt_;
                array_ = new bool[Nelt_];
                for (unsigned int i = 0; i < Nelt_; ++i)
                    array_[i] = rhs.array_[i];
            }

            return *this;
        }
        

        void BoolArray::Print(std::ostream& out) const
        {
            if (Nelt_ == 0)
            {
                out << "{ }" << std::endl;
                return;
            }

            out << "{ ";

            for (unsigned int i = 0; i < Nelt_ - 1; ++i)
                out << array_[i] << ", ";

            out << array_[Nelt_ - 1] << " }";
        }



        std::vector<bool> VectorFromBoolArray(const BoolArray& array)
        {
            unsigned int Nelt = array.Size();

            std::vector<bool> ret(Nelt);

            for (unsigned int i = 0; i < Nelt; ++i)
                ret[i] = array[i];

            return ret;
        }


    } // namespace Utilities
    
    
} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup

