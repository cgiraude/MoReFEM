//! \file
//
//
//  Normal.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_NORMAL_HXX_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_NORMAL_HXX_


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<class ElementTypeT>
    void Normal::Do(std::ostream& stream, ElementTypeT&& element)
    {
        stream << element;
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_NORMAL_HXX_
