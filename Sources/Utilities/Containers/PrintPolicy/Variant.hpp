//! \file
//
//
//  Variant.hpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_VARIANT_HPP_
# define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_VARIANT_HPP_

# include <variant>
# include <ostream>

# include "Utilities/Miscellaneous.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{


    /*!
     * \brief Policy to handle the case a container element might be a std::variant.
     *
     * If so, a visitor is used to print it (so it does assume all types within the std::variant are directly printable).
     *
     * This policy assumes the container is not associative.
     */
    struct Variant
    {

    public:

        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);

    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


# include "Utilities/Containers/PrintPolicy/Variant.hxx"


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_VARIANT_HPP_
