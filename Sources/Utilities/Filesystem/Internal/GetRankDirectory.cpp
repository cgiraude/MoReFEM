//! \file 
//
//
//  Directory.cpp
//  MoReFEM
//
//  Created by sebastien on 01/08/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include <memory>

#include "Utilities/Filesystem/Internal/GetRankDirectory.hpp"


namespace MoReFEM::Internal::FilesystemNS
{

    
    namespace // anonymous
    {

        
        using Directory = ::MoReFEM::FilesystemNS::Directory;

        using behaviour = ::MoReFEM::FilesystemNS::behaviour;


    } // namespace anonymous


    Directory GetRankDirectory(const Directory& root_directory,
                               unsigned int rank,
                               const char* invoking_file, int invoking_line)
    {
        assert(root_directory.GetMpi().IsRootProcessor());
        assert(root_directory.IsWithRank());

        // I usually don't like the '..' trick but exceptionally it is the best way to do it without complexifying
        // further Directory API.
        Directory one_step_above(root_directory,
                                 "..",
                                 __FILE__, __LINE__,
                                 std::make_unique<behaviour>(behaviour::read));

        Directory ret(one_step_above,
                      "Rank_" + std::to_string(rank),
                      invoking_file, invoking_line);

        return ret;
    }


} // namespace MoReFEM::Internal::FilesystemNS
