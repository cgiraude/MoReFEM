//! \file 
//
//
//  Directory.cpp
//  MoReFEM
//
//  Created by sebastien on 01/08/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include <cassert>
#include <deque>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Containers/Print.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"


namespace MoReFEM::FilesystemNS
{


    namespace // anonymous
    {


        enum class ask_status
        {
            no_preexisting = 0,
            remove_yes,
            remove_no
        };

        ask_status AskCaseHelper(const std::string& message);

        ask_status AskCaseRootProcessor(const std::string& wildcard_path,
                                        const std::vector<int>& result);


        std::string ComputeWildcardPath(const Directory& directory);

        std::string XCodeWarning();
        

    } // namespace anonymous
    

    Directory::Directory(const Wrappers::Mpi& mpi,
                         const std::string& a_path,
                         behaviour directory_behaviour,
                         const char* invoking_file, int invoking_line,
                         add_rank do_add_rank)
    : mpi_(&mpi),
    directory_behaviour_(directory_behaviour),
    with_rank_(do_add_rank == add_rank::yes)
    {
        std::ostringstream oconv;
        oconv << a_path;

        switch(do_add_rank)
        {
            case add_rank::no:
                break;
            case add_rank::yes:
                oconv << "/Rank_" << mpi.GetRank<unsigned int>();

                break;
        }

        oconv << '/';

        path_ = oconv.str();

        Construct(invoking_file, invoking_line);
    }


    Directory::Directory(const std::string& path,
                         behaviour directory_behaviour,
                         const char* invoking_file, int invoking_line)
    : mpi_(nullptr),
    directory_behaviour_(directory_behaviour),
    with_rank_(false)
    {
        std::ostringstream oconv;
        oconv << path << '/';
        path_ = oconv.str();

        Construct(invoking_file, invoking_line);
    }


    void Directory::Construct(const char* invoking_file, int invoking_line) const
    {
        // First tackle - if relevant - the 'ask' case policy:
        // - The question of overwriting can only be answered on the root rank.
        // - So the informations about the existence or not of the path for a given rank must be first send on root
        // processor.
        // - Root processor asks whether the directories must be overwritten or not. If 'no', a GracefulExit occurs.
        // - Then root processor must tell the other processors to remove the directories.
        // All the other policies may be handled without interprocessor communication.
        if (directory_behaviour_ == behaviour::ask)
        {
            if (IsMpi())
                CollectAnswer(invoking_file, invoking_line);
            else
            {
                std::ostringstream oconv;
                oconv << "Directory '" << GetPath() << "' already exist. Do you want to remove it? [y/n]";
                oconv << XCodeWarning();
                AskCaseHelper(oconv.str());
            }
        }

        // At this point, each rank may do its own bidding.
        if (Advanced::FilesystemNS::DirectoryNS::DoExist(path_))
        {
            switch(directory_behaviour_)
            {
                case behaviour::ask:
                case behaviour::overwrite:
                        Advanced::FilesystemNS::DirectoryNS::Remove(path_, invoking_file, invoking_line);
                    assert(!Advanced::FilesystemNS::DirectoryNS::DoExist(path_));
                    Advanced::FilesystemNS::DirectoryNS::Create(path_, invoking_file, invoking_line);
                    break;
                case behaviour::ignore:
                    break;
                case behaviour::quit:
                {
                    std::cout << "Directory '" << path_ << "' already exists; an abortion of the program is therefore "
                    "scheduled." << std::endl;
                    throw ExceptionNS::GracefulExit(invoking_file, invoking_line);
                }
                case behaviour::read:
                    // Do nothing: we expects it to exist!
                    break;
                case behaviour::create:
                {
                    std::ostringstream oconv;
                    oconv << "Directory " << path_ << " already exists whereas it was expected not to (maybe the "
                    "behaviour set at call site was not the proper one).";
                    throw Exception(oconv.str(), invoking_file, invoking_line);
                }
            }
        }
        else
        {
            switch(directory_behaviour_)
            {
                case behaviour::ask:
                case behaviour::overwrite:
                case behaviour::ignore:
                case behaviour::quit:
                case behaviour::create:
                    Advanced::FilesystemNS::DirectoryNS::Create(path_, invoking_file, invoking_line);
                    break;
                case behaviour::read:
                    if (!Advanced::FilesystemNS::DirectoryNS::DoExist(path_))
                    {
                        std::ostringstream oconv;
                        oconv << "Directory '" << path_ << "' was expected to exist and could not be found.";
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }
                    break;

            }
        }
    }


    void Directory::CollectAnswer(const char* invoking_file, int invoking_line) const
    {
        assert(directory_behaviour_ == behaviour::ask);

        decltype(auto) mpi = GetMpi();
        const auto rank = mpi.GetRank<int>();

        const bool do_path_exist = Advanced::FilesystemNS::DirectoryNS::DoExist(path_);

        std::vector<int> sent_data { do_path_exist ? rank : -1 };
        std::vector<int> gathered_data;

        mpi.Gather(sent_data, gathered_data);

        ask_status ret = ask_status::no_preexisting;

        if (mpi.IsRootProcessor())
        {
            if (std::any_of(gathered_data.cbegin(),
                            gathered_data.cend(),
                            [](auto i)
                            {
                                return i != -1;
                            }))
            {
                ret = AskCaseRootProcessor(ComputeWildcardPath(*this),
                                           gathered_data);
            }
        }

        mpi.Barrier();

        // Tell the ranks whether they may remove the directory.
        std::vector<int> do_remove(1ul);

        if (mpi.IsRootProcessor())
            do_remove[0] = EnumUnderlyingType(ret);

        mpi.Broadcast(do_remove);

        if (do_remove.back() == EnumUnderlyingType(ask_status::remove_no))
            throw ExceptionNS::GracefulExit(invoking_file, invoking_line);
    }



    std::ostream& operator<<(std::ostream& out, const Directory& directory)
    {
        out << directory.GetPath();
        return out;
    }


    std::string Directory::AddFile(const std::string& filename) const
    {
        std::ostringstream oconv;
        oconv << GetPath() << filename;
        return oconv.str();
    }


    void Directory::SetBehaviour(behaviour new_behaviour)
    {
        directory_behaviour_ = new_behaviour;
    }


    std::ostream& operator<<(std::ostream& out, behaviour rhs)
    {
        switch(rhs)
        {
            case behaviour::overwrite:
                out << "overwrite";
                break;
            case behaviour::ask:
                out << "ask";
                break;
            case behaviour::quit:
                out << "quit";
                break;
            case behaviour::ignore:
                out << "ignore";
                break;
            case behaviour::read:
                out << "read";
                break;
            case behaviour::create:
                out << "create";
                break;
        }

        return out;
    }


    void Directory::AddSubdirectory(const std::string& subdirectory, const char* invoking_file, int invoking_line)
    {
        std::ostringstream oconv;
        oconv << GetPath() << subdirectory << '/';
        path_ = oconv.str();

        Construct(invoking_file, invoking_line);
    }


    namespace // anonymous
    {


        std::string RankList(const std::vector<int>& result)
        {
            std::vector<int> helper;
            helper.reserve(result.size());

            std::copy_if(result.cbegin(),
                         result.cend(),
                         std::back_inserter(helper),
                         [](auto i)
                         {
                             return i != -1;
                         });

            std::ostringstream oconv;
            Utilities::PrintContainer<>::Do(helper, oconv);
            return oconv.str();
        }


        ask_status AskCaseRootProcessor(const std::string& wildcard_path,
                                        const std::vector<int>& result)
        {
            std::ostringstream oconv;

            const std::string rank_list = RankList(result);

            oconv << "Directories '" << wildcard_path << "' already exist for ranks " << rank_list
            << ". Do you want to remove them? [y/n]"<< std::endl;
            oconv << XCodeWarning();

            return AskCaseHelper(oconv.str());
        }


        ask_status AskCaseHelper(const std::string& message)
        {
            std::string answer;

            while (answer != "y" && answer != "n")
            {
                do
                {
                    if (!std::cin)
                    {
                        std::cin.clear(); // clear the states of std::cin, putting it back to `goodbit`.
                        std::cin.ignore(10000, '\n'); // clean-up what might remain in std::cin before using it again.
                    }

                    std::cout << message << std::endl;

                    std::cin >> answer;

                    #ifndef NDEBUG
                    std::cout << "\t answer read = |" << answer << '|' << std::endl;
                    #endif // NDEBUG


                } while (!std::cin);
            }

            return answer == "y" ? ask_status::remove_yes : ask_status::remove_no;
        }



        std::string ComputeWildcardPath(const Directory& directory)
        {
            decltype(auto) mpi = directory.GetMpi();
            assert(mpi.IsRootProcessor());

            auto ret = directory.GetPath();

            auto Nmodif = Utilities::String::Replace("Rank_" + std::to_string(mpi.GetRank<int>()),
                                                     "Rank_*",
                                                     ret);

            assert(Nmodif == 1 && "Should be relevant only for directory created with add_rank::yes.");
            static_cast<void>(Nmodif);

            return ret;
        }


        std::string XCodeWarning()
        {
            return "\tNote for XCode user: doesn't work in XCode  in parallel.. You should consider "
            "--overwrite_directory flag in the arguments on command line.";
        }


    } // namespace anonymous

    
} // namespace MoReFEM::FilesystemNS


namespace MoReFEM::Internal::FilesystemNS
{


    namespace // anonymous
    {


        using Directory = ::MoReFEM::FilesystemNS::Directory;


    } // namespace anonymous



    void CheckForSubdirectoryConstructor(const Directory& parent_directory,
                                         const char* invoking_file, int invoking_line)
    {
        if (!::MoReFEM::Advanced::FilesystemNS::DirectoryNS::DoExist(parent_directory.GetPath()))
        {
            std::ostringstream oconv;
            oconv << "Directory '" << parent_directory.GetPath() << "' couldn't be found whereas we were trying to build a "
            "subdirectory from it (so the directory has been created at some point and then removed).";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }
    }


} // namespace MoReFEM::Internal::FilesystemNS

