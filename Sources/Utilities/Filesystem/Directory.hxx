//! \file
//
//
//  Directory.hxx
//  MoReFEM
//
//  Created by sebastien on 01/08/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_
# define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_


namespace MoReFEM::FilesystemNS
{


    template<class StringT>
    Directory::Directory(const Directory& parent_directory,
                         const StringT& subdirectory,
                         const char* invoking_file, int invoking_line,
                         std::unique_ptr<behaviour> directory_behaviour_ptr)
    : mpi_(parent_directory.GetMpiPtr()),
    directory_behaviour_(directory_behaviour_ptr == nullptr ? parent_directory.GetBehaviour() : *directory_behaviour_ptr),
    with_rank_(parent_directory.IsWithRank())
    {
        Internal::FilesystemNS::CheckForSubdirectoryConstructor(parent_directory, invoking_file, invoking_line);

        std::ostringstream oconv;
        oconv << parent_directory.GetPath() << subdirectory << '/';
        path_ = oconv.str();

        Construct(invoking_file, invoking_line);
    }


    template<class StringT>
    Directory::Directory(const Directory& parent_directory,
                         const std::vector<StringT>& subdirectories,
                         const char* invoking_file, int invoking_line,
                         std::unique_ptr<behaviour> directory_behaviour_ptr)
    : mpi_(parent_directory.GetMpiPtr()),
    directory_behaviour_(directory_behaviour_ptr == nullptr ? parent_directory.GetBehaviour() : *directory_behaviour_ptr),
    with_rank_(parent_directory.IsWithRank())
    {
        Internal::FilesystemNS::CheckForSubdirectoryConstructor(parent_directory, invoking_file, invoking_line);

        std::ostringstream oconv;
        oconv << parent_directory.GetPath();
        Utilities::PrintContainer<>::Do(subdirectories, oconv, '/', "", "");
        oconv << '/';
        path_ = oconv.str();

        Construct(invoking_file, invoking_line);
    }


    inline const std::string& Directory::GetPath() const noexcept
    {
        return path_;
    }


    inline const Wrappers::Mpi& Directory::GetMpi() const noexcept
    {
        assert(IsMpi());
        return *mpi_;
    }


    inline bool Directory::IsMpi() const noexcept
    {
        return mpi_ != nullptr;
    }


    inline const Wrappers::Mpi* Directory::GetMpiPtr() const noexcept
    {
        return mpi_; // might be nullptr
    }


    inline behaviour Directory::GetBehaviour() const noexcept
    {
        return directory_behaviour_;
    }


    inline Directory::operator const std::string& () const noexcept
    {
        return GetPath();
    }


    inline bool Directory::IsWithRank() const noexcept
    {
        return with_rank_;
    }


} // namespace MoReFEM::FilesystemNS





#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_
