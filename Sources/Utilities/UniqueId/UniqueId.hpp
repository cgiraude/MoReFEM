/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Nov 2014 11:12:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HPP_
# define MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HPP_


# include <set>
# include <sstream>

# include "Utilities/Numeric/Numeric.hpp"
# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace UniqueIdNS
    {


        /*!
         * \brief Whether the unique id is given in constructor or generated automatically by incrementing a static counter.
         *
         * Details for each mode:
         * - \a automatic: Each time a new instance of \a DerivedT is created, it is assigned a value (which is the number
         * of already created instance). This value is accessible through the method GetUniqueId().
         * - \a manual: The ID is given by the user, and there is a check the id remains effectively unique.
         */
        enum class AssignationMode
        {
            automatic,
            manual
        };


        /*!
         * \brief Whether it is possible to build an object without assigning to it a unique id.
         *
         * \attention Relevant only for a \a UniqueId which \a AssignationMode is manual.
         */
        enum class DoAllowNoId
        {
            no,
            yes
        };



    } // namespace UniqueIdNS



    namespace Crtp
    {


        /*!
         * \brief This CRTP class defines a unique ID for each object of the DerivedT class.
         *
         * \tparam AssignationModeT See \a AssignationMode enum.
         * \tparam DoAllowNoIdT See \a DoAllowNoId enum.
         */
        template
        <
            class DerivedT,
            UniqueIdNS::AssignationMode AssignationModeT = UniqueIdNS::AssignationMode::automatic,
            UniqueIdNS::DoAllowNoId DoAllowNoIdT = UniqueIdNS::DoAllowNoId::no
        >
        class UniqueId
        {


        public:

            /// \name Special members.
            ///@{

            //! Constructor for AssignationMode::automatic.
            explicit UniqueId();

            //! Constructor for AssignationMode::manual.
            //! \param[in] id Unique id to assign. If invalid (e.g. already existing) an exception is thrown.
            explicit UniqueId(unsigned int id);

            //! Constructor for AssignationMode::manual when no id is to be assigned (only if DoAllowNoIdT is yes).
            explicit UniqueId(std::nullptr_t);

            //! Destructor.
            ~UniqueId() = default;

            //! \copydoc doxygen_hide_copy_constructor
            UniqueId(const UniqueId& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            UniqueId(UniqueId&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            UniqueId& operator=(const UniqueId& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            UniqueId& operator=(UniqueId&& rhs) = delete;


            ///@}

        public:

            /*!
             * \brief Get the value of the internal unique ID.
             *
             * \return Unique id.
             */
            unsigned int GetUniqueId() const;


        private:

            /*!
             * \brief If AssignationMode is automatic, generates a new unique identifier.
             *
             * \return The new unique id, which is just an increment from the previously assigned one.
             */
            static unsigned int AssignUniqueId();

        private:

            /*!
             * \brief The value of the unique id for the current \a DerivedT object.
             */
            const unsigned int unique_id_;

            /*!
             * \brief Returns the list of already known ids.
             *
             * This is relevant only for manual assignation.
             *
             * \return List of already known ids
             */
            static std::set<unsigned int>& GetNonCstUniqueIdList();

        };


    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#include "Utilities/UniqueId/UniqueId.hxx"


#endif // MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HPP_
