/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Nov 2014 11:12:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HXX_
# define MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HXX_


namespace MoReFEM
{


    namespace Crtp
    {


        template<class DerivedT, UniqueIdNS::AssignationMode AssignationModeT, UniqueIdNS::DoAllowNoId DoAllowNoIdT>
        unsigned int UniqueId<DerivedT, AssignationModeT, DoAllowNoIdT>
        ::AssignUniqueId()
        {
            static_assert(AssignationModeT == UniqueIdNS::AssignationMode::automatic,
                          "This method makes sense only in automatic id assignation.");

            // Initial value so that first unique id assigned is 0.
            static unsigned int counter = static_cast<unsigned int>(-1);
            return ++counter;
        }


        template<class DerivedT, UniqueIdNS::AssignationMode AssignationModeT, UniqueIdNS::DoAllowNoId DoAllowNoIdT>
        UniqueId<DerivedT, AssignationModeT, DoAllowNoIdT>::UniqueId()
        : unique_id_(AssignUniqueId())
        {
            static_assert(AssignationModeT == UniqueIdNS::AssignationMode::automatic,
                          "This constructor makes sense only in automatic id assignation.");
        }


        template<class DerivedT, UniqueIdNS::AssignationMode AssignationModeT, UniqueIdNS::DoAllowNoId DoAllowNoIdT>
        UniqueId<DerivedT, AssignationModeT, DoAllowNoIdT>::UniqueId(unsigned int id)
        : unique_id_(id)
        {
            if (id == NumericNS::UninitializedIndex<unsigned int>())
                throw Exception("Identifier " + std::to_string(id) + " is reserved; please choose any other unsigned int!",
                                __FILE__, __LINE__);

            static_assert(AssignationModeT == UniqueIdNS::AssignationMode::manual,
                          "This constructor makes sense only in manual id assignation.");

            if (GetNonCstUniqueIdList().insert(id).second == false)
            {
                std::ostringstream oconv;
                oconv << "The id " << id << " has already been given to another "
                << static_cast<DerivedT&>(*this).ClassName() << " object.";

                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

        }


        template<class DerivedT, UniqueIdNS::AssignationMode AssignationModeT, UniqueIdNS::DoAllowNoId DoAllowNoIdT>
        UniqueId<DerivedT, AssignationModeT, DoAllowNoIdT>::UniqueId(std::nullptr_t)
        : unique_id_(NumericNS::UninitializedIndex<unsigned int>())
        {
            static_assert(AssignationModeT == UniqueIdNS::AssignationMode::manual,
                          "This constructor makes sense only in manual id assignation.");

            static_assert(DoAllowNoIdT == UniqueIdNS::DoAllowNoId::yes,
                          "DerivedT class must accept objects without identifiers!");
        }


        template<class DerivedT, UniqueIdNS::AssignationMode AssignationModeT, UniqueIdNS::DoAllowNoId DoAllowNoIdT>
        inline unsigned int UniqueId<DerivedT, AssignationModeT, DoAllowNoIdT>::GetUniqueId() const
        {
            # ifndef NDEBUG
            // In the case no unique id is allowed, the default UninitializedIndex is given if no id were provided.
            if (UniqueIdNS::DoAllowNoId::no == DoAllowNoIdT)
                assert(unique_id_ != NumericNS::UninitializedIndex<unsigned int>());
            # endif // NDEBUG

            return unique_id_;
        }


        template<class DerivedT, UniqueIdNS::AssignationMode AssignationModeT, UniqueIdNS::DoAllowNoId DoAllowNoIdT>
        std::set<unsigned int>& UniqueId<DerivedT, AssignationModeT, DoAllowNoIdT>::GetNonCstUniqueIdList()
        {
            static std::set<unsigned int> ret;
            return ret;
        }



    } // namespace Crtp


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HXX_
