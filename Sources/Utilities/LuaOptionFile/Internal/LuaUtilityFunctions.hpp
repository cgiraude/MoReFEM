//! \file
//
//
//  LuaDumpStack.hpp
//  MoReFEM
//
//  Created by sebastien on 26/06/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_INTERNAL_x_LUA_UTILITY_FUNCTIONS_HPP_
# define MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_INTERNAL_x_LUA_UTILITY_FUNCTIONS_HPP_

# include <iostream>

# include "Utilities/Type/PrintTypeName.hpp"
# include "Utilities/Miscellaneous.hpp"
# include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    /*!
     * \brief Dump onto \a stream the current content of a Lua stack.
     *
     * Said stack is left unchanged by this function.
     *
     * This function is adapted from the one suggested in https://www.lua.org/pil/24.2.3.html.
     *
     * \param[in] state The Lua stack to study.
     * \param[in,out] stream The output stream onto which the results must be dumped.
     */
    void LuaStackDump(lua_State* state, std::ostream& stream = std::cout);

    /*!
     * \brief Put \a entry_name on top of Lua stack.
     *
     * If \a entry_name is a simple variable, it calls 'lua_getglobal' once. But if
     * \a entry_name is encapsulated in a table, this method iterates until it finds
     * the variable.
     * \param[in] entry_name Name of the entry to be put on top of the stack.
     * \param[in,out] state Stack onto which new content is put.
     */
    void PutOnStack(lua_State* state, const std::string& entry_name);


    /*!
     * \brief Push on stack the value related to the last entry put on stack.
     *
     * This method is only used along with \a Apply, to give to Lua the values from C++ types.
     *
     * \param[in] value Value to push on Lua stack.
     *
     * \tparam T C++ type of the value. It might be:
     * - Integral types.
     * - Floating point types.
     * - bool
     * - std::string
     * - A std::tuple with one of the above type inside (as a matter of fact it is always a tuple that is called
     * in the first place; this tuple will recursively call the version on single types).
     *
     * \param[in,out] state Stack onto which new content is pushed.
     */
    template<typename T>
    void PushOnStack(lua_State* state, T value);

    /*!
     * \brief Take a value from Lua stack and convert it into a C++ type.
     *
     * \tparam T C++ type into which we want to convert the Lua value.
     *
     * \param[in] lua_index Lua index of the element on Lua stack. Negative values means from top to bottom; Lua arrays
     * starts as 1 (so default value takes the value on the top on the stack).
     * \param[in,out] state Stack from which new content is pulled.
     */
    template<typename T>
    T PullFromStack(lua_State* state, int lua_index = -1);


} // namespace MoReFEM::Internal::LuaNS


#include "Utilities/LuaOptionFile/Internal/LuaUtilityFunctions.hxx"


#endif // MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_INTERNAL_x_LUA_UTILITY_FUNCTIONS_HPP_
