/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 1 Mar 2018 15:22:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HPP_
# define MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HPP_

# include <cassert>
# include <memory>
# include <vector>
# include <map>
# include <sstream>
# include <iostream>
# include <tuple>
# include <variant>

# include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"

# include "Utilities/String/Traits.hpp"
# include "Utilities/Exceptions/Exception.hpp"
# include "Utilities/Miscellaneous.hpp"
# include "Utilities/Type/PrintTypeName.hpp"
# include "Utilities/LuaOptionFile/Internal/LuaUtilityFunctions.hpp"


namespace MoReFEM
{


    namespace Utilities::InputDataNS
    {


        template<class T>
        struct LuaFunction;


    } // namespace Utilities::InputDataNS


    /*!
     * \brief A class to load input parameter data stored in a Lua file.
     *
     * This class is very inspired from https://gitlab.com/libops, which was used for a long time in MoReFEM. The
     * reason I no longer use it directly is that I needed extensions (e.g. handling of map containers) I wasn't
     * comfortable in adding directly in the sometimes weird libops logic. Current class is not a mere extension of
     * libops:
     * - Some libops features aren't present here, such as the reading of individual values of an array. The reason is
     * that LuaOptionFile is used only by higher-level InputData which doesn't need this feature.
     * - On the other hand, (ordered) associative containers are now supported.
     * - Using a Lua function will be more efficient: libops reallocates a std::vector at each call, whereas I'm using
     * instead variadic parameters. So I do not have a limit of the number of parameters in a Lua function (but they
     * still should all be of the same type).
     *
     * It must be noted that I'm not familiar with Lua and Lua code has been mostly been left untouched.
     *
     * \attention This class requires Lua 5.3, whereas libops expected 5.1. The reason is that I wanted lua_isinteger
     * not yet in 5.1, whereas 5.2+ made compilation errors with part of libops code I didn't retain.
     */
    class LuaOptionFile
    {

    public:

        //! \copydoc doxygen_hide_alias_self
        using self = LuaOptionFile;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


    public:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] filename Path of the Lua file to load.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit LuaOptionFile(const std::string& filename, const char* invoking_file, int invoking_line);

        //! Destructor.
        ~LuaOptionFile();

        //! \copydoc doxygen_hide_copy_constructor
        LuaOptionFile(const LuaOptionFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LuaOptionFile(LuaOptionFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LuaOptionFile& operator=(const LuaOptionFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LuaOptionFile& operator=(LuaOptionFile&& rhs) = delete;

        ///@}

    public:

        /*!
         * \class doxygen_hide_lua_option_file_constraint
         *
         * \param[in] constraint constraint that the entry value must satisfy. The value is named 'v' by convention; the
         * constraint might be:
         * - A direct comparison, e.g. 'v > 10', 'v <= 3'. or  'v == "foo"'
         * - Checking it is among a list of choices, e.b. 'value_in(v, { "constant", "piecewise_per_domain", "function"} )'
         * - Empty! (i.e. "")
         *
         * An exception is thrown is the constraint is not fulfilled.
         */


        /*!
         * \class doxygen_hide_lua_option_file_read
         *
         * \param[in] entry_name name of the entry.
         * \copydoc doxygen_hide_lua_option_file_constraint
         * \copydoc doxygen_hide_invoking_file_and_line
         */

        /*!
         * \class doxygen_hide_variant_selector_arg
         *
         * \tparam VariantSelectorT Type of \a variant_selector, which is assumed to be a specialization of
         * std::variant OR a std::vector of several such variants (this is clearly an internal level feature and
         * hopefully a end-user shouldn't deal with it directly).
         *
         * \param[in] variant_selector This is the variant (or a vector of variants) which has been properly
         * initialized with the right type. For instance, for a \a Parameter from the input data file, if the field
         * that address the nature says we expect a piecewise-constant per domain, \a variant_selector is expected
         * to have been initialized with a value of this type (the value itself is not yet the one written in the
         * input data - this is a preliminary step before the right value is assigned with the use of a std::visit).
         * The input data objects that make use of this are expected to define a static method called Selector which
         * initializes correctly the variant_selector; see for instance Advanced::InputDataNS::ParamNS::Value class.
         * When \a variant_selector are completely irrelevant, convention is to pass nullptr (and set appropriately
         * \a VariantSelectorT to std::nullptr_t).
         */

        /*!
         * \brief Retrieves a value from the configuration file.
         *
         * \tparam T C++ type of the value read in the Lua file. Possible values are:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         * - std::vector<> (with the template parameter chosen among the ones mentioned above)
         * - std::map<> (with the template parameters for key and values chosen among the ones mentioned above).
         * - A specialization of Utilities::InputDataNS::LuaFunction
         * - A std::variant, for which the relevant alternatives are all the choices above.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         * \copydoc doxygen_hide_variant_selector_arg
         *
         * \param[out] ret The value of the entry.
         */
        template<class T, class VariantSelectorT = std::nullptr_t>
        void Read(const std::string& entry_name,
                  const std::string& constraint,
                  T& ret,
                  const char* invoking_file, int invoking_line,
                  VariantSelectorT&& variant_selector = nullptr);

    public:

        /*!
         * \brief Get the list of all entries in the Lua file.
         *
         * Entries are in the format "section1.subsection.subsubsection.entry".
         *
         * \return List of all entries found.
         */
        const std::vector<std::string>& GetEntryKeyList() const noexcept;

    private:

        /*!
         * \brief Open a Lua file and load it.
         *
         * The file is examined in a first step to determine all the entry keys.
         * Then luaL_dofile is called on it.
         *
         * \param[in] filename Path of the Lua file to load.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        void Open(const std::string& filename, const char* invoking_file, int invoking_line);


    private:

        //! Get the Lua state.
        lua_State* GetNonCstLuaState() noexcept;

        
        /*!
         * \brief Convert the \a index_in_lua_stack -th element into a C++ type.
         *
         * \tparam T C++ type into which Lua data will be converted. Possible choices are:
         * - Integral types.
         * - Floating point types.
         * - bool
         * - std::string
         *
         * \tparam StringT The universal reference trick; read it as either const std::string& or std::string&&.
         *
         * \param[in] index_in_lua_stack Index to indicate which element in Lua stack must be converted. Usually -1 or
         * -2.
         * \param[in] entry_name Name of the entry for which the converion occurs. Only used to display meaningful
         * error message,
         * \copydoc doxygen_hide_invoking_file_and_line
         * \copydoc doxygen_hide_variant_selector_arg
         *
         * \param[out] ret The value in a C++ type.
         */
        template<class T, class StringT, class VariantSelectorT>
        void Convert(int index_in_lua_stack, StringT&& entry_name,
                     T& ret,
                     const char* invoking_file, int invoking_line,
                     const VariantSelectorT& variant_selector);

        //! Clears the Lua stack.
        void ClearStack();

        /*!
         * \brief Checks whether an entry satisfies a constraint.
         *
         * \tparam StringT The universal reference trick; read it as either const std::string& or std::string&&.
         *
         * \param[in] entry_name The entry for which the constraint is to be checked.
         * \copydoc doxygen_hide_lua_option_file_constraint
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         */
        template<class StringT>
        void CheckConstraint(StringT&& entry_name, const std::string& constraint,
                             const char* invoking_file, int invoking_line);


    private:

        /*!
         * \brief Sub-function of Read when T is a std::vector.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         * \copydoc doxygen_hide_variant_selector_arg
         * \param[out] vector The resulting std::vector with all its value properly filled from Lua data.
         */
        template<class T, class VariantSelectorT>
        void ReadVector(const std::string& entry_name, const std::string& constraint,
                        const char* invoking_file, int invoking_line, T& vector,
                        VariantSelectorT&& variant_selector);



        /*!
         * \brief Sub-function of Read when T is a std::map.
         *
         * \copydoc doxygen_hide_lua_option_file_read
         * \copydoc doxygen_hide_variant_selector_arg
         * \param[out] map The resulting std::map with all its value properly filled from Lua data.
         */
        template<class T, class VariantSelectorT>
        void ReadMap(const std::string& entry_name, const std::string& constraint,
                     const char* invoking_file, int invoking_line, T& map,
                     VariantSelectorT&& variant_selector);

        /*!
         * \brief Return the path of the Lua file which was interpreted.
         *
         * \return Path to the Lua file.
         */
        const std::string& GetFilename() const noexcept;

        /*!
         * \brief Set the list of all entry keys.
         *
         * This method should only be called once.
         *
         * \param[in] entry_key_list List of all the entries in the Lua file to be set.
         */
        void SetEntryKeyList(std::vector<std::string>&& entry_key_list);


    private:


        //! Lua state.
        lua_State* state_ = nullptr;

        /*!
         * \brief List of all entry keys found in the Lua file.
         *
         * \internal This list was provided by manual parsing, not by Lua directly.
         * \endinternal
         */
        std::vector<std::string> entry_key_list_;

        //! Filename read.
        std::string filename_ = "";

    };


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


# include "Utilities/LuaOptionFile/LuaOptionFile.hxx"


#endif // MOREFEM_x_UTILITIES_x_LUA_OPTION_FILE_x_LUA_OPTION_FILE_HPP_
