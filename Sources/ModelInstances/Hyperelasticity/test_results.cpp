/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE model_hyperelasticity
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "ModelInstances/Hyperelasticity/InputData.hpp"

using namespace MoReFEM;

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"
#include "Test/Tools/Fixture/Environment.hpp"


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par);


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(sequential, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq");
}


BOOST_FIXTURE_TEST_CASE(mpi4, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir, output_dir;

        /* BOOST_REQUIRE_NO_THROW */(root_dir = environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */(output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(root_dir));
        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(output_dir));

        std::string ref_dir_path = root_dir + "/Sources/ModelInstances/Hyperelasticity/ExpectedResults/";
        std::string obtained_dir_path = output_dir + std::string("/") + seq_or_par
            + std::string("/MidpointHyperelasticity/")  + std::string("/Rank_0");

        FilesystemNS::Directory ref_dir(ref_dir_path,
                                        FilesystemNS::behaviour::read,
                                        __FILE__, __LINE__);

        FilesystemNS::Directory obtained_dir(obtained_dir_path,
                                             FilesystemNS::behaviour::read,
                                             __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);


        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        std::ostringstream oconv;

        for (auto i = 0ul; i <= 20; ++i)
        {
            oconv.str("");
            oconv << "displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareEnsightFiles(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__);
        }

    }
    

} // namespace anonymous


