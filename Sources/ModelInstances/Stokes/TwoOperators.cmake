add_library(MoReFEM4Stokes_2_operators_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4Stokes_2_operators_lib
    PRIVATE
        "${CMAKE_CURRENT_LIST_DIR}/Model.cpp" / 
        "${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp"
	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Model.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/Model.hxx" /
		"${CMAKE_CURRENT_LIST_DIR}/InputData.hpp" /
 		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx"
)

target_compile_definitions(MoReFEM4Stokes_2_operators_lib PUBLIC MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS)

target_link_libraries(MoReFEM4Stokes_2_operators_lib
                      ${ALL_LOAD_BEGIN_FLAG}                    
                      ${MOREFEM_MODEL}
                      ${ALL_LOAD_END_FLAG})

add_executable(MoReFEM4Stokes_2_operators ${CMAKE_CURRENT_LIST_DIR}/main.cpp)

target_compile_definitions(MoReFEM4Stokes_2_operators PUBLIC MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS)

target_link_libraries(MoReFEM4Stokes_2_operators
                      MoReFEM4Stokes_2_operators_lib)
apply_lto_if_supported(MoReFEM4Stokes_2_operators)

add_executable(MoReFEM4Stokes_2_operatorsEnsightOutput ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output_monolithic.cpp)

target_compile_definitions(MoReFEM4Stokes_2_operatorsEnsightOutput PUBLIC MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS)

target_link_libraries(MoReFEM4Stokes_2_operatorsEnsightOutput
                      ${MOREFEM_POST_PROCESSING})

morefem_install(MoReFEM4Stokes_2_operators MoReFEM4Stokes_2_operators_lib MoReFEM4Stokes_2_operatorsEnsightOutput)

add_test(Stokes_2_operators
         MoReFEM4Stokes_2_operators
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Stokes/demo.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/TwoOperators/Seq)

set_tests_properties(Stokes_2_operators PROPERTIES TIMEOUT 40)

add_test(Stokes_2_operators-mpi
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Stokes_2_operators
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Stokes/demo.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/TwoOperators/Mpi4)

set_tests_properties(Stokes_2_operators-mpi PROPERTIES TIMEOUT 40)

add_test(Stokes_2_operatorsModelEnsightOutput
         MoReFEM4Stokes_2_operatorsEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/TwoOperators/Seq/Stokes/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/TwoOperators/Seq)

set_tests_properties(Stokes_2_operatorsModelEnsightOutput PROPERTIES TIMEOUT 40)

add_test(Stokes_2_operatorsModelEnsightOutput-mpi
         MoReFEM4Stokes_2_operatorsEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/TwoOperators/Mpi4/Stokes/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/TwoOperators/Mpi4)

set_tests_properties(Stokes_2_operatorsModelEnsightOutput-mpi PROPERTIES TIMEOUT 40)


# Not a typo: the Stokes executable with a different MOREFEM_TEST_OUTPUT_DIR is fine!
add_test(Stokes_2_operatorsCheckResults
         MoReFEM4StokesCheckResults
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}/TwoOperators
         )
