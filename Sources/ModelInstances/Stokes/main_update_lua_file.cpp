//! \file 
//
//
//  main_update_lua_file.cpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/InputData/RewriteInputDataFile.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "ModelInstances/Stokes/InputData.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    using InputData = StokesNS::InputData;
    
    try
    {
        MoReFEMData<InputData, program_type::model> morefem_data(argc, argv);

        RewriteInputDataFile(morefem_data.GetInputData());
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

