/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 16:20:48 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "ModelInstances/Stokes/Model.hpp"


namespace MoReFEM
{
    
    
    namespace StokesNS
    {
        

        Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }
        
        
        void Model::SupplInitialize()
        {
            const auto& god_of_dof = this->GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            
            const auto& felt_space_highest_dimension =
                god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::highest_dimension));
            
            auto&& numbering_subset_list = felt_space_highest_dimension.GetNumberingSubsetList();

            assert((numbering_subset_list.size() == 1 || numbering_subset_list.size() == 2)
                   && "1 or 2 numbering subsets expected here!");
            
            is_monolithic_ = numbering_subset_list.size() == 1 ? true : false;
            
            const auto& unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);
            
            const auto& velocity = unknown_manager.GetUnknown(EnumUnderlyingType(UnknownIndex::velocity));
            const auto& pressure = unknown_manager.GetUnknown(EnumUnderlyingType(UnknownIndex::pressure));
            
            // This form takes care of both monolithic and non monolithic case (in the former front == back...).
            constexpr const double uzawa_coupling_term = 15.;

            decltype(auto) morefem_data = parent::GetMoReFEMData();
            
            {
                const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);
                
                auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr("sole") };
                
                variational_formulation_ =
                    std::make_unique<VariationalFormulation>(morefem_data,
                                                             felt_space_highest_dimension.GetNumberingSubset(velocity),
                                                             felt_space_highest_dimension.GetNumberingSubset(pressure),
                                                             GetNonCstTimeManager(),
                                                             god_of_dof,
                                                             std::move(bc_list),
                                                             uzawa_coupling_term);
            }

            
           
            
            auto& formulation = this->GetNonCstVariationalFormulation();
          
            
            const auto& mpi = this->GetMpi();
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n",
                                                          mpi, __FILE__, __LINE__);
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n",
                                                          mpi, __FILE__, __LINE__);
            
            Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n",
                                                          mpi, __FILE__, __LINE__);
            
            formulation.Init(morefem_data.GetInputData());
            
            
            formulation.RunStaticCase();
        }
        
                
        void Model::Forward()
        {
            assert(false && "Only static case at the moment!");
            exit(-1);
        }
        
        
        void Model::SupplFinalizeStep()
        {
            assert(false && "Only static case at the moment!");
            exit(-1);
        }
        
        
        void Model::SupplFinalize() const
        { }
        
              
    } // namespace StokesNS
    
    
} // namespace MoReFEM
