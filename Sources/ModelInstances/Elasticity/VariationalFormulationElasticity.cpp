/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 20 Oct 2015 16:18:30 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Geometry/Domain/DomainManager.hpp"

#include "ModelInstances/Elasticity/VariationalFormulationElasticity.hpp"


namespace MoReFEM
{


    namespace ElasticityNS
    {


        VariationalFormulationElasticity::~VariationalFormulationElasticity() = default;
        
        
        void VariationalFormulationElasticity
        ::SupplInit(const InputData& input_data)
        {
            decltype(auto) domain =
                DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);
            
            volumic_mass_ = InitScalarParameterFromInputData<InputDataNS::Solid::VolumicMass>("Volumic mass",
                                                                                              domain,
                                                                                              input_data);
            
            if (!GetVolumicMass().IsConstant())
                throw Exception("Current elastic model is restricted to a constant volumic mass!", __FILE__, __LINE__);
            
            young_modulus_ = InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>("Young modulus",
                                                                                                domain,
                                                                                                input_data);
            
            poisson_ratio_ = InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>("Poisson ratio",
                                                                                                domain,
                                                                                                input_data);
            
            DefineOperators(input_data);
            
            {
                // Assemble the stiffness matrix.
                GlobalMatrixWithCoefficient matrix(GetNonCstStiffness(), 1.);
                GetStiffnessOperator().Assemble(std::make_tuple(std::ref(matrix)));
            }
        }
        
        
        
        VariationalFormulationElasticity
        ::VariationalFormulationElasticity(const morefem_data_type& morefem_data,
                                           const FEltSpace& main_felt_space,
                                           const FEltSpace& neumann_felt_space,
                                           const Unknown& solid_displacement,
                                           const NumberingSubset& numbering_subset,
                                           TimeManager& time_manager,
                                           const GodOfDof& god_of_dof,
                                           DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list)
        : parent(morefem_data,
                 time_manager,
                 god_of_dof,
                 std::move(boundary_condition_list)),
        main_felt_space_(main_felt_space),
        neumann_felt_space_(neumann_felt_space),
        solid_displacement_(solid_displacement),
        numbering_subset_(numbering_subset)
        {
            assert(time_manager.IsTimeStepConstant() && "Model's current implementation relies on this assumption.");
        }
        
        
        
        
        void VariationalFormulationElasticity::RunStaticCase()
        {
            AddSourcesToRhs();
            
            const auto& numbering_subset = GetNumberingSubset();
            parent::GetNonCstSystemMatrix(numbering_subset, numbering_subset).Copy(GetStiffness(), __FILE__, __LINE__);
            
            parent::template ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(numbering_subset, numbering_subset);
            parent::template SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset,
                                                           __FILE__, __LINE__);
        }
        
        
        
        void VariationalFormulationElasticity::PrepareDynamicRuns()
        {
            // Assemble once and for all the system matrix in dynamic case; intermediate matrices used
            // to compute rhs at each time iteration are also computed there.
            ComputeDynamicMatrices();
            UpdateDisplacement();
        }

        
        
        void VariationalFormulationElasticity::ComputeDynamicSystemRhs()
        {
            auto& rhs = this->GetNonCstSystemRhs(GetNumberingSubset());
            
            // Compute the system RHS. The rhs is effectively zeroed through the first MatMult call.
            const auto& current_displacement_matrix = GetMatrixCurrentDisplacement();
            const auto& current_velocity_matrix = GetMatrixCurrentVelocity();
            
            const auto& current_displacement_vector = GetVectorCurrentDisplacement();
            const auto& current_velocity_vector = GetVectorCurrentVelocity();
            
            Wrappers::Petsc::MatMult(current_displacement_matrix, current_displacement_vector, rhs, __FILE__, __LINE__);
            Wrappers::Petsc::MatMultAdd(current_velocity_matrix, current_velocity_vector, rhs, rhs, __FILE__, __LINE__);
        }
        
        
        void VariationalFormulationElasticity::AddSourcesToRhs()
        {
            const auto& numbering_subset = GetNumberingSubset();
            auto& rhs = parent::GetNonCstSystemRhs(numbering_subset);
            const auto time = parent::GetTimeManager().GetTime();
            
            if (this->template IsOperatorActivated<SourceType::volumic>())
            {
                GlobalVectorWithCoefficient force_vector(rhs, 1.);
                this->template GetForceOperator<SourceType::volumic>().Assemble(std::make_tuple(std::ref(force_vector)),
                                                                                time);
            }
            
            if (this->template IsOperatorActivated<SourceType::surfacic>())
            {
                GlobalVectorWithCoefficient force_vector(rhs, 1.);
                this->template GetForceOperator<SourceType::surfacic>().Assemble(std::make_tuple(std::ref(force_vector)),
                                                                                 time);
            }
        }
        
        
        void VariationalFormulationElasticity
        ::AllocateMatricesAndVectors()
        {
            const auto& numbering_subset = GetNumberingSubset();
            
            parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
            parent::AllocateSystemVector(numbering_subset);
            
            const auto& system_matrix = parent::GetSystemMatrix(numbering_subset, numbering_subset);
            const auto& system_rhs = parent::GetSystemRhs(numbering_subset);
            
            mass_ = std::make_unique<GlobalMatrix>(system_matrix);
            stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
            matrix_current_displacement_ = std::make_unique<GlobalMatrix>(system_matrix);
            matrix_current_velocity_ = std::make_unique<GlobalMatrix>(system_matrix);
            
            vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
            vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
        }
        
        
        
        void VariationalFormulationElasticity::ComputeDynamicMatrices()
        {
            const auto& numbering_subset = GetNumberingSubset();
            auto& system_matrix = this->GetNonCstSystemMatrix(numbering_subset, numbering_subset);
            const auto& stiffness = GetStiffness();
            
            {
                GlobalMatrixWithCoefficient mass(GetNonCstMass(), 1.);
                GetMassOperator().Assemble(std::make_tuple(std::ref(mass)));
            }
            
            const auto& mass = GetMass();
            
            {
                // Compute the system matrix, which won't change afterwards!
                system_matrix.Copy(stiffness, __FILE__, __LINE__);
                system_matrix.Scale(0.5, __FILE__, __LINE__);

                const auto coefficient =
                    2. * GetVolumicMass().GetConstantValue() / NumericNS::Square(parent::GetTimeManager().GetTimeStep());
            
                #ifndef NDEBUG
                AssertSameNumberingSubset(mass, system_matrix);
                #endif // NDEBUG
                
                Wrappers::Petsc::AXPY<NonZeroPattern::same>( coefficient,
                                      mass,
                                      system_matrix,
                                      __FILE__, __LINE__);
            }
            
            {
                // Displacement matrix.
                auto& current_displacement_matrix = GetNonCstMatrixCurrentDisplacement();
                current_displacement_matrix.Copy(mass, __FILE__, __LINE__);
                
                const auto coefficient =
                2. * GetVolumicMass().GetConstantValue() / NumericNS::Square(parent::GetTimeManager().GetTimeStep());
                
                current_displacement_matrix.Scale(coefficient, __FILE__, __LINE__);
                
                #ifndef NDEBUG
                AssertSameNumberingSubset(stiffness, current_displacement_matrix);
                #endif // NDEBUG
                
                Wrappers::Petsc::AXPY<NonZeroPattern::same>( -.5,
                                      stiffness,
                                      current_displacement_matrix,
                                      __FILE__, __LINE__);
                
            }
            
            {
                // Velocity matrix.
                auto& current_velocity_matrix = GetNonCstMatrixCurrentVelocity();
                current_velocity_matrix.Copy(mass, __FILE__, __LINE__);
                current_velocity_matrix.Scale(2. * GetVolumicMass().GetConstantValue() / parent::GetTimeManager().GetTimeStep(),
                                              __FILE__, __LINE__);
            }
        }
        
        
        
        void VariationalFormulationElasticity::UpdateDisplacement()
        {
            GetNonCstVectorCurrentDisplacement().Copy(parent::GetSystemSolution(GetNumberingSubset()),
                                                      __FILE__, __LINE__);
        }
        
        
        
        void VariationalFormulationElasticity::UpdateVelocity()
        {
            const auto& current_displacement_vector = GetVectorCurrentDisplacement();
            const auto& system_solution = parent::GetSystemSolution(GetNumberingSubset());
            auto& current_velocity_vector = GetNonCstVectorCurrentVelocity();
            
            assert(parent::GetTimeManager().GetStaticOrDynamic() == StaticOrDynamic::dynamic_);
            
            {
                // Update first the velocity.
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> solution(system_solution, __FILE__, __LINE__);
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> displacement_prev(current_displacement_vector, __FILE__, __LINE__);
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> velocity(current_velocity_vector, __FILE__, __LINE__);
                
                const unsigned int size = velocity.GetSize(__FILE__, __LINE__);
                assert(size == solution.GetSize(__FILE__, __LINE__));
                assert(size == displacement_prev.GetSize(__FILE__, __LINE__));
                
                const double factor = 2. / parent::GetTimeManager().GetTimeStep();
                
                for (unsigned int i = 0u; i < size; ++i)
                {
                    velocity[i] *= -1.;
                    velocity[i] += factor * (solution.GetValue(i) - displacement_prev.GetValue(i));
                }
            }
        }


        
        
        
        void VariationalFormulationElasticity::DefineOperators(const InputData& input_data)
        {
            const auto& god_of_dof = this->GetGodOfDof();
            const auto& felt_space_highest_dimension = GetMainFEltSpace();
            const auto& felt_space_neumann = GetNeumannFEltSpace();
            
            const auto mesh_dimension = god_of_dof.GetMesh().GetDimension();
            
            const auto& displacement_ptr = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknownPtr(EnumUnderlyingType(UnknownIndex::solid_displacement));
            
            this->template SetIfTaggedAsActivated<SourceType::volumic>("Volumic force",
                                                                       input_data,
                                                                       felt_space_highest_dimension,
                                                                       displacement_ptr);
            
            this->template SetIfTaggedAsActivated<SourceType::surfacic>("Surfacic force",
                                                                        input_data,
                                                                        felt_space_neumann,
                                                                        displacement_ptr);
            
            namespace GVO = GlobalVariationalOperatorNS;
            
            namespace IPL = Utilities::InputDataNS;
            
            stiffness_operator_ = std::make_unique<GVO
            ::GradOnGradientBasedElasticityTensor>(felt_space_highest_dimension,
                                                   displacement_ptr,
                                                   displacement_ptr,
                                                   GetYoungModulus(),
                                                   GetPoissonRatio(),
                                                   ParameterNS::ReadGradientBasedElasticityTensorConfigurationFromFile(mesh_dimension,
                                                                                                                       input_data));
            
            mass_operator_ = std::make_unique<GVO::Mass>(felt_space_highest_dimension,
                                                         displacement_ptr,
                                                         displacement_ptr);
        }

      
    } // namespace ElasticityNS
    
    
} // namespace MoReFEM

