/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 13:43:40 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_ELASTICITY_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_ELASTICITY_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputData/LuaFunction.hpp"

# include "Geometry/Domain/Domain.hpp"

# include "Parameters/Parameter.hpp"
# include "ParameterInstances/GradientBasedElasticityTensor/Internal/Configuration.hpp"

# include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
# include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hpp"

# include "FormulationSolver/VariationalFormulation.hpp"
# include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"

# include "ModelInstances/Elasticity/InputData.hpp"


namespace MoReFEM
{


    namespace ElasticityNS
    {


        //! \copydoc doxygen_hide_simple_varf
        class VariationalFormulationElasticity final
        : public VariationalFormulation
        <
            VariationalFormulationElasticity,
            EnumUnderlyingType(SolverIndex::solver)
        >,
        public Crtp::VolumicAndSurfacicSource
        <
            VariationalFormulationElasticity,
            ParameterNS::Type::vector,
            EnumUnderlyingType(SourceIndex::volumic),
            EnumUnderlyingType(SourceIndex::surfacic),
            ParameterNS::TimeDependencyNS::None
        >
        {

        private:

            //! Alias to self.
            using self = VariationalFormulationElasticity;

            //! Alias to parent class.
            using parent = VariationalFormulation<self, EnumUnderlyingType(SolverIndex::solver)>;

            //! Friendship to parent class, so this one can access private methods defined below through CRTP.
            friend parent;

        public:

            //! Alias to unique pointer.
            using unique_ptr = std::unique_ptr<self>;

            //! Alias to the appropriate \a ScalarParameter used to describe the solid.
            using solid_scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

            //! Alias on a pair of Unknown.
            using UnknownPair = std::pair<const Unknown&, const Unknown&>;

        public:

            /// \name Special members.
            ///@{

            //! \copydoc doxygen_hide_varf_constructor
            //! \param[in] main_felt_space Main \a FEltSpace for the model.
            //! \param[in] neumann_felt_space \a FEltSpace into which Neumann conditions are defined.
            //! \param[in] solid_displacement \a Unknown related to solid displacement.
            //! \param[in] numbering_subset The only \a NumberingSubset used in this variational formulation.
            explicit VariationalFormulationElasticity(const morefem_data_type& morefem_data,
                                                      const FEltSpace& main_felt_space,
                                                      const FEltSpace& neumann_felt_space,
                                                      const Unknown& solid_displacement,
                                                      const NumberingSubset& numbering_subset,
                                                      TimeManager& time_manager,
                                                      const GodOfDof& god_of_dof,
                                                      DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list);

            //! Destructor.
            ~VariationalFormulationElasticity() override;

            //! \copydoc doxygen_hide_copy_constructor
            VariationalFormulationElasticity(const VariationalFormulationElasticity& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            VariationalFormulationElasticity(VariationalFormulationElasticity&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            VariationalFormulationElasticity& operator=(const VariationalFormulationElasticity& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            VariationalFormulationElasticity& operator=(VariationalFormulationElasticity&& rhs) = delete;

            ///@}


            //! Run the static case.
            void RunStaticCase();

            /*!
             * \brief Prepare dynamic runs.
             *
             * For instance for dynamic iterations the system matrix is always the same; compute it once and for all
             * here.
             *
             * StaticOrDynamic rhs is what changes between two time iterations, but to compute it the same matrices are used at
             * each time iteration; they are also computed there.
             */
            void PrepareDynamicRuns();

            //! At each time iteration, compute the system Rhs.
            void ComputeDynamicSystemRhs();

            //! Update displacement and velocity for next time step.
            void UpdateDisplacementAndVelocity();

            /*!
             * \brief Get the only numbering subset relevant for this VariationalFormulation.
             *
             * There is a more generic accessor in the base class but is use is more unwieldy.
             *
             * \return The only \a NumberingSubset present in this \a VariationalFormulation.
             */
            const NumberingSubset& GetNumberingSubset() const noexcept;


            //! Finite element space upon which the variational formulation apply.
            const FEltSpace& GetMainFEltSpace() const noexcept;

            //! Finite element space upon which the Neumann condition apply.
            const FEltSpace& GetNeumannFEltSpace() const noexcept;


        private:

            //! Compute all the matrices required for dynamic calculation.
            void ComputeDynamicMatrices();

            //! Update the displacement for the next time iteration.
            void UpdateDisplacement();

            /*!
             * \brief Update the velocity for the next time iteration.
             *
             * BEWARE: this method must be called BEFORE UpdateDisplacement(), as it relies upon the displacement
             * that has been used in the last Ksp solve.
             */
            void UpdateVelocity();

        public:

            /*!
             * \brief Add in the RHS the contributions of the surfacic and volumic sources, and also the one from
             * a dof source of DofSource policy is used.
             */
            void AddSourcesToRhs();


        private:

            /// \name CRTP-required methods.
            ///@{

            /*!
             * \brief Specific initialisation for derived class attributes.
             *
             * \internal <b><tt>[internal]</tt></b> This method is called by base class method VariationalFormulation::Init().
             * \endinternal
             *
             * \copydoc doxygen_hide_input_data_arg
             */
            void SupplInit(const InputData& input_data);

            /*!
             * \brief Allocate the global matrices and vectors.
             */
            void AllocateMatricesAndVectors();

            //! Define the pointer function required to test the convergence required by the non-linear problem.
            Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const noexcept;

            ///@}

            /*!
             * \brief Define the properties of all the global variational operators involved.
             *
             * \copydoc doxygen_hide_input_data_arg
             */
            void DefineOperators(const InputData& input_data);


            /// \name Accessors to vectors and matrices specific to the elastic problem.
            ///@{

            //! Accessor to the \a GlobalVector which contains current displacement.
            const GlobalVector& GetVectorCurrentDisplacement() const noexcept;

            //! Non constant accessor to the \a GlobalVector which contains current displacement.
            GlobalVector& GetNonCstVectorCurrentDisplacement() noexcept;

            //! Accessor to the \a GlobalVector which contains current velocity.
            const GlobalVector& GetVectorCurrentVelocity() const noexcept;

            //! Non constant accessor to the \a GlobalVector which contains current velocity.
            GlobalVector& GetNonCstVectorCurrentVelocity() noexcept;

            //! Accessor to the \a GlobalMatrix used along displacement in the model.
            const GlobalMatrix& GetMatrixCurrentDisplacement() const noexcept;

            //! Non constant accessor to the \a GlobalMatrix used along displacement in the model.
            GlobalMatrix& GetNonCstMatrixCurrentDisplacement() noexcept;

            //! Accessor to the \a GlobalMatrix used along velocity in the model.
            const GlobalMatrix& GetMatrixCurrentVelocity() const noexcept;

            //! Non constant accessor to the \a GlobalMatrix used along velocity in the model.
            GlobalMatrix& GetNonCstMatrixCurrentVelocity() noexcept;

            //! Accessor to the mass matrix.
            const GlobalMatrix& GetMass() const noexcept;

            //! Non constant accessor to the mass matrix.
            GlobalMatrix& GetNonCstMass() noexcept;

            //! Accessor to the stiffness matrix.
            const GlobalMatrix& GetStiffness() const noexcept;

            //! Non constant accessor to the stiffness matrix.
            GlobalMatrix& GetNonCstStiffness() noexcept;

            ///@}

            //! Get the stiffness operator.
            const GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor& GetStiffnessOperator() const noexcept;

            //! Get the mass per square time step operator.
            const GlobalVariationalOperatorNS::Mass& GetMassOperator() const noexcept;

            //! Get the solid displacement.
            const Unknown& GetSolidDisplacement() const noexcept;

        private:


            /// \name Global variational operators.

            ///@{


            //! Stiffness operator.
            GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor::const_unique_ptr
                stiffness_operator_ = nullptr;

            //! Mass operator.
            GlobalVariationalOperatorNS::Mass::const_unique_ptr
                mass_operator_ = nullptr;


            ///@}


            /// \name Material parameters.
            ///@{

            //! Volumic mass.
            const ScalarParameter<>& GetVolumicMass() const noexcept;

            //! Young modulus.
            const ScalarParameter<>& GetYoungModulus() const noexcept;

            //! Poisson ratio.
            const ScalarParameter<>& GetPoissonRatio() const noexcept;

            ///@}


        private:

            /// \name Global vectors and matrices specific to the elastic problem.
            ///@{

            //! Vector current displacement.
            GlobalVector::unique_ptr vector_current_displacement_ = nullptr;

            //! Vector current velocity.
            GlobalVector::unique_ptr vector_current_velocity_ = nullptr;

            //! Matrix current displacement.
            GlobalMatrix::unique_ptr matrix_current_displacement_ = nullptr;

            //! Matrix current velocity.
            GlobalMatrix::unique_ptr matrix_current_velocity_ = nullptr;

            //! Mass matrix.
            GlobalMatrix::unique_ptr mass_ = nullptr;

            /*!
             * \brief Stiffness matrix.
             *
             * \internal <b><tt>[internal]</tt></b> Actually we could dodge this one entirely but the code is more readable by making it explicit.
             * \endinternal
             */
            GlobalMatrix::unique_ptr stiffness_ = nullptr;

            ///@}

            //! Finite element space upon which the variational formulation apply.
            const FEltSpace& main_felt_space_;

            //! Finite element space upon which the Neumann condition apply.
            const FEltSpace& neumann_felt_space_;

            //! Unknown considered; it should be a solid displacement.
            const Unknown& solid_displacement_;

            //! Numbering subset covering the displacement in the main finite element space.
            const NumberingSubset& numbering_subset_;

        private:

            //! \name Material parameters.
            ///@{

            //! Volumic mass.
            solid_scalar_parameter::unique_ptr volumic_mass_ = nullptr;

            //! Young modulus.
            solid_scalar_parameter::unique_ptr young_modulus_ = nullptr;

            //! Poisson ratio.
            solid_scalar_parameter::unique_ptr poisson_ratio_ = nullptr;

            ///@}
        };


    } // namespace ElasticityNS


} // namespace MoReFEM


# include "ModelInstances/Elasticity/VariationalFormulationElasticity.hxx"


#endif // MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_ELASTICITY_HPP_
