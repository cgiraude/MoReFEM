/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Apr 2018 17:53:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE model_elasticity
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "ModelInstances/Elasticity/InputData.hpp"

using namespace MoReFEM;

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareEnsightFiles.hpp"
#include "Test/Tools/Fixture/Environment.hpp"


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par,
                        std::string&& dimension,
                        std::string&& ascii_or_bin);


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(seq_3d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_2d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(seq_2d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Seq", "2D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d_bin, TestNS::FixtureNS::Environment)
{
    CommonTestCase("Mpi4", "2D", "Binary");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par,
                        std::string&& dimension,
                        std::string&& ascii_or_bin)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir, output_dir;

        /* BOOST_REQUIRE_NO_THROW */(root_dir = environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */(output_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR",
                                                                               __FILE__, __LINE__));

        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(root_dir));
        BOOST_REQUIRE(Advanced::FilesystemNS::DirectoryNS::DoExist(output_dir));

        std::string ref_dir_path = root_dir + "/Sources/ModelInstances/Elasticity/ExpectedResults/" + ascii_or_bin
            + std::string("/") + dimension;
        std::string obtained_dir_path = output_dir + std::string("/") + seq_or_par + std::string("/") + ascii_or_bin
            + std::string("/Elasticity/") + dimension + std::string("/Rank_0");

        FilesystemNS::Directory ref_dir(ref_dir_path,
                                        FilesystemNS::behaviour::read,
                                        __FILE__, __LINE__);

        FilesystemNS::Directory obtained_dir(obtained_dir_path,
                                             FilesystemNS::behaviour::read,
                                             __FILE__, __LINE__);

        /* BOOST_REQUIRE_NO_THROW */(TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua",
                                                           __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */(TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata",
                                                           __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */(TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata",
                                                           __FILE__, __LINE__));

        ref_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Mesh_1", __FILE__, __LINE__);


        /* BOOST_REQUIRE_NO_THROW */(TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata",
                                                           __FILE__, __LINE__));

        ref_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);
        obtained_dir.AddSubdirectory("Ensight6", __FILE__, __LINE__);

        /* BOOST_REQUIRE_NO_THROW */(TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo",
                                                                 __FILE__, __LINE__));
        /* BOOST_REQUIRE_NO_THROW */(TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case",
                                                                 __FILE__, __LINE__));

        std::ostringstream oconv;

        for (auto i = 0ul; i <= 5; ++i)
        {
            oconv.str("");
            oconv << "solid_displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
            /* BOOST_REQUIRE_NO_THROW */(TestNS::CompareEnsightFiles(ref_dir, obtained_dir, oconv.str(),
                                                               __FILE__, __LINE__, 1.e-11));
        }

    }


} // namespace anonymous
