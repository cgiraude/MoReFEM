add_library(MoReFEM4Laplacian_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEM4Laplacian_lib
    PRIVATE
        "${CMAKE_CURRENT_LIST_DIR}/Model.cpp" / 
        "${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp"
	PUBLIC
		"${CMAKE_CURRENT_LIST_DIR}/Model.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/Model.hxx" /
		"${CMAKE_CURRENT_LIST_DIR}/InputData.hpp" /
 		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp" /
		"${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx"
)

target_link_libraries(MoReFEM4Laplacian_lib
                      ${ALL_LOAD_BEGIN_FLAG}
                      ${MOREFEM_MODEL}
                      ${ALL_LOAD_END_FLAG})


add_executable(MoReFEM4Laplacian ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4Laplacian
                      MoReFEM4Laplacian_lib)
apply_lto_if_supported(MoReFEM4Laplacian)

morefem_install(MoReFEM4Laplacian MoReFEM4Laplacian_lib)

add_executable(MoReFEM4LaplacianEnsightOutput ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)
target_link_libraries(MoReFEM4LaplacianEnsightOutput
                      ${MOREFEM_POST_PROCESSING})

add_executable(MoReFEM4LaplacianUpdateLuaFile ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)
target_link_libraries(MoReFEM4LaplacianUpdateLuaFile ${MOREFEM_CORE})

morefem_install(MoReFEM4LaplacianEnsightOutput MoReFEM4LaplacianUpdateLuaFile)     

add_test(Laplacian
         MoReFEM4Laplacian
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Laplacian/demo_input_laplacian.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq)

add_test(Laplacian-mpi
         ${OPEN_MPI_INCL_DIR}/../bin/mpirun
         --oversubscribe
         -np 4 MoReFEM4Laplacian
         --overwrite_directory
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_ROOT}/Sources/ModelInstances/Laplacian/demo_input_laplacian.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4)

add_test(LaplacianModelEnsightOutput
         MoReFEM4LaplacianEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Seq/Laplacian/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Seq)

add_test(LaplacianModelEnsightOutput-mpi
         MoReFEM4LaplacianEnsightOutput
         -e MOREFEM_ROOT=${MOREFEM_ROOT}
         -i ${MOREFEM_TEST_OUTPUT_DIR}/Mpi4/Laplacian/Rank_0/input_data.lua
         -e MOREFEM_RESULT_DIR=${MOREFEM_TEST_OUTPUT_DIR}/Mpi4)

add_executable(MoReFEM4LaplacianCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
target_link_libraries(MoReFEM4LaplacianCheckResults
                      ${MOREFEM_TEST_TOOLS})

add_test(LaplacianCheckResults
         MoReFEM4LaplacianCheckResults
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR}
         )

