/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 16:20:48 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include "ModelInstances/Heat/HeatModel.hpp"


namespace MoReFEM
{
    
    
    namespace HeatNS
    {
        
        
        HeatModel::HeatModel(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }
        
        
        void HeatModel::SupplInitialize()
        {
            const GodOfDof& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();
            const NumberingSubset& numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::monolithic));
            
            {
                const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);
                
                auto&& bc_list =
                {
                    bc_manager.GetDirichletBoundaryConditionPtr("first"),
                    bc_manager.GetDirichletBoundaryConditionPtr("second")
                };
                
                variational_formulation_ =
                    std::make_unique<HeatVariationalFormulation>(morefem_data,
                                                                 numbering_subset,
                                                                 GetNonCstTimeManager(),
                                                                 god_of_dof,
                                                                 std::move(bc_list));
                                                                                    
            }
            
            HeatVariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
            
            namespace IPL = Utilities::InputDataNS;
            using TimeManager = InputDataNS::TimeManager;
            
            decltype(auto) input_data = morefem_data.GetInputData();
            
            double time_max = IPL::Extract<TimeManager::TimeMax>::Value(input_data);
            
            if (NumericNS::IsZero(time_max))
                variational_formulation.SetStaticOrDynamic(HeatVariationalFormulation::StaticOrDynamic::static_case);
            else
                variational_formulation.SetStaticOrDynamic(HeatVariationalFormulation::StaticOrDynamic::dynamic_case);
            
            variational_formulation.Init(morefem_data.GetInputData());
            variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
            
        }
        
        
        void HeatModel::Forward()
        {
            HeatVariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
            
            // Only Rhs is modified at each time iteration; compute it and solve the system.
            variational_formulation.ComputeDynamicSystemRhs();
            
            const NumberingSubset& numbering_subset = variational_formulation.GetNumberingSubset();
           
            if (GetTimeManager().NtimeModified() == 1)
            {
                variational_formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(numbering_subset, numbering_subset);
                variational_formulation.SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset,
                                                                      __FILE__, __LINE__);
            }
            else
            {
                variational_formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset, numbering_subset);
                variational_formulation.SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset,
                                                                       __FILE__, __LINE__);
            }
        }
        
        
        void HeatModel::SupplFinalizeStep()
        {
            // Update quantities for next iteration.
            HeatVariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
            const NumberingSubset& numbering_subset = variational_formulation.GetNumberingSubset();
            
            variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
        }
        
        
        void HeatModel::SupplFinalize() const
        { }

        
    } // namespace HeatNS
    
    
} // namespace MoReFEM
