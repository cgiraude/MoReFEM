/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_INPUT_DATA_HPP_
# define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/MoReFEMData/MoReFEMData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
# include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
# include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
# include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
# include "Core/InputData/Instances/Geometry/Mesh.hpp"
# include "Core/InputData/Instances/Solver/Petsc.hpp"


namespace MoReFEM
{


    namespace HeatNS
    {


        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList : unsigned int
        {
            volumic_source = 1,
            neumann_boundary_condition = 2,
            robin_boundary_condition = 3
        };


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1 // only one mesh considered in current model!
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            highest_dimension = 1,
            neumann = 2,
            robin = 3,
            dirichlet_1 = 4,
            dirichlet_2 = 5,
            full_mesh = 6
        };


        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            first = 1,
            second = 2
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            neumann = 2,
            robin = 3
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            temperature = 1
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            monolithic = 1
        };


        //! Index to tag the tensors involved.
        enum class TensorIndex
        {
            diffusion_tensor = 1
        };


        //! \copydoc doxygen_hide_initial_condition_enum
        enum class InitialConditionIndex
        {
            temperature_initial_condition = 1
        };


        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::temperature)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::robin)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet_1)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet_2)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>,
            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::second)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::robin)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Diffusion::Density,
            InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>,
            InputDataNS::Diffusion::TransfertCoefficient,

            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::volumic_source)>,
            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::neumann_boundary_condition)>,
            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::robin_boundary_condition)>,

            InputDataNS::InitialCondition<EnumUnderlyingType(InitialConditionIndex::temperature_initial_condition)>,

            InputDataNS::Result

        >;


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::model>;


    } // namespace HeatNS


} // namespace MoReFEM


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_INPUT_DATA_HPP_
