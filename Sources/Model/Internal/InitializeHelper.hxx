/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


#ifndef MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_
# define MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ModelNS
        {


            template<class MoReDEMDataT>
            void InitEachGodOfDof(const MoReDEMDataT& morefem_data,
                                  std::map<unsigned int, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index,
                                  DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                                  std::map<unsigned int, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                                  mesh_output_directory_storage)
            {
                const auto& god_of_dof_storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

                for (const auto& pair : god_of_dof_storage)
                {
                    auto& god_of_dof_ptr = pair.second;

                    assert(!(!god_of_dof_ptr));
                    auto& god_of_dof = *god_of_dof_ptr;

                    auto it = felt_space_list_per_god_of_dof_index.find(god_of_dof.GetUniqueId());

                    if (it == felt_space_list_per_god_of_dof_index.cend())
                        throw Exception("No finite element space in GodOfDof " + std::to_string(god_of_dof.GetUniqueId()),
                                        __FILE__, __LINE__);

                    auto it_output_dir = mesh_output_directory_storage.find(god_of_dof.GetUniqueId());

                    assert(it_output_dir != mesh_output_directory_storage.cend());
                    assert(it_output_dir->second != nullptr);

                    god_of_dof.Init(morefem_data,
                                    std::move(it->second),
                                    do_consider_proc_wise_local_2_global,
                                    *(it_output_dir->second));
                }
            }


            template<class MoReDEMDataT>
            void InitGodOfDof(const MoReDEMDataT& morefem_data,
                              DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
                              std::map<unsigned int, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                              mesh_output_directory_storage)
            {
                std::map<unsigned int, FEltSpace::vector_unique_ptr> felt_space_list_per_god_of_dof_index;

                CreateFEltSpaceList(morefem_data.GetInputData(), felt_space_list_per_god_of_dof_index);

                InitEachGodOfDof(morefem_data,
                                 felt_space_list_per_god_of_dof_index,
                                 do_consider_proc_wise_local_2_global,
                                 std::move(mesh_output_directory_storage));
            }


            template<class InputDataT>
            void CreateFEltSpaceList(const InputDataT& input_data,
                                     std::map<unsigned int, FEltSpace::vector_unique_ptr>& felt_space_list_per_god_of_dof_index)
            {
                auto create =[&felt_space_list_per_god_of_dof_index](const auto& section) -> void
                {
                    namespace ipl = Internal::InputDataNS;

                    using section_type = std::decay_t<decltype(section)>;

                    const auto unique_id = section.GetUniqueId();

                    decltype(auto) god_of_dof_index = ipl::ExtractParameter<typename section_type::GodOfDofIndex>(section);
                    decltype(auto) domain_index = ipl::ExtractParameter<typename section_type::DomainIndex>(section);

                    auto&& extended_unknown_list =
                        MoReFEM::Internal::FEltSpaceNS::ExtractExtendedUnknownList(section);

                    decltype(auto) god_of_dof_ptr = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetGodOfDofPtr(god_of_dof_index);

                    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

                    try
                    {
                        const auto& domain = domain_manager.GetDomain(domain_index, __FILE__, __LINE__);

                        auto felt_space_ptr = std::make_unique<FEltSpace>(god_of_dof_ptr,
                                                                          domain,
                                                                          unique_id,
                                                                          std::move(extended_unknown_list));

                        felt_space_list_per_god_of_dof_index[god_of_dof_index].emplace_back(std::move(felt_space_ptr));
                    }
                    catch(const Exception& e)
                    {
                        std::ostringstream oconv;
                        oconv << "Ill-defined finite element space " << unique_id << ": " << e.GetRawMessage();
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                    }
                };


                using input_data_tuple_iteration =
                    Internal::InputDataNS::TupleIteration
                    <
                        typename InputDataT::Tuple,
                        0,
                        std::tuple_size<typename InputDataT::Tuple>::value
                    >;

                input_data_tuple_iteration
                ::template ActIfSection<::MoReFEM::InputDataNS::BaseNS::FEltSpace>(input_data.GetTuple(),
                                                                                      create);
            }


        } // namespace ModelNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ModelGroup


#endif // MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_
