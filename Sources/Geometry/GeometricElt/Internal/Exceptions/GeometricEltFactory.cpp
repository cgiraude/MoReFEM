/*!
//
// \file
//
//
// Created by Sebastien Gilles <srpgilles@gmail.com> on the Tue, 5 Feb 2013 11:59:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <sstream>
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"


namespace // anonymous
{
    
    
    // Forward declarations here; definitions are at the end of the file
    std::string InvalidEnsightGeometricEltNameMsg(const std::string& geometric_elt_name);
    
    std::string InvalidGeometricEltIdMsg(MoReFEM::Advanced::GeometricEltEnum identifier);
    
    std::string InvalidGeometricEltNameMsg(const std::string& name);
    
    
} // namespace anonymous



namespace MoReFEM
{
    

    namespace ExceptionNS
    {
        
        
        namespace Factory
        {
            
            
            namespace GeometricElt
            {
                
                
                Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
                : ::MoReFEM::ExceptionNS::Factory::Exception(msg, invoking_file, invoking_line)
                { }
                
                
                Exception::~Exception() = default;
                
                
                InvalidEnsightGeometricEltName
                ::InvalidEnsightGeometricEltName(const std::string& geometric_elt_name,
                                                 const char* invoking_file, int invoking_line)
                : Exception(InvalidEnsightGeometricEltNameMsg(geometric_elt_name), invoking_file, invoking_line)
                { }
                
                
                InvalidEnsightGeometricEltName::~InvalidEnsightGeometricEltName() = default;
                
                
                InvalidGeometricEltId::InvalidGeometricEltId(Advanced::GeometricEltEnum identifier,
                                                             const char* invoking_file, int invoking_line)
                : Exception(InvalidGeometricEltIdMsg(identifier), invoking_file, invoking_line)
                { }
                
                InvalidGeometricEltId::~InvalidGeometricEltId() = default;
                
                
                InvalidGeometricEltName::InvalidGeometricEltName(const std::string& name,
                                                                 const char* invoking_file,
                                                                 int invoking_line)
                : Exception(InvalidGeometricEltNameMsg(name), invoking_file, invoking_line)
                { }
                
                
                InvalidGeometricEltName::~InvalidGeometricEltName() = default;
                
                
            } // namespace GeometricEltFactory
            
            
        } // namespace Factory
        
        
    } // namespace ExceptionNS
    
    
} // namespace MoReFEM





namespace // anonymous
{
    
    
    // Definitions of functions defined at the beginning of the file
    
    
    std::string InvalidEnsightGeometricEltNameMsg(const std::string& geometric_elt_name)
    {
        std::ostringstream oconv;
        oconv << "Unavailable Ensight geometric element name (\"";
        oconv << geometric_elt_name << "\"); possible choices are: ";
        oconv << MoReFEM::Advanced::GeometricEltFactory::GetInstance(__FILE__, __LINE__).EnsightGeometricEltNames();
        return oconv.str();
    }
    
    
    std::string InvalidGeometricEltIdMsg(MoReFEM::Advanced::GeometricEltEnum identifier)
    {
        std::ostringstream oconv;
        oconv << "Unavailable geometric element identifier (\"";
        oconv << static_cast<unsigned int>(identifier) << "\")";
        return oconv.str();
    }
    
    
    std::string InvalidGeometricEltNameMsg(const std::string& name)
    {
        std::ostringstream oconv;
        oconv << "Unavailable geometric element name (\"";
        oconv << name << "\")";
        return oconv.str();
    }
    
    
    
} // namespace anonymous


/// @} // addtogroup GeometryGroup








