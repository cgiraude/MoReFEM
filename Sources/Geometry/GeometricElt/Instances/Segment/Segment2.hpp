/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT2_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT2_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Segment/Segment2.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Segment2 geometric element read in a mesh.
     *
     * We are not considering here a generic Segment2 object (that's the role of MoReFEM::RefGeomEltNS::Segment2), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Segment2 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Segment2>
    {
    public:

        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Segment2(unsigned int mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Segment2(unsigned int mesh_unique_id,
                          const Coords::vector_unique_ptr& mesh_coords_list,
                          std::istream& stream);

        //! \copydoc doxygen_hide_geom_elt_vector_coords_constructor
        explicit Segment2(unsigned int mesh_unique_id,
                        const Coords::vector_unique_ptr& mesh_coords_list,
                        std::vector<unsigned int>&& coords_index_list);

        //! Destructor.
        ~Segment2() override;

        //! \copydoc doxygen_hide_copy_constructor
        Segment2(const Segment2& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Segment2(Segment2&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Segment2& operator=(const Segment2& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Segment2& operator=(Segment2&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Segment2& StaticRefGeomElt();

    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_SEGMENT_x_SEGMENT2_HPP_
