/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Hexahedron/Hexahedron8.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Hexahedron8 geometric element read in a mesh.
     *
     * We are not considering here a generic Hexahedron8 object (that's the role of MoReFEM::RefGeomEltNS::Hexahedron8), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Hexahedron8 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Hexahedron8>
    {
    public:

        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Hexahedron8(unsigned int mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Hexahedron8(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::istream& stream);

        //! \copydoc doxygen_hide_geom_elt_vector_coords_constructor
        explicit Hexahedron8(unsigned int mesh_unique_id,
                             const Coords::vector_unique_ptr& mesh_coords_list,
                             std::vector<unsigned int>&& coords_index_list);

        //! Destructor.
        ~Hexahedron8() override;

        //! \copydoc doxygen_hide_copy_constructor
        Hexahedron8(const Hexahedron8& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Hexahedron8(Hexahedron8&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Hexahedron8& operator=(const Hexahedron8& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Hexahedron8& operator=(Hexahedron8&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Hexahedron8& StaticRefGeomElt();


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_HEXAHEDRON_x_HEXAHEDRON8_HPP_
