/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/GeometricElt/Instances/Triangle/Triangle3.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"



namespace MoReFEM
{
    
    
    // The anonymous namespace is extremely important here: function is invisible from other modules
    namespace // anonymous
    {
        
        
        auto CreateEnsight(unsigned int mesh_unique_id,
                           const Coords::vector_unique_ptr& mesh_coords_list,
                           std::istream& in)
        {
            return std::make_unique<Triangle3>(mesh_unique_id, mesh_coords_list, in);
        }
        
        
        auto CreateMinimal(unsigned int mesh_unique_id)
        {
            return std::make_unique<Triangle3>(mesh_unique_id);
        }
        
        
        // Register the geometric element in the 'GeometricEltFactory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
        Advanced::GeometricEltFactory::CreateOrGetInstance(__FILE__, __LINE__).RegisterGeometricElt<RefGeomEltNS::Triangle3>(CreateMinimal,
                                                                                                    CreateEnsight);


        
    } // namespace anonymous
    
    
    
    Triangle3::Triangle3(unsigned int mesh_unique_id)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Triangle3>(mesh_unique_id)
    { }
    
    
    Triangle3::Triangle3(unsigned int mesh_unique_id,
                         const Coords::vector_unique_ptr& mesh_coords_list,
                         std::istream& stream)
    : TGeometricElt<RefGeomEltNS::Traits::Triangle3>(mesh_unique_id, mesh_coords_list, stream)
    { }
    
    
    Triangle3::Triangle3(unsigned int mesh_unique_id,
                        const Coords::vector_unique_ptr& mesh_coords_list,
                        std::vector<unsigned int>&& coords)
    : TGeometricElt<RefGeomEltNS::Traits::Triangle3>(mesh_unique_id, mesh_coords_list, std::move(coords))
    { }


    Triangle3::~Triangle3() = default;
    
    
    const RefGeomElt& Triangle3::GetRefGeomElt() const
    {
        return StaticRefGeomElt();
    }
    
    
    const RefGeomEltNS::Triangle3& Triangle3::StaticRefGeomElt()
    {
        static RefGeomEltNS::Triangle3 ret;
        return ret;
    }


    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup

