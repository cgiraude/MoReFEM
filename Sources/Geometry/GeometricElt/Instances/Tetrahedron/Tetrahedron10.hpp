/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON10_HPP_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON10_HPP_

# include "Geometry/RefGeometricElt/RefGeomElt.hpp"
# include "Geometry/GeometricElt/Advanced/TGeometricElt.hpp"
# include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

# include "Geometry/RefGeometricElt/Instances/Tetrahedron/Tetrahedron10.hpp"


namespace MoReFEM
{


    /*!
     * \brief A Tetrahedron10 geometric element read in a mesh.
     *
     * We are not considering here a generic Tetrahedron10 object (that's the role of MoReFEM::RefGeomEltNS::Tetrahedron10), but
     * rather a specific geometric element of the mesh, with for instance the coordinates of its coord,
     * the list of its interfaces and so forth.
     */
    class Tetrahedron10 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Tetrahedron10>
    {
    public:

        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Tetrahedron10(unsigned int mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Tetrahedron10(unsigned int mesh_unique_id,
                               const Coords::vector_unique_ptr& mesh_coords_list,
                               std::istream& stream);

        //! \copydoc doxygen_hide_geom_elt_vector_coords_constructor
        explicit Tetrahedron10(unsigned int mesh_unique_id,
                        const Coords::vector_unique_ptr& mesh_coords_list,
                        std::vector<unsigned int>&& coords_index_list);

        //! Destructor.
        ~Tetrahedron10() override;

        //! \copydoc doxygen_hide_copy_constructor
        Tetrahedron10(const Tetrahedron10& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Tetrahedron10(Tetrahedron10&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Tetrahedron10& operator=(const Tetrahedron10& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Tetrahedron10& operator=(Tetrahedron10&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

    private:

        //! Reference geometric element.
        static const RefGeomEltNS::Tetrahedron10& StaticRefGeomElt();


    };



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_TETRAHEDRON10_HPP_
