/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Utilities/Containers/Print.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Mesh/Mesh.hpp"
#include "Geometry/Domain/Domain.hpp"


namespace MoReFEM
{

    
    GeometricElt::GeometricElt(const unsigned int mesh_unique_id)
    : mesh_identifier_(mesh_unique_id)
    { }
    
    
    GeometricElt::~GeometricElt() = default;
    
    
    GeometricElt::GeometricElt(const unsigned int mesh_unique_id,
                               const Coords::vector_unique_ptr& mesh_coords_list,
                               unsigned int index,
                               std::vector<unsigned int>&& coords)
    : GeometricElt(mesh_unique_id)
    {
        index_ = index;
        SetCoordsList(mesh_coords_list, std::move(coords));
    }

    
    GeometricElt::GeometricElt(const unsigned int mesh_unique_id,
                               const Coords::vector_unique_ptr& mesh_coords_list,
                               std::vector<unsigned int>&& coords)
    : GeometricElt(mesh_unique_id,
                   mesh_coords_list,
                   NumericNS::UninitializedIndex<decltype(index_)>(),
                   std::move(coords))
    { }    
    
    
    void GeometricElt::SetCoordsList(const Coords::vector_unique_ptr& mesh_coords_list,
                                     std::vector<unsigned int>&& coords_list)
    {
        assert(!mesh_coords_list.empty());
        using difference_type = Coords::vector_unique_ptr::difference_type;

        for (auto coord_index : coords_list)
        {
            auto match_function = [coord_index](const auto& coord_ptr)
            {
                assert(!(!coord_ptr));
                return coord_ptr->GetIndex() == coord_index;
            };
            
            
            // In all likeliness, coord_index will be at the coord_index-th position in the vector, plus or
            // minus one possibly due to numbering convention. Hence this first look-up.
            // \todo Check these conditions are correctly calibrated for most cases!
            auto begin = mesh_coords_list.cbegin();
            auto end = mesh_coords_list.cend();
            
            auto it_begin_guess = begin + static_cast<difference_type>(coord_index) - 1;
            if (it_begin_guess < begin || it_begin_guess >= end)
                it_begin_guess = begin;
            
            auto it_end_guess = begin + static_cast<difference_type>(coord_index) + 2;
            if (it_end_guess > end)
                it_end_guess = end;

            assert(it_begin_guess <= it_end_guess);
            
            auto it = std::find_if(it_begin_guess, it_end_guess, match_function);
            
            // If not in the narrow guess range, look for it in the entire container.
            if (it == end)
                it = std::find_if(mesh_coords_list.cbegin(), end, match_function);
            
            assert(it != end);
            assert(*it);
            coords_list_.push_back(it->get()); // Store raw pointer.
        }
    }

    
    const Coords& GeometricElt::GetCoord(unsigned int i) const
    {
        assert(i < coords_list_.size());
        assert(!(!coords_list_[i]));
        return *coords_list_[i];
    }
    
    
    template<>
    void GeometricElt
    ::BuildInterface<Vertex>(const GeometricElt* geom_elt_ptr,
                             Vertex::InterfaceMap& interface_list)
    {
        BuildVertexList(geom_elt_ptr,
                        interface_list);
    }
    
       
    template<>
    void GeometricElt
    ::BuildInterface<Edge>(const GeometricElt* geom_elt_ptr,
                           Edge::InterfaceMap& interface_list)
    {
        BuildEdgeList(geom_elt_ptr,
                      interface_list);
    }


    template<>
    void GeometricElt
    ::BuildInterface<Face>(const GeometricElt* geom_elt_ptr,
                           Face::InterfaceMap& interface_list)
    {
        BuildFaceList(geom_elt_ptr,
                      interface_list);
    }

    
    
    template<>
    void GeometricElt
    ::BuildInterface<Volume>(const GeometricElt* geom_elt_ptr,
                             Volume::InterfaceMap& interface_list)
    {
        BuildVolumeList(geom_elt_ptr,
                        interface_list);
    }
     
        
} // namespace MoReFEM 


/// @} // addtogroup GeometryGroup
