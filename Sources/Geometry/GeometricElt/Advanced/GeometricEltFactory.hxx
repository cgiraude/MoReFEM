/*!
//
// \file
//
//
// Created by Sebastien Gilles <srpgilles@gmail.com> on the Tue, 5 Feb 2013 11:59:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_FACTORY_HXX_
# define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_FACTORY_HXX_





namespace MoReFEM
{


    namespace Advanced
    {


        namespace Impl
        {


            // Forward declarations here; definitions are later in this file.
            template<class GeometricEltT>
            inline void RegisterEnsight(std::true_type,
                                        GeometricEltFactory::CreateGeometricEltCallBackIstream ensight_create,
                                        GeometricEltFactory::CallBackEnsight& call_back_ensight);

            template<class GeometricEltT>
            inline void RegisterEnsight(std::false_type,
                                        GeometricEltFactory::CreateGeometricEltCallBackIstream ensight_create,
                                        GeometricEltFactory::CallBackEnsight& call_back_ensight);

            template<class GeometricEltT>
            inline void RegisterMedit(std::false_type,
                                      GeometricEltFactory::CreateGeometricEltCallBack default_create,
                                      GeometricEltFactory::CallBackMedit& call_back_medit,
                                      std::map<GmfKwdCod, unsigned int>& Nnode_medit);

            template<class GeometricEltT>
            void RegisterMedit(std::true_type,
                               GeometricEltFactory::CreateGeometricEltCallBack default_create,
                               GeometricEltFactory::CallBackMedit& call_back_medit,
                               std::map<GmfKwdCod, unsigned int>& Nnode_medit);


        } // namespace Impl


        inline const std::unordered_set<std::string>& GeometricEltFactory::GetNameList() const
        {
            return geometric_elt_name_list_;
        }


        template<class RefGeomEltT>
        bool GeometricEltFactory::RegisterGeometricElt(CreateGeometricEltCallBack default_create,
                                                       CreateGeometricEltCallBackIstream ensight_create)
        {

            if (!geometric_elt_name_list_.insert(RefGeomEltT::traits::ClassName()).second)
            {
                auto&& exception =
                    ExceptionNS::Factory::UnableToRegister(RefGeomEltT::traits::ClassName(),
                                                           "name", __FILE__, __LINE__);
                ThrowBeforeMain(std::move(exception));
            }


            if (!callbacks_.insert(make_pair(RefGeomEltT::traits::Identifier(), default_create)).second)
            {
                auto&& exception =
                    ExceptionNS::Factory::UnableToRegister(RefGeomEltT::traits::ClassName(),
                                                           "identifier", __FILE__, __LINE__);
                ThrowBeforeMain(std::move(exception));
            }

            // Fill geom_ref_elt_type_list_.
            {
                auto geom_ref_elt_type = std::make_shared<RefGeomEltT>();

                if (!geom_ref_elt_type_list_.insert(make_pair(RefGeomEltT::traits::Identifier(), geom_ref_elt_type)).second)
                {
                    auto&& exception =
                        ExceptionNS::Factory::UnableToRegister(RefGeomEltT::traits::ClassName(),
                                                               "identifier", __FILE__, __LINE__);
                    ThrowBeforeMain(std::move(exception));
                }
            }

            // Register as Ensight geometric element if relevant.
            using support_ensight_type =
                typename Internal::MeshNS::FormatNS::Support
                <
                    ::MoReFEM::MeshNS::Format::Ensight,
                    RefGeomEltT::traits::Identifier()
                >;

            constexpr const bool do_register_ensight =
                typename Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,
                                                             RefGeomEltT::traits::Identifier()>();

            if (do_register_ensight)
                Impl::RegisterEnsight<RefGeomEltT>(support_ensight_type(),
                                                   ensight_create,
                                                   callbacks_ensight_);
            // Register as Medit geometric element if relevant.
            using support_medit_type =
                typename Internal::MeshNS::FormatNS::Support
                <
                    ::MoReFEM::MeshNS::Format::Medit,
                    RefGeomEltT::traits::Identifier()
                >;


            constexpr const bool do_register_medit =
                typename Internal::MeshNS::FormatNS::Support
                <
                    ::MoReFEM::MeshNS::Format::Medit,
                    RefGeomEltT::traits::Identifier()
                >();

            if (do_register_medit)
                Impl::RegisterMedit<RefGeomEltT>(support_medit_type(),
                                             default_create,
                                             callbacks_medit_,
                                             Ncoord_medit_);

            static_assert(do_register_ensight || do_register_medit,
                          "At least one format must be associated to a geometric element!"
                          "Specifically, there should be a template specialization of class Internal::MeshNS::Support "
                          "for at least one of the supported format that inherits from std::true_type; this overload "
                          "must be accessible in the RefGeomElt instantiation.");

            // Register the matching between RefGeomElt name and the GeometricEnum.
            if (!match_name_enum_.insert(make_pair(RefGeomEltT::traits::ClassName(), RefGeomEltT::traits::Identifier())).second)
            {
                auto&& exception =
                    ExceptionNS::Factory::UnableToRegister(RefGeomEltT::traits::ClassName(),
                                                           "name", __FILE__, __LINE__);
                ThrowBeforeMain(std::move(exception));
            }


            return true;
        }


        inline GeometricEltFactory::CallBack::size_type GeometricEltFactory::NgeometricElt() const
        {
            return callbacks_.size();
        }


        inline bool GeometricEltFactory::IsEnsightName(const std::string& name) const
        {
            return callbacks_ensight_.find(name) != callbacks_ensight_.cend();
        }


        inline const GeometricEltFactory::CallBackMedit& GeometricEltFactory::MeditRefGeomEltList() const
        {
            return callbacks_medit_;
        }


        inline unsigned int GeometricEltFactory::NumberOfCoordsInGeometricElt(GmfKwdCod code) const
        {
            auto it = Ncoord_medit_.find(code);
            assert("If raised something went wrong in geometric elementregistration" && it != Ncoord_medit_.cend());
            return it->second;
        }


        inline const RefGeomElt& GeometricEltFactory
        ::GetRefGeomElt(Advanced::GeometricEltEnum identifier) const
        {
            auto pointer = GetRefGeomEltPtr(identifier);
            assert(!(!pointer));
            return *pointer;
        }


        // Definitions of template functions declared above.
        namespace Impl
        {


            template<class GeometricEltT>
            inline void RegisterEnsight(std::true_type,
                                        GeometricEltFactory::CreateGeometricEltCallBackIstream ensight_create,
                                        GeometricEltFactory::CallBackEnsight& call_back_ensight)
            {
                const std::string& ensight_name =
                    Internal::MeshNS::FormatNS::Support
                    <
                        ::MoReFEM::MeshNS::Format::Ensight,
                        GeometricEltT::traits::Identifier()
                    >::EnsightName();


                if (!ensight_create || !call_back_ensight.insert(std::make_pair(ensight_name, ensight_create)).second)
                    ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(ensight_name, "ensight",
                                                                           __FILE__, __LINE__));
            }


            template<class GeometricEltT>
            inline void RegisterEnsight(std::false_type,
                                        GeometricEltFactory::CreateGeometricEltCallBackIstream ensight_create,
                                        GeometricEltFactory::CallBackEnsight& call_back_ensight)
            {
                // ensight_create should be nullptr when no Ensight support!
                assert(!ensight_create);

                // to avoid compilation warnings.
                (void) ensight_create;
                (void) call_back_ensight;
            }


            template<class GeometricEltT>
            inline void RegisterMedit(std::false_type,
                                      GeometricEltFactory::CreateGeometricEltCallBack default_create,
                                      GeometricEltFactory::CallBackMedit& call_back_medit,
                                      std::map<GmfKwdCod, unsigned int>& Nnode_medit)
            {
                // To avoid compilation warnings.
                (void) call_back_medit;
                (void) Nnode_medit;
                (void) default_create;
            }


            template<class GeometricEltT>
            void RegisterMedit(std::true_type,
                               GeometricEltFactory::CreateGeometricEltCallBack default_create,
                               GeometricEltFactory::CallBackMedit& call_back_medit,
                               std::map<GmfKwdCod, unsigned int>& Nnode_medit)
            {
                auto medit_id = Internal::MeshNS::FormatNS::Support
                                <
                                    ::MoReFEM::MeshNS::Format::Medit,
                                    GeometricEltT::traits::Identifier()
                                >::MeditId();

                if (!call_back_medit.insert(std::make_pair(medit_id, default_create)).second)
                    ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(GeometricEltT::traits::ClassName(),
                                                                           "medit_id",
                                                                           __FILE__, __LINE__));

                const unsigned int Ncoords = static_cast<unsigned int>(GeometricEltT::traits::Ncoords);

                if (!Nnode_medit.insert(std::make_pair(medit_id, Ncoords)).second)
                    ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(GeometricEltT::traits::ClassName(),
                                                                           "medit_id",
                                                                           __FILE__, __LINE__));
            }




        } // namespace Impl


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_GEOMETRIC_ELT_FACTORY_HXX_
