/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Jan 2013 16:47:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <sstream>
#include <algorithm>
#include <cassert>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{

    
    namespace Advanced
    {


        GeometricEltFactory::~GeometricEltFactory() = default;
        
        
        const std::string GeometricEltFactory::ClassName()
        {
            static std::string ret("GeometricEltFactory");
            return ret;
        }
        


        GeometricEltFactory::GeometricEltFactory()
        {
            callbacks_ensight_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            callbacks_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            geom_ref_elt_type_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
            geometric_elt_name_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        }
        
        
        GeometricElt::unique_ptr GeometricEltFactory
        ::CreateFromEnsightName(unsigned int mesh_unique_id,
                                const Coords::vector_unique_ptr& mesh_coords_list,
                                const std::string& geometric_elt_name,
                                std::istream& stream) const
        {
            CallBackEnsight::const_iterator it = callbacks_ensight_.find(geometric_elt_name);

            if (it == callbacks_ensight_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidEnsightGeometricEltName(geometric_elt_name,
                                                                                         __FILE__, __LINE__);
            
            auto ret = it->second(mesh_unique_id,
                                  mesh_coords_list,
                                  stream);
            
            // If stream state is invalid return nullptr instead!
            if (stream)
                return ret;
            
            return nullptr;
        }

             
        RefGeomElt::shared_ptr GeometricEltFactory
        ::GetRefGeomEltPtr(Advanced::GeometricEltEnum identifier) const
        {
            auto it = geom_ref_elt_type_list_.find(identifier);
            
            if (it == geom_ref_elt_type_list_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltId(identifier, __FILE__, __LINE__);
            
            const auto& pointer = it->second;
            assert(!(!pointer));
            
            return pointer;
        }
        
        
        Advanced::GeometricEltEnum GeometricEltFactory
        ::GetIdentifier(const std::string& geometric_reference_name) const
        {
            auto it = match_name_enum_.find(geometric_reference_name);
            
            if (it == match_name_enum_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltName(geometric_reference_name,
                                                                                          __FILE__, __LINE__);
            
            return it->second;
        }
        
        
        const std::string GeometricEltFactory::EnsightGeometricEltNames() const
        {    
            if (callbacks_ensight_.empty())
                throw MoReFEM::Exception("No Ensight geometric elements defined!!!", __FILE__, __LINE__);
            
            std::ostringstream oconv;
            Utilities::PrintContainer<Utilities::PrintPolicyNS::Key>::Do(callbacks_ensight_, oconv, ", ", "[", "]");
            return oconv.str();
        }


        GeometricElt::unique_ptr GeometricEltFactory::CreateFromIdentifier(Advanced::GeometricEltEnum identifier,
                                                                           unsigned int mesh_unique_id) const
        {
            const auto it = callbacks_.find(identifier);

            if (it == callbacks_.cend())
                throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltId(identifier,
                                                                                __FILE__, __LINE__);

            return it->second(mesh_unique_id);
        }
        
                
    } // namespace Advanced

    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
