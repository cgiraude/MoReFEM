/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Oct 2014 11:45:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HPP_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HPP_

# include <memory>
# include <vector>

# include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
# include "Geometry/Interfaces/Instances/OrientedFace.hpp"
# include "Geometry/Interfaces/Internal/ComputeOrientation.hpp"
# include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace InterfaceNS
        {



            /*!
             * \brief Creates the list of \a Edge if it is relevant for \a TopologyT.
             *
             * \tparam TopologyT Topology currently considered.
             *
             * \param[in] geom_elt_ptr Pointer to the \a GeometricElt for which we want to determine edges.
             * \param[in] coords_list_in_geom_elt \a Coords in the \a GeometricElt.
             * \param[in,out] existing_list List of all edges found so far (in the whole mesh, not only for
             * current geometric element).
             */
            template<class TopologyT>
            OrientedEdge::vector_shared_ptr ComputeEdgeList(const GeometricElt* geom_elt_ptr,
                                                            const Coords::vector_raw_ptr& coords_list_in_geom_elt,
                                                            Edge::InterfaceMap& existing_list);


            /*!
             * \brief Creates the list of \a Face if it is relevant for \a TopologyT.
             *
             * \tparam TopologyT Topology currently considered.
             *
             * \param[in] geom_elt_ptr Pointer to the \a GeometricElt for which we want to determine faces.
             * \param[in] coords_list_in_geom_elt \a Coords in the \a GeometricElt.
             * \param[in,out] existing_list List of all edges found so far (in the whole mesh, not only for
             * current geometric element).
             */
            template<class TopologyT>
            OrientedFace::vector_shared_ptr ComputeFaceList(const GeometricElt* geom_elt_ptr,
                                                            const Coords::vector_raw_ptr& coords_list_in_geom_elt,
                                                            Face::InterfaceMap& existing_list);


        } // namespace InterfaceNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Interfaces/Internal/BuildInterfaceListHelper.hxx"


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_BUILD_INTERFACE_LIST_HELPER_HPP_
