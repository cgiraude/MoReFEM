/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 25 Mar 2014 11:23:13 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_EDGE_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_EDGE_HXX_


namespace MoReFEM
{


    template<class LocalEdgeContentT>
    Edge::Edge(const LocalEdgeContentT& local_edge_content, const Coords::vector_raw_ptr& elt_coords_list)
    : Internal::InterfaceNS::TInterface<Edge, InterfaceNS::Nature::edge>(local_edge_content, elt_coords_list)
    { }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_EDGE_HXX_
