/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 31 Mar 2014 10:54:52 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VOLUME_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VOLUME_HXX_


namespace MoReFEM
{




} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_VOLUME_HXX_
