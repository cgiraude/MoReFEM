/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 27 Mar 2014 12:36:09 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HXX_
# define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HXX_



namespace MoReFEM
{


    template<class LocalContentT>
    Interface::Interface(const LocalContentT& local_content, const Coords::vector_raw_ptr& elt_coords_list)
    {
        Coords::vector_raw_ptr content;

        for (auto vertex_index : local_content)
        {
            assert(vertex_index < elt_coords_list.size());
            content.push_back(elt_coords_list[vertex_index]);
        }

        // Set the coordinates and shuffle the points.
        SetVertexCoordsList(content);
    }



    inline void Interface::SetIndex(unsigned int id) noexcept
    {
        id_ = id;
    }


    inline unsigned int Interface::GetIndex() const noexcept
    {
        assert(id_ != NumericNS::UninitializedIndex<unsigned int>());
        return id_;
    }


    inline const Coords::vector_raw_ptr& Interface::GetVertexCoordsList() const noexcept
    {
        #ifndef NDEBUG
        if (GetNature() == InterfaceNS::Nature::volume)
            assert(vertex_coords_list_.empty() && "A specialization should be called for Volume!");
        #endif // NDEBUG

        return vertex_coords_list_;
    }


    inline bool operator==(const Interface& lhs, const Interface& rhs) noexcept
    {
        if (lhs.GetNature() != rhs.GetNature())
            return false;

        assert(lhs.GetIndex() != NumericNS::UninitializedIndex<unsigned int>());
        assert(rhs.GetIndex() != NumericNS::UninitializedIndex<unsigned int>());
        return lhs.GetIndex() == rhs.GetIndex();
    }


    inline bool operator<(const Interface& lhs, const Interface& rhs) noexcept
    {
        const auto lhs_nature = lhs.GetNature();
        const auto rhs_nature = rhs.GetNature();

        if (lhs_nature != rhs_nature)
            return lhs_nature < rhs_nature;

        assert(lhs.GetIndex() != NumericNS::UninitializedIndex<unsigned int>());
        assert(rhs.GetIndex() != NumericNS::UninitializedIndex<unsigned int>());
        return lhs.GetIndex() < rhs.GetIndex();
    }


    inline bool operator!=(const Interface& lhs, const Interface& rhs) noexcept
    {
        return !(operator==(lhs, rhs));
    }


    inline const LocalVector& Interface::GetPseudoNormal() const noexcept
    {
        assert(!(!pseudo_normal_));
        return *pseudo_normal_;
    }

    inline LocalVector& Interface::GetNonCstPseudoNormal() noexcept
    {
        return const_cast<LocalVector&>(GetPseudoNormal());
    }


    inline const std::unique_ptr<LocalVector>& Interface::GetPseudoNormalPtr() const noexcept
    {
        return pseudo_normal_;
    }




} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERFACE_HXX_
