/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 28 Jun 2013 14:07:54 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <sstream>
#include <cassert>

#include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace // anonymous
{
    // Only declarations are provided here; definitions are at the end of this file
    
    std::string FileInformation(const std::string& input_file);
    std::string UnableToOpenFileMsg(const std::string& input_file);
    std::string UnsupportedGeometricEltMsg(const std::string& geometric_elt_identifier, const std::string& format);
    
    
} // namespace anonymous



namespace MoReFEM
{
    
    
    namespace ExceptionNS
    {
        
        
        namespace Format
        {
            
            
            Exception::~Exception() = default;
            
            
            Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
            : MoReFEM::Exception(msg, invoking_file, invoking_line)
            { }
            
            
            UnableToOpenFile::~UnableToOpenFile() = default;
            
            
            UnableToOpenFile::UnableToOpenFile(const std::string& input_file, const char* invoking_file, int invoking_line)
            : Exception(UnableToOpenFileMsg(input_file), invoking_file, invoking_line)
            { }
            
            
            UnsupportedGeometricElt::~UnsupportedGeometricElt() = default;
            
            
            UnsupportedGeometricElt::UnsupportedGeometricElt(const std::string& geometric_elt_identifier,
                                                             const std::string& format,
                                                             const char* invoking_file, int invoking_line)
            : Exception(UnsupportedGeometricEltMsg(geometric_elt_identifier, format),
                        invoking_file, invoking_line)
            { }
            
            
            
        } // namespace Format
        
        
    } // namespace ExceptionNS
    
    
} // namespace MoReFEM



// Definitions are provided here; declarations were provided at the beginning of this file
namespace // anonymous
{
    
    
    std::string FileInformation(const std::string& input_file)
    {
        std::ostringstream oconv;
        oconv << "Error in Format file ";
        oconv << input_file << ": ";
        return oconv.str();
    }
    
        
    std::string UnableToOpenFileMsg(const std::string& input_file)
    {
        std::ostringstream oconv;
        oconv << FileInformation(input_file);
        oconv << "Unable to open file";
        
        return oconv.str();
    }
    
    
    std::string UnsupportedGeometricEltMsg(const std::string& geometric_elt_identifier, const std::string& format)
    {
        std::ostringstream oconv;
        oconv << "Can't write in " << format << " format: geometric element'" << geometric_elt_identifier
        << "' is not supported";
        return oconv.str();
    }
    
    
} // namespace anonymous


/// @} // addtogroup GeometryGroup
