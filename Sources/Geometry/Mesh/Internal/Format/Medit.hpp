/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HPP_

# include <memory>
# include <vector>

# include "Geometry/GeometricElt/GeometricElt.hpp"
# include "Geometry/Mesh/Internal/Format/Exceptions/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                /*!
                 * \brief Specialization of Informations struct that provides generic informations about Medit format.
                 */
                template<>
                struct Informations<::MoReFEM::MeshNS::Format::Medit>
                {

                    //! Name of the format.
                    static const std::string& Name();

                    //! Extension of the Medit files.
                    static const std::string& Extension();

                };


                namespace Medit
                {


                    /*!
                     * \brief Read a mesh in Medit format.
                     *
                     * \copydoc doxygen_hide_mesh_constructor_1_bis
                     * \copydoc doxygen_hide_mesh_constructor_3_bis
                     * \copydoc doxygen_hide_mesh_constructor_4
                     */
                    void ReadFile(const unsigned int unique_id,
                                  const std::string& mesh_file,
                                  double space_unit,
                                  unsigned int& dimension,
                                  GeometricElt::vector_shared_ptr& unsort_element_list,
                                  Coords::vector_unique_ptr& coords_list,
                                  MeshLabel::vector_const_shared_ptr& mesh_label_list);


                    /*!
                     * \brief Write a mesh in Medit format.
                     *
                     * \copydoc doxygen_hide_geometry_format_write_common_arg
                     * \param[in] version Version of Medit to use (usually choose '2'; '1' is for float precision).
                     */
                    void WriteFile(const Mesh& mesh,
                                   const std::string& mesh_file,
                                   int version = 2);


                } // namespace Medit


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Internal/Format/Medit.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HPP_
