/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HXX_
# define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ColoringNS
        {




        } // namespace ColoringNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_COLORING_HXX_
