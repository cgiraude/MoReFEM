/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_MESH_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_MESH_HPP_

# include <string>
# include <iostream>
# include <memory>
# include <cassert>
# include <unordered_map>

# include "Utilities/UniqueId/UniqueId.hpp"
# include "Utilities/Mpi/Mpi.hpp"
# include "Utilities/Filesystem/Directory.hpp"

# include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

# include "Core/InputData/InputData.hpp"

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/Mesh/Internal/GeometricEltList.hpp"

# include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"
# include "Geometry/Mesh/Internal/Format/Format.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    namespace Internal
    {

        class PseudoNormalsManager;


        namespace MeshNS
        {


            class MeshManager;


        } // namespace MeshNS


        namespace FEltSpaceNS
        {


            struct ReduceMesh;


        } // namespace FEltSpaceNS


    } // namespace Internal




    namespace RefGeomEltNS
    {


        namespace Traits
        {


            class Triangle3;


        }


    }


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Class responsible for a mesh and its content.
     *
     * Objects of this class can only be created through the MeshManager.
     */
    class Mesh final
    : public Crtp::UniqueId<Mesh, UniqueIdNS::AssignationMode::manual>
    {

    public:

        /*!
         * \class doxygen_hide_mesh_enum
         * \name Convenient enum classes.
         *
         * Their purpose is to make Init() reading more palatable:
         * \code
         * Mesh mesh;
         * mesh.Init(mesh_file, format, BuildEdge::yes, BuildFace::no);
         * \endcode
         * instead of:
         * \code
         * Mesh mesh;
         * mesh.Init(mesh_file, format, true, false);
         * \endcode
         * As a bonus, there is a type checking involved that prevent messing up the order.
         */

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildEdge { no, yes };

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildFace { no, yes };

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildVolume { no, yes };

        //! \copydoc doxygen_hide_mesh_enum
        enum class BuildPseudoNormals { no, yes };

    public:

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<const Mesh>;

        //! Convenient alias to parent.
        using unique_id_parent = Crtp::UniqueId<Mesh, UniqueIdNS::AssignationMode::manual>;

        //! Alias to vector of unique_ptr.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;

        //! Convenient alias.
        using iterator_geometric_element = GeometricElt::vector_shared_ptr::const_iterator;

        //! Alias for subset.
        using subset_range = std::pair<iterator_geometric_element, iterator_geometric_element>;

        //! Returns the name of the class.
        static const std::string& ClassName();


        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        // \todo #9 Because it creates a spurious warning; I would like though the friendship to ultimately appear.
        // ============================

        //! Friendship.
        friend Internal::MeshNS::MeshManager;

        //! Friendship.
        friend Internal::FEltSpaceNS::ReduceMesh;

        //! Friendship.
        friend Internal::PseudoNormalsManager;

        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================


    private:

        /// \name Special members.
        ///@{

        // BEWARE: if dimension is in/out, use the 3_bis comment!
        /*!
         * \class doxygen_hide_mesh_constructor_1
         *
         * \param[in] unique_id Unique identifier of the \a Mesh.
         * \param[in] dimension Dimension of the mesh.
         * \copydoc doxygen_hide_space_unit_arg
         */


        /*!
         * \class doxygen_hide_mesh_constructor_1_bis
         *
         * \param[in] unique_id Unique identifier of the \a Mesh.
         * \param[in,out] dimension Dimension of the mesh read in the input file.
         * \copydoc doxygen_hide_space_unit_arg
         */


        /*!
         * \class doxygen_hide_mesh_constructor_2
         *
         * \param[in] do_build_edge Whether edges should be built or not.
         * \param[in] do_build_face Whether faces should be built or not.
         * \param[in] do_build_volume Whether volumes should be built or not.
         * \param[in] do_build_pseudo_normals Whether pseudo-normals should be built or not.
         */

        // BEWARE: if parameters are in/out, use the 3_bis comment!
        /*!
         * \class doxygen_hide_mesh_constructor_3
         *
         * \param[in] unsort_element_list List of \a GeometricElt. No specific order is expected here.
         * \param[in] coords_list List of \a Coords objects.
         * \param[in] mesh_label_list List of \a MeshLabels.
         */


        /*!
         * \class doxygen_hide_mesh_constructor_3_bis
         *
         * \param[in,out] unsort_element_list List of \a GeometricElt. No specific order is expected here.
         * \param[in,out] coords_list List of \a Coords objects.
         * \param[in,out] mesh_label_list List of \a MeshLabels.
         */


        /*!
         * \class doxygen_hide_mesh_constructor_4
         *
         * \param[in] mesh_file File from which the data will be loaded.
         */

        /*!
         * \class doxygen_hide_mesh_constructor_5
         *
         * \copydoc doxygen_hide_mesh_constructor_4
         * \param[in] format Format of the input file.
         */



        /*!
         * \brief Constructor from a file.
         *
         * The constructor is private on purpose: Mesh objects are intended to be created
         * through MeshManager singleton class, which performs additional bookkeeping on them.
         *
         * \copydoc doxygen_hide_mesh_constructor_1
         * \copydoc doxygen_hide_mesh_constructor_2
         * \copydoc doxygen_hide_mesh_constructor_5
         */
        explicit Mesh(unsigned int unique_id,
                      const std::string& mesh_file,
                      unsigned dimension,
                      MeshNS::Format format,
                      const double space_unit,
                      BuildEdge do_build_edge = BuildEdge::no,
                      BuildFace do_build_face = BuildFace::no,
                      BuildVolume do_build_volume = BuildVolume::no,
                      BuildPseudoNormals do_build_pseudo_normals = BuildPseudoNormals::no);


        /*!
         * \brief Construct a Mesh given the list of \a Coords, \a GeometricElt and \a MeshLabel.
         *
         * The constructor is private on purpose: Mesh objects are intended to be created
         * through MeshManager singleton class, which performs additional bookkeeping on them.
         *
         * \copydoc doxygen_hide_mesh_constructor_1
         * \copydoc doxygen_hide_mesh_constructor_2
         * \copydoc doxygen_hide_mesh_constructor_3
         */
        explicit Mesh(unsigned int unique_id,
                      unsigned int dimension,
                      double space_unit,
                      GeometricElt::vector_shared_ptr&& unsort_element_list,
                      Coords::vector_unique_ptr&& coords_list,
                      MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                      Mesh::BuildEdge do_build_edge,
                      Mesh::BuildFace do_build_face,
                      Mesh::BuildVolume do_build_volume,
                      Mesh::BuildPseudoNormals do_build_pseudo_normals = BuildPseudoNormals::no);


        /*!
         * \brief Constructor from pre-partitioned data.
         *
         * \copydoc doxygen_hide_mpi_param
         * \copydoc doxygen_hide_mesh_constructor_5
         * \param[in] unique_id Unique identifier of the \a Mesh to be created.
         * \param[in] prepartitioned_data Lua file which gives the data needed to reconstruct the data
         * from pre-computed partitioned data. Note: it is not const as such objects relies on a Lua stack
         * which is modified for virtually each operation.
         * \param[in,out] dimension Dimension of the mesh read in the input file.
         * \copydoc doxygen_hide_mesh_constructor_2
         * \copydoc doxygen_hide_space_unit_arg
         */
        explicit Mesh(const Wrappers::Mpi& mpi,
                      unsigned int unique_id,
                      const std::string& mesh_file,
                      LuaOptionFile& prepartitioned_data,
                      unsigned dimension,
                      MeshNS::Format format,
                      const double space_unit,
                      BuildEdge do_build_edge = BuildEdge::no,
                      BuildFace do_build_face = BuildFace::no,
                      BuildVolume do_build_volume = BuildVolume::no,
                      BuildPseudoNormals do_build_pseudo_normals = BuildPseudoNormals::no);



    public:

        //! Destructor.
        ~Mesh();

        //! \copydoc doxygen_hide_copy_constructor - disabled.
        Mesh(const Mesh& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Mesh(Mesh&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Mesh& operator=(const Mesh& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Mesh& operator=(Mesh&& rhs) = delete;

        ///@}



        //! Get the dimension.
        unsigned int GetDimension() const noexcept;

        //! List of all geometric elements that belong to the same label.
        //! \param[in] label \a MeshLabel used as filter.
        GeometricElt::vector_shared_ptr GeometricEltListInLabel(const MeshLabel::const_shared_ptr& label) const;

        //! Print the number of geometric elements for each label (for debug purposes).
        //! \copydoc doxygen_hide_stream_inout
        void PrintGeometricEltsInLabel(std::ostream& stream) const;

        //! Number of processor-wise \a Coords.
        unsigned int NprocessorWiseCoord() const noexcept;

        /*!
         * \brief Number of \a Coords that are exclusively associated to ghost \a NodeBearer.
         *
         * Should not be useful to develop a model; it's there for two usages only:
         * - Movemesh implementation within the library.
         * - Some integration tests.
         *
         * \return Number of \a Coords that are exclusively associated to ghost \a NodeBearer.
         */
         unsigned int NghostCoord() const noexcept;

        /*!
         * \brief A vector that contains a pointer to a default geometric element for each type encountered in the mesh.
         *
         * For instance the vector would return a pointer to default Triangle3 and Tetraedre4 geometric elements
         * if the mesh is constituted solely of these two types of elements.
         *
         * \return One RefGeomElt object if GeometricElt of the same type were found.
         */
        RefGeomElt::vector_shared_ptr BagOfEltType() const;

        /*!
         * \brief Same as namesake methods except we limit ourselves to a given dimension.
         *
         * \param[in] dimension Only objects of this dimension will be considered.
         *
         * \return One RefGeomElt object if GeometricElt of the same type were found.
         */
        RefGeomElt::vector_shared_ptr BagOfEltType(unsigned int dimension) const;

        /*!
         * \brief Obtains all the geometric elements of a given type.
         *
         * \param[in] geometric_type Type of the geometric element requested.
         *
         * \return Pair of iterators that delimits the required subset.
         *
         * \code
         * auto subset = GetSubsetGeometricEltList(geometric_type); // geometric_type is assumed to be properly initialized.
         * for (auto it = subset.first; it != subset.second; ++it)
         * {
         *      ... loop over all elements that share the given type ...
         * }
         *
         * \endcode
         *
         * \internal <b><tt>[internal]</tt></b> The internal structure of the geometric elements storage shouldn't
         * vary much (if at all...) during the program, so you can be fairly assured the iterators won't be invalidated.
         * Nonetheless, do not store them for instance in the class: generate them when they are required.
         * \endinternal
         *
         */
        subset_range GetSubsetGeometricEltList(const RefGeomElt& geometric_type) const;

        /*!
         * \brief Obtains all the geometric elements of a given type belonging to a given label.
         *
         * \param[in] geometric_type Type of the geometric element requested.
         * \param[in] label_index Index of the label considered.
         *
         * \return Pair of iterators that delimits the required subset.
         *
         * See namesake method to see how it should be used.
         */
        subset_range GetSubsetGeometricEltList(const RefGeomElt& geometric_type,
                                               unsigned int label_index) const;

        /*!
         * \brief Return the list of geometric elements of the requested \a dimension.
         *
         * This method does not allocate new dynamic array.
         *
         * \param[in] dimension Dimension used as filter.
         *
         * \return Pair of iterators that delimit all the appropriate  \a GeometricElt.
         */
        subset_range GetSubsetGeometricEltList(unsigned int dimension) const;

        # ifndef NDEBUG
        /*!
         * \brief Check the underlying ordering is what is expected within #Mesh.
         *
         * This is obviously a debug method...
         */
        void CheckGeometricEltOrdering() const;

        # endif // NDEBUG

        //! Get the number of processor-wise vertex.
        unsigned int Nvertex() const;

        //! Get the number of program-wise vertex.
        unsigned int NprogramWiseVertex() const noexcept;

        /*!
         * \class doxygen_hide_mesh_get_geom_elt_from_index_1
         *
         * \brief Return the \a GeometricElt which index (given by GetIndex()) is \a index.
         *
         * \note This method is frankly not efficient at all; it is introduced solely for some PostProcessing need.
         *
         * \param[in] index Index which associated \a GeometricElt is sought.
         *
         * \return \a GeometricElt which index (given by GetIndex()) is \a index.
         */

        //! \copydoc doxygen_hide_mesh_get_geom_elt_from_index_1
        const GeometricElt& GetGeometricEltFromIndex(unsigned int index) const;

        /*!
         * \class doxygen_hide_mesh_get_geom_elt_from_index_2
         *
         * \brief Return the \a GeometricElt which index (given by GetIndex()) is \a index and which type is \a
         * \a ref_geom_elt.
         *
         * \note This method is slightly better than its counterpart without \a ref_geom_elt but the comment
         * about its lack of efficiency still stands.
         *
         * \param[in] index Index which associated \a GeometricElt is sought.
         * \param[in] ref_geom_elt \a RefGeomElt of the sought \a GeometricElt.
         *
         * \return \a GeometricElt which index (given by GetIndex()) is \a index.
         */

        //! \copydoc doxygen_hide_mesh_get_geom_elt_from_index_2
        const GeometricElt& GetGeometricEltFromIndex(unsigned int index,
                                                     const RefGeomElt& ref_geom_elt) const;

    public:

        //! Returns the format in which the mesh was originally read. May be 'None' if undisclosed.
        MeshNS::Format GetInitialFormat() const noexcept;

    public:

        /// \name Edge-related methods.
        ///@{


        //! Tells whether the edges have been built.
        bool AreEdgesBuilt() const;

        //! Get the number of edges. The edges must have been built prior to the call.
        unsigned int Nedge() const;
        ///@}


    public:

        /// \name Face-related methods.
        ///@{

        //! Tells whether the faces have been built.
        bool AreFacesBuilt() const;

        //! Get the number of faces. The faces must have been built prior to the call.
        unsigned int Nface() const;
        ///@}


    public:

        /// \name Volume-related methods.
        ///@{

        //! Tells whether the volumes have been built.
        bool AreVolumesBuilt() const;

        //! Get the number of volumes. The volumes must have been built prior to the call.
        unsigned int Nvolume() const;
        ///@}


        //! Number of geometric elements.
        unsigned int NgeometricElt() const;

        //! Number of geometric elements of a given dimension.
        //! \param[in] dimension Dimension used as filter.
        unsigned int NgeometricElt(unsigned int dimension) const;

        //! Get the \a index -th coord on the processor.
        //! \param[in] index Position of the sought \a Coords in the processor-wise \a Coords list. This index is
        //! the output of Coords::GetPositionInCoordsListInMesh(), NOT Coords::GetIndex() which might not be contiguous.
        const Coords& GetCoord(unsigned int index) const;

        /*!
         * \brief Return the list of geometric elements.
         *
         */
        const GeometricElt::vector_shared_ptr& GetGeometricEltList() const noexcept;

        //! Get the list of all \a Coords on the processor.
        const Coords::vector_unique_ptr& GetProcessorWiseCoordsList() const noexcept;

        /*!
         * \brief Get the list of the \a Coords that are solely related to ghosted \a NodeBearer.
         *
         * A model user shouldn't have to deal with that accessor at all; it is needed publicly for only two reasons:
         * - To move the mesh: movemesh functionality must not forget to move these \a Coords as well. This is something
         * purely internal to the library; model user just call FEltSpace::MoveMesh() method and the library takes care
         * of everything.
         * - In the test that check mesh movement is correct.
         *
         * So to put in a nutshell: unless you are a core library develop that needs to tinker with those aspects, you
         * don't need this method!
         *
         * \return List of \a Coords.
         */
        const Coords::vector_unique_ptr& GetGhostedCoordsList() const noexcept;

        //! Get the list of labels.
        const MeshLabel::vector_const_shared_ptr& GetLabelList() const noexcept;

        //! Get the space unit.
        double GetSpaceUnit() const noexcept;

        /*!
         * \brief Write the mesh into a file in the given format.
         *
         * Several notes and caveat:
         * - Only processor-wise data are written on disk; i.e. there is one mesh per processor.
         * - So you need to use mpi rank in the mesh file name, to avoid overwriting the work of another processor.
         * - Some elements of the original mesh (before data reduction) might be 'lost': for instance if there are
         * no boundary condition on an edge and therefore no specific finite element treatment, it is dropped in the
         * reduction.
         * - The operation will fail if you write in a different format than the initial one and one of the original
         * geometric element is not supported by chosen output format.
         *
         * \param[in] mesh_file Path to the file into which the mesh will be written.
         * \tparam FormatT Format of the file to be written. Read the documentation above about the possible discrepancies
         * of formats.
         */
        template<::MoReFEM::MeshNS::Format FormatT>
        void Write(const std::string& mesh_file) const;

    private:

        /*!
         * \brief Compute the relevant pseudo-normals on the mesh.
         *
         * \param[in] label_index_list List of the label on which compute pseudo normals.
         * \param[in] vertex_interface_list List of \a Vertex.
         * \param[in] edge_interface_list List of \a Edge.
         * \param[in] face_interface_list List of \a Face.
         * \warning Should not be called directly. To compute pseudo-normals use PseudoNormals1 in Lua that will use
         * the dedicated magager to compute them.
         *
         */
        void ComputePseudoNormals(const std::vector<unsigned int>& label_index_list,
                                  const Vertex::InterfaceMap& vertex_interface_list,
                                  const Edge::InterfaceMap& edge_interface_list,
                                  const Face::InterfaceMap& face_interface_list);

    private:

        /*!
         * \class doxygen_hide_reduced_coords_list_param
         *
         * \param[in] processor_wise_coords_list List of processor-wise coords list, devised from the processor-wise
         * list of \a NodeBearer.
         * \param[in] ghosted_coords_list List of ghosted coords list, i.e. \a Coords that are only involved
         * with ghosted \a NodeBearer. If a \a Coords is related to both a processor-wise and a ghost \a NodeBearer,
         * it won't appear in this list (put differently, intersection of processor_wise_coords_list and
         * ghosted_coords_list is the empty set).
         */

        /*!
         * \brief Init a local mesh from a global one.
         *
         * \copydoc doxygen_hide_mpi_param
         * \param[in] processor_wise_geo_element List of all geometric elements that are handled by the local processor.
         * This list must be a subset of the current list of geometric elements.
         * \copydoc doxygen_hide_reduced_coords_list_param
         * \param[in] Nprocessor_wise_vertex Number of processor-wise interfaces that are related to a NodeBearer.
         * Interfaces on processor-wise \a GeomElt but which related \a NodeBearer are managed by another processor
         * aren't counted here!
         * \param[in] Nprocessor_wise_edge Same as Nprocessor_wise_vertex for edges (orientation not considered in this
         * count).
         * \param[in] Nprocessor_wise_face Same as Nprocessor_wise_edge for faces.
         * \param[in] Nprocessor_wise_volume Same as Nprocessor_wise_vertex for volumes.
         */
        void ShrinkToProcessorWise(const Wrappers::Mpi& mpi,
                                   const GeometricElt::vector_shared_ptr& processor_wise_geo_element,
                                   Coords::vector_raw_ptr&& processor_wise_coords_list,
                                   Coords::vector_raw_ptr&& ghosted_coords_list,
                                   unsigned int Nprocessor_wise_vertex,
                                   unsigned int Nprocessor_wise_edge,
                                   unsigned int Nprocessor_wise_face,
                                   unsigned int Nprocessor_wise_volume);

        /*!
         * \brief Set the new coords after reduction to processor-wise.
         *
         * This method is supposed to be called only in ShrinkToProcessorWise().
         *
         * \copydoc doxygen_hide_mpi_param
         * \copydoc doxygen_hide_reduced_coords_list_param
         */
        void SetReducedCoordsList(const Wrappers::Mpi& mpi,
                                  Coords::vector_raw_ptr&& processor_wise_coords_list,
                                  Coords::vector_raw_ptr&& ghosted_coords_list);


    private:

        /*!
         * \brief Constructor helper method.
         *
         * This method should be called by a any constructor, as it builds and sorts the expected content of the class.
         *
         * \copydoc doxygen_hide_mesh_constructor_3
         * \copydoc doxygen_hide_mesh_constructor_2
         */
        void Construct(GeometricElt::vector_shared_ptr&& unsort_element_list,
                       Coords::vector_unique_ptr&& coords_list,
                       MeshLabel::vector_const_shared_ptr&& mesh_label_list,
                       Mesh::BuildEdge do_build_edge,
                       Mesh::BuildFace do_build_face,
                       Mesh::BuildVolume do_build_volume,
                       Mesh::BuildPseudoNormals do_build_pseudo_normals);



        /*!
         * \brief Read a mesh from a file.
         *
         * \param[in] mesh_file File from which the data will be loaded.
         * \param[in] format Format of the input file.

         * \copydoc doxygen_hide_space_unit_arg
         * \copydoc doxygen_hide_mesh_constructor_3
         *
         * (for instance Ensight will always yield 3 here by construct).
         */
        void Read(const std::string& mesh_file,
                  ::MoReFEM::MeshNS::Format format,
                  const double space_unit,
                  GeometricElt::vector_shared_ptr& unsort_element_list,
                  Coords::vector_unique_ptr& coords_list,
                  MeshLabel::vector_const_shared_ptr& mesh_label_list);


        //! \name Helper methods for edges and faces.
        //@{

        /*!
         * \brief Actual implementation of BuildEdgeList or BuildFaceList.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         *
         * \internal <b><tt>[internal]</tt></b> A specialization for Vertex is declared in namespace scope at the end of
         * this file.
         * \endinternal
         *
         * \param[in,out] interface_list List of \a InterfaceT under construct.
         */
        template<class InterfaceT>
        void BuildInterface(typename InterfaceT::InterfaceMap& interface_list);


        /*!
         * \brief Set the number of interfaces of \a InterfaceT type.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         * \param[in] N Number of edges or faces fed to the object.
         */
        template<class InterfaceT>
        void SetNinterface(unsigned int N);


        /*!
         * \brief Determine the number of interfaces of \a InterfaceT type.
         *
         * \tparam InterfaceT Interface type, which derives from Interface base class. Choices are among
         * { Vertex, Edge, Face, Volume }.
         *
         * \return Number of interfaces of type \a InterfaceT.
         */
        template<class InterfaceT>
        unsigned int DetermineNInterface() const;


        //@}


        /*!
         * \brief Reconstruct the labels after reduction to processor-wise data.
         *
         * Only labels involved with \a GeometricElt kept on the local processors are kept.
         */
        void ReduceLabelList();

    private:

        //! Non constant access to the list of all \a Coords on the processor.
        Coords::vector_unique_ptr& GetNonCstProcessorWiseCoordsList() noexcept;

        //! Mutator the list of the \a Coords related ONLY to ghosted \a NodeBearer.
        //! \param[in] list The list of ghosted \a Coords computed outside the \a Mesh.
        void SetGhostedCoordsList(Coords::vector_unique_ptr&& list);

        //! Non constant access to the list of labels.
        MeshLabel::vector_const_shared_ptr& GetNonCstLabelList() noexcept;


    private:

        //! List of all \a Coords on the processor.
        Coords::vector_unique_ptr processor_wise_coords_list_;

        /*!
         * \brief List of \a Coords related ONLY to ghosted \a NodeBearer.
         *
         * Meaningless in sequential.
         *
         * One shouldn't give access to this data, which is purely practical: ghosted \a NodeBearer 's \a Interface
         * needs access to all their \a Coords, and so we must keep track of those after the reduction to processor
         * wise data. However, from the reduced mesh point of view these \a Coords should not be considered at all:
         * another processor is in charge of them.
         */
        Coords::vector_unique_ptr ghosted_coords_list_;

        /*!
         * \brief List of all the geometric elements of the mesh, sort by dimension.
         *
         * Key is the dimension, and value is the list of all geometric elements of that dimension in the mesh.
         *
         * Underlying order is expected to be by geometric element nature (all triangles3, then all triangles6, etc...).
         */
        Internal::MeshNS::GeometricEltList geometric_elt_list_;

        //! Dimension of the mesh.
        const unsigned int dimension_;

        //! All processor-wise labels.
        MeshLabel::vector_const_shared_ptr label_list_;

        /*!
         * \brief Number of built vertex (processor-wise).
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         */
        unsigned int Nprocessor_wise_vertex_ = NumericNS::UninitializedIndex<unsigned int>();

        /*!
         * \brief Number of built vertex (program-wise).
         */
        unsigned int Nprogram_wise_vertex_ = NumericNS::UninitializedIndex<unsigned int>();

        /*!
         * \brief Number of built edges that are related to a NodeBearer.
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         *
         * If BuildEdgeList() has not been called, this quantity is an UninitializedIndex; fetching this quantity
         * should lead to an assert.
         */
        unsigned int Nedge_ = NumericNS::UninitializedIndex<unsigned int>();

        /*!
         * \brief Number of built faces.
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         *
         * If BuildFaceList() has not been called, this quantity is an UninitializedIndex; fetching this quantity
         * should lead to an assert.
         */
        unsigned int Nface_ = NumericNS::UninitializedIndex<unsigned int>();

        /*!
         * \brief Number of built volumes.
         *
         * Interfaces that are present on a processor-wise GeomElt but which NodeBearer is handled by another processor
         * is not counted here.
         *
         * If BuildFaceList() has not been called, this quantity is an UninitializedIndex; fetching this quantity
         * should lead to an assert.
         */
        unsigned int Nvolume_ = NumericNS::UninitializedIndex<unsigned int>();

        //! Space unit of the mesh.
        const double space_unit_;

        //! Format in which the mesh was first read. May be 'None' if undisclosed or not kept.
        MeshNS::Format initial_format_ = MeshNS::Format::None;


    }; // class Mesh


    //! Specialization for Volume.
    template<>
    unsigned int Mesh::DetermineNInterface<Volume>() const;


    /*!
     * \brief Write in the given output directory a file in which all the interfaces are written.
     *
     * This function should be called only on root processor.
     *
     * \param[in] mesh Key is the unique id of the \a Mesh which interface list is to be written; value
     * the path of its associated output directory (which should have already been created).
     *
     * A 'interfaces.hhdata' file will be written inside this output directory.
     */
    void WriteInterfaceList(const std::pair<const unsigned int, FilesystemNS::Directory::const_unique_ptr>& mesh);


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * The comparison is performed using the underlying unique id.
     */
    bool operator==(const Mesh& lhs, const Mesh& rhs);


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Mesh/Mesh.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_MESH_HPP_
