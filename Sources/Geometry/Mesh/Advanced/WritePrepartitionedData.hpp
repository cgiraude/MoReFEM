//! \file
//
//
//  WritePrepartitionedData.hpp
//  MoReFEM
//
//  Created by sebastien on 15/07/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HPP_
# define MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HPP_

# include <string>
# include <cassert>

# include "Geometry/Mesh/Format.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================

    class Mesh;


    namespace Wrappers
    {


        class Mpi;


    } // namespace Wrappers

    // ============================
    // Forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced::MeshNS
    {


        /*!
         * \brief This lightweight class writes on disk the data that will be required to a further run parallel run from the data computed here.
         *
         * If the parallelism strategy is "Precompute", the computation will stop shortly after this class enters the game. If "Parallel", it will do the
         * parallel computation after these data are written. The class isn't used for the others parallelism strategies.
         *
         * This is mostly a function - I made it a class just to provide user-friendly access to the generated
         * files.
         */
        class WritePrepartitionedData
        {

           public:

            /// \name Special members.
            ///@{

            /*!
             * \brief Constructor.
             *
             * \param[in] mesh \a Mesh which partitioning will be written on disk.
             * \param[in] output_directory Directory into which the data are written for a given \a Mesh and a given
             * mpi rank. Typically something like /Volumes/Data/${USER}/MoReFEM/Results/MyModel/Rank_0/Mesh_1.
             * \param[in] format Format into which write the reduced mesh. Should be the same as the format used
             * to read the original mesh (this way you're sure the operation is possible - some types of geometric
             * element are not supported by all formats).
             *
             */
            explicit WritePrepartitionedData(const Mesh& mesh,
                                             const std::string& output_directory,
                                             ::MoReFEM::MeshNS::Format format);

            //! Destructor.
            ~WritePrepartitionedData() = default;

            //! \copydoc doxygen_hide_copy_constructor
            WritePrepartitionedData(const WritePrepartitionedData& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            WritePrepartitionedData(WritePrepartitionedData&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            WritePrepartitionedData& operator=(const WritePrepartitionedData& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            WritePrepartitionedData& operator=(WritePrepartitionedData&& rhs) = delete;

            ///@}

        public:

            //! Path to the generated mesh for current rank.
            const std::string& GetReducedMeshFile() const noexcept;

            //! Path to the Lua file with the data required to rebuild the partitioned mesh.
            const std::string& GetPartitionDataFile() const noexcept;

        private:

            //! Path to the generated mesh for current rank.
            std::string reduced_mesh_file_ = "";

            //! Path to the Lua file with the data required to rebuild the partitioned mesh.
            std::string partition_data_file_ = "";

        };



    } // namespaceAdvanced::MeshNS


} // namespace MoReFEM


# include "Geometry/Mesh/Advanced/WritePrepartitionedData.hxx"


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HPP_
