/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Aug 2017 17:16:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HPP_
# define MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HPP_

# include "Geometry/Coords/Coords.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace CoordsNS
        {


            /*!
             * \brief A factory class to create new \a Coords objects.
             *
             * This factory, which is a friend of \a Coords, is there merely to make \a Coords constructors private
             * and therefore make it clear such objects should not be created by a model user (\a SpatialPoint are
             * an entirely different story and are probably what a model writer might be seeking).
             */
            struct Factory
            {


                /*!
                 * \brief Create a \a Coords objets with its three components set to 0.
                 *
                 * \return Pointer to newly created \a Coords.
                 */
                static Coords::unique_ptr Origin();


                /*!
                 * \brief Create a \a Coords objets from its three components.
                 *
                 * \copydoc doxygen_hide_coords_from_components_params
                 *
                 * \return Pointer to newly created \a Coords.
                 */
                static Coords::unique_ptr FromComponents(double x, double y, double z, const double space_unit);


                /*!
                 * \brief Create a \a Coords objets from its three components.
                 *
                 * \copydoc doxygen_hide_coords_from_stream_params
                 *
                 * \return Pointer to newly created \a Coords.
                 */
                static Coords::unique_ptr FromStream(unsigned int Ncoor, std::istream& stream, const double space_unit);

                /*!
                 * \brief Create a \a Coords objets from a 3D array.
                 *
                 * \copydoc doxygen_hide_coords_from_array_params
                 *
                 * \return Pointer to newly created \a Coords.
                 */
                template<typename T>
                static Coords::unique_ptr FromArray(T&& value, const double space_unit);


            };



        } // namespace CoordsNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Coords/Internal/Factory.hxx"


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HPP_
