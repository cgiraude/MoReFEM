/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Mar 2017 22:51:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_LOCAL_COORDS_HPP_
# define MOREFEM_x_GEOMETRY_x_COORDS_x_LOCAL_COORDS_HPP_


# include <memory>
# include <numeric>
# include <cassert>
# include <vector>

# ifndef NDEBUG
#  include <algorithm>
# endif // NDEBUG


namespace MoReFEM
{


    /*!
     * \brief LocalCoords used in the local reference element.
     *
     * \note You shouldn't have to handle this directly at all, even if you're writing your own operators. The only
     * reason it is in Advanced rather than in Internal namespace is that you might need it should you define your own
     * \a QuadratureRule.
     *
     * \internal <b><tt>[internal]</tt></b> LocalCoords doesn't inherit SpatialPoint on purpose: I do not want a
     * IS-A relationship induced by a public inheritance. The reason is that I want the strong typing to help keep the
     * code clean and clear: there is hence no risk on giving a SpatialPoint when a LocalCoords is expected, or vice-versa.
     * Another reason is that for performance reasons I do not want virtual methods in Point classes, and
     * I would have to put the destructor virtual as SpatialPoint are an obvious candidate to be used polymorphically.
     * \endinternal
     */
    class LocalCoords
    {
    public:

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<LocalCoords>;

      public:


        /// \name Constructors and destructor.
        ///@{


        /*!
         * \brief Constructor from a vector.
         *
         * T is expected to be std::vector<double>.
         * \param[in] coordinates_as_double Value for each component of the object to build. Contrary to \a Coords,
         * a \a LocalCoords may encompass 1, 2 or 3 components.
         */
        template<class T>
        explicit LocalCoords(T&& coordinates_as_double);

        //! Constructor from an initializer list.
        //! \param[in] coor Initializer list which gives the (x, y, z) values.
        LocalCoords(std::initializer_list<double>&& coor);

        //! \copydoc doxygen_hide_copy_constructor
        LocalCoords(const LocalCoords& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        LocalCoords(LocalCoords&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        LocalCoords& operator=(const LocalCoords& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        LocalCoords& operator=(LocalCoords&& rhs) = default;

        //! Destructor.
        virtual ~LocalCoords();

        ///@}


        //! Access coordinate r of the point.
        double r() const;

        //! Access coordinate s of the point.
        double s() const;

        //! Access coordinate t of the point.
        double t() const;

        /*!
         * \brief Subscript operator, const version.
         *
         * \param[in] index Component index. x, y and z coordinates may be accessed respectively with [0], [1], [2].
         *
         * \return Coordinate at the index-th component.
         */
        double operator[](unsigned int index) const;

        /*!
         * \brief Subscript operator, non-const version.
         *
         * \param[in] index Component index. x, y and z coordinates may be accessed respectively with [0], [1], [2].
         *
         * \return Coordinate at the index-th component.
         */
        double& GetNonCstValue(unsigned int index);

        //! Get the dimension of the LocalCoords.
        unsigned int GetDimension() const;

        //! Print function (used also for operator<< overload).
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;

        //! Return the coordinates of the LocalCoords.
        const std::vector<double>& GetCoordinates() const;

        //! Returns the value of the \a index -th coordinates, or 0 if there are none.
        //! \param[in] index Index of the component requested: 0 for x, 1 for y, 2 for z.
        double GetValueOrZero(unsigned int index) const;


    private:

        //! List of coordinates. Might be 1, 2 or 3 depending on the dimension of the element for which the coords are defined.
        std::vector<double> coordinate_list_;

    };


    /*!
     * \copydoc doxygen_hide_operator_equal
     *
     * Two LocalCoords are consider equal when all their components are.
     *
     * It is assumed here (and asserted in debug) that both arguments are the same dimension.
     */
    bool operator==(const LocalCoords& lhs, const LocalCoords& rhs);


    /*!
     * \brief Return the center of gravity of several LocalCoords.
     *
     * It is assumed here (and asserted in debug) that all LocalCoords are the same dimension.
     *
     * \param[in] coords_list List of \a Coords for which center of gravity is sought.
     *
     * \tparam ContainerT A contained to enclose several LocalCoords objects. Typically a
     * std::vector<LocalCoords> or std::array<LocalCoords, ...>.
     *
     * \return Computed center of gravity.
     */
    template<class ContainerT>
    LocalCoords ComputeCenterOfGravity(const ContainerT& coords_list);

    /*!
     * \brief Find how many components differ between two LocalCoords, and returns their index.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return Indexes of the mismatched components. For instance if (0., 1., 0.) and (0., 4., 0.) are considered in
     * \a coords_list, return {1}.
     */
    std::vector<unsigned int> ExtractMismatchedComponentIndexes(const LocalCoords& lhs,
                                                                const LocalCoords& rhs);


    /*!
     * \brief A more limited version of ExtractMismatchedComponentIndexes in the case exactly one value is expected.
     *
     * This is the one actually required: this function is called when determining which is the component that
     * vary inside an edge of a reference topology element that is quadrangular of hexahedronal.
     *
     * There is an assert here that requires only one value fits the bill.
     *
     * \copydoc doxygen_hide_lhs_rhs_arg
     *
     * \return Position of the mismatched component (unique by design of this function; if not the call was improper).
     */
    unsigned int ExtractMismatchedComponentIndex(const LocalCoords& lhs,
                                                 const LocalCoords& rhs);


    /*!
     * \brief Find how many components are identical for each of the input LocalCoords, and returns their index.
     *
     * \param[in] coords_list List of \a Coords under investigation.
     *
     * \return Indexes of the components that are equal. For instance if (0, 1, 0) and (0, 4, 0) are considered in
     * \a coords_list, return (0, 2).
     */
    std::vector<unsigned int> ExtractIdenticalComponentIndexes(const std::vector<LocalCoords>& coords_list);


    /*!
     * \brief A more limited version of ExtractIdenticalComponentIndexes in the case exactly one value is expected.
     *
     *
     * There is an assert here that requires only one value fits the bill.
     * \param[in] coords_list List of \a Coords under investigation.
     * \return Key is the position of the identical component (assumed to be unique) and the value is its value.
     */
    std::pair<unsigned int, double> ExtractIdenticalComponentIndex(const std::vector<LocalCoords>& coords_list);



    //! \copydoc doxygen_hide_std_stream_out_overload
    std::ostream& operator<<(std::ostream& stream, const LocalCoords& rhs);



} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/Coords/LocalCoords.hxx"


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_LOCAL_COORDS_HPP_
