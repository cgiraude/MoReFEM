/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 31 Mar 2014 16:36:13 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <sstream>
#include "Geometry/Coords/Exceptions/Coords.hpp"


namespace // anonymous
{
    
    
    // Forward declarations here; definitions are at the end of the file
    std::string InconsistentTypeMsg(unsigned int index);
    
    
} // namespace anonymous



namespace MoReFEM
{
    
    
    namespace ExceptionNS
    {
        
        
        namespace CoordsNS
        {
        
            
            Coords::~Coords() = default;
            
            
            Coords::Coords(const std::string& msg, const char* invoking_file, int invoking_line)
            : MoReFEM::Exception(msg, invoking_file, invoking_line)
            { }
            
            
            InconsistentType::~InconsistentType() = default;
            
            
            InconsistentType::InconsistentType(unsigned int index, const char* invoking_file, int invoking_line)
            : Coords(InconsistentTypeMsg(index), invoking_file, invoking_line)
            { }
            
            
        } // namespace CoordsNS
        
        
    } // namespace ExceptionNS
    
    
} // namespace MoReFEM



namespace // anonymous
{
    
    
    // Definitions of functions defined at the beginning of the file
    std::string InconsistentTypeMsg(unsigned int index)
    {
        std::ostringstream oconv;
        oconv << "Same Coords (index " <<index << ") has been assigned two incompatible types (for instance it has "
        "been tagged as both a vertex and an edge!)";
        return oconv.str();
    }
    
    
} // namespace anonymous


/// @} // addtogroup GeometryGroup
