/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Sep 2014 10:04:38 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HXX_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HXX_



namespace MoReFEM
{


    namespace Internal
    {


        namespace RefGeomEltNS
        {


            template<class TraitsRefGeomEltT>
            inline Advanced::GeometricEltEnum TRefGeomElt<TraitsRefGeomEltT>::GetIdentifier() const
            {
                return TraitsRefGeomEltT::Identifier();
            }

            template<class TraitsRefGeomEltT>
            inline unsigned int TRefGeomElt<TraitsRefGeomEltT>::GetDimension() const
            {
                return TraitsRefGeomEltT::topology::dimension;
            }


            template<class TraitsRefGeomEltT>
            inline const std::string& TRefGeomElt<TraitsRefGeomEltT>::GetName() const
            {
                return TraitsRefGeomEltT::ClassName();
            }


            template<class TraitsRefGeomEltT>
            inline const std::string& TRefGeomElt<TraitsRefGeomEltT>::GetTopologyName() const
            {
                return TraitsRefGeomEltT::topology::ClassName();
            }


            template<class TraitsRefGeomEltT>
            inline ::MoReFEM::RefGeomEltNS::TopologyNS::Type
            TRefGeomElt<TraitsRefGeomEltT>::GetTopologyIdentifier() const
            {
                return TraitsRefGeomEltT::topology::GetType();
            }


            template<class TraitsRefGeomEltT>
            inline unsigned int TRefGeomElt<TraitsRefGeomEltT>::Ncoords() const
            {
                return TraitsRefGeomEltT::Ncoords;
            }


            template<class TraitsRefGeomEltT>
            const LocalCoords& TRefGeomElt<TraitsRefGeomEltT>::GetBarycenter() const
            {
                return TraitsRefGeomEltT::GetBarycenter();
            }


            template<class TraitsRefGeomEltT>
            const std::vector<LocalCoords>& TRefGeomElt<TraitsRefGeomEltT>::GetVertexLocalCoordsList() const
            {
                return TraitsRefGeomEltT::topology::GetVertexLocalCoordsList();
            }


            template<class TraitsRefGeomEltT>
            inline double TRefGeomElt<TraitsRefGeomEltT>::ShapeFunction(unsigned int i,
                                                                        const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::ShapeFunction(i, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double TRefGeomElt<TraitsRefGeomEltT>::FirstDerivateShapeFunction(unsigned int i, unsigned int icoor,
                                                                                     const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::FirstDerivateShapeFunction(i, icoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            inline double TRefGeomElt<TraitsRefGeomEltT>
            ::SecondDerivateShapeFunction(unsigned int i, unsigned int icoor, unsigned int jcoor,
                                          const LocalCoords& local_coords) const
            {
                return TraitsRefGeomEltT::SecondDerivateShapeFunction(i, icoor, jcoor, local_coords);
            }


            template<class TraitsRefGeomEltT>
            unsigned int TRefGeomElt<TraitsRefGeomEltT>::Nvertex() const noexcept
            {
                return TraitsRefGeomEltT::topology::Nvertex;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TRefGeomElt<TraitsRefGeomEltT>::Nedge() const noexcept
            {
                return TraitsRefGeomEltT::topology::Nedge;
            }


            template<class TraitsRefGeomEltT>
            unsigned int TRefGeomElt<TraitsRefGeomEltT>::Nface() const noexcept
            {
                return TraitsRefGeomEltT::topology::Nface;
            }


            template<class TraitsRefGeomEltT>
            ::MoReFEM::InterfaceNS::Nature TRefGeomElt<TraitsRefGeomEltT>
            ::GetInteriorInterfaceNature() const noexcept
            {
                return TraitsRefGeomEltT::topology::GetInteriorInterface();
            }


            namespace Impl
            {


                template<class ListT>
                std::deque<bool> IsNodeOnReferenceInterface(const ListT& reference_interface_list)
                {
                    std::deque<bool> ret;

                    for (const auto& reference_interface : reference_interface_list)
                        ret.push_back(reference_interface.IsNode());

                    return ret;
                }

            }


        } // namespace RefGeomEltNS


    } // namespace Internal


} // namespace MoReFEM



/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HXX_
