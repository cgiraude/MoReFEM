/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 10:56:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HPP_


# include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace RefGeomEltNS
        {


            /*!
             * \brief Derived from 'RefGeomElt', for which it defines all the virtual methods.
             *
             * 'TRefGeomElt' stands for templatized FeomRefElt.
             *
             * \tparam TraitsRefGeomEltT A trait class that defines RefGeomElt behaviour. This is a class that must be
             * defined within RefGeomEltNS::Traits namespace, e.g. RefGeomEltNS::Traits::Triangle3.
             */
            template<class TraitsRefGeomEltT>
            class TRefGeomElt : public RefGeomElt
            {

            public:

                //! Alias to the template parameter of the class.
                using traits = TraitsRefGeomEltT;

            protected:

                ///@{

                //! Default constructor.
                TRefGeomElt() = default;

                //! Destructor.
                virtual ~TRefGeomElt() override = default;

                //! \copydoc doxygen_hide_copy_constructor
                TRefGeomElt(const TRefGeomElt& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                TRefGeomElt(TRefGeomElt&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                TRefGeomElt& operator=(const TRefGeomElt& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                TRefGeomElt& operator=(TRefGeomElt&& rhs) = delete;


            public:

                /*!
                 * \brief Get the identifier of the geometric element.
                 *
                 * \return The identifier of a GeometricElt as defined within MoReFEM (independant of IO format).
                 */
                virtual Advanced::GeometricEltEnum GetIdentifier() const override final;

                //! Get the dimension of the geometric element.
                virtual unsigned int GetDimension() const override final;

                /*!
                 * \brief Get the number of Coords object required to characterize completely a GeometricElt of this type.
                 *
                 * For instance 27 for an Hexahedron27.
                 *
                 * \return Number of Coords object required to characterize completely a GeometricElt of this type.
                 */
                virtual unsigned int Ncoords() const override final;

                //! Returns the name of the geometric element (for instance 'Triangle3').
                virtual const std::string& GetName() const override final;

                //! Get the name associated to the Topology (e.g. 'Triangle').
                virtual const std::string& GetTopologyName() const override final;

                //! Get the enum value associated to the Topology (e.g. 'RefGeomEltNS::TopologyNS::Type::tetrahedron').
                virtual ::MoReFEM::RefGeomEltNS::TopologyNS::Type GetTopologyIdentifier() const override final;

                //! Get the local coordinates of the barycenter.
                virtual const LocalCoords& GetBarycenter() const override final;

                //! Get the list of local coordinates of the vertices.
                virtual const std::vector<LocalCoords>& GetVertexLocalCoordsList() const override final;

                //! \copydoc doxygen_hide_shape_function
                virtual double ShapeFunction(unsigned int local_node_index, const LocalCoords& local_coords) const override final;

                //! \copydoc doxygen_hide_first_derivate_shape_function
                virtual double FirstDerivateShapeFunction(unsigned int local_node_index,
                                                          unsigned int component,
                                                          const LocalCoords& local_coords) const override final;

                //! \copydoc doxygen_hide_second_derivate_shape_function
                virtual double SecondDerivateShapeFunction(unsigned int local_node_index,
                                                           unsigned int component1,
                                                           unsigned int component2,
                                                           const LocalCoords& local_coords) const override final;

                //! Return the number of vertices.
                virtual unsigned int Nvertex() const noexcept override final;

                //! Return the number of edges.
                virtual unsigned int Nedge() const noexcept override final;

                //! Return the number of faces.
                virtual unsigned int Nface() const noexcept override final;

                //! Returns the nature of the interior interface.
                virtual ::MoReFEM::InterfaceNS::Nature GetInteriorInterfaceNature() const noexcept override final;



            };


        } // namespace RefGeomEltNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hxx"


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INTERNAL_x_REF_GEOM_ELT_x_T_REF_GEOM_ELT_HPP_
