/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 27 Apr 2016 14:46:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <iostream>

#include "Geometry/RefGeometricElt/Internal/Topology/EnumTopology.hpp"


namespace MoReFEM::RefGeomEltNS::TopologyNS
{
    
    
    std::ostream& operator<<(std::ostream& stream, const Type topology)
    {
        using type = MoReFEM::RefGeomEltNS::TopologyNS::Type;
        
        switch (topology)
        {
            case type::point:
                stream << "point";
                break;
            case type::segment:
                stream << "segment";
                break;
            case type::triangle:
                stream << "triangle";
                break;
            case type::tetrahedron:
                stream << "tetrahedron";
                break;
            case type::quadrangle:
                stream << "quadrangle";
                break;
            case type::hexahedron:
                stream << "hexahedron";
                break;
        
        } // switch
        
        return stream;
    }
    
    
} // namespace MoReFEM::RefGeomEltNS::TopologyNS


/// @} // addtogroup GeometryGroup
