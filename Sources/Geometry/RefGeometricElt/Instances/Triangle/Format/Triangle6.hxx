/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Apr 2016 22:46:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_FORMAT_x_TRIANGLE6_HXX_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_FORMAT_x_TRIANGLE6_HXX_

# include "Geometry/Mesh/Internal/Format/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                inline constexpr GmfKwdCod Support
                <
                    ::MoReFEM::MeshNS::Format::Medit,
                    Advanced::GeometricEltEnum::Triangle6
                >
                ::MeditId()
                {
                    return GmfTrianglesP2;
                }


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_FORMAT_x_TRIANGLE6_HXX_
