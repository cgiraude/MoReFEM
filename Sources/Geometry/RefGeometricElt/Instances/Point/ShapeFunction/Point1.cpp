/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Point/ShapeFunction/Point1.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"

#include "Geometry/Coords/LocalCoords.hpp"


namespace MoReFEM
{
    
    
    namespace RefGeomEltNS
    {
        
        
        namespace ShapeFunctionNS
        {
            
            
            namespace // anonymous
            {
                
                
                const std::array<ShapeFunctionType, 0>& EmptyArray()
                {
                    static std::array<ShapeFunctionType, 0> ret;
                    return ret;
                };

                
                
            } // namespace anonymous

            
            
            const std::array<ShapeFunctionType, 1>& Point1::ShapeFunctionList()
            {
                static std::array<ShapeFunctionType, 1> ret
                {
                    {
                        Constant<1>()
                    }
                };
                
                return ret;
            };
            
            
            const std::array<ShapeFunctionType, 0>& Point1::FirstDerivateShapeFunctionList()
            {
                return EmptyArray();
            };
            
            
            
            const std::array<ShapeFunctionType, 0>& Point1::SecondDerivateShapeFunctionList()
            {
                return EmptyArray();
            };

            
            
            
        } //  namespace ShapeFunctionNS
        
        
    } // namespace RefGeomEltNS
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
