/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/RefGeometricElt/Instances/Hexahedron/Hexahedron27.hpp"


namespace MoReFEM
{
    
    
    namespace RefGeomEltNS
    {
        
        
        Hexahedron27::~Hexahedron27() = default;
        
        
        
        namespace Traits
        {
            
            
            const std::string& Hexahedron27::ClassName()
            {
                static std::string ret("Hexahedron27");
                return ret;
            }
            
            
        } // namespace Traits

        
        
    } // namespace RefGeomEltNS
    
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
