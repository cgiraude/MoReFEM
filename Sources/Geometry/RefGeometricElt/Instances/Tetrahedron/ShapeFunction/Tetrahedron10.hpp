/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_SHAPE_FUNCTION_x_TETRAHEDRON10_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_SHAPE_FUNCTION_x_TETRAHEDRON10_HPP_


# include <array>

# include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp"
# include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace ShapeFunctionNS
        {



            /*!
             * \brief Define Triangle3 shape functions and its derivative.
             */
            struct Tetrahedron10 : public Crtp::AccessShapeFunction<Tetrahedron10>
            {


                //! \copydoc doxygen_hide_shape_function_instance_enum
                enum
                {
                    Nderivate_component_ = 3,
                    Nphi_ = 10,
                    Order = 2
                };

                //! Shape functions.
                static const std::array<ShapeFunctionType, Nphi_>& ShapeFunctionList();

                /*!
                 * \brief First derivative of the shape functions.
                 *
                 * Ordering:
                 *   \li d(phi[0], r), d(phi[0], s)
                 *   \li d(phi[1], r), d(phi[1], s)
                 *   etc...
                 *
                 * \return The derivatives as an array of functions (ordering defined just above)
                 */
                static const std::array<ShapeFunctionType, Nphi_ * Nderivate_component_>& FirstDerivateShapeFunctionList();

                /*!
                 * \brief Second derivative of the shape functions.
                 *
                 * Ordering:
                 *   \li d2(phi[0], r, r), d2(phi[0], r, s)
                 *   \li d2(phi[0], s, r), d2(phi[0], s, s)
                 *   \li d2(phi[1], r, r), d2(phi[1], r, s)
                 *   etc...
                 *
                 * \return The derivatives as an array of functions (ordering defined just above)
                 */

                static const std::array<ShapeFunctionType, Nphi_ * Nderivate_component_ * Nderivate_component_>& SecondDerivateShapeFunctionList();

            };


        } // namespace ShapeFunctionNS


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TETRAHEDRON_x_SHAPE_FUNCTION_x_TETRAHEDRON10_HPP_
