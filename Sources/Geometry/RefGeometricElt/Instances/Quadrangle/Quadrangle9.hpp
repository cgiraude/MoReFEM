/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE9_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE9_HPP_

# include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"
# include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle9.hpp"
# include "Geometry/RefGeometricElt/Instances/Quadrangle/Format/Quadrangle9.hpp"
    // < absolutely required to let MoReFEM know the format actually supported!

# include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"
# include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"


namespace MoReFEM
{


    namespace RefGeomEltNS
    {


        namespace Traits
        {


            /*!
             * \brief Traits class that holds the static functions related to shape functions, interface and topology.
             *
             * It can't be instantiated directly: its purpose is either to provide directly a data through
             * static function:
             *
             * \code
             * constexpr auto Nshape_function = Quadrangle9::NshapeFunction();
             * \endcode
             *
             * or to be a template parameter to MoReFEM::RefGeomEltNS::Quadrangle9 class (current class is in an
             * additional layer of namespace):
             *
             * \code
             * MoReFEM::RefGeomEltNS::Traits::Quadrangle9
             * \endcode
             *
             */
            class Quadrangle9 final
            : public ::MoReFEM::Internal::RefGeomEltNS::RefGeomEltImpl
            <
                Quadrangle9,
                ShapeFunctionNS::Quadrangle9,
                TopologyNS::Quadrangle
            >
            {


            protected:

                //! Convenient alias.
                using self = Quadrangle9;

                /// \name Special members: prevent direct instantiation of RefGeomEltImpl objects.
                ///@{

                //! Constructor.
                Quadrangle9() = delete;

                //! Destructor.
                ~Quadrangle9() = delete;

                //! \copydoc doxygen_hide_copy_constructor
                Quadrangle9(const Quadrangle9& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                Quadrangle9(Quadrangle9&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                Quadrangle9& operator=(const Quadrangle9& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                self& operator=(self&& rhs) = delete;

                ///@}


            public:

                /*!
                 * \brief Name associated to the RefGeomElt.
                 *
                 * \return Name that is guaranteed to be unique (throught the GeometricEltFactory) and can
                 * therefore also act as an identifier.
                 */
                static const std::string& ClassName();

                //! Number of Coords required to describe fully a GeometricElt of this type.
                enum : unsigned int { Ncoords = 9u };

                /*!
                 * \brief Enum associated to the RefGeomElt.
                 *
                 * \return Enum value guaranteed to be unique (throught the GeometricEltFactory); its
                 * raison d'être is that many operations are much faster on an enumeration than on a string.
                 */
                static constexpr MoReFEM::Advanced::GeometricEltEnum Identifier()
                {
                    return MoReFEM::Advanced::GeometricEltEnum::Quadrangle9;
                }


            private:

                // THIS IS A TRAIT CLASS, NO MEMBERS ALLOWED HERE!


            };



        } // namespace Traits


        /*!
         * \brief Acts as a strawman class for MoReFEM::RefGeomEltNS::Traits::Quadrangle9.
         *
         * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
         * store in one dynamic container all the kinds of GeometricElt present in a mesh.
         *
         * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
         * can be included in:
         *
         * \code
         * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
         * \endcode
         *
         */
        class Quadrangle9 final
        : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Quadrangle9>
        {
        public:

            //! Constructor.
            Quadrangle9() = default;

            //! Destructor.
            ~Quadrangle9();

            //! \copydoc doxygen_hide_copy_constructor
            Quadrangle9(const Quadrangle9& rhs) = delete;

            //! \copydoc doxygen_hide_move_constructor
            Quadrangle9(Quadrangle9&& rhs) = delete;

            //! \copydoc doxygen_hide_copy_affectation
            Quadrangle9& operator=(const Quadrangle9& rhs) = delete;

            //! \copydoc doxygen_hide_move_affectation
            Quadrangle9& operator=(Quadrangle9&& rhs) = delete;


        private:

            // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...


        };


    } // namespace RefGeomEltNS


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_QUADRANGLE9_HPP_
