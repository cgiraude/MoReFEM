/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 21 Apr 2016 22:46:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_FORMAT_x_QUADRANGLE9_HPP_
# define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_FORMAT_x_QUADRANGLE9_HPP_

# include "Geometry/Mesh/Internal/Format/Format.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace MeshNS
        {


            namespace FormatNS
            {


                //! \copydoc doxygen_hide_geometry_format_medit_support
                template<>
                struct Support
                <
                    ::MoReFEM::MeshNS::Format::Medit,
                    Advanced::GeometricEltEnum::Quadrangle9
                >
                : public std::true_type
                {


                    //! Medit code for this object.
                    static constexpr GmfKwdCod MeditId();


                };


            } // namespace FormatNS


        } // namespace MeshNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


# include "Geometry/RefGeometricElt/Instances/Quadrangle/Format/Quadrangle9.hxx"


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_FORMAT_x_QUADRANGLE9_HPP_
