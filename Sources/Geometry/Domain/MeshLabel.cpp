/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM
{
    
    
    namespace // anonymous
    {
        
        
        std::string DescriptionHelper(std::string&& description, unsigned int index)
        {
            if (description.empty())
            {
                std::ostringstream oconv;
                oconv << "MeshLabel " << index;
                return oconv.str();
            }
            
            return std::move(description);
        }
        
        
    
    } // namespace anonymous
    
    
    MeshLabel::MeshLabel(const unsigned int mesh_id,
                         const unsigned int index,
                         std::string&& description)
    : mesh_identifier_(mesh_id),
    index_(index),
    description_(DescriptionHelper(std::move(description), index))
    { }


    MeshLabel::~MeshLabel() = default;
    
    
    const std::string& MeshLabel::ClassName()
    {
        static std::string ret("MeshLabel");
        return ret;
    }
        
    
} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
