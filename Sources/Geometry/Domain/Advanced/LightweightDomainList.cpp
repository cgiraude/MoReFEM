/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 16 Apr 2017 22:29:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Domain/Advanced/LightweightDomainList.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
    
    
        const std::string& LightweightDomainList::ClassName()
        {
            static std::string ret("LightweightDomainList");
            return ret;
        }
        
        
        LightweightDomainList::LightweightDomainList(unsigned int unique_id,
                                                     unsigned int mesh_index,
                                                     const std::vector<unsigned int>& domain_index_list,
                                                     const std::vector<unsigned int>& mesh_label_list,
                                                     const std::vector<unsigned int>& number_in_domain_list)
        : unique_id_parent(unique_id),
        mesh_(Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__).GetMesh(mesh_index))
        {
            // Check consistency of both vectors.
            const auto Nlabel = std::accumulate(number_in_domain_list.cbegin(),
                                                number_in_domain_list.cend(),
                                                0ul);
            
            if (mesh_label_list.size() != Nlabel)
            {
                std::ostringstream oconv;
                oconv << "Inconsistency in the creation of LightweightDomainList" << unique_id << ": " << Nlabel
                << " were expected according to number_in_domain_list field but " << mesh_label_list.size()
                << " were actually provided in mesh_label_list.";

                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
            
            if (domain_index_list.size() != number_in_domain_list.size())
            {
                std::ostringstream oconv;
                oconv << "Inconsistency in the creation of LightweightDomainList" << unique_id << ": domain_index_list "
                "and number_in_domain_list should be the same size.";
                
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
            
            
            auto it_label = mesh_label_list.cbegin();
            
            std::size_t current_index(0ul);
            
            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
            
            for (const auto Nlabel_in_domain : number_in_domain_list)
            {
                std::vector<unsigned int> domain_label_list;
                
                for (unsigned int i = 0u; i < Nlabel_in_domain; ++i)
                {
                    assert(it_label != mesh_label_list.cend() && "Should have been covered by the above exception.");
                    domain_label_list.push_back(*it_label);
                    ++it_label;
                }
                
                assert(current_index < domain_index_list.size());
                
                const auto domain_unique_id = domain_index_list[current_index++];
                
                domain_manager.CreateLightweightDomain(domain_unique_id,
                                                       mesh_index,
                                                       domain_label_list);
                
                domain_storage_.push_back(&domain_manager.GetDomain(domain_unique_id, __FILE__, __LINE__));
            }
            
            assert(it_label == mesh_label_list.cend());
        }
        
        
    } // namespace Advanced
    

} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
