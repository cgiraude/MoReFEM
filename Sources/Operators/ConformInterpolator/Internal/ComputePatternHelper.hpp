/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 21 Oct 2016 16:22:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_COMPUTE_PATTERN_HELPER_HPP_
# define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_COMPUTE_PATTERN_HELPER_HPP_

# include <vector>

# include "Utilities/MatrixOrVector.hpp"



namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;


    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            class InterpolationData;


        } // namespace ConformInterpolatorNS


    } // namespace Advanced




    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace ConformInterpolatorNS
        {


                /*!
                 * \brief Free function called by LagrangianInterpolator::ComputePattern().
                 *
                 * \param[in] ref_felt_space \a RefLocalFEltSpace for which pattern is computed.
                 * \param[in] source_local_felt_space_list Key is the index of the geometric element, value the actual
                 * pointer to the LocalFEltSpace.
                 * \param[in] local_projection_matrix Local projection matrix.
                 * \param[in] interpolation_data Interpolation data.
                 * \param[in] map_pattern Map pattern.
                 * \param[in] map_values Map values.
                 */
                void ComputePatternFromRefGeomElt(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                                  const LocalFEltSpace::per_geom_elt_index& source_local_felt_space_list,
                                                  const LocalMatrix& local_projection_matrix,
                                                  const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data,
                                                  std::map<std::size_t, std::vector<PetscInt>>& map_pattern,
                                                  std::unordered_map<std::size_t, std::vector<PetscScalar>>& map_values);


        } // namespace ConformInterpolatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/ConformInterpolator/Internal/ComputePatternHelper.hxx"


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_INTERNAL_x_COMPUTE_PATTERN_HELPER_HPP_
