/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Sep 2015 17:07:12 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_CRTP_x_LAGRANGIAN_INTERPOLATOR_HXX_
# define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_CRTP_x_LAGRANGIAN_INTERPOLATOR_HXX_


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace Crtp
        {


            template<class DerivedT>
            LagrangianInterpolator<DerivedT>
            ::LagrangianInterpolator(const FEltSpace& source_felt_space,
                                     const NumberingSubset& source_numbering_subset,
                                     const FEltSpace& target_felt_space,
                                     const NumberingSubset& target_numbering_subset,
                                     pairing_type&& pairing)
            : pairing_(std::move(pairing))
            {
                assert("Current interpolator assumes geometric conformity!"
                       && source_felt_space.GetGodOfDofFromWeakPtr() == target_felt_space.GetGodOfDofFromWeakPtr());

                using type = Advanced::ConformInterpolatorNS::SourceOrTargetData;

                auto&& source_data =
                    std::make_unique<type>(source_felt_space,
                                           source_numbering_subset,
                                           pairing_,
                                           Advanced::ConformInterpolatorNS::SourceOrTargetDataType::source);

                auto&& target_data =
                    std::make_unique<type>(target_felt_space,
                                           target_numbering_subset,
                                           pairing_,
                                           Advanced::ConformInterpolatorNS::SourceOrTargetDataType::target);

                interpolation_data_ =
                    std::make_unique<Advanced::ConformInterpolatorNS::InterpolationData>(std::move(source_data),
                                                                                         std::move(target_data));


            }


            template<class DerivedT>
            template<typename... Args>
            void LagrangianInterpolator<DerivedT>::Init(Args&&... args)
            {
                AllocateInterpolationMatrix();
                static_cast<DerivedT&>(*this).ComputeInterpolationMatrix(std::forward<decltype(args)>(args)...);
            }


            template<class DerivedT>
            void LagrangianInterpolator<DerivedT>::AllocateInterpolationMatrix()
            {
                assert(interpolation_matrix_ == nullptr && "Should be called only once!");

                const auto& interpolation_data = GetInterpolationData();

                interpolation_matrix_ = std::make_unique<GlobalMatrix>(interpolation_data.GetTargetData().GetNumberingSubset(),
                                                                       interpolation_data.GetSourceData().GetNumberingSubset());
            }


            template<class DerivedT>
            GodOfDof::const_shared_ptr LagrangianInterpolator<DerivedT>::GetGodOfDofFromWeakPtr() const noexcept
            {
                const auto& interpolation_data = GetInterpolationData();
                const auto& felt_space = interpolation_data.GetSourceData().GetFEltSpace();

                auto&& god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
                assert("Current interpolator assumes geometric conformity!"
                       && god_of_dof_ptr->GetUniqueId() == interpolation_data.GetTargetData().GetFEltSpace().GetGodOfDofFromWeakPtr()->GetUniqueId());

                return god_of_dof_ptr;
            }


            template<class DerivedT>
            inline const GlobalMatrix& LagrangianInterpolator<DerivedT>::GetInterpolationMatrix() const noexcept
            {
                assert(!(!interpolation_matrix_)
                       && "Make sure Init() was properly called for the interpolator (constructor is not enough).");
                return *interpolation_matrix_;
            }


            template<class DerivedT>
            inline GlobalMatrix& LagrangianInterpolator<DerivedT>::GetNonCstInterpolationMatrix() const noexcept
            {
                return const_cast<GlobalMatrix&>(GetInterpolationMatrix());
            }


            template<class DerivedT>
            inline const Advanced::ConformInterpolatorNS::InterpolationData& LagrangianInterpolator<DerivedT>::GetInterpolationData() const noexcept
            {
                assert(!(!interpolation_data_));
                return *interpolation_data_;
            }


        } // namespace Crtp


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_CRTP_x_LAGRANGIAN_INTERPOLATOR_HXX_
