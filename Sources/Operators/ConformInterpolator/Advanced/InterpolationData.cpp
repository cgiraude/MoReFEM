/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 23 Sep 2015 18:06:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"


namespace MoReFEM
{
    
    
    namespace Advanced
    {
        
        
        namespace ConformInterpolatorNS
        {
            

            InterpolationData::InterpolationData(SourceOrTargetData::unique_ptr&& source_data,
                                                 SourceOrTargetData::unique_ptr&& target_data)
            : source_data_(std::move(source_data)),
            target_data_(std::move(target_data))
            { }
            
          
            
        } // namespace ConformInterpolatorNS
        
        
    } // namespace Advanced
  

} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
