/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Sep 2015 14:29:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_SOURCE_OR_TARGET_DATA_HPP_
# define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_SOURCE_OR_TARGET_DATA_HPP_

# include <memory>
# include <vector>

# include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class FEltSpace;
    class NumberingSubset;


    namespace Internal
    {


        namespace RefFEltNS
        {


            class BasicRefFElt;
            class RefLocalFEltSpace;


        } // namespace RefFEltNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Advanced
    {


        namespace ConformInterpolatorNS
        {


            //! Convenient alias to pairing.
            using pairing_type = std::vector<std::pair<Unknown::const_shared_ptr, Unknown::const_shared_ptr>>;

            //! Enum class whcih specifies whether a source or a target is considered.
            enum class SourceOrTargetDataType { source, target };


            /*!
             * \class doxygen_hide_source_or_target_data_note
             *
             * \note Remember these objects go by pair: one is instantiated for the source, the other for the target.
             */


            /*!
             * \brief Helper object which includes most relevant data about the source or the target space.
             *
             * \copydoc doxygen_hide_source_or_target_data_note
             */
            class SourceOrTargetData
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = SourceOrTargetData;

                //! Alias to unique pointer.
                using unique_ptr = std::unique_ptr<self>;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \class doxygen_hide_conform_interpolation_pairing_arg
                 *
                 * \param[in] pairing The list of pair source unknown/target unknown to consider. Depending on \a type,
                 * only either the first or the second element of each pair is actually considered in this class.
                 */

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_source_or_target_data_note
                 *
                 * \param[in] felt_space \a FEltSpace considered.
                 * \param[in] numbering_subset \a NumberingSubset considered.
                 * \copydoc doxygen_hide_conform_interpolation_pairing_arg
                 * \param[in] type Whether the instantiated object deals with source or target data.
                 *
                 */
                explicit SourceOrTargetData(const FEltSpace& felt_space,
                                            const NumberingSubset& numbering_subset,
                                            const pairing_type& pairing,
                                            SourceOrTargetDataType type);


                //! Destructor.
                ~SourceOrTargetData() = default;

                //! \copydoc doxygen_hide_copy_constructor
                SourceOrTargetData(const SourceOrTargetData& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                SourceOrTargetData(SourceOrTargetData&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                SourceOrTargetData& operator=(const SourceOrTargetData& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                SourceOrTargetData& operator=(SourceOrTargetData&& rhs) = delete;

                ///@}

                //! Number of unknown components considered.
                std::size_t NunknownComponent() const noexcept;

                //! Access to the finite element space.
                const FEltSpace& GetFEltSpace() const noexcept;

                //! Access to the numbering subset.
                const NumberingSubset& GetNumberingSubset() const noexcept;

                //! Access to the extended unknown list relevant for the interpolator.
                const ExtendedUnknown::vector_const_shared_ptr& GetExtendedUnknownList() const noexcept;


                /*!
                 * \brief Extract a BasicRefFElt that would be yielded by any of the extended unknown.
                 *
                 * This method is intended to be used in specific interpolators, such as P1_to_P2 into which all
                 * extended unknowns of a given side source/target are expected to share the same numbering subset
                 * and shape function label.
                 *
                 * \param[in] ref_local_felt_space \a RefLocalFEltSpace for which the \a BasicRefFElt is sought.
                 *
                 * \return \a BasicRefFElt matching the \a ref_local_felt_space.
                 */
                const Internal::RefFEltNS::BasicRefFElt&
                GetCommonBasicRefFElt(const Internal::RefFEltNS::RefLocalFEltSpace& ref_local_felt_space) const;



            private:

                //! Finite element space.
                const FEltSpace& felt_space_;

                //! Numbering subset.
                const NumberingSubset& numbering_subset_;

                //! Number of unknown components considered in the felt space/numbering subset.
                std::size_t Nunknown_component_ = 0ul;

                /*!
                 * \brief List of unknowns inside the felt space/numbering subset considered by both side of the
                 * interpolator.
                 *
                 * If for instance there is pressure in source finite element space but no counterpart in target
                 * finite element space, this unknown will appear in neither of Source and Target objects.
                 */
                ExtendedUnknown::vector_const_shared_ptr extended_unknown_list_;



            };


        } // namespace ConformInterpolatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/ConformInterpolator/Advanced/SourceOrTargetData.hxx"


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ADVANCED_x_SOURCE_OR_TARGET_DATA_HPP_
