/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Sep 2015 15:50:44 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ConformInterpolator/Lagrangian/LocalLagrangianInterpolator.hpp"
#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"


namespace MoReFEM
{
    
    
    namespace ConformInterpolatorNS
    {
    
        
        namespace LagrangianNS
        {
        
        
            LocalLagrangianInterpolator::LocalLagrangianInterpolator(const RefGeomElt& ref_geom_elt,
                                                                     const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data)
            : ref_geom_elt_(ref_geom_elt),
            interpolation_data_(interpolation_data)
            { }
            
            LocalLagrangianInterpolator::~LocalLagrangianInterpolator() = default;
            
            
            void LocalLagrangianInterpolator::FillMatrixFromNodeBlock(LocalMatrix&& node_block)
            {
                const auto& interpolation_data = GetInterpolationData();
                
                decltype(auto) source_data = interpolation_data.GetSourceData();
                
                const auto& source_unknown_list = source_data.GetExtendedUnknownList();
                
                #ifndef NDEBUG
                decltype(auto) target_data = interpolation_data.GetTargetData();
                const auto& target_unknown_list = target_data.GetExtendedUnknownList();
                
                assert(std::none_of(source_unknown_list.cbegin(),
                                    source_unknown_list.cend(),
                                    Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
                
                assert(std::none_of(target_unknown_list.cbegin(),
                                    target_unknown_list.cend(),
                                    Utilities::IsNullptr<ExtendedUnknown::const_shared_ptr>));
                #endif // NDEBUG
                
                
                const auto Nunknown = source_unknown_list.size();
                assert(Nunknown == target_unknown_list.size());
                
                auto shift_row = 0ul;
                auto shift_col = 0ul;
                
                const auto dimension = source_data.GetFEltSpace().GetMeshDimension();
                
                const auto Nnode_in_row = node_block.shape(0);
                const auto Nnode_in_col = node_block.shape(1);
                
                auto& local_projection_matrix = GetNonCstProjectionMatrix();
                
                assert(local_projection_matrix.shape(0) % Nnode_in_row == 0);
                assert(local_projection_matrix.shape(1) % Nnode_in_col == 0);
                
                for (auto unknown_index = 0ul; unknown_index < Nunknown; ++unknown_index)
                {
                    const auto& source_unknown_ptr = source_unknown_list[unknown_index];
                    
                    const auto Ncomponent = static_cast<int>(MoReFEM::Ncomponent(source_unknown_ptr->GetUnknown(), dimension));
                    
                    #ifndef NDEBUG
                    {
                        const auto& target_unknown_ptr = target_unknown_list[unknown_index];
                        assert(Ncomponent == static_cast<int>(MoReFEM::Ncomponent(target_unknown_ptr->GetUnknown(), dimension)));
                    }
                    #endif // NDEBUG
                    
                    for (auto component = 0; component < Ncomponent; ++component)
                    {
                        for (auto row = 0ul; row < Nnode_in_row; ++row)
                        {
                            for (auto col = 0ul; col < Nnode_in_col; ++col)
                                local_projection_matrix(row + shift_row, col + shift_col) = node_block(row, col);
                        }
                        
                        shift_row += Nnode_in_row;
                        shift_col += Nnode_in_col;
                    }
                }
            }
            
            
            
            const Internal::RefFEltNS::RefLocalFEltSpace& LocalLagrangianInterpolator
            ::GetRefLocalFEltSpace(const FEltSpace& felt_space,
                              const RefGeomElt& ref_geom_elt) const
            {
                return felt_space.GetRefLocalFEltSpace(ref_geom_elt);
            }
            
            
            
            const Advanced::ConformInterpolatorNS::InterpolationData& LocalLagrangianInterpolator::GetInterpolationData() const noexcept
            {
                return interpolation_data_;
            }
            
            
            
      
        } // namespace LagrangianNS
    
        
    } // namespace ConformInterpolatorNS
    
    
} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
