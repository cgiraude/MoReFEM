/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 7 Sep 2015 16:27:28 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LAGRANGIAN_INTERPOLATOR_HPP_
# define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LAGRANGIAN_INTERPOLATOR_HPP_

# include <memory>
# include <vector>

# include "Operators/ConformInterpolator/Crtp/LagrangianInterpolator.hpp"
# include "Operators/ConformInterpolator/Internal/ComputePatternHelper.hpp"


namespace MoReFEM
{


    namespace ConformInterpolatorNS
    {


        namespace LagrangianNS
        {


            /*!
             * \brief Parent class of all LagrangianInterpolator (through a CRTP).
             *
             * \tparam ElementaryInterpolatorT Class that describes the operation that occurs at local level.
             */
            template
            <
                class DerivedT,
                class ElementaryInterpolatorT
            >
            class LagrangianInterpolator
            : public Crtp::LagrangianInterpolator<LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>>
            {


            public:

                //! Alias over the class that handles the elementary calculation.
                using LocalVariationalOperator = ElementaryInterpolatorT;

                //! Alias to parent.
                using parent = Crtp::LagrangianInterpolator<LagrangianInterpolator<DerivedT, ElementaryInterpolatorT>>;

                //! Friendship to parent.
                friend parent;


                /// \name Special members.
                ///@{


                /*!
                 * \brief Constructor.
                 *
                 * \attention Constructor call must be followed by Init() to fully initialize the interpolator.
                 *
                 * \param[in] source_felt_space Finite element space of the source.
                 * \param[in] source_numbering_subset Numbering subset of the source. First arguments of \a pairing
                 * pairs must belong to it, and \a source_felt_space must encompass it.
                 * \param[in] target_felt_space Finite element space of the target.
                 * \param[in] target_numbering_subset Numbering subset of the target. Second arguments of \a pairing
                 * pairs must belong to it, and \a target_felt_space must encompass it.
                 *
                 * \param[in] pairing A vector of pair in which each pair is an association between an unknown of the
                 * source and one from the target. For instance if we consider a fluid with (vf, pf) and a solid with
                 * (vs, ds) and want to interpolate from the former to the latter, (vf, vs) must be specified to indicate
                 * ds and pf are ignored and vf is associated with vs.
                 * \param[in] args Variadic arguments, should your interpolator need additional arguments in its
                 * constructor.
                 *
                 */
                template<typename... Args>
                explicit LagrangianInterpolator(const FEltSpace& source_felt_space,
                                                const NumberingSubset& source_numbering_subset,
                                                const FEltSpace& target_felt_space,
                                                const NumberingSubset& target_numbering_subset,
                                                pairing_type&& pairing,
                                                Args&&... args);

                //! Protected destructor: no direct instance of this class should occur!
                ~LagrangianInterpolator() = default;

                //! \copydoc doxygen_hide_copy_constructor
                LagrangianInterpolator(const LagrangianInterpolator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                LagrangianInterpolator(LagrangianInterpolator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                LagrangianInterpolator& operator=(const LagrangianInterpolator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                LagrangianInterpolator& operator=(LagrangianInterpolator&& rhs) = delete;


                ///@}

            private:

                /*!
                 * \brief Init interpolation matrix.
                 *
                 * \copydoc doxygen_hide_cplusplus_variadic_args
                 * \internal <b><tt>[internal]</tt></b> This method is expected by the parent/Crtp class.
                 * \endinternal
                 */
                template<typename... Args>
                void ComputeInterpolationMatrix(Args&&... args);


            private:


                /*!
                 * \brief Whether there is a local variational operator related to a given \a ref_geom_elt.
                 *
                 * \param[in] ref_geom_elt Reference geometric element which pertinence is evaluated.
                 *
                 * \return True if a local variational operator was found.
                 *
                 * It might not if there is a restriction of the domain of definition (for instance elastic stiffness
                 * operator does not act upon geometric objects of dimension 1).
                 *
                 * This method should be called prior to GetNonCstLocalOperator(): the latter will assert if the
                 * \a ref_felt is invalid.
                 */
                bool DoConsider(const RefGeomElt& ref_geom_elt) const;


                /*!
                 * \brief Create a LocalVariationalOperator for each RefFEltInFEltSpace and store it into the class.
                 *
                 * \param[in] args List of variadic arguments given to the instance of the \a DerivedT
                 * constructor.
                 *
                 */
                template<typename... Args>
                void CreateLocalOperatorList(Args&&... args);


                /*!
                 * \brief Fetch the local operator associated to the finite element type.
                 *
                 * \return Local operator associated to a given reference geometric element. This method assumes the
                 * \a ref_geom_elt is valid and gets an associated LocalOperator (an assert is there in debug mode to check that);
                 * the check of such an assumption may be performed by a call to DoConsider() in case there is a genuine
                 * reason to check that in a release mode context.
                 *
                 * \param[in] ref_geom_elt Reference geometric element which local variational operator is requested.
                 *
                 */
                const LocalVariationalOperator& GetLocalOperator(const RefGeomElt& ref_geom_elt) const;


                /*!
                 * \brief Fetch the local operator associated to the finite element type.
                 *
                 * \return Local operator associated to a given reference geometric element. This method assumes the
                 * \a ref_geom_elt is valid and gets an associated LocalOperator (an assert is there in debug mode to check that);
                 * the check of such an assumption may be performed by a call to DoConsider() in case there is a genuine
                 * reason to check that in a release mode context.
                 *
                 * \param[in] ref_geom_elt Reference geometric element which local variational operator is requested.
                 *
                 */
                LocalVariationalOperator& GetNonCstLocalOperator(const RefGeomElt& ref_geom_elt);

                //! Iterator to the given local operator in \a local_operator_list_. cend() if none found.
                //! \param[in] ref_geom_elt \a Reference geometric element which iterator is sought.
                auto GetIterator(const RefGeomElt& ref_geom_elt) const;

                //! Compute pattern of the matrix.
                //! \param[out] map_values Must be empty in input. In output, container with informations about
                //! how the pattern was built.
                //! \return Computed matrix pattern.
                Wrappers::Petsc::MatrixPattern ComputePattern(std::unordered_map<std::size_t, std::vector<PetscScalar>>& map_values);

                //! Non constant access to the list of local operators.
                typename ElementaryInterpolatorT::vector_unique_ptr& GetNonCstLocalOperatorList() noexcept;


            private:


                //! List of local operators. There's one of them for each RefGeomElt.
                typename ElementaryInterpolatorT::vector_unique_ptr local_operator_list_;

            };


        } // namespace LagrangianNS


    } // namespace ConformInterpolatorNS


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/ConformInterpolator/Lagrangian/LagrangianInterpolator.hxx"


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_LAGRANGIAN_x_LAGRANGIAN_INTERPOLATOR_HPP_
