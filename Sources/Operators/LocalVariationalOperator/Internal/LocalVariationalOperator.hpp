/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 15 Sep 2016 11:15:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_HPP_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_HPP_

# include <memory>
# include <vector>

# include "Utilities/MatrixOrVector.hpp"

# include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
# include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

# include "Operators/LocalVariationalOperator/ElementaryData.hpp"
# include "Operators/LocalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        //! Convenient enum class.
        enum class assemble_into_matrix { no, yes };

        //! Convenient enum class.
        enum class assemble_into_vector { no, yes };


        namespace LocalVariationalOperatorNS
        {


            /*!
             * \brief Parent class of all LocalVariationalOperators.
             *
             * \tparam OperatorNatureT Whether the elementary data is related to a matrix, a vector or both.
             * \tparam MatrixTypeT Type of the elementary matrix, if relevant, or std::false_type otherwise.
             * \tparam VectorTypeT Type of the elementary vector, if relevant, or std::false_type otherwise.
             *
             * A LocalVariationalOperator aims to provide the computation of elementary data; it also holds some
             * data (quadrature rules, shape functions) common to all the FElts to which it might be applied.
             *
             * \internal <b><tt>[internal]</tt></b> The LocalVariationalOperator classes should by no mean be called
             * directly by anything but the base GlobalVariationalOperator class, even the public interface.
             * \endinternal
             *
             */
            template
            <
                Advanced::OperatorNS::Nature OperatorNatureT,
                class MatrixTypeT,
                class VectorTypeT
            >
            class LocalVariationalOperator
            : public Internal::LocalVariationalOperatorNS::ExtendedUnknownAndTestUnknownList<LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>>
            {

            private:

                //! Convenient alias to parent.
                using extended_unknown_and_test_unknown_list_parent =
                    Internal::LocalVariationalOperatorNS::ExtendedUnknownAndTestUnknownList<LocalVariationalOperator<OperatorNatureT, MatrixTypeT, VectorTypeT>>;

            public:

                //! Alias for ElementaryData type.
                using elementary_data_type = Advanced::ElementaryData<OperatorNatureT, MatrixTypeT, VectorTypeT>;

            public:


                /*!
                 * \brief Whether the operator can be assembled in vector, in matrix or in both.
                 *
                 * \return Whether the operator can be assembled in vector, in matrix or in both.
                 *
                 * \internal <b><tt>[internal]</tt></b> It can be used inside <> brackets due to its constexpr nature
                 * (for metaprogramming purposes).
                 * \endinternal
                 */
                constexpr static Advanced::OperatorNS::Nature GetOperatorNature();


            protected: // Important here:

                /// \name Special members.
                ///@{

                //! \copydoc doxygen_hide_local_var_op_constructor
                explicit LocalVariationalOperator(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                                  const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                                  elementary_data_type&& elementary_data);

                //! Destructor.
                virtual ~LocalVariationalOperator() = 0;

            private:

                //! \copydoc doxygen_hide_copy_constructor
                LocalVariationalOperator(const LocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                LocalVariationalOperator(LocalVariationalOperator&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                LocalVariationalOperator& operator=(const LocalVariationalOperator& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                LocalVariationalOperator& operator=(LocalVariationalOperator&& rhs) = delete;


                ///@}

            public:

                /*!
                 * \brief Link the LocalVariationalOperator to a given local finite element space.
                 *
                 * \param[in] local_felt_space Local finite element space to which the DerivedT will be associated.
                 *
                 * The data in elementary_data_ (matrices or vector mostly) are allocated at the beginning of the program
                 * but their values keeps changing; what makes them change is current method which is therefore called
                 * very often (typical assembling iterate through a wide range of local finite element spaces).
                 *
                 */
                void SetLocalFEltSpace(const LocalFEltSpace& local_felt_space);

                //! Gain access to the elementary data.
                const elementary_data_type& GetElementaryData() const;

                //! Gain non constant access to the elementary data.
                elementary_data_type& GetNonCstElementaryData();

                /*!
                 * \brief Set whether the assembling occurs into a vector or a matrix.
                 *
                 * This method is called automatically in each Assemble() call in the GlobalVariationalOperator;
                 * for bilinear and linear operators it is fairly trivial but might vary for non linear ones.
                 *
                 * \param[in] do_assemble_into_matrix Whether the operator is assembled into a matrix.
                 * \param[in] do_assemble_into_vector Whether the operator is assembled into a vector.
                 */
                void SetAssembleTarget(assemble_into_matrix do_assemble_into_matrix,
                                       assemble_into_vector do_assemble_into_vector) noexcept;

                /*!
                 * \brief Whether the operator is assembled into a matrix.
                 *
                 * \return True if this is the case, false otherwise.
                 */
                bool DoAssembleIntoMatrix() const noexcept;

                /*!
                 * \brief Whether the operator is assembled into a vector.
                 *
                 * \return True if this is the case, false otherwise.
                 */
                bool DoAssembleIntoVector() const noexcept;
                

            private:

                //! ElementaryData (matrices and/or vectors used for elementary calculation).
                elementary_data_type elementary_data_;

                //! Whether the operator is assembled into a matrix.
                bool do_assemble_into_matrix_;

                //! Whether the operator is assembled into a vector.
                bool do_assemble_into_vector_;

            };


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/LocalVariationalOperator/Internal/LocalVariationalOperator.hxx"


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_LOCAL_VARIATIONAL_OPERATOR_HPP_
