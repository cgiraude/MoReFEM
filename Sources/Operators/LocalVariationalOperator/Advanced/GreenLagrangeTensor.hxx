/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 18 Mar 2016 11:42:09 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GREEN_LAGRANGE_TENSOR_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GREEN_LAGRANGE_TENSOR_HXX_


namespace MoReFEM
{
    namespace Advanced
    {


        namespace LocalVariationalOperatorNS
        {


            inline unsigned int GreenLagrangeTensor::GetMeshDimension() const noexcept
            {
                assert(mesh_dimension_ == 2u || mesh_dimension_ == 3u);
                return mesh_dimension_;
            }


            inline const LocalVector& GreenLagrangeTensor::GetVector() const noexcept
            {
                return vector_;
            }


            inline LocalVector& GreenLagrangeTensor::GetNonCstVector() noexcept
            {
                return const_cast<LocalVector&>(GetVector());
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Advanced


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_GREEN_LAGRANGE_TENSOR_HXX_
