### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP}

	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryDataStorage.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ElementaryDataStorage.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknownAndTestUnknownList.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ExtendedUnknownAndTestUnknownList.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetSubMatrix.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetSubMatrix.hxx"
)

include(${CMAKE_CURRENT_LIST_DIR}/NumberingSubsetSubMatrix/SourceList.cmake)
