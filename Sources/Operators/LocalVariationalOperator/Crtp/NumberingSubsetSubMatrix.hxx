/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 11:34:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_HXX_
# define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace LocalVariationalOperatorNS
        {


            template<class DerivedT>
            LocalMatrix& NumberingSubsetSubMatrix<DerivedT>
            ::GetSubMatrix(const NumberingSubset& row_numbering_subset,
                           const NumberingSubset& col_numbering_subset) const
            {
                const auto& list = GetSubMatrixList();

                const auto end = list.end();

                using comp =
                    typename Internal::NumberingSubsetNS::FindIfConditionForPair<typename list_type::value_type>;

                auto it = std::find_if(list.begin(),
                                       end,
                                       comp(row_numbering_subset, col_numbering_subset));

                assert(it != end);
                assert(!(!*it));
                return (*it)->GetSubMatrix();

            }


            template<class DerivedT>
            template<class ElementaryDataT>
            void NumberingSubsetSubMatrix<DerivedT>
            ::AllocateSubMatrices(const ElementaryDataT& elementary_data)
            {
                static_assert(!std::is_same<typename ElementaryDataT::matrix_type, std::false_type>(),
                              "THis method should be called only for valid elementary_data!");

                const auto& derived = static_cast<DerivedT&>(*this);

                decltype(auto) extended_unknown_list = derived.GetExtendedUnknownList();

                NumberingSubset::vector_const_shared_ptr numbering_subset_list;

                {

                    for (const auto& extended_unknown_ptr : extended_unknown_list)
                    {
                        assert(!(!extended_unknown_ptr));
                        numbering_subset_list.push_back(extended_unknown_ptr->GetNumberingSubsetPtr());
                    }
                }

                Utilities::EliminateDuplicate(numbering_subset_list,
                                              Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

                decltype(auto) test_extended_unknown_list = derived.GetExtendedTestUnknownList();

                NumberingSubset::vector_const_shared_ptr test_numbering_subset_list;

                {

                    for (const auto& extended_test_unknown_ptr : test_extended_unknown_list)
                    {
                        assert(!(!extended_test_unknown_ptr));
                        test_numbering_subset_list.push_back(extended_test_unknown_ptr->GetNumberingSubsetPtr());
                    }
                }

                Utilities::EliminateDuplicate(test_numbering_subset_list,
                                              Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>(),
                                              Utilities::PointerComparison::Equal<NumberingSubset::const_shared_ptr>());

                NumberingSubset::vector_const_shared_ptr concatenate_numbering_subset_list;

                concatenate_numbering_subset_list.reserve(numbering_subset_list.size() + test_numbering_subset_list.size());

                std::set_union(numbering_subset_list.begin(),
                               numbering_subset_list.end(),
                               test_numbering_subset_list.begin(),
                               test_numbering_subset_list.end(),
                               std::back_inserter(concatenate_numbering_subset_list),
                               Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>());

                // Key : numbering_subset_id, Value : Ndof
                std::map<unsigned int, int> Ndof_per_numbering_subset;

                for (const auto& numbering_subset_ptr : concatenate_numbering_subset_list)
                {
                    assert(numbering_subset_ptr != nullptr);
                    const auto numbering_subset_id = numbering_subset_ptr->GetUniqueId();

                    unsigned int Ndof = 0u;

                    for (const auto& unknown_ptr : extended_unknown_list)
                    {
                        assert(!(!unknown_ptr));

                        if (unknown_ptr->GetNumberingSubset().GetUniqueId() == numbering_subset_id)
                            Ndof += elementary_data.GetRefFElt(*unknown_ptr).Ndof();
                    }

                    auto check = Ndof_per_numbering_subset.insert({numbering_subset_id, static_cast<int>(Ndof)});
                    static_cast<void>(check);
                    assert(check.second && "Numbering subset within the list must be unique!");
                }

                // \todo #513 All combinations allocated here... However the memory used is quite limited: only a handful
                // of local matrices.
                for (const auto& row_numbering_subset_ptr : numbering_subset_list)
                {
                    const auto& row_numbering_subset = *row_numbering_subset_ptr;

                    auto it_row = Ndof_per_numbering_subset.find(row_numbering_subset.GetUniqueId());

                    if (it_row != Ndof_per_numbering_subset.end())
                    {
                        const auto Nrow = static_cast<std::size_t>(it_row->second);

                        for (const auto& col_numbering_subset_ptr : test_numbering_subset_list)
                        {
                            const auto& col_numbering_subset = *col_numbering_subset_ptr;

                            auto it_col = Ndof_per_numbering_subset.find(col_numbering_subset.GetUniqueId());

                            if (it_col != Ndof_per_numbering_subset.end())
                            {
                                const auto Ncol = static_cast<std::size_t>(it_col->second);

                                auto ptr =
                                    std::make_unique<SubMatrixForNumberingSubsetPair>(row_numbering_subset,
                                                                                      col_numbering_subset);

                                auto& sub_matrix = ptr->GetSubMatrix();

                                sub_matrix.resize({ Nrow, Ncol });

                                sub_matrix_list_.emplace_back(std::move(ptr));
                            }
                        }
                    }
                }
            }


            template<class DerivedT>
            inline const typename NumberingSubsetSubMatrix<DerivedT>::list_type&
            NumberingSubsetSubMatrix<DerivedT>
            ::GetSubMatrixList() const
            {
                return sub_matrix_list_;
            }


        } // namespace LocalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_HXX_
