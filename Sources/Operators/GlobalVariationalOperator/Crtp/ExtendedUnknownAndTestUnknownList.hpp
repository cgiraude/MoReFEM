/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 May 2016 14:07:34 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HPP_
# define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HPP_

# include <cassert>
# include <algorithm>

# include "Utilities/Miscellaneous.hpp"

# include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            /*!
             * \brief Crtp that stores one or two ExtendedUnknowns objects and provide constant accessors to them.
             */
            template<class DerivedT>
            class ExtendedUnknownAndTestUnknownList
            {

            public:

                /// \name Special members.
                ///@{

             

                /*!
                 * \brief Constructor.
                 *
                 * \param[in] extended_unknown_list List of extended unknowns to use, sort in an arbitrary order
                 * that will be followed in implementation of global and local operators.
                 * \a LocalVariationalOperator instances will by construct follow the same convention.
                 * \copydoc doxygen_hide_test_extended_unknown_list_param
                 */
                explicit ExtendedUnknownAndTestUnknownList(const ExtendedUnknown::vector_const_shared_ptr extended_unknown_list,
                                                           const ExtendedUnknown::vector_const_shared_ptr test_extended_unknown_list);

                //! Destructor.
                ~ExtendedUnknownAndTestUnknownList() = default;

                //! \copydoc doxygen_hide_copy_constructor
                ExtendedUnknownAndTestUnknownList(const ExtendedUnknownAndTestUnknownList& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ExtendedUnknownAndTestUnknownList(ExtendedUnknownAndTestUnknownList&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ExtendedUnknownAndTestUnknownList& operator=(const ExtendedUnknownAndTestUnknownList& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ExtendedUnknownAndTestUnknownList& operator=(ExtendedUnknownAndTestUnknownList&& rhs) = delete;

                ///@}

             public :

                //! Return the list of Unknowns and their associated numbering subset.
                const ExtendedUnknown::vector_const_shared_ptr& GetExtendedUnknownList() const;

                /*!
                 * \brief Returns the list of ExtendedUnknowns that match \a numbering_subset.
                 *
                 * \param[in] numbering_subset \a NumberingSubset used as filter.
                 */
                const ExtendedUnknown::vector_const_shared_ptr& GetExtendedUnknownList(const NumberingSubset& numbering_subset) const;


                //! Get the list of numbering subsets.
                const NumberingSubset::vector_const_shared_ptr& GetNumberingSubsetList() const;

            public :

                //! Return the list of test Unknowns and their associated numbering subset.
                const ExtendedUnknown::vector_const_shared_ptr& GetExtendedTestUnknownList() const;

                /*!
                 * \brief Returns the list of test ExtendedUnknowns that match \a numbering_subset.
                 *
                 * \param[in] numbering_subset \a NumberingSubset used as filter.
                 */
                const ExtendedUnknown::vector_const_shared_ptr& GetExtendedTestUnknownList(const NumberingSubset& numbering_subset) const;

                //! Get the list of test numbering subsets.
                const NumberingSubset::vector_const_shared_ptr& GetTestNumberingSubsetList() const;

            protected:

                /*!
                 * \class doxygen_hide_crtp_Nunknown_method
                 *
                 * \brief Access to one of the unknowns.
                 *
                 * \attention This method is protected as it should be used with great care: index is  NOT the unique
                 * id related to the unknown, but the position of the \a ExtendedUnknown within the operator.
                 * This position is determined by the \a GlobalVariationalOperator constructor: first unknown
                 * is indexed 0, and the second (if any) 1.
                 * For instance, for ScalarDivVectorial operator the convention is to specify vectorial first and
                 *  scalar second.
                 *
                 * \param[in] index Index of the required \a ExtendedUnknown within the operator.
                 *
                 * \return Requested \a ExtendedUnknown.
                 */


                //! \copydoc doxygen_hide_crtp_Nunknown_method
                const ExtendedUnknown& GetNthUnknown(std::size_t index = 0) const;


                /*!
                 * \brief Constant accessor to the \a ExtendedUnknown list per numbering subset.
                 */
                const std::map<unsigned int, ExtendedUnknown::vector_const_shared_ptr>&
                    GetExtendedUnknownListPerNumberingSubset() const noexcept;

            protected:

                //! \copydoc doxygen_hide_crtp_Nunknown_method
                const ExtendedUnknown& GetNthTestUnknown(std::size_t index = 0) const;


                /*!
                 * \brief Constant accessor to the \a test ExtendedUnknown list per numbering subset.
                 */
                const std::map<unsigned int, ExtendedUnknown::vector_const_shared_ptr>&
                GetExtendedTestUnknownListPerNumberingSubset() const noexcept;

            private:

                //! Unknown/numbering subset list.
                const ExtendedUnknown::vector_const_shared_ptr extended_unknown_list_;

                //! List of numbering subsets.
                NumberingSubset::vector_const_shared_ptr numbering_subset_list_;

                /*!
                 * \brief For each numbering subset, the list of ExtendedUnknown.
                 *
                 * The ordering here is the same as in \a extended_unknown_list_.
                 */
                std::map<unsigned int, ExtendedUnknown::vector_const_shared_ptr>
                    extended_unknown_list_per_numbering_subset_;


            private:

                //! Unknown/numbering subset list.
                const ExtendedUnknown::vector_const_shared_ptr test_extended_unknown_list_;

                //! List of numbering subsets.
                NumberingSubset::vector_const_shared_ptr test_numbering_subset_list_;

                /*!
                 * \brief For each numbering subset, the list of ExtendedUnknown.
                 *
                 * The ordering here is the same as in \a test_extended_unknown_list_.
                 */
                std::map<unsigned int, ExtendedUnknown::vector_const_shared_ptr>
                test_extended_unknown_list_per_numbering_subset_;


            };


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


# include "Operators/GlobalVariationalOperator/Crtp/ExtendedUnknownAndTestUnknownList.hxx"


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_CRTP_x_EXTENDED_UNKNOWN_AND_TEST_UNKNOWN_LIST_HPP_
