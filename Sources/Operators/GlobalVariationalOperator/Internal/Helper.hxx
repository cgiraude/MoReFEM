/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 29 Apr 2015 15:16:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_HELPER_HXX_
# define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_HELPER_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace GlobalVariationalOperatorNS
        {


            template<class LinearAlgebraTupleT>
            void Assembly(const LinearAlgebraTupleT& linear_algebra_tuple)
            {
                static_assert(Utilities::IsSpecializationOf<std::tuple, LinearAlgebraTupleT>::value,
                              "Compilation error if LinearAlgebraTupleT is not a std::tuple.");

                Recursivity<LinearAlgebraTupleT, 0, std::tuple_size<LinearAlgebraTupleT>::value>
                ::Assembly(linear_algebra_tuple);

            }


            template
            <
                class LinearAlgebraTupleT,
                class GlobalVariationalOperatorT,
                class LocalOperatorT
            >
            void InjectInGlobalLinearAlgebra(const GlobalVariationalOperatorT& global_variational_operator,
                                             const LinearAlgebraTupleT& linear_algebra_tuple,
                                             const LocalFEltSpace& local_felt_space,
                                             LocalOperatorT& local_operator)
            {

                Recursivity<LinearAlgebraTupleT, 0, std::tuple_size<LinearAlgebraTupleT>::value>
                ::InjectInGlobalLinearAlgebra(global_variational_operator,
                                              linear_algebra_tuple,
                                              local_felt_space,
                                              local_operator);
            }



            template
            <
                class LinearAlgebraTupleT,
                std::size_t I,
                std::size_t TupleSizeT
            >
            void Recursivity<LinearAlgebraTupleT, I, TupleSizeT>
            ::Assembly(const LinearAlgebraTupleT& linear_algebra_tuple)
            {
                static_assert(I < TupleSizeT, "Otherwise endless recursion!");

                auto& current_linear_algebra = std::get<I>(linear_algebra_tuple).first;
                current_linear_algebra.Assembly(__FILE__, __LINE__);

                Recursivity<LinearAlgebraTupleT, I + 1, TupleSizeT>::Assembly(linear_algebra_tuple);
            }



            template
            <
                class LinearAlgebraTupleT,
                std::size_t I,
                std::size_t TupleSizeT
            >
            template
            <
                class GlobalVariationalOperatorT,
                class LocalOperatorT
            >
            void Recursivity<LinearAlgebraTupleT, I, TupleSizeT>
            ::InjectInGlobalLinearAlgebra(const GlobalVariationalOperatorT& global_variational_operator,
                                          const LinearAlgebraTupleT& linear_algebra_tuple,
                                          const LocalFEltSpace& local_felt_space,
                                          LocalOperatorT& local_variational_operator)
            {
                static_assert(I < TupleSizeT, "Otherwise endless recursion!");


                // \todo #1076 Reactiuvate that somehow! (OPeratorNature must be made available to GlobalOperator.
//                static_assert(! (
//                                 (std::is_same<current_type, GlobalMatrixWithCoefficient>::value
//                                  && GlobalVariationalOperatorT::GetOperatorNature() == Advanced::OperatorNS::Nature::linear)
//                                 ||
//                                 (std::is_same<current_type, GlobalVectorWithCoefficient>::value
//                                  && GlobalVariationalOperatorT::GetOperatorNature() == Advanced::OperatorNS::Nature::bilinear)
//                                 )
//                              , "Given linear algebra given for assembling must be compatible with the operator.");


                const double previous_coefficient =
                ZeroSpecialCase<LinearAlgebraTupleT, I>::template FetchPreviousCoefficient<current_type>(linear_algebra_tuple);

                const auto& global_linear_algebra = std::get<I>(linear_algebra_tuple);

                InjectInGlobalLinearAlgebraImpl(global_variational_operator,
                                                local_felt_space,
                                                local_variational_operator,
                                                global_linear_algebra,
                                                previous_coefficient);


                Recursivity<LinearAlgebraTupleT, I + 1, TupleSizeT>::InjectInGlobalLinearAlgebra(global_variational_operator,
                                                                                                 linear_algebra_tuple,
                                                                                                 local_felt_space,
                                                                                                 local_variational_operator);
            }


            template
            <
                class LinearAlgebraTupleT,
                std::size_t I,
                std::size_t TupleSizeT
            >
            template
            <
                class GlobalVariationalOperatorT,
                class LocalOperatorT
            >
            void Recursivity<LinearAlgebraTupleT, I, TupleSizeT>
            ::InjectInGlobalLinearAlgebraImpl(const GlobalVariationalOperatorT& global_variational_operator,
                                              const LocalFEltSpace& local_felt_space,
                                              LocalOperatorT& local_variational_operator,
                                              const GlobalMatrixWithCoefficient& global_matrix_with_coefficient,
                                              const double previous_coefficient)
            {
                auto& global_matrix = global_matrix_with_coefficient.first;

                const auto& row_numbering_subset = global_matrix.GetRowNumberingSubset();
                const auto& col_numbering_subset = global_matrix.GetColNumberingSubset();

                auto& elementary_data = local_variational_operator.GetNonCstElementaryData();

                auto& local_matrix = elementary_data.GetNonCstMatrixResult();

                const double coefficient = global_matrix_with_coefficient.second;

                assert(!NumericNS::IsZero(previous_coefficient));

                // Trick here: local matrix was multiplied by a coefficient in previous iteration; we here both apply
                // the new one and cancel the previous one.
                const auto factor = coefficient / previous_coefficient;

                if (!NumericNS::AreEqual(factor, 1.))
                    local_matrix *= factor;

                decltype(auto) row_test_extended_unknown_list =
                    global_variational_operator.GetExtendedTestUnknownList(row_numbering_subset);

                const auto& row_local_2_global = local_felt_space.GetLocal2Global<MpiScale::program_wise>(row_test_extended_unknown_list);

				assert(row_local_2_global.size() == local_matrix.shape(0));

                decltype(auto) col_extended_unknown_list =
                    global_variational_operator.GetExtendedUnknownList(col_numbering_subset);
                
                const auto& col_local_2_global = local_felt_space.GetLocal2Global<MpiScale::program_wise>(col_extended_unknown_list);

              	assert(col_local_2_global.size() == local_matrix.shape(1));

                auto row_local_matrix = 0ul;
                auto col_local_matrix = 0ul;

                for (auto& row : row_local_2_global)
                {
                    col_local_matrix = 0ul;

                    for (auto& col : col_local_2_global)
                    {
                        global_matrix.SetValue(row,
                                               col,
                                               local_matrix(row_local_matrix, col_local_matrix),
                                               ADD_VALUES,
                                               __FILE__, __LINE__);

                        ++col_local_matrix;
                    }

                    ++row_local_matrix;
                }
            }


            template
            <
                class LinearAlgebraTupleT,
                std::size_t I,
                std::size_t TupleSizeT
            >
            template
            <
                class GlobalVariationalOperatorT,
                class LocalOperatorT
            >
            void Recursivity<LinearAlgebraTupleT, I, TupleSizeT>
            ::InjectInGlobalLinearAlgebraImpl(const GlobalVariationalOperatorT& global_variational_operator,
                                              const LocalFEltSpace& local_felt_space,
                                              LocalOperatorT& local_variational_operator,
                                              const GlobalVectorWithCoefficient& global_vector_with_coefficient,
                                              const double previous_coefficient)
            {
                auto& global_vector = global_vector_with_coefficient.first;

                auto& elementary_data = local_variational_operator.GetNonCstElementaryData();
                auto& local_vector = elementary_data.GetNonCstVectorResult();

                const double coefficient = global_vector_with_coefficient.second;

                assert(!NumericNS::IsZero(previous_coefficient));

                // Trick here: local matrix was multiplied by a coefficient in previous iteration; we here both apply
                // the new one and cancel the previous one.
                const auto factor = coefficient / previous_coefficient;

                if (!NumericNS::AreEqual(factor, 1.))
                    local_vector *= factor;

                const auto& numbering_subset = global_vector.GetNumberingSubset();

                decltype(auto) test_unknown_list =
                    global_variational_operator.GetExtendedTestUnknownList(numbering_subset);

                decltype(auto) local_2_global =
                    local_felt_space.GetLocal2Global<MpiScale::program_wise>(test_unknown_list);

                assert(local_2_global.size() == local_vector.size());
                if constexpr(std::is_same<std::decay_t<decltype(local_vector)>, LocalVector>())
                    global_vector.SetValues(local_2_global,
                                            local_vector.data(),
                                            ADD_VALUES,
                                            __FILE__, __LINE__);
                else
                    global_vector.SetValues(local_2_global,
                                            local_vector.GetData(),
                                            ADD_VALUES,
                                            __FILE__, __LINE__);
            }


            template<class LinearAlgebraTupleT, std::size_t TupleSizeT>
            void Recursivity<LinearAlgebraTupleT, TupleSizeT, TupleSizeT>
            ::Assembly(const LinearAlgebraTupleT& linear_algebra_tuple)
            {
                // End recursion.
                static_cast<void>(linear_algebra_tuple);
            }


            template<class LinearAlgebraTupleT, std::size_t TupleSizeT>
            template
            <
                class GlobalVariationalOperatorT,
                class LocalOperatorT
            >
            void Recursivity<LinearAlgebraTupleT, TupleSizeT, TupleSizeT>
            ::InjectInGlobalLinearAlgebra(const GlobalVariationalOperatorT& global_variational_operator,
                                          const LinearAlgebraTupleT& linear_algebra_tuple,
                                          const LocalFEltSpace& local_felt_space,
                                          LocalOperatorT& local_variational_operator)
            {
                // End recursion.
                static_cast<void>(global_variational_operator);
                static_cast<void>(linear_algebra_tuple);
                static_cast<void>(local_felt_space);
                static_cast<void>(local_variational_operator);
            }


            template<class LinearAlgebraTupleT>
            template<class CurrentTypeT>
            double ZeroSpecialCase<LinearAlgebraTupleT, 0>
            ::FetchPreviousCoefficient(const LinearAlgebraTupleT& linear_algebra_tuple)
            {
                static_cast<void>(linear_algebra_tuple);
                return 1.;
            }



            template<class LinearAlgebraTupleT, std::size_t I>
            template<class CurrentTypeT>
            double ZeroSpecialCase<LinearAlgebraTupleT, I>
            ::FetchPreviousCoefficient(const LinearAlgebraTupleT& linear_algebra_tuple)
            {
                // What we really seek is the previous coefficient FOR THE SAME TYPE of linear algebra.
                // However, nothing prevent a user to ask for:
                // non_linear_operator.Assemble(*matrix1*, *vector1*, *matrix2*);
                // In this case, the expected previous coefficient are respectively: 1., 1., coeff(matrix1).

                using previous_type = typename std::tuple_element<I - 1, LinearAlgebraTupleT>::type;

                return std::is_same<CurrentTypeT, previous_type>::value
                ? std::get<I - 1>(linear_algebra_tuple).second
                : ZeroSpecialCase<LinearAlgebraTupleT, I - 1>::template FetchPreviousCoefficient<CurrentTypeT>(linear_algebra_tuple);

            }


        } // namespace GlobalVariationalOperatorNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_HELPER_HXX_
