/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 15:35:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_GRADIENT2_STRAIN_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_GRADIENT2_STRAIN_HXX_


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {


            template<int DimensionT>
            const LocalMatrix& TransposeGradient2Strain()
            {
                // \todo #1292 Probably remove entirely the function
                static auto ret = xt::eval(xt::transpose(Gradient2Strain<DimensionT>()));
                return ret;
            };


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_GRADIENT2_STRAIN_HXX_
