/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 16:10:44 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_

# include <memory>
# include <vector>

# include "Utilities/MatrixOrVector.hpp"

# include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"
# include "ParameterInstances/GradientBasedElasticityTensor/Internal/Configuration.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace ParameterNS
        {



            //! Helper class that actually computes the tensor given Young modulus and Poisson ratio.
            template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
            class ComputeGradientBasedElasticityTensor
            {
            private:

                //! Alias to traits class that enrich ConfigurationT.
                using traits = TraitsNS::Configuration<ConfigurationT>;

            public:

                /// \name Special members.
                ///@{

                //! Constructor.
                explicit ComputeGradientBasedElasticityTensor();

                //! Destructor.
                ~ComputeGradientBasedElasticityTensor() = default;

                //! \copydoc doxygen_hide_copy_constructor
                ComputeGradientBasedElasticityTensor(const ComputeGradientBasedElasticityTensor& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                ComputeGradientBasedElasticityTensor(ComputeGradientBasedElasticityTensor&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                ComputeGradientBasedElasticityTensor& operator=(const ComputeGradientBasedElasticityTensor& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                ComputeGradientBasedElasticityTensor& operator=(ComputeGradientBasedElasticityTensor&& rhs) = delete;

                ///@}

                //! Compute the gradient based elasticity tensor and returns it.
                //! \param[in] young_modulus Young's modulus at a given spatial position.
                //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
                const LocalMatrix& Compute(double young_modulus, double poisson_ratio);

                /*!
                 * \brief Returns the value of the gradient based elasticity tensor.
                 *
                 * Should not be called in non constant case (this is up to GradientBasedElasticityTensor class
                 * to ensure that (except for GetAnyValue()); only this class is expected to manipulate present one).
                 *
                 * \return Value of the gradient based elasticity tensor.
                 */
                const LocalMatrix& GetResult() const noexcept;

                //! Whether the tensor has already been computed. Meaningful only for spatially constant parameters.
                bool IsAlreadyComputed() const;


            private:

                //! Compute the Engineering elasticity tensor and returns it.
                //! \param[in] young_modulus Young's modulus at a given spatial position.
                //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
                const LocalMatrix& ComputeEngineeringElasticityTensor(double young_modulus, double poisson_ratio);

                //! Non constant access to engineering_elasticity_tensor_.
                LocalMatrix& GetNonCstEngineeringElasticityTensor();

                //! Non constant access to result_.
                LocalMatrix& GetNonCstResult();

                //! Non constant access to intermediate_product_.
                LocalMatrix& GetNonCstIntermediateProduct();

            private:

                //! Storage of the matrix of interest.
                LocalMatrix result_;

                //! Engineering elasticity tensor.
                LocalMatrix engineering_elasticity_tensor_;

                //! Matrix used to store intermediate product in computation of result_.
                LocalMatrix intermediate_product_;


            };


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1>
            ::ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);

            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain>
            ::ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);

            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress>
            ::ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);


            template<>
            const LocalMatrix& ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3>
            ::ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);




            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


        } // namespace ParameterNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_GRADIENT_BASED_ELASTICITY_TENSOR_x_INTERNAL_x_COMPUTE_GRADIENT_BASED_ELASTICITY_TENSOR_HPP_
