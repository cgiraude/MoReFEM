/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 24 Aug 2018 11:05:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#include <sstream>

#include "ParameterInstances/Compound/Solid/Exceptions/Solid.hpp"


namespace // anonymous
{


    std::string UndefinedDataMsg(const std::string& data_name);


} // namespace anonymous


namespace MoReFEM
{
    
    
    namespace SolidNS
    {


        UndefinedData::UndefinedData(const std::string& data, const char* invoking_file, int invoking_line)
        : ::MoReFEM::Exception(UndefinedDataMsg(data), invoking_file, invoking_line)
        { }


        UndefinedData::~UndefinedData() = default;
     
        
    } // namespace SolidNS


} // namespace MoReFEM


namespace // anonymous
{


    std::string UndefinedDataMsg(const std::string& data_name)
    {
        std::ostringstream oconv;
        oconv << "Data '" << data_name << "' is undefined: in all likelihood either the model is ill-formed and the "
        "data is not in the InputData, or it is but in the Lua file the chosen nature was 'ignore'.";
        std::string ret = oconv.str();
        return ret;
    }


} // namespace anonymous


/// @} // addtogroup ParameterInstancesGroup

