/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 24 Aug 2018 11:05:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_EXCEPTIONS_x_SOLID_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_EXCEPTIONS_x_SOLID_HPP_

# include <string>

# include "Utilities/Exceptions/Exception.hpp"


namespace MoReFEM
{


    namespace SolidNS
    {



        //! Exception thrown when access to an undefined data is attempted.
        class UndefinedData : public ::MoReFEM::Exception
        {
        public:

            /*!
             * \brief Constructor with simple message
             *
             * \param[in] data Name of the data concerned, e.g. 'volumic mass'.
             * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
             * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
             */
            explicit UndefinedData(const std::string& data, const char* invoking_file, int invoking_line);

            //! Destructor
            virtual ~UndefinedData() override;

            //! \copydoc doxygen_hide_copy_constructor
            UndefinedData(const UndefinedData& rhs) = default;

            //! \copydoc doxygen_hide_move_constructor
            UndefinedData(UndefinedData&& rhs) = default;

            //! \copydoc doxygen_hide_copy_affectation
            UndefinedData& operator=(const UndefinedData& rhs) = default;

            //! \copydoc doxygen_hide_move_affectation
            UndefinedData& operator=(UndefinedData&& rhs) = default;

        };


    } // namespace SolidNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/Compound/Solid/Exceptions/Solid.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_EXCEPTIONS_x_SOLID_HPP_
