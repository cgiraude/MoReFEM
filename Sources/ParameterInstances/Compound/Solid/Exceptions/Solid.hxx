/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 24 Aug 2018 11:05:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_EXCEPTIONS_x_SOLID_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_EXCEPTIONS_x_SOLID_HXX_


namespace MoReFEM
{


    namespace SolidNS
    {



    } // namespace SolidNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_EXCEPTIONS_x_SOLID_HXX_
