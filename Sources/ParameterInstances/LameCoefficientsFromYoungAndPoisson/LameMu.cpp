/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 29 May 2015 14:24:14 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#include "ParameterInstances/LameCoefficientsFromYoungAndPoisson/LameMu.hpp"


namespace MoReFEM
{
    
    
    namespace ParameterNS
    {
        
        
        LameMu::LameMu(const scalar_parameter& young_modulus,
                       const scalar_parameter& poisson_ratio)
        : scalar_parameter("Lame coefficient 'mu'",
                           young_modulus.GetDomain()),
        young_modulus_(young_modulus),
        poisson_ratio_(poisson_ratio)
        {
            assert(young_modulus.GetDomain() == poisson_ratio.GetDomain());
                   
            if (IsConstant())
                constant_value_ = ComputeValue(young_modulus.GetConstantValue(), poisson_ratio.GetConstantValue());
        }
        
        
        LameMu::~LameMu() = default;
        
        
        void LameMu::SupplWrite(std::ostream& out) const
        {
            out << "# Lame lambda is defined from Young modulus and Poisson ratio, which values are defined the "
            "following way:" << std::endl;
            GetYoungModulus().Write(out);
            GetPoissonRatio().Write(out);
        }
        
        
        void LameMu::SupplTimeUpdate()
        {
            assert(!GetYoungModulus().IsTimeDependent());
            assert(!GetPoissonRatio().IsTimeDependent());
        }
        
        
        void LameMu::SupplTimeUpdate(double time)
        {
            assert(!GetYoungModulus().IsTimeDependent());
            assert(!GetPoissonRatio().IsTimeDependent());
            static_cast<void>(time);
        }

        
        
        double LameMu::SupplGetAnyValue() const
        {
            return 0.;
        }

        
        
    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup
