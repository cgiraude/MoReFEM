/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Oct 2015 15:29:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HXX_


namespace MoReFEM
{


    template<ParameterNS::Type TypeT>
    const std::string& FiberList<TypeT>::ClassName()
    {
        static std::string ret = std::string("FiberList<") + ParameterNS::Traits<TypeT>::TypeName() + ">";
        return ret;
    }


    template<ParameterNS::Type TypeT>
    FiberList<TypeT>::FiberList(const unsigned int unique_id,
                                const std::string& fiber_file,
                                const Domain& domain,
                                const FEltSpace& felt_space,
                                const TimeManager& time_manager,
                                const Unknown& unknown)
    : unique_id_parent(unique_id),
    parent("Fiber manager",
           domain),
    felt_space_(felt_space),
    time_manager_(time_manager),
    unknown_(unknown)
    {
        assert(felt_space.GetDomain().GetUniqueId() == domain.GetUniqueId()
               && "Domain to create the parameter and domain used to define the finite element space should be "
               "the same.");

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
        const auto& god_of_dof = *god_of_dof_ptr;

        const auto& numbering_subset = felt_space.GetNumberingSubset(unknown);

        global_vector_ = std::make_unique<GlobalVector>(numbering_subset);
        AllocateGlobalVector(god_of_dof, *global_vector_);

        assert(god_of_dof.GetMesh().GetUniqueId() == domain.GetMesh().GetUniqueId());

        typename Internal::FiberNS::Traits<TypeT>::value_list_per_coord_index_type value_list_per_coord_index;

        decltype(auto) mesh = god_of_dof.GetMesh();

        Internal::FiberNS::ReadFiberFile<TypeT>(god_of_dof.GetMpi(),
                                               fiber_file,
                                               mesh,
                                                domain,
                                                value_list_per_coord_index);

        std::cout << god_of_dof.GetMpi().GetRankPreffix() << ' '
        << value_list_per_coord_index.size() << " values read in the fiber file." << std::endl;

        auto& global_vector = GetNonCstGlobalVector();

        // Values of fiber are actually given at dofs; so build first a \a ParameterAtDof.
        Internal::FiberNS::FillGlobalVector<TypeT>(value_list_per_coord_index,
                                                   god_of_dof.GetProcessorWiseNodeBearerList(),
                                                   unknown,
                                                   numbering_subset,
                                                   global_vector);
    }


    template<ParameterNS::Type TypeT>
    void FiberList<TypeT>::Initialize(const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    {
        assert(underlying_parameter_ == nullptr);
        const auto& domain = this->GetDomain();
        const auto& felt_space = GetFeltSpace();
        const auto& time_manager = GetTimeManager();
        const auto& unknown = GetUnknown();

        ParameterAtDof<TypeT, ParameterNS::TimeDependencyNS::None> param_at_dof("Temporary",
                                                                                domain,
                                                                                felt_space,
                                                                                unknown,
                                                                                GetNonCstGlobalVector());

        // Then convert them into the actual attribute.
        using converter_type =
        ParameterNS::FromParameterAtDof
        <
            TypeT,
            ParameterNS::TimeDependencyNS::None,
            1u
        >;

        underlying_parameter_ = converter_type::Perform("Fiber manager",
                                                        domain,
                                                        quadrature_rule_per_topology == nullptr
                                                        ? felt_space.GetQuadratureRulePerTopologyRawPtr()
                                                        : quadrature_rule_per_topology,
                                                        time_manager,
                                                        param_at_dof);
    }


    template<ParameterNS::Type TypeT>
    inline bool FiberList<TypeT>::IsConstant() const noexcept
    {
        return GetUnderlyingParameter().IsConstant();
    }


    template<ParameterNS::Type TypeT>
    inline typename FiberList<TypeT>::return_type FiberList<TypeT>::SupplGetConstantValue() const
    {
        return GetUnderlyingParameter().SupplGetConstantValue();
    }


    template<ParameterNS::Type TypeT>
    inline typename FiberList<TypeT>::return_type FiberList<TypeT>::SupplGetAnyValue() const
    {
        return GetUnderlyingParameter().SupplGetAnyValue();
    }


    template<ParameterNS::Type TypeT>
    inline typename FiberList<TypeT>::return_type FiberList<TypeT>
    ::SupplGetValue(const local_coords_type& local_coords,
                    const GeometricElt& geom_elt) const
    {
        return GetUnderlyingParameter().SupplGetValue(local_coords, geom_elt);
    }


    template<ParameterNS::Type TypeT>
    inline void FiberList<TypeT>::SupplWrite(std::ostream& out) const
    {
        return GetUnderlyingParameter().SupplWrite(out);
    }


    template<ParameterNS::Type TypeT>
    inline GlobalVector& FiberList<TypeT>::GetNonCstGlobalVector() noexcept
    {
        return const_cast<GlobalVector&>(GetGlobalVector());
    }


    template<ParameterNS::Type TypeT>
    inline const GlobalVector& FiberList<TypeT>::GetGlobalVector() const noexcept
    {
        assert(!(!global_vector_));
        return *global_vector_;
    }


    template<ParameterNS::Type TypeT>
    inline const typename FiberList<TypeT>::parameter_type& FiberList<TypeT>
    ::GetUnderlyingParameter() const noexcept
    {
        assert(!(!underlying_parameter_) && "You should call Initialize() in your model.");
        return *underlying_parameter_;
    }


    template<ParameterNS::Type TypeT>
    void FiberList<TypeT>::SupplTimeUpdate()
    {
        assert(!GetUnderlyingParameter().IsTimeDependent());
    }


    template<ParameterNS::Type TypeT>
    void FiberList<TypeT>::SupplTimeUpdate(double time)
    {
        assert(!GetUnderlyingParameter().IsTimeDependent());
        static_cast<void>(time);
    }


    template<ParameterNS::Type TypeT>
    inline void FiberList<TypeT>::SetConstantValue(double value)
    {
        static_cast<void>(value);
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT>
    inline const FEltSpace&  FiberList<TypeT>::GetFeltSpace() const noexcept
    {
        return felt_space_;
    }


    template<ParameterNS::Type TypeT>
    const TimeManager& FiberList<TypeT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }

    template<ParameterNS::Type TypeT>
    const Unknown& FiberList<TypeT>::GetUnknown() const noexcept
    {
        return unknown_;
    }



} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HXX_
