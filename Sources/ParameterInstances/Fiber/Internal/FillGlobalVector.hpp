/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:00:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FILL_GLOBAL_VECTOR_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FILL_GLOBAL_VECTOR_HPP_

# include <unordered_map>
# include <string>

# include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

# include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"

# include "ParameterInstances/Fiber/Internal/Traits.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class Unknown;
    class NumberingSubset;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    namespace Internal
    {


        namespace FiberNS
        {


            /*!
             * \brief Fill the global vector from the data read in the file.
             *
             * This function is an helper function of FiberList constructor.
             *
             * \param[in] value_list_per_coord_index THe output of ReadFiberFile() function.
             * \param[in] node_bearer_list List of all node bearers considered in the GodOfDof on current processor.
             * \param[in] unknown A scalar or vectorial unknown that acts a bit as a strawman: dofs are defined only
             * in relationship to an unknown, so you must create one if none fulfill your purposes (for instance
             * if you deal with a vectorial unknown and need a scalar Dof field, you must create another unknown
             * only for ther Parameter. To save space, it's better if this unknown is in its own numbering subset,
             * but this is not mandatory. Unknown should be scalar for TypeT == ParameterNS::Type::scalar and vectorial
             * for TypeT == ParameterNS::Type::vectorial.
             * \param[in] numbering_subset Numbering subset upon which the unknown is defined.
             * \param[out] vector The global vector upon which Parameter is to be defined completely filled.
             *
             */

            //!
            template<ParameterNS::Type TypeT>
            void FillGlobalVector(const typename Traits<TypeT>::value_list_per_coord_index_type& value_list_per_coord_index,
                                  const NodeBearer::vector_shared_ptr& node_bearer_list,
                                  const Unknown& unknown,
                                  const NumberingSubset& numbering_subset,
                                  GlobalVector& vector);



        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/Fiber/Internal/FillGlobalVector.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FILL_GLOBAL_VECTOR_HPP_
