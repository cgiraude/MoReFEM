/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 21:51:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HPP_

# include <unordered_map>
# include <array>

# include "Parameters/ParameterType.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FiberNS
        {


            /*!
             * \brief Traits class conveniently used to provide a generic interface for both scalar and vector
             * fiber managers.
             */
            template<ParameterNS::Type TypeT>
            struct Traits;


            // ============================
            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            // ============================


            template<>
            struct Traits<ParameterNS::Type::scalar>
            {

                using value_list_type = std::array<double, 1>;

                using value_list_per_coord_index_type = std::unordered_map<unsigned int, value_list_type>;

                static void CheckFirstLineOfFile(const std::string& fiber_file,
                                                 const std::string& line);

                static constexpr unsigned int NvertexPerLine();

                static constexpr unsigned int NvaluePerVertex();


            };


            template<>
            struct Traits<ParameterNS::Type::vector>
            {
                using value_list_type = std::array<double, 3>;

                using value_list_per_coord_index_type = std::unordered_map<unsigned int, value_list_type>;

                static void CheckFirstLineOfFile(const std::string& fiber_file,
                                                 const std::string& line);

                static constexpr unsigned int NvertexPerLine();

                static constexpr unsigned int NvaluePerVertex();

            };

            // ============================
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN
            // ============================



        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/Fiber/Internal/Traits.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HPP_
