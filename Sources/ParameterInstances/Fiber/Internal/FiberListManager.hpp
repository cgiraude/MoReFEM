/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 10:32:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FIBER_LIST_MANAGER_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FIBER_LIST_MANAGER_HPP_

# include "Utilities/Singleton/Singleton.hpp"

# include "Parameters/ParameterType.hpp"
# include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"

# include "Geometry/Mesh/Internal/MeshManager.hpp"

# include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

# include "ParameterInstances/Fiber/FiberList.hpp"


namespace MoReFEM
{


    namespace Internal
    {


        namespace FiberNS
        {


            /*!
             * \brief This class is used to create and retrieve fiber_list_type objects.
             *
             * fiber_list_type objects get private constructor and can only be created through this class. In addition
             * to their creation, this class keeps their address, so it's possible from instance to retrieve a
             * fiber_list_type object given its unique id (which is the one that appears in the input data file).
             *
             * \tparam TypeT There is actually one manager for scalar parameters and another for vectorial ones.
             */
            template<ParameterNS::Type TypeT>
            class FiberListManager : public Utilities::Singleton<FiberListManager<TypeT>>
            {

            public:

                /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
                static const std::string& ClassName();


                /*!
                 * \brief Base type of Mesh as input parameter (requested to identify domains in the input parameter data).
                 *
                 * Mesh is not an error: there is currently exactly one god of dof created for each mesh.
                 */
                using input_data_type = ::MoReFEM::InputDataNS::BaseNS::Fiber<TypeT>;

                //! Alias to the type of fiber_list_type stored.
                using fiber_list_type = FiberList<TypeT>;


            public:


                /*!
                 * \brief Create a new fiber_list_type object from the data of the input data file.
                 *
                 * \param[in] section Section read in the input file.
                 */
                template<class FiberSectionT>
                void Create(const FiberSectionT& section);

            private:

                //! Destructor.
                virtual ~FiberListManager() override = default;

            public:

                //! Fetch the god of dof object associated with \a unique_id unique identifier.
                //! \unique_id_param_in_accessor{fiber_list_type}
                const fiber_list_type& GetFiberList(unsigned int unique_id) const;

                //! Fetch the god of dof object associated with \a unique_id unique identifier.
                //! \unique_id_param_in_accessor{fiber_list_type}
                fiber_list_type& GetNonCstFiberList(unsigned int unique_id);

                //! Access to the storage.
                const auto& GetStorage() const noexcept;

            private:


                /*!
                 * \brief Method that cconstruct a new \a FiberList and store it into the class.
                 *
                 * \param[in] unique_id Unique identifier of the \a FiberList.
                 * \param[in] fiber_file File from which the fiber data are read.
                 * \param[in] domain \a Domain upon which the \a Parameter is defined.
                 * \param[in] felt_space \a FEltSpace in which the \a Parameter is defined.
                 * \param[in] unknown \a Unknown considered.
                 *
                 */
                void Create(const unsigned int unique_id,
                            const std::string& fiber_file,
                            const Domain& domain,
                            const FEltSpace& felt_space,
                            const Unknown& unknown);

                //! Time manager of the \a Model.                
                const TimeManager& GetTimeManager() const noexcept;


            private:


                //! \name Singleton requirements.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydetails doxygen_hide_time_manager_arg
                 */
                explicit FiberListManager(const TimeManager& time_manager);

                //! Friendship declaration to Singleton template class (to enable call to constructor).
                friend class Utilities::Singleton<FiberListManager>;
                ///@}


            private:

                //! Store the god of dof objects by their unique identifier.
                std::unordered_map<unsigned int, typename fiber_list_type::unique_ptr> list_;

                //! Time manager of the \a Model.
                const TimeManager& time_manager_;

            };


        } // namespace FiberNS


    } // namespace Internal


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/Fiber/Internal/FiberListManager.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_FIBER_LIST_MANAGER_HPP_
