/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Oct 2015 17:06:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HPP_

# include <memory>
# include <vector>
# include <string>
# include <algorithm>
# include <unordered_map>

# include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

# include "Parameters/Internal/ParameterInstance.hpp"
# include "Parameters/ParameterAtDof.hpp"

# include "ParameterInstances/Fiber/FiberList.hpp"
# include "ParameterInstances/Fiber/Internal/ReadFiberFile.hpp"
# include "ParameterInstances/Fiber/Internal/FillGlobalVector.hpp"
# include "ParameterInstances/Fiber/Internal/Traits.hpp"
# include "ParameterInstances/FromParameterAtDof/FromParameterAtDof.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GlobalVector;
    class Mesh;
    class FEltSpace;
    class Unknown;


    namespace Internal
    {


        namespace FiberNS
        {


            template<ParameterNS::Type TypeT>
            class FiberListManager;


        } // namespace FiberNS


    } // namespace Internal


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Read from an Ensight file values given at dofs and interpret then in term of a Parameter object.
     *
     * \attention It is assumed here a P1 mesh is considered!
     *
     * \tparam TypeT Whether a scalar or vectorial parameter is to be considered.
     */
    template<ParameterNS::Type TypeT>
    class FiberList final :
    public Crtp::UniqueId<FiberList<TypeT>, UniqueIdNS::AssignationMode::manual>,
    public Parameter<TypeT, QuadraturePoint, ParameterNS::TimeDependencyNS::None>
    {

    public:

        static_assert(TypeT != ParameterNS::Type::matrix,
                      "Current implementation can't deal with matrix parameters.");

        //! \copydoc doxygen_hide_alias_self
        using self = FiberList;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = QuadraturePoint;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to parent.
        using parent = Parameter<TypeT, QuadraturePoint, ParameterNS::TimeDependencyNS::None>;

        //! Alias to underlying parameter.
        using parameter_type = ParameterAtQuadraturePoint<TypeT, ParameterNS::TimeDependencyNS::None>;

        //! 'Inherit' alias from parent.
        using return_type = typename parent::return_type;

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend Internal::FiberNS::FiberListManager<TypeT>;
        //! Friendship to FiberListManager, only class able to build a FiberList object.
        // Excluded as Doxygen doesn't handle it well.
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Return the name of the class.
        static const std::string& ClassName();

    private:

        //! Alias to unique id parent.
        using unique_id_parent = Crtp::UniqueId<self, UniqueIdNS::AssignationMode::manual>;


    private:

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unique_id Unique id of the FilberList as read in the input file.
         * \param[in] fiber_file File at the Ensight format which will be interpreted. Its format is specified by
         * Ensight specifications;  typically scalar files get a 'scl' extension and their first line is 'Scalar per node'
         * whereas vectorial get a 'vct' extension and first line is 'Vector per node'.
         * \param[in] domain \a Domain considered for the parameters; in peculiar vertices to consider will
         * be taken from there.
         * \param[in] felt_space Finite element space upon which the Parameter should be defined.
         * \param[in] unknown A scalar or vectorial unknown that acts a bit as a strawman: dofs are defined only
         * in relationship to an unknown, so you must create one if none fulfill your purposes (for instance
         * if you deal with a vectorial unknown and need a scalar Dof field, you must create another unknown
         * only for the Parameter. To save space, it's better if this unknown is in its own numbering subset,
         * but this is not mandatory. Unknown should be scalar for TypeT == ParameterNS::Type::scalar and vectorial
         * for TypeT == ParameterNS::Type::vectorial.
         * \copydetails doxygen_hide_time_manager_arg
         *
         */
        explicit FiberList(unsigned int unique_id,
                           const std::string& fiber_file,
                           const Domain& domain,
                           const FEltSpace& felt_space,
                           const TimeManager& time_manager,
                           const Unknown& unknown);

    public:

        //! Destructor.
        ~FiberList() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        FiberList(const FiberList& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        FiberList(FiberList&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        FiberList& operator=(const FiberList& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        FiberList& operator=(FiberList&& rhs) = delete;

        ///@}

    public:

        /*!
         * \brief Initialize by converting into a \a ParameterAtQuadraturePoint.
         *
         * \param[in] quadrature_rule_per_topology If specified, the quadrature rule to use for each topology. If nullptr,
         * just take the choices stored in the \a FEltSpace.
         */
        void Initialize(const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);

        //! Whether the parameter is constant spatially (False for this one).
        bool IsConstant() const noexcept override final;

        //! Accessor to the global vector which entails the values at dof.
        const GlobalVector& GetGlobalVector() const noexcept;

        /*!
         * \brief Enables to modify the constant value of a parameter. Deactivated for this kind of \a Parameter.
         */
        void SetConstantValue(double ) override;

    private:

        //! \copydoc doxygen_hide_parameter_suppl_get_constant_value
        virtual return_type SupplGetConstantValue() const override final;

        //! \copydoc doxygen_hide_parameter_suppl_get_value_quad_pt
        virtual return_type SupplGetValue(const local_coords_type& quad_pt, const GeometricElt& geom_elt) const override final;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        virtual return_type SupplGetAnyValue() const override final;

        //! Write the content of the Parameter in a stream.
        //! \copydoc doxygen_hide_stream_inout
        virtual void SupplWrite(std::ostream& stream) const override final;

        //! \copydoc doxygen_hide_parameter_suppl_time_update
        virtual void SupplTimeUpdate() override final;

        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        virtual void SupplTimeUpdate(double time) override final;


    private:

        //! Non constant accessor to the global vector which entails the values at dof.
        GlobalVector& GetNonCstGlobalVector() noexcept;

        //! Accessor to the finite element space.
        const FEltSpace& GetFeltSpace() const noexcept;

        //! Accessor to the time manager.
        const TimeManager& GetTimeManager() const noexcept;

        //! Accessor to the unknown.
        const Unknown& GetUnknown() const noexcept;

        //! Accesspor to underlying parameter.
        const parameter_type& GetUnderlyingParameter() const noexcept;


    private:

        //! Finite element space to define the Parameter at Dof.
        const FEltSpace& felt_space_;

        //! Time manager
        const TimeManager& time_manager_;

        //! Unknown.
        const Unknown& unknown_;

        //! Global vector which entails the values at dof.
        GlobalVector::unique_ptr global_vector_;

        //! Parameter upon which most functionalities will be built.
        typename parameter_type::unique_ptr underlying_parameter_;
    };


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/Fiber/FiberList.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_HPP_
