/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 13 Oct 2016 16:28:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_FROM_PARAMETER_AT_DOF_HXX_
# define MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_FROM_PARAMETER_AT_DOF_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {


        template
        <
            ParameterNS::Type TypeT,
            template<ParameterNS::Type> class TimeDependencyT,
            unsigned int NfeltSpaceT
        >
        template<class StringT>
        typename ParameterAtQuadraturePoint<TypeT, TimeDependencyT>::unique_ptr
        FromParameterAtDof<TypeT, TimeDependencyT, NfeltSpaceT>
        ::Perform(StringT&& name,
                  const Domain& domain,
                  const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                  const TimeManager& time_manager,
                  const ParameterAtDof<TypeT, TimeDependencyT, NfeltSpaceT>& param_at_dof)
        {
            decltype(auto) felt_space_storage = param_at_dof.GetFEltSpaceStorage(); // current class is a friend of \a AtDof policy.

            decltype(auto) mesh = domain.GetMesh();
            const auto mesh_dimension = mesh.GetDimension();

            auto ret = std::make_unique<ParameterAtQuadraturePoint<TypeT, TimeDependencyT>>(name,
                                                                                            domain,
                                                                                            *quadrature_rule_per_topology,
                                                                                            Traits<TypeT>::AllocateDefaultValue(mesh_dimension, mesh_dimension),
                                                                                            time_manager);

            // For all \a FEltSpaces considered in the \a ParameterAtDof, report the values in the
            // \a ParameterAtQuadraturePoint. It is not straighforward to read as contrary to my habit, there
            // is an underlying knownledge of the internals of AtDof policy here (because exposing it would take more
            // time and would likely not be used elsewhere).
            for (auto i = 0u; i < NfeltSpaceT; ++i)
            {
                decltype(auto) felt_space = felt_space_storage.GetFEltSpace(mesh_dimension - i);

                Internal::GlobalParameterOperatorNS::FromAtDofToAtQuadPt<TypeT, TimeDependencyT, NfeltSpaceT>
                conversion_operator(felt_space,
                                    param_at_dof,
                                    quadrature_rule_per_topology,
                                    *ret);

                conversion_operator.Update();
            }


            // Also transmit the time dependency.
            // In C++ 17 if constexpr would help... but not yet there: we have to resort to cumbersome template
            // specialization.

            Internal::ParameterNS::CopyTimeDependency<TypeT, TimeDependencyT, NfeltSpaceT>
            ::Perform(param_at_dof, *ret);

            return ret;
        }


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FROM_PARAMETER_AT_DOF_x_FROM_PARAMETER_AT_DOF_HXX_
