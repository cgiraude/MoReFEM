/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_SCALAR_PARAMETER_FROM_FILE_x_SCALAR_PARAMETER_FROM_FILE_HPP_
# define MOREFEM_x_PARAMETER_INSTANCES_x_SCALAR_PARAMETER_FROM_FILE_x_SCALAR_PARAMETER_FROM_FILE_HPP_

# include <memory>
# include <fstream>
# include <array>
# include <cassert>
# include <functional>


# include "Parameters/ParameterType.hpp"

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/Mesh/Mesh.hpp"

# include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

# include "Parameters/Internal/ParameterInstance.hpp"
# include "Parameters/Policy/Constant/Constant.hpp"
# include "Parameters/TimeDependency/TimeDependency.hpp"


namespace MoReFEM
{


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Forward declarations.
    // ============================


    class GeometricElt;
    class TimeManager;


    // ============================
    // End of forward declarations.
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    /*!
     * \brief Define a spatially constant \a Parameter that varies with a time dependency given from a file.
     *
     *
     * A ParameterFromFile is first and foremost a time-dependant data: its main purpose is to describe the value
     * of a physical parameter over time. It is assumed that is is constant over space.
     *
     * \internal By adding few template parameters and substituting functions such as InitScalarParameterFromInputData,
     * it would be easy to extend present class to encompass much more cases. It is not yet done as the need has not
     * yet arisen. Anyway, current class is purely cosmetic: one can always create its own Parameter with time dependency
     * type properly given and then use afterwards call SetTimeDependency(); present class is just there to provide
     * a slicker interface to a very specific case.
     * \endinternal
     *
     */
    class ScalarParameterFromFile final
    {
    public:

        //! \copydoc doxygen_hide_alias_self
        using self = ScalarParameterFromFile;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to unique pointer to const object.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to array of unique pointer.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;

        //! Alias to traits.
        using traits = ParameterNS::Traits<ParameterNS::Type::scalar>;

        //! Alias to return type.
        using return_type = typename traits::return_type;

        //! Alias to the parent
        using parent = Parameter<ParameterNS::Type::scalar, LocalCoords, ParameterNS::TimeDependencyFromFile>;

        //! Alias to internal parameter.
        using internal_param_type =
            Parameter
            <
                ParameterNS::Type::scalar,
                LocalCoords,
                ParameterNS::TimeDependencyFromFile
            >;

    public:

        /// \name Special members.
        ///@{


        /*!
         * \brief Constructor.
         *
         * \param[in] name Name that will appear in outputs.
         * \copydoc doxygen_hide_parameter_domain_arg
         * \param[in] file File to create the parameter.
         * \param[in] time_manager \a TimeManager of the model.
         *
         * \tparam T Type of name, in forwarding reference idiom. It must be convertible to a std::string.
         *
         * \copydoc doxygen_hide_param_time_dependancy
         *
         */
        template<class T>
        explicit ScalarParameterFromFile(T&& name,
                                         const Domain& domain,
                                         const TimeManager& time_manager,
                                         const std::string& file);


    public:

        //! Destructor.
        ~ScalarParameterFromFile();

        //! \copydoc doxygen_hide_copy_constructor
        ScalarParameterFromFile(const ScalarParameterFromFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ScalarParameterFromFile(ScalarParameterFromFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ScalarParameterFromFile& operator=(const ScalarParameterFromFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ScalarParameterFromFile& operator=(ScalarParameterFromFile&& rhs) = delete;

        ///@}


    public:

        //! Apply the time dependency if relevant.
        void TimeUpdate();

        /*!
         * \brief Apply the time dependency if relevant.
         *
         * One should prefer to use the default one if one wants to use the current time.
         * Extra security to verify the synchro of the parameter to the current time is done in he default one.
         * This method is for particular cases only when the user knows exactly what is he doing.
         *
         * \param[in] time Time for the update.
         */
        void TimeUpdate(double time);


        /*!
         * \brief Returns the constant value (if the parameter is constant).
         *
         * If not constant, an assert is raised (in debug mode).
         *
         * \return Constant value of the parameter.
         */
        return_type GetConstantValue() const;

        //! Whether the parameter varies spatially or not.
        bool IsConstant() const;

        /*!
         * \brief Write the content of the Parameter in a stream.
         *
         * In first draft the output format is up to the policy (maybe later we may prefer to write at all quadrature
         * points for all cases); the exact content is indeed defined in the virtual method SupplWrite(), to be defined
         * in each inherited classes.
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void Write(std::ostream& stream) const;

        /*!
         * \brief Write the content of the Parameter in a file.
         *
         * This method calls the namesake method that writes on a stream.
         *
         * \param[in] filename Path to the file in which value will be written. The path must be valid (all directories
         * must exist) and if a namesake already exists it is overwritten.
         */
        void Write(const std::string& filename) const;

        //! Returns the \a Domain upon which the parameter is defined.
        const Domain& GetDomain() const noexcept;

        //! Whether the class is time-dependent or not.
        bool IsTimeDependent() const noexcept;

        /*!
         * \brief Constant accessor to the object which handles if relevant the time dependancy (computation of
         * the time related factor, etc...).
         *
         * Shouldn't be called very often...
         *
         * \return Time dependency object.
         */
        const ParameterNS::TimeDependencyFromFile<ParameterNS::Type::scalar>& GetTimeDependency() const noexcept;

    private:

        //! Constant accessor to the internal parameter.
        const internal_param_type& GetInternalParameter() const noexcept;

        //! Non constant accessor to the internal parameter.
        internal_param_type& GetNonCstInternalParameter() noexcept;


    private:

        //! Internal parameter which varies in time.
        typename internal_param_type::unique_ptr internal_parameter_ = nullptr;
    };


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


# include "ParameterInstances/ScalarParameterFromFile/ScalarParameterFromFile.hxx"


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_SCALAR_PARAMETER_FROM_FILE_x_SCALAR_PARAMETER_FROM_FILE_HPP_
