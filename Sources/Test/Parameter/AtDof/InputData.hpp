/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_PARAMETER_x_AT_DOF_x_INPUT_DATA_HPP_
# define MOREFEM_x_TEST_x_PARAMETER_x_AT_DOF_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/MoReFEMData/MoReFEMData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"


namespace MoReFEM
{


    namespace TestAtDofNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            domain = 1,
            full_mesh = 2
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            index = 1
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            scalar = 1,
            vectorial = 2
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            scalar = 1,
            vectorial = 2
        };


        //! \copydoc doxygen_hide_fiber_enum
        enum class FiberIndex
        {
            fiber = 1,
            distance = 2
        };



        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::index)>,

            InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiber), ParameterNS::Type::vector>,
            InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::distance), ParameterNS::Type::scalar>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestAtDofNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_PARAMETER_x_AT_DOF_x_INPUT_DATA_HPP_
