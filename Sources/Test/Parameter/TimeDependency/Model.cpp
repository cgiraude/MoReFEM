/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 8 Oct 2015 11:49:17 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Utilities/Filesystem/File.hpp"

#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Parameters/ParameterType.hpp"

#include "Parameters/Policy/AtDof/AtDof.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Internal/ParameterInstance.hpp"
#include "Parameters/TimeDependency/TimeDependency.hpp"

#include "Test/Parameter/TimeDependency/Model.hpp"


namespace MoReFEM
{
    
    
    namespace TestParameterTimeDependencyNS
    {
        
        
        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {
            decltype(auto) god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) morefem_data = parent::GetMoReFEMData();
            decltype(auto) input_data = morefem_data.GetInputData();
            
            output_file_ = parent::GetOutputDirectory().AddFile("test_values_"
            + std::to_string(parent::GetMpi().GetRank<int>()) + ".hhdata");
            
            if (FilesystemNS::File::DoExist(output_file_))
                FilesystemNS::File::Remove(output_file_, __FILE__, __LINE__);
            
            std::ofstream out;
            FilesystemNS::File::Create(out, output_file_, __FILE__, __LINE__);
            
            decltype(auto) domain =
                DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            using Solid = InputDataNS::Solid;
            young_modulus_ =
                InitScalarParameterFromInputData
                <
                    Solid::YoungModulus,
                    ParameterNS::TimeDependencyFunctor
                >("Young modulus",
                  domain,
                  input_data);
            
            
            {
                auto linear = [](double time)
                {
                    return time;
                };
                
                auto time_dep = std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::scalar>>(GetTimeManager(),
                                                                                                         std::move(linear));

                young_modulus_->SetTimeDependency(std::move(time_dep));
            }
            
            
            poisson_ratio_ =
                InitScalarParameterFromInputData
                <
                    Solid::YoungModulus,
                    ParameterNS::TimeDependencyNS::None
                >("Poisson ratio",
                  domain,
                  input_data);
            
            static_pressure_ =
                InitScalarParameterFromInputData<InputDataNS::Source::StaticPressure, ParameterNS::TimeDependencyNS::None>("StaticPressure",
                                                                                                                              domain,
                                                                                                                              input_data);
            
            namespace IPL = Utilities::InputDataNS;
            
            const std::string file =
                IPL::Extract<InputDataNS::Source::PressureFromFile<EnumUnderlyingType(PressureIndex::pressure)>::FilePath>::Path(input_data);
            
            dynamic_pressure_ = std::make_unique<ScalarParameterFromFile>("DynamicPressure",
                                                                          domain,
                                                                          GetTimeManager(),
                                                                          file);
           
                        
            using parameter_type = InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::source)>;
            
            source_parameter_ =
                InitThreeDimensionalParameterFromInputData
                <
                    parameter_type,
                    ParameterNS::TimeDependencyFunctor
                >("Source",
                  domain,
                  input_data);
            
            assert(!(!source_parameter_));
            
            {
                auto linear = [](double time)
                {
                    return time;
                };
                
                auto time_dep = std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::vector>>(GetTimeManager(),
                                                                                                         std::move(linear));
                
                GetNonCstSourceParameter().SetTimeDependency(std::move(time_dep));
            }
            
            using operator_type = decltype(source_operator_)::element_type;
            
            decltype(auto) felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::index));
            decltype(auto) unknown_ptr = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknownPtr(EnumUnderlyingType(UnknownIndex::scalar));
            
            source_operator_ =
                std::make_unique<operator_type>(felt_space,
                                                unknown_ptr,
                                                GetNonCstSourceParameter());
        }
    

        void Model::Forward()
        {
            std::ofstream out;
            
            FilesystemNS::File::Append(out, output_file_, __FILE__, __LINE__);
            
            decltype(auto) time_manager = GetTimeManager();
            
            out << "Time iteration = " << time_manager.NtimeModified() << "\t(" << time_manager.GetTime() << ')' << std::endl;
            out << "\tTime varying scalar= " << GetYoungModulus().GetConstantValue() << std::endl;
            out << "\tTime constant scalar = " << GetPoissonRatio().GetConstantValue() << std::endl;
            out << "\tTime varying transient source  = " << GetSourceParameter().GetConstantValue() << std::endl;
            
            std::cout << "Static Pressure " << GetStaticPressure().GetConstantValue() << std::endl;
            std::cout << "Dynamic Pressure " << GetDynamicPressure().GetConstantValue() << std::endl;
        }

        
        void Model::SupplInitializeStep()
        {
            GetNonCstYoungModulus().TimeUpdate();
            GetNonCstSourceParameter().TimeUpdate();
            GetNonCstDynamicPressure().TimeUpdate();
            
            const auto& time_manager = GetTimeManager();
            const double time = time_manager.GetTime();
            const double time_step = time_manager.GetTimeStep();
            
            GetNonCstDynamicPressure().TimeUpdate(time - 0.5 * time_step);
        }
        

        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        {
            std::cout << output_file_ << " has been written." << std::endl;
        }


    } // namespace TestParameterTimeDependencyNS


} // namespace MoReFEM
