/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_INPUT_DATA_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/MoReFEMData/MoReFEMData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/Geometry/PseudoNormals.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
# include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
# include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
# include "Core/InputData/Instances/Solver/Petsc.hpp"
# include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"

# include "Test/Operators/VariationalInstances/NonlinearMembrane/Enum.hpp"


namespace MoReFEM
{


    namespace TestNS::NonLinearMembraneOperatorNS
    {


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            displacementP1 = 1,
            displacementP2 = 2
        };

        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            displacementP1 = 1,
            displacementP2 = 2
        };

        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };

        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            full_mesh = 1,
            surface = 2,
            force = 3,
            dirichlet = 4,
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            surface = 1,
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList
        {
            surfacic = 1
        };

        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacementP1)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacementP2)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacementP1)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacementP2)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::surface)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::force)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>,

            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::PoissonRatio,

            InputDataNS::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace CardiacMechanicsPrestressNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_INPUT_DATA_HPP_
