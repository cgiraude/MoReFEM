add_library(MoReFEMTestNonLinearMembrane_lib ${LIBRARY_TYPE} "")

target_sources(MoReFEMTestNonLinearMembrane_lib
    PRIVATE
        "${CMAKE_CURRENT_LIST_DIR}/Model.cpp"
        "${CMAKE_CURRENT_LIST_DIR}/TestP1P1.cpp"
        "${CMAKE_CURRENT_LIST_DIR}/ExpectedResults_P1P1.cpp"
	PRIVATE
		"${CMAKE_CURRENT_LIST_DIR}/Model.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/Model.hxx"
		"${CMAKE_CURRENT_LIST_DIR}/InputData.hpp"
		"${CMAKE_CURRENT_LIST_DIR}/ExpectedResults.hpp"
)

target_link_libraries(MoReFEMTestNonLinearMembrane_lib
                      ${MOREFEM_TEST_TOOLS})


add_executable(MoReFEMTestNonLinearMembrane ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEMTestNonLinearMembrane
                      MoReFEMTestNonLinearMembrane_lib)

add_test(NonLinearMembrane MoReFEMTestNonLinearMembrane
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(NonLinearMembrane PROPERTIES TIMEOUT 20)
