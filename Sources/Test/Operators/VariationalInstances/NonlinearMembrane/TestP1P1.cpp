/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hpp"

#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/ExpectedResults.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::NonLinearMembraneOperatorNS
    {


        namespace // anonymous
        {


            using ::MoReFEM::Internal::assemble_into_matrix;

            using ::MoReFEM::Internal::assemble_into_vector;

            const double thickness_value = 4.2375;

            const double pretension_value = 17.984152;


        } // namespace anonymous


        void Model::TestP1P1(pretension is_pretension,
                             assemble_into_matrix do_assemble_into_matrix,
                             assemble_into_vector do_assemble_into_vector) const
        {
            const auto& god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));

            decltype(auto) morefem_data = parent::GetMoReFEMData();
            
            const auto& displacement_numbering_subset =
            god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::displacementP1));

            const auto& felt_space_surface = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::surface));

            decltype(auto) domain_full_mesh =
            DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(EnumUnderlyingType(DomainIndex::full_mesh), __FILE__, __LINE__);

            Solid solid(morefem_data.GetInputData(),
                        domain_full_mesh,
                        felt_space_surface.GetQuadratureRulePerTopology());

            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& displacement_ptr =
                unknown_manager.GetUnknownPtr(EnumUnderlyingType(UnknownIndex::displacementP1));

            namespace GVO = GlobalVariationalOperatorNS;

            namespace IPL = Utilities::InputDataNS;

            using scalar_parameter_type = Internal::ParameterNS::ParameterInstance
            <
                ParameterNS::Type::scalar,
                ::MoReFEM::ParameterNS::Policy::Constant,
                ParameterNS::TimeDependencyNS::None
            >;

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            decltype(auto) domain_surface =
            domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::surface), __FILE__, __LINE__);

            scalar_parameter_type thickness("Thickness",
                                            domain_surface,
                                            thickness_value);

            const double pretension_to_use
                = is_pretension == pretension::yes ? pretension_value : 0.;

            scalar_parameter_type pretension_param("Pretension",
                                                   domain_surface,
                                                   pretension_to_use);

            GVO::NonlinearMembrane stiffness_operator(felt_space_surface,
                                                      displacement_ptr,
                                                      displacement_ptr,
                                                      solid.GetYoungModulus(),
                                                      solid.GetPoissonRatio(),
                                                      thickness,
                                                      pretension_param);

            GlobalMatrix matrix_tangent_stiffness(displacement_numbering_subset, displacement_numbering_subset);
            AllocateGlobalMatrix(god_of_dof, matrix_tangent_stiffness);

            GlobalVector vector_stiffness_residual(displacement_numbering_subset);
            AllocateGlobalVector(god_of_dof, vector_stiffness_residual);

            GlobalVector previous_iteration(vector_stiffness_residual);

            {
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write>
                    content(previous_iteration,
                            __FILE__, __LINE__);

                content[0] = 30.;
                content[1] = -42.;
                content[2] = -17.;
                content[3] = 10.;
                content[4] = 97.;
                content[5] = 41.;
                content[6] = 5.;
                content[7] = -84.;
                content[8] = -20.5;
            }

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            if (do_assemble_into_matrix == assemble_into_matrix::yes
                && do_assemble_into_vector == assemble_into_vector::yes)
            {
                stiffness_operator.Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            previous_iteration);

                CheckMatrix(god_of_dof,
                            matrix_tangent_stiffness,
                            GetExpectedMatrixP1P1(is_pretension),
                            __FILE__, __LINE__,
                            1.);

                CheckVector(god_of_dof,
                            vector_stiffness_residual,
                            GetExpectedVectorP1P1(is_pretension),
                            __FILE__, __LINE__,
                            1.);
            }
            else if (do_assemble_into_matrix == assemble_into_matrix::yes
                     && do_assemble_into_vector == assemble_into_vector::no)
            {
                stiffness_operator.Assemble(std::make_tuple(std::ref(mat)),
                                            previous_iteration);

                CheckMatrix(god_of_dof,
                            matrix_tangent_stiffness,
                            GetExpectedMatrixP1P1(is_pretension),
                            __FILE__, __LINE__,
                            1.);
            }
            else if (do_assemble_into_matrix == assemble_into_matrix::no
                     && do_assemble_into_vector == assemble_into_vector::yes)
            {
                stiffness_operator.Assemble(std::make_tuple(std::ref(vec)),
                                            previous_iteration);

                CheckVector(god_of_dof,
                            vector_stiffness_residual,
                            GetExpectedVectorP1P1(is_pretension),
                            __FILE__, __LINE__,
                            1.);
            }
        }

        
    } // namespace TestNS::NonLinearMembraneOperatorNS


} // namespace MoReFEM
