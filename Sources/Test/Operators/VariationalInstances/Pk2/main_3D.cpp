/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE second_piola_kirchhoff_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Operators/VariationalInstances/Pk2/Model.hpp"
#include "Test/Operators/VariationalInstances/Pk2/InputData.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
        TestNS::FixtureNS::Model
        <
            TestNS::Pk2::Model,
            LuaFile
        >;


    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)

    BOOST_AUTO_TEST_CASE(matrix_only)
    {
        GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::no);
    }


    BOOST_AUTO_TEST_CASE(vector_only)
    {
        GetModel().SameUnknown(assemble_into_matrix::no, assemble_into_vector::yes);
    }


    BOOST_AUTO_TEST_CASE(matrix_and_vector)
    {
        GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::yes);
    }

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(same_unknown_triangles_inverted, fixture_type)

    BOOST_AUTO_TEST_CASE(matrix_only)
    {
        GetModel().SameUnknownInverted(assemble_into_matrix::yes, assemble_into_vector::no);
    }


    BOOST_AUTO_TEST_CASE(vector_only)
    {
        GetModel().SameUnknownInverted(assemble_into_matrix::no, assemble_into_vector::yes);
    }


    BOOST_AUTO_TEST_CASE(matrix_and_vector)
    {
        GetModel().SameUnknownInverted(assemble_into_matrix::yes, assemble_into_vector::yes);
    }

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(different_unknown, fixture_type)

    BOOST_AUTO_TEST_CASE(unknown_p1_test_p1)
    {
        GetModel().UnknownP1TestP1();
    }

    BOOST_AUTO_TEST_CASE(unknown_p2_test_p1)
    {
        GetModel().UnknownP2TestP1();
    }


BOOST_AUTO_TEST_SUITE_END()


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/"
                                                       "VariationalInstances/Pk2/"
                                                       "demo_input_parameter_3D.lua");
    }


} // namespace anonymous

