-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {



	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.,

	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,


	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 0.

} -- transient

NumberingSubset1 = {

	-- Name of the numbering subset (not really used; at the moment I just need one input parameter to ground 
	-- the possible values to choose elsewhere). 
	-- Expected format: "VALUE"
    name = "monolithic",
    
    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset1

Unknown1 = {

	-- Name of the unknown (used for displays in output).
	-- Expected format: "VALUE"
	name = "displacement",

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'scalar', 'vectorial'})
	nature = "vectorial",

} -- Unknown1

Mesh1 = {

	-- Path of the mesh file to use.
	-- Expected format: "VALUE"
	mesh = "${MOREFEM_ROOT}/Data/Mesh/rivlin_cube_one_hexa_edges.mesh",

	-- Format of the input mesh.
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Ensight', 'Medit'})
	format = "Medit",

	-- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
	-- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
	-- exception is thrown. 
	-- Expected format: VALUE
	-- Constraint: v <= 3 and v > 0
	dimension = 3,
    
    -- Space unit of the mesh.
    -- Expected format: VALUE
    space_unit = 1.

} -- Mesh1

Domain1 = {

	-- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be 
	-- left empty if domain not limited to one mesh; at most one value is expected here. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: {VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 3 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after 
	-- generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built.
 	-- The known types when this file was generated are: 
 	-- . Point1
 	-- . Segment2, Segment3
 	-- . Triangle3, Triangle6
 	-- . Quadrangle4, Quadrangle8, Quadrangle9
 	-- . Tetrahedron4, Tetrahedron10
 	-- . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }

} -- Domain1

Domain2 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 1 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain2

Domain3 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 2 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain3

Domain4 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 3 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain4

Domain5 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 4 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain5

Domain6 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 5 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain6

Domain7 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 6 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { "Quadrangle4" }
    
} -- Domain7


Domain8 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain8


Domain9 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 2 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain9

Domain10 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 1 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 233 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain10

Domain11 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 1 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 201 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain11

Domain12 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 1 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 204 },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at LuaOptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain12



EssentialBoundaryCondition1 = {

	-- Name of the boundary condition (must be unique).
	-- Expected format: "VALUE"
	name = "edge1",

	-- Comp1, Comp2 or Comp3
	-- Expected format: "VALUE"
	-- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
	component = 'Comp1',

    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'displacement',

    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = { 0. },

    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 10,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false

} -- EssentialBoundaryCondition1

EssentialBoundaryCondition2 = {

    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = "edge2",

    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp2',

    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'displacement',

    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = { 0. },

    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 11,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false

} -- EssentialBoundaryCondition2

EssentialBoundaryCondition3 = {
    
    -- Name of the boundary condition (must be unique).
    -- Expected format: "VALUE"
    name = "edge3",
    
    -- Comp1, Comp2 or Comp3
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})
    component = 'Comp3',
    
    -- Name of the unknown addressed by the boundary condition.
    -- Expected format: "VALUE"
    unknown = 'displacement',
    
    -- Values at each of the relevant component.
    -- Expected format: {VALUE1, VALUE2, ...}
    value = { 0. },
    
    -- Index of the domain onto which essential boundary condition is defined.
    -- Expected format: VALUE
    domain_index = 12,
    
    -- Whether the values of the boundary condition may vary over time.
    -- Expected format: 'true' or 'false' (without the quote)
    is_mutable = false,
    
    -- Whether a dof of this boundary condition may also belong to another one. This highlights an ill-defined
    -- model in most cases, but I nonetheless need it for FSI/ALE.
    -- Expected format: 'true' or 'false' (without the quote)
    may_overlap = false
    
} -- EssentialBoundaryCondition3

-- Face1
TransientSource1 = {
    
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = {"constant", "constant", "constant" },
    
    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
    -- value_x = -bouss(1,1),
    -- value_y = -bouss(2,1),
    -- value_z = -bouss(3,1),
    
    value = {-4.672317, -1.059389, -0.000000}
    
} -- TransientSource1

-- Face2
TransientSource2 = {

    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = {"constant", "constant", "constant" },
    
    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
    -- value_x = -bouss(1,2),
    -- value_y = -bouss(2,2),
    -- value_z = -bouss(3,2),
    
    value = {-1.046259, 4.473888, 0.000000}
        
} -- TransientSource2

-- Face3
TransientSource3 = {
    
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = {"constant", "constant", "constant" },
    
    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
     -- value_x = -bouss(1,3),
     -- value_y = -bouss(2,3),
     -- value_z = -bouss(3,3)
     value = {-0.000000, 0.000000, -0.507083}
    
} -- TransientSource3

-- Face4
TransientSource4 = {
    
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = {"constant", "constant", "constant" },
    
    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
    -- value_x = bouss(1,1),
    -- value_y = bouss(2,1),
    -- value_z = bouss(3,1),
    value = {4.672317, 1.059389, 0.000000}
    
} -- TransientSource4

-- Face5
TransientSource5 = {
    
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = {"constant", "constant", "constant" },
    
    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
    -- value_x = bouss(1,2),
    -- value_y = bouss(2,2),
    -- value_z = bouss(3,2),
    value = {1.046259, -4.473888, -0.000000}

    
    
} -- TransientSource5

-- Face6
TransientSource6 = {
    
    -- How is given the transient source value (as a constant, as a Lua function, per quadrature point, etc...)
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function'})
    nature = {"constant", "constant", "constant" },
    
    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
    -- value_x = bouss(1,3),
    -- value_y = bouss(2,3),
    -- value_z = bouss(3,3),
    
    value = {0.000000, -0.000000, 0.507083}
       
} -- TransientSource6

FiniteElementSpace1 = {

	-- Index of the god of dof into which the finite element space is defined.
	-- Expected format: VALUE
	god_of_dof_index = 1,

	-- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
	-- Expected format: VALUE
	domain_index = 1,

	-- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as 
	-- an 'Unknown' block; expected name/identifier is the name given there. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	unknown_list = {"displacement"},

	-- List of the shape function to use for each unknown;
	-- Expected format: {"VALUE1", "VALUE2", ...}
	shape_function_list = {"Q1"},

	-- List of the numbering subset to use for each unknown;
	-- Expected format: {VALUE1, VALUE2, ...}
	numbering_subset_list = { 1 },

} -- FiniteElementSpace1

--face1
FiniteElementSpace2 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 2,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"displacement"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace2

--face2
FiniteElementSpace3 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 3,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"displacement"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace3

--face3
FiniteElementSpace4 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 4,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"displacement"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace4

--face4
FiniteElementSpace5 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 5,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"displacement"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace5

--face5
FiniteElementSpace6 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 6,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"displacement"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace6

--face6
FiniteElementSpace7 = {
    
    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,
    
    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 7,
    
    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = {"displacement"},
    
    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = {"Q1"},
    
    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1 },
    
} -- FiniteElementSpace7

-- Petsc
Petsc1 = {
    -- Absolute tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    absoluteTolerance = 1e-10,
    
    -- gmresStart
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v >= 0
    gmresRestart = 200,
    
    -- Maximum iteration
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0
    maxIteration = 1000,
    
    -- List of preconditioner: { none jacobi sor lu bjacobi ilu asm cholesky }.
    -- To use mumps: preconditioner = lu
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, 'lu')
    preconditioner = 'lu',
    
    -- Relative tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    relativeTolerance = 1e-10,
    
    -- Step size tolerance
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: v > 0.
    stepSizeTolerance = 1e-8,
    
    -- List of solver: { chebychev cg gmres preonly bicg python };
    -- To use Mumps choose preonly.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    -- Constraint: value_in(v, {'Mumps', 'Umfpack'})
    solver = 'Mumps'
}

Solid = {
    
    VolumicMass = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 1.
        
    }, -- VolumicMass
    
    HyperelasticBulk = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 170
        
    }, -- HyperelasticBulk
    
    Kappa1 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 10
        
        
    }, -- Kappa1
    
    Kappa2 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
        -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','at_quadrature_point','piecewise_constant_by_domain'})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        --  If nature is 'constant', expected format is VALUE
        --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 0.
        
        
    }, -- Kappa2


	-- If the displacement induced by the applied loading is strong enough it can lead to inverted elements:
    -- some vertices of the element are moved such that the volume of the finite element becomes negative.
    -- This means that the resulting deformed mesh is no longer valid.
    -- This parameter enables a computationally expensive test on all quadrature points to check that the volume
    -- of all finite elements remains positive throughout the computation.
    CheckInvertedElements = false
    
    
} -- Solid

Fiber_vector_1 = {
    
    -- Path to Ensight file.
    -- Expected format: "VALUE"
    ensight_file = "${MOREFEM_ROOT}/Data/Fiber/Fibers1MoReFEM_Rivlin_cube.vct",
    
    -- Index of the geometric mesh region upon which parameter is defined.
    -- Expected format: VALUE
    domain_index = 1,
    
    -- Index of the finite element space upon which parameter is defined.
    -- Expected format: VALUE
    felt_space_index = 1,
    
    -- Name of the unknown used to describe the dofs. Might be a fictitious one (see documentation for more
    -- details).
    -- Expected format: "VALUE"
    unknown = 'displacement'
    
} -- Fiber1


Fiber_vector_2 = {
    
    -- Path to Ensight file.
    -- Expected format: "VALUE"
    ensight_file = "${MOREFEM_ROOT}/Data/Fiber/Fibers2MoReFEM_Rivlin_cube.vct",
    
    -- Index of the geometric mesh region upon which parameter is defined.
    -- Expected format: VALUE
    domain_index = 1,
    
    -- Index of the finite element space upon which parameter is defined.
    -- Expected format: VALUE
    felt_space_index = 1,
    
    -- Name of the unknown used to describe the dofs. Might be a fictitious one (see documentation for more
    -- details).
    -- Expected format: "VALUE"
    unknown = 'displacement'
    
} -- Fiber2

Microsphere = {
    
    -- InPlaneFiberDispersionI4
    InPlaneFiberDispersionI4 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {"ignore", "constant", "lua_function", "piecewise_constant_by_domain"})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        -- If nature is 'constant', expected format is VALUE
        -- If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        -- If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 0.5,
        
    },
    
    -- OutOfPlaneFiberDispersionI4
    OutOfPlaneFiberDispersionI4 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {"ignore", "constant", "lua_function", "piecewise_constant_by_domain"})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        -- If nature is 'constant', expected format is VALUE
        -- If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        -- If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 0.5
        
    },
    
    
    -- FiberStiffnessDensityI4
    FiberStiffnessDensityI4 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {"ignore", "constant", "lua_function", "piecewise_constant_by_domain"})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        -- If nature is 'constant', expected format is VALUE
        -- If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        -- If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 0.1
        
    },
    
    -- InPlaneFiberDispersionI6
    InPlaneFiberDispersionI6 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {"ignore", "constant", "lua_function", "piecewise_constant_by_domain"})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        -- If nature is 'constant', expected format is VALUE
        -- If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        -- If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 0.5
        
    },
    
    -- OutOfPlaneFiberDispersionI6
    OutOfPlaneFiberDispersionI6 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {"ignore", "constant", "lua_function", "piecewise_constant_by_domain"})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        -- If nature is 'constant', expected format is VALUE
        -- If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        -- If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 0.5
        
    },
    
    
    -- FiberStiffnessDensityI6
    FiberStiffnessDensityI6 = {
        
        -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...)
        -- Expected format: "VALUE"
        -- Constraint: value_in(v, {"ignore", "constant", "lua_function", "piecewise_constant_by_domain"})
        nature = "constant",
        
        -- The value for the parameter, which type depends directly on the nature chosen:
        -- If nature is 'constant', expected format is VALUE
        -- If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
        -- If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
        -- return x + y - z
        -- end
        -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' preffix.
        value = 0.1
        
    },
}

Result = {

	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step.
	-- Expected format: "VALUE"
    output_directory = "${MOREFEM_RESULT_DIR}/Microsphere",
    
    -- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
    -- Expected format: VALUE
    display_value = 1,
    
    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: 'true' or 'false' (without the quote)
    binary_output = false
    
} -- Result
