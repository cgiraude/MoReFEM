//! \file
//
//
//  InputData.hpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MICROSPHERE_x_INPUT_DATA_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MICROSPHERE_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/MoReFEMData/MoReFEMData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/Geometry/PseudoNormals.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
# include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
# include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"
# include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
# include "Core/InputData/Instances/Solver/Petsc.hpp"
# include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
# include "Core/InputData/Instances/Geometry/Mesh.hpp"
# include "Core/InputData/Instances/Solver/Petsc.hpp"
# include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
# include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"




namespace MoReFEM
{


    namespace TestNS::Microsphere
    {


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            displacement = 1
        };

        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            displacement = 1,
        };

        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };

        //! \copydoc doxygen_hide_fiber_enum
        enum class FiberIndex
        {
            fiberI4 = 1,
            fiberI6 = 2
        };

        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            highest_dimension = 1,
            face1 = 2,
            face2 = 3,
            face3 = 4,
            face4 = 5,
            face5 = 6,
            face6 = 7,
            full_mesh = 8,
            all_faces = 9,
            edge1 = 10,
            edge2 = 11,
            edge3 = 12
        };

        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            edge1 = 1,
            edge2 = 2,
            edge3 = 3
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            highest_dimension = 1,
            face1 = 2,
            face2 = 3,
            face3 = 4,
            face4 = 5,
            face5 = 6,
            face6 = 7
        };

        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex

        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList
        {
            surfacic_force_face1 = 1,
            surfacic_force_face2 = 2,
            surfacic_force_face3 = 3,
            surfacic_force_face4 = 4,
            surfacic_force_face5 = 5,
            surfacic_force_face6 = 6
        };

        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
        InputDataNS::TimeManager,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face2)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face3)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face4)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face5)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face6)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::all_faces)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge1)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge2)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge3)>,

        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge1)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge2)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge3)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face1)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face2)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face3)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face4)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face5)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face6)>,

        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>,
        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face2)>,
        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face3)>,
        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face4)>,
        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face5)>,
        InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face6)>,

        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
		InputDataNS::Solid::CheckInvertedElements,

        InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiberI4), ParameterNS::Type::vector>,
        InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiberI6), ParameterNS::Type::vector>,

        InputDataNS::Microsphere::InPlaneFiberDispersionI4,
        InputDataNS::Microsphere::OutOfPlaneFiberDispersionI4,
        InputDataNS::Microsphere::FiberStiffnessDensityI4,
        InputDataNS::Microsphere::InPlaneFiberDispersionI6,
        InputDataNS::Microsphere::OutOfPlaneFiberDispersionI6,
        InputDataNS::Microsphere::FiberStiffnessDensityI6,

        InputDataNS::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::model>;


    } // namespace TestNS::Microsphere


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MICROSPHERE_x_INPUT_DATA_HPP_
