Geometry file
Geometry file
node id assign
element id assign
coordinates
       8
 0.00000e+00 0.00000e+00 0.00000e+00
 1.00000e+00 0.00000e+00 0.00000e+00
 1.00000e+00 1.00000e+00 0.00000e+00
 0.00000e+00 1.00000e+00 0.00000e+00
 0.00000e+00 0.00000e+00 1.00000e+00
 1.00000e+00 0.00000e+00 1.00000e+00
 1.00000e+00 1.00000e+00 1.00000e+00
 0.00000e+00 1.00000e+00 1.00000e+00
part       1
MeshLabel 1
quad4
       1
       5       1       4       8
part       2
MeshLabel 2
quad4
       1
       1       2       6       5
part       3
MeshLabel 3
quad4
       1
       1       2       3       4
part       4
MeshLabel 4
quad4
       1
       6       2       3       7
part       5
MeshLabel 5
quad4
       1
       8       7       3       4
part       6
MeshLabel 6
quad4
       1
       5       6       7       8
part       7
MeshLabel 30
hexa8
       1
       1       2       3       4       5       6       7       8
part       8
MeshLabel 201
bar2
       1
       1       2
part       9
MeshLabel 202
bar2
       1
       2       3
part      10
MeshLabel 203
bar2
       1
       3       4
part      11
MeshLabel 204
bar2
       1
       4       1
part      12
MeshLabel 211
bar2
       1
       5       6
part      13
MeshLabel 212
bar2
       1
       6       7
part      14
MeshLabel 213
bar2
       1
       7       8
part      15
MeshLabel 214
bar2
       1
       8       5
part      16
MeshLabel 231
bar2
       1
       8       4
part      17
MeshLabel 232
bar2
       1
       3       7
part      18
MeshLabel 233
bar2
       1
       5       1
part      19
MeshLabel 234
bar2
       1
       2       6
