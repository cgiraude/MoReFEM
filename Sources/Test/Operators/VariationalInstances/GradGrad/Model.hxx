/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_MODEL_HXX_
# define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_MODEL_HXX_


namespace MoReFEM
{


    namespace TestNS::GradGrad
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("Test GradGrad operator");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        { }


    } // namespace TestNS::GradGrad


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_GRAD_GRAD_x_MODEL_HXX_
