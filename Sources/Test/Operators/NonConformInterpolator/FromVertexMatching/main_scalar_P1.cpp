/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/



#include <cstdlib>

#define BOOST_TEST_MODULE from_vertex_matching
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/NonConformInterpolator/FromVertexMatching/Model.hpp"
#include "Test/Operators/NonConformInterpolator/FromVertexMatching/InputData.hpp"
#include "Test/Operators/NonConformInterpolator/FromVertexMatching/CheckInterpolator.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };

    using fixture_type =
        TestNS::FixtureNS::Model
        <
            TestNS::FromVertexMatchingNS::Model,
            LuaFile
        >;


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"



BOOST_FIXTURE_TEST_CASE(check_interpolator, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    mpi.Barrier();

    // Post processing is a sequential process...
    if (mpi.IsRootProcessor())
        CheckInterpolator(model);
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Operators/NonConformInterpolator/"
                                         "FromVertexMatching/demo_scalar_P1.lua");
    }


} // namespace anonymous

