/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 16:00:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "PostProcessing/PostProcessing.hpp"

#include "Test/Operators/NonConformInterpolator/FromVertexMatching/CheckInterpolator.hpp"
#include "Test/Operators/NonConformInterpolator/FromVertexMatching/Model.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::FromVertexMatchingNS
    {
        
        
        namespace // anonymous
        {
            
            using result_type = std::unordered_map<unsigned int, std::vector<unsigned int> >;
            
            
            result_type PerformComputationForNumberingSubset(const GodOfDof& god_of_dof,
                                                             const PostProcessingNS::PostProcessing& post_processing,
                                                             NumberingSubsetIndex numbering_subset);

            
        } // namespace anonymous
        

        void CheckInterpolator(const Model& model)
        {
            decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
            
            decltype(auto) solid_god_of_dof = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::solid));
            decltype(auto) fluid_god_of_dof = god_of_dof_manager.GetGodOfDof(EnumUnderlyingType(MeshIndex::fluid));
            
            using namespace TestNS::FromVertexMatchingNS;

            PostProcessingNS::PostProcessing fluid_post_processing(model.GetOutputDirectory(),
                                                                   { EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid) },
                                                                  fluid_god_of_dof.GetMesh());
            
            
            PostProcessingNS::PostProcessing solid_post_processing(model.GetOutputDirectory(),
                                                                   { EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid) },
                                                                   solid_god_of_dof.GetMesh());
            
            result_type unknown_on_fluid_content =
                PerformComputationForNumberingSubset(fluid_god_of_dof,
                                                     fluid_post_processing,
                                                     NumberingSubsetIndex::unknown_on_fluid);
            
            result_type unknown_on_solid_content =
                PerformComputationForNumberingSubset(solid_god_of_dof,
                                                     solid_post_processing,
                                                     NumberingSubsetIndex::unknown_on_solid);
            
            
            decltype(auto) interpolator = model.GetOperatorUnknownSolidToFluid();
            
            decltype(auto) pattern = interpolator.GetMatrixPattern();
            
            decltype(auto) icsr = pattern.GetICsr();
            decltype(auto) jcsr = pattern.GetJCsr();
            
            const auto Nicsr = icsr.size();
            const auto Njcsr = jcsr.size();
            
            if (Njcsr + 1ul != Nicsr)
                throw Exception("CSR format for unknown interpolator is incorrect: we expect exactly one value "
                                "per line.", __FILE__, __LINE__);
            
            #ifndef NDEBUG
            const auto end_vel_on_fluid = unknown_on_fluid_content.cend();
            const auto end_vel_on_solid = unknown_on_solid_content.cend();
            #endif // NDEBUG
            
            for (auto i = 0ul; i < Nicsr; ++i)
            {
                if (static_cast<std::size_t>(icsr[i]) != i)
                    throw Exception("CSR format for unknown interpolator is incorrect: we expect exactly one value "
                                    "per line.", __FILE__, __LINE__);
            }
            
            
            for (auto fluid_index = 0ul; fluid_index < Njcsr; ++fluid_index) // Njcsr is NOT a mistake!
            {
                assert(fluid_index < Njcsr);
                const auto solid_index = jcsr[fluid_index];
                
                const auto it_fluid = unknown_on_fluid_content.find(static_cast<unsigned int>(fluid_index));
                const auto it_solid = unknown_on_solid_content.find(static_cast<unsigned int>(solid_index));
                
                assert(it_fluid != end_vel_on_fluid);
                assert(it_solid != end_vel_on_solid);
                
                if (it_fluid->second != it_solid->second)
                {
                    std::ostringstream oconv;
                    oconv << "Error somewhere in interpolator implementation: interface ";
                    Utilities::PrintContainer<>::Do(it_fluid->second, oconv);
                    oconv << " on fluid mesh do not match interface ";
                    Utilities::PrintContainer<>::Do(it_fluid->second, oconv);
                    oconv << " on solid mesh.";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }
            }
            
        }
        
        
        namespace // anonymous
        {
            
 
            result_type PerformComputationForNumberingSubset(const GodOfDof& god_of_dof,
                                                             const PostProcessingNS::PostProcessing& post_processing,
                                                             NumberingSubsetIndex numbering_subset_index)
            {
                result_type ret;
                ret.max_load_factor(Utilities::DefaultMaxLoadFactor());
                
                decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(numbering_subset_index));
                
                ret.reserve(god_of_dof.NprogramWiseDof(numbering_subset));
                
                decltype(auto) mpi = god_of_dof.GetMpi();
                const auto Nprocessor = mpi.template Nprocessor<unsigned int>();

                for (auto rank = 0u; rank < Nprocessor; ++rank)
                {
                    decltype(auto) dof_list = post_processing.GetDofInformationList(EnumUnderlyingType(numbering_subset_index),
                                                                                    rank);

                    for (const auto& dof_ptr : dof_list)
                    {
                        assert(!(!dof_ptr));
                        const auto& dof = *dof_ptr;
                        
                        const auto dof_index = dof.GetProgramWiseIndex();
                        const auto& interface = dof.GetInterface();
                        
                        auto check = ret.insert(std::make_pair(dof_index, interface.GetVertexCoordsIndexList()));
                        assert(check.second);
                        static_cast<void>(check);
                    }
                }
                
                assert(ret.size() == god_of_dof.NprogramWiseDof(numbering_subset));
                
                return ret;
            }
            
            
        } // namespace anonymous

        
    } // namespace TestNS::FromVertexMatchingNS


} // namespace MoReFEM
