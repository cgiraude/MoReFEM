/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 17 Nov 2016 12:27:37 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Core/InputData/Instances/Result.hpp"

#include "Test/Operators/NonConformInterpolator/FromVertexMatching/Model.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::FromVertexMatchingNS
    {
        
        
        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }


        void Model::SupplInitialize()
        {
            using type = NonConformInterpolatorNS::FromVertexMatching;
            decltype(auto) morefem_data = parent::GetMoReFEMData();

            unknown_solid_2_fluid_ =
                std::make_unique<type>(morefem_data.GetInputData(),
                                       EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_solid),
                                       EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_fluid),
                                       NonConformInterpolatorNS::store_matrix_pattern::yes);
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        { }


    } // namespace TestNS::FromVertexMatchingNS


} // namespace MoReFEM
