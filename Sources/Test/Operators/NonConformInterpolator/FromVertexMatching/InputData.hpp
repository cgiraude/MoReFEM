/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_INPUT_DATA_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/MoReFEMData/MoReFEMData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"



namespace MoReFEM
{


    namespace TestNS::FromVertexMatchingNS
    {


        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            fluid = 1,
            solid = 2
        };


        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            fluid = 10,
            solid = 20
        };


        //! \copydoc doxygen_hide_felt_space_enum
        enum class FEltSpaceIndex
        {
            fluid = 10,

            solid = 20
        };


        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            unknown = 1,

            chaos = 3 // this unknown is just there to ensure the dof repartition is not the same for unknown and pressure.
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex : unsigned int
        {
            unknown_on_fluid = 10,

            unknown_on_solid = 20,
            chaos = 22,
        };


        //! Indexes for the vertex matching interpolator.
        enum class InitVertexMatchingInterpolator
        {
            unknown_on_fluid = 10,
            unknown_on_solid = 20,
        };




        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::chaos)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::chaos)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>,
            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::InterpolationFile,

            InputDataNS::InitVertexMatchingInterpolator<EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_solid)>,
            InputDataNS::InitVertexMatchingInterpolator<EnumUnderlyingType(InitVertexMatchingInterpolator::unknown_on_fluid)>,

            InputDataNS::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace TestNS::FromVertexMatchingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_VERTEX_MATCHING_x_INPUT_DATA_HPP_
