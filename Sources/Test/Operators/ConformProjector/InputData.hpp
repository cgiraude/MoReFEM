/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_CONFORM_PROJECTOR_x_INPUT_DATA_HPP_
# define MOREFEM_x_TEST_x_OPERATORS_x_CONFORM_PROJECTOR_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/MoReFEMData/MoReFEMData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"



namespace MoReFEM
{


    namespace ConformProjectorNS
    {


        //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
        {
            mesh = 1
        };


        //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
        {
            domain = 1
        };


        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            velocity_pressure = 1,
            velocity = 2,
            pressure = 3
        };


        //! \copydoc doxygen_hide_unknown_enum
            enum class UnknownIndex
        {
            velocity = 1,
            pressure = 2
        };


        //! \copydoc doxygen_hide_solver_enum
            enum class SolverIndex

        {
            solver = 1
        };


        //! \copydoc doxygen_hide_numbering_subset_enum
            enum class NumberingSubsetIndex
        {
            velocity_pressure = 1,
            velocity = 2,
            pressure = 3
        };


        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity_pressure)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity_pressure)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::pressure)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::Result
        >;


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;


    } // namespace ConformProjectorNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_CONFORM_PROJECTOR_x_INPUT_DATA_HPP_
