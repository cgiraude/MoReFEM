/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Oct 2017 21:38:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_EXPECTED_RESULTS_HXX_
# define MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_EXPECTED_RESULTS_HXX_


namespace MoReFEM
{


    namespace TestFunctionsNS
    {


        template<IsMatrixOrVector IsMatrixOrVectorT>
        void InsertNewEntry(const std::string& name,
                            content_type<IsMatrixOrVectorT>&& content,
                            expected_results_type<IsMatrixOrVectorT>& expected_results,
                            const char* invoking_file, int invoking_line)
        {
            const auto check = expected_results.insert(std::make_pair(name,
                                                                      std::move(content)));

            if (!check.second)
                throw Exception("Entry " + name + " already exists!", invoking_file, invoking_line);
        }



    } // namespace TestFunctionsNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_OPERATORS_x_TEST_FUNCTIONS_x_EXPECTED_RESULTS_HXX_
