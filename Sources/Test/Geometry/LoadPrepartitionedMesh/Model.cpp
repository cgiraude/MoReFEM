/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <fstream>
#include <sstream>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/LuaOptionFile/LuaOptionFile.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"

#include "Test/Geometry/LoadPrepartitionedMesh/Model.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
{


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) mpi = parent::GetMpi();
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) mesh = mesh_manager.GetMesh<1>();
        decltype(auto) output_dir = parent::GetOutputDirectory();

        decltype(auto) input_data = parent::GetMoReFEMData().GetInputData();

        decltype(auto) format =
            Internal::MeshNS::FormatNS::GetType(Utilities::InputDataNS
                                                ::Extract<InputDataNS::Mesh<1>::Format>::Value(input_data));

        Advanced::MeshNS::WritePrepartitionedData partition_data_facility(mesh,
                                                                          output_dir,
                                                                          format);

        LuaOptionFile mesh_prepartitioned_data(partition_data_facility.GetPartitionDataFile() , __FILE__, __LINE__);

        mesh_manager.LoadFromPrepartitionedData(mpi,
                                                10,
                                                partition_data_facility.GetReducedMeshFile(),
                                                mesh_prepartitioned_data,
                                                mesh.GetDimension(),
                                                MeshNS::Format::Medit);
    }


    void Model::CheckGeometricEltList() const
    {
        decltype(auto) mesh = GetOriginalMesh();
        decltype(auto) mesh_from_prepartitioned_data = GetMeshFromPrepartitionedData();

        BOOST_CHECK(mesh.GetDimension() == mesh_from_prepartitioned_data.GetDimension());

        {
            decltype(auto) original_geom_elt_list = mesh.GetGeometricEltList();
            decltype(auto) reconstructed_geom_elt_list = mesh_from_prepartitioned_data.GetGeometricEltList();

            # ifndef NDEBUG
            mesh.CheckGeometricEltOrdering();
            mesh_from_prepartitioned_data.CheckGeometricEltOrdering();
            # endif // NDEBUG

            const auto size = original_geom_elt_list.size();

            BOOST_CHECK_EQUAL(size, reconstructed_geom_elt_list.size());

            for (auto i = 0ul; i < size; ++i)
            {
                const auto& original_geom_elt_ptr = original_geom_elt_list[i];
                const auto& reconstructed_geom_elt_ptr = reconstructed_geom_elt_list[i];
                assert(!(!original_geom_elt_ptr));
                assert(!(!reconstructed_geom_elt_ptr));

                const auto& original_geom_elt = *original_geom_elt_ptr;
                const auto& reconstructed_geom_elt = *reconstructed_geom_elt_ptr;

                BOOST_CHECK_EQUAL(original_geom_elt.GetIndex(), reconstructed_geom_elt.GetIndex());

                decltype(auto) original_coords_list = original_geom_elt.GetCoordsList();
                decltype(auto) reconstructed_coords_list = reconstructed_geom_elt.GetCoordsList();

                const auto Ncoords = original_coords_list.size();

                BOOST_CHECK_EQUAL(Ncoords, reconstructed_coords_list.size());

                for (auto j = 0ul; j < Ncoords; ++j)
                {
                    const auto& original_coords_ptr = original_coords_list[j];
                    const auto& reconstructed_coords_ptr = reconstructed_coords_list[j];

                    assert(!(!original_coords_ptr));
                    assert(!(!reconstructed_coords_ptr));

                    BOOST_CHECK_EQUAL(original_coords_ptr->x(), reconstructed_coords_ptr->x());
                    BOOST_CHECK_EQUAL(original_coords_ptr->y(), reconstructed_coords_ptr->y());
                    BOOST_CHECK_EQUAL(original_coords_ptr->z(), reconstructed_coords_ptr->z());
                }
            }
        }

        {
            decltype(auto) original_bag_of_elt_type = mesh.BagOfEltType();
            decltype(auto) reconstructed_bag_of_elt_type = mesh_from_prepartitioned_data.BagOfEltType();

            const auto size = original_bag_of_elt_type.size();
            BOOST_CHECK_EQUAL(size, reconstructed_bag_of_elt_type.size());

            for (auto i = 0ul; i < size; ++i)
                BOOST_CHECK_EQUAL(original_bag_of_elt_type[i]->GetName(),
                                  reconstructed_bag_of_elt_type[i]->GetName());

        }
    }


    void Model::CheckLabelList() const
    {
        decltype(auto) mesh = GetOriginalMesh();
        decltype(auto) mesh_from_prepartitioned_data = GetMeshFromPrepartitionedData();

        decltype(auto) original_label_list = mesh.GetLabelList();
        decltype(auto) reconstructed_label_list = mesh_from_prepartitioned_data.GetLabelList();

        const auto size = original_label_list.size();

        BOOST_CHECK_EQUAL(size, reconstructed_label_list.size());

        for (auto i = 0ul; i < size; ++i)
        {
            auto original_label_ptr = original_label_list[i];
            auto reconstructed_label_ptr = reconstructed_label_list[i];

            assert(!(!original_label_ptr));
            assert(!(!reconstructed_label_ptr));

            BOOST_CHECK_EQUAL(original_label_ptr->GetIndex(), reconstructed_label_ptr->GetIndex());
            BOOST_CHECK_EQUAL(original_label_ptr->GetDescription(), reconstructed_label_ptr->GetDescription());
        }
    }


    void Model::CheckCoordsList() const
    {
        decltype(auto) mesh = GetOriginalMesh();
        decltype(auto) mesh_from_prepartitioned_data = GetMeshFromPrepartitionedData();

        BOOST_CHECK_EQUAL(mesh.NprocessorWiseCoord(), mesh_from_prepartitioned_data.NprocessorWiseCoord());
        BOOST_CHECK_EQUAL(mesh.NghostCoord(), mesh_from_prepartitioned_data.NghostCoord());

        std::ostringstream original_conv, reconstructed_conv;

        {
            const auto size = mesh.NprocessorWiseCoord();
            decltype(auto) original_coords_list = mesh.GetProcessorWiseCoordsList();
            decltype(auto) reconstructed_coords_list = mesh_from_prepartitioned_data.GetProcessorWiseCoordsList();

            BOOST_CHECK_EQUAL(size, original_coords_list.size());
            BOOST_CHECK_EQUAL(size, reconstructed_coords_list.size());

            for  (auto i = 0ul; i < size; ++i)
            {
                const auto& original_coords_ptr = original_coords_list[i];
                const auto& reconstructed_coords_ptr = reconstructed_coords_list[i];

                assert(!(!original_coords_ptr));
                assert(!(!reconstructed_coords_ptr));

                BOOST_CHECK_EQUAL(original_coords_ptr->GetIndex(), reconstructed_coords_ptr->GetIndex());
                original_conv.str("");
                reconstructed_conv.str("");

                original_coords_ptr->Print(original_conv);
                reconstructed_coords_ptr->Print(reconstructed_conv);

                BOOST_CHECK_EQUAL(original_conv.str(), reconstructed_conv.str());
            }
        }
    }


    void Model::CheckInterfaceList() const
    {
        decltype(auto) mesh = GetOriginalMesh();
        decltype(auto) mesh_from_prepartitioned_data = GetMeshFromPrepartitionedData();

        BOOST_CHECK_EQUAL(mesh.Nvertex(), mesh_from_prepartitioned_data.Nvertex());
        BOOST_CHECK_EQUAL(mesh.NprogramWiseVertex(), mesh_from_prepartitioned_data.NprogramWiseVertex());

        BOOST_CHECK_EQUAL(mesh.Nedge(), mesh_from_prepartitioned_data.Nedge());
        BOOST_CHECK_EQUAL(mesh.Nface(), mesh_from_prepartitioned_data.Nface());
        BOOST_CHECK_EQUAL(mesh.Nvolume(), mesh_from_prepartitioned_data.Nvolume());

        decltype(auto) original_geom_elt_list = mesh.GetGeometricEltList();
        decltype(auto) reconstructed_geom_elt_list = mesh_from_prepartitioned_data.GetGeometricEltList();

        # ifndef NDEBUG
        mesh.CheckGeometricEltOrdering();
        mesh_from_prepartitioned_data.CheckGeometricEltOrdering();
        # endif // NDEBUG

        const auto Ngeom_elt = original_geom_elt_list.size();

        BOOST_CHECK_EQUAL(Ngeom_elt, reconstructed_geom_elt_list.size());

        for (auto i = 0ul; i < Ngeom_elt; ++i)
        {
            const auto& original_geom_elt_ptr = original_geom_elt_list[i];
            const auto& reconstructed_geom_elt_ptr = reconstructed_geom_elt_list[i];
            assert(!(!original_geom_elt_ptr));
            assert(!(!reconstructed_geom_elt_ptr));

            const auto& original_geom_elt = *original_geom_elt_ptr;
            const auto& reconstructed_geom_elt = *reconstructed_geom_elt_ptr;

            BOOST_CHECK_EQUAL(original_geom_elt.GetIndex(), reconstructed_geom_elt.GetIndex());

            const auto& original_vertex_list = original_geom_elt.GetVertexList();
            const auto& reconstructed_vertex_list = reconstructed_geom_elt.GetVertexList();

            const auto Nvertex = original_vertex_list.size();
            BOOST_CHECK_EQUAL(Nvertex, reconstructed_vertex_list.size());

            for (auto j = 0ul; j < Nvertex; ++j)
            {
                const auto& original_vertex_ptr = original_vertex_list[j];
                const auto& reconstructed_vertex_ptr = reconstructed_vertex_list[j];

                assert(!(!original_vertex_ptr));
                assert(!(!reconstructed_vertex_ptr));

                BOOST_CHECK_EQUAL(original_vertex_ptr->GetIndex(), reconstructed_vertex_ptr->GetIndex());
            }

            const auto& original_edge_list = original_geom_elt.GetOrientedEdgeList();
            const auto& reconstructed_edge_list = reconstructed_geom_elt.GetOrientedEdgeList();
            const auto Nedge = original_edge_list.size();
            BOOST_CHECK_EQUAL(Nedge, reconstructed_edge_list.size());

            for (auto j = 0ul; j < Nedge; ++j)
            {
                const auto& original_edge_ptr = original_edge_list[j];
                const auto& reconstructed_edge_ptr = reconstructed_edge_list[j];

                assert(!(!original_edge_ptr));
                assert(!(!reconstructed_edge_ptr));

                BOOST_CHECK_EQUAL(original_edge_ptr->GetIndex(), reconstructed_edge_ptr->GetIndex());
            }

            const auto& original_face_list = original_geom_elt.GetOrientedFaceList();
            const auto& reconstructed_face_list = reconstructed_geom_elt.GetOrientedFaceList();
            const auto Nface = original_face_list.size();
            BOOST_CHECK_EQUAL(Nface, reconstructed_face_list.size());

            for (auto j = 0ul; j < Nface; ++j)
            {
                const auto& original_face_ptr = original_face_list[j];
                const auto& reconstructed_face_ptr = reconstructed_face_list[j];

                assert(!(!original_face_ptr));
                assert(!(!reconstructed_face_ptr));

                BOOST_CHECK_EQUAL(original_face_ptr->GetIndex(), reconstructed_face_ptr->GetIndex());
            }

            const auto& original_volume_ptr = original_geom_elt.GetVolumePtr();
            const auto& reconstructed_volume_ptr = reconstructed_geom_elt.GetVolumePtr();

            if (original_volume_ptr == nullptr)
                BOOST_CHECK(reconstructed_volume_ptr == nullptr);
            else
            {
                assert(!(!reconstructed_volume_ptr));
                BOOST_CHECK_EQUAL(original_volume_ptr->GetIndex(), reconstructed_volume_ptr->GetIndex());
            }

        }
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
