/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_x_INPUT_DATA_HPP_
# define MOREFEM_x_TEST_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"
# include "Core/MoReFEMData/MoReFEMData.hpp"

# include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/Geometry/Mesh.hpp"
# include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
# include "Core/InputData/Instances/Solver/Petsc.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace LightweightDomainListNS
        {


            //! \copydoc doxygen_hide_mesh_enum
            enum class MeshIndex
            {
                mesh = 1
            };


            //! \copydoc doxygen_hide_domain_enum
            enum class DomainIndex
            {
                domain = 1,
            };


            //! Default value for some input parameter that are required by a MoReFEM model but are actually unused for current test.
            constexpr auto whatever = 0;


            //! \copydoc doxygen_hide_input_data_tuple
            using InputDataTuple = std::tuple
            <
                InputDataNS::TimeManager,

                InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

                InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,
                InputDataNS::LightweightDomainList<1>,

                InputDataNS::Unknown<whatever>,
                InputDataNS::NumberingSubset<whatever>,
                InputDataNS::FEltSpace<whatever>,
                InputDataNS::Petsc<whatever>,

                InputDataNS::Result
            >;


            //! \copydoc doxygen_hide_model_specific_input_data
            using InputData = InputData<InputDataTuple>;

            //! \copydoc doxygen_hide_morefem_data_type
            using morefem_data_type = MoReFEMData<InputData, program_type::test>;


        } // namespace P1_to_P_HigherOrder_NS


    } // namespace LightweightDomainListNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_x_INPUT_DATA_HPP_
