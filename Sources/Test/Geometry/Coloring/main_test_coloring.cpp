/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 22:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>
#include <iostream>
#include <fstream>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "Test/Geometry/Coloring/InputData.hpp"
#include "Test/Geometry/Coloring/Model.hpp"


int main(int argc, char** argv)
{
    static_cast<void>(argc);
    static_cast<void>(argv);
    
    using namespace MoReFEM;
    
    //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = TestNS::ColoringNS::InputData;
    
    try
    {
        MoReFEMData<InputData, program_type::test> morefem_data(argc, argv);
        
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            TestNS::ColoringNS::Model model(morefem_data);
            model.Run();
        }
        catch(const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
        
    }
    catch(const ExceptionNS::GracefulExit&)
    {
        return EXIT_SUCCESS;
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;

    
   
}
