/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Geometry/Mesh/ComputeColoring.hpp"

#include "Test/Geometry/Coloring/Model.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS
    {
        
        
        namespace ColoringNS
        {
            
            
            Model::Model(const morefem_data_type& morefem_data)
            : parent(morefem_data)
            { }
            
            
            void Model::SupplInitialize()
            {
                
                
           void SupplInitialize();
                
                auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
                
                const auto& mesh = manager.GetMesh(1);
                
                std::vector<unsigned int> Ngeometric_elt_per_color;
                
                const auto& output_file = parent::GetOutputDirectory().AddFile("coloring_"
                + std::to_string(parent::GetMpi().GetRank<int>()) + ".hhdata");
                
                if (FilesystemNS::File::DoExist(output_file))
                    FilesystemNS::File::Remove(output_file, __FILE__, __LINE__);
                
                std::ofstream out;
                
                FilesystemNS::File::Create(out, output_file, __FILE__, __LINE__);

                auto coloring = ComputeColoring(mesh, 2, Ngeometric_elt_per_color);
                
                const std::size_t Ncolor = Ngeometric_elt_per_color.size();
                for (std::size_t color = 0u; color < Ncolor; ++color)
                    out << "Color " << color << " -> " << Ngeometric_elt_per_color[color] << " geometric elements." << std::endl;
                
                for (auto dimension = 1u; dimension < 4u; ++dimension)
                {
                    if (mesh.NgeometricElt(dimension))
                    {
                        auto geometric_elt_range = mesh.GetSubsetGeometricEltList(dimension);
                        out << "Ngeom elt dim " << dimension << " = "
                        << geometric_elt_range.second - geometric_elt_range.first << std::endl;
                    }
                }
            }
            
            
            void Model::Forward()
            { }
            
            
            void Model::SupplFinalizeStep()
            { }
            
            
            void Model::SupplFinalize()
            { }
            
            
        } // namespace ColoringNS
        
        
    } // namespace NS
    
    
} // namespace MoReFEM
