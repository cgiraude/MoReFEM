/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Test/Geometry/Movemesh/Model.hpp"


namespace MoReFEM
{
    
    
    namespace TestNS::MovemeshNS
    {


        Model::Model::Model(const morefem_data_type& morefem_data)
        : parent(morefem_data)
        { }
      
        
        namespace // anonymous
        {
            
            // The processor-wise and ghosted Coords should be one of the possibility below; if not something went wrong.
            const std::array<std::array<double, 3ul>, 12ul> expected_value_list
            {
                {
                    { { 100, -100, 200 } },
                    { { 100, -99, 200 } },
                    { { 101, -99, 200 } },
                    { { 101, -100, 200 } },
                    { { 100, -100, 201 } },
                    { { 100, -99, 201 } },
                    { { 101, -99, 201 } },
                    { { 101, -100, 201 } },
                    { { 100, -100, 202 } },
                    { { 100, -99, 202 } },
                    { { 101, -99, 202 } },
                    { { 101, -100, 202 } }
                }
            };
            
            
            const auto begin_expected_value_list = expected_value_list.cbegin();
            const auto end_expected_value_list = expected_value_list.cend();

            void CheckCoords(const Coords::vector_unique_ptr& coords_list)
            {
                for (const auto& coords_ptr : coords_list)
                {
                    assert(!(!coords_ptr));
                    
                    decltype(auto) coords_array = coords_ptr->GetCoordinateList();
                    
                    if (std::find(begin_expected_value_list, end_expected_value_list, coords_array)
                        == end_expected_value_list)
                    {
                        std::ostringstream oconv;
                        Utilities::PrintContainer<>::Do(coords_array, oconv, ", ", "[", "] ");
                        oconv << "was found but is not among the expected values which are:\n";
                        
                        for (const auto& item : expected_value_list)
                        Utilities::PrintContainer<>::Do(item, oconv, ", ", "\t[", "]\n");
                        
                        throw Exception(oconv.str(), __FILE__, __LINE__);
                        }
                    }
            }

            
            
        } // namespace anonymous
        


        void Model::SupplInitialize()
        {
            
           void SupplInitialize();
            
            decltype(auto) god_of_dof = parent::GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
            decltype(auto) mpi = parent::GetMpi();
            decltype(auto) felt_space = god_of_dof.GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::felt_space));
            decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::subset));
            decltype(auto) mesh = god_of_dof.GetMesh();
            decltype(auto) processor_wise_coords_list = mesh.GetProcessorWiseCoordsList();
            decltype(auto) ghosted_coords_list = mesh.GetGhostedCoordsList();
            
            assert(mesh.GetDimension() == 3u && "Test devised for 3D mesh.");
            
            const bool is_sequential = mpi.Nprocessor<int>() == 1;
            
            if (!is_sequential && mpi.Nprocessor<int>() != 4)
                throw Exception("This test was written to be run on 1 or 4 processors (expected results are generated "
                                "only for these cases", __FILE__, __LINE__);
            

            GlobalVector vector(numbering_subset);
            AllocateGlobalVector(god_of_dof, vector);
            
            {
                Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector, __FILE__, __LINE__);
                
                const auto size = content.GetSize(__FILE__, __LINE__);
                assert(size % 3u == 0u && "Only one three dimensional unknown in this test.");

                for (auto i = 0u; i < size; ++i)
                {
                    if (i % 3u == 0u) // x component
                        content[i] = 100.;
                    else if (i % 3u == 1u) // y component
                        content[i] = -100.;
                    else // z component
                        content[i] = 200.;
                }
            }

            felt_space.MoveMesh(vector);
            
            CheckCoords(processor_wise_coords_list);
            CheckCoords(ghosted_coords_list);
        }


        void Model::Forward()
        { }


        void Model::SupplFinalizeStep()
        { }
        

        void Model::SupplFinalize()
        { }


    } // namespace TestNS::MovemeshNS


} // namespace MoReFEM
