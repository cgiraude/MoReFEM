/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_MOVEMESH_x_MODEL_HXX_
# define MOREFEM_x_TEST_x_GEOMETRY_x_MOVEMESH_x_MODEL_HXX_


namespace MoReFEM
{


    namespace TestNS::MovemeshNS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string name("Movemesh");
            return name;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            // TODO: Put here any additional conditions that may trigger the end of the dynamic run.
            // If there is one, return true.
            // If there is none (i.e. the sole ending condition is to reach last time step) simply return false.
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        {
            // Base class InitializeStep() update the time for next iteration and print it on screen. If you
            // need additional operations, this is the place for them.
        }


    } // namespace TestNS::MovemeshNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_MOVEMESH_x_MODEL_HXX_
