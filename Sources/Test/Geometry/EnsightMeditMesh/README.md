Check the equivalence between an Ensight and Medit mesh (of course when possible: some geometric elements are supported by only one of them).

These tests create one from the other and check the content is the same.
