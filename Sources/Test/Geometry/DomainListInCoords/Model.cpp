/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "Test/Geometry/DomainListInCoords/Model.hpp"


namespace MoReFEM
{


    namespace TestNS
    {


        namespace DomainListInCoordsNS
        {


            Model::Model(const morefem_data_type& morefem_data)
            : parent(morefem_data, create_domain_list_for_coords::yes)
            { }

            
            namespace // anonymous
            {
                
                
                void CheckValue(std::string&& name,
                                unsigned int expected_value,
                                unsigned int computed_value);

                
            } // namespace anonymous


            void Model::SupplInitialize()
            {
                decltype(auto) god_of_dof = GetGodOfDof(EnumUnderlyingType(MeshIndex::mesh));
                
                decltype(auto) mesh = god_of_dof.GetMesh();
                
                decltype(auto) coords_list = mesh.GetProcessorWiseCoordsList();
                
                decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

                decltype(auto) domain_volume =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::volume), __FILE__, __LINE__);

                decltype(auto) domain_ring =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::ring), __FILE__, __LINE__);

                decltype(auto) domain_exterior =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::exterior_surface), __FILE__, __LINE__);

                decltype(auto) domain_interior =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::interior_surface), __FILE__, __LINE__);

                // Domains through lightweight domain list
                decltype(auto) domain_exterior_and_ring =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::exterior_and_ring), __FILE__, __LINE__);

                decltype(auto) domain_interior_and_ring =
                    domain_manager.GetDomain(EnumUnderlyingType(DomainIndex::interior_and_ring), __FILE__, __LINE__);

                for (decltype(auto) coords_ptr : coords_list)
                {
                    assert(!(!coords_ptr));
                    
                    decltype(auto) coords = *coords_ptr;
                    
                    if (coords.IsInDomain(domain_volume))
                        ++Ncoords_in_volume_;
                    
                    if (coords.IsInDomain(domain_ring))
                        ++Ncoords_in_ring_;
                    
                    if (coords.IsInDomain(domain_exterior))
                        ++Ncoords_in_exterior_;
                    
                    if (coords.IsInDomain(domain_interior))
                        ++Ncoords_in_interior_;
                    
                    if (coords.IsInDomain(domain_exterior_and_ring))
                        ++Ncoords_in_exterior_and_ring_;
                    
                    if (coords.IsInDomain(domain_interior_and_ring))
                        ++Ncoords_in_interior_and_ring_;
                }
            }


            void Model::CheckNormalDomain() const
            {
                decltype(auto) mpi = parent::GetMpi();
                const auto Nprocessor = mpi.Nprocessor<int>();

                auto Ncoords_in_volume = Ncoords_in_volume_;
                auto Ncoords_in_ring = Ncoords_in_ring_;
                auto Ncoords_in_exterior = Ncoords_in_exterior_;
                auto Ncoords_in_interior = Ncoords_in_interior_;

                if (Nprocessor > 1)
                {
                    Ncoords_in_volume = mpi.AllReduce(Ncoords_in_volume_, Wrappers::MpiNS::Op::Sum);
                    Ncoords_in_ring = mpi.AllReduce(Ncoords_in_ring_, Wrappers::MpiNS::Op::Sum);
                    Ncoords_in_exterior = mpi.AllReduce(Ncoords_in_exterior_, Wrappers::MpiNS::Op::Sum);
                    Ncoords_in_interior = mpi.AllReduce(Ncoords_in_interior_, Wrappers::MpiNS::Op::Sum);
                }

                CheckValue("Ncoords_in_volume", 48, Ncoords_in_volume);
                CheckValue("Ncoords_in_ring", 8, Ncoords_in_ring);
                CheckValue("Ncoords_in_exterior", 24, Ncoords_in_exterior);
                CheckValue("Ncoords_in_interior", 24, Ncoords_in_interior);
            }


            void Model::CheckLightweightDomain() const
            {
                decltype(auto) mpi = parent::GetMpi();
                const auto Nprocessor = mpi.Nprocessor<int>();

                auto Ncoords_in_exterior_and_ring = Ncoords_in_exterior_and_ring_;
                auto Ncoords_in_interior_and_ring = Ncoords_in_interior_and_ring_;

                if (Nprocessor > 1)
                {
                    Ncoords_in_exterior_and_ring =
                        mpi.AllReduce(Ncoords_in_exterior_and_ring, Wrappers::MpiNS::Op::Sum);
                    Ncoords_in_interior_and_ring =
                        mpi.AllReduce(Ncoords_in_interior_and_ring, Wrappers::MpiNS::Op::Sum);
                }

                CheckValue("Ncoords_in_exterior_and_ring", 28, Ncoords_in_exterior_and_ring);
                CheckValue("Ncoords_in_interior_and_ring", 28, Ncoords_in_interior_and_ring);
            }

            
            namespace // anonymous
            {


                PRAGMA_DIAGNOSTIC(push)
                #ifdef __clang__
                #include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
                #endif // # __clang__
                
                
                void CheckValue(std::string&& name,
                                unsigned int expected_value,
                                unsigned int computed_value)
                {
                    BOOST_TEST_INFO("Expected value for " << name << " was " << expected_value << " but computation "
                                    "through Coords iteration yielded " << computed_value);
                    BOOST_TEST(expected_value == computed_value);
                }


                PRAGMA_DIAGNOSTIC(pop)
                
                
            } // namespace anonymous



            void Model::Forward()
            { }


            void Model::SupplFinalizeStep()
            { }


            void Model::SupplFinalize()
            { }


    
        } // namespace DomainListInCoordsNS


    } // namespace TestNS


} // namespace MoReFEM
