//! \file
//
//
//  MacroVariationalOperator.hpp
//  MoReFEM
//
//  Created by sebastien on 02/04/2019.
//Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_MACRO_VARIATIONAL_OPERATOR_HPP_
# define MOREFEM_x_TEST_x_TOOLS_x_MACRO_VARIATIONAL_OPERATOR_HPP_

# include <memory>
# include <vector>

/*!
 * \class doxygen_hide_TEST_VARIATIONAL_OPERATOR
 *
 * \brief Convenient macro for testing operators that acts upon both scalar and vectorial unknowns.
 * It's ugly as the code is duplicated just to ignore a pragma only for clang compiler, but I can't
 * use something else than the macro I despised as Boost test is itself relying heavily on them.
 */

# ifdef __clang__
//! \copydoc doxygen_hide_TEST_VARIATIONAL_OPERATOR
#  define TEST_VARIATIONAL_OPERATOR \
PRAGMA_DIAGNOSTIC(push) \
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion") \
\
BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type) \
\
BOOST_AUTO_TEST_CASE(p1_scalar_same) \
{ \
    GetModel().SameUnknownP1(UnknownNS::Nature::scalar); \
} \
\
BOOST_AUTO_TEST_CASE(p1_vectorial_same) \
{ \
    GetModel().SameUnknownP1(UnknownNS::Nature::vectorial); \
} \
\
BOOST_AUTO_TEST_CASE(p2_scalar_same) \
{ \
    GetModel().SameUnknownP2(); \
} \
\
BOOST_AUTO_TEST_SUITE_END() \
\
BOOST_FIXTURE_TEST_SUITE(different_unknown, fixture_type) \
\
BOOST_AUTO_TEST_CASE(p1_scalar_same) \
{ \
    GetModel().UnknownP1TestP1(); \
} \
\
BOOST_AUTO_TEST_CASE(unknown_p2_test_p1) \
{ \
    GetModel().UnknownP2TestP1(); \
} \
\
BOOST_AUTO_TEST_SUITE_END() \
\
PRAGMA_DIAGNOSTIC(pop)


# else // __clang__

//! \copydoc doxygen_hide_TEST_VARIATIONAL_OPERATOR
#  define TEST_VARIATIONAL_OPERATOR \
BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type) \
\
BOOST_AUTO_TEST_CASE(p1_scalar_same) \
{ \
GetModel().SameUnknownP1(UnknownNS::Nature::scalar); \
} \
\
BOOST_AUTO_TEST_CASE(p1_vectorial_same) \
{ \
GetModel().SameUnknownP1(UnknownNS::Nature::vectorial); \
} \
\
BOOST_AUTO_TEST_CASE(p2_scalar_same) \
{ \
GetModel().SameUnknownP2(); \
} \
\
BOOST_AUTO_TEST_SUITE_END() \
\
BOOST_FIXTURE_TEST_SUITE(different_unknown, fixture_type) \
\
BOOST_AUTO_TEST_CASE(p1_scalar_same) \
{ \
GetModel().UnknownP1TestP1(); \
} \
\
BOOST_AUTO_TEST_CASE(unknown_p2_test_p1) \
{ \
GetModel().UnknownP2TestP1(); \
} \
\
BOOST_AUTO_TEST_SUITE_END()

# endif // __clang__

#endif // MOREFEM_x_TEST_x_TOOLS_x_MACRO_VARIATIONAL_OPERATOR_HPP_
