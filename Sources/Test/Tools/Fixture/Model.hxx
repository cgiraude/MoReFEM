/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HXX_
# define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HXX_


namespace MoReFEM
{


    namespace TestNS::FixtureNS
    {


        template
        <
            class ModelT,
            class LuaFileT
        >
        Model<ModelT, LuaFileT>::Model()
        {
            static bool first_call = true;

            if (first_call)
            {
                const auto root_value = std::getenv("MOREFEM_RESULT_DIR");

                if (root_value != nullptr)
                {
                    std::ostringstream oconv;
                    oconv << "Please run the tests with MOREFEM_RESULT_DIR environment variables unset! "
                    "(currently it is set to " << root_value << ").";
                    throw Exception(oconv.str(), __FILE__, __LINE__);
                }

                first_call = false;
            }
        }


        template
        <
            class ModelT,
            class LuaFileT
        >
        const ModelT& Model<ModelT, LuaFileT>::GetModel()
        {
            static bool first_call = true;

            static ModelT model(GetMoReFEMData());

            if (first_call)
            {
                model.Run();
                first_call = false;
            }

            return model;
        }


        template
        <
            class ModelT,
            class LuaFileT
        >
        const typename Model<ModelT, LuaFileT>::morefem_data_type&
        Model<ModelT, LuaFileT>::GetMoReFEMData()
        {
            static initialize_type init(LuaFileT::GetPath());

            return init.GetMoReFEMData();
        }


    } // namespace TestNS::FixtureNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HXX_
