/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Mar 2018 16:10:26 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_INITIALIZE_TEST_MO_RE_F_E_M_DATA_HXX_
# define MOREFEM_x_TEST_x_TOOLS_x_INITIALIZE_TEST_MO_RE_F_E_M_DATA_HXX_


namespace MoReFEM
{


    namespace TestNS
    {


        template<class InputDataT>
        template<class T>
        InitializeTestMoReFEMData<InputDataT>
        ::InitializeTestMoReFEMData(T&& lua_file)
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

            decltype(auto) input_file =
                environment.SubstituteValues(lua_file);

            char program_name[] = "ProgramName";
            char option[] = "-i";
            char* input_file_char = const_cast<char*>(input_file.c_str());
            char* dummy_args[] = { program_name, option, input_file_char, nullptr };

            argv_ = dummy_args;
            morefem_data_ = std::make_unique<morefem_data_type>(3, argv_);
        }


        template<class InputDataT>
        const typename InitializeTestMoReFEMData<InputDataT>::morefem_data_type&
        InitializeTestMoReFEMData<InputDataT>::GetMoReFEMData() const noexcept
        {
            assert(!(!morefem_data_));
            return *morefem_data_;
        }
    

    } // namespace TestNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_TOOLS_x_INITIALIZE_TEST_MO_RE_F_E_M_DATA_HXX_
