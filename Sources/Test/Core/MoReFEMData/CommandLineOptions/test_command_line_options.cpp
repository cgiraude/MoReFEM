/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sat, 7 Apr 2018 20:10:49 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE cli_options
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/InputData/InputData.hpp"

#include "Test/Tools/Fixture/Mpi.hpp"
#include "Test/Tools/Fixture/Environment.hpp"

using namespace MoReFEM;


namespace // anonymous
{

    struct NewArguments
    {

        void Add(TCLAP::CmdLine& command);

        const std::string& GetAdditionalStringValue() const;

        int GetAdditionalIntValue() const;

    private:

        std::unique_ptr<TCLAP::ValueArg<std::string>> additional_string_argument_ = nullptr;

        std::unique_ptr<TCLAP::ValueArg<int>> additional_int_argument_ = nullptr;

    };


    struct Fixture
    : public TestNS::FixtureNS::Environment,
    public TestNS::FixtureNS::Mpi
    { };


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(cli_options, Fixture)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    /* BOOST_REQUIRE_NO_THROW */(environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));

    decltype(auto) input_file =
        environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/CommandLineOptions/"
                                     "demo_morefem_data.lua");

    using InputDataTuple = std::tuple
    <
        InputDataNS::Result
    >;

    using InputData = InputData<InputDataTuple>;
    using morefem_data_type = MoReFEMData
    <
        InputData,
        program_type::test,
        Utilities::InputDataNS::DoTrackUnusedFields::no,
        NewArguments
    >;

    char program_name[] = "ProgramName";
    char option1[] = "-i";
    char* input_file_char = const_cast<char*>(input_file.c_str());
    char option2[] = "-e";
    char arg2[] = "MOREFEM_WHATEVER_DIR=/tmp/Whatever";
    char option3[] = "-e";
    char arg3[] = "MOREFEM_WHATEVER=42";
    char option4[] = "--suppl_string";
    char arg4[] = "Foo";
    char option5[] = "--suppl_int";
    char arg5[] = "42";


    char* argv[] = { program_name, option1, input_file_char, option2, arg2, option3, arg3, option4, arg4, option5,
        arg5, nullptr };

    morefem_data_type::const_unique_ptr ptr;

    /* BOOST_REQUIRE_NO_THROW */(ptr = std::make_unique<morefem_data_type>(11, argv));

    BOOST_CHECK(environment.GetEnvironmentVariable("MOREFEM_WHATEVER", __FILE__, __LINE__) == "42");

    const auto& morefem_data = *ptr;

    decltype(auto) input_data = ptr->GetInputData();

    BOOST_CHECK(Utilities::InputDataNS::Extract<InputDataNS::Result::OutputDirectory>::Path(input_data)
          == std::string("/tmp/Whatever/Foo"));

    BOOST_CHECK(morefem_data.GetAdditionalStringValue() == std::string("Foo"));
    BOOST_CHECK(morefem_data.GetAdditionalIntValue() == 42);
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void NewArguments::Add(TCLAP::CmdLine& command)
    {
        additional_string_argument_ = std::make_unique<TCLAP::ValueArg<std::string>>("",
                                                                                     "suppl_string",
                                                                                     "additional command only for this model",
                                                                                     true, // is mandatory or not
                                                                                     "",
                                                                                     "string",
                                                                                     command);

        additional_int_argument_ = std::make_unique<TCLAP::ValueArg<int>>("",
                                                                          "suppl_int",
                                                                          "additional command only for this model",
                                                                          false, // is mandatory or not
                                                                          0,
                                                                          "int",
                                                                          command);
    }


    const std::string& NewArguments::GetAdditionalStringValue() const
    {
        assert(!(!additional_string_argument_));
        return additional_string_argument_->getValue();
    }

    int NewArguments::GetAdditionalIntValue() const
    {
        assert(!(!additional_int_argument_));
        return additional_int_argument_->getValue();
    }


} // namespace anonymous
