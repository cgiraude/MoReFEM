add_executable(MoReFEMTestStrongType
              ${CMAKE_CURRENT_LIST_DIR}/main.cpp
              )
          
target_link_libraries(MoReFEMTestStrongType
                      ${MOREFEM_TEST_TOOLS})

add_test(StrongType MoReFEMTestStrongType)
set_tests_properties(StrongType PROPERTIES TIMEOUT 20)
