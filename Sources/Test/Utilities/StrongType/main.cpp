/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 27 Feb 2020 18:22:06 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE strongtype
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Type/StrongType.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(strongtype_test)
{
    using StrongIntType = StrongType<int, struct IntTag>;
    using StrongIntRefType = StrongType<int&, struct IntRefTag>;
    
    int raw_int = 42;
    auto raw_int_ref = &raw_int;
    
    StrongIntType strong_int = StrongIntType(raw_int);
    StrongIntRefType strong_int_ref = StrongIntRefType(raw_int);
    
    BOOST_REQUIRE(strong_int.get() == raw_int);
    BOOST_REQUIRE(strong_int_ref.get() == raw_int);
    BOOST_REQUIRE(&strong_int_ref.get() == raw_int_ref);
    BOOST_REQUIRE(sizeof(StrongIntType) == sizeof(int));
    
    strong_int_ref.get() = 3;
    BOOST_REQUIRE(raw_int == 3);
    
    using StrongStringType = StrongType<std::string, struct StringTag>;
    using StrongStringRefType = StrongType<std::string&, struct StringRefTag>;
    
    std::string raw_string = "foo";
    auto raw_string_ref = &raw_string;
    
    StrongStringType strong_string = StrongStringType(raw_string);
    StrongStringRefType strong_string_ref = StrongStringRefType(raw_string);
    
    BOOST_REQUIRE(strong_string.get() == raw_string);
    BOOST_REQUIRE(strong_string_ref.get() == raw_string);
    BOOST_REQUIRE(&strong_string_ref.get() == raw_string_ref);
    BOOST_REQUIRE(sizeof(StrongStringType) == sizeof(std::string));
    
    strong_string_ref.get() = "bar";
    BOOST_REQUIRE(raw_string == "bar");
}

PRAGMA_DIAGNOSTIC(pop)
