/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <list>
#include <deque>
#include <map>

#define BOOST_TEST_MODULE print_containers
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Variant.hpp"
#include "Utilities/Containers/PrintPolicy/Pointer.hpp"
#include "Utilities/Containers/PrintPolicy/Associative.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(vector_simple)
{
    std::vector<int> a { 5, 7, -4, 3 };

    std::ostringstream oconv;

    Utilities::PrintContainer<>::Do(a, oconv);
    BOOST_CHECK_EQUAL(oconv.str(), "[5, 7, -4, 3]\n");
}


BOOST_AUTO_TEST_CASE(deque_custom)
{
    std::deque<std::string> a { "Not", "a", "hello", "world" };

    std::ostringstream oconv;

    Utilities::PrintContainer<>::Do(a, oconv, ' ', "", '!');
    BOOST_CHECK_EQUAL(oconv.str(), "Not a hello world!");
}


BOOST_AUTO_TEST_CASE(vector_of_simple_variant)
{
    using variant = std::variant<int, double, std::string>;

    std::vector<variant> a { 4.3, 3, "Hello", -6, "Bye!" };
    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Variant>::Do(a, oconv,  ", ", "{", '}');

    BOOST_CHECK_EQUAL(oconv.str(), "{4.3, 3, Hello, -6, Bye!}");
}


BOOST_AUTO_TEST_CASE(list_print_n_elt)
{
    std::list<unsigned int> primes { 2, 3, 5, 7, 11, 13 };
    std::ostringstream oconv;
    Utilities::PrintContainer<>::Nelt<3>(primes, oconv, ", ", "{", ", ... }");

    BOOST_CHECK_EQUAL(oconv.str(), "{2, 3, 5, ... }");
}


BOOST_AUTO_TEST_CASE(print_tuple)
{
    std::tuple<int, std::string, double, char> tuple { 5, "Hello", 3.14, 'a'};

    std::ostringstream oconv;
    Utilities::PrintTuple(tuple, oconv, ", ", "{ ", "}");

    BOOST_CHECK_EQUAL(oconv.str(), "{ 5, \"Hello\", 3.14, \"a\"}");
}


BOOST_AUTO_TEST_CASE(print_vector_of_pointer)
{
    std::vector<std::unique_ptr<std::size_t>> vector_of_ptr;

    for (auto i = 0ul; i < 10; ++i)
        vector_of_ptr.emplace_back(std::make_unique<std::size_t>(i));

    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Pointer>::Do(vector_of_ptr, oconv, ", ", "{ ", " }");

    BOOST_CHECK_EQUAL(oconv.str(), "{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }");
}


BOOST_AUTO_TEST_CASE(print_associative_containers)
{
    std::map<std::string, unsigned int> map { { "triangle", 3 }, { "segment", 2 }, { "quadrangle", 4 } };

    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Associative<>>::Do(map, oconv, ", ", "{ ", " }");

    BOOST_CHECK_EQUAL(oconv.str(), "{ (quadrangle, 4), (segment, 2), (triangle, 3) }");
}


BOOST_AUTO_TEST_CASE(print_associative_keys)
{
    std::map<std::string, unsigned int> map { { "triangle", 3 }, { "segment", 2 }, { "quadrangle", 4 } };

    std::ostringstream oconv;
    Utilities::PrintContainer<Utilities::PrintPolicyNS::Key>::Do(map, oconv, ", ", "{ ", " }");

    BOOST_CHECK_EQUAL(oconv.str(), "{ quadrangle, segment, triangle }");
}


PRAGMA_DIAGNOSTIC(pop)

