add_executable(MoReFEMTestTuple
                ${CMAKE_CURRENT_LIST_DIR}/main.cpp
               )

target_link_libraries(MoReFEMTestTuple
                      ${MOREFEM_TEST_TOOLS}
)

add_test(TestUtilitiesTuple MoReFEMTestTuple)
set_tests_properties(TestUtilitiesTuple PROPERTIES TIMEOUT 20)
