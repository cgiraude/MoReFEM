add_executable(MoReFEMTestTclap
                ${CMAKE_CURRENT_LIST_DIR}/test.cpp
               )
          
target_link_libraries(MoReFEMTestTclap
                      ${MOREFEM_TEST_TOOLS}
                      )
                      
add_test(Tclap MoReFEMTestTclap)
set_tests_properties(Tclap PROPERTIES TIMEOUT 20)
