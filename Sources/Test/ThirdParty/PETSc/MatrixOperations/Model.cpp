/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <fstream>
#include <sstream>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
{


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(1);
        decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(1);
        decltype(auto) mpi = god_of_dof.GetMpi();

        vector_init_1_ = std::make_unique<GlobalVector>(numbering_subset);
        vector_init_2_ = std::make_unique<GlobalVector>(numbering_subset);
        vector_init_3_ = std::make_unique<GlobalVector>(numbering_subset);
        
        auto& vector_init_1 = *vector_init_1_;
        auto& vector_init_2 = *vector_init_2_;
        auto& vector_init_3 = *vector_init_3_;
        
        AllocateGlobalVector(god_of_dof, vector_init_1);
        AllocateGlobalVector(god_of_dof, vector_init_2);
        AllocateGlobalVector(god_of_dof, vector_init_3);

        matrix_init_1_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_init_2_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_init_3_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        
        matrix_4_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_5_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_6_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_7_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_8_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_9_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        matrix_10_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        
        auto& matrix_init_1 = *matrix_init_1_;
        auto& matrix_init_2 = *matrix_init_2_;
        auto& matrix_init_3 = *matrix_init_3_;
        
        // no initialization for the following matrices:
        auto& matrix_4 = *matrix_4_;
        auto& matrix_5 = *matrix_5_;
        auto& matrix_6 = *matrix_6_;
        auto& matrix_7 = *matrix_7_;
        auto& matrix_8 = *matrix_8_;
        auto& matrix_9 = *matrix_9_;
        auto& matrix_10 = *matrix_10_;
        
        // only three matrices allocated, all the others are set to PETSC_NULL
        AllocateGlobalMatrix(god_of_dof, matrix_init_1);
        AllocateGlobalMatrix(god_of_dof, matrix_init_2);
        AllocateGlobalMatrix(god_of_dof, matrix_init_3);
        
        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector_init_1,
                                                                                            __FILE__, __LINE__);
            
            const auto size = content.GetSize(__FILE__, __LINE__);
            const auto rank_plus_one = static_cast<double>(1 + mpi.GetRank<int>());

            for (auto i = 0u; i < size; ++i)
                content[i] = rank_plus_one * std::sqrt(i); // completely arbitrary value with no redundancy!
            
            // First all wrappers that do not use a DeReuseMatrix policy
            Wrappers::Petsc::MatCreateTranspose(matrix_init_1,
                                                matrix_4,
                                                __FILE__, __LINE__);

            Wrappers::Petsc::AXPY<NonZeroPattern::same>(1.,
                                                        matrix_init_1,
                                                        matrix_init_2,
                                                        __FILE__, __LINE__);
                        
            Wrappers::Petsc::MatShift(1.,
                                      matrix_init_1,
                                      __FILE__, __LINE__);
            
            Wrappers::Petsc::MatMult(matrix_init_1,
                                     vector_init_1,
                                     vector_init_2,
                                     __FILE__, __LINE__);
            
            Wrappers::Petsc::MatMultAdd(matrix_init_1,
                                        vector_init_1,
                                        vector_init_2,
                                        vector_init_3,
                                        __FILE__, __LINE__);
            
            Wrappers::Petsc::MatMultTranspose(matrix_init_1,
                                              vector_init_1,
                                              vector_init_2,
                                              __FILE__, __LINE__);

            Wrappers::Petsc::MatMultTransposeAdd(matrix_init_1,
                                                 vector_init_1,
                                                 vector_init_2,
                                                 vector_init_3,
                                                 __FILE__, __LINE__);

            // Petsc wrappers that have a DoReuseMatrix policy
            {
                Wrappers::Petsc::MatTranspose(matrix_init_1,
                                              matrix_5,
                                              __FILE__, __LINE__,
                                              Wrappers::Petsc::DoReuseMatrix::no);
                
                Wrappers::Petsc::MatTranspose(matrix_init_1,
                                              matrix_5,
                                              __FILE__, __LINE__,
                                              Wrappers::Petsc::DoReuseMatrix::yes);
              
                Wrappers::Petsc::MatTranspose(matrix_init_1,
                                              matrix_init_1,
                                              __FILE__, __LINE__,
                                              Wrappers::Petsc::DoReuseMatrix::in_place);
            }
            {
                Wrappers::Petsc::MatMatMult(matrix_init_1,
                                            matrix_init_2,
                                            matrix_6,
                                            __FILE__, __LINE__,
                                            Wrappers::Petsc::DoReuseMatrix::no);
              
                Wrappers::Petsc::MatMatMult(matrix_init_1,
                                            matrix_init_2,
                                            matrix_6,
                                            __FILE__, __LINE__,
                                            Wrappers::Petsc::DoReuseMatrix::yes);
            }
            {
                Wrappers::Petsc::MatTransposeMatMult(matrix_init_1,
                                                     matrix_init_2,
                                                     matrix_7,
                                                     __FILE__, __LINE__,
                                                     Wrappers::Petsc::DoReuseMatrix::no);
            
                Wrappers::Petsc::MatTransposeMatMult(matrix_init_1,
                                                     matrix_init_2,
                                                     matrix_7,
                                                     __FILE__, __LINE__,
                                                     Wrappers::Petsc::DoReuseMatrix::yes);
            }
            {
                Wrappers::Petsc::MatMatTransposeMult(matrix_init_1,
                                                     matrix_init_2,
                                                     matrix_8,
                                                     __FILE__, __LINE__,
                                                     Wrappers::Petsc::DoReuseMatrix::no);
                
                Wrappers::Petsc::MatMatTransposeMult(matrix_init_1,
                                                     matrix_init_2,
                                                     matrix_8,
                                                     __FILE__, __LINE__,
                                                     Wrappers::Petsc::DoReuseMatrix::yes);
            }
            {
                Wrappers::Petsc::PtAP(matrix_init_1,
                                      matrix_init_2,
                                      matrix_9,
                                      __FILE__, __LINE__,
                                      Wrappers::Petsc::DoReuseMatrix::no);
            
                Wrappers::Petsc::PtAP(matrix_init_1,
                                      matrix_init_2,
                                      matrix_9,
                                      __FILE__, __LINE__,
                                      Wrappers::Petsc::DoReuseMatrix::yes);
            }
            {
                Wrappers::Petsc::MatMatMatMult(matrix_init_1,
                                               matrix_init_2,
                                               matrix_init_3,
                                               matrix_10,
                                               __FILE__, __LINE__,
                                               Wrappers::Petsc::DoReuseMatrix::no);
                
                Wrappers::Petsc::MatMatMatMult(matrix_init_1,
                                               matrix_init_2,
                                               matrix_init_3,
                                               matrix_10,
                                               __FILE__, __LINE__,
                                               Wrappers::Petsc::DoReuseMatrix::yes);
            }
            {
                IS rperm;
                IS cperm;
                    
                Wrappers::Petsc::GetOrdering(matrix_init_1,
                                             MATORDERINGNATURAL,
                                             &rperm,
                                             &cperm,
                                             __FILE__, __LINE__);
                ISDestroy(&rperm);
                ISDestroy(&cperm);
            }
            
            // \todo
//
//            {
//                IS perm;
//
//                Wrappers::Petsc::CholeskyFactor(matrix_init_1,
//                                                perm,
//                                                NULL,
//                                                __FILE__, __LINE__);
//            }
//
//            {
//                IS row;
//                IS col;
//
//                Wrappers::Petsc::LUFactor(matrix_init_1,
//                                          NULL,
//                                          NULL,
//                                          NULL,
//                                          __FILE__, __LINE__);
//            }
//
//            {
//
//                        Wrappers::Petsc::MatMatSolve(matrix_init_1,
//                                                     matrix_init_2,
//                                                     matrix_4,
//                                                     __FILE__, __LINE__);
//
//            }
          

        }

    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
