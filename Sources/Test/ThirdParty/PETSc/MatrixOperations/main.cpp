/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>
#include <cstdlib>
#include <iostream>
#include <fstream>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Test/ThirdParty/PETSc/MatrixOperations/InputData.hpp"
#include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"


int main(int argc, char** argv)
{
    static_cast<void>(argc);
    static_cast<void>(argv);
    
    using namespace MoReFEM;
    
    //! \copydoc doxygen_hide_model_specific_input_data
    using InputData = TestNS::PetscNS::MatrixOperationsNS::InputData;
    
    try
    {
        MoReFEMData<InputData, program_type::test> morefem_data(argc, argv);
        
        const auto& mpi = morefem_data.GetMpi();
        
        try
        {
            TestNS::PetscNS::MatrixOperationsNS::Model model(morefem_data);
            model.Run();
        }
        catch(const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch(const std::exception& e)
        {
            ExceptionNS::PrintAndAbort(mpi, e.what());
        }
    }
    catch(const ExceptionNS::GracefulExit&)
    {
        return EXIT_SUCCESS;
    }
    catch(const std::exception& e)
    {
        std::ostringstream oconv;
        oconv << "Exception caught from MoReFEMData<InputData>: " << e.what() << std::endl;
        
        std::cout << oconv.str();
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
    
    
    
}
