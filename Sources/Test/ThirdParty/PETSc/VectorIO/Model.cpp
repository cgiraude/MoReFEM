/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 28 Jul 2017 12:12:25 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <fstream>
#include <sstream>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Filesystem/Advanced/Directory.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/ThirdParty/PETSc/VectorIO/Model.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    namespace // anonymous
    {


        std::string ComputeProcessorWiseAsciiFile(const Model& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/vector_"
            << model.GetMpi().GetRank<int>() << ".hhdata";
            return oconv.str();
        }


        std::string ComputeProcessorWiseBinaryFile(const Model& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/vector_"
            << model.GetMpi().GetRank<int>() << ".bin";
            return oconv.str();
        }


        std::string ComputeProgramWiseAsciiFile(const Model& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/program_wise_vector.m";
            std::string ret = oconv.str();

            oconv.str("");
            oconv << "Rank_" << model.GetMpi().GetRank<int>();
            Utilities::String::Replace(oconv.str(), "Rank_0", ret);
            return ret;
        }


        std::string ComputeProgramWiseBinaryFile(const Model& model)
        {
            std::ostringstream oconv;
            oconv << model.GetOutputDirectory() << "/program_wise_vector.bin";
            std::string ret = oconv.str();

            oconv.str("");
            oconv << "Rank_" << model.GetMpi().GetRank<int>();
            Utilities::String::Replace(oconv.str(), "Rank_0", ret);
            return ret;
        }


    } // namespace anonymous


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data)
    { }


    const std::string& Model::GetProcessorWiseBinaryFile() const noexcept
    {
        static auto ret = ComputeProcessorWiseBinaryFile(*this);
        return ret;
    }


    const std::string& Model::GetProcessorWiseAsciiFile() const noexcept
    {
        static auto ret = ComputeProcessorWiseAsciiFile(*this);
        return ret;
    }


    const std::string& Model::GetProgramWiseBinaryFile() const noexcept
    {
        static auto ret = ComputeProgramWiseBinaryFile(*this);
        return ret;
    }


    const std::string& Model::GetProgramWiseAsciiFile() const noexcept
    {
        static auto ret = ComputeProgramWiseAsciiFile(*this);
        return ret;
    }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(1);
        decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(1);
        decltype(auto) mpi = god_of_dof.GetMpi();

        vector_ = std::make_unique<GlobalVector>(numbering_subset);
        auto& vector = *vector_;

        AllocateGlobalVector(god_of_dof, vector);

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector,
                                                                                            __FILE__, __LINE__);


            const auto size = content.GetSize(__FILE__, __LINE__);
            const auto rank_plus_one = static_cast<double>(1 + mpi.GetRank<int>());

            for (auto i = 0u; i < size; ++i)
                content[i] = rank_plus_one * std::sqrt(i); // completely arbitrary value with no redundancy!

        }

        {
            const auto binary_file = GetProcessorWiseBinaryFile();
            if (FilesystemNS::File::DoExist(binary_file))
                FilesystemNS::File::Remove(binary_file, __FILE__, __LINE__);
        }

        {
            const auto ascii_file = GetProcessorWiseAsciiFile();
            if (FilesystemNS::File::DoExist(ascii_file))
                FilesystemNS::File::Remove(ascii_file, __FILE__, __LINE__);
        }

        if (mpi.IsRootProcessor())
        {
            {
                const auto binary_file = GetProgramWiseBinaryFile();
                if (FilesystemNS::File::DoExist(binary_file))
                    FilesystemNS::File::Remove(binary_file, __FILE__, __LINE__);
            }

            {
                const auto ascii_file = GetProgramWiseAsciiFile();
                if (FilesystemNS::File::DoExist(ascii_file))
                    FilesystemNS::File::Remove(ascii_file, __FILE__, __LINE__);
            }
        }
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS
