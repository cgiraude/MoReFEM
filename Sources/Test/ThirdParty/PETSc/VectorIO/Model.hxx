/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_MODEL_HXX_
# define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_MODEL_HXX_


namespace MoReFEM
{


    namespace TestNS::PetscNS::VectorIONS
    {


        inline const std::string& Model::ClassName()
        {
            static std::string ret("Test Petsc Vector IO");
            return ret;
        }


        inline bool Model::SupplHasFinishedConditions() const
        {
            return false; // ie no additional condition
        }


        inline void Model::SupplInitializeStep()
        { }


        inline const GlobalVector& Model::GetVector() const noexcept
        {
            assert(!(!vector_));
            return *vector_;
        }


    } // namespace  TestNS::PetscNS::VectorIONS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_MODEL_HXX_
