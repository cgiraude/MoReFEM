/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE petsc_vector_io
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/Fixture/Model.hpp"
#include "Test/ThirdParty/PETSc/VectorIO/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct LuaFile
    {


        static std::string GetPath();


    };


    using fixture_type =
    TestNS::FixtureNS::Model
    <
        TestNS::PetscNS::VectorIONS::Model,
        LuaFile
    >;


} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_SUITE(processor_wise, fixture_type)

    BOOST_AUTO_TEST_CASE(creation)
    {
        decltype(auto) model = GetModel();
        decltype(auto) mpi = model.GetMpi();

        decltype(auto) vector = model.GetVector();

        std::string ascii_file = model.GetProcessorWiseAsciiFile();
        std::string binary_file = model.GetProcessorWiseBinaryFile();

        BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == false);
        BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == false);

        vector.Print<MpiScale::processor_wise>(mpi, binary_file, __FILE__, __LINE__);

        // As Lua file is set to ask binary file.
        BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == false);
        BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == true);

        vector.Print<MpiScale::processor_wise>(mpi, ascii_file, __FILE__, __LINE__, binary_or_ascii::ascii);

        BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == true);
        BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == true);
    }


    BOOST_AUTO_TEST_CASE(load_ascii)
    {
        decltype(auto) model = GetModel();
        decltype(auto) mpi = model.GetMpi();

        std::string ascii_file = model.GetProcessorWiseAsciiFile();
        BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == true); // created in previous test!
        decltype(auto) vector = model.GetVector();

        std::vector<PetscInt> ghost_padding;

        if (mpi.Nprocessor<int>() > 1)
            ghost_padding = vector.GetGhostPadding();

        Wrappers::Petsc::Vector from_file;
        from_file.InitParallelFromProcessorWiseAsciiFile(mpi,
                                                  static_cast<unsigned int>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
                                                  static_cast<unsigned int>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
                                                  ghost_padding,
                                                  ascii_file,
                                                  __FILE__, __LINE__);

        mpi.Barrier();

        std::string inequality_description;
        Wrappers::Petsc::AreEqual(from_file,
                                  vector,
                                  NumericNS::DefaultEpsilon<double>(),
                                  inequality_description,
                                  __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(inequality_description, "");
    }

    BOOST_AUTO_TEST_CASE(load_binary)
    {
        decltype(auto) model = GetModel();
        decltype(auto) mpi = model.GetMpi();

        std::string binary_file = model.GetProcessorWiseBinaryFile();
        BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == true); // created in previous test!
        decltype(auto) vector = model.GetVector();

        std::vector<PetscInt> ghost_padding;

        if (mpi.Nprocessor<int>() > 1)
            ghost_padding = vector.GetGhostPadding();

        Wrappers::Petsc::Vector from_file;
        from_file.InitParallelFromProcessorWiseBinaryFile(mpi,
                                                          static_cast<unsigned int>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
                                                          static_cast<unsigned int>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
                                                          ghost_padding,
                                                          binary_file,
                                                          __FILE__, __LINE__);

        mpi.Barrier();

        std::string inequality_description;
        Wrappers::Petsc::AreEqual(from_file,
                                  vector,
                                  NumericNS::DefaultEpsilon<double>(),
                                  inequality_description,
                                  __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(inequality_description, "");
    }


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(program_wise, fixture_type)

    BOOST_AUTO_TEST_CASE(creation)
    {
        decltype(auto) model = GetModel();
        decltype(auto) mpi = model.GetMpi();

        decltype(auto) vector = model.GetVector();

        std::string ascii_file = model.GetProgramWiseAsciiFile();
        std::string binary_file = model.GetProgramWiseBinaryFile();

        BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == false);
        BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == false);

        vector.Print<MpiScale::program_wise>(mpi, binary_file, __FILE__, __LINE__);

        // As Lua file is set to ask binary file.
        if (mpi.IsRootProcessor())
        {
            BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == false);
            BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == true);
        }

        vector.Print<MpiScale::program_wise>(mpi, ascii_file, __FILE__, __LINE__, binary_or_ascii::ascii);

        if (mpi.IsRootProcessor())
        {
            BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == true);
            BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == true);
        }
    }


    BOOST_AUTO_TEST_CASE(load_binary)
    {
        decltype(auto) model = GetModel();
        decltype(auto) mpi = model.GetMpi();

        std::string binary_file = model.GetProgramWiseBinaryFile();
        BOOST_CHECK(FilesystemNS::File::DoExist(binary_file) == true); // created in previous test!
        decltype(auto) vector = model.GetVector();
        Wrappers::Petsc::Vector from_file;

        std::vector<PetscInt> ghost_padding;

        if (mpi.Nprocessor<int>() > 1)
            ghost_padding = vector.GetGhostPadding();

        from_file.InitFromProgramWiseBinaryFile(mpi,
                                                static_cast<unsigned int>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
                                                static_cast<unsigned int>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
                                                ghost_padding,
                                                binary_file,
                                                __FILE__, __LINE__);

        std::string inequality_description;
        Wrappers::Petsc::AreEqual(from_file,
                                  vector,
                                  NumericNS::DefaultEpsilon<double>(),
                                  inequality_description,
                                  __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(inequality_description, "");
    }

    // No load_ascii: not foreseen apparently in PETSc interface!


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(sequential_only, fixture_type)

    BOOST_AUTO_TEST_CASE(load_ascii)
    {
        decltype(auto) model = GetModel();
        decltype(auto) mpi = model.GetMpi();

        if (mpi.Nprocessor<int>() == 1)
        {
            std::string ascii_file = model.GetProgramWiseAsciiFile();

            BOOST_CHECK(FilesystemNS::File::DoExist(ascii_file) == true); // created in previous test!
            decltype(auto) vector = model.GetVector();
            Wrappers::Petsc::Vector from_file;

            std::vector<PetscInt> ghost_padding;

            BOOST_CHECK_EQUAL(vector.GetProcessorWiseSize(__FILE__, __LINE__),
                              vector.GetProgramWiseSize(__FILE__, __LINE__));

            from_file.InitSequentialFromFile(mpi,
                                             ascii_file,
                                             __FILE__, __LINE__);

            std::string inequality_description;
            Wrappers::Petsc::AreEqual(from_file,
                                      vector,
                                      NumericNS::DefaultEpsilon<double>(),
                                      inequality_description,
                                      __FILE__, __LINE__);

            BOOST_CHECK_EQUAL(inequality_description, "");
        }
    }



BOOST_AUTO_TEST_SUITE_END()


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    std::string LuaFile::GetPath()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

        return
            environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/ThirdParty/PETSc/VectorIO/demo.lua");
    }


} // namespace anonymous

