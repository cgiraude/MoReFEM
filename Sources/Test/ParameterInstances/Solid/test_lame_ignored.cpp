/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 24 Aug 2018 15:34:17 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE solid_lame_ignored
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Domain/DomainManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Tools/InitializeTestMoReFEMData.hpp"
#include "Test/Tools/Fixture/Environment.hpp"


using namespace MoReFEM;


namespace MoReFEM
{


    struct TestHelper
    {
        TestHelper()
        {
            static bool first = true;

            if (first)
            {
                first = false;
                DomainManager::CreateOrGetInstance(__FILE__, __LINE__).Create(1,
                                                                              { 1 },
                                                                              { 2 },
                                                                              { }, { });
            }
        }

    };


} // namespace MoReFEM


namespace  // anonymous
{


    constexpr double volumic_mass = 10.4;

    constexpr double hyperelastic_bulk =  1750000.;

    constexpr double kappa1 = 500.;

    constexpr double kappa2 = 403346.1538461538;

    constexpr double poisson_ratio = .3;

    constexpr double young_modulus = 21.e5;

    constexpr double viscosity = 8.;

    constexpr double mu1 =  9.;

    constexpr double mu2 = 10.;

    constexpr double c0 = 11.;

    constexpr double c1 = 12.;

    constexpr double c2 = 13.;

    constexpr double c3 = 14.;

    constexpr double c4 = 15.;
    
    constexpr double c5 = 16.;
    
    
} // namespace anonymous


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_CASE(lame_ignored, TestNS::FixtureNS::Environment)
{
    decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

    using InputDataTuple = std::tuple
    <
        InputDataNS::Solid,
        InputDataNS::Result
    >;

    using InputData = InputData<InputDataTuple>;

    TestNS::InitializeTestMoReFEMData<InputData>
    init(environment.SubstituteValues("${MOREFEM_ROOT}/Sources/Test/ParameterInstances/Solid/"
                                      "demo_solid_lame_ignored.lua"));

    decltype(auto) mesh_file = environment.SubstituteValues("${MOREFEM_ROOT}/Data/Mesh/one_triangle.mesh");

    Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__).Create(1,
                                                                                  mesh_file,
                                                                                  2,
                                                                                  MeshNS::Format::Medit,
                                                                                  1.);

    TestHelper test;

    QuadratureRulePerTopology quad_rule_per_topology(3, 3);

    Solid solid(init.GetMoReFEMData().GetInputData(),
                DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(1, __FILE__, __LINE__),
                quad_rule_per_topology);

    BOOST_CHECK_THROW(solid.GetLameLambda(), std::exception);
    BOOST_CHECK_THROW(solid.GetLameMu(), std::exception);
    BOOST_CHECK(NumericNS::AreEqual(solid.GetVolumicMass().GetConstantValue(), volumic_mass));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetHyperelasticBulk().GetConstantValue(), hyperelastic_bulk));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetKappa1().GetConstantValue(), kappa1));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetKappa2().GetConstantValue(), kappa2));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetViscosity().GetConstantValue(), viscosity));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetMu1().GetConstantValue(), mu1));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetMu2().GetConstantValue(), mu2));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC0().GetConstantValue(), c0));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC1().GetConstantValue(), c1));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC2().GetConstantValue(), c2));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC3().GetConstantValue(), c3));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC4().GetConstantValue(), c4));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC5().GetConstantValue(), c5));

    BOOST_CHECK(NumericNS::AreEqual(solid.GetPoissonRatio().GetConstantValue(), poisson_ratio));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetYoungModulus().GetConstantValue(), young_modulus));
}


PRAGMA_DIAGNOSTIC(pop)
