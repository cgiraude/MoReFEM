add_executable(MoReFEMTestSolidAllDefined
               ${CMAKE_CURRENT_LIST_DIR}/test_all_defined.cpp)

add_executable(MoReFEMTestSolidLameIgnored
               ${CMAKE_CURRENT_LIST_DIR}/test_lame_ignored.cpp)

add_executable(MoReFEMTestSolidNoKappaMuC
               ${CMAKE_CURRENT_LIST_DIR}/test_no_kappa_mu_C.cpp)

add_executable(MoReFEMTestSolidPoissonWithoutYoung
               ${CMAKE_CURRENT_LIST_DIR}/test_poisson_without_young.cpp)

target_link_libraries(MoReFEMTestSolidAllDefined
                      ${MOREFEM_TEST_TOOLS})

target_link_libraries(MoReFEMTestSolidLameIgnored
                       ${MOREFEM_TEST_TOOLS})

target_link_libraries(MoReFEMTestSolidNoKappaMuC
                      ${MOREFEM_TEST_TOOLS})

target_link_libraries(MoReFEMTestSolidPoissonWithoutYoung
                      ${MOREFEM_TEST_TOOLS})

add_test(SolidAllDefined
         MoReFEMTestSolidAllDefined
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(SolidAllDefined PROPERTIES TIMEOUT 20)

add_test(SolidLameIgnored
         MoReFEMTestSolidLameIgnored
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(SolidLameIgnored PROPERTIES TIMEOUT 20)

add_test(SolidNoKappaMuC
         MoReFEMTestSolidNoKappaMuC
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(SolidNoKappaMuC PROPERTIES TIMEOUT 20)

add_test(SolidPoissonWithoutYoung
         MoReFEMTestSolidPoissonWithoutYoung
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(SolidPoissonWithoutYoung PROPERTIES TIMEOUT 20)

