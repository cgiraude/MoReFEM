/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_ONDOMATIC_NUMBERING_x_INPUT_DATA_HPP_
# define MOREFEM_x_TEST_x_ONDOMATIC_NUMBERING_x_INPUT_DATA_HPP_

# include "Core/MoReFEMData/MoReFEMData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"


namespace MoReFEM
{


    namespace OndomaticNumberingNS
    {


        //! \copydoc doxygen_hide_input_data_tuple
        using InputDataTuple = std::tuple
        <
            InputDataNS::Unknown::Name<1>,
            InputDataNS::Unknown::Nature<1>,

            InputDataNS::Mesh::Mesh<1>,
            InputDataNS::Mesh::Format<1>,
            InputDataNS::Mesh::Dimension<1>,

            InputDataNS::Domain::MeshIndex<1>,
            InputDataNS::Domain::DimensionList<1>,
            InputDataNS::Domain::MeshLabelList<1>,
            InputDataNS::Domain::GeomEltTypeList<1>,

            InputDataNS::FEltSpace::GodOfDofIndex<1>,
            InputDataNS::FEltSpace::DomainIndex<1>,
            InputDataNS::FEltSpace::UnknownList<1>,
            InputDataNS::FEltSpace::ShapeFunctionList<1>,
            InputDataNS::FEltSpace::NumberingSubsetList<1>,

            InputData::BoundaryCondition,

            InputDataNS::Result::OutputDirectory
        >;


        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

        //! \copydoc doxygen_hide_morefem_data_type
        using morefem_data_type = MoReFEMData<InputData, program_type::test>;



    }; // namespace OndomaticNumberingNS


} // namespace MoReFEM


#endif // MOREFEM_x_TEST_x_ONDOMATIC_NUMBERING_x_INPUT_DATA_HPP_
