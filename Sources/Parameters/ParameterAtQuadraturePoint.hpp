/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Mar 2016 17:51:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_PARAMETER_AT_QUADRATURE_POINT_HPP_
# define MOREFEM_x_PARAMETERS_x_PARAMETER_AT_QUADRATURE_POINT_HPP_

# include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
# include "Parameters/Internal/ParameterInstance.hpp"


namespace MoReFEM
{


    /*!
     * \brief Convenient alias for a parameter defined at dof.
     *
     * Currently Doxygen doesn't seem able to display the interface of the underlying class, despite the
     * TYPEDEF_HIDES_STRUCT = YES in Doxygen file. You may see the full interface of the class (on complete Doxygen
     * documentation) at
     * Internal::ParameterNS::ParameterInstance except the constructor (due to variadic arguments)
     *
     * \code
     * template<class StringT>
     * ParameterAtQuadraturePoint(T&& name,
     *                            const Mesh& mesh,
     *                            const QuadratureRulePerTopology& quadrature_rule_per_topology,
     *                            storage_value_type initial_value,
     *                            const TimeManager& time_manager);
     * \endcode
     *
     * where:
     * - mesh Mesh considered. It is actually unused for this kind Parameter, but required nonetheless
     * to conform to generic parameter interface.
     * - initial_value A scalar, vectorial or matrix initial value to create the parameter.
     * - time_manager The time manager of MoReFEM.
     *
     * \attention Current implementation is not secure enough with respect of quadrature rule! \todo #1031
     */

    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None
    >
    using ParameterAtQuadraturePoint =
        Internal::ParameterNS::ParameterInstance
        <
            TypeT,
            ParameterNS::Policy::AtQuadraturePoint,
            TimeDependencyT
        >;


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_PARAMETER_AT_QUADRATURE_POINT_HPP_
