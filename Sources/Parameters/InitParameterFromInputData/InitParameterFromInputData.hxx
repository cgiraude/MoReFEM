/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 11:30:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
# define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_



namespace MoReFEM
{


    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class T,
        class InputDataT
    >
    typename ScalarParameter<TimeDependencyT>::unique_ptr
    InitScalarParameterFromInputData(T&& name,
                                     const Domain& domain,
                                     const InputDataT& input_data)
    {
        namespace IPL = Utilities::InputDataNS;

        decltype(auto) nature =
            IPL::Extract<typename ParameterT::Nature>::Value(input_data);

        if (nature == "ignore")
            return nullptr;

        decltype(auto) value =
            IPL::Extract<typename ParameterT::Value>::Value(input_data);

        return Internal::ParameterNS::InitScalarParameterFromInputData
                <
                    TimeDependencyT
                >(std::forward<T>(name),
                  domain,
                  nature,
                  value);
    }


    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class T,
        class InputDataT
    >
    typename Parameter<ParameterNS::Type::vector, LocalCoords, TimeDependencyT>::unique_ptr
    InitThreeDimensionalParameterFromInputData(T&& name,
                                               const Domain& domain,
                                               const InputDataT& input_data)
    {
        namespace IPL = Utilities::InputDataNS;

        decltype(auto) nature_vector = IPL::Extract<typename ParameterT::Nature>::Value(input_data);

        assert(nature_vector.size() == 3ul);
        const auto& nature_x = nature_vector[0];
        const auto& nature_y = nature_vector[1];
        const auto& nature_z = nature_vector[2];

        if ((nature_x == "ignore" || nature_y == "ignore" || nature_z == "ignore")
            && (nature_x != nature_y || nature_x != nature_z))
            throw Exception("Error for " + ParameterT::GetName() + ": if "
                            "one of the item is 'ignore' the other ones should be 'ignore' as well.",
                            __FILE__, __LINE__);

        if (nature_x == "ignore")
            return nullptr;

        decltype(auto) value_vector = IPL::Extract<typename ParameterT::Value>::Value(input_data);
        assert(value_vector.size() == 3ul);

        auto&& component_x =
            Internal::ParameterNS::InitScalarParameterFromInputData
            <
                ParameterNS::TimeDependencyNS::None
            >("Value X",
              domain,
              nature_x,
              value_vector[0]);

        auto&& component_y =
            Internal::ParameterNS::InitScalarParameterFromInputData
            <
                ParameterNS::TimeDependencyNS::None
            >("Value Y",
              domain,
              nature_y,
              value_vector[1]);

        auto&& component_z =
            Internal::ParameterNS::InitScalarParameterFromInputData
            <
                ParameterNS::TimeDependencyNS::None
            >("Value Z",
              domain,
              nature_z,
              value_vector[2]);

        return std::make_unique<ParameterNS::ThreeDimensionalParameter<TimeDependencyT>>(std::forward<T>(name),
                                                                                         std::move(component_x),
                                                                                         std::move(component_y),
                                                                                         std::move(component_z));
    }


    template<>
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class StringT,
        class InputDataT
    >
    inline typename Parameter<ParameterNS::Type::scalar, LocalCoords, TimeDependencyT>::unique_ptr
    InitParameterFromInputData<ParameterNS::Type::scalar>::Perform(StringT&& name,
                                                                   const Domain& domain,
                                                                   const InputDataT& input_data)
    {
        return InitScalarParameterFromInputData<ParameterT, TimeDependencyT>(name,
                                                                             domain,
                                                                             input_data);
    }


    template<>
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT,
        class StringT,
        class InputDataT
    >
    inline typename Parameter<ParameterNS::Type::vector, LocalCoords, TimeDependencyT>::unique_ptr
    InitParameterFromInputData<ParameterNS::Type::vector>::Perform(StringT&& name,
                                                                   const Domain& domain,
                                                                   const InputDataT& input_data)
    {
        return InitThreeDimensionalParameterFromInputData<ParameterT, TimeDependencyT>(name,
                                                                                       domain,
                                                                                       input_data);
    }


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
