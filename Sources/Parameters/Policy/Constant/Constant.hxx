/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_CONSTANT_x_CONSTANT_HXX_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_CONSTANT_x_CONSTANT_HXX_


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            template<ParameterNS::Type TypeT>
            Constant<TypeT>::Constant(const std::string&,
                                      const Domain& ,
                                      typename Constant<TypeT>::storage_type value)
            : value_(value)
            { }


            template<ParameterNS::Type TypeT>
            [[noreturn]]  typename Constant<TypeT>::return_type Constant<TypeT>
            ::GetValueFromPolicy(const local_coords_type& local_coords,
                                 const GeometricElt& geom_elt) const
            {
                static_cast<void>(local_coords);
                static_cast<void>(geom_elt);

                assert(false && "Parameter class should have guided toward GetConstantValue()!");
                exit(-1);
            }


            template<ParameterNS::Type TypeT>
            inline typename Constant<TypeT>::return_type Constant<TypeT>::GetConstantValueFromPolicy() const
            {
                return value_;
            }


            template<ParameterNS::Type TypeT>
            inline typename Constant<TypeT>::return_type Constant<TypeT>::GetAnyValueFromPolicy() const
            {
                return GetConstantValueFromPolicy();
            }


            template<ParameterNS::Type TypeT>
            inline constexpr bool Constant<TypeT>::IsConstant() const noexcept
            {
                return true;
            }


            template<ParameterNS::Type TypeT>
            void Constant<TypeT>::WriteFromPolicy(std::ostream& out) const
            {
                out << "# Homogenous value:" << std::endl;
                out << GetConstantValueFromPolicy() << std::endl;
            }


            template<ParameterNS::Type TypeT>
            inline void Constant<TypeT>::SetConstantValue(double value) noexcept
            {
                value_ = value;
            }



        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_CONSTANT_x_CONSTANT_HXX_
