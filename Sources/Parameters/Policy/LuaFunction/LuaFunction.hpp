/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HPP_
# define MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HPP_

# include <memory>
# include <vector>

# include "Utilities/InputData/LuaFunction.hpp"

# include "Core/SpatialLuaFunction.hpp"
# include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"

# include "Geometry/Coords/Coords.hpp"
# include "Geometry/GeometricElt/Advanced/FreeFunctions.hpp"

# include "Parameters/ParameterType.hpp"


namespace MoReFEM
{


    namespace ParameterNS
    {


        namespace Policy
        {


            /*!
             * \brief Parameter policy when the parameter gets a value given by an analytic function provided in the
             * input data file.
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             * \tparam SettingsSpatialFunctionT An instantiation of InputData::SpatialFunction<> template class.
             *
             * \internal <b><tt>[internal]</tt></b> An enum or a template template parameter would be more elegant for
             * SettingsSpatialFunctionT, but it is unfortunately not possible, as the policy is passed through variadic
             * arguments, and variadic arguments can't mix type and non-type parameters.
             * \endinternal
             */
            template
            <
                ParameterNS::Type TypeT,
                class SettingsSpatialFunctionT
            >
            class LuaFunction
            {

            public:

                //! \copydoc doxygen_hide_alias_self
                using self = LuaFunction<TypeT, SettingsSpatialFunctionT>;

                //! \copydoc doxygen_hide_parameter_local_coords_type
                using local_coords_type = LocalCoords;

            private:

                //! Alias to traits class related to TypeT.
                using traits = Traits<TypeT>;

            public:

                //! Alias to return type.
                using return_type = typename traits::return_type;

                //! Alias to the type of the Lua function.
                using lua_function_type = typename SettingsSpatialFunctionT::lua_function_type;

                //! Alias to the storage of the Lua function.
                using storage_type = const lua_function_type&;

            public:

                /// \name Special members.
                ///@{

                /*!
                 * \brief Constructor.
                 *
                 * \copydoc doxygen_hide_parameter_name_and_domain_arg
                 * \param[in] lua_function The lua function that returns the value of the Parameter.
                 */
                explicit LuaFunction(const std::string& name,
                                     const Domain& domain,
                                     storage_type lua_function);

                //! Destructor.
                ~LuaFunction() = default;

                //! \copydoc doxygen_hide_copy_constructor
                LuaFunction(const LuaFunction& rhs) = delete;

                //! \copydoc doxygen_hide_move_constructor
                LuaFunction(LuaFunction&& rhs) = delete;

                //! \copydoc doxygen_hide_copy_affectation
                LuaFunction& operator=(const LuaFunction& rhs) = delete;

                //! \copydoc doxygen_hide_move_affectation
                LuaFunction& operator=(LuaFunction&& rhs) = delete;

                ///@}

            public:

                /*!
                 * \brief Enables to modify the constant value of a parameter. Disabled for this Policy.
                 */
                void SetConstantValue(double );

            protected:

                /*!
                 * \brief Provided here to make the code compile, but should never be called
                 *
                 * (Constant value should not be given through a function: it is less efficient and the constness
                 * is not appearant in the code...).
                 *
                 * \return Irrelevant here.
                 */
                [[noreturn]] return_type GetConstantValueFromPolicy() const;

                //! \copydoc doxygen_hide_parameter_get_value
                return_type GetValueFromPolicy(const local_coords_type& local_coords,
                                               const GeometricElt& geom_elt) const;


                //! \copydoc doxygen_hide_parameter_suppl_get_any_value
                return_type GetAnyValueFromPolicy() const;

            protected:

                //! Whether the parameter varies spatially or not.
                bool IsConstant() const;

                //! Write the content of the parameter for which policy is used in a stream.
                //! \copydoc doxygen_hide_stream_inout
                void  WriteFromPolicy(std::ostream& stream) const;

            private:

                /*!
                 * \brief Non constant accessor to the helper Coords object, useful when Lua function acts upon global
                 * elements.
                 *
                 * \internal <b><tt>[internal]</tt></b> This is const because the helper coords is mutable to be
                 * used in const methods.
                 * \endinternal
                 *
                 * \return Reference to the helper Coords object.
                 */
                SpatialPoint& GetNonCstWorkCoords() const noexcept;

            private:

                //! Store a reference to the Lua function (which is owned by the InputData class).
                storage_type lua_function_;

                //! Helper Coords object, useful when Lua function acts upon global elements.
                mutable SpatialPoint work_coords_;

            };


            /*!
             * \class doxygen_hide_parameter_lua_function_specialization
             *
             * \tparam TypeT  Type of the parameter (real, vector, matrix).
             *
             * \internal This is helpful to run it within template programming context: alias defined here gets
             * this way a similar nature as Constant or PiecewiseConstantByDomain and hence may be used in functions
             * expecting a specific template template parameter.
             * \endinternal
             */

            /*!
             * \brief Specialization of the LuaFuntion policy for spatial functions with global coordinates.
             *
             * The \a Parameter is evaluated locally as are all \a Parameters, but the Lua function provided in the
             * Lua file is defined with x, y and z as GLOBAL coordinates.
             *
             * \copydoc doxygen_hide_parameter_lua_function_specialization
             *
             */
            template<ParameterNS::Type TypeT>
            using LuaFunction3DGlobalCoords =
                ::MoReFEM::ParameterNS::Policy::LuaFunction
                <
                    TypeT,
                    InputDataNS::SpatialFunction
                    <
                        CoordsType::global,
                        spatial_lua_function
                    >
                >;


            /*!
             * \brief Specialization of the LuaFuntion policy for spatial functions with local coordinates.
             *
             * \copydoc doxygen_hide_parameter_lua_function_specialization
             *
             */
            template<ParameterNS::Type TypeT>
            using LuaFunction3DLocalCoords =
                ::MoReFEM::ParameterNS::Policy::LuaFunction
                <
                    TypeT,
                    InputDataNS::SpatialFunction
                    <
                        CoordsType::local,
                        spatial_lua_function
                    >
                >;



        } // namespace Policy


    } // namespace ParameterNS


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


# include "Parameters/Policy/LuaFunction/LuaFunction.hxx"


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_LUA_FUNCTION_x_LUA_FUNCTION_HPP_
