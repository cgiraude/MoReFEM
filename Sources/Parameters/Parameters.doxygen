/*!
* \defgroup ParametersGroup Parameter
*
* \brief This module encompass the definition of so-called Parameters, which are objects for which values at quadrature
* points might be queried.
*
* There are two distinct categories of \a Parameter:
* - Those that are defined from the input data file. They might be constant, piece constant by domain or given
* by an analytical Lua function, at the choice of the user of the model.
* - Parameters defined at dofs (that extract values from an underlying \a GlobalVector) or directly at quadrature points,
* which are hardcoded in each model.
*/


/// \addtogroup ParametersGroup
///@{


/// \namespace MoReFEM::ParameterNS
/// \brief Namespace that enclose stuff related to \a Parameter.


///@} // addtogroup


/*!
 * \class doxygen_hide_param_time_dependancy
 *
 * A Parameter is first and foremost a spatial-dependant data: its main purpose is to describe the value
 * of a physical parameter at a given local position. However, we might also want to apply a decoupled time
 * dependancy, i.e. consider value of the parameter might be determined by:
 * \verbatim
 P(x, t) = f(x) * g(t)
 \endverbatim
 *
 * In this case, g(t) is stored as a function and is recomputed at each \a Parameter::TimeUpdate() calls (such calls
 * should therefore be located in Model::InitializeStep() where the time update actually occur).
 */


/*!
 * \class doxygen_hide_parameter_suppl_get_value
 *
 * \brief Get the (spatially-only) value of the parameter at a given local position in a given \a geom_elt.
 *
 * If the \a Parameter gets a time dependency (which is of the form f(x) * g(t)), current method returns only
 * f(x). This method is expected to be called only in GetValue() method, which adds up the g(t) contribution
 * \internal This choice also makes us respect a C++ idiom that recommends avoiding virtual public methods.
 * \endinternal
 *
 * \param[in] geom_elt \a GeometricElt inside which the value is computed.
 *
 * \return Value of the parameter.
 */

/*!
 * \class doxygen_hide_parameter_suppl_get_value_local_coords
 *
 * \copydoc doxygen_hide_parameter_suppl_get_value
 * \param[in] local_coords \a LocalCoords at which the value is computed.
 */

 /*!
 * \class doxygen_hide_parameter_suppl_get_value_quad_pt
 *
  * \copydoc doxygen_hide_parameter_suppl_get_value
 * \param[in] quad_pt \a QuadraturePoint at which the value is computed.
 */
 
 
 /*!
* \class doxygen_hide_parameter_suppl_get_any_value
*
* \brief Returns a stored value (Any: the point is actually to assert its type for some functions overload).
*
* \internal <b><tt>[internal]</tt></b> The point here is not the value itself, but the informations that
* might be retrieved from it, such as number of rows and columns if TypeT == Type::matrix.
* \endinternal
*
* \return Any value of the relevant type for the \a Parameter.
*/


/*!
 * \class doxygen_hide_parameter_suppl_time_update_with_time
 *
 * \brief Add here any additional TimeUpdate that might be relevant.
 *
 * For instance, if a Parameter depends on other parameters, you must make sure those are correctly updated,
 * and possibly some internals might have to be recomputed.
 *
 * One should prefer to use the default one if one wants to use the current time.
 * Extra security to verify the synchro of the parameter to the current time is done in he default one.
 * This method is for particular cases only when the user knows exactly what is he doing.
 *
 * \param[in] time Time for the update.
 */

/*!
 * \class doxygen_hide_parameter_suppl_time_update
 *
 * \brief Add here any additional TimeUpdate that might be relevant.
 *
 * For instance, if a Parameter depends on other parameters, you must make sure those are correctly updated,
 * and possibly some internals might have to be recomputed.
 */


/*!
 * \class doxygen_hide_parameter_domain_arg
 *
 * \param[in] domain \a Domain upon which the \a Parameter is defined.
 */


/*!
 * \class doxygen_hide_parameter_name_and_domain_arg
 *
 * \param[in] name Name of the Parameter.
 * \copydoc doxygen_hide_parameter_domain_arg
 */


/*!
 * \class doxygen_hide_parameter_local_coords_type
 *
 * \brief Convenient alias to decide where a \a Parameter is computed.
 *
 * Parameters are by construct objects that are evaluated at a local position in a given \a GeometricElt. Depending
 * on the exact type of the \a Parameter, the local position might be given by either a \a LocalCoords or a \a
 * QuadraturePoint (the latter being a child of the former).
 */


/*!
 * \class doxygen_hide_parameter_get_value
 *
 * \brief Get the value of the \a Parameter at a specific location for a given \a GeometricElt.
 *
 * \param[in] local_coords \a Local position for which the \a Parameter value is sought.
 * \param[in] geom_elt \a GeometricElt for which the \a Parameter value is sought.
 */


/*!
 * \class doxygen_hide_parameter_get_value_quad_pt
 *
 * \brief Get the value of the \a Parameter at a specific location for a given \a GeometricElt.
 *
 * \param[in] quad_pt \a Local position for which the \a Parameter value is sought.
 * \param[in] geom_elt \a GeometricElt for which the \a Parameter value is sought.
 */
 
/*!
* \class doxygen_hide_quadrature_rule_per_topology_arg
*
* \param[in] quadrature_rule_per_topology Quadrature rule for each relevant topology (if one useful in the Model
* is not specified there an exception will be thrown when usage is attempted). Do not deallocate this raw pointer:
* it is assumed the actual object is stored in an object such as \a VariationalFormulation or a \a Model,
* probably under a const_unique_ptr to avoid bothering with manual deallocation.
*/


/*!
* \class doxygen_hide_quadrature_rule_per_topology_nullptr_arg
*
* \copydetails doxygen_hide_quadrature_rule_per_topology_arg
* If nullptr, the rules defined in the \a FEltSpace are used.
*/


/*!
 * \class doxygen_hide_at_dof_policy_tparam
 *
 * \tparam TypeT \a ParameterNS::Type might be scalar or vectorial.
 * \tparam NfeltSpace If 1, only one \a FEltSpace is expected; of any dimension.
 * If 2, two are expected: one that deals with \a LocalFEltSpace the same dimension of the mesh, the other
 * for this dimension minus 1. 3 is possible as well, going up to dimension minus 2.
 */


/*!
 * \class doxygen_hide_param_at_dof_and_unknown
 *
 * \attention The purpose of to get here the dofs related to a given \a Unknown. The \a GlobalVector from
 * which data are extracted might actually include more, but a single \a ParameterAtDof can provide
 * values for only one of them. If you actually needs both, just define two \a ParameterAtDof, one for each
 * \a Unknown.
 */


/*!
 *
 * \class doxygen_hide_parameter_without_time_dependency
 *
 * At the moment the time dependency is not allowed (it is relatively easy to change that: just add a new
 * argument in the constructor and propagate it into the underlying Parameter. Only drawback is that the class
 * should become template with time dependancy functor as template argument, unless you impose the functor
 * to be something like std::function<double(double)>).
 */




/*!
 * \class doxygen_hide_parameter_at_dof_template_args
 *
 * \copydoc doxygen_hide_at_dof_policy_tparam
 * \copydoc doxygen_hide_param_time_dependancy
 */


