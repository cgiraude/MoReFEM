%
%  untitled
%
%  Created by Sebastien Gilles on 2013-05-16.
%  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
%
\documentclass[]{article}

% Use utf-8 encoding for foreign characters
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

% Setup for fullpage use
\usepackage{fullpage}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% More symbols
%\usepackage{amsmath}
%\usepackage{amssymb}
%\usepackage{latexsym}

% Surround parts of graphics with box
\usepackage{boxedminipage}

% Package for including code in the document
\usepackage{listings}
\usepackage{verbatim}
\usepackage{color}
\usepackage{indentfirst}

% Package for biblio
\usepackage[authoryear,round,comma]{natbib}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=C++,                    % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}





% If you want to generate a toc for each chapter (use with book)
\usepackage{minitoc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

%\newif\ifpdf
%\ifx\pdfoutput\undefined
%\pdffalse % we are not running PDFLaTeX
%\else
%\pdfoutput=1 % we are running PDFLaTeX
%\pdftrue
%\fi

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi



\title{SCons compilation in HappyHeart}
\author{Sébastien Gilles}

%\date{2013-05-23}

\begin{document}
	
\newcommand{\refchapter}[1]{chapter \ref{#1}}
\newcommand{\refsection}[1]{section \ref{#1}}
\newcommand{\refcode}[1]{listing \ref{#1}}
\newcommand{\path}[1]{\textit{#1}}
\newcommand{\code}[1]{\textit{#1}}
%\renewcommand{\lstlistingname}{Code excerpt}

\newcommand{\subsubsubsection}[1]
{
\bigskip
\textbf{#1}
\bigskip
}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle

\section*{Introduction}

By and large, HappyHeart's main compilation method is its XCode project: the IDE is really handy in development steps, especially for its static checks and the integrated use of the debugger.
\medskip

However, there are some limitations attained to it:

\begin{itemize}    
    \item XCode uses clang as C/C++ compiler, and it's not straightforward to switch compiler inside it. clang is fine but it's convenient to be able to switch compiler to check code is completely compliant with C++ standard\footnote{Probably no compiler is 100 \% standard compliant so it's better to make sure something is not working due a quirk of current compiler.}.
    \item XCode is available only in Mac OS X, so we need an alternate way to compile the code under any other Unix-based system\footnote{HappyHeart is currently not expected to compile under Visual Studio.}.
\end{itemize}

A command-line compilation tool may address all of these issues; SCons has been chosen mostly because it was already in use in the Verdandi project in which M3DISIM team is also involved.

\section{A quick overview of SCons interface}

Before delving into the internals of HappyHeart's SCons structure, let's see first the commands required to build HappyHeart.
\medskip

SCons must be run from the \lstinline{Sources/} folder of HappyHeart. The minimal command line is:
\medskip

\lstset{language=sh,literate={--}{{-\,-}}1}
\begin{lstlisting}[label=BasicSConsCommand,caption=Minimal SCons command line.]
    scons --config_file=*build_configuration_file*
\end{lstlisting}

This command calls the SConstruct file\footnote{Basically the equivalent of a Makefile.} and read the user specific data in the given build configuration file. Said file is a Python file which must provide several definitions without which SCons will fail to compile (see an example of it on the \refcode{BuildExample})
\medskip

HappyHeart git repository provides sample files which help a user to write his own; here is mine for the compilation of HappyHeart with clang compiler and with static libraries:

\begin{lstlisting}[label=BuildExample, caption=Example of a SCons build file.]
    # Build and intermediate build directories.
    # Subdirectories for each type of compilation will be created there.
    BUILD_DIR = '/Volumes/Data/sebastien/HappyHeart/Build'
    INTERMEDIATE_BUILD_DIR =  '/Volumes/Data/sebastien/HappyHeart/IntermediateBuild'

    # Choose C and C++ compilers.
    COMPILER= 'clang' # name used in output folders.
    CC = '/Users/Shared/LibraryVersions/clang/Openmpi/bin/mpicc'
    CXX = '/Users/Shared/LibraryVersions/clang/Openmpi/bin/mpic++'

    # Choose either 'debug' or 'release'.
    MODE='debug'

    # Choose either 'static' or 'shared'.
    LIBRARY_TYPE='static'

    # OpenMPI libary.
    OPEN_MPI_INCL='/Users/Shared/LibraryVersions/clang/Openmpi/include'
    OPEN_MPI_LIB='/Users/Shared/LibraryVersions/clang/Openmpi/lib'

    # Petsc library.
    PETSC_GENERAL_INCL='/Users/Shared/LibraryVersions/clang/Petsc/include'
    PETSC_DEBUG_INCL='/Users/Shared/LibraryVersions/clang/Petsc/debug/include'
    PETSC_RELEASE_INCL='/Users/Shared/LibraryVersions/clang/Petsc/release/include'

    PETSC_DEBUG_LIB='/Users/Shared/LibraryVersions/clang/Petsc/debug/lib'
    PETSC_RELEASE_LIB='/Users/Shared/LibraryVersions/clang/Petsc/release/lib'

    # Parmetis library.
    PARMETIS_INCL='/Users/Shared/LibraryVersions/clang/Parmetis/include'
    PARMETIS_LIB='/Users/Shared/LibraryVersions/clang/Parmetis/lib'

    # Lua library.
    LUA_INCL='/Users/Shared/LibraryVersions/clang/Lua/include'
    LUA_LIB='/Users/Shared/LibraryVersions/clang/Lua/lib'

    # Yuni library.
    YUNI_INCL='/Users/Shared/LibraryVersions/clang/Yuni/src'
    YUNI_DEBUG_LIB='/Users/Shared/LibraryVersions/clang/Yuni/src/build/debug/lib'
    YUNI_RELEASE_LIB='/Users/Shared/LibraryVersions/clang/Yuni/src/build/release/lib'

    # Ops directory (source files are looked there and library is compiled by HappyHeart).
    OPS_DIR='/Users/Shared/LibraryVersions/clang/Ops'

    # Seldon directory (source files are looked there and library is compiled by HappyHeart).
    SELDON_DIR='/Users/Shared/LibraryVersions/clang/Seldon'
    
\end{lstlisting}

Most of it is really the paths of the different libraries used by HappyHeart, plus few informations such as where to build the code and which variant is considered (see \refsection{SectionVariants}).
\medskip

As a matter of fact, the command given in \refcode{BasicSConsCommand} doesn't do much: it just gives the list of possible targets, as shown in \refcode{LstNoTarget}.
\medskip

\begin{lstlisting}[label=LstNoTarget, caption = Output to SCons command when no target is given.]
    scons: Reading SConscript files ...
    Mkdir("/Volumes/Data/sebastien/HappyHeart/IntermediateBuild/clang/debug")
    Mkdir("/Volumes/Data/sebastien/HappyHeart/Build/clang/debug/shared")
    Warning: no target was actually given, and as a result nothing was built. Possible targets are (more than one might be specified):
    	Bidomain_CRN
    	Bidomain_MS
    	Heat
    	ReactionDiffusion_CRN
    	ReactionDiffusion_FHN
    	ReactionDiffusion_MS
    	RivlinCube
    	SurfacicBidomain_CRN
    	SurfacicBidomain_MS
    	all
    	elasticity
    	fsi_ei_newton_2_meshes
    	hyperelasticity_CG_half_sum
    	hyperelasticity_CG_midpoint
    	hyperelasticity_StVK_half_sum
    	hyperelasticity_StVK_midpoint
    	libraries
    	stokes
    scons: done reading SConscript files.
    scons: Building targets ...
    scons: `.' is up to date.
    scons: done building targets.
    
\end{lstlisting}

 You may choose one or several targets in this list; \textit{all} is a special target that compiles everything.
 
\medskip

\begin{lstlisting}[label=BuildTargetCommand, caption = Command to build a target]
    scons --config_file=*build_configuration_file* *target name(s)*
\end{lstlisting}

and to clean:
\medskip

\begin{lstlisting}[label=CleanCommand, caption = Command to clean a target]
    scons --clean --config_file=*build_configuration_file* *target name(s)*
\end{lstlisting}


\section{Variants}\label{SectionVariants}

SCons enables the management of several variants of the code at the same time. Currently three criteria are used to define a variant:

\begin{itemize}
    \item The compiler used. The valid compilers are the ones for which a dedicated Python file is defined in \lstinline{HappyHeart/SCons/Compilers} folder. Currently clang and gcc 5 are supported, and icc should be added at some point.
    \item The type of the libraries used (shared or static).
    \item The mode (debug or release).
\end{itemize}

The build directory defined in \refcode{BuildExample} provides a hierarchy to hold each of these variants; for instance executables created from the \refcode{BuildExample} will land in \lstinline{/Volumes/Data/sebastien/HappyHeart/Build/clang/debug/static}.

\section{SConstruct and SConscript(s)}

The SConstruct file is left as empty as possible: the Python functions used inside are put in a dedicated directory in HappyHeart structure (\lstinline{HappyHeart/SCons}) and the targets to build are defined in specific SCons files named \textit{SConscript}.
\medskip

For a HappyHeart advanced user (i.e. someone who is writing his own Model using HappyHeart) two files are of interest:

\begin{itemize}
    \item \lstinline{HappyHeart/Sources/ModelInstances/SConscript}: In this file is the list of all model instances known to SCons build. You should just add here the name of the folder in which you are building your new model in the \lstinline{model_list} list.
    \item \lstinline{HappyHeart/Sources/ModelInstances/*your model*/SConscript}: SConscript defined for your model. An example of how this file should look like is given in \refcode{SConscriptExample}; a XCode template has been defined as well to give the basic structure the creator of the model must fill.
\end{itemize}


\begin{lstlisting}[label=SConscriptExample, caption=Example of a model SConscript file.]
    import os
    import copy
    import imp 

    Import('env')

    custom = imp.load_source('custom_scons_functions', '{0}/custom_scons_functions.py'.format(env["HAPPY_HEART_SCONS"]))

    # Custom part here: gives away the list of all compiled files (except the main).
    src = Split('''
        Model.cpp
        VariationalFormulation.cpp
    ''')


    # Second argument is the name given to the created library.
    model_lib = custom.HappyHeartLibrary(env, 'happy_heart_stokes', src)

    main_src = Split('''
        main.cpp
        ''')

    # Second argument is the name given to the created executable.
    stokes = custom.HappyHeartProgram(env, 'stokes', main_src, [model_lib])

    list_exec = (stokes, )
    list_lib = (model_lib, )

    Return('list_exec', 'list_lib')
   
\end{lstlisting}



\section{Special status of Seldon and Ops libraries}

Seldon and Ops are handled differently from other third-party libraries: their compilation is driven by HappyHeart's SConstruct rather than done independantly. Doing so ensures that the compilation options used for these libraries are the same as those used in HappyHeart itself.
\medskip

This choice is somewhat justified by the fact they are both developed by Inria team CLIME, with which feedback is much easier.
\medskip

To enable this, a SConscript named \lstinline{HappyHeartSConscript} has been added in Seldon and Ops folders and is called by HappyHeart build.
\medskip

These specific SConscripts are tailored for this specific usage and are not exactly the same as the ones used for HappyHeart models; for instance warnings stemming from Seldon and Ops files are silenced so that only those from HappyHeart code appear\footnote{This behaviour is actually entailed on all third party libraries used in HappyHeart through the use of pragmas.}.

\end{document}










