%!TEX root = /Users/sebastien/Codes/HappyHeart/Documentation/latex/happy_heart.tex
\chapter{Geometry}


Geometry part in HappyHeart covers what was in Geometry and IO in the original FELiScE code, reimplemented very differently. The point of the new implementation is to be much easier to maintain: it is for instance quite easy to provide a new type of geometric element, and there are very few risks of doing anything wrong in the process.
\medskip

The interface aims to be as user-friendly as possible, but the implementation by itself is quite subtle: a geometric element for instance is formed by several classes that acts through policies and traits. 
\medskip

I will sketch here the general idea of the different classes of the public interface; the full detail with all attributes described and implementation gory details may be found in the Doxygen documentation.

\section{GeometricMeshRegion}

This class is in charge of managing mesh data. 
\medskip

It can read or write Ensight and Medit files\footnote{Provided the geometric elements are supported: if you read an Ensight file with Quadrangle8 and attempt to write the resulting mesh as Medit you would get an exception as this type of geometric element is not supported by Medit.} and provide access to the mesh data.
\medskip

Its direct content is quite limited: 
\begin{itemize}
	\item The list of vertices if the mesh, as a \code{Vertex} smart pointers (see \refchapter{ChapSmartPointers}).
	\item The list of all geometric elements of the mesh. This list is actually an object itself called \code{GeometricElementList}. It is hidden in a Private namespace and it shouldn't be used directly by the user.
	\item The list of all the references, handled by a \code{Label} smart pointers.
	\item The dimension of the mesh.
	\item The number of edges and faces, if they have been built. Their building is not automatic: it could be quite consuming in term of memory and for no reason in some cases (for instance faces aren't actually used in current hyperelasticity code). The edges and faces objects themselves aren't currently stored there; they are however accessible from the \lstinline{GeometricElement} objects.
\end{itemize}

Most of its public interface is dedicated to access to underlying data; actually the only method that modify the structure of the object is \code{Init}, that must be called to build really the object\footnote{The constructor is mostly an empty shell that does barely anything. The reason is that a pointer \code{this} is given to \code{GeometricElement} objects in initialization, and such an operation can only be done with an object fully built.}.
\medskip

So typical creation is just:

\begin{lstlisting}
GeometricMeshRegion mesh;
mesh.Init("medit_mesh_file", GeometricMeshRegion::Format::Medit, GeometricMeshRegion::BuildEdge::yes);
GeometricMeshRegion other_mesh;
other_mesh.Init("ensight_mesh_file", GeometricMeshRegion::Format::Ensight);
\end{lstlisting}

Public interface is thoroughly described in the Doxygen documentation.


\section{Point/Coords/QuadraturePoint}\label{SecPoints}

Geometric points are handled by no less than three different classes: 
\begin{itemize}
	\item \code{Utilities::Point}
	\item \code{Coords} 
	\item \code{QuadraturePoint}. 
\end{itemize}
	Whereas it could seem cumbersome to do something so complicated for so simple a feature, it is actually a safeguard: it limits greatly the risk of providing something to a method that is not what is required\footnote{There are obviously no implicit conversion from one of this type to another, as it would break the purpose.}.
\medskip

The three types of points are:

\subsection{Utilities::Point}

This class is really simple, and is not even truly part of the geometry (it is compiled in a \code{Utilities} library): it is simply used to store the coordinates of a point, and is therefore little more than a std::vector with a slightly prettier interface.
\medskip

It is used internally by both of the classes presented below.

\subsection{Coords}

A \code{Coords} is basically a point that belongs to the mesh, and as such has a larger interface:
\begin{itemize}
	\item \code{Coords} retains some data such as to which label and mesh it belongs and what is its personal index in said mesh.\footnote{I do not endorse usually such indexes as it tends to break encapsulation, but an index is anyway required by some of the input/output mesh formats.}.
	\item Free functions are provided to write the coords in any format supported by HappyHeart (Ensight and Medit at the moment).
\end{itemize}

\code{Coords} is expected to be used through derived classes such as \code{Vertex} or \code{NodeOnEdge}.

\subsection{QuadraturePoint}

A \code{QuadraturePoint} is mostly a point with a weight.

\section{GeometricElement}

The heart of the Geometry implementation, which is in charge of most the geometry interface. Its actual implementation is quite complex, but the intended purpose is that its use should remain straightforward. 

\subsection{A sketch of geometric elements implementation}\label{SecGeoElementLayout}

GeometricElement objects are defined on three layers:
\begin{itemize}
	\item A \code{GeometricElement} class, which is dedicated to be used polymorphically (See section \ref{SecPolymorphism}). This class is abstract: no object of this type can be created directly. It features many virtual pure methods to overload in inherited classes.
	\item A \code{Private::TGeometricElement}, which purpose is to define all pure virtual methods of \code{GeometricElement}. The T prefix in its name stands for template: this class requires three template parameters, which are used as traits and CRTP (see \refchapter{ChapTemplate}). All pure virtual methods are overloaded and tagged as \code{final}, meaning an inherited class can't overload it any further. The reason of this structure are explained in \refsection{SecTemplateAndPolymorphism}.
	\item Instantiations of the different type of geometric elements. For instance:
\begin{lstlisting}
class Triangle3 : public Private::TGeometricElement<Triangle3, GeoRefNS::Triangle3, Format::Triangle3>
\end{lstlisting} Few additional static methods are added at this stage.
\end{itemize}

So the general idea is that at creation the program must take care of filling the vector of smart pointers in \code{GeometricMeshRegion} (this is typically done while reading a mesh from an Ensight or a Medit file) and that afterwards this is this vector which is actually manipulated. \code{GeometricMeshRegion} holds the methods that allow to access accurately to any subset of the list, for instance to only \code{Triangle3} in a given label.
\medskip

The \code{GeometricElement} base class is intended to fulfill all the public interface: there is no need in HappyHeart to perform a dynamic\_cast\footnote{Or more exactly its equivalent for smart pointers.} to transform the \code{GeometricElement} into the RTTI\footnote{Run-time type information.} type of the element (e.g. \code{Triangle3}).

\subsection{GeometricElementType}

As a matter of fact, \code{GeometricElement} class is made of two very different types of attributes:
\begin{itemize}
	\item Some that are related to the intrinsic nature of the geometric element, and in C++ are therefore handled with static const attributes (see \refsection{SecVirtualAndStatic}).
	\item Some that are specifically related to the geometric element being considered (for instance its position in the mesh or its index). Those are managed by non static attributes.
\end{itemize}

In some cases however, we would like to handle a generic geometric element type: for instance in \code{GeometricMeshRegion::BagDomain()} we need to know whether there are \code{Triangle3} and \code{Triangle6}, but we do not care about the position or the index. Some functionalities are completely irrelevant there: for instance calling \code{GetEdgeList()} doesn't make any sense, and in this case would lead to an exception that does not describe well the issue\footnote{The message would complain about GeometricMeshRegion::BuildEdgeList() not called, even if that one was dutifully called.}.
\medskip

The way to avoid this is to define a different class named \code{GeometricElementType}, which provides a much limited interface that covers only static const attributes of the class (see \refsection{SecInheritanceAndComposition} for a discussion about it).

\section{Others public classes}

\subsection{Label}

\code{Label} is a very tiny class that manages a label. It consists essentially of an index and optionally of a description\footnote{Such a description is supported by Ensight but not by Medit.}.

\subsection{QuadratureRule}

A quadrature rule is a simple object that holds two data: the list of all quadrature points for the shape considered, and the degree of exactness of the rule. 
Each \code{GeometricElement} type holds the list of all the quadrature rules that have been implemented.

\subsection{Edge and face}

An edge or a face is defined by all the nodes that are part of it; the nodes are ordered so that the lower index comes first (and the relative ordering is preserved). It is truly the \code{GeometricElement} that keeps track of the edges or faces that it has; it does only this if \code{GeometricMeshRegion::BuildEdgeList()} or \code{GeometricMeshRegion::BuildFaceList()} respectively have been called. What an object owns is a smart pointer to the edge or face; when several objects share an edge or a face it is really the same object that is pointed out (to avoid to waste uselessly resources).
\medskip

Even so, for a huge mesh storing all edges and faces is a costly operation, hence the need to specify in \code{GeometricMeshRegion} construction you need those.


\section{Mesh formats}\label{SecMeshFormat}

At the moment two formats are handled by HappyHeart code: Ensight and Medit. I will present very briefly how to interact with both of them below.

\subsection{Ensight}

The most straightforward: a mesh is basically built by reading an ascii file line by line. The format is specified in the chapter 9 of the user manual \citep{Ensight}. One reading is enough to build the mesh: each block begins with the exact number of elements to be read. If the number effectively read is not the one announced (ie the file is malformed) an exception is thrown in Happy Heart.

\subsection{Medit}

Medit approach is radically different: the file is not directly read by Happy Heart. Instead, the file is read by Lm5 library and interface occurs through Lm5 API. The detailed way to proceed is described in \citep{Lm5}.
\medskip

So for instance to read all the geometric elements in the mesh you must iterate through all the types supported by Medit, see if there are some in the mesh to read and if so bid Lm5 to unroll this list. This may be dangerous: one should be certain not to forget a type in the list of supported types, in which case the mesh would be incompletely read with no direct way to know a part of it has been left away.


\section{Partitioning the mesh}

The whole partitioning operation is devolved to the \code{DofManager} class, which decides how the degree of freedoms and the geometric elements are shared between the different processors.
\medskip

That being said, once that choice has been made we do not need to keep all the data read from the original file: geometric elements not handled by the current processor are for instance clearly irrelevant. So once the choice has been made, there is a call to \code{GeometricMeshRegion::ShrinkToProcessorWise()} that reduces the mesh data to the ones really relevant for the current processor. This method ensured that all that remains is actually useful: if for instance a label is not represented on a processor, it is guaranteed to be removed from the list of labels on this current processor.
\medskip

There is no need for a user to explicitly call \code{GeometricMeshRegion::ShrinkToProcessorWise()}: it is already handled by \code{DofManager::Init()}.


\section{Extending the code}

\subsection{How to add a new geometric element type to the code?}\label{SecAddNewGeoElement}

We have mostly seen how to use the geometric elements in HappyHeart: they are built polymorphically so essentially the vector that lists them in \code{GeometricMeshRegion}\footnote{To be accurate the actual vector is indeed in the \code{Private::GeometricElementList} object in \code{GeometricMeshRegion}.} and said vector is created while reading Ensight or Medit.
\medskip

The only missing step is to add a new geometric element type that is not currently handled in HappyHeart, and there are plenty: to limit unnecessary development overhaul while designing the \code{Geometry} only the following types are properly implemented: 
\begin{itemize}
	\item Point
	\item Segment2
	\item Segment3
	\item Triangle3
	\item Triangle6
	\item Tetraedre4
	\item Tetraedre10
\end{itemize}

So is it easy to add a new element? Unfortunately, the answer is hardly yes: there are several classes to write before this task is done. That is not as such a flaw in the code: there are numerous informations that are actually required to use effectively a geometric element type, and the code is built so that it won't compile until all is correctly provided\footnote{It is far to be the case in FELiScE: pyramids were seemingly implemented but in truth basis functions weren't implemented at all.}. The reproach that can be made is that there are numerous classes to be added whereas one would have been possible; it is actually a design choice to separate clearly functionalities\footnote{And it will come handy when \code{ReferenceElement} will be reimplemented: some of the traits class will be reusable directly for it.}.
\medskip

The good news is that although this is a tedious work, there are little chance of doing it wrong\footnote{Provided you enter the numerous and lengthy formula correctly\dots}: if a method is missing the compiler will complain about it, which is much better than an unexpected crash in runtime.
\medskip

Several classes are to be defined, as we shall see in the steps described below, but their content is extremely similar to the ones of geometric element types that already exist. So you should really have a glimpse at existing files and modifying them to describe accurately the new type to be introduced.
\medskip

Hereafter are the steps to create a new type of geometric element (say \code{Quadrangle4}), fully functional:

\begin{enumerate}
	\item Create a new identifier in \code{GeometricElementEnum}, defined in \path{Geometry/geometric\_elements}.
	\item In \path{Geometry/geometric\_elements}, create the new class \code{Quadrangle4} in HappyHeart namespace. You should do it by taking an existing object that shares the same IO format support, to get it right directly\footnote{Otherwise you might miss a required method, such as \code{EnsightName()} for Ensight support, but the compiler won't let you go with it.}, and adapt it to your new object. A very important part is to register the new element to the \code{Private::GeometricElementFactory}(see \refchapter{ChapObjectFactory}). The identifier(s) must be unique(s)\footnote{There are typically one identifier per supported IO format, and two for HappyHeart itself (an enum and a string description).}, or an error will be generated at the very beginning the execution of the program.
	\item If you adapted from an existing new element, you must have now a line:
\begin{lstlisting}
typedef Private::GeoRefElement<GeoRefWithoutBasisNS::QuadrangleQ1, BasisFunctionNS::QuadrangleQ1> Quadrangle4;
\end{lstlisting}
Both template parameters are Traits classes (see sections \ref{SecGeoElementLayout} and \ref{SecTrait}).
	\item Define \code{BasisFunctionNS::QuadrangleQ1}, in \path{Geometry/geometric\_elements/BasisFunctions}; three static methods and two enum values are expected. Definition itself might be a tedious task (there are for instance 135 functions to define for second derivatives of a \code{PrismR2}!); the formula were taken directly from FELiScE source code.
	\item Check in \path{Geometry/private/Shape.hpp} whether the shape already exists or not\footnote{What I call shape here is for instance Triangle for a Triangle3 or a Triangle6: the properties that are true for all geometric figures with three summits and three edges.}. If not, create a new typedef in namespace ShapeNS.
	\item If you have to create a new shape, you'll probably have to define the list of quadrature rules as well, in \path{Geometry/geometric\_elements/QuadratureRules}. As usual, see how the already existing	elements are defined; this time the CRTP idiom is used (see \refsection{SecCRTP}).
	
	\item Define \code{GeoRefWithoutBasis::QuadrangleQ1}, in \path{Geometry/geometric\_elements/GeoRefWithoutBasis}. There are actually two classes to define in \path{QuadrangleQ1.hpp}: another Traits class, and  \code{GeoRefWithouBasis::QuadrangleQ1} itself which is actually defined with a CRTP (see \refsection{SecCRTP}).

\end{enumerate}


\subsection{How to add a new IO format?}\label{SecAddNewFormat}

This one is much less straightforward than adding a new element, as you have to take into account the way the data are stored in said IO format and also to understand more finely how HappyHeart Geometry is actually implemented (you have to manipulate directly the objects of the private interface). I can't therefore provide a turnkey procedure to add a new IO format, but here are some hints of what should be done:

\begin{enumerate}
	\item Edit \path{Geometry/private/IOFormat.hpp} and add your new IO format:
	\begin{itemize}
		\item Add an enum value in List corresponding to the new type. This will be used for instance in \code{GeometricMeshRegion} constructor to decide which format should be used to decipher the input file.
		\item In \code{Traits} template class, add a new template parameter for the new type. Proceed then as for existing type.
	\end{itemize}
	\item In \path{Geometry/geometric\_elements}, edit all geometric element types and make sure to add the new template parameter just added in \code{Traits}: each geometric type must know whether it can support the new IO format or not.
	\item If it can, you will have to add a new static method that returns the adequate identifier; this will be used to serialize/deserialize the geometric element object (see \refchapter{ChapObjectFactory}). Its nature depends on the IO format: it is for instance a mere string for Ensight, an enum value for Medit.
	\item Write the serialization methods or free functions for \code{MeshPoint} and \code{GeometricElement} classes (search for instance in the code \code{WriteEnsightFormat} to see an example). The interface might differ considerably from one format to another.\footnote{No generic template method was used here (such as \code{WriteFormat<Ensight>}) because we needed these functions to be virtual as well.}
	\item Then the most important part: you will need to provide a specialization of \code{GeometricMeshRegion::Read<>} and \code{GeometricMeshRegion::Write<>} for the new format. It is the delicate part of the process; you should have a look at \path{GeometricMeshRegion\_\_Medit.cpp} and \path{GeometricMeshRegion\_\_Ensight.cpp} to see two very different instantiations. It is very important to check here the process is consistent; the first test to do is read a mesh file and write it again to see if it is consistent\footnote{It is not necessarily strictly equal: in some format there might be leeway for instance in the manner the labels or the elements are ordered.}.
	
	
One last note: the public interface avoids at all cost to disclose the indexes of the object: when for instance you want the vertices that belongs to a geometric element, smart pointers over them is returned. However, when a file is read this is typically handled by indexes. The best to do is to keep the indexes as they are: if your format use Fortran-style arrays (this is typically the case for Medit), do the same and start indexing at 1.
	
\end{enumerate}







