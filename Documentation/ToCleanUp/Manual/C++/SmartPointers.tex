\chapter{Smart pointers}\label{ChapSmartPointers}

As we shall see, smart pointers are a really useful extension of the concept of pointer, which makes their use much easier and also much less dangerous, at a very reasonable cost.

\section{The RAII idiom}\label{RAII}

The principe behind the smart pointers is the so-called RAII, that stands for \textit{Resource Acquisition Is Initialization}. The idea is very simple: when you have a structure that requires both an initialization and a closing operation, you encapsulate it into an object.

\subsection{Very straightforward smart pointer implementation}

So very basically for a RAII around a pointer you have:

\begin{lstlisting}[caption=Principle of a smart pointer class definition: RAII over a pointer, label=SketchSmartPtr]

// SmartPtr.hpp
template<class T>
class SmartPtr
{
	public:
	
	// Constructor
	SmartPtr(T* object);
	
	// Destructor
	~SmartPtr();
	...
	
	private:
	
	// Underlying pointer.
	// Direct access should be limited as possible; in particular
	// user of smart pointer class shouldn't be allowed to delete it directly!
	T* pointer_;	
};

template<class T>
SmartPtr<T>::SmartPtr(T* object)
: pointer_(object)
{ }

template<class T>
SmartPtr<T>::~SmartPtr()
{
	delete pointer_;
}

\end{lstlisting}


\subsection{Example of use: exit routes of a function}

At first sight, not much has been done, but there is now no need to take care of destruction: it is handled gracefully by the smart pointer itself whatever the exit route taken by the run of the code.

So for instance in the function below:

\begin{lstlisting}
// MyFile.cpp
#include "GeometricElement.hpp"
#include "Triangle3.hpp"
#include "SmartPtr.hpp"

int MyFunction()
{
	std::vector<unsigned int> vertices { 3, 5, 6 };
	SmartPtr<GeometricElement> pointer(new Triangle3(vertices));

	if (first_condition)
		return 1;
	[...]
	SomeOtherFunction();
	[...]
	if (second_condition)
		return 2;
	
	return 0;
}

\end{lstlisting}

memory covered by \code{pointer} local variable which be released in all of the following paths:
\begin{itemize}
	\item first\_condition is fulfilled.
	\item second\_condition is fulfilled.
	\item End of the function declaration is reached.
	\item An exception is thrown anywhere in the function block.
\end{itemize}

Without smart pointer, the pointer should have been deallocated explicitly for every of these exit routes\dots which is extremely tedious and inefficient to achieve: you would have for instance to catch all exceptions from \code{SomeOtherFunction}:

\begin{lstlisting}
try
{
	SomeOtherFunction();
}
catch(...) // catch all possible exceptions
{
	delete pointer;
	throw;
}
\end{lstlisting}

Furthermore, maintaining such a code could be a real pain: you would have to make sure the memory is correctly deallocated should a new exit route be provided.

\subsection{Example of use: smart pointers as data attribute}\label{SecSmartPtrAttribute}

A key usage of smart pointers is as data attribute: it helps tremendously to share a same object among several classes. To understand why, consider the following case with raw pointers:

\begin{lstlisting}
class GeometricElement
{
	...
	SurfaceReference* surface_reference_;
};


class MeshPoint
{
	...
	SurfaceReference* surface_reference_;
};


\end{lstlisting}

In this example, the same surface reference object might shared by two different classes. It can be very useful: if the underlying surface reference object is modified somehow, all classes that bears a pointer to this class will automatically refer to the new state of the \code{SurfaceReference} class. This could also be achieved by a reference, but a reference is limited to the cases the underlying object is known when the constructor is called, as you can't init a reference data attribute elsewhere.
\medskip

The problem arises at the deletion of one of these objects: what to do with the pointer data attribute? If you foresee its deletion within the destructor, it can cause havoc: the \code{SurfaceReference} will be destroyed for all classes. So if \code{GeometricElement} destroys it, \code{GeometricMeshRegion} will not have any longer access to it, which is not probably what you want. On the other hand, if no class manages the destruction of this \code{SurfaceReference} object, you must be absolutely sure to delete it at some point, to avoid memory leak. 
\medskip

You can see how complicated it can become: the architecture may become quickly very unwieldy, as you must track yourself whether an object is used or not and make sure to delete if no object uses it.
\medskip

Some sort of smart pointers nail neatly this problem: the \code{SurfaceReference} object is deleted as soon as no one uses it.

\section{Ownership policies of smart pointers}

The smart pointer class described in \refcode{SketchSmartPtr} is very sketchy: lots of details have to be added to provide a functional smart pointers. Implementing a fully functional smart pointer is an art by itself; \citep{Alexandrescu2001} for instance describes in its seventh chapter a beautiful policy-based\footnote{See \refsection{SecPolicy}} design  that allow to craft the exact smart pointer you need. You can for instance decide whether the underlying object is a pointer or not, what to do when an invalid request is done, whether the smart pointer is thread-safe or not, etc\dots 
\medskip

The most important policy is however the ownership policy, which answers the following question: what is the behaviour during a copy?
\medskip

I will present in this section several possible ownership policies, with the STL implementation specified\footnote{In the STL, there are 4 different smart pointers defined, and you can't specify the other behaviour such as the thread-safety or the error handling.}

\subsection{Destructive smart pointer}

I mentioned this one because it was the only one available in the STL up to C++ 11.
\medskip

It was a last minute add to the 2003 revision of the standard, and one that was rather ill-defined, to the point it has been deprecated in C++ 11 standard. 
\medskip

The principle is that ownership is transferred: 
\begin{lstlisting}
std::vector<unsigned int> vertices { 3, 5, 7 };
std::auto_ptr<GeometricElement> pointer(new Triangle3(vertices));

std::auto_ptr<GeometricElement> new_pointer = pointer;
// Now pointer is NULL
\end{lstlisting}

The problem is that ownership can sometimes be transferred without the developer realizing it, which is often called a \textit{sink effect}:

\begin{lstlisting}
void MyFunction(std::auto_ptr<GeometricElement> pointer);
MyFunction(new_pointer);
// new_pointer is NULL now!
\end{lstlisting}

You can delve into much more details concerning \code{std::auto\_ptr} and its shortcomings in item 8 of \citep{MeyersSTL}. Destructive smart pointers are never used in HappyHeart.

\subsection{Unique smart pointers}

The underlying idea of a unique smart pointer, such as \code{std::unique\_ptr} is that only one pointer should point to the dynamically allocated object. In that respect it is very close to the destructive smart pointer, but it is much less dangerous to use: transfer of ownership can only be done explicitly, using an explicit move semantics:

\begin{lstlisting}
#include <memory> // header for STL smart pointers

std::unique_ptr<GeometricElement> first_element(new Triangle3);

std::unique_ptr<GeometricElement> copied_element(first_element); 
// ERROR (detected at compile time with clang++/libc++)

std::unique_ptr<GeometricElement> moved_element(std::move(first_element)); 
// OK; first_element is now nullptr.

\end{lstlisting}

Unique smart pointers are not used a lot in Happy Heart, as for many objects what we want is to use multiple pointers to the same underlying object.
\medskip

For more details about them, you can read section 5.2 of \citep{JosuttisSTL}.


\subsection{Reference-counted smart pointers}\label{SecSharedPtr}

The principle of a reference-counted smart pointer is that the smart pointers hold an internal count on the number of pointers to a given object, and that this object will be destroyed only when this count reaches 0.
\medskip

It is therefore really a smart pointer of this type that addresses best the issues discussed in \refsection{SecSmartPtrAttribute}, where several objects were sharing a pointer to a same pointee.
\medskip

Reference-counted smart pointers are really adapted to be used within containers, as will be explained more in depth in \refsection{SecPolymorphism}, and therefore they are currently heavily featured in HappyHeart (for geometric elements, vertices, surface reference, etc\dots)
\medskip

STL version of such a smart pointer is called \code{std::shared\_ptr}.
\medskip

For more details about them, you can read section 5.2 of \citep{JosuttisSTL}.

\subsection{Weak smart pointer}\label{SecWeakPtr}

There is a case in which issues may arise with reference-counted smart pointers: when two objects refers each other through a smart pointer:

\begin{lstlisting}
#include <iostream>
#include <memory>
#include <vector>

class GeometricMeshRegion;

class GeometricElement
{
	public:  
		
		GeometricElement(unsigned int index)
		: index_(index)
		{ }
		
		~GeometricElement()
			{ std::cout << "Destroy element " << index_ << std::endl;}
			
		void AddNeighbour(std::shared_ptr<GeometricElement> neighbour)
			{ neighbour_list_.push_back(neighbour); }
	
	
	private:
		
		std::vector<std::shared_ptr<GeometricElement> > neighbour_list_;	
		
		unsigned int index_;
};


int main()
{
	std::shared_ptr<GeometricElement> element1(new GeometricElement(1));
	std::shared_ptr<GeometricElement> element2(new GeometricElement(2));
	element1->AddNeighbour(element2);
	element2->AddNeighbour(element1);		

	return 0;
}
\end{lstlisting}

In the code above the \textit{Destroy element} lines are never printed: internally the reference counts never reach 0 and the objects are therefore never deleted.
\medskip

The solution is to use a \textbf{weak smart pointer}: this is basically a shared smart pointer which is not counted in the reference count. So in the code above the only change is to declare \code{neighbour\_list} as 

\begin{lstlisting}
std::vector<std::weak_ptr<GeometricElement> > neighbour_list_;
\end{lstlisting} 

The expected lines in the destructor would then appear correctly, meaning the destructor are this time neatly called\footnote{There are no mistake here: \code{AddNeighbour} might take a \code{shared\_ptr} as argument that will be implicitly converted into a weak one through the \code{push\_back} operation.}.
\medskip

Weak smart pointer is also discussed in depth in section 5.2 of \citep{JosuttisSTL}, with in particular a more detailed example of the circular reference issue.

\subsection{Intrusive smart pointers}\label{SecIntrusivePtr}

So far, the smart pointers we saw could be used on any object, be it an integral type, a home-made class or a class stemming from a library. It is no longer true for an intrusive smart pointers, which requires the object it embeds to store the reference count.
\medskip

Doing so present many advantages over a reference counted smart pointer:
\begin{itemize}
	\item It consumes less memory: the reference count is stored once (in the object) whereas with reference counted smart pointers each smart pointer has to store it by itself one way or another (that usually means an additional pointer to the structure that hold the reference counting).
	\item There are no risk of circular reference problem such as pointed out in \refsection{SecWeakPtr}.
\end{itemize}

However this comes at a cost:

\begin{itemize}
	\item They can be used only along classes tailored for them. So it can't be used with an object declared in an outside library. That is not that much of a problem for us, but it is better to keep that limitation in mind.
	\item There are no such smart pointer provided in the STL; an exterior library is therefore required here.	
\end{itemize}

Currently HappyHeart is using such intrusive smart pointers, more specifically the implementation from the Yuni C++ library.



\section{Caution on smart pointer use}

A possible security risk with some pointers is present if the user is allowed to deal directly with the underlying raw pointer (see \refcode{SketchSmartPtr} to remember the basic structure of a smart pointer class.).
\medskip

That is the reason for which most smart pointer implementation do not provide an implicit conversion to a raw pointer (see chapter 7 \citep{Alexandrescu2001} for a thorough discussion on this point.).
\medskip

Even so, there is often a method provided to give access to the underlying pointer, and it really should be used with care:

\begin{lstlisting}
#include "GeometricElement.hpp"

int main()
{
	std::shared_ptr<GeometricElement> element(new GeometricElement);
	GeometricElement* ptr = element.get(); // really ., not -> (it is a smart pointer method, not a GeometricElement one)
	delete ptr; 
	
	element->GetVertices(); // undefined behaviour: element pointee no longer exists!
	
}
\end{lstlisting}

The only reason they could be required is when you interface with an exterior library, and in this case you should be sure the function can't delete the pointer or modify its pointee.
\medskip

There are no occurrences in HappyHeart of raw pointer call such as presented above; all is handled with the smart pointer interface.


\section{Convenient typedef in classes}\label{SecPtrTypedef}

I usually use the following notations in the classes that might be used along with a smart pointer:

\begin{lstlisting}


class GeometricElement
{
	public:
	
		typedef std::shared_ptr<GeometricElement> Ptr;
		typedef std::vector<Ptr> Vector;
};

\end{lstlisting}

There are several perks to do so:
\begin{itemize}
	\item It is shorter to type: \code{GeometricElement::Vector} is more easy to write and read than \\
	 \code{std::vector<std::shared\_ptr<GeometricElement> >}.
	\item Should we change our mind and decide to use an intrusive one instead, we would mostly have to change the typedef (and obviously tailor \code{GeometricElement} for intrusive smart pointer requirements). Compilers will say the few places that will need to be reworked (for instance if \code{std::make\_shared} has been used to create one). It is therefore both quicker and safer that to track all instances of the smart pointer into the code.
\end{itemize}

There are however some cons as well:
\begin{itemize}
	\item We can't rely on forward declaration: \code{std::shared\_ptr<GeometricElement>} can be used without the definition of \code{GeometricElement}, whereas \code{GeometricElement::Ptr} can't.
	\item There is a subtle case in which overload might not work as expected, when mixed with template class (see \refcode{CodeTemplatePtr} and the discussion there.).
\end{itemize}


\section{Vector of smart pointers and polymorphism}

\subsection{Vector of pointers versus vector of objects}

In HappyHeart, the storage of choice is a vector of smart pointers, which short hand notation in the code has been introduced in \refsection{SecPtrTypedef}.
\medskip

The first reason is that there is a huge performance gain when using vector of pointers: many operations are much faster with pointers than with full-fledged objects. For instance, the STL vector allocate a certain amount of memory when you create a vector, which can be known with method \code{capacity}. When you add a new element with \code{push\_back}:

\begin{itemize}
	\item Either there is enough room in the capacity for your new element, in which case the element is simply added.
	\item The size is equal to the capacity, and there is no room for a new element. In this case, a new vector is created, with a greater capacity, and all elements are copied from the old vector to the new vector. The pushed element is then added in the more roomy vector.
\end{itemize}

Copying a full-fledged object might be costly if the object is important, whereas copy a simple pointer is a cheap operation, both in execution time and in memory (it copies an address which is usually 4 or 8 bits-long).
\medskip

Combining this with smart pointer makes the concern about the destruction of the vector irrelevant, as the smart pointer does take care of that all by itself.


\subsection{Polymorphism}\label{SecPolymorphism}

Another powerful feature opened by a vector of (smart) pointers is the polymorphism:

\begin{lstlisting}[caption=A simple example of polymorphism]
GeometricElement::Vector geometric_elements;

{
	std::vector<unsigned int> vertice(3, 5, 7);
	GeometricElement::Ptr triangle = new Triangle3(vertice);
	geometric_elements.push_back(triangle);
}
	
{
	std::vector<unsigned int> vertice(2, 42, 87);
	GeometricElement::Ptr triangle = new Triangle3(vertice);
	geometric_elements.push_back(triangle);
}
	
{	
	std::vector<unsigned int> vertice(12, 39, 61, 23);
	GeometricElement::Ptr tetra = new Tetraedre4(vertice);
	geometric_elements.push_back(tetra);
}

...

// New C++ 11 notation to iterate through an entire container
for (GeometricElement::Ptr geometric_element : geometric_elements_)
{
	...
	Point point;
	...
	geometric_element->BasisFunction(0, point);
}

\end{lstlisting}

In the code above (and in HappyHeart code) all geometric elements are stored inside the same container, and when \code{BasisFunction()} is called what is returned depends on the nature of the object: the basis function to call for the tetrahedron is not the same used for the triangle\dots 
\medskip

To make the code understand this, the following conditions must be fulfilled:
\begin{itemize}
	\item \code{Triangle3} and \code{Tetraedre4} must inherit from \code{GeometricElement}.
	\item \code{BasisFunction} method must be declared as virtual in the \code{GeometricElement} class.
	\item \code{GeometricElement} destructor should be virtual\footnote{See item 7 of \citep{MeyersCpp} to understand why; to make it very short data attributes declared in derived classes might not be released upon destruction.}.
\end{itemize}

This behaviour, called polymorphism, can only work with pointers or references, not with full-fledged objects. As references can't be stored inside containers, pointers are the sole choice for using polymorphism.
\medskip

It should be noted that whereas polymorphism is an extremely useful feature, it comes at a cost, which is fully described in \refsection{SecTemplateAndPolymorphism}.






