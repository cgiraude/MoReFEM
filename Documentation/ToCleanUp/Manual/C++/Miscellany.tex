\chapter{Miscellany}\label{ChapMiscellany}

\section{Careful use of static attributes}

\subsection{Reminder: what is a static attribute in C++}

A static attribute is a property at class level, by opposition to usual attributes which are defined at object level.
\medskip

The typical example is a counter that would keep track on the number of objects of this class that are actually instantiated (see code example \ref{CodeStaticDemo}; an actual use of such a counter is described in \refsection{SecSharedPtr}).

\begin{lstlisting}[caption=Example of use of static attributes, label=CodeStaticDemo]
// In Triangle.hpp
struct Triangle
{
	// Constructor.
	Triangle();
	
	// Destructor.
	~Triangle();
	
	// Returns number of summit.
	static const unsigned int Nsummit();
	
	// Returns number of geometric element instantiated.
	static unsigned int Nobject;
};

// In Triangle.cpp
Triangle::Triangle() { ++Nobject; }
Triangle::~Triangle() { --Nobject; }

const unsigned int Triangle::Nsummit() { return 3; }
// %*Note: See \refsection{SecStaticInitialization} for an important problem I'm blatently ignoring below.*)
unsigned int Triangle::Nobject = 0u;

// In main.cpp

int main()
{
	// Next line will always print '3'; note no object was instantiated.
	std::cout << Triangle::Nsummit() << std::endl; // 3 
	
	std::cout << Triangle::Nobject << std::endl; // 0
	Triangle triangle_1;
	std::cout << Triangle::Nobject << std::endl; // 1
	{
		Triangle triangle_2;
		std::cout << Triangle::Nobject << std::endl; // 2
	}
	std::cout << Triangle::Nobject << std::endl; // 1	
	
}


\end{lstlisting}

\subsection{static keyword in C/C++}

\code{static} was also a keyword in C language, which was basically used in two contexts:
\begin{itemize}
	\item When used along with a function, it means the function won't be exported outside the current compilation unit. So if a file test.cpp you declare and define \lstinline{static void MyFunction();}, it will be visible only to the other function and methods defined in the same cpp file. It is a nice way to keep local helper functions without cluttering the code with useless symbols\footnote{In C++ anonymous namespace are usually preferred to static functions (see \refsection{SecAnonymousNamespace}). }.
	\item When used along with a variable, it means the variable will be initialized there and in next iterations the line will be ignored. The variable lifetime runs until the very end of the program (see code example \ref{CodeCStaticVariable}).
	
\end{itemize}

\begin{lstlisting}[caption=Static keyword for a variable, label=CodeCStaticVariable]
void MyFunction()
{
	static bool first_call = true; // read only in first call
	
	if (first_call)
	{
		... some initialization steps ...
		first_call = false; // now first_call is false
	}
	
} // value still exists after MyFunction is ended.

int main()
{
	...
	MyFunction();
	...
	return 0;
}

// first_call will be deallocated only there, after the end of the main function.


\end{lstlisting}





This reminder isn't innocent at all: we shall see that using static data attributes can be very tricky, and the C static keyword is the way to solve the pitfall.

\subsection{Static initialization of attributes}\label{SecStaticInitialization}

When a class defines a \code{static} data attribute, as \code{Nobject} in \refcode{CodeStaticDemo}, the C++ standard imposes that the value is set in an implementation (i.e. suffixed by .cpp in HappyHeart) file.
\medskip

The problem with that prerequisite is that when there are several implementation files to compile: there is absolutely no guarantee upon the compilation order.
\medskip

In the case above for instance, if \code{main.cpp} is compiled before \code{Triangle.cpp} the behaviour is undefined: no value has yet been assigned to \code{Nobject}.
\medskip

The danger is that is can remain unseen in your compilation environment: if \code{Triangle.cpp} is compiled before \code{main.cpp}, the code behaves exactly as you think it does. But if someone else compiles it it might compile the other way\dots
\medskip

So static data attributes should be avoided at any cost, except in the very peculiar case in which a class is defined in a cpp file (typically for a helper class that might be defined in an anonymous namespace - see \refsection{SecAnonymousNamespace}).
\medskip

Item 4 of \citep{MeyersCpp} provides a nifty way to work around this problem: data attributes are replaced by static method, inside which a static variable is stored. The best way to explain it quickly is probably to demonstrate it though an example:

\begin{lstlisting}
// In Triangle.hpp
struct Triangle
{
	...
	
	// Returns number of geometric element instantiated.
	static unsigned int& Nobject();
};

// In Triangle.cpp
unsigned int& Triangle::Nobject()
{
	static unsigned int value = 0;
	return value;
}

Triangle::Triangle() { ++Nobject(); }
Triangle::~Triangle() { --Nobject(); }

\end{lstlisting}

Instead of reading directly a data attribute \code{Nobject} which might be undefined, the code reads a method \code{Nobject()} and will seek its definition. The value is initialized in first call, so there is no risk at all to deal with an uninitialized value.
\medskip

The cost is cheap: extra parenthesis to write and a very slight overhead due to the function call.
\medskip

This trick is used heavily in Happy Heart, for instance for \code{GeometricElement} in which many properties are not specific to the current instantiation.





\section{C-array replacement}

In Happy Heart, C-style arrays are never used to store a collection of values:  \code{std::vector} and \code{std::array}\footnote{std::array is a new C++ 11 feature when the number of elements is frozen and known at compile-time.} are better choices to do so, for several reasons:

\begin{itemize}
	\item Deallocation is automatically enforced through RAII (see \refsection{RAII}).
	\item No need to keep track of the size of the container: it is provided by the method \code{size()}. When using C arrays, you have to bookkeep it with care; so for instance if an array is stored in a class you have to also keep the size as another attribute.
	\item Resizing is easy through methods such as \code{push\_back} or \code{resize}.
	\item Copy is much easier: you can rely upon default copy constructor and assignment operator in your classes that store data attributes in these containers. That is not the case with C-arrays: if you create a new object from an existing one, both will share the same underlying array, which is probably not your intent:
	\begin{lstlisting}
	// In hpp file
	struct ClassWithCArray
	{
		int* c_array_;
	};
	
	
	// In main.cpp
	ClassWithCArray first;
	[ ... init the object here ...]
	ClassWithCArray second(first);
	
	second.c_array_[0] = 5; // first is also modified!!!
	\end{lstlisting}

\end{itemize}

There is one occurrence in which a C-array might seem unavoidable: when it is an argument of a C library. For instance, 

\begin{lstlisting}
// Prototype of Petsc function
PetscErrorCode  VecGetValues(Vec x, PetscInt ni, const PetscInt ix[], PetscScalar y[]);
\end{lstlisting}

\code{y} is here an array that might be modified by the function, including its size. But actually the C++ standard guarantees the contiguity of memory for a \code{std::vector}, so you can pass the pointer to the first element, exactly as you do with a C-array:

\begin{lstlisting}
Vec x;
std::vector<PetscInt> ix(ni); 
std::vector<PetscScalar> y(ni); // Petsc expects memory to be already allocated
[...]
VecSetValues(x, ix.size(), &ix[0], &y[0]);															
\end{lstlisting}

This syntax is not very pleasant\footnote{Though quite straightforward: it tells to take the address of the very first element}; new standard C++ 11 introduced a more palatable:

\begin{lstlisting}
VecSetValues(x, ix.size(), ix.data(), y.data());
\end{lstlisting}

To learn more about this topic, see items 13 and 16 of \citep{MeyersSTL}.



\section{Inheritance and composition}\label{SecInheritanceAndComposition}

\subsection{How to limit the interface to a class}

Actualiser:

It is mostly just an extra level of indirection: this class is implemented with an underlying \code{GeometricElement} object as a private attribute, and relevant methods are just strawmen that actually calls the underlying object





\section{Anonymous namespace}\label{SecAnonymousNamespace}


\section{Error handling}\label{SecErrorHandling}


\section{Move semantic}\label{SecMoveSemantic}