

<!-- toc -->

- [Install Miniconda environment](#install-miniconda-environment)
- [Install third party libraries](#install-third-party-libraries)
- [Install gitlab-runner](#install-gitlab-runner)
  * [As an administrator:](#as-an-administrator)
  * [As CI user:](#as-ci-user)

<!-- tocstop -->

# Install Miniconda environment


````
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh
conda update conda
conda create -n Python3  python=3
conda activate Python3
conda install six
python -m pip install distro
````

# Install third party libraries

````
git clone https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory.git
````

Edit morefem_macos_clang.py:

````
    target_directory = "/Volumes/Data/ci/opt/clang_{}".format(mode)
    python = "/Users/ci/miniconda3/bin/python"
````

In Conda environment Python3:

````    
python morefem_macos_clang.py
````

# Install gitlab-runner

## As an administrator:

````
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64

sudo chmod +x /usr/local/bin/gitlab-runner
````

## As CI user:

````
gitlab-runner register
````

Then choose:

- Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):

    Copy/paste password the choice on ![Gitlab page](../Images/RunnerCredential.png).
    
- Please enter the gitlab-ci token for this runner:

    The token given in Runner page (see image above).
    
- Please enter the gitlab-ci description for this runner:

    You may just repeat the name of the machine... or really put whatever you want (you may change it afterwards anyway).
    
- Please enter the gitlab-ci tags for this runner (comma separated):
    
    Put the tags for the runner you want to set up, e.g. "macos,debug,shared"
    
- Please enter the executor:

    Choose _shell_.
    

The runner should now appear in gitlab, but possibly with a grey dot instead of a green one. In this case, type:

````
gitlab-runner --debug run 
````

which should make the dot green within a minute.