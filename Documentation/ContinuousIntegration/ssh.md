# SSH access

<!-- toc -->

- [Generate a pair of public and private keys](#generate-a-pair-of-public-and-private-keys)
- [Register this key in CI platform](#register-this-key-in-ci-platform)
- [Shortcut to ease connexion](#shortcut-to-ease-connexion)

<!-- tocstop -->

To connect yourself to the CI Inria VMs, you need first to follow several steps documented [here](https://wiki.inria.fr/ciportal/Slaves_Access_Tutorial).

To put in a nutshell:

## Generate a pair of public and private keys

For instance for the M3DISIM-CI account, I have generated a key with:

````
mkdir ~/.ssh/m3disim-ci
ssh-keygen -t rsa -C "m3disim-ci@inria.fr" -b 4096 -f ~/.ssh/m3disim-ci/id_rsa
````

## Register this key in CI platform

On the [CI platform](https://ci.inria.fr), log yourself in and click on your login:

![screenshot login](Images/login.png)

and choose "My account".

There, copy the *public* key generated above (i.e. the content of the file ~/.ssh/m3disim-ci/id_rsa.pub).

## Shortcut to ease connexion

As indicated on the [CI tutorial](https://wiki.inria.fr/ciportal/Slaves_Access_Tutorial), you should edit ~/.ssh/config and write inside:

````
Host *.ci
ProxyCommand ssh mci001@ci-ssh.inria.fr "/usr/bin/nc `basename %h .ci` %p"
````

(replace mci001 by your CI login if you're not using M3DISIM-CI account).
