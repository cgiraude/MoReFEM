

<!-- toc -->

- [Introduction](#introduction)
- [Set up the Yaml file to use](#set-up-the-yaml-file-to-use)
- [Runners in Gitlab interface](#runners-in-gitlab-interface)
  * [Runner with docker executor](#runner-with-docker-executor)
    + [Introduction](#introduction-1)
    + [Setting up your Docker executors](#setting-up-your-docker-executors)
      - [Activate specific runner](#activate-specific-runner)
      - [Create new Docker runners on MoReFEM VMs](#create-new-docker-runners-on-morefem-vms)
        * [Example: on VM Docker1](#example-on-vm-docker1)
        * [The 5 others VMs](#the-5-others-vms)
  * [Runner with shell executor (on macOS)](#runner-with-shell-executor-on-macos)

<!-- tocstop -->

# Introduction

Gitlab-CI relies on a relatively simple configuration:

- A Yaml file is provided within the Git repository, and is used by Gitlab to describe the continuous integration tasks.
- Runners which take up the jobs described in the Yaml and run them.

Gitlab offers a way to share runners between several projects... unfortunately currently not activated in the Inria instance.

So I have defined very basic VMs (with the procedure described [here](Internal/SetUpVMForDockerExecutor.md)) onto which you may define your own runners.

As you shall see, the procedure is rather fast: you should have 7 (Docker) runners defined in around 10 minutes

# Set up the Yaml file to use

By default, if CI is activated for your projects Gitlab assumes it is at the root of the projet and is named .gitlab-ci.yml. In MoReFEM I chose to put it elsewhere (so that they appear more explicitly in the project and several may be defined) so you need to specify the path:

* Go in _Settings_ / _CI/CD_ / _General pipelines_.
* In _Custom CI config path_, choose the one you wish.

The most usual is _ExternalTools/Gitlab-CI/gitlab-ci.yml_, but if you do not have access to a macOS machine _ExternalTools/Gitlab-CI/gitlab-ci-no-macos.yml_ is more fitting.

# Runners in Gitlab interface

The most usual runner choices are _docker_ and _shell_. I will describe briefly both of them: Docker ones will be useful for our Linux builds, and shell ones for macOS ones.

## Runner with docker executor

### Introduction

The idea of a docker executor is to run the job inside a Docker image.

This is a very appealing idea because:

- Maintainance is much easier: you need to just maintain few Docker images that are then deployed on as many machines as you wish.
- Ressources may be used more efficiently: a same machine may be used for thoroughly different projects. There is an experimentation at Inria to provide many machines able to run such Docker executors.

There are however few drawback a well:

- Debugging is harder: if the log doesn't give you the reason of an eventual failure, you can't easily (or at least I didn't manage to) grab the container to play with it. The official documentation [alludes briefly](https://docs.gitlab.com/runner/executors/) to this issue but doesn't provide more explanation. You may of course run locally in Docker what was run remotely during CI to play with it and understand the issue.
- Docker is really tightly coupled to Linux and you can't run macOS (or Windows but we don't support it anyway in MoReFEM) images.

The base images used in MoReFEM are available in [ThirdPartyCompilationFactory registry](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory/container_registry).


### Setting up your Docker executors

In your own fork of MoReFEM (you need to get a high enough status to be able to reach settings of a project, and for your fork you obviously have it!), go in the __Settings__ tab, choose __CI/CD__ and then __Runners__.

#### Activate specific runner

There is an on-going test to provide a Docker farm at Inria; so far three machines are provided in the experimentation phase:

![SpecificRunners](Images/SpecificRunners.png)

Choose _INRIA QLF-CI_ large which is provides enough horsepower to run our job in a reasonable time.

#### Create new Docker runners on MoReFEM VMs

While the Docker farm is not available, we need to define our very own VM to run the tasks, which is a waste (we are sitting over Go of RAM that are used only once in a while).

First make sure your [SSH credentials](ssh.md) are properly registered.

You also need to make a request to _M3DISIM-CI@inria.fr_ to be added as members to the MoReFEM CI projects; this alias encompasses currently Philippe Moireau, Dominique Chapelle and Sébastien Gilles. The procedure to add new members is described [here](Internal/AddMembersToCIProjects.md) (might be useful as Sébastien is currently the only one aware of it).

So connect on each of them and set-up your own runner:

##### Example: on VM Docker1

````
ssh ci@docker1.ci
````

(ask the password at m3disim-ci@inria.fr).

On this machine:

````
sudo gitlab-runner register
````

Then choose:

- Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):

    Copy/paste password the choice on ![Gitlab page](Images/RunnerCredential.png).
    
- Please enter the gitlab-ci token for this runner:

    The token given in Runner page (see image above).
    
- Please enter the gitlab-ci description for this runner:

    You may just repeat the name of the machine... or really put whatever you want (you may change it afterwards anyway).
    
- Please enter the gitlab-ci tags for this runner (comma separated):
    
    Skip that: we don't use tags so far.
    
- Please enter the executor:

    Choose _docker_.
    
- Please enter the default Docker image
    
    Type ``alpine``
    
Your runner should appear shortly on ther gitlab runner pages with a green dot.

Now there is currently an issue with Inria configuration, so you need to add an option in config.toml file to make things run smoothly (see [here](https://gitlab.inria.fr/siteadmin/doc/wikis/faq#using-a-docker-executor) for the explanation). 

````
sudo nano /etc/gitlab-runner/config.toml
````

and add ``network_mode = "host"`` in _[runners.docker]_ section of your runner.



##### The 5 others VMs

Repeat the exact same procedure on the following VMs (I've not been very consistent on names...):

ssh ci@morefem-gitlab-docker-2.ci  
ssh ci@morefem-gitlab-docker-3.ci  
ssh ci@morefem-gitlab-docker-4.ci  
ssh ci@docker5.ci  
ssh ci@docker6.ci 
    
## Runner with shell executor (on macOS)

macOS can't run in a Docker container, so we will need to use shell executors on machines. A shell executor simply means the job will run directly on the machine; so you need to connect yourself regularly on this machine to update it.

There is currently only one such executor on M3DISIM machine _Nostromo_; you need to be part of the team to be able to use it. If you don't have access to it, you may use the _gitlab-ci-no-macos.yml_ provided which skips the related job in your Gitlab project (see [here](#set-up-the-yaml-file-to-use)).

If you have credential to _Nostromo_:

* Log in with your account.
* Change account and choose CI (same credential as for Linux VM).
* Run _gitlab-runner_ (without _sudo_) and choose:

    - Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):

        Copy/paste password just as for the Linux case.
    
    - Please enter the gitlab-ci token for this runner:

        Same as Linux case.
    
    - Please enter the gitlab-ci description for this runner:

        You may just repeat the name of the machine... or really put whatever you want (you may change it afterwards anyway).
    
    - Please enter the gitlab-ci tags for this runner (comma separated):
    
        Choose _macos_.
    
    - Please enter the executor:

        Choose _shell_.




