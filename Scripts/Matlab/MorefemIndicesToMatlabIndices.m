function [indices_renumbering_in_matlab, indices_hh_in] = MoReFEMIndicesToMatlabIndices(path_hh, mesh_number, numbering_subset_number, indices_in_name_file, dimension)
% Here this script is made to have indices but not all of them.
% Essentially usefull to extract Dirichlet BC dofs. 

[interfaces_indices, dof_infos_indices] = LoadInterfacesAndDofsInfosMoReFEM(path_hh, mesh_number, numbering_subset_number);

indices_out = CreateIndicesOutMoReFEMToMatlab(interfaces_indices, dof_infos_indices, dimension);

indices_hh_in = load(indices_in_name_file);

indices_hh_in = sort(indices_hh_in);

nDof_local = length(indices_hh_in);

indices_renumbering_in_matlab = 0*indices_hh_in;

for i=1:nDof_local
    indices_renumbering_in_matlab(i) = indices_out(indices_hh_in(i) + 1);
end

% +1 because matlab goes from 1 to nDof not 0 to nDof - 1
indices_renumbering_in_matlab = sort(indices_renumbering_in_matlab) + 1;

end