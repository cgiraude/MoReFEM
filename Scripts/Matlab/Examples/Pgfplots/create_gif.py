#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Need image magick and pdftk. Easy to install.
# https://www.pdflabs.com/tools/pdftk-server/
# https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/pdftk_server-2.02-mac_osx-10.11-setup.pkg  : the first link seems not to work on El Capitan
# http://www.macports.org/install.php for imagemagick first
# http://imagemagick.org/script/binary-releases.php 
# sudo port install ImageMagick
# Need 2 matlab scripts create_HH_pgfplots and load_files and 2 other files : output_plot.tex and delete_last_page.sh.
# First one creates the data in the right format for the pgfplot in the Data folder.
# The second one is needed in the first one to load the results from HH in matlab.
# The matlab script create_HH_pgfplots may be changed and adapt to your cases. 
# For now it only works for sequential files hhdata from MoReFEM named solution. 
# The reason is that is you create Ensight output files .scl matlab will not be able to load it...
# For sequential name = solution extension = hhdata.
# 

import sys, os, re

option_file = sys.argv[1]

init_path = os.path.dirname(os.path.abspath(__file__))

dir_hh = list()
name_data_output = list()
color = list()
legend = list()
extension = list()
name_input = list()

execfile(init_path + "/" + option_file)

########### Script
directory_path = os.path.dirname(os.path.abspath(__file__)) + "/" + project_name

if not os.path.exists(directory_path):
    os.makedirs(directory_path)
else:
    sys.exit("The directory already exists. Script stopped.")
    
os.system("cp output_plot.tex " + directory_path + "/output_plot.tex")
os.chdir(directory_path)

os.mkdir("Data")

directory_data = os.path.dirname(os.path.abspath(__file__)) + "/Data"

# Loop for every plot to create the data to plot 
for i in range(0, len(dir_hh)):
    os.system('/Applications/MATLAB_R2015b.app/bin/matlab -nodesktop -nosplash -r "addpath(\'~/Codes/MoReFEM/Scripts/Matlab/Examples/Pgfplots\');create_HH_pgfplots(\''+ dir_hh[i] + '\', \'' + name_input[i] + '\' , \'' + extension[i] + '\' , \''+ str(x_init) + '\', \''+ str(x_final) + '\', \''+ str(dx) + '\', \''+ directory_data + '\', \''+  name_data_output[i] + '\');quit"')

# Max and min
if not (manual_y_bounds):
    print('Matlab bounds for Y.')
    max_min_file = open(directory_data + '/max_min.dat', 'r')
    line = max_min_file.readline()
    ymin = float(line.split(" ")[1])
    ymax = float(line.split(" ")[0])
    max_min_file.close()

# Adapt the latex to the number of plots
latex_file = open('output_plot.tex', 'r+')
latex_file.read()

latex_file.write('  \multiframe{'+ str(num_time_step) + '}{iIndex=1+' + str(increment_time_step) + '}{\n')
latex_file.write('\\begin{tikzpicture}\n')
latex_file.write('	\\begin{axis}[axis background/.style={fill=white},\n')
latex_file.write('		title = {\\begin{large}' + title + ' \end{large}},\n')
latex_file.write('		ymin = ' + str(ymin) + ',\n')
latex_file.write('		ymax = ' + str(ymax) + ',\n')
latex_file.write('		xmin = ' + str(xmin) + ',\n')
latex_file.write('		xmax = ' + str(xmax) + ',\n')
latex_file.write('		xtick= ' + xtick + ',\n')
latex_file.write('		ytick= ' + ytick + ',\n')
latex_file.write('		]\n')

# Loop over the plots
for i in range(0, len(dir_hh)):
    latex_file.write('		\\addplot[color=' + color[i] + '!50,no markers,line width=1pt,each nth point={1}]  table[x=grid,y=solution]   {Data/' + name_data_output[i] + '_\iIndex.data};\n')
    latex_file.write('		\\addlegendentry{\\begin{small}' + legend[i] + '\end{small}}\n')

# End of the latex file
latex_file.write('	\end{axis} \n')
latex_file.write('\end{tikzpicture}\n')
latex_file.write('}\n')
latex_file.write('\end{animateinline}\n')

# Make sur to get the last time step = num_time_step*increment_time_step + 1
latex_file.write('\\begin{animateinline}{10}\n')
latex_file.write('\\begin{tikzpicture}\n')
latex_file.write('	\\begin{axis}[axis background/.style={fill=white},\n')
latex_file.write('		title = {\\begin{large}' + title + ' \end{large}},\n')
latex_file.write('		ymin = ' + str(ymin) + ',\n')
latex_file.write('		ymax = ' + str(ymax) + ',\n')
latex_file.write('		xmin = ' + str(xmin) + ',\n')
latex_file.write('		xmax = ' + str(xmax) + ',\n')
latex_file.write('		xtick= ' + xtick + ',\n')
latex_file.write('		ytick= ' + ytick + ',\n')
latex_file.write('		]\n')

# Loop over the plots
for i in range(0, len(dir_hh)):
    latex_file.write('		\\addplot[color=' + color[i] + '!50,no markers,line width=1pt,each nth point={1}]  table[x=grid,y=solution]   {Data/' + name_data_output[i] + '_' + str(num_time_step*increment_time_step + 1) + '.data};\n')
    latex_file.write('		\\addlegendentry{\\begin{small}' + legend[i] + '\end{small}}\n')

# End of the latex file
latex_file.write('	\end{axis}\n')
latex_file.write('\end{tikzpicture}\n')
latex_file.write('\end{animateinline}\n')

# End of the document
latex_file.write('\n')
latex_file.write('\end{document}\n')

latex_file.close()

# Convert latex in pdf
os.system('pdflatex output_plot.tex output_plot.pdf')

print("PDF created.")

# Remove last page that is blank, problem with latex that has not been solved.
os.system(init_path + '/delete_last_page.sh')
os.system('rm -f output_plot.pdf')
os.system('mv output.pdf output_plot.pdf')

# Convert in gif
os.system("convert -verbose -delay 10 -loop 1 -density 600 output_plot.pdf output_plot.gif")

print("End of the Script.")
