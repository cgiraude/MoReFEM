#!/bin/bash

last=$(pdftk output_plot.pdf dump_data | grep NumberOfPages | sed 's/[^0-9]*//')
pdftk /output_plot.pdf cat 1-$((last-1)) output output.pdf verbose
