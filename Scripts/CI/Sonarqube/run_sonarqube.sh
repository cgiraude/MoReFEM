#!/bin/bash

# create the sonarqube config file
cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.inria.fr/sonarqube
sonar.login=38a5102208b9b34fb90fdc89358289239488da30

sonar.links.homepage=$CI_PROJECT_URL
sonar.links.scm=$CI_REPOSITORY_URL
sonar.scm.disabled=true
sonar.scm.provider=git

sonar.projectKey=m3disim:morefem:1500_speedup_analysis
sonar.projectDescription=MoReFEM finite element library
sonar.projectVersion=0.9

sonar.language=c++
sonar.sources=Sources
sonar.cxx.includeDirectories=Sources,/usr/include,/usr/lib64/clang/8.0.0/include,/usr/include/c++/v1

sonar.sourceEncoding=UTF-8

sonar.cxx.errorRecoveryEnabled=true
sonar.cxx.compiler.reportPath=build_4_sonarqube/morefem-build.log
sonar.cxx.clangsa.reportPath=build_4_sonarqube/analyzer_reports/*/*.plist
sonar.cxx.cppcheck.reportPath=morefem-cppcheck.xml
sonar.cxx.rats.reportPath=morefem-rats.xml

EOF

echo "sonar-project.properties file written."

# run sonar analysis 
STARTTIME=$(date +%s)
sonar-scanner -X >& sonar.log
ENDTIME=$(date +%s)

echo "Output written in sonar.log; analysis performed in $(($ENDTIME - $STARTTIME)) seconds."


# Left out in sonar.cxx.includeDirectories to be a tad more faster:
# /opt/Boost/include,/opt/Libmeshb/include,/opt/Lua/include,/opt/Openblas/include,/opt/Openmpi/include,/opt/Parmetis/include,/opt/Petsc/include,/opt/Xtensor/include,/opt/Tclap/include