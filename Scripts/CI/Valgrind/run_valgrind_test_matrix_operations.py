import os

from run_valgrind_tools import MoReFEMRootDir, RunValgrind

if __name__ == "__main__":

    morefem_root_dir = MoReFEMRootDir()
    lua_file = os.path.join(morefem_root_dir, "Sources", "Test", "ThirdParty", "PETSc", "MatrixOperations", "demo.lua")

    RunValgrind("Sources/MoReFEMTestPetscMatrixOperations",  lua_file, os.path.join(morefem_root_dir, "memcheck_test_matrix_operations.txt"), is_model = False)
