import os
import pathlib
import sys
import shutil
import subprocess


def MoReFEMRootDir():
    """Returns the path to the root dir of MoReFEM.
    
    This uses up the fact the directory in which present script is stored is known.
    """
    return pathlib.Path(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..").resolve()
    

class RunValgrind:
    """Compile the sources related to one executable and then run the Valgrind analysis on the executable.
    
    \param[in] executable Executable to compile (e.g. MoReFEM4Elasticity).
    \param[in] lua_file Lua file with the data required to run the model.
    \param[in] output File in which output of the Valgrind analysis is written.
    \param[in] is_model True if a model is involved, False otherwise.
    """

    def __init__(self, executable, lua_file, output, is_model = True):
        self.__executable = executable
        self.__lua_file = lua_file
        self.__output = output
        self.__morefem_root_dir = MoReFEMRootDir()
        self.__is_model = is_model
        
        try:
            self._callCMake()        
            self._compile()
            self._runValgrind()
        except subprocess.CalledProcessError as e:
            print("Error in process: {}".format(e))
            sys.exit(e.returncode)
        
        
    def _callCMake(self):
                
        morefem_cmake_dir = os.path.join(self.__morefem_root_dir, "cmake")
        
        cmd = ("python",
               f"{morefem_cmake_dir}/Scripts/configure_cmake.py",
               f"--cache_file={morefem_cmake_dir}/PreCache/linux.cmake",
               f'--cmake_args=-G Ninja',
               "--third_party_directory=/opt",
               '--mode=debug')
        
        subprocess.run(cmd, shell = False).check_returncode()


    def _compile(self):
        cmd = ("ninja", self.__executable)
        subprocess.run(cmd, shell = False).check_returncode()
        
    
    def _runValgrind(self):     

        os.environ["MOREFEM_ROOT"] = str(self.__morefem_root_dir)

        cmd = ["valgrind",
               "--error-exitcode=1",
               f"--log-file={self.__output}",
               "--gen-suppressions=all",
               "--show-leak-kinds=all",
               "--show-reachable=yes",
               "--track-origins=yes",
               "--leak-check=full",
               "--errors-for-leak-kinds=all",
               "--suppressions=../ExternalTools/Valgrind/openmpi.suppr",
               "--suppressions=../ExternalTools/Valgrind/libgomp.suppr", 
               "--suppressions=../ExternalTools/Valgrind/libc.suppr", 
               self.__executable,
               "-i",
               self.__lua_file,               
               "-e",
               "MOREFEM_RESULT_DIR=Results"]
               
        if self.__is_model:
            cmd.append("--overwrite_directory")

        try:
            subprocess.run(cmd, shell = False).check_returncode()
        except subprocess.CalledProcessError as e:
            print("Failure encountered while running command: \n\t{}\n".format(" ".join(cmd)))
            print("[with the environment variable MOREFEM_ROOT set to '{}']\n\n".format(os.environ["MOREFEM_ROOT"]))
            raise e
