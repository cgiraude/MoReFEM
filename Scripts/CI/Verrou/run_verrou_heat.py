import os

from run_verrou_tools import MoReFEMRootDir, RunVerrou

if __name__ == "__main__":

    morefem_root_dir = MoReFEMRootDir()
    morefem_model_instances_dir = os.path.join(morefem_root_dir, "Sources", "ModelInstances")

    lua_file = os.path.join(morefem_model_instances_dir, "Heat", "demo_input_heat.lua")
#
    RunVerrou("Sources/MoReFEM4Heat",  lua_file, os.path.join(morefem_root_dir, "verrou_heat.txt"))
