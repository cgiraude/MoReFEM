# StarPU --- Runtime system for heterogeneous multicore architectures.
#
# Copyright (C) 2012                                     Inria
# Copyright (C) 2012-2017                                CNRS
# Copyright (C) 2014-2018                                Université de Bordeaux
#
# StarPU is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
#
# StarPU is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See the GNU Lesser General Public License in COPYING.LGPL for more details.
#

{
   suppr1
   Memcheck:Param
   writev(vector[...])
   ...
   fun:ompi_mpi_init
   ...
}

{
   suppr2
   Memcheck:Addr4
   ...
   fun:orte_init
   ...
}

{
   suppr3
   Memcheck:Param
   sched_setaffinity(mask)
   ...
   fun:orte_init
   ...
}

{
   suppr4
   Memcheck:Addr8
   ...
   fun:orte_init
}

{
   suppr5
   Memcheck:Leak
   ...
   fun:ompi_mpi_init
}

{
   suppr5
   Helgrind:Race
   ...
   fun:ompi_mpi_init
}

{
   suppr6
   Memcheck:Leak
   ...
   fun:mca_pml_base_open
}

{
   suppr7
   Memcheck:Leak
   ...
   fun:orte_init
}

{
   suppr7
   Memcheck:Leak
   ...
   fun:orte_progress_thread_engine
}

{
   suppr7
   Helgrind:Race
   ...
   fun:orte_progress_thread_engine
}

{
   suppr8
   Memcheck:Leak
   ...
   fun:orte_ess_base_app_setup
}

{
   suppr9
   Memcheck:Leak
   ...
   fun:opal_paffinity_base_open
}

{
   suppr10
   Memcheck:Leak
   ...
   fun:ompi_mpi_finalize
}

{
   suppr10
   Helgrind:Race
   ...
   fun:ompi_mpi_finalize
}

{
   suppr10
   Helgrind:Misc
   ...
   fun:ompi_mpi_finalize
}

{
   suppr10
   Helgrind:PthAPIerror
   ...
   fun:ompi_mpi_finalize
}

{
   suppr11
   Memcheck:Leak
   ...
   fun:mca_base_components_open
}

{
   suppr12
   Memcheck:Param
   writev(vector[...])
   ...
   fun:PMPI_Init_thread
}

{
   suppr13
   Memcheck:Param
   writev(vector[...])
   ...
   fun:PMPI_Init_thread
}

{
   suppr14
   Memcheck:Param
   sched_setaffinity(mask)
   ...
   fun:PMPI_Init_thread
}

{
   suppr15
   Memcheck:Leak
   fun:malloc
   fun:ompi_free_list_grow
   ...
   fun:opal_progress
   fun:ompi_request_default_test
   fun:PMPI_Test
}

{
   suppr15
   Memcheck:Leak
   fun:malloc
   fun:opal_free_list_grow
   ...
   fun:opal_progress
   fun:ompi_request_default_test
   fun:PMPI_Test
}

{
   suppr16
   Memcheck:Leak
   fun:malloc
   fun:ompi_ddt_set_args
   fun:PMPI_Type_vector
}

{
   suppr17
   Memcheck:Leak
   fun:malloc
   fun:ompi_ddt_optimize_short.constprop.0
   fun:ompi_ddt_commit
   fun:PMPI_Type_commit
}

{
   suppr18
   Memcheck:Leak
   fun:calloc
   fun:ompi_ddt_create
   fun:ompi_ddt_create_vector
   fun:PMPI_Type_vector
}

{
   suppr19
   Memcheck:Leak
   fun:malloc
   fun:ompi_ddt_create
   fun:ompi_ddt_create_vector
   fun:PMPI_Type_vector
}

{
   suppr20
   Memcheck:Leak
   fun:malloc
   fun:ompi_free_list_grow
   ...
   fun:PMPI_Isend
}

{
   suppr20
   Memcheck:Leak
   fun:malloc
   fun:opal_free_list_grow
   ...
   fun:PMPI_Isend
}

{
   suppr20
   Memcheck:Leak
   fun:malloc
   fun:ompi_free_list_grow
   ...
   fun:PMPI_Barrier
}

{
   suppr20
   Memcheck:Leak
   fun:malloc
   fun:opal_free_list_grow
   ...
   fun:PMPI_Barrier
}

{
   suppr21
   Memcheck:Leak
   ...
   fun:hwloc_topology_set_xmlbuffer
   fun:opal_hwloc_unpack
   fun:opal_dss_unpack_buffer
}

{
   suppr22
   Memcheck:Leak
   ...
   fun:hwloc_topology_set_xmlbuffer
   fun:opal_hwloc_unpack
}

{
   suppr23
   Memcheck:Leak
   ...
   fun:hwloc_topology_load
   fun:opal_hwloc_unpack
}


{
   suppr24
   Memcheck:Leak
   fun:malloc
   ...
   fun:xmlParseElement
}

{
   suppr25
   Memcheck:Leak
   match-leak-kinds: indirect
   ...
   fun:ompi_datatype_commit
   fun:PMPI_Type_commit
}

{
   suppr26
   Memcheck:Leak
   match-leak-kinds: definite
   ...
   fun:ompi_datatype_create_vector
   fun:PMPI_Type_vector
}

{
   suppr27
   Memcheck:Leak
   match-leak-kinds: indirect
   ...
   fun:ompi_datatype_create_vector
   fun:PMPI_Type_vector
}

{
   suppr28
   Memcheck:Leak
   match-leak-kinds: indirect
   fun:malloc
   fun:ompi_datatype_set_args
   fun:PMPI_Type_vector
}

{
   suppr29
   Memcheck:Leak
   ...
   fun:PMPI_Comm_split
   fun:main
}

{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: definite
   fun:malloc
   fun:orte_grpcomm_base_update_modex_entries
   fun:orte_grpcomm_base_modex_unpack
   ...
   fun:opal_event_base_loop
   fun:opal_progress
   obj:*
   fun:ompi_modex_recv_key_value
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: definite
   fun:malloc
   obj:/usr/lib/openmpi/lib/libmpi.so.1.0.8
   fun:orte_grpcomm_base_update_modex_entries
   fun:orte_grpcomm_base_modex_unpack
   ...
   fun:opal_event_base_loop
   fun:opal_progress
   obj:*
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: indirect
   fun:malloc
   fun:orte_grpcomm_base_update_modex_entries
   fun:orte_grpcomm_base_modex_unpack
   ...
   fun:opal_event_base_loop
   fun:opal_progress
   obj:*
   fun:ompi_modex_recv_key_value
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: indirect
   fun:malloc
   obj:/usr/lib/openmpi/lib/libmpi.so.1.0.8
   fun:orte_grpcomm_base_update_modex_entries
   fun:orte_grpcomm_base_modex_unpack
   ...
   fun:opal_event_base_loop
   fun:opal_progress
   obj:*
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: reachable
   fun:malloc
   fun:lt__malloc
   fun:lt__zalloc
   obj:/usr/lib/x86_64-linux-gnu/libltdl.so.7.3.0
   fun:lt_dlopenadvise
   obj:/usr/lib/x86_64-linux-gnu/hwloc/hwloc_cuda.so
   obj:/usr/lib/x86_64-linux-gnu/hwloc/hwloc_cuda.so
   obj:/usr/lib/x86_64-linux-gnu/libhwloc.so.5.6.8
   fun:hwloc_topology_init
   fun:opal_hwloc_unpack
   fun:opal_dss_unpack_buffer
   fun:opal_dss_unpack
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: reachable
   fun:malloc
   fun:strdup
   obj:/usr/lib/x86_64-linux-gnu/libhwloc.so.5.6.8
   obj:/usr/lib/x86_64-linux-gnu/libltdl.so.7.3.0
   obj:/usr/lib/x86_64-linux-gnu/libltdl.so.7.3.0
   fun:lt_dlforeachfile
   obj:/usr/lib/x86_64-linux-gnu/libhwloc.so.5.6.8
   obj:/usr/lib/x86_64-linux-gnu/libhwloc.so.5.6.8
   fun:hwloc_topology_init
   fun:opal_hwloc_unpack
   fun:opal_dss_unpack_buffer
   fun:opal_dss_unpack
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: reachable
   fun:malloc
   obj:/usr/lib/x86_64-linux-gnu/libhwloc.so.5.6.8
   obj:/usr/lib/x86_64-linux-gnu/libltdl.so.7.3.0
   obj:/usr/lib/x86_64-linux-gnu/libltdl.so.7.3.0
   fun:lt_dlforeachfile
   obj:/usr/lib/x86_64-linux-gnu/libhwloc.so.5.6.8
   obj:/usr/lib/x86_64-linux-gnu/libhwloc.so.5.6.8
   fun:hwloc_topology_init
   fun:opal_hwloc_unpack
   fun:opal_dss_unpack_buffer
   fun:opal_dss_unpack
   fun:orte_util_nidmap_init
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: definite
   fun:malloc
   fun:opal_dss_unpack_string
   fun:opal_dss_unpack_buffer
   fun:opal_dss_unpack
   fun:orte_grpcomm_base_update_modex_entries
   fun:orte_grpcomm_base_modex_unpack
   ...
   fun:opal_event_base_loop
}
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: indirect
   fun:malloc
   fun:strdup
   fun:orte_grpcomm_base_update_modex_entries
   fun:orte_grpcomm_base_modex_unpack
   ...
   fun:opal_event_base_loop
   fun:opal_progress
   obj:*
}


{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: definite
   fun:realloc
   ...
   fun:event_process_active_single_queue
   fun:event_process_active
   fun:opal_libevent2022_event_base_loop
   obj:*
}

{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: indirect
   ...
   fun:event_process_active
   fun:opal_libevent2022_event_base_loop
   obj:*
   fun:start_thread
}



{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: definite
   fun:malloc
   ...
   fun:event_process_active_single_queue
   fun:event_process_active
   fun:opal_libevent2022_event_base_loop
   obj:*
   fun:start_thread
}



