stages:
    - build_and_test
    - check_warnings
    - deploy
    - verrou
    - valgrind
    - analysis
    - generate_sonarqube
    - deploy_docker_images


# As suggested by https://blog.sparksuite.com/7-ways-to-speed-up-gitlab-ci-cd-times-29f60aab69f9
variables:
  DOCKER_DRIVER: overlay2

.build_template_linux: &build_template_linux
    stage: build_and_test
    except:
        - master
    image: registry.gitlab.inria.fr/morefem/thirdpartycompilationfactory/${OS}-${COMPILER}-${MODE}
    cache:
        key: "cache_${CI_PROJECT_ID}_${CI_COMMIT_REF_SLUG}_${OS}-${COMPILER}-${MODE}-${LIB_NATURE}-${IS_ONLY_ONE_LIB}"
        untracked: true
        paths:
            - build
    artifacts:
        name: "artifact_${CI_PROJECT_ID}_${CI_COMMIT_REF_SLUG}_${OS}-${COMPILER}-${MODE}-${LIB_NATURE}-${IS_ONLY_ONE_LIB}"
        expire_in: 2 days
        when: always
        paths:
            - build/compilation.log
    script:
        - mkdir -p build
        - cd build && python ../cmake/Scripts/configure_cmake.py --cache_file=../cmake/PreCache/linux.cmake --cmake_args="-G Ninja"  --third_party_directory=/opt --mode=${MODE} --library_type=${LIB_NATURE}  --morefem_as_single_library=${IS_ONLY_ONE_LIB}
        - ninja |& tee compilation.log
        - ctest --output-on-failure
    after_script:
        # Remove the final targets that might be huge with static library, as cache creation could be beforehand
        # more than 80 % of the time spent on the job in the worst cases!
        - mv build/Sources/CMakeFiles Tmp
        - rm -rf build/Sources
        - mkdir -p build/Sources
        - mv Tmp build/Sources/CMakeFiles
        
        
.build_template_macos: &build_template_macos
    stage: build_and_test
    tags:
        - macos
    except:
        - master        
    cache:
        key: "cache_${CI_PROJECT_ID}_${CI_COMMIT_REF_SLUG}_${OS}-${COMPILER}-${MODE}-${LIB_NATURE}-${IS_ONLY_ONE_LIB}"
        untracked: true
        paths:
            - build
    artifacts:
        name: "artifact_${CI_PROJECT_ID}_${CI_COMMIT_REF_SLUG}_${OS}-${COMPILER}-${MODE}-${LIB_NATURE}-${IS_ONLY_ONE_LIB}"
        expire_in: 2 days
        when: always
        paths:
            - build/compilation.log
    script:
        - mkdir -p build
        - conda activate Python3
        - cd build && python ../cmake/Scripts/configure_cmake.py --cache_file=../cmake/PreCache/macos_apple_clang.cmake  --cmake_args="-G Ninja"  --third_party_directory=/Volumes/Data/ci/opt/clang_${MODE} --mode=${MODE} --library_type=${LIB_NATURE}  --morefem_as_single_library=${IS_ONLY_ONE_LIB}
        - ninja 2>&1 | tee compilation.log
        - ctest --output-on-failure 
    
    
.run_doxygen_template: &run_doxygen_template
    stage: build_and_test
    image: ubuntu:latest
    artifacts:
        name: "artifact_run_doxygen_${CI_PROJECT_ID}_${CI_COMMIT_REF_SLUG}_${DOX}"
        expire_in: 1 day
        paths:
            - Documentation/Doxygen/${DOXYGEN_OUTPUT_FILE}
    before_script:
        - apt-get update -y
        - apt-get install -y doxygen graphviz libjs-mathjax

    script:
        - cd Documentation/Doxygen
        - doxygen ${DOX}
    
.check_compilation_warning_template_docker: &check_compilation_warning_template_docker
    stage: check_warnings
    except:
        - master
    image: alpine:latest
    before_script:
        - apk add --update python python-dev && rm -rf /var/cache/apk/*
    # dependencies: in the instantiations!
    script:
        - python Scripts/Tools/find_warning_in_compilation_log.py --log build/compilation.log
    allow_failure: true
    
.check_compilation_warning_template_macos: &check_compilation_warning_template_macos
    stage: check_warnings
    except:
        - master
    # dependencies: in the instantiations!
    script:
        - python Scripts/Tools/find_warning_in_compilation_log.py --log build/compilation.log
    allow_failure: true    
    
    
.check_doxygen_warning_template: &check_doxygen_warning_template
    stage: check_warnings    
    except:
        - master    
    image: alpine:latest
    artifacts:
        name: "artifact_check_warning_doxygen_${CI_PROJECT_ID}_${CI_COMMIT_REF_SLUG}_${DOXYGEN_OUTPUT_FILE}"
        when: on_failure
        expire_in: 1 day
        paths:
            - filtered_doxygen_log.txt
    before_script:
        - apk add --update python python-dev && rm -rf /var/cache/apk/*
    # dependencies: in the instantiations!
    script:
        - ls
        - ls Documentation/Doxygen
        - ls Documentation/Doxygen/${DOXYGEN_OUTPUT_FILE}
        - python Scripts/Tools/find_warning_in_doxygen_log.py --log Documentation/Doxygen/${DOXYGEN_OUTPUT_FILE}
    allow_failure: true    
    

    
